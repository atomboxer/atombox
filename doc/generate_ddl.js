var fs = require('fs')
, markdown = require('ab_markdown')
    //, markdown = require('github-flavored-markdown').markdown
    , stream
    , opts
    , buffer = ""
    ;


var fullpath = "ddl.md.IMPORTANT";
buffer = fs.readFileSync(fullpath, 'utf8');

var html = markdown.parse(buffer);

//
//
//
var head = '<head>    <meta charset="utf-8"> <!--[if lt IE 9]>      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>    <![endif]-->    <script type="text/javascript" src="js/syntaxhighlighter/scripts/shCore.js"></script>   <script type="text/javascript" src="js/syntaxhighlighter/scripts/shBrushJScript.js"></script> <script type="text/javascript" src="js/syntaxhighlighter/scripts/shBrushDDL.js"></script><script type="text/javascript" src="js/syntaxhighlighter/scripts/shBrushXml.js"></script><script type="text/javascript" src="js/syntaxhighlighter/scripts/shBrushPlain.js"></script> <link type="text/css" rel="stylesheet" href="js/syntaxhighlighter/styles/shCoreEclipse.css"/>  <link type="text/css" rel="stylesheet" href="styles/atombox.css"> <link type="text/css" rel="stylesheet" href="styles/ddl_atombox.css"> </head>'

html = head + '<section>'+html+'</section>';
html += '<script type="text/javascript">SyntaxHighlighter.all();</script>'+'</body>';
 
var outpath = fullpath.replace(/\.md/g,".html");

html = html.replace(/<pre><code class="lang-javascript">/g,'<pre class="brush: js;gutter:false;collapse:false">');
html = html.replace(/<pre><code class="lang-js">/g,'<pre class="brush: js;gutter:false;collapse:false">');
html = html.replace(/<pre><code class="lang-ddl">/g,'<pre class="brush: ddl;">');
html = html.replace(/<pre><code class="lang-xml">/g,'<pre class="brush: xml;gutter:false;collapse:false">');
html = html.replace(/<pre><code class="lang-plain">/g,'<pre class="brush: plain;gutter:false;collapse:false">');
html = html.replace(/<pre><code class="lang-">/g,'');

//html = html.replace(/<\/code><\/pre>/g,'</pre>');

//require("console").log(html);
//require("console").log(outpath);

fs.writeFileSync('html/'+outpath, html, 'utf8');

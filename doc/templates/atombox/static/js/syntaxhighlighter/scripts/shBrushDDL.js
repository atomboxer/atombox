/**
 * SyntaxHighlighter
 * http://alexgorbatchev.com/SyntaxHighlighter
 *
 * SyntaxHighlighter is donationware. If you are using it, please donate.
 * http://alexgorbatchev.com/SyntaxHighlighter/donate.html
 *
 * @version
 * 3.0.83 (July 02 2010)
 * 
 * @copyright
 * Copyright (C) 2004-2010 Alex Gorbatchev.
 *
 * @license
 * Dual licensed under the MIT and GPL licenses.
 */
;(function()
{
	// CommonJS
	typeof(require) != 'undefined' ? SyntaxHighlighter = require('shCore').SyntaxHighlighter : null;

	function Brush()
	{
	    var keywords = 'all allowed are as ascending assigned audit auditcompress be begin binary bit block buffered buffersize by cfieldalign_matched2 character c_match_historic_tal code comp comp\\\-3 complex compress computational computational\\\-3 constant crtpid current date datetime day dcompress def definition delete depending descending device display duplicates edit\\\-pic end entry\\\-sequenced enum exit ext external file fieldalign_shared8 filler float fname32 for fraction heading help high\\\-number high\\\-value hour icompress index indexed interval is just justified key key\\\-sequenced keytag logical low\\\-number low\\\-value low\\\-values maxextents minute month must no not novalue noversion null occurs oddunstr of on output packed packed\\\-decimal phandle pic picture quote quotes record redefines refresh relative renames right second seq sequence serialwrites setlocalename show space spaces spi\\\-null sql sqlnull sql\\\-nullable ssid subvol system tacl talunderscore temporary through thru time times timestamp to token\\\-code token\\\-map token\\\-type transid tstamp type unsigned unstructured update upshift usage use username value varchar varying verifiedwritesversion year zero zeroes zeros key\\\-sequenced relative entry\\\-sequenced unstructured audit auditcompress block buffered buffersize code compress dcompress icompress ext maxextents oddunstr refress serialwrites verifywritesduplicates allowed update sequence ascending descending ';

	    keywords += ' ALL ALLOWED ARE AS ASCENDING ASSIGNED AUDIT AUDITCOMPRESS BE BEGIN BINARY BIT BLOCK BUFFERED BUFFERSIZE BY CFIELDALIGN_MATCHED2 CHARACTER C_MATCH_HISTORIC_TAL CODE COMP COMP\\\-3 COMPLEX COMPRESS COMPUTATIONAL COMPUTATIONAL\\\-3 CONSTANT CRTPID CURRENT DATE DATETIME DAY DCOMPRESS DEF DEFINITION DELETE DEPENDING DESCENDING DEVICE DISPLAY DUPLICATES EDIT\\\-PIC END ENTRY\\\-SEQUENCED ENUM EXIT EXT EXTERNAL FILE FIELDALIGN_SHARED8 FILLER FLOAT FNAME32 FOR FRACTION HEADING HELP HIGH\\\-NUMBER HIGH\\\-VALUE HOUR ICOMPRESS INDEX INDEXED INTERVAL IS JUST JUSTIFIED KEY KEY\\\-SEQUENCED KEYTAG LOGICAL LOW\\\-NUMBER LOW\\\-VALUE LOW\\\-VALUES MAXEXTENTS MINUTE MONTH MUST NO NOT NOVALUE NOVERSION NULL OCCURS ODDUNSTR OF ON OUTPUT PACKED PACKED\\\-DECIMAL PHANDLE PIC PICTURE QUOTE QUOTES RECORD REDEFINES REFRESH RELATIVE RENAMES RIGHT SECOND SEQ SEQUENCE SERIALWRITES SETLOCALENAME SHOW SPACE SPACES SPI\\\-NULL SQL SQLNULL SQL\\\-NULLABLE SSID SUBVOL SYSTEM TACL TALUNDERSCORE TEMPORARY THROUGH THRU TIME TIMES TIMESTAMP TO TOKEN\\\-CODE TOKEN\\\-MAP TOKEN\\\-TYPE TRANSID TSTAMP TYPE UNSIGNED UNSTRUCTURED UPDATE UPSHIFT USAGE USE USERNAME VALUE VARCHAR VARYING VERIFIEDWRITESVERSION YEAR ZERO ZEROES ZEROS KEY\\\-SEQUENCED RELATIVE ENTRY\\\-SEQUENCED UNSTRUCTURED AUDIT AUDITCOMPRESS BLOCK BUFFERED BUFFERSIZE CODE COMPRESS DCOMPRESS ICOMPRESS EXT MAXEXTENTS ODDUNSTR REFRESS SERIALWRITES VERIFYWRITESDUPLICATES ALLOWED UPDATE SEQUENCE ASCENDING DESCENDING ';

            keywords += " ASCII BCD CHARSET CHOICE DEFAULT DELIMITER DLPAD EBCDIC INCLUSIVE ISOBITMAP REGEX RPAD VALUES VLENGTH LEFT LPAD"


		var r = SyntaxHighlighter.regexLib;
		
		this.regexList = [
			{ regex: r.multiLineDoubleQuotedString,					css: 'string' },			// double quoted strings
			{ regex: r.multiLineSingleQuotedString,					css: 'string' },			// single quoted strings
			{ regex: /--.*/gm,   						        css: 'comments' },			// one line comments
			{ regex: /\![^\!]*\!/gm,							css: 'comments' },		
			{ regex: new RegExp(this.getKeywords(keywords), 'gm'),	css: 'keyword' } 		                        // keywords
			];
	
		this.forHtmlScript(r.scriptScriptTags);
	};

	Brush.prototype	= new SyntaxHighlighter.Highlighter();
	Brush.aliases	= ['ddl'];

	SyntaxHighlighter.brushes.ddl = Brush;

	// CommonJS
	typeof(exports) != 'undefined' ? exports.Brush = Brush : null;
})();/**
 * SyntaxHighlighter
 * http://alexgorbatchev.com/SyntaxHighlighter
 *
 * SyntaxHighlighter is donationware. If you are using it, please donate.
 * http://alexgorbatchev.com/SyntaxHighlighter/donate.html
 *
 * @version
 * 3.0.83 (July 02 2010)
 * 
 * @copyright
 * Copyright (C) 2004-2010 Alex Gorbatchev.
 *
 * @license
 * Dual licensed under the MIT and GPL licenses.
 */
;(function()
{
	// CommonJS
	typeof(require) != 'undefined' ? SyntaxHighlighter = require('shCore').SyntaxHighlighter : null;

	function Brush()
	{
	    var keywords = 'all allowed are as ascending assigned audit auditcompress be begin binary bit block buffered buffersize by cfieldalign_matched2 character c_match_historic_tal code comp comp\\\-3 complex compress computational computational\\\-3 constant crtpid current date datetime day dcompress def definition delete depending descending device display duplicates edit\\\-pic end entry\\\-sequenced enum exit ext external file fieldalign_shared8 filler float fname32 for fraction heading help high\\\-number high\\\-value hour icompress index indexed interval is just justified key key\\\-sequenced keytag logical low\\\-number low\\\-value low\\\-values maxextents minute month must no not novalue noversion null occurs oddunstr of on output packed packed\\\-decimal phandle pic picture quote quotes record redefines refresh relative renames right second seq sequence serialwrites setlocalename show space spaces spi\\\-null sql sqlnull sql\\\-nullable ssid subvol system tacl talunderscore temporary through thru time times timestamp to token\\\-code token\\\-map token\\\-type transid tstamp type unsigned unstructured update upshift usage use username value varchar varying verifiedwritesversion year zero zeroes zeros key\\\-sequenced relative entry\\\-sequenced unstructured audit auditcompress block buffered buffersize code compress dcompress icompress ext maxextents oddunstr refress serialwrites verifywritesduplicates allowed update sequence ascending descending ';

	    keywords += ' ALL ALLOWED ARE AS ASCENDING ASSIGNED AUDIT AUDITCOMPRESS BE BEGIN BINARY BIT BLOCK BUFFERED BUFFERSIZE BY CFIELDALIGN_MATCHED2 CHARACTER C_MATCH_HISTORIC_TAL CODE COMP COMP\\\-3 COMPLEX COMPRESS COMPUTATIONAL COMPUTATIONAL\\\-3 CONSTANT CRTPID CURRENT DATE DATETIME DAY DCOMPRESS DEF DEFINITION DELETE DEPENDING DESCENDING DEVICE DISPLAY DUPLICATES EDIT\\\-PIC END ENTRY\\\-SEQUENCED ENUM EXIT EXT EXTERNAL FILE FIELDALIGN_SHARED8 FILLER FLOAT FNAME32 FOR FRACTION HEADING HELP HIGH\\\-NUMBER HIGH\\\-VALUE HOUR ICOMPRESS INDEX INDEXED INTERVAL IS JUST JUSTIFIED KEY KEY\\\-SEQUENCED KEYTAG LOGICAL LOW\\\-NUMBER LOW\\\-VALUE LOW\\\-VALUES MAXEXTENTS MINUTE MONTH MUST NO NOT NOVALUE NOVERSION NULL OCCURS ODDUNSTR OF ON OUTPUT PACKED PACKED\\\-DECIMAL PHANDLE PIC PICTURE QUOTE QUOTES RECORD REDEFINES REFRESH RELATIVE RENAMES RIGHT SECOND SEQ SEQUENCE SERIALWRITES SETLOCALENAME SHOW SPACE SPACES SPI\\\-NULL SQL SQLNULL SQL\\\-NULLABLE SSID SUBVOL SYSTEM TACL TALUNDERSCORE TEMPORARY THROUGH THRU TIME TIMES TIMESTAMP TO TOKEN\\\-CODE TOKEN\\\-MAP TOKEN\\\-TYPE TRANSID TSTAMP TYPE UNSIGNED UNSTRUCTURED UPDATE UPSHIFT USAGE USE USERNAME VALUE VARCHAR VARYING VERIFIEDWRITESVERSION YEAR ZERO ZEROES ZEROS KEY\\\-SEQUENCED RELATIVE ENTRY\\\-SEQUENCED UNSTRUCTURED AUDIT AUDITCOMPRESS BLOCK BUFFERED BUFFERSIZE CODE COMPRESS DCOMPRESS ICOMPRESS EXT MAXEXTENTS ODDUNSTR REFRESS SERIALWRITES VERIFYWRITESDUPLICATES ALLOWED UPDATE SEQUENCE ASCENDING DESCENDING ';

            keywords += " ASCII BCD CHARSET CHOICE DEFAULT DELIMITER DLPAD EBCDIC INCLUSIVE ISOBITMAP REGEX RPAD VALUES VLENGTH LEFT LPAD"


		var r = SyntaxHighlighter.regexLib;
		
		this.regexList = [
			{ regex: r.multiLineDoubleQuotedString,					css: 'string' },			// double quoted strings
			{ regex: r.multiLineSingleQuotedString,					css: 'string' },			// single quoted strings
			{ regex: /--.*/gm,   						        css: 'comments' },			// one line comments
			{ regex: /\![^\!]*\!/gm,							css: 'comments' },		
			{ regex: new RegExp(this.getKeywords(keywords), 'gm'),	css: 'keyword' } 		                        // keywords
			];
	
		this.forHtmlScript(r.scriptScriptTags);
	};

	Brush.prototype	= new SyntaxHighlighter.Highlighter();
	Brush.aliases	= ['ddl'];

	SyntaxHighlighter.brushes.ddl = Brush;

	// CommonJS
	typeof(exports) != 'undefined' ? exports.Brush = Brush : null;
})();
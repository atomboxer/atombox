
var system  = require("system");
var console = require("console");

//Load the ctree/ace module
var Ctree = require("ctree/ace").Ctree;

//Load some arbitrary Atom/Box model
var Interface_Manager = require("mdbcsv", "mdbcsv").Interface_Manager;
var im = new Interface_Manager();
im.setByteOrder(LITTLE_ENDIAN);
var c = new Ctree();
//Connect to the database
console.writeln("connect");
c.connect("FAIRCOMS","ADMIN","ADMIN");
console.writeln("connected");

//Open the file
var stream = c.open("ifmgrd.dat");


while (!stream.atEnd()) {
    var row = stream.read();
    im.pack(row);
    console.writeln(im.intf_nam.value);
}

// c.beginTransaction();
// //write a record
// try {
//     stream.write(im.unpack()); 
// } catch(e) {

//     //position
//     stream.setKey(im.intf_nam.bytes);

//     //reread the record
//     stream.read();

//     //delete it
//     stream.write(0);
// }

// //try again
// stream.write(im.unpack()); 
//c.endTransaction()

system.exit(0);

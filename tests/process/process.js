var console = require("console");
var Process = require("process").Process;
var system  = require("system");
var fs      = require("fs");

var Q = require("q").Q;

// returns a promise with the result read from a file
// when command finishes
function executeCommand(cmd, params) 
{
    var deferred = Q.defer();
    process = new Process({'out':'deleteme'});

    process.error.connect(function(err) {
        //fs.remove('deleteme');
        deferred.reject(new Error(err));
    });

    process.finished.connect(function() {
        try {
        console.writeln("slot finished");
        // try {
        //     var stream = fs.open('deleteme',"r");
        //     var lines = stream.readLines().join("\n");
        //     stream.raw.close();
        //     fs.remove('deleteme');
        // } catch(e) {
        //     console.writeln("reject!"+e);
        //     defer.reject("err");
        // }
        delete process;
        deferred.resolve("hello");

        } catch(e) {
            console.writeln("wtf:"+e);
        }
    });
    
    process.started.connect(function() {
        console.writeln("process started");
    });

    if (params == undefined)
        params = [];
    
    process.start(cmd, params);

    return deferred.promise;
}

function go() {
    var p = executeCommand("cmd",["/c dir"]).then(function(d) {
        console.writeln("OK");
        //system.exit(0);
    }, function(e) {
        console.writeln("NOK"+e);
        //system.exit(0);
    }).done();
    delete p;
}

for (var i=0;i<1000;i++) {
    go();
}






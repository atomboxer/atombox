var assert = require("assert");
var fs = require("fs");

var system = require("system");

exports.testStreams = function()
{
    assert.ok(system.env != undefined);
    assert.ok(system.env.length>0);
    assert.ok(system.args != undefined);
    assert.ok(system.args.length>0);

    system.stdout.raw.write("To the stdout".toByteArray());
    system.stdout.writeLine("\nTo the stderr");
    //system.stdin.readLine();
    //system.exit(-1);
}

if (require.main == module.id)
    require("test").run(exports);


var assert = require("assert");
var fs = require("fs");
var console = require("console");

var io = require("io");
require("binary");

var toType = function(obj) {
    return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1]
}

exports.testIsInstance = function()
{
    var f = open("xxx","wb");
    assert.equal(f instanceof Object,true);
    assert.equal(f instanceof Stream,true,"f instanceof Stream");


    var s = new Stream(f);
    //assert.equal(s instanceof Object);
    assert.equal(s instanceof Stream,true,"s instanceof Stream");

    var ff = open("deletemepls","wb");

    var net = require("net");
    var t = new net.TcpSocket();
    assert.equal(t instanceof Object,true);
    assert.equal(t instanceof net.TcpSocket,true,"t instanceof TcpSocket");
    toggled = false;

    f.aboutToClose.connect(f, function() {
        toggled = true;
    });

    s.bytesWritten(10);
    s.close();
    assert.equal(toggled,true);
}

exports.testBinary = function()
{
    if (fs.exists("delbinar")) {
        fs.move("delbinar","delold");
        assert.ok(fs.exists("delold"),"file should exist");
        fs.remove("delold");
        assert.ok(!fs.exists("delold"),"file should not exist");
    }

    assert.ok(!fs.exists("delbinar"),"file should not exist");

    //
    // Open it in write only
    //
    var st = open("delbinar","wb");

    var string = "48 65 6c 6c 6f 20 41 54 4f 4d 42 4f 58 21";

    st.write(string.toByteArray("hex"));        //printable
    st.write("01 02 03 AA".toByteArray("hex")); //not printable

    assert.ok(st instanceof Stream,"not instance of Stream");
    assert.equal(st.position,18, "Position is not 18");

    assert.ok(fs.isWritable("delbinar"));
    assert.ok(fs.isReadable("delbinar"));
    assert.equal(fs.isLink("delbinar"),false);
    assert.equal(fs.isDirectory("delbinar"),false);

    assert.equal(st.isClosed(),false);
    assert.equal(st.isReadable(),false);
    assert.equal(st.isWritable(),true);
    assert.equal(st.isSequential(),false);
    st.close();
    assert.equal(st.isClosed(),true);
    assert.equal(st.isReadable(),false);
    assert.equal(st.isWritable(),false);
}

exports.testBinaryRead = function()
{
    //
    // Open it again in readonly
    //
    st = open("delbinar",{binary:true, read:true});
    assert.ok(st instanceof Stream,"not instance of Stream");
    assert.equal(st.isWritable(),false);
    var ba = new ByteArray(100);
    var num_bytes = st.readInto(ba,5,10);
    assert.equal(num_bytes,5);
    assert.equal(ba.slice(0,5).decodeToString("hex"),"0000000000");
    assert.equal(ba.slice(5,10).decodeToString(),"Hello");

    var bs = st.read();
    assert.equal(bs.length,13);
    assert.equal(bs.slice(9,13).toByteArray()[0],1);
    assert.equal(bs.slice(9,13).toByteArray()[1],2);
    assert.equal(bs.slice(12,13).decodeToString("hex"),"aa");

    st.close();
}

exports.testBinaryWrite = function()
{
    //
    // Open it again in read/write/append
    //
    var st = open("delbinar",{binary:true, read:true, write:true, append:true});
    assert.ok(st instanceof Stream, "not instance of Stream");

    assert.equal(st.isWritable(),true);
    assert.equal(st.isReadable(),true);
    assert.equal(st.position,18); //at the end

    st.write(new ByteArray("bb cc dd","hex"));
    st.write(new ByteString("ee ff 00","hex"));
    assert.equal(st.position,18+6);
    st.close();

    //
    //   Open it again in write only
    //
    st = open("delbinar",{binary:true, write:true,read:true});
    assert.ok(st instanceof Stream, "not instance of Stream");
    assert.equal(st.position,0);
    st.position = 10;
    st.skip(5);
    assert.equal(st.position,15);

    st.position = 10;
    var ba = new ByteArray(4);
    var num_bytes = st.readInto(ba);
    assert.equal(ba.decodeToString("utf-8"),"BOX!");

    st.write(new ByteArray("OK?"));
    var bs = st.read();
    assert.equal(bs.length,7);

    st.close();
}

exports.testText = function()
{
    if (fs.exists("deltext")) {
        fs.move("deltext","delold");
        assert.ok(fs.exists("delold"),"file should exist");
        fs.remove("delold");
        assert.ok(!fs.exists("delold"),"file should not exist");
    }

    assert.ok(!fs.exists("deltext"),"file should not exist");

    //
    // Open it in write only
    //
    var textst = open("deltext",{binary:false, write:true, read:true});

    textst.writeLine("Ana are mere");             //printable
    //ts.write("01 02 03 AA".toByteArray("hex")); //not printable

    assert.ok(textst instanceof TextStream,"not instance of TextStream");

    textst.raw.position = 14;
    assert.equal(textst.raw.position,14,"Position is not 14");

    assert.equal(textst.raw.isClosed(),false);
    assert.equal(textst.raw.isReadable(),true);
    assert.equal(textst.raw.isWritable(),true);
    assert.equal(textst.raw.isSequential(),false);
    textst.raw.close();
    assert.equal(textst.raw.isClosed(),true);
}

exports.testTextWrite = function()
{
    //
    // Open it in write only
    //
    var textst = open("deltext",{binary:false,read:true, write:true, append:true});
    textst.writeLine("SECOND2SECOND2SECOND2SECOND2SECOND2");//printable
    textst.writeLine("THIRD3THIRD3THIRD3");//printable
    textst.raw.close();
}

exports.testTextRead = function()
{
    var textst = open("deltext",{binary:false,read:true, write:true});
    assert.equal(textst.readLine(),"Ana are mere");
  
    assert.equal(textst.readLines().length,2);
    textst.raw.write(new ByteString("One line to be read after\n"));
    textst.raw.close();

    var textst = open("deltext",{binary:false,read:true, write:true});
    textst.raw.position = 5;
    var bs = textst.raw.read();
    assert.ok(bs.length>0);
    textst.raw.close();
}


if (require.main == module.id)
    require("test").run(exports);


var assert = require("assert");
var fs = require("fs");

require("io");
require("binary");

var ens_filename = "$CARD.PBZ2DATA.ptdd1";

exports.testEnscribeOpen = function()
{
    var st = open(ens_filename, {read:true, write:false, update:false});
    assert.ok(st instanceof Stream,"not instance of Stream");

    var done = false;
    var cnt =0;
    while(!done && cnt < 5) {
        var bytes = st.read(); //returns ByteString
        cnt++;
        if ( bytes.length == 0 ) {
            done = true;
        }
    }
    assert.equal(st.position!="",true);
    assert.equal(st.isSequential(),false);
    assert.ok(st.length>0);
    st.close();
    assert.equal(st.isClosed(),true);
}

exports.testEnscribeWrite = function()
{

    var st = open(ens_filename, {read:true, write:true, update:false});
    assert.ok(st instanceof Stream,"not instance of Stream");

    var bytes = st.read();

    assert.equal(bytes.length, 3514);
    var new_record = "DELETEM1        ".toByteString().concat(new ByteArray(3514-16));
    assert.equal(new_record.length,3514);

    //write a record
    try {
        st.write(new_record);
    } catch (e) {
        assert.ok(false,"exeption thrown durring write:"+e);
    }

    //write again
    try {
        st.write(new_record);
    } catch (e) {
        assert.ok(true);
    }
    st.close();
}

exports.testEnscribeUpdate = function()
{
    var st = open(ens_filename, {write:true, read:true, update:true});
    st.position = "DELETEM1        ".toByteString();
    var bytes = st.read(10000).toByteArray();
    bytes[20] = 0x31;
    try {
        st.write(bytes);
     } catch(e) {
        assert.ok(false,"Unexpected exception thrown durring update:"+e);
    }
    st.close();

    // open it again and check the update
    var st = open(ens_filename, {write:false, read:true});
    st.position = "DELETEM1        ".toByteString();
    var bytes = st.read(10000).toByteArray();
    assert.equal(bytes[20],0x31);
    st.close();
}

exports.testEnscribeDelete = function()
{
    var st = open(ens_filename, {write:true, update:false});
    delete it
    try {
        st.setKey("DELETEM1        ".toByteString());
        st.write(0);
    } catch (e) {
        assert.ok(false,"Unexpected exception thrown durring delete:"+e);
    }
    st.close();
    assert.equal(st.isClosed(),true);
}


if (require.main == module.id)
    require("test").run(exports);





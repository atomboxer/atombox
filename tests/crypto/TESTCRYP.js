var assert = require("assert");
var fs = require("fs");
var console = require("console");

var crypto = require("crypto");
var cc = require("crypto/cards");

exports.testCiphersAES_EBC = function()
{
    var key = "1234567890ABCDEFFEDCBA0987654321".toByteArray("hex");

    var cipher = crypto.createCipher("aes-128-ecb",key);
    var enc = cipher.update("hello world!  .0".toByteArray());
    enc = enc.concat(cipher.final());

    assert.equal(enc.decodeToString("hex"),
                 "665ad70f51507442e49382e52cab04644357b9f93067ee3a233395339342d7c6");

    var decipher = crypto.createDecipher("aes-128-ecb",key);
    var dec = decipher.update(enc);
    dec = dec.concat(decipher.final());

    assert.equal(dec.decodeToString("ascii"), "hello world!  .0");
}

exports.testCiphersDES_ECB = function()
{
    var key = "1234567890ABCDEFFEDCBA0987654321".toByteArray("hex");

    var cipher = crypto.createCipher("des-ede",key);
    var enc = cipher.update("hello world!  .".toByteArray());
    enc = enc.concat(cipher.final());

    assert.equal(enc.decodeToString("hex"), "54857a2cf763eb53d3b8ff1e1a8e611e");

    var decipher = crypto.createDecipher("des-ede",key);
    var dec = decipher.update(enc);
    dec = dec.concat(decipher.final());

    assert.equal(dec.decodeToString("ascii"), "hello world!  .");
}

exports.testCiphers3DES_CBC_KCV = function()
{
    var key = "1234567890ABCDEFFEDCBA09876543211234567890FEDCBA".toByteArray("hex");
    var iv  = "0000000000000000".toByteArray("hex");

    var cipher = crypto.createCipher("des-ede3-cbc",key,iv);
    //
    //  this is KCV
    //
    var enc = cipher.update("0000000000000000".toByteArray("hex"));
    enc = enc.concat(cipher.final());
    assert.equal(enc.decodeToString("hex"), "f7bac812ab592250e8b8224f3de8d40d");
}

exports.testCiphers3DES_ECB_KCV = function()
{
    var key = "1234567890ABCDEFFEDCBA09876543211234567890FEDCBA".toByteArray("hex");

    var cipher = crypto.createCipher("des-ede3",key);
    //
    //  this is KCV
    //
    var enc = cipher.update("0000000000000000".toByteArray("hex"));
    assert.equal(enc.decodeToString("hex"), "f7bac812ab592250");
}

exports.testHashes = function()
{
    var sha_hash = crypto.createHash("sha1");
    sha_hash.update("Hello".toByteArray());
    var dig = sha_hash.digest();

    assert.equal(dig.decodeToString("hex"),"f7ff9e8b7bb2e09b70935a5d785e0cc5d9d0abf0");

    var sha256_hash = crypto.createHash("sha256");
    sha256_hash.update("Hello".toByteArray());
    var dig256 = sha256_hash.digest();

    assert.equal(dig256.decodeToString("hex"),"185f8db32271fe25f561a6fc938b2e264306ec304eda518007d1764826381969");

    var md5_hash = crypto.createHash("md5");
    md5_hash.update("Hello".toByteArray());
    var digmd5 = md5_hash.digest();
    assert.equal(digmd5.decodeToString("hex"),"8b1a9953c4611296a827abf8c47804d7");
}

exports.testKeyParity = function()
{
    var ba = "AB4968B6D9E50E5E2C3175A80DAB89F7".toByteArray("hex");
    assert.ok(crypto.checkBlockParity(ba),true);

    var triple = crypto.generateTripleKey();
    assert.equal(triple.length, 24);
    assert.ok(crypto.checkBlockParity(triple));
}

exports.testCVV = function()
{
    var cvv = cc.generateCVV("1234567890ABCDEF".toByteString("hex"),
                             "FEDCBA0987654321".toByteString("hex"),
                             "1234567890123456789",
                             "1111",
                             "201" );
    assert.equal(cvv,"237");
}

exports.testiCVV = function()
{
    var cvv = cc.generateCVV("1234567890ABCDEF".toByteString("hex"),
                             "FEDCBA0987654321".toByteString("hex"),
                             "1234567890123456789",
                             "1111",
                             "999" );
    assert.equal(cvv,"165");
}

exports.testVisaPVV = function()
{
    var pvv = cc.generateVisaPVV("1234567890ABCDEF".toByteString("hex"),
                                 "FEDCBA0987654321".toByteString("hex"),
                                 "1234111111111111111",
                                 "6",
                                 "1234");
    assert.equal(pvv, "4637");
}

exports.testIBMOffset = function()
{

    var offset = cc.generateIbmOffset  ("1234567890ABCDEFFEDCBA0987654321".toByteString("hex"), //key
                                        "3344",                                                 //pin
                                        "1234567891111111111");                                 //account
    assert.equal(offset,"2839");

    var pin = cc.genPinFromIbmOffset( "1234567890ABCDEFFEDCBA0987654321".toByteString("hex"), //key
                                      offset,                 //offset
                                      "1234567891111111111"); //account
    assert.equal(pin,"3344");
}

exports.testPinBlocks = function()
{
    var pb="";
    var pin="";

    //AnsiX98
    pb = cc.generatePinBlock("5D40A4621A8AC4ABF46E5BB3CE98CDB0C15E4679083883B5".toByteArray("hex"), //key triple
                                 "AnsiX98",         //algo
                                 "4433",            //PIN
                                 "F",               //PAD (not used)
                                 "1234567891111111111");  //account

    assert.equal(pb.decodeToString("hex"), "C8CECEF0FE741FB9".toLowerCase());

    pin = cc.getPinFromPinBlock("5D40A4621A8AC4ABF46E5BB3CE98CDB0C15E4679083883B5".toByteArray("hex"), //key triple
                                    "AnsiX98",         //algo
                                    pb,                //PINBlock
                                    "F",               //PAD (not used)
                                    "1234567891111111111");  //account
    assert.equal(pin,"4433");

    //ISO_1
    pb = cc.generatePinBlock("5D40A4621A8AC4ABF46E5BB3CE98CDB0C15E4679083883B5".toByteArray("hex"), //key triple
                                 "ISO_1",           //algo
                                 "4433",            //PIN
                                 "F",               //PAD (not used)
                                 "1234567891111111111");  //account

    pin = cc.getPinFromPinBlock("5D40A4621A8AC4ABF46E5BB3CE98CDB0C15E4679083883B5".toByteArray("hex"), //key triple
                                    "ISO_1",           //algo
                                    pb,                //PINBlock
                                    "F",               //PAD (not used)
                                    "1234567891111111111");  //account
    assert.equal(pin,"4433");

    //Diebold
    pb = cc.generatePinBlock("5D40A4621A8AC4ABF46E5BB3CE98CDB0C15E4679083883B5".toByteArray("hex"), //key triple
                                 "Diebold",         //algo
                                 "4433",            //PIN
                                 "F",               //PAD (not used)
                                 "1234567891111111111");  //account
    assert.equal(pb.decodeToString("hex"), "CAC0E3065DC17CBC".toLowerCase());

    pin = cc.getPinFromPinBlock("5D40A4621A8AC4ABF46E5BB3CE98CDB0C15E4679083883B5".toByteArray("hex"), //key triple
                                    "Diebold",         //algo
                                    pb,                //PINBlock
                                    "F",               //PAD (not used)
                                    "1234567891111111111");  //account
    assert.equal(pin,"4433");

    //IBM_3624
    pb = cc.generatePinBlock("5D40A4621A8AC4ABF46E5BB3CE98CDB0C15E4679083883B5".toByteArray("hex"), //key triple
                                 "IBM_3624",         //algo
                                 "4433",             //PIN
                                 "F",                //PAD (not used)
                                 "1234567891111111111");  //account
    assert.equal(pb.decodeToString("hex"), "CAC0E3065DC17CBC".toLowerCase());

    pin = cc.getPinFromPinBlock("5D40A4621A8AC4ABF46E5BB3CE98CDB0C15E4679083883B5".toByteArray("hex"), //key triple
                                    "IBM_3624",         //algo
                                    pb,                 //PINBlock
                                    "F",                //PAD (not used)
                                    "1234567891111111111");  //account
    assert.equal(pin,"4433");

    var ba = crypto.generateTripleKey();
    var pba = crypto.fixBlockParity("1234567890ABCDEF".toByteArray("hex"));
    assert.equal(pba.decodeToString("hex"),"1334577991abcdef");
}

exports.testVisa96_ARQC_ARPC  = function() {

    var arqc = cc.generateVisa96Arqc( "01010101010101010202020202020202".toByteString("hex"), //mdk
                                          "4220000000000030", //pan
                                          "01", //pan seq number
                                          "000000000100000000000000084080000100000826060412009BADBCAB5c0000FF03A0B000" //cdol
                                        );

    assert.equal(arqc.decodeToString("hex"), "22abca55a65c3335");

    var arpc = cc.generateVisa96Arpc( "01010101010101010202020202020202".toByteString("hex"), //mdk,
                                      "4220000000000030", //pan
                                      "01", //pan seq number
                                      arqc,
                                      "0003"
                                    ); //arc

    assert.equal(arpc.decodeToString("hex"), "fb335eebea22ed6e");
}

if (require.main == module.id)
    require("test").run(exports);

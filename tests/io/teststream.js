var assert  = require("assert");
var system  = require("system");
var console = require("console");
var Binary  = require("binary").Binary;
var Stream  = require("io").Stream;

exports.testStreamConstructor = function() {
    var ba = new ByteArray("11111");
    var ba_stream = new Stream(ba);
    var ba_for_read = new Stream(ba);

    var triggered = false;
    ba_stream.bytesWritten.connect(ba_stream, function(b) {
        assert.equal(b,12);
        console.assert(ba,"asdasdasd".toByteArray());
        triggered = true;
        assert.equal(ba_for_read.read(5).decodeToString(),5);
    });

    ba_stream.write("asdasdasdasd".toByteArray());

    var ba_alt = new ByteArray();
    var ba_alt_stream = new Stream(ba_alt);

    ba_alt_stream.readyRead.connect(function() {
        assert.equal(ba_alt.decodeToString(),"bbbb");
        assert.equal(triggered,true);
    });
    
    ba_stream.position = 2;
    assert.equal(ba_stream.read(2).decodeToString(), "da");
    assert.equal(ba_stream.position,4);
    ba_alt_stream.write("bbbb".toByteArray());
};


if (require.main == module.id) {
    require('test').run(exports);
}

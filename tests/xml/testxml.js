var console = require("console");
var sax = require("xml/sax.js"),
strict = true, // set to false for html-mode
parser = sax.parser(strict);

parser.onerror = function (e) {
    // an error happened.
};
parser.ontext = function (t) {
    // got some text.  t is the string of text.

    console.write("onText");
    console.dir(t);
};
parser.onopentag = function (node) {
    // opened a tag.  node has "name" and "attributes"
    console.write("onopentag");
    console.dir(node);
};
parser.onattribute = function (attr) {
    // an attribute.  attr has "name" and "value"
    console.dir(attr.name);
};
parser.onend = function () {
    // parser stream is done, and ready to have more stuff written to it.
    console.write("end");
};

var xml = '<xml>Hello, <who name="world" some="other">world<other></other></who>!</xml>';
parser.write(xml).close();

var builder = require('xml/xmlbuilder');
var root = builder.create('squares');
root.com('f(x) = x^2');
for(var i = 1; i <= 5; i++)
{
    var item = root.ele('data');
    item.att('x', i);
    item.att('y', i * i);
}
root.end({pretty:true});

var parseString = require('xml/xml2js').parseString;
var obj = undefined;
parseString(xml, function (err, result) {
    obj = result;
});

var Builder = new require('xml/xml2js').Builder;
var builder = new Builder();

var xml = builder.buildObject(obj);
console.write(xml);


var console = require("console");
var system  = require("system");
var sql     = require("sql");

var db1 = new sql.Database("db2");
db1.setDatabaseName("testdb1");
db1.open();

if (!db1.isOpen()) {
    console.writeln("unable to open database"+db1.error());
    system.exit(0);
}

var q = new sql.Query(db1);

q.exec("DROP TABLE Persons;");

try {
    q.prepare('CREATE TABLE Persons(P_Id int,LastName varchar(255),FirstName varchar(255));');
    q.exec();

    q.exec('INSERT INTO Persons VALUES(0, "BEJAN", "MARIUS")');
    q.exec('INSERT INTO Persons VALUES(1, "DOE", "JOHN")');
} catch (e) {
    console.error("error +"+q.error());
}

var qs = new sql.Query(db1);
try {
    qs.exec("SELECT * from Persons");
} catch (e) {
    console.error("error +"+qs.error());
}

while(qs.next()) {
    console.write("last name:"+qs.value(1)+"\n");
}

system.exit(0);

//outputs: 
//
//last name:BEJAN
//last name:DOE



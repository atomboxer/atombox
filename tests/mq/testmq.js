
var system  = require("system");
var console = require("console");

var MQManager      = require("mq/websphere").MQManager;

var mm = new MQManager();

//Connect to the MQ Manager
mm.connect("EP92.MANAGER");

//Open a queue
var q = mm.openQueue("EP92.ABI",{input:true, output:true});

//Connect the requestReady signal and deal with the request/response objects
q.requestReady.connect( function(rq, rs) {
	console.writeln("requestReady:");
	console.writeln(rq.data().decodeToString());

	rs.write("Hello back".toByteArray());
	rs.end();
});

//Connect the datagramReady signal (no response)
q.datagramReady.connect( function(rq) {
	console.writeln("datagramReady:");
	console.writeln(rq.data());
});

//Connect the error signal
q.error.connect( function(num) {
	console.writeln("error "+num);
});

//Send a request
var r = q.createRequest();
r.ReplyToQ = "EP92.VISA";
r.ReplyToQMgr = "EP92.MANAGER";
r.end("This is a requst".toByteArray());

//Send a datagram
var d = q.createDatagram();
d.end("I am sending something back".toByteArray());


var assert  = require("assert");
var console = require("console");
var net     = require("net");
var ssl     = require("ssl");
var system  = require("system");

exports.testSslServer = function()
{
    var sslServer = new ssl.SslServer();
    sslServer.listen(8150, undefined);

    assert.equal(sslServer.isListening(), true);

    sslServer.newConnection.connect(function() {
        print("!!!!! new connection !!!!");
        var sock = sslServer.nextPendingConnection();
       
        sock["disconnected()"].connect(function() {
            print("*** One client is bye bye");
        });
    });
}

exports.testSocket = function()
{
    // var sslSocket = new ssl.SslSocket();

    // sslSocket.connected.connect(function() {
    //     print("connected");
    // });

    // sslSocket.error.connect(this, function() {
    //     //print("error"+this.errorString());
    // });

    // sslSocket.encrypted.connect(function() {
    //     print("encrypted!");
    // });

    // sslSocket.connectToHost("10.203.9.221", 22);
}

if (require.main == module.id)
    require("test").run(exports);

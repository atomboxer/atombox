
var assert  = require("assert");
var console = require("console");
var net     = require("net");
var system  = require("system");

exports.testSocket = function()
{
    var server = new net.TcpServer();
    server.listen(8150,undefined);

    assert.equal(server.isListening(), true);

    var num = 0;

    server.newConnection.connect(function() {
        print("!!!!! new connection !!!!");
        var sock = server.nextPendingConnection();
        sock["disconnected()"].connect(function() {
            print("*** One client is bye bye");
        });

        sock.readyRead.connect(function() {
            var what = sock.read();
            print(":::server:::"+what.decodeToString());
            sock.write(("hel:".toByteArray()).concat(what));
            sock.disconnectFromHost();
        });
    });

    var first = new TcpSocket();
    first.disconnected.connect(function() {
        print("*** First client... bye bye");
        //system.exit(0);
    });
    first.connected.connect(function() {
        print("*** First client... connected signal");
        //system.exit(0);
    });

    first.connectToHost("127.0.0.1", 8150);

    var alts = new TcpSocket();
    alts.disconnected.connect(function() {
        print("*** Second client... bye bye");
    });
    alts.connectToHost("127.0.0.1", 8150);

    var alt1 = new TcpSocket();
    alt1.disconnected.connect(function() {
        print("*** Third Client... bye bye");
    });

    alt1.connectToHost("127.0.0.1", 8150);
    alts.disconnectFromHost();
    alt1.disconnectFromHost();
    first.disconnectFromHost();
}

exports.testReadWrite = function()
{
    var sock = new net.TcpSocket();
    sock.connected.connect(function() {
        print(":::testReadWrite::sock connected");
        sock.write("Hello from client".toByteArray());
    });

    sock.disconnected.connect(function() {
        print(":::testReadWrite::sock disconnected");
    });
    sock.readyRead.connect(function() {
        print(":::testReadWrite::readyRead::"+sock.read().decodeToString());
    });

    sock.error.connect(sock, function() {
        print(":::testReadWrite::socketError wtf::"+this.errorString());
    });

    //sock.connectToHost("127.0.0.1",81);
    //sock.connectToHost("10.203.34.43",22);
    sock.connectToHost("127.0.0.1",8150);
}

if (require.main == module.id)
    require("test").run(exports);

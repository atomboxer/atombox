var assert  = require("assert");
var console = require("console");
var net     = require("net");
var system  = require("system");


var server = new net.TcpServer();
server.listen(8150,undefined);

assert.equal(server.isListening(), true);

var num = 0;

server.newConnection.connect(function() {
    print("!!!!! new connection !!!!");
    var sock = server.nextPendingConnection();
    sock["disconnected()"].connect(function() {
        print("*** One client is bye bye");
    });
    
    sock.readyRead.connect(function() {
        var what = sock.read();
        print(":::server:::"+what.decodeToString());
        sock.write(("hel:".toByteArray()).concat(what));
        sock.disconnectFromHost();
    });
});


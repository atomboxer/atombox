var system  = require("system");
var console = require("console");
var assert  = require("assert");

function checkHex(n){return/^[0-9A-Fa-f]{1,64}$/.test(n)}
function hex2Bin(n){if(!checkHex(n))return 0;return parseInt(n,16).toString(2)}

exports.testV3 = function ()
{
    var f = require("testbit.ddl","ddl");
    var o = new f.v1();

    o.pack("4210001102C04804000000000000000331323333".toByteArray("hex"));
    //assert.equal(o.unpack(o.pack(o.unpack())).decodeToString("hex"), "4210001102c048040000000000000003");

    console.dir(o);
}

if (require.main == module.id)
    require("test").run(exports);

system.exit(0);

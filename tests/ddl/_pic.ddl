

DEF comp_pictures.
    02 c_1 PIC 9999 USAGE IS COMP DEFAULT 1234.
    02 c_2 PIC 9999 USAGE         COMP DEFAULT 5678.
end.

DEF comp3_pictures.
    02 c_1 PIC 9999 USAGE IS COMP-3 DEFAULT 1234.
    02 c_2 PIC 9999 PACKED-DECIMAL DEFAULT 5678.
end.

DEF ASCII_numeric_pictures USAGE IS PACKED.
    02 signed_expl PIC S9(2) DEFAULT 1.
    02 signed_expr PIC 99S  DEFAULT %0115.
    02 signed_impr PIC S9(1)9(2) DEFAULT +888.
    02 signed_impl PIC 999S DEFAULT -777.
    02 unsign      PIC 9(3)99V99(2).
    02 kk redefines unsign PIC X(9).
END.

DEF ascii-pictures.
    02 filler PIC X (100) DEFAULT "hello" JUSTIFIED RIGHT.           
    02 kk1.
        04 alt PIC AAX (4) 9 (4) OCCURS 10 TIMES DEFAULT "hi" JUSTIFIED.       --
    02 maca PIC X(200) REDEFINES kk1.
END.


DEF binary-pictures.
    02 bin-8  BINARY 8 DEFAULT %B10000001.
    02 bin-16 TYPE BINARY 16 DEFAULT 16.
    02 bin-32 TYPE BINARY 32 DEFAULT 32.
    02 bin-64 TYPE BINARY 64 DEFAULT 64.

    02 ubin-8  TYPE BINARY 8 UNSIGNED DEFAULT %HFF.
    02 ubin-16      BINARY 16 UNSIGNED DEFAULT 16.
    02 ubin-32 TYPE BINARY 32 UNSIGNED DEFAULT 32.

    02 rbin-32      FLOAT 32 .
    02 rbin-64 TYPE FLOAT 64.
END.
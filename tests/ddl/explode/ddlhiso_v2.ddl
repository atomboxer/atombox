
def pbitmap.
    02 BP1	TYPE BIT 1.
    02 BP2	TYPE BIT 1.
    02 BP3	TYPE BIT 1.
    02 BP4	TYPE BIT 1.
    02 BP5	TYPE BIT 1.
    02 BP6	TYPE BIT 1.
    02 BP7	TYPE BIT 1.
    02 BP8	TYPE BIT 1.
    02 BP9	TYPE BIT 1.
    02 BP10	TYPE BIT 1.
    02 BP11	TYPE BIT 1.
    02 BP12	TYPE BIT 1.
    02 BP13	TYPE BIT 1.
    02 BP14	TYPE BIT 1.
    02 BP15	TYPE BIT 1.
    02 BP16	TYPE BIT 1.
    02 BP17	TYPE BIT 1.
    02 BP18	TYPE BIT 1.
    02 BP19	TYPE BIT 1.
    02 BP20	TYPE BIT 1.
    02 BP21	TYPE BIT 1.
    02 BP22	TYPE BIT 1.
    02 BP23	TYPE BIT 1.
    02 BP24	TYPE BIT 1.
    02 BP25	TYPE BIT 1.
    02 BP26	TYPE BIT 1.
    02 BP27	TYPE BIT 1.
    02 BP28	TYPE BIT 1.
    02 BP29	TYPE BIT 1.
    02 BP30	TYPE BIT 1.
    02 BP31	TYPE BIT 1.
    02 BP32	TYPE BIT 1.
    02 BP33	TYPE BIT 1.
    02 BP34	TYPE BIT 1.
    02 BP35	TYPE BIT 1.
    02 BP36	TYPE BIT 1.
    02 BP37	TYPE BIT 1.
    02 BP38	TYPE BIT 1.
    02 BP39	TYPE BIT 1.
    02 BP40	TYPE BIT 1.
    02 BP41	TYPE BIT 1.
    02 BP42	TYPE BIT 1.
    02 BP43	TYPE BIT 1.
    02 BP44	TYPE BIT 1.
    02 BP45	TYPE BIT 1.
    02 BP46	TYPE BIT 1.
    02 BP47	TYPE BIT 1.
    02 BP48	TYPE BIT 1.
    02 BP49	TYPE BIT 1.
    02 BP50	TYPE BIT 1.
    02 BP51	TYPE BIT 1.
    02 BP52	TYPE BIT 1.
    02 BP53	TYPE BIT 1.
    02 BP54	TYPE BIT 1.
    02 BP55	TYPE BIT 1.
    02 BP56	TYPE BIT 1.
    02 BP57	TYPE BIT 1.
    02 BP58	TYPE BIT 1.
    02 BP59	TYPE BIT 1.
    02 BP60	TYPE BIT 1.
    02 BP61	TYPE BIT 1.
    02 BP62	TYPE BIT 1.
    02 BP63	TYPE BIT 1.
    02 BP64	TYPE BIT 1.
end.

def sbitmap.
    02 BS1	TYPE BIT 1.
    02 BS2	TYPE BIT 1.
    02 BS3	TYPE BIT 1.
    02 BS4	TYPE BIT 1.
    02 BS5	TYPE BIT 1.
    02 BS6	TYPE BIT 1.
    02 BS7	TYPE BIT 1.
    02 BS8	TYPE BIT 1.
    02 BS9	TYPE BIT 1.
    02 BS10	TYPE BIT 1.
    02 BS11	TYPE BIT 1.
    02 BS12	TYPE BIT 1.
    02 BS13	TYPE BIT 1.
    02 BS14	TYPE BIT 1.
    02 BS15	TYPE BIT 1.
    02 BS16	TYPE BIT 1.
    02 BS17	TYPE BIT 1.
    02 BS18	TYPE BIT 1.
    02 BS19	TYPE BIT 1.
    02 BS20	TYPE BIT 1.
    02 BS21	TYPE BIT 1.
    02 BS22	TYPE BIT 1.
    02 BS23	TYPE BIT 1.
    02 BS24	TYPE BIT 1.
    02 BS25	TYPE BIT 1.
    02 BS26	TYPE BIT 1.
    02 BS27	TYPE BIT 1.
    02 BS28	TYPE BIT 1.
    02 BS29	TYPE BIT 1.
    02 BS30	TYPE BIT 1.
    02 BS31	TYPE BIT 1.
    02 BS32	TYPE BIT 1.
    02 BS33	TYPE BIT 1.
    02 BS34	TYPE BIT 1.
    02 BS35	TYPE BIT 1.
    02 BS36	TYPE BIT 1.
    02 BS37	TYPE BIT 1.
    02 BS38	TYPE BIT 1.
    02 BS39	TYPE BIT 1.
    02 BS40	TYPE BIT 1.
    02 BS41	TYPE BIT 1.
    02 BS42	TYPE BIT 1.
    02 BS43	TYPE BIT 1.
    02 BS44	TYPE BIT 1.
    02 BS45	TYPE BIT 1.
    02 BS46	TYPE BIT 1.
    02 BS47	TYPE BIT 1.
    02 BS48	TYPE BIT 1.
    02 BS49	TYPE BIT 1.
    02 BS50	TYPE BIT 1.
    02 BS51	TYPE BIT 1.
    02 BS52	TYPE BIT 1.
    02 BS53	TYPE BIT 1.
    02 BS54	TYPE BIT 1.
    02 BS55	TYPE BIT 1.
    02 BS56	TYPE BIT 1.
    02 BS57	TYPE BIT 1.
    02 BS58	TYPE BIT 1.
    02 BS59	TYPE BIT 1.
    02 BS60	TYPE BIT 1.
    02 BS61	TYPE BIT 1.
    02 BS62	TYPE BIT 1.
    02 BS63	TYPE BIT 1.
    02 BS64	TYPE BIT 1.
end.

DEF MTI_HISO87_E TYPE ENUM BEGIN.
      89 _   VALUE 0800 AS "Network Management Request".
      89 _   VALUE 0810 AS "Network Management Request Response".
      89 _   VALUE 0100 AS "Authorisation Request".
      89 _   VALUE 0110 AS "Authorisation Request Response".
      89 _   VALUE 0120 AS "Authorisation Advice Request".
      89 _   VALUE 0130 AS "Authorisation Advice Request Response".
      89 _   VALUE 0200 AS "Financial Request".
      89 _   VALUE 0210 AS "Financial Request Response".
      89 _   VALUE 0220 AS "Financial Advice Request".
      89 _   VALUE 0230 AS "Financial Advice Request Response".
      89 _   VALUE 0420 AS "Reversal Advice Request".
      89 _   VALUE 0430 AS "Reversal Advice Request Response".
END.

DEF HISO87.
  02  HDR.
      04  STRT_OF_TXT                         PIC X(3) MUST BE "ISO".
      04  PROD_ID                             TYPE *.
      04  REL_NUM                             PIC X(2) MUST BE "60".
      04  STAT                                PIC X(3).
      04  ORIGINATOR                          PIC X.
      04  RESPONDER                           PIC X.

  02  TYP                                     PIC 9(4)  DISPLAY "Message Type" 
                                  MUST BE MTI_HISO87_E, 0 THROUGH 1000 AS "Unknown".

  02  PRI_BIT_MAP PIC X(32) PACKED DISPLAY "Primary Bitmap".
  02  PRI_BIT_MAPB                         TYPE  pbitmap 
                                           DISPLAY "Primary Bitmap red" 
                                           REDEFINES PRI_BIT_MAP.

  02  SECNDRY_BIT_MAP PIC X(32) PACKED OCCURS 0 to 1 TIMES depending ON BP1 DISPLAY "Secondary Bitmap".
  02  SECNDRY_BIT_MAPB                     TYPE  sbitmap DISPLAY "Secondary Bitmap"
                                           REDEFINES SECNDRY_BIT_MAP.

  02  PAN OCCURS 0 TO 1 TIMES DEPENDING ON BP2.
      04  LEN                             PIC 9(2).
      04  DATA                            PIC 9(19) VLENGTH DEPENDING ON LEN.

  02  PROC_CDE  OCCURS 0 TO 1 TIMES DEPENDING ON BP3    DISPLAY "Processing Code".                       !DE003
      04  TRAN_CDE                        PIC X(2).
      04  FROM_ACCT_TYP                   PIC X(2).
      04  TO_ACCT_TYP                     PIC X(2).

  02  TRAN_AMT                            OCCURS 0 TO 1 TIMES DEPENDING ON BP4 PIC 9(12) DISPLAY "Transaction Amount".                    !DE004
  02  SETL_AMT                            OCCURS 0 TO 1 TIMES DEPENDING ON BP5 PIC 9(12) DISPLAY "Settlement Amount".                     !DE005
  02  BILL_AMT                            OCCURS 0 TO 1 TIMES DEPENDING ON BP6 PIC 9(12) DISPLAY "Billing Amount".                        !DE006
  02  XMIT_DAT_TIM                        OCCURS 0 TO 1 TIMES DEPENDING ON BP7 PIC 9(10) DISPLAY "Tran Date and Time".                    !DE007
  02  BILL_FEE                            OCCURS 0 TO 1 TIMES DEPENDING ON BP8 PIC 9(8)  DISPLAY "Billing Fee".                           !DE008
  02  SETL_CONV_RAT                       OCCURS 0 TO 1 TIMES DEPENDING ON BP9 PIC 9(8)  DISPLAY "Settlment Conv Rate".                   !DE009
  02  BILL_CONV_RAT                       OCCURS 0 TO 1 TIMES DEPENDING ON BP10 PIC 9(8)  DISPLAY "Billing Conv Rate".                     !DE010
  02  TRACE_NUM                           OCCURS 0 TO 1 TIMES DEPENDING ON BP11 PIC 9(6)  DISPLAY "Trace Number".                          !DE011
  02  TRAN_TIM                            OCCURS 0 TO 1 TIMES DEPENDING ON BP12 PIC 9(6)  DISPLAY "Local Time".                            !DE012
  02  TRAN_DAT                            OCCURS 0 TO 1 TIMES DEPENDING ON BP13 PIC 9(4)  DISPLAY "Local Date".                            !DE013
  02  EXP_DAT                             OCCURS 0 TO 1 TIMES DEPENDING ON BP14 PIC 9(4)  DISPLAY "Expiry Date".                           !DE014
  02  SETL_DAT                            OCCURS 0 TO 1 TIMES DEPENDING ON BP15 PIC 9(4)  DISPLAY "Settlement Date".                       !DE015
  02  CONV_DAT                            OCCURS 0 TO 1 TIMES DEPENDING ON BP16 PIC 9(4)  DISPLAY "Conversion Date".                       !DE016
  02  CAP_DAT                             OCCURS 0 TO 1 TIMES DEPENDING ON BP17 PIC 9(4)  DISPLAY "Capure Date".                           !DE017
  02  MRCHT_TYP_CDE                       OCCURS 0 TO 1 TIMES DEPENDING ON BP18 PIC 9(4)  DISPLAY "Merchant Type".                         !DE018
  02  ACQ_INST_CNTRY_CDE                  OCCURS 0 TO 1 TIMES DEPENDING ON BP19 PIC 9(3)  DISPLAY "Aquiring Inst Contry".                  !DE019
  02  PAN_EXT_CNTRY_CDE                   OCCURS 0 TO 1 TIMES DEPENDING ON BP20 PIC 9(3)  DISPLAY "Pan Ext. Country Code".                 !DE020
  02  FRWD_INST_CNTRY_CDE                 OCCURS 0 TO 1 TIMES DEPENDING ON BP21 PIC 9(3)  DISPLAY "Forwarding Inst Country".               !DE021
  02  ENTRY_MDE                           OCCURS 0 TO 1 TIMES DEPENDING ON BP22 PIC 9(3)  DISPLAY "Entry Mode".                            !DE022
  02  MBR_NUM                             OCCURS 0 TO 1 TIMES DEPENDING ON BP23 TYPE *    DISPLAY "Member Number".                         !DE023
  02  NETW_INTL_ID                        OCCURS 0 TO 1 TIMES DEPENDING ON BP24 PIC 9(3).                                                  !DE024
  02  PT_TRAN_SPCL_CDE                    OCCURS 0 TO 1 TIMES DEPENDING ON BP25 PIC 9(2).                                                  !DE025
  02  POS_PIN_CAPTURE_CDE                 OCCURS 0 TO 1 TIMES DEPENDING ON BP26 PIC 9(2).                                                  !DE026
  02  AUTH_ID_RESP_LEN                    OCCURS 0 TO 1 TIMES DEPENDING ON BP27 PIC 9.                                                     !DE027
  02  TRAN_FEE                            OCCURS 0 TO 1 TIMES DEPENDING ON BP28           DISPLAY "Transaction Fee".                       !DE028
      04  IND                             PIC X.
      04  AMT                             PIC 9(8).
  02  SETL_FEE                            OCCURS 0 TO 1 TIMES DEPENDING ON BP29          DISPLAY "Settlement Fee".                        !DE029
      04  IND                             PIC X.
      04  AMT                             PIC 9(8).
  02  TRAN_PROC_FEE                       OCCURS 0 TO 1 TIMES DEPENDING ON BP30          DISPLAY "Processing Fee".                        !DE030
      04  IND                             PIC X.
      04  AMT                             PIC 9(8).
  02  SETL_PROC_FEE OCCURS 0 TO 1 TIMES DEPENDING ON BP31.                                                                                 !DE031
      04  IND                             PIC X     DISPLAY "Settlement Proc. Fee".
      04  AMT                             PIC 9(8).
  02  ACQ_INST_ID                         OCCURS 0 TO 1 TIMES DEPENDING ON BP32          DISPLAY "Acquiring Inst ID".                     !DE032
      04  LEN                             PIC 9(2).
      04  NUM                             PIC X(11) VLENGTH DEPENDING ON LEN.
  02  FRWD_INST_ID                        OCCURS 0 TO 1 TIMES DEPENDING ON BP33          DISPLAY "Forwarding Inst. ID".                   !DE033
      04  LEN                             PIC 9(2).
      04  NUM                             PIC 9(11) VLENGTH DEPENDING ON LEN.
  02  PAN_EXTND OCCURS 0 TO 1 TIMES DEPENDING ON BP34.                                                                                     !DE034
      04  LEN                             PIC 9(2).
      04  NUM                             PIC X(28) VLENGTH DEPENDING ON LEN.
  02  TRACK2                              OCCURS 0 TO 1 TIMES DEPENDING ON BP35          DISPLAY "Track 2".                               !DE035
      04  LEN                             PIC 9(2).
      04  DATA                            PIC X(37) VLENGTH DEPENDING ON LEN.
  02  TRACK3                              OCCURS 0 TO 1 TIMES DEPENDING ON BP36          DISPLAY "Track 3".                               !DE036
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(104) VLENGTH DEPENDING ON LEN.
  02  RETRVL_REF_NUM                      OCCURS 0 TO 1 TIMES DEPENDING ON BP37          DISPLAY "RRN".                                   !DE037
      04  DATA                            PIC X(12).
      04  TLR                             REDEFINES DATA.
         06 TRAN_SEQ_NUM                  PIC X(6).
         06 DEV_SEQ_NUM                   PIC X(6).
      04  ATM                             REDEFINES DATA.
          06 SEQ_NUM                      PIC X(12).
      04  POS                             REDEFINES DATA.
          06 SEQ_NUM                      PIC X(12).
      04  TB                              REDEFINES DATA.
          06 SEQ_NUM                      PIC X(12).
  02  AUTH_ID_RESP                        OCCURS 0 TO 1 TIMES DEPENDING ON BP38 PIC X(6)  DISPLAY "Auth ID Resp".                          !DE038
  02  RESP_CDE                            OCCURS 0 TO 1 TIMES DEPENDING ON BP39 PIC X(2)  DISPLAY "Response Code".                         !DE039
  02  SERVICE_CDE                         OCCURS 0 TO 1 TIMES DEPENDING ON BP40 PIC X(3)  DISPLAY "Service Code".                          !DE040
  02  TERM_ID                             OCCURS 0 TO 1 TIMES DEPENDING ON BP41 PIC X(16) DISPLAY "Card Accptr Term ID".                   !DE041
  02  CRD_ACCPT_ID_CDE                    OCCURS 0 TO 1 TIMES DEPENDING ON BP42 PIC X(15) DISPLAY "Card Accptr ID".                        !DE042
  02  CRD_ACCPT_NAME_LOC                  OCCURS 0 TO 1 TIMES DEPENDING ON BP43          DISPLAY "Card Accptr Name/Locn". !43              !DE043
      04  TERM_OWNER                      PIC X(22).
      04  TERM_CITY                       PIC X(13).
      04  TERM_ST                         PIC X(3).
      04  TERM_CNTRY                      PIC X(2).
  02  RESP_DATA                           OCCURS 0 TO 1 TIMES DEPENDING ON BP44          DISPLAY "Response Data".                         !DE044
      04  LEN                             PIC 9(2).
      04  DATA                            PIC X(25) VLENGTH DEPENDING ON LEN.
!     04 BAL                              REDEFINES DATA.
!        06  PRESENCE_IND                 PIC X(1).
!        06  LEDG_BAL                     PIC X(12).
!        06  AVAIL_BAL                    PIC X(12).
!        06  AVAIL_CREDIT REDEFINES AVAIL_BAL  PIC X(12).
!     04 POS                              REDEFINES DATA.
!        06  RESP_DATA                    PIC X.
!        06  ADDR_VRFY_STAT               PIC X.
!        06  RESERVED                     PIC X(23).
!     04 TB                               REDEFINES DATA.
!        06  PMNT_DAT                     PIC X(8).
!        06  RESERVED                     PIC X(17).
  02  TRACK1                              OCCURS 0 TO 1 TIMES DEPENDING ON BP45          DISPLAY "Track 1".                               !DE045
      04  LEN                             PIC 9(2).
      04  DATA                            PIC X(76) VLENGTH DEPENDING ON LEN.
  02  ADD_DATA_ISO OCCURS 0 TO 1 TIMES DEPENDING ON BP46.                                                                                  !DE046
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
  02  ADD_DATA_NATL OCCURS 0 TO 1 TIMES DEPENDING ON BP47.      !47                                                                        !DE047
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
  02  ADD_DATA_PRVT                       OCCURS 0 TO 1 TIMES DEPENDING ON BP48          DISPLAY "Additional Data".                       !DE048
       04  LEN                            PIC 9(3).
       04  DATA                           PIC X(200) VLENGTH DEPENDING ON LEN.
  02  CRNCY_CDE                           OCCURS 0 TO 1 TIMES DEPENDING ON BP49 PIC 9(3)        DISPLAY "Tran Currency Code".              !DE049
  02  SETL_CRNCY                          OCCURS 0 TO 1 TIMES DEPENDING ON BP50 PIC 9(3)        DISPLAY "Settl Currency Code".             !DE050
  02  BILL_CRNCY                          OCCURS 0 TO 1 TIMES DEPENDING ON BP51 PIC 9(3)        DISPLAY "Billing Currency Code".           !DE051
  02  PIN                                 OCCURS 0 TO 1 TIMES DEPENDING ON BP52 PIC X(16)       DISPLAY "PIN Block".                       !DE052
  02  SEC_CNTRL_INFO                      OCCURS 0 TO 1 TIMES DEPENDING ON BP53 PIC 9(16)       DISPLAY "Sec Related Info".                !DE053

  02  BASE24_SEC_INFO                     REDEFINES SEC_CNTRL_INFO.
      04 KEY_TYP                          PIC X(2).
      04 KEY_DIR                          PIC X(2).
      04 PIN_CHK_DGT                      PIC X(6).
      04 MAC_CHK_DGT                      PIC X(6).

  02  ADD_AMTS                            OCCURS 0 TO 1 TIMES DEPENDING ON BP54                DISPLAY "Additional Amounts".              !DE054
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(120) VLENGTH DEPENDING ON LEN.
  02  PRI_RSRVD1_ISO OCCURS 0 TO 1 TIMES DEPENDING ON BP55.                                                                                !DE055
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
  02  PRI_RSRVD2_ISO OCCURS 0 TO 1 TIMES DEPENDING ON BP56.                                                                                !DE056
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
  02  PRI_RSRVD1_NATL OCCURS 0 TO 1 TIMES DEPENDING ON BP57.                                                                               !DE057
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
  02  PRI_RSRVD2_NATL OCCURS 0 TO 1 TIMES DEPENDING ON BP58.                                                                               !DE058
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(200) VLENGTH DEPENDING ON LEN.
      04  TLR                             REDEFINES DATA.
          06  TKN_ID                      PIC X(2).
          06  FNCL_TKN                    PIC X(198).
  02  PRI_RSRVD3_NATL OCCURS 0 TO 1 TIMES DEPENDING ON BP59.                                                                               !DE059
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
      04  TLR                             REDEFINES DATA.
          06  TKN_ID                      PIC X(2).
          06  CAFU_TKN                    PIC X(98).
  02  PRI_RSRVD1_PRVT                     OCCURS 0 TO 1 TIMES DEPENDING ON BP60              DISPLAY "Terminal Data".                     !DE060
      04 LEN                              PIC 9(3).
      04 DATA                             PIC X(100) VLENGTH DEPENDING ON LEN.
!      04 TLR                              REDEFINES DATA.
!         06  TERM_FIID                    TYPE FIID.
!         06  TERM_LN                      TYPE LN.
!         06  TLR_ID                       PIC X(8).
!         06  CRD_FIID                     TYPE FIID.
!         06  CRD_LN                       TYPE LN.
!         06  REGN_ID                      TYPE REGN.
!         06  BRCH_ID                      TYPE BRCH.
!         06  TERM_LOC                     PIC X(25).
!         06  DDA_CUR_FLG                  PIC X.
!         06  SAV_CUR_FLG                  PIC X.
!         06  CCD_CUR_FLG                  PIC X.
!         06  SPF_CUR_FLG                  PIC X.
!         06  NBF_CUR_FLG                  PIC X.
!         06  WHFF_CUR_FLG                 PIC X.
!         06  CRNCY_CDE                    PIC 9(3).
!         06  RESERVED                     PIC X(34).
!      04 ATM                              REDEFINES DATA.
!         06  TERM_FIID                    TYPE FIID.
!         06  TERM_LN                      TYPE LN.
!         06  TERM_TIME_OFST               PIC S999.
!         06  RESERVED                     PIC X(88).
!      04 POS                              REDEFINES DATA.
!         06  TERM_FIID                    TYPE FIID.
!         06  TERM_LN                      TYPE LN.
!         06  TERM_TIME_OFST               PIC S999.
!         06  PSEUDO_TERM_ID               PIC X(4).
!         06  RESERVED                     PIC X(84).
!      04 FHM                              REDEFINES DATA.
!         06  DPC_NUM                      PIC 9(4).
!         06  STA_INDEX                    PIC 9(4).
!         06  LN                           TYPE *.
!         06  LT_TIMSTP                    PIC 9(14).
!         06  LOG_IND                      PIC X.
!         06  COMPL_REQ                    PIC X.
!         06  FM_USER_GRP                  PIC 9(4).
!         06  FM_USER_NUM                  PIC 9(8).
!         06  FM_USER_STA                  PIC X(4).
!         06  FM_TIMSTP                    PIC 9(14).
!         06  RESERVED                     PIC X(42).
!      04 EMS                              REDEFINES DATA.
!         06  DPC_NUM                      PIC 9(4).
!         06  HOST_PRO_NAM                 PIC X(16).
!         06  LN                           TYPE *.
!         06  MSG_ID                       PIC X(4).
!         06  FIID                         TYPE FIID.
!         06  CATEGORY_NAM                 PIC X(16).
!         06  RESERVED                     PIC X(52).
!      04 TB                               REDEFINES DATA.
!         06  ACQ_FIID                     TYPE FIID.
!         06  ACQ_LN                       TYPE LN.
!         06  SRC_CDE                      PIC X(2).
!         06  RESERVED                     PIC X(90).
  02  PRI_RSRVD2_PRVT                      OCCURS 0 TO 1 TIMES DEPENDING ON BP61              DISPLAY "POS Data".                         !DE061
      04  LEN                              PIC 9(3).
      04  DATA                             PIC X(100) VLENGTH DEPENDING ON LEN.
      04  ATM                              REDEFINES DATA.
          06  CRD_FIID                     TYPE FIID.
          06  CRD_LN                       TYPE LN.
          06  SAVE_ACCT_TYP                PIC X(4).
          06  AUTHORIZER                   PIC X(1).
          06  ORIG_TRAN_CDE                PIC X(2).
          06  RESERVED                     PIC X(85).
      04  POS                              REDEFINES DATA.
          06  CRD_FIID                     TYPE FIID.
          06  CRD_LN                       TYPE LN.
          06  CATEGORY                     PIC X(1).
          06  SAVE_ACCT_TYP                PIC X(2).
          06  ICHG_RESP                    PIC X(8).
          06  RESERVED                     PIC X(81).
!      04  FHM                              REDEFINES DATA.
!          06  USER_DATA                    PIC X(100).
!      04  EMS                              REDEFINES DATA.
!          06  CERTIFY_STMP.
!            08  DELIVERY_DAT               PIC 9(6).
!            08  DELIVERY_TIM               PIC 9(4).
!          06  DELIVERY_STAT                PIC 9.
!          06  LOC_NAM                      PIC X(16).
!          06  LOC_TYP                      PIC 9.
!          06  MBF_TIMSTP                   PIC 9(4).
!          06  SIG                          PIC X(16).
!          06  RESERVED                     PIC X(52).
!      04  TLR                              REDEFINES DATA.
!          06  DEV_TRAN_CDE                 PIC X(6).
!          06  CUST_PASSBOOK_BAL            PIC X(19).
!          06  INTL_OVRRD_LVL               PIC X.
!          06  MAX_TERM_OVRRD_LVL           PIC X.
!          06  PAPERLESS_TRAN               PIC X.
!          06  ADVC_RESP_REQ                PIC X.
!          06  CRD_PRESENT                  PIC X.
!          06  RSN_CDE                      PIC X(2).
!          06  SAVE_FROM_ACCT_TYP           PIC 9(2).
!          06  SAVE_TO_ACCT_TYP             PIC 9(2).
!          06  MULT_ACCT                    PIC X.
!          06  TERM_TIM_OFST                PIC S9999.
!          06  COMPLETE_TRACK2_DATA         PIC X.
!          06  OVRRD_TLR_ID                 PIC X(8).
!          06  RESERVED                     PIC X(49).
!      04  TB                               REDEFINES DATA.
!          06  ISS_FIID                     TYPE FIID.
!          06  ISS_LN                       TYPE LN.
!          06  ERR_FLG                      PIC X.
!          06  CUST_REF_NUM                 PIC X(6).
!          06  RESERVED                     PIC X(85).
  02  PRI_RSRVD3_PRVT OCCURS 0 TO 1 TIMES DEPENDING ON BP62.                                                                               !DE062
      04  LEN                             PIC 9(3).
      04  DATA                            PIC 9(100) VLENGTH DEPENDING ON LEN.
      04  B24_DEF                         REDEFINES DATA.
          06  POSTAL_CDE                  TYPE POSTAL_CDE.
          06  RESERVED                    PIC X(90).
!      04  EMS                              REDEFINES DATA.
!          06  TERM_NAM                     PIC X(16).
!          06  RESERVED                     PIC X(84).
!      04  FHM_CAF                          REDEFINES DATA.
!          06  EXPONENT                     PIC X.
!          06  RESERVED                     PIC X(99).
!      04  TLR                              REDEFINES DATA.
!          06  CRD_ACTION                   PIC X.
!          06  ERR_FLG                      PIC X.
!          06  MIN_OVRRD_LVL                PIC X.
!          06  CRD_VRFY_FLG                 PIC X.
!          06  ACCT_IND                     PIC X.
!          06  RESERVED                     PIC X(95).
!      04  TB                               REDEFINES DATA.
!          06  PRD_TYP                      PIC X(2).
!          06  NUM_PRD                      PIC X(4).
!          06  SKIP_NXT_PMNT_IND            PIC 9.
!          06  RESERVED                     PIC X(93).
  02  PRI_RSRVD4_PRVT                     OCCURS 0 TO 1 TIMES DEPENDING ON BP63           DISPLAY  "PIN ofst|Pos addl data|.".            !DE063
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(997) VLENGTH DEPENDING ON LEN.
!      04  PIN_SELECT                      REDEFINES DATA.
!          06  NEW_PIN_OFST                PIC X(16).
!          06  RESERVED                    PIC X(981).
!      04  EMS                             REDEFINES DATA.
!          06  RQST_TYP                    PIC 99.
!          06  LOOKUP_TYP                  PIC 9.
!          06  SCROLL_THRU                 PIC 9.
!          06  RESERVED                    PIC X(993).
!      04  TLR                             REDEFINES DATA.
!          06  TKN_ID                      PIC X(2).
!          06  NBF_TKN                     PIC X(595).
!          06  RESERVED                    PIC X(400).
!      04  POS                             REDEFINES DATA.
!          06  ADD_DATA                    PIC X(997).
!      04  FHM_CAF                         REDEFINES DATA.
!          06  SUPER_TLR_TERM_ID           PIC X(16) OCCURS 10 TIMES.
!          06  RESERVED                    PIC X(837).
!      04  FHM_NEG                         REDEFINES DATA.
!          06  SUPER_TLR_TERM_ID           PIC X(16) OCCURS 10 TIMES.
!          06  RESERVED                    PIC X(837).
!      04  TB                              REDEFINES DATA.
!          06  NEW_PIN_OFST                PIC X(16).
!          06  OLD_PIN_OFST                PIC X(16).
!          06  RESERVED                    PIC X(965).
!      04  FHM_SEMF                        REDEFINES DATA.
!          06  PROD_TYP                    PIC X(2).
!          06  PLASTIC_TYP                 PIC X(2).
!          06  RQST_DAT                    PIC X(8).
!          06  PIN_CHNG                    PIC X(8).
!          06  LAST_ISS                    PIC X(8).
!          06  EMBOSS                      PIC X(8).
!          06  LAST_CRV                    PIC X(8).
!          06  LAST_ADDR_CHNG              PIC X(8).
!          06  LAST_STAT_CHNG              PIC X(8).
!          06  OPN_DAT                     PIC X(8).
!          06  NUM_ISS                     PIC X(2).
!          06  CVV2                        PIC X(3).
!          06  STAT                        PIC X(1).
!          06  BLK_CDE                     PIC X(1).
!          06  RECLASS_CDE                 PIC X(1).
!          06  RATE_CLASS                  PIC X(1).
!          06  BHVR_SCORE                  PIC X(4).
!          06  CASH_ADV_AMT                PIC X(12).
!          06  PURCH_AMT                   PIC X(12).
!          06  BAL_AMT                     PIC X(12).
!          06  FRAUD_SCORE                 PIC X(3).
!          06  WRK_PHN                     PIC X(20).
!          06  MOB_PHN                     PIC X(20).
!          06  EMAIL_ADDR                  PIC X(40).
!          06  ID                          PIC X(30).
!          06  PREV_CRD_NUM                PIC X(19).
!          06  PRI_NAM                     PIC X(40).
!          06  PRI_GOVT_ID                 PIC X(20).
!          06  PRI_DOB                     PIC X(8).
!          06  SEC_NAM                     PIC X(40).
!          06  ADNL_NAM                    PIC X(40).
!          06  MTHR_MDN_NAM                PIC X(40).
!          06  ADDR_1                      PIC X(40).
!          06  ADDR_2                      PIC X(40).
!          06  CITY                        PIC X(30).
!          06  ST                          PIC X(30).
!          06  CNTRY                       PIC X(3).
!          06  POSTAL_CDE                  PIC X(16).
!          06  HOME_PHN                    PIC X(20).
!          06  RESERVED                    PIC X(381).
!      04  FHM_SEMF_70                     REDEFINES DATA.
!          06  PROD_TYP                    PIC X(2).
!          06  PLASTIC_TYP                 PIC X(2).
!          06  RQST_DAT                    PIC X(8).
!          06  PIN_CHNG                    PIC X(8).
!          06  LAST_ISS                    PIC X(8).
!          06  EMBOSS                      PIC X(8).
!          06  LAST_CRV                    PIC X(8).
!          06  LAST_ADDR_CHNG              PIC X(8).
!          06  LAST_STAT_CHNG              PIC X(8).
!          06  OPN_DAT                     PIC X(8).
!          06  NUM_ISS                     PIC X(2).
!          06  BLK_CDE                     PIC X(1).
!          06  RECLASS_CDE                 PIC X(1).
!          06  BHVR_SCORE                  PIC X(4).
!          06  CASH_ADV_AMT                PIC X(12).
!          06  PURCH_AMT                   PIC X(12).
!          06  BAL_AMT                     PIC X(12).
!          06  CUST_MSTR_AMT               PIC X(16).
!          06  CUST_MSTR_DAT               PIC X(8).
!          06  CUST_MSTR_FLD1              PIC X(2).
!          06  CUST_MSTR_FLD2              PIC X(4).
!          06  WRK_PHN                     PIC X(20).
!          06  MOB_PHN                     PIC X(20).
!          06  EMAIL_ADDR                  PIC X(40).
!          06  ID                          PIC X(30).
!          06  PREV_CRD_NUM                PIC X(19).
!          06  PRI_NAM                     PIC X(40).
!          06  PRI_GOVT_ID                 PIC X(20).
!          06  PRI_DOB                     PIC X(8).
!          06  SEC_NAM                     PIC X(40).
!          06  ADNL_NAM                    PIC X(40).
!          06  OTHR_NAM                    PIC X(40).
!          06  ADDR_1                      PIC X(40).
!          06  ADDR_2                      PIC X(40).
!          06  CITY                        PIC X(30).
!          06  ST                          PIC X(30).
!          06  CNTRY                       PIC X(3).
!          06  POSTAL_CDE                  PIC X(16).
!          06  HOME_PHN                    PIC X(20).
!          06  RESERVED                    PIC X(359).
!      04  FHM_ENHNC_PRE_AUTH_CAF          REDEFINES DATA.
!          06  ENHNC_PRE_AUTH              OCCURS 9 TIMES.
!              08  SEQ_NUM                 PIC X(12).
!              08  HOLD_AMT                PIC X(19).
!              08  PR_TS.
!                  10  DAT                 TYPE DAT.
!                  10  TIM                 TYPE TIM.
!              08  APPV_CDE                PIC X(8).
!              08  PR_TXN_TS.
!                  10  DAT                 TYPE DAT.
!                  10  TIM                 TYPE TIM.
!              08  TERM_ID                 PIC X(16).
!              08  ACCT_TYP                PIC X(2).
!              08  ACCT_NUM                PIC X(19).
!              08  HOLD_FLG                PIC X.
!          06  RESERVED                    PIC X(52).
  02  PRI_MAC_CDE                         OCCURS 0 TO 1 TIMES DEPENDING ON BP64 PIC X(16).                                                 !DE064

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  02  FILLER                              OCCURS 0 TO 1 TIMES DEPENDING ON BS1 PIC X.                                                     !DE065
  02  SETL_CDE                            OCCURS 0 TO 1 TIMES DEPENDING ON BS2 PIC 9       DISPLAY "Settl Code".                          !DE066
  02  EXTD_PAY_CDE                        OCCURS 0 TO 1 TIMES DEPENDING ON BS3 PIC 9(2).                                                  !DE067
  02  RCV_INST_CNTRY_CDE                  OCCURS 0 TO 1 TIMES DEPENDING ON BS4 PIC 9(3)    DISPLAY "Recv. Inst Cntry Code".               !DE068
  02  SETL_INST_CNTRY_CDE                 OCCURS 0 TO 1 TIMES DEPENDING ON BS5 PIC 9(3).                                                  !DE069
  02  NETW_MGMT_CDE                       OCCURS 0 TO 1 TIMES DEPENDING ON BS6 PIC 9(3)    DISPLAY "NMM Code".                            !DE070
  02  MSG_NUM                             OCCURS 0 TO 1 TIMES DEPENDING ON BS7 PIC 9(4).                                                  !DE071
  02  LST_MSG_NUM                         OCCURS 0 TO 1 TIMES DEPENDING ON BS8 PIC 9(4).                                                  !DE072
  02  ACTION_DAT                          OCCURS 0 TO 1 TIMES DEPENDING ON BS9 PIC 9(6).                                                  !DE073
  02  NUM_CR                              OCCURS 0 TO 1 TIMES DEPENDING ON BS10 PIC 9(10).                                                 !DE074
  02  NUM_CR_RVSL                         OCCURS 0 TO 1 TIMES DEPENDING ON BS11 PIC 9(10).                                                 !DE075
  02  NUM_DB                              OCCURS 0 TO 1 TIMES DEPENDING ON BS12 PIC 9(10).                                                 !DE076
  02  NUM_DB_RVSL                         OCCURS 0 TO 1 TIMES DEPENDING ON BS13 PIC 9(10).                                                 !DE077
  02  NUM_XFER                            OCCURS 0 TO 1 TIMES DEPENDING ON BS14 PIC 9(10).                                                 !DE078
  02  NUM_XFER_RVSL                       OCCURS 0 TO 1 TIMES DEPENDING ON BS15 PIC 9(10).                                                 !DE079
  02  NUM_INQ                             OCCURS 0 TO 1 TIMES DEPENDING ON BS16 PIC 9(10).                                                 !DE080
  02  NUM_AUTH                            OCCURS 0 TO 1 TIMES DEPENDING ON BS17 PIC 9(10).                                                 !DE081
  02  AMT_CR_PROC_FEES                    OCCURS 0 TO 1 TIMES DEPENDING ON BS18 PIC 9(12).                                                 !DE082
  02  AMT_CR_TRAN_FEES                    OCCURS 0 TO 1 TIMES DEPENDING ON BS19 PIC 9(12).                                                 !DE083
  02  AMT_DB_PROC_FEES                    OCCURS 0 TO 1 TIMES DEPENDING ON BS20 PIC 9(12).                                                 !DE084
  02  AMT_DB_TRAN_FEES                    OCCURS 0 TO 1 TIMES DEPENDING ON BS21 PIC 9(12).                                                 !DE085
  02  AMT_CR                              OCCURS 0 TO 1 TIMES DEPENDING ON BS22 PIC 9(16).                                                 !DE086
  02  AMT_CR_RVSL                         OCCURS 0 TO 1 TIMES DEPENDING ON BS23 PIC 9(16).                                                 !DE087
  02  AMT_DB                              OCCURS 0 TO 1 TIMES DEPENDING ON BS24 PIC 9(16).                                                 !DE088
  02  AMT_DB_RVSL                         OCCURS 0 TO 1 TIMES DEPENDING ON BS25 PIC 9(16).                                                 !DE089
  02  ORIG_INFO                           OCCURS 0 TO 1 TIMES DEPENDING ON BS26             DISPLAY "Original Data Elements".              !DE090
      04  TYP                             PIC 9(4).
      04  TRACE_NUM                       PIC 9(6).
      04  XMIT_DAT_TIM                    PIC 9(10).
      04  ACQ_INST_ID_NUM                 PIC X(11).
      04  FRWD_INST_ID_NUM                PIC 9(11).
  02  B24_ORIG                            REDEFINES ORIG_INFO.
      04  ORIG_TYP                        PIC X(4).
      04  ORIG_SEQ_NUM                    PIC X(12).
      04  ORIG_TRAN_DAT                   PIC X(4).
      04  ORIG_TRAN_TIM                   TYPE TIM.
      04  ORIG_B24_POST_DAT               PIC X(4).
      04  RESERVED                        PIC X(10).

  02  FILE_UPDT_CDE                       OCCURS 0 TO 1 TIMES DEPENDING ON BS27 PIC X.                                                     !DE091
  02  FILE_SEC_CDE                        OCCURS 0 TO 1 TIMES DEPENDING ON BS28 PIC X(2).                                                  !DE092
  02  RESP_IND                            OCCURS 0 TO 1 TIMES DEPENDING ON BS29 PIC X(5).                                                  !DE093
  02  SRV_IND                             OCCURS 0 TO 1 TIMES DEPENDING ON BS30 PIC X(7).                                                  !DE094
  02  REPLACEMENT OCCURS 0 TO 1 TIMES DEPENDING ON BS31.                                                                                   !DE095
      04  AMT                             PIC 9(12).
      04  SETL_AMT                        PIC 9(12).
      04  TRAN_FEE                        PIC 9(9).
      04  SETL_FEE                        PIC 9(9).

  02  MSG_SEC_CDE                         OCCURS 0 TO 1 TIMES DEPENDING ON BS32 PIC X(16).                                                 !DE096
  02  SETL_AMT_NET                        OCCURS 0 TO 1 TIMES DEPENDING ON BS33.                                                           !DE097
      04  IND                             PIC X.
      04  AMT                             PIC 9(16).
  02  PAYEE                               OCCURS 0 TO 1 TIMES DEPENDING ON BS34 PIC X(25).                                                 !DE098
  02  SETL_INST OCCURS 0 TO 1 TIMES DEPENDING ON BS35.                                                                                     !DE099
      04  LEN                             PIC 9(2).
      04  ID_NUM                          PIC 9(11) VLENGTH DEPENDING ON LEN.
  02  RCV_INST                            OCCURS 0 TO 1 TIMES DEPENDING ON BS36               DISPLAY "Receiving Inst Id".                 !DE100
      04  LEN                             PIC 9(2).
      04  ID_NUM                          PIC 9(11) VLENGTH DEPENDING ON LEN.
  02  FNAME OCCURS 0 TO 1 TIMES DEPENDING ON BS37 .                                                                                        !DE101
      04  LEN                             PIC 9(2).
      04  NAME                            PIC X(17) VLENGTH DEPENDING ON LEN.
  02  ACCT1                               OCCURS 0 TO 1 TIMES DEPENDING ON BS38               DISPLAY "Account 1".                         !DE102
      04  LEN                             PIC 9(2).
      04  NUM                             PIC X(28) VLENGTH DEPENDING ON LEN.
  02  ACCT2                               OCCURS 0 TO 1 TIMES DEPENDING ON BS39                DISPLAY "Account 2".                        !DE103
      04  LEN                             PIC 9(2).
      04  NUM                             PIC X(28) VLENGTH DEPENDING ON LEN.
  02  TRAN_DESCR OCCURS 0 TO 1 TIMES DEPENDING ON BS40.                                                                                    !DE104
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
!      04  TB                              REDEFINES DATA.
!          06  CUST_ACCT_NUM_VNDR          PIC X(28).
!          06  VNDR_NAM                    PIC X(32).
!          06  RESERVED                    PIC X(40).
  02  SECNDRY_RSRVD1_ISO OCCURS 0 TO 1 TIMES DEPENDING ON BS41.                                                                            !DE105
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
  02  SECNDRY_RSRVD2_ISO OCCURS 0 TO 1 TIMES DEPENDING ON BS42.                                                                            !DE106
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
  02  SECNDRY_RSRVD3_ISO OCCURS 0 TO 1 TIMES DEPENDING ON BS43.                                                                            !DE107
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
  02  SECNDRY_RSRVD4_ISO OCCURS 0 TO 1 TIMES DEPENDING ON BS44.                                                                            !DE108
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
  02  SECNDRY_RSRVD5_ISO OCCURS 0 TO 1 TIMES DEPENDING ON BS45.                                                                            !DE109
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
  02  SECNDRY_RSRVD6_ISO OCCURS 0 TO 1 TIMES DEPENDING ON BS46.                                                                            !DE110
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
  02  SECNDRY_RSRVD7_ISO OCCURS 0 TO 1 TIMES DEPENDING ON BS47.                                                                            !DE111
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
  02  SECNDRY_RSRVD1_NATL OCCURS 0 TO 1 TIMES DEPENDING ON BS48.                                                                           !DE112
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(200) VLENGTH DEPENDING ON LEN.
!      04  TLR                             REDEFINES DATA.
!          06  TKN_ID                      PIC X(2).
!          06  OVRRD_TKN                   PIC X(198).
!      04  FHM_ENHNC_PRE_AUTH_CAF_EXT      REDEFINES DATA.
!          06  SEQ_NUM                     PIC X(12).
!          06  HOLD_AMT                    PIC X(19).
!          06  PR_TS.
!              08  DAT                     TYPE DAT.
!              08  TIM                     TYPE TIM.
!          06  APPV_CDE                    PIC X(8).
!          06  PR_TXN_TS.
!              08  DAT                     TYPE DAT.
!              08  TIM                     TYPE TIM.
!          06  TERM_ID                     PIC X(16).
!          06  ACCT_TYP                    PIC X(2).
!          06  ACCT_NUM                    PIC X(19).
!          06  HOLD_FLG                    PIC X.
!          06  RESERVED                    PIC X(95).
  02  SECNDRY_RSRVD2_NATL OCCURS 0 TO 1 TIMES DEPENDING ON BS49.                                                                          !DE113
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(400).
      04  HOT_CRD_UPDT                    REDEFINES DATA.
          06  DEST_SVC                    PIC X(16).
          06  PATH_HDR_FRMT               PIC X(2).
          06  PATH_RQST                   PIC X(382).
  02  SECNDRY_RSRVD3_NATL OCCURS 0 TO 1 TIMES DEPENDING ON BS50.                                                                          !DE114
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(500) VLENGTH DEPENDING ON LEN.
      04  TLR                             REDEFINES DATA.
          06  TKN_ID                      PIC X(2).
          06  WHFFI_TKN                   PIC X(498).
!      04  FHM_PBF_CSFC                    REDEFINES DATA.
!          06  PRIOR_YTD_INT               PIC X(18).
!          06  MIN_AMT_DUE                 PIC X(18).
!          06  CUR_INT_RATE                PIC X(8).
!          06  CASH_ADV_INT_RATE           PIC X(8).
!          06  NEXT_PMNT_DUE_DAT           PIC X(6).
!          06  CR_OD_LMT_CHG_DAT           PIC X(6).
!          06  CYC_CNT                     PIC X(2).
!          06  CYC_DATA                    OCCURS 12 TIMES.
!              08  DB_CR_HIST              PIC X(10).
!              08  DB_HIST                 REDEFINES DB_CR_HIST.
!                  10  NSF                 PIC X(5).
!                  10  OD                  PIC X(5).
!              08  CR_HIST                 REDEFINES DB_CR_HIST.
!                  10  NUM_DELINQ          PIC X(5).
!                  10  NUM_CR_LMT_EXCEED   PIC X(5).
!              08  ACCT_BAL                PIC X(18).
!              08  ACCT_STAT               PIC X.
!              08  USER_FLD3               PIC X.
!          06  RESERVED                    PIC X(74).
!      04  HOT_CRD_UPDT                    REDEFINES DATA.
!          06  DEST_SVC                    PIC X(16).
!          06  PATH_HDR_FRMT               PIC X(2).
!          06  PATH_RQST                   PIC X(482).
  02  SECNDRY_RSRVD4_NATL OCCURS 0 TO 1 TIMES DEPENDING ON BS51.                                                                       !DE115
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(400) VLENGTH DEPENDING ON LEN.
!      04  TLR                             REDEFINES DATA.
!          06  TKN_ID                      PIC X(2).
!          06  WHFFI_TKN                   PIC X(398).
!      04  FHM_CAF_BASE_USER_INFO          REDEFINES DATA.
!          06  USER_FLD_ACI                PIC X(50).
!          06  USER_FLD_REGN               PIC X(50).
!          06  USER_FLD_CUST               PIC X(50).
!          06  RESERVED                    PIC X(250).
!      04  FHM_PBF_BASE_USER_INFO          REDEFINES DATA.
!          06  USER_FLD_ACI                PIC X(50).
!          06  USER_FLD_REGN               PIC X(50).
!          06  USER_FLD_CUST               PIC X(50).
!          06  RESERVED                    PIC X(250).
  02  SECNDRY_RSRVD5_NATL OCCURS 0 TO 1 TIMES DEPENDING ON BS52.                                                                       !DE116
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(400) VLENGTH DEPENDING ON LEN.
!      04  TLR                             REDEFINES DATA.
!          06  TKN_ID                      PIC X(2).
!          06  WHFFI_TKN                   PIC X(398).
!      04  FHM_NCD_CAF                     REDEFINES DATA.
!          06  USE_LMT                     PIC X(4).
!          06  CASH_VAL_TTL_WDL_LMT        PIC X(12).
!          06  CASH_VAL_OFFL_WDL_LMT       PIC X(12).
!          06  CASH_VAL_TTL_CCA_LMT        PIC X(12).
!          06  CASH_VAL_OFFL_CCA_LMT       PIC X(12).
!          06  NCD1_CDE                    PIC X(2).
!          06  NCD1_TTL_WDL_LMT            PIC X(12).
!          06  NCD1_OFFL_WDL_LMT           PIC X(12).
!          06  NCD1_TTL_CCA_LMT            PIC X(12).
!          06  NCD1_OFFL_CCA_LMT           PIC X(12).
!          06  NCD2_CDE                    PIC X(2).
!          06  NCD2_TTL_WDL_LMT            PIC X(12).
!          06  NCD2_OFFL_WDL_LMT           PIC X(12).
!          06  NCD2_TTL_CCA_LMT            PIC X(12).
!          06  NCD2_OFFL_CCA_LMT           PIC X(12).
!          06  RESERVED                    PIC X(248).
  02  SECNDRY_RSRVD6_NATL OCCURS 0 TO 1 TIMES DEPENDING ON BS53.                                                                      !DE117
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
!      04  TLR                             REDEFINES DATA.
!          06  TKN_ID                      PIC X(2).
!          06  PBFU_TKN                    PIC X(98).
!      04  FHM_CAF_EMV                     REDEFINES DATA.
!          06  ATC_LMT                     PIC X(4).
!          06  SEND_CRD_BLK                PIC X.
!          06  SEND_PUT_DATA               PIC X.
!          06  VLCTY_LMTS.
!              08  LWR_CONSEC_LMT          PIC X(4).
!              08  DATA_TAG                PIC X(4).
!          06  SEND_PIN_UNBLK              PIC X.
!          06  SEND_PIN_CHNG               PIC X.
!          06  PIN_SYNC_ACT                PIC X.
!          06  ACCESS_SCRIPT_MGMT_SUB_SYS  PIC X.
!          06  ISS_APPL_DATA_FRMT          PIC X.
!          06  ACTION_TABLE_INDEX          PIC X.
!          06  CAP_APSN_1                  PIC X(2).
!          06  CAP_DKI_1                   PIC X(2).
!          06  CAP_APSN_2                  PIC X(2).
!          06  CAP_DKI_2                   PIC X(2).
!          06  BAD_CAP_TKN_OVRRD_FLG       PIC X.
!          06  SCRIPT_TPLT_TAG             PIC X.
!          06  SCRIPT_MAC_LGTH             PIC X.
!          06  RESERVED                    PIC X(69).
  02  SECNDRY_RSRVD7_NATL OCCURS 0 TO 1 TIMES DEPENDING ON BS54.                                                                       !DE118
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
!      04  TLR                             REDEFINES DATA.
!          06  TKN_ID                      PIC X(2).
!          06  SPFU_TKN                    PIC X(98).
!      04  FHM_CAF_PFRD_TXN                REDEFINES DATA.
!          06 ACCT_NUM                     PIC X(19).
!          06 TRAN_CDE                     PIC X(2).
!          06 FROM_ACCT_TYP                PIC X(2).
!          06 TO_ACCT_TYP                  PIC X(2).
!          06 RCPT_OPT                     PIC X(1).
!          06 AMT                          PIC X(19).
!          06 PRFL_UPDT_IND                PIC X(1).
!          06 ADA_IND                      PIC X(1).
!          06 MRKT_SEG_IND                 PIC X(2).
!          06 RESERVED                     PIC X(51).
!      04  FHM_PBF                         REDEFINES DATA.
!          06  CASH_ADV_MIN                PIC X(15).
!          06  CASH_ADV_INCR               PIC X(15).
!          06  PRD_LMT.
!              08  XFER.
!                  10  AMT                 PIC X(15).
!                  10  CNT                 PIC X(4).
!          06  CYC_LMT.
!              08  XFER.
!                  10  AMT                 PIC X(15).
!                  10  CNT                 PIC X(4).
!          06 RESERVED                     PIC X(32).
  02  SECNDRY_RSRVD8_NATL OCCURS 0 TO 1 TIMES DEPENDING ON BS55.                                                                      !DE119
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(100) VLENGTH DEPENDING ON LEN.
!      04  TLR                             REDEFINES DATA.
!          06  TKN_ID                      PIC X(2).
!          06  WHFFU_TKN                   PIC X(98).
!      04  FHM_CAF_SSBC                    REDEFINES DATA.
!          06  CORP_NUM                    PIC X(10).
!          06  CHK_BASE_FLG                PIC X.
!          06  CSF_CHK_BASE_FLG            PIC X.
!          06  CSF_CHK_USE_LMT             PIC X(4).
!          06  TTL_CHK_LMT                 PIC X(12).
!          06  OFFL_CHK_LMT                PIC X(12).
!          06  TTL_CSF_CHK_LMT             PIC X(12).
!          06  OFFL_CSF_CHK_LMT            PIC X(12).
!          06  RESERVED                    PIC X(36).
!      04  FHM_NEG_SSBC                    REDEFINES DATA.
!          06  CORP_NUM                    PIC X(10).
!          06  RESERVED                    PIC X(90).
  02  SECNDRY_RSRVD1_PRVT OCCURS 0 TO 1 TIMES DEPENDING ON BS56.                                                                      !DE120
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(150) VLENGTH DEPENDING ON LEN.
!      04  TLR                             REDEFINES DATA.
!          06  TKN_ID                      PIC X(2).
!          06  ADMIN_TKN                   PIC X(148).
!      04  ATM                             REDEFINES DATA.
!          06  TERM_NAME_LOC               PIC X(25).
!          06  BRCH_ID                     TYPE BRCH.
!          06  REGN_ID                     TYPE REGN.
!          06  RESERVED                    PIC X(117).
!      04  POS                             REDEFINES DATA.
!          06  TERM_NAME_LOC               PIC X(25).
!          06  BRCH_ID                     TYPE BRCH.
!          06  RESERVED                    PIC X(121).
!      04  FHM_CAF_EXPAND                  REDEFINES DATA.
!          06  CRD_TYP                     PIC X(2).
!          06  CRD_STAT                    PIC X.
!          06  PIN_OFST                    PIC X(16).
!          06  TTL_WDL_LMT                 PIC X(12).
!          06  OFFL_WDL_LMT                PIC X(12).
!          06  TTL_CCA_LMT                 PIC X(12).
!          06  OFFL_CCA_LMT                PIC X(12).
!          06  AGGR_LMT                    PIC X(12).
!          06  OFFL_AGGR_LMT               PIC X(12).
!          06  EXP_DAT                     PIC X(4).
!          06  EFFECTIVE_DAT               PIC X(4).
!          06  SCND_CRD_DATA.
!              08  EXP_DAT_2               PIC X(4).
!              08  EFFECTIVE_DAT_2         PIC X(4).
!              08  CRD_STAT_2              PIC X(1).
!              08  USER_FLD1_SCND_CRD_DATA PIC X(1).
!          06  RESERVED                    PIC X(41).
!      04  FHM_CAF                         REDEFINES DATA.
!          06  CRD_TYP                     PIC X(2).
!          06  CRD_STAT                    PIC X.
!          06  PIN_OFST                    PIC X(16).
!          06  TTL_WDL_LMT                 PIC X(12).
!          06  OFFL_WDL_LMT                PIC X(12).
!          06  TTL_CCA_LMT                 PIC X(12).
!          06  OFFL_CCA_LMT                PIC X(12).
!          06  AGGR_LMT                    PIC X(12).
!          06  OFFL_AGGR_LMT               PIC X(12).
!          06  EXP_DAT                     PIC X(4).
!          06  RESERVED                    PIC X(55).
!      04  FHM_NEG                         REDEFINES DATA.
!          06  CRD_TYP                     PIC X(2).
!          06  RSN_CDE                     PIC X(2).
!          06  CAPTURE_CDE                 PIC X.
!          06  ADD_DAT                     PIC X(6).
!          06  EXP_DAT                     PIC X(4).
!          06  RESERVED                    PIC X(135).
!      04  FHM_PBF                         REDEFINES DATA.
!          06  ACCT_STAT                   PIC X.
!          06  AVAIL_BAL                   PIC X(12).
!          06  LEDG_BAL                    PIC X(12).
!          06  AMT_ON_HLD                  PIC X(12).
!          06  OVRDRFT_LMT                 PIC X(8).
!          06  LAST_DEP_DAT                PIC X(6).
!          06  LAST_DEP_AMT                PIC X(12).
!          06  LAST_WDL_DAT                PIC X(6).
!          06  LAST_WDL_AMT                PIC X(12).
!          06  RESERVED                    PIC X(69).
!      04  FHM_PBF_EXPAND_REL5             REDEFINES DATA.
!          06  ACCT_STAT                   PIC X.
!          06  AVAIL_BAL                   PIC X(19).
!          06  LEDG_BAL                    PIC X(19).
!          06  AMT_ON_HLD                  PIC X(19).
!          06  OVRDRFT_LMT                 PIC X(11).
!          06  LAST_DEP_DAT                PIC X(6).
!          06  LAST_DEP_AMT                PIC X(15).
!          06  LAST_WDL_DAT                PIC X(6).
!          06  LAST_WDL_AMT                PIC X(15).
!          06  RESERVED                    PIC X(39).
!      04  FHM_PBF_EXPAND                  REDEFINES DATA.
!          06  ACCT_STAT                   PIC X.
!          06  AVAIL_BAL                   PIC X(19).
!          06  LEDG_BAL                    PIC X(19).
!          06  AMT_ON_HLD                  PIC X(19).
!          06  OVRDRFT_LMT                 PIC X(11).
!          06  LAST_DEP_DAT                PIC X(6).
!          06  LAST_DEP_AMT                PIC X(15).
!          06  LAST_WDL_DAT                PIC X(6).
!          06  LAST_WDL_AMT                PIC X(15).
!          06  CRNCY_CDE                   TYPE *.
!          06  RESERVED                    PIC X(36).
!      04  FHM_LAF                         REDEFINES DATA.
!          06  CUR_BAL                     PIC X(12).
!          06  PAY_AMT                     PIC X(8).
!          06  RESERVED                    PIC X(130).
!      04  SPF                             REDEFINES DATA.
!          06  AMT                         PIC X(15).
!          06  DAT                         TYPE DAT.
!          06  TIM                         TYPE TIM.
!          06  EXP_DAT                     TYPE DAT.
!          06  DESCR                       PIC X(35).
!          06  PBF_SP_WARN_STAT            PIC X.
!          06  RESERVED                    PIC X(79).
!      04  WHFF                            REDEFINES DATA.
!          06  EXP_DAT                     TYPE DAT.
!          06  DESCR                       PIC X(35).
!          06  PBF_SP_WARN_STAT            PIC X.
!          06  RESERVED                    PIC X(108).
!      04  NBF                             REDEFINES DATA.
!          06  POST_DAT                    TYPE DAT.
!          06  PRNT_STAT                   PIC X.
!          06  PRNT_BAL                    PIC X(12).
!          06  POSTING_SYS                 PIC X.
!          06  TRAN_TYP                    PIC X.
!          06  TRAN_CDE                    PIC X(6).
!          06  TRAN_AMT                    PIC X(12).
!          06  RESERVED                    PIC X(111).
!      04  NBF_EXPAND                      REDEFINES DATA.
!          06  POST_DAT                    TYPE DAT.
!          06  PRNT_STAT                   PIC X.
!          06  PRNT_BAL                    PIC X(19).
!          06  POSTING_SYS                 PIC X.
!          06  TRAN_TYP                    PIC XX.
!          06  TRAN_CDE                    PIC X(6).
!          06  TRAN_AMT                    PIC X(15).
!          06  DEV_TRAN_CDE                PIC X(6).
!          06  TLR_ID                      PIC X(8).
!          06  REGN_ID                     TYPE REGN.
!          06  BRCH_ID                     TYPE BRCH.
!          06  CITY                        PIC X(13).
!          06  RESERVED                    PIC X(65).
!      04  KEY_MGMT                        REDEFINES DATA.
!          06  CHK_DIGITS                  PIC X(6).
!          06  RESERVED                    PIC X(144).
!      04  FHM_CCF                         REDEFINES DATA.
!          06  CORP_NUM                    PIC X(10).
!          06  CORP_DESCR                  PIC X(25).
!          06  CORP_CNT                    PIC X(2).
!          06  RESERVED                    PIC X(113).
!      04  FHM_CSF                         REDEFINES DATA.
!          06  INST_ID_NUM                 PIC X(11).
!          06  ACCT_NUM                    PIC X(19).
!          06  RGSTR_PAN                   PIC X(19).
!          06  RGSTR_MBR_NUM               PIC X(3).
!          06  CHK_STAT                    PIC X.
!          06  RET_CHK_FLG                 PIC X.
!          06  CHK_LMT                     PIC X(15).
!          06  RESERVED                    PIC X(81).
!      04  FHM_CARF                        REDEFINES DATA.
!          06 ACCT_QUAL                    PIC X(4).
!          06 VER                          PIC 9(4).
!          06 STAT                         PIC X.
!          06 DESCR                        PIC X(15).
!          06 ACT_ALWD.
!             08 DB                        PIC X.
!             08 CR                        PIC X.
!             08 INQ                       PIC X.
!          06 RESERVED                     PIC X(123).
!      04  FHM_CUST                        REDEFINES DATA.
!          06 VER                          PIC 9(4).
!          06 FIID                         PIC X(4).
!          06 PRFL                         PIC X(8).
!          06 STAT                         PIC X(2).
!          06 TYP                          PIC X.
!          06 PIN_CHNG_REQ                 PIC X.
!          06 PVD                          PIC X(16).
!          06 PVK_IDX                      PIC X.
!          06 CUST_INFO                    PIC X(60).
!          06 DFLT_ACCT_TYP                PIC X(2).
!          06 DFLT_ACCT_NUM                PIC X(28).
!          06 MAX_HIST_RECS                PIC 9(4).
!          06 RESERVED                     PIC X(19).
!      04  FHM_CCIF0005                    REDEFINES DATA.
!          06  SEG_ID                      PIC X(4).
!          06  VER                         PIC X(4).
!          06  BHVR_SCORE                  PIC X(3).
!          06  RECENT_REISS_IND            PIC X.
!          06  CVV_VAL                     PIC X(3).
!          06  NUM_ISS                     PIC X(2).
!          06  USER_FLD4                   PIC X.
!          06  RESERVED                    PIC X(132).
!      04  FHM_CACT                        REDEFINES DATA.
!          06 ACCT_TYP                     PIC X(2).
!          06 ACCT_QUAL                    PIC X(4).
!          06 FIID                         PIC X(4).
!          06 STAT                         PIC X.
!          06 VER                          PIC 9(4).
!          06 DESCR                        PIC X(15).
!          06 ACT_ALWD_DB                  PIC X.
!          06 ACT_ALWD_CR                  PIC X.
!          06 ACT_ALWD_INQ                 PIC X.
!          06 BEG_DAT                      PIC 9(8).
!          06 END_DAT                      PIC 9(8).
!          06 RESERVED                     PIC X(101).
!      04  FHM_CPIT                        REDEFINES DATA.
!          06 PRSNL_ID                     PIC X(28).
!          06 ID_TYP                       PIC X(2).
!          06 VER                          PIC 9(4).
!          06 RESERVED                     PIC X(116).
  02  SECNDRY_RSRVD2_PRVT                 OCCURS 0 TO 1 TIMES DEPENDING ON BS57        DISPLAY "Auth Agent ID".                           !DE121
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(150) VLENGTH DEPENDING ON LEN.
!      04  POS                             REDEFINES DATA.
!          06  CLERK_ID                    PIC X(6).
!          06  CRT_AUTH_GRP                PIC X(4).
!          06  CRT_AUTH_USER_ID            PIC X(8).
!          06  AUTH_IND                    PIC X.
!          06  AUTH_IND2                   PIC X.
!          06  RESERVED                    PIC X(130).
!      04  FHM_CAF_EXPAND                  REDEFINES DATA.
!          06  USE_LMT                     PIC X(4).
!          06  TTL_WDL_LMT                 PIC X(12).
!          06  OFFL_WDL_LMT                PIC X(12).
!          06  TTL_CCA_LMT                 PIC X(12).
!          06  OFFL_CCA_LMT                PIC X(12).
!          06  DEP_CR_LMT                  PIC X(8).
!          06  ISS_TXN_PRFL                PIC X(16).
!          06  RESERVED                    PIC X(74).
!      04  FHM_CAF                         REDEFINES DATA.
!          06  USE_LMT                     PIC X(4).
!          06  TTL_WDL_LMT                 PIC X(12).
!          06  OFFL_WDL_LMT                PIC X(12).
!          06  TTL_CCA_LMT                 PIC X(12).
!          06  OFFL_CCA_LMT                PIC X(12).
!          06  DEP_CR_LMT                  PIC X(8).
!          06  RESERVED                    PIC X(90).
!      04  TLR                             REDEFINES DATA.
!          06  TKN_ID                      PIC X(2).
!          06  NATIVE_TKN                  PIC X(148).
!      04  TB                              REDEFINES DATA.
!          06  PIN_NEW_1                   PIC X(16).
!          06  PIN_NEW_2                   PIC X(16).
!          06  RESERVED                    PIC X(118).
!      04  FHM_CCIF0001                    REDEFINES DATA.
!          06  SEG_ID                      PIC X(4).
!          06  VER                         PIC X(4).
!          06  STR_ADDR3                   PIC X(30).
!          06  PHN_OTHR2                   PIC X(20).
!          06  PHN_OTHR2_DESCR             PIC X(8).
!          06  RESERVED                    PIC X(84).
  02  SECNDRY_RSRVD3_PRVT OCCURS 0 TO 1 TIMES DEPENDING ON BS58 .                                                                        !DE122
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(200) VLENGTH DEPENDING ON LEN.
!      04  B24                             REDEFINES DATA.
!          06  CRD_ISS_ID_NUM              PIC X(11).
!          06  RESERVED                    PIC X(189).
!      04  FHM_CAF_EXPAND                  REDEFINES DATA.
!          06  TTL_PUR_LMT                 PIC X(12).
!          06  OFFL_PUR_LMT                PIC X(12).
!          06  TTL_CCA_LMT                 PIC X(12).
!          06  OFFL_CCA_LMT                PIC X(12).
!          06  TTL_WDL_LMT                 PIC X(12).
!          06  OFFL_WDL_LMT                PIC X(12).
!          06  USE_LMT                     PIC X(4).
!          06  TTL_RFND_CR_LMT             PIC X(12).
!          06  OFFL_RFND_CR_LMT            PIC X(12).
!          06  RSN_CDE                     PIC X.
!          06  ISS_TXN_PRFL                PIC X(16).
!          06  RESERVED                    PIC X(83).
!      04  FHM_CAF                         REDEFINES DATA.
!          06  TTL_PUR_LMT                 PIC X(12).
!          06  OFFL_PUR_LMT                PIC X(12).
!          06  TTL_CCA_LMT                 PIC X(12).
!          06  OFFL_CCA_LMT                PIC X(12).
!          06  TTL_WDL_LMT                 PIC X(12).
!          06  OFFL_WDL_LMT                PIC X(12).
!          06  USE_LMT                     PIC X(4).
!          06  TTL_RFND_CR_LMT             PIC X(12).
!          06  OFFL_RFND_CR_LMT            PIC X(12).
!          06  RSN_CDE                     PIC X.
!          06  RESERVED                    PIC X(99).
!      04  FHM_PBF                         REDEFINES DATA.
!          06  TTL_FLOAT                   PIC X(12).
!          06  CUR_FLOAT                   PIC X(12).
!          06  RESERVED                    PIC X(176).
!      04  FHM_PBF_EXPAND                  REDEFINES DATA.
!          06  TTL_FLOAT                   PIC X(15).
!          06  CUR_FLOAT                   PIC X(15).
!          06  RESERVED                    PIC X(170).
!      04  EMS                             REDEFINES DATA.
!          06  AFT_STMP.
!            08  PICKUP_STAT               PIC 9.
!            08  PICKUP_DAT                PIC 9(6).
!            08  PICKUP_TIM                PIC 9(4).
!          06  CATEGORY_NAM                PIC X(16).
!          06  COMPL_CDE                   PIC X(2).
!          06  DELIVERY_MDE                PIC 9.
!          06  EXP_STMP.
!            08  DAT                       PIC 9(6).
!            08  TIM                       PIC 9(4).
!          06  ID.
!            08  MSG_DAT                   PIC 9(6).
!            08  MSG_ID                    PIC 9(4).
!          06  INVOICE_NUM                 PIC 9(5).
!          06  MSG_SOURCE_NAM              PIC X(16).
!          06  MSG_SOURCE_TYP              PIC X(1).
!          06  RESP_REQ                    PIC 9.
!          06  TIMSTP                      PIC 9(4).
!          06  RESERVED                    PIC X(123).
!      04  TLR                             REDEFINES DATA.
!          06  TKN_ID                      PIC X(2).
!          06  ACCT_TKN                    PIC X(198).
!      04  FHM_CCIF0002                    REDEFINES DATA.
!          06  SEG_ID                      PIC X(4).
!          06  VER                         PIC X(4).
!          06  NAM                         PIC X(30).
!          06  GOVT_ID                     PIC X(15).
!          06  MTHR_MDN_NAM                PIC X(20).
!          06  DOB                         PIC X(6).
!          06  RESERVED                    PIC X(121).
  02  SECNDRY_RSRVD4_PRVT OCCURS 0 TO 1 TIMES DEPENDING ON BS59.                                                                           !DE123
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(550) VLENGTH DEPENDING ON LEN.
!      04  ATM                             REDEFINES DATA.
!          06  AMT_DEP_CR                  PIC 9(12).
!          06  SETL_AMT_DEP_CR             PIC 9(12).
!          06  RESERVED                    PIC X(526).
!      04  POS                             REDEFINES DATA.
!          06  INVOICE_NUM                 PIC X(10).
!          06  ORIG_INVOICE_NUM            PIC X(10).
!          06  RESERVED                    PIC X(530).
!      04  POS_SETL1A                      REDEFINES DATA.
!          06  SET_RECX1A                  TYPE *.
!          06  RESERVED                    PIC X(382).
!      04  FHM_PBF                         REDEFINES DATA.
!          06  TTL_DEP_AMT                 PIC X(12).
!          06  CONFIDENTIAL_FLG            PIC X.
!          06  SP_STAT                     PIC X.
!          06  ACCRUED_INTEREST_YTD        PIC X(8).
!          06  STRT_BAL                    PIC X(12).
!          06  PASSBOOK_BAL                PIC X(12).
!          06  NBF_REC_CNT                 PIC X(4).
!          06  SIG_CRD_LOC                 PIC X(9).
!          06  PASSBOOK_IND                PIC X.
!          06  CUST_CLASS                  PIC X.
!          06  CASHOUT_LMT                 PIC X(12).
!          06  CASHIN_LMT                  PIC X(12).
!          06  RESERVED                    PIC X(465).
!      04  FHM_PBF_EXPAND                  REDEFINES DATA.
!          06  TTL_DEP_AMT                 PIC X(15).
!          06  CONFIDENTIAL_FLG            PIC X.
!          06  SP_STAT                     PIC X.
!          06  ACCRUED_INTEREST_YTD        PIC X(19).
!          06  STRT_BAL                    PIC X(19).
!          06  PASSBOOK_BAL                PIC X(19).
!          06  NBF_REC_CNT                 PIC X(4).
!          06  SIG_CRD_LOC                 PIC X(9).
!          06  PASSBOOK_IND                PIC X.
!          06  CUST_CLASS                  PIC X.
!          06  CASHOUT_LMT                 PIC X(12).
!          06  CASHIN_LMT                  PIC X(12).
!          06  RESERVED                    PIC X(437).
!      04  FHM_CSTT                        REDEFINES DATA.
!          06  VER                         PIC 9(4).
!          06  FIID                        PIC X(4).
!          06  PRFL                        PIC X(8).
!          06  VRFY_FLG                    PIC X(2).
!          06  LAST_VRFY_FLG_UPD_TS        PIC X(20).
!          06  TYP                         PIC X(2).
!          06  PVD                         PIC X(16).
!          06  LAST_PVD_UPDT_TS            PIC X(20).
!          06  PVK_IDX                     PIC X.
!          06  DFLT_ACCT_NUM               PIC X(28).
!          06  DFLT_ACCT_TYP               PIC X(2).
!          06  MAX_HIST_RECS               PIC 9(5).
!          06  CUST_INFO_1                 PIC X(20).
!          06  CUST_INFO_2                 PIC X(20).
!          06  CUST_INFO_3                 PIC X(20).
!          06  ALT_CONTACT                 PIC X(30).
!          06  BEG_DAT                     PIC 9(8).
!          06  END_DAT                     PIC 9(8).
!          06  BRCH_ID                     PIC X(4).
!          06  PIN_CHNG_REQ                PIC X.
!          06  BP_BILL_TYP                 PIC X(2).
!          06  PCKT_STAT                   PIC X.
!          06  LAST_BP_BILL_TYP_UPDT_TS    PIC X(20).
!          06  LAST_PCKT_STAT_UPDT_TS      PIC X(20).
!          06  BP_BILL_GRP                 PIC X(4).
!          06  BP_TXN_LMT                  PIC 9(15).
!          06  SVC_FEE_ACCT                PIC X(19).
!          06  SVC_FEE_ACCT_TYP            PIC X(2).
!          06  RESERVED                    PIC X(244).
!      04  FHM_PIT                         REDEFINES DATA.
!          06  PRSNL_ID                    PIC X(28).
!          06  VER                         PIC 9(4).
!          06  FIID                        PIC X(4).
!          06  NAM_FMLY                    PIC X(20).
!          06  NAM_GIVEN                   PIC X(10).
!          06  NAM_M_I                     PIC X.
!          06  NAM_TKN_FMLY                PIC X(16).
!          06  NAM_TKN_GIVEN               PIC X(6).
!          06  TITLE                       PIC X(6).
!          06  GOVT_ID                     PIC X(15).
!          06  DOB                         PIC 9(8).
!          06  STR_ADDR_1                  PIC X(30).
!          06  STR_ADDR_2                  PIC X(30).
!          06  STR_ADDR_3                  PIC X(30).
!          06  CITY                        PIC X(25).
!          06  ST_CDE                      PIC X(2).
!          06  CNTRY_CDE                   PIC X(3).
!          06  POSTAL_CDE                  PIC X(10).
!          06  LANG_IND                    PIC 9(4).
!          06  PHN_HOME                    PIC X(20).
!          06  PHN_WORK                    PIC X(20).
!          06  PHN_OTHER_1                 PIC X(20).
!          06  PHN_OTHER_1_DESCR           PIC X(10).
!          06  PHN_OTHER_2                 PIC X(20).
!          06  PHN_OTHER_2_DESCR           PIC X(10).
!          06  MTHR_MDN_NAM                PIC X(20).
!          06  RESERVED                    PIC X(178).
!      04  X917_CSM                        REDEFINES DATA.
!          06  DATA                        PIC X(550).
!      04  EMS                             REDEFINES DATA.
!          06  MAIL_MSG.
!            08  TXT_LINES                 PIC 99.
!            08  TXT                       PIC X(40)
!                                          OCCURS 11 TIMES.
!          06  RESERVED                    PIC X(108).
!      04  TLR                             REDEFINES DATA.
!          06  TKN_ID                      PIC X(2).
!          06  SPFI_TKN                    PIC X(548).
!      04  FHM_CAF                         REDEFINES DATA.
!          06  BRANCH_NUM                  PIC X(4).
!          06  DEPT_NUM                    PIC X(2).
!          06  PIN_MAILER                  PIC X.
!          06  CARD_CARRIER                PIC X.
!          06  CARDHOLDER_TITLE            PIC X.
!          06  OPEN_TEXT1                  PIC X(40).
!          06  NAME_LINE_1                 PIC X(30).
!          06  NAME_LINE_2                 PIC X(30).
!          06  ADDR_LINE_1                 PIC X(34).
!          06  ADDR_LINE_2                 PIC X(34).
!          06  CITY                        PIC X(22).
!          06  STATE                       PIC X(3).
!          06  POSTAL_CODE                 PIC X(9).
!          06  COUNTRY_CODE                PIC X(3).
!          06  ISSUE_STAT                  PIC X(2).
!          06  CARDS_TO_ISSUE              PIC X(2).
!          06  CARDS_RET                   PIC X(2).
!          06  SRVC_CDE                    PIC X(3).
!          06  RESERVED                    PIC X(327).
!      04  TB                              REDEFINES DATA.
!          06  ACCT1_QUAL                  PIC X(4).
!          06  ACCT2_QUAL                  PIC X(4).
!          06  RESERVED                    PIC X(542).
!      04  FHM_CCIF0004                    REDEFINES DATA.
!          06  SEG_ID                      PIC X(4).
!          06  VER                         PIC X(4).
!          06  PHN_HOME                    PIC X(20).
!          06  PHN_WRK                     PIC X(20).
!          06  PHN_OTHR1                   PIC X(20).
!          06  PHN_OTHR1_DESCR             PIC X(8).
!          06  PHN_OTHR2                   PIC X(20).
!          06  PHN_OTHR2_DESCR             PIC X(8).
!          06  RESERVED                    PIC X(446).
  02  SECNDRY_RSRVD5_PRVT OCCURS 0 TO 1 TIMES DEPENDING ON BS60.                                                                           !DE124
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(684) VLENGTH DEPENDING ON LEN.
!      04  ATM                             REDEFINES DATA.
!          06  DEP_TYP                     PIC X.
!          06  RESERVED                    PIC X(683).
!      04  POS                             REDEFINES DATA.
!          06  BATCH_SEQ_NUM               PIC 9(3).
!          06  BATCH_NUM                   PIC 9(3).
!          06  SHIFT_NUM                   PIC 9(3).
!          06  RESERVED                    PIC X(675).
!      04  POS_SETL2                       REDEFINES DATA.
!          06  SET_RECX2                   TYPE *.
!      04  FHM_CAF                         REDEFINES DATA.
!          06  TTL_PUR_LMT                 PIC X(12).
!          06  OFFL_PUR_LMT                PIC X(12).
!          06  TTL_CCA_LMT                 PIC X(12).
!          06  OFFL_CCA_LMT                PIC X(12).
!          06  TTL_WDL_LMT                 PIC X(12).
!          06  OFFL_WDL_LMT                PIC X(12).
!          06  USE_LMT                     PIC X(4).
!          06  RSN_CDE                     PIC X.
!          06  TTL_KEY_ENTRY_LMT.
!              08  CNT                     PIC 9(4).
!              08  AMT                     PIC 9(12).
!          06  OFFL_KEY_ENTRY_LMT.
!              08  CNT                     PIC 9(4).
!              08  AMT                     PIC 9(12).
!          06  PER_DB_TRAN_LMT             PIC 9(7).
!          06  PIN_REQ                     PIC 9.
!          06  OLD_CRD_EXP_DAT             PIC X(4).
!          06  OLD_CRD_STAT                PIC X.
!          06  OLD_CRD_RSN_CDE             PIC X.
!          06  RESERVED                    PIC X(561).
!      04  FHM_NEG                         REDEFINES DATA.
!          06  CRD_EXP_DAT                 PIC X(4).
!          06  OLD_CRD_EXP_DAT             PIC X(4).
!          06  OLD_CRD_RSN_CDE             PIC 9(2).
!          06  RESERVED                    PIC X(674).
!      04  FHM_PBF                         REDEFINES DATA.
!          06  TTL_FLOAT                   PIC X(12).
!          06  CUR_FLOAT                   PIC X(12).
!          06  RESERVED                    PIC X(660).
!      04  TLR                             REDEFINES DATA.
!          06  ADD_DATA                    PIC X(684).
!      04  FHM_PRE_AUTH_CAF                REDEFINES DATA.
!          06  PRE_AUTH                    OCCURS 10 TIMES.
!              08  SEQ_NUM                 PIC X(12).
!              08  HOLD_AMT                PIC X(19).
!              08  PR_TS.
!                  10  DAT                 TYPE DAT.
!                  10  TIM                 TYPE TIM.
!              08  ACCT_TYP                PIC X(2).
!              08  ACCT                    PIC X(19).
!          06  RESERVED                    PIC X(24).
!      04  FHM_PRE_AUTH_PBF                REDEFINES DATA.
!          06  PRE_AUTH                    OCCURS 10 TIMES.
!              08  SEQ_NUM                 PIC X(12).
!              08  HOLD_AMT                PIC X(19).
!              08  PR_TS.
!                  10  DAT                 TYPE DAT.
!                  10  TIM                 TYPE TIM.
!          06  RESERVED                    PIC X(234).
!      04  TB                              REDEFINES DATA.
!          06  TKN_DATA                    PIC X(400).
!          06  RESERVED                    PIC X(284).
!      04  FHM_CCIF0000                    REDEFINES DATA.
!          06  SEG_ID                      PIC X(4).
!          06  VER                         PIC X(4).
!          06  NAM                         PIC X(30).
!          06  GOVT_ID                     PIC X(15).
!          06  MTHR_MDN_NAM                PIC X(20).
!          06  DOB                         PIC X(6).
!          06  STR_ADDR1                   PIC X(30).
!          06  STR_ADDR2                   PIC X(30).
!          06  CITY                        PIC X(18).
!          06  ST                          PIC X(3).
!          06  CNTRY                       PIC X(3).
!          06  POSTAL_CDE                  PIC X(10).
!          06  PHN_HOME                    PIC X(20).
!          06  PHN_WRK                     PIC X(20).
!          06  PHN_OTHR1                   PIC X(20).
!          06  PHN_OTHR1_DESCR             PIC X(8).
!          06  RESERVED                    PIC X(443).
!      04  FHM_CCMF                        REDEFINES DATA.
!          06  PAN                         PIC X(19).
!          06  VER                         PIC X(4).
!          06  DAT_LOST                    PIC X(6).
!          06  DAT_RPTD                    PIC X(6).
!          06  RPT_TAKEN_BY                PIC X(3).
!          06  LOST_STLN_IND               PIC X.
!          06  LINE_CNT                    PIC X(2).
!          06  MEMO_DATA                   PIC X(72) OCCURS 8 TIMES.
!          06  RESERVED                    PIC X(67).
  02  SECNDRY_RSRVD6_PRVT                 OCCURS 0 TO 1 TIMES DEPENDING ON BS61        DISPLAY "Acct Ind/SP/Settl Data".                  !DE125
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(680) VLENGTH DEPENDING ON LEN.
!      04  ATM                             REDEFINES DATA.
!          06 PROC_ACCT_IND                PIC X(1).
!          06 RESERVED                     PIC X(679).
!      04  STMT                            REDEFINES DATA.
!          06 PAGE_IND                     PIC X(2).
!          06 LST_STMT_DAT                 TYPE DAT.
!          06 HEADER_LINES                 PIC X(2).
!          06 COLUMN_LINES                 PIC X(2).
!          06 STMT_FLD                     PIC X(360).
!          06 USER_FLD                     PIC X(308).
!      04  POS                             REDEFINES DATA.
!          06 SRV                          PIC XX.
!          06 ORIG                         PIC X(4).
!          06 DEST                         PIC X(4).
!          06 DFT_CAPTURE_FLG              PIC 9.
!          06 SETL_FLG                     PIC X.
!          06 RESERVED                     PIC X(668).
!      04  POS_SETL1B                      REDEFINES DATA.
!          06 SET_RECX1B                   TYPE *.
!          06 RESERVED                     PIC X(416).
!      04  FHM_PBF_PARAMETRIC_AUTH         REDEFINES DATA.
!          06 DAYS_DELINQ                  PIC 99.
!          06 MONTHS_ACTIVE                PIC 99.
!          06 CYCLE_1                      PIC 99.
!          06 CYCLE_2                      PIC 99.
!          06 CYCLE_3                      PIC 99.
!          06 RESERVED                     PIC X(670).
!      04  TB                              REDEFINES DATA.
!          06  CR_LINE_ACCT                PIC X(28).
!          06  CR_LINE_ACCT_TYP            TYPE ACCT_TYP.
!          06  CR_LINE_AMT                 PIC 9(12).
!          06  CR_LINE_AMT_CHB             PIC 9(12).
!          06  CRNCY_CDE                   PIC 9(3).
!          06  RESERVED                    PIC X(623).
!      04  FHM_CCIF0003                    REDEFINES DATA.
!          06  SEG_ID                      PIC X(4).
!          06  VER                         PIC X(4).
!          06  STR_ADDR1                   PIC X(30).
!          06  STR_ADDR2                   PIC X(30).
!          06  STR_ADDR3                   PIC X(30).
!          06  CITY                        PIC X(18).
!          06  ST                          PIC X(3).
!          06  CNTRY                       PIC X(3).
!          06  POSTAL_CDE                  PIC X(10).
!          06  RESERVED                    PIC X(548).
!      04  HOT_CRD_UPDT                    REDEFINES DATA.
!          06  DEST_SVC                    PIC X(16).
!          06  PATH_HDR_FRMT               PIC X(2).
!          06  PATH_RQST                   PIC X(662).
  02  SECNDRY_RSRVD7_PRVT                 OCCURS 0 TO 1 TIMES DEPENDING ON BS62       DISPLAY "ATM Addl/Preauth Data ".                   !DE126
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(998) VLENGTH DEPENDING ON LEN.
      04  POS                             REDEFINES DATA.
          06 PRE_AUTH_HLD                 PIC 999.
          06 PRE_AUTH_SEQ_NUM             PIC X(12).
          06 RFRL_PHONE                   TYPE PHONE_NUM.
          06 REA_FOR_CHRGBCK              PIC 99.
          06 NUM_OF_CHRGBCK               PIC 9.
          06 RESERVED                     PIC X(960).
  02  SECNDRY_RSRVD8_PRVT OCCURS 0 TO 1 TIMES DEPENDING ON BS63.                                                                          !DE127
      04  LEN                             PIC 9(3).
      04  DATA                            PIC X(200) VLENGTH DEPENDING ON LEN.
!      04  POS_USER_DATA   REDEFINES DATA  PIC X(200).
!      04  ATM                             REDEFINES DATA.
!          06  MICR_DATA                   PIC X(43).
!          06  RESERVED                    PIC X(157).
!      04  FHM_CAF_ADDR                    REDEFINES DATA.
!          06  ADDR                        PIC X(20).
!          06  ZIP_CDE                     PIC X(9).
!          06  RESERVED                    PIC X(171).
!      04  FHM_PBF_NAM                     REDEFINES DATA.
!          06  CUST_SHORT_NAM              PIC X(40).
!          06  RESERVED                    PIC X(160).
!      04  TLR                             REDEFINES DATA.
!          06  TKN_ID                      PIC X(2).
!          06  CAFI_TKN                    PIC X(198).
!      04  TB                              REDEFINES DATA.
!          06  LAST_TXN_ALWD_CNT           PIC 9(2).
!          06  LAST_TXN_CUST_MAX           PIC 9(4).
!          06  RESERVED                    PIC X(194).
!      04  FHM_CCIF0006                    REDEFINES DATA.
!          06  SEG_ID                      PIC X(4).
!          06  VER                         PIC X(4).
!          06  DISCR_DATA                  PIC X(72).
!          06  RESERVED                    PIC X(120).
  02  SECNDRY_MAC_CDE                     OCCURS 0 TO 1 TIMES DEPENDING ON BS64 PIC X(16)         DISPLAY "MAC".
END.
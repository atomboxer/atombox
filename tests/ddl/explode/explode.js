var console = require("console");
var system  = require("system");

require("ddlgdefs","ddl");   //globals

var XPNET_HDR = require("ddlxpnt","ddl").XPNET_HDR;
var HISO87    = require("ddlhiso","ddl").HISO87;
var BANKNET   = require("ddlbnet","ddl").BANKNET;

var fs = require("fs");
var MAX_SCREEN_WIDTH = 78;

function display(de, charset, offset_idx) 
{    
    //var display = de.index.toString().lpad(' ',3);
    var display = "   ";
    display +="  ";
    display += de.display.rpad(' ',23)+":";
    
    var decoded = de.bytes.decodeToString(charset);

    var dispValue="";

    try {
    if (!/[\x00-\x1F\x80-\xFF]/.test(de.value)&&
	    de.value != undefined) {
	    dispValue += de.value;
    } else if (/[\x00-\x1F\x80-\xFF]/.test(decoded)) {
	    dispValue += de.bytes.decodeToString("hex");
    } else {
	    dispValue += decoded;
    }
    } catch(e) {}

    for (var i=1;i<5;i++) {
	    var num = Number(dispValue.substring(0,i)); 
        if (num == 0)
	        continue;
	    if ( num == de.length - i && de.length > 5) {
	        dispValue = dispValue.substring(i) + " <" + num + ">";
	        break;
	    }
    }
    display += dispValue;
    console.write(display+"\n");
}

function inject(obj, max_lvls, charset, curr_lvl) {
    if (max_lvls == undefined )
	    max_lvls = 2;

    if (curr_lvl == undefined)
	    curr_lvl = 0;

    if (charset == undefined)
	    charset = "ascii";
    
    if (charset != "ascii" &&
	    charset != "ebcdic") {
	    return;
    }
    if (curr_lvl == max_lvls) //stop condition
	    return;

    var skip = false;

    if (obj instanceof Array) {
        if (curr_lvl != 0 && !skip) {
            if (obj[0]["packed"] != undefined) {
                obj[0].packed.connect(function(o) {               
                    display(obj[0], charset);
                });
            }
        }
        
        for (var i in obj[0]) {
	        if (obj[0].hasOwnProperty(i)) {
	            inject(obj[0][i], max_lvls, charset, ++curr_lvl);
	        }
	        curr_lvl--;
        }
    } else {
        if (curr_lvl != 0 && !skip) {
            if (obj["packed"] != undefined) {
                obj.packed.connect(function(o) {
                    if (obj.index!=0) display(obj, charset);
                });
            }
        }
        for (var i in obj) {
	        if (obj.hasOwnProperty(i)) {
	            inject(obj[i], max_lvls, charset, ++curr_lvl);
	        }
	        curr_lvl--;
        }
    }
}

var xpnet_hdr = new XPNET_HDR();
var hiso87    = new HISO87();
var bnet      = new BANKNET();


// hiso87.PRI_BIT_MAP.packed.connect(function(o) {
//     hiso87.PRI_BIT_MAP.value = hiso87.PRI_BIT_MAP.value
//         .toString().toByteArray("hex").decodeToString()+"0000000000000000";
// });

// hiso87.SECNDRY_BIT_MAP[0].packed.connect(function(o) {
//     hiso87.SECNDRY_BIT_MAP[0].value = hiso87.SECNDRY_BIT_MAP[0].value
//         .toString().toByteArray("hex").decodeToString()+"0000000000000000";
// });

//inject(xpnet_hdr,2,"ascii");
//inject(hiso87,2,"ascii");
inject(bnet,3,"ebcdic");

xpnet_hdr.packed.connect(function(o) {
    if (o.recordNum == undefined)
	    o.recordNum = 0;
    else 
	    o.recordNum++;

    var msg = "Rec " + o.recordNum.toString().lpad('0',4); //Displays Rec xxx
    msg += o.sym_source.value.toString().lpad(' ',20)+" ";
    msg += "->  " +o.sym_dest.value.toString().lpad(' ',20)+"  ";
    msg += "Len "+o.len.value.toString().lpad(' ',3)+"  ";

    var dt = new Date.parseTandem(o.time.bytes.toNumber());

    msg += dt.getDate()+"/"+(1+dt.getMonth())+"  ";
    msg += dt.getHours().toString().lpad('0');
    msg +=":"+dt.getMinutes().toString().lpad('0');
    msg +=":"+dt.getSeconds().toString().lpad('0');
    msg +="."+(dt.getMilliseconds()/10).toString().lpad('0');
    console.write(msg+"\n");
});

/////////////////////////////////////////////////////////////////////////////
//                MAIN ENTRY POINT                                        //
////////////////////////////////////////////////////////////////////////////
var filename = "m2b0901g";//"m2g1129a"
////var filename = "m2g1129a";

var stream = fs.open(filename,"rb");

var idx = 0;
var done = false;

var d1 = new Date();
console.time("hello");
while(!done) {
    var len = stream.read(2).readInt16();
    if (len <= 2) {
	    done = true;
	    continue;
    }
    var message = stream.read(len-2);
    xpnet_hdr.pack(message);

    var msg = message.slice(xpnet_hdr.offset_txt.value).toByteArray();

    // if (msg.slice(0,3).decodeToString() == "ISO") {
    //     hiso87.pack(msg);
    // } else 
    if (xpnet_hdr.sym_source.value.toString().match(/.*S.*BNET.*/g).length != 0 ||
        xpnet_hdr.sym_dest.value.toString().match(/.*S.*BNET.*/g).length != 0) {
        //console.dir(xpnet_hdr);
        bnet.pack(msg);
        if (bnet.TYP.value != 800 &&
            bnet.TYP.value != 810 &&
            bnet.TYP.value != 0)
            system.exit(0);
    } else {
        //print(xpnet_hdr.sym_source.value.toString());
    }

    //console.dir(hiso87);
    //system.exit(0);
    //system.exit(0);
    //if (idx++>100)
    //done = true;    
}
console.timeEnd("hello");
print("done has read "+idx)

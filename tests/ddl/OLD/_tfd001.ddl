!Field definition for a two character field that can be either "NO" or "OK" with a default value of "NO"!
DEFINITION FD01 PIC XX DISPLAY "one field" MUST BE "OK", "NO"  VALUE "NO".

!Field definition for two digit field that can be between 50 and 99 with a default value of 50.!
DEFINITION FD02 PIC 99 MUST BE 50 THROUGH 99 VALUE 50.

!Field definition for an email address, validated through a regular expression, and left padded with '_''!
DEFINITION FD03 PIC X(50) MUST BE REGEX "[A-Za-z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}.*", 
                                  REGEX "\s*" LPAD '_' DEFAULT "default@default.com".

* some shit
!asdasd

--group definition in order to be used in js--
DEFINITION Box_FD. 
     02 fd01  TYPE *.    !looks for FD01!
     02 fd02  TYPE FD02. !explicit specifier!
    
     02 email TYPE FD03.
     
     02 alt_email redefines email occurs 5 times.
         04 wtf PIC X(15).

     02 alt_email_r redefines email.
         04 wtf PIC X(10) occurs 5 times.


     02 one.
         04 len   PIC 99.
         04 data  pic X(99) VLENGTH !some shit! DEFAULT "some default data".


         04 red_data     PIC X(10) redefines data.
         04 red_data_alt PIC X(10) redefines data.

         04 em redefines data.
             06 f1 PIC X.
             06 f2 PIC X(10) RPAD 's'.

         04 em_alt redefines data.
             06 am_alt_alt.
                 08 rf1 PIC X(10).

         04 em_arr redefines data.
             06 arr PIC X(2) OCCURS 1 to 10 TIMES depending on len.


END.


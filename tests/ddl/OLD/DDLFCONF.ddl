**FIX2.11  01/02/01  DDLFCONF6001 DDL    BA60DDL  BA04165  A                 ***
!*SEQ0.04  11/01/00  DDLFCONF6000 DDL    BA60DDL    04000                      !
!*SYNC.04  12/09/98  DDLFCONF5300 DDL    BA53DDL    04000                      !
!*SYNC.03  08/28/98  DDLFCONF5300 DDL    BA53DDL    03000                      !
!*SYNC.03  08/22/97  DDLFCONF5100 DDL    BA51DDL    03000                      !
!*SYNC.02  06/25/96  DDLFCONF5100 DDL    BA51DDL    02000                      !
!*SEQ2.00  10/20/92  DDLFCONF5100 DDL    BA51DDL                               !
                                                                       !00000A00
?page "LCONF - Logical Network Configuration File"
?section LCONF-history
?setsection LCONF-history
********************************************************************** !00000A07
*                                                                    * !00000A08
*                               BASE24                               * !00000A09
*                               ------                               * !00000A0A
*                                                                    * !00000A0B
*             DDL for the Logical Network Configuration File         * !00000A0C
*                                                                    * !00000A0D
*                   Proprietary Software Product                     * !00000A0E
*                                                                    * !00000A0F
*                         ACI Worldwide Inc.                         * !00000A0G
*                       330 South 108th Avenue                       * !00000A0H
*                       Omaha, Nebraska  68154                       * !00000A0I
*                           (402) 390-7600                           * !00000A0J
*                                                                    * !00000A0K
*    Copyright by ACI Worldwide Inc. 1992 - 2000                     * !00000A0L
*                                                                    * !00000A0M
*    All Rights Reserved.  No part of this document may be           * !00000A0N
*    reproduced in any manner without the prior written consent of   * !00000A0O
*    ACI Worldwide Inc.  This material is a trade secret and its     * !00000A0P
*    confidentiality is strictly maintained.  Use of any copyright   * !00000A0Q
*    notice does not imply unrestricted or public access to these    * !00000A0R
*    materials.                                                      * !00000A0S
*                                                                    * !00000A0T
*    BASE24 (R) is a registered trademark of ACI Worldwide Inc.      * !00000A0U
*                                                                    * !00000A0V
********************************************************************** !00000A0W
* Record of Changes:                                                   !00000A0X
*                                                                      !00000A0Y
* Date        Person/Emp #                                             !00000A0Z
* ---------   ------------                                             !00000A10
********************************************************************** !00000A11
*             Release 6.0                                            * !00000A12
********************************************************************** !00000A13
* 30NOV2000   DJB/040 CLR/451 jfu/543                                  !00000A14
* Symptom:    Release 6.0 Enhancements                                 !00000A15
* Problem:    None                                                     !00000A16
* Fix:        1) Merged the Classic and ITS versions of the LCONF      !00000A17
*                DDL by adding redefines to the PRO-NAME, ITEM-NAME,   !00000A18
*                FILE-NAME, and TEMPLATE fields.  These redefines      !00000A19
*                represent the ITS names for the fields.               !00000A1A
*             2) Added support for 256 Products.                       !00000A1B
*             3) Added LAST-AFM to allow multiple users to have the    !00000A1C
*                file open so the file can be maintained with a        !00000A1D
*                Pathway Requester/Server. Left the old                !00000A1E
*                LAST-CHNG-TIME to minimize changes to other code.     !00000A1F
*             4) Replaced the reference of the SYM-NAME definition     !00000A1G
*                with PIC X(16).  This definition is not available     !00000A1H
*                in the ITS kernel DDLs.  It is SYM-NAM-DEF in the     !00000A1I
*                ITS kernel DDLs.                                      !00000A1J
*             5) Replaced the references of the FNAME definition       !00000A1K
*                with PIC X(16).  This definition is not available     !00000A1L
*                in the ITS kernel DDLs.  It is FNAME-EXTRN-DEF in     !00000A1M
*                the ITS kernel DDLs.                                  !00000A1N
*             6) The use of LAST-AFM definition was hard-coded in      !00000A1O
*                this DDL.  It is the LAST-FM definition in the        !00000A1P
*                ITS kernel DDLs.                                      !00000A1Q
* Dependency: Restore Release 6.0 files, modify the appropriate        !00000A1R
*             CUSTMACS flags, and run MAKE.                            !00000A1S
* Reference:  WO #971118-2 (PITABLE Expansion)                         !00000A1T
*********************************************************************  !00000A1U
*####################################################################  !00000A1V
*#          Logical Network Congiguration File                      #  !00000A1W
*####################################################################  !00000A1X
*                                                                      !00000A1Y
*             The Logical Network Configuration File (LCONF) is an     !00000A1Z
*             key-sequenced file which contains one record for each    !00000A20
*             ASSIGN or PARAMETER used by the application processes    !00000A21
*             and Pathway.                                             !00000A22
*                                                                      !00000A23
*             Usage:                                                   !00000A24
*                                                                      !00000A25
*             File maintenance     i/o       RANDOM (Primary) shared   !00000A26
*             Application process  Read Only RANDOM (Primary) shared   !00000A27
*                                                                      !00000A28
                                                                       !00000A29
                                                                       !00009A00
                                                                       !00009A01
                                                                       !00009A02
**********************************************************************!!00026
?comments
?PAGE "LOGICAL NETWORK CONFIGURATION FILE"
?section LCONF-KEY
?setsection LCONF-KEY
                                                                       !00026A06
                                                                       !00029
*              This definition constitutes the primary key to the     !!00030
*              LCONF.                                                 !!00031
DEF LCONF-KEY.                                                         !00032
                                                                       !00033
*              The type of LCONF item contained within a given record.!!00034
*              A = Assign record                                      !!00035
*              P = Param record                                       !!00036
                                                                       !00037
 02  ITEM-TYP                     PIC X(1).                            !00038
                                                                       !00039
*              This field is not used.                                !!00040
                                                                       !00041
 02  USER-FLD1                    PIC X(1).                            !00042
                                                                       !00043
*              The symbolic name of the process using a given assign  !!00044
*              or param, if needed for process-specific assigns or    !!00045
*              params.  If a process name is not entered, the default !!00046
*              is 16 '*'.                                             !!00047
                                                                       !00048
                                                                       !00049A00
 02  PRO-NAME                     PIC X(16).                           !00049A01
                                                                       !00049A02
*              The ITS name for the previous field.                    !00049A03
                                                                       !00049A04
 02  PRO-NAM                      REDEFINES PRO-NAME                   !00049A05
                                  PIC X(16).                           !00049A06
                                                                       !00049A07
                                                                       !00050
*              THe name of the assign or param ( for example, POS-PTLF!!00051
*              or LOGICAL-NET.                                        !!00052
*                                                                     !!00053
 02 ITEM-NAME                     PIC X(32).                           !00054
                                                                       !00055
                                                                       !00055A00
*              The ITS name for the previous field.                    !00055A01
                                                                       !00055A02
 02 ITEM-NAM                      REDEFINES ITEM-NAME                  !00055A03
                                  PIC X(32).                           !00055A04
                                                                       !00055A05
                                                                       !00056
                                                                       !00057
END                                                                    !00058
                                                                       !00059
                                                                       !00059A00
?section LCONF
?setsection LCONF
                                                                       !00059A05
*                                                                     !!00060
*              The Logical Network Configuration File (LCONF) contains!!00061
*              information that links physical file names to file     !!00062
*              names as they are referenced in BASE24 code and        !!00063
*              information that sets processing values for a logical  !!00064
*              network.                                               !!00065
*                                                                     !!00066
*              One LCONF exists for each logical network.  An LCONF is!!00067
*              created via a data entry program.  Both the LCONF and  !!00068
*              its data entry program are located on the execution    !!00069
*              subvolume.                                             !!00070
*                                                                     !!00071
*              All BASE24 processes and file servers access the LCONF !!00072
*              when they initialize.  For complete information on the !!00073
*              LCONF, please reference the "BASE24 Logical Network    !!00074
*              Configuration File Manual."                            !!00075
*                                                                     !!00076
                                                                       !00077
                                                                       !00078
                                                                       !00079A00
RECORD LCONF.                     FILE IS "LCONF" KEY-SEQUENCED        !00079A01
                                                  CODE 2852.           !00079A02
                                                                       !00079A03
                                                                       !00080
*              The primary key is ITEM-TYP/PRO-NAME/ITEM-NAME for all !!00081
*              types of information.                                  !!00082
                                                                       !00083
 02 PRIKEY                        TYPE LCONF-KEY        KEYTAG 0.      !00084
                                                                       !00085
                                                                       !00086
*              A byte map indicating which product(s) use this        !!00087
*              specific assign or param or if the assign or param is  !!00088
*              Base.                                                  !!00089
*                                                                     !!00090
                                                                       !00091
 02 PROD-IND                      PIC X(32).                           !00092
                                                                       !00093
                                                                       !00094
                                                                       !00095A00
*              The user-entered comments providing additional         !
                                                                       !00095A03
*              information about the assign or param.                 !!00096
*                                                                     !!00097
*              Each record can contain up to 256 bytes of explanatory !!00098
*              information using 4 text lines of 64 bytes each.       !!00099
*              Information in these fields are left-justified, and    !!00100
*              blank filled.                                          !!00101
                                                                       !00102
 02 COMMENTS                      OCCURS 4 TIMES.                      !00103
                                                                       !00104
    04 TXT-LINE                   PIC X(64).                           !00105
                                                                       !00106
                                                                       !00107
*              The timestamp for the latest maintenance to this       !!00108
*              record.                                                !!00109
                                                                       !00110
 02 LAST-CHNG-TIME                TYPE BINARY OCCURS 3 TIMES.          !00111
                                                                       !00112
                                                                       !00113
*              The re-define area for the two specific types of       !!00114
*              information which can be found within the file.        !!00115
                                                                       !00116
 02 LCONF-ITEM.                                                        !00117
                                                                       !00118
    04  ITEM-TXT                  PIC X(120).                          !00119
                                                                       !00120
                                                                       !00121
*              The following fields contain assign information.       !!00122
                                                                       !00123
 02 ASSIGN-MSG                    REDEFINES LCONF-ITEM.                !00124
                                                                       !00125
*              This field contains the target or physical file name   !!00126
*              of the assign (e.g.: $DATA1.PRO1DATA.TDF).             !!00127
                                                                       !00128
                                                                       !00129A00
   04 FILE-NAME                   PIC X(35).                           !00129A01
                                                                       !00129A02
*              The ITS name for the previous field.                    !00129A03
                                                                       !00129A04
   04 FNAME                       REDEFINES FILE-NAME                  !00129A05
                                  PIC X(35).                           !00129A06
                                                                       !00129A07
                                                                       !00130
                                                                       !00131A00
   04 TEMPLATE                    PIC X(35).                           !00131A01
                                                                       !00131A02
*              The ITS name for the previous field.                    !00131A03
                                                                       !00131A04
   04 TPLT                        REDEFINES TEMPLATE                   !00131A05
                                  PIC X(35).                           !00131A06
                                                                       !00131A07
                                                                       !00132
*              This field is not used.                                !!00133
                                                                       !00134
   04 USER-FIELD                  PIC X(35).                           !00135
                                                                       !00136
*              This field is not used.                                !!00137
                                                                       !00138
   04 USER-FLD2                   PIC X(15).                           !00139
                                                                       !00140
*              The following fields contain param information.        !!00141
                                                                       !00142
 02 PARAM-MSG                     REDEFINES LCONF-ITEM.                !00143
                                                                       !00144
*              The length of the param definition.                    !!00145
                                                                       !00146
   04 PLGTH                       TYPE BINARY 16.                      !00147
                                                                       !00148
*              The actual parameter text or value.                    !!00149
                                                                       !00150
   04 PTXT                        PIC X(70).                           !00151
                                                                       !00152
*              This field is not used.                                !!00153
                                                                       !00154
   04 USER-FLD3                   PIC X(48).                           !00155
                                                                       !00156
                                                                       !00156A00
*              A byte map for PI indicators 33 - 256 indicating which  !00156A01
*              product(s) use this specific assign or param or if the  !00156A02
*              assign or param is base.                                !00156A03
                                                                       !00156A04
 02 PROD-IND-ADNL                 PIC X(224).                          !00156A05
                                                                       !00156A06
*              This field is not used.                                 !00156A07
                                                                       !00156A08
 02 USER-FLD4                     PIC X(6).                            !00156A09
                                                                       !00156A0A
*              This field describes the last on-line file maintenance  !00156A0B
*              update applied to this record. This includes the user   !00156A0C
*              who did the update, the time at which it was done, the  !00156A0D
*              type of update and the terminal originating the update  !00156A0E
*              transaction.                                            !00156A0F
                                                                       !00156A0G
 02 LAST-AFM.                                                          !00156A0H
                                                                       !00156A0I
*              This structure contains the pertinent data regarding    !00156A0J
*              any AFM file maintenance activity.                      !00156A0K
                                                                       !00156A0L
    04 FM.                                                             !00156A0M
                                                                       !00156A0N
*              Julian timestamp (date/time) of the last AFM function   !00156A0O
*              performed to the data record.                           !00156A0P
                                                                       !00156A0Q
       06 AFM-TS                  TYPE BINARY 64.                      !00156A0R
                                                                       !00156A0S
*              Record maintenance function:                            !00156A0T
*                                                                      !00156A0U
*              0 - Add                                                 !00156A0V
*              1 - Image "before" modify                               !00156A0W
*              2 - Image "after" modify                                !00156A0X
*              3 - Purge                                               !00156A0Y
                                                                       !00156A0Z
       06 UPDT-TYP                TYPE BINARY 16.                      !00156A10
                                                                       !00156A11
*              Only used by Pathway servers.  Non-Pathway servers      !00156A12
*              default USER-ID to zeros.                               !00156A13
                                                                       !00156A14
       06 USER-ID.                                                     !00156A15
                                                                       !00156A16
*              The group number of the operator that performed the     !00156A17
*              last file maintenance application.  This value is       !00156A18
*              taken from the information given when the operator      !00156A19
*              last logged on to BASE24.                               !00156A1A
                                                                       !00156A1B
          08 GRP-NUM              TYPE BINARY 16.                      !00156A1C
                                                                       !00156A1D
*              The user number of the operator that performed the      !00156A1E
*              last file maintenance application.  This value is       !00156A1F
*              taken from the information given when the operator      !00156A1G
*              last logged on to BASE24.                               !00156A1H
                                                                       !00156A1I
          08 USER-NUM             TYPE BINARY 32.                      !00156A1J
                                                                       !00156A1K
*              This field contains the terminal ID where the           !00156A1L
*              record maintenance occurred. Used only by Pathway       !00156A1M
*              servers. Non-Pathway servers default TERM-NAM           !00156A1N
*              field to blanks.                                        !00156A1O
                                                                       !00156A1P
       06 TERM-NAM                PIC X(24).                           !00156A1Q
                                                                       !00156A1R
                                                                       !00156A1S
END.                                                                   !00157
                                                                       !00157A00
?setsection
                                                                       !00157A03

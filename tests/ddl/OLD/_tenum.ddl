
 DEF EMV_LTAGS TYPE ENUM BEGIN.
     89  _  VALUE "5F2A" AS "Tran Currency Code".
     89  _  VALUE "9F02" AS "Amt Auth'd - Numeric".
     89  _  VALUE "9F03" AS "Amt Other - Numeric".
     89  _  VALUE "9F1A" AS "Term Country Code".
     89  _  VALUE "5F34" AS "PAN Sequence Num".
     89  _  VALUE "9F37" AS "Unpredict Number".
     89  _  VALUE "9F36" AS "App Tran Counter ATC".
     89  _  VALUE "9F10" AS "Iss App Data     IAD".
     89  _  VALUE "9F26" AS "App Cryptogram".
     89  _  VALUE "9F09" AS "App Version Number".
     89  _  VALUE "9F1E" AS "I/face Dev Serial Nr".
     89  _  VALUE "9F27" AS "Cgram Info Data  CID".
     89  _  VALUE "9F33" AS "Term Capabilities".
     89  _  VALUE "9F34" AS "C/hldrVerifMethRslts".
     89  _  VALUE "9F35" AS "Terminal Type".
     89  _  VALUE "9F41" AS "Tran Seq Counter".
     89  _  VALUE "9F53" AS "E'pay Category Code".
 END.

 DEF EMV_STAGS TYPE ENUM BEGIN.
     89  _  VALUE "71" AS "Issuer Script".
     89  _  VALUE "72" AS "Issuer Script".
     89  _  VALUE "91" AS "ARPC".
     89  _  VALUE "95" AS "Term Verif Rslts TVR".
     89  _  VALUE "9A" AS "Tran Date".
     89  _  VALUE "9C" AS "Tran Type".
     89  _  VALUE "82" AS "App Ichg Profile AIP".
     89  _  VALUE "84" AS "Dedicated File (DF)".
 END.

DEFINITION HPDH_MSG.
  02 header  PIC X(10) PACKED-DECIMAL.

  02 MTI     PIC 9(4)  PACKED-DECIMAL.

  02 bitmap1 PIC 9(16) PACKED-DECIMAL ISOBITMAP DISPLAY "Bitmap".
  02 NOT_USED_1                           PIC 9(999).                                                !DE001

  02  PAN                                           DISPLAY "PAN".                                   !DE002
      04  LEN                             PIC 9(2)  PACKED-DECIMAL.
      04  DATA                            PIC X(20) PACKED-DECIMAL VLENGTH.

  02  PROC_CDE                            PIC X(6)  PACKED-DECIMAL DISPLAY "Processing Code".        !DE003
  02  TRAN_AMT                            PIC X(12) PACKED-DECIMAL DISPLAY "Transaction Amount".     !DE004

  02  NOT_USED_5                          PIC 9(999).
  02  NOT_USED_6                          PIC 9(999).
  02  NOT_USED_7                          PIC 9(999).
  02  NOT_USED_8                          PIC 9(999).
  02  NOT_USED_9                          PIC 9(999).
  02  NOT_USED_10                         PIC 9(999).

  02  STAN                                PIC X(6)  PACKED-DECIMAL DISPLAY "STAN        ".

  02  LOCAL_TIM                           PIC X(6)  PACKED-DECIMAL DISPLAY "Local Time".             !DE012
  02  TRAN_DAT                            PIC X(4)  PACKED-DECIMAL DISPLAY "Local Date".             !DE013
  02  EXP_DAT                             PIC X(4)  PACKED-DECIMAL DISPLAY "Expiry Date".            !DE014

  02  NOT_USED_15                         PIC 9(999).
  02  NOT_USED_16                         PIC 9(999).
  02  NOT_USED_17                         PIC 9(999).
  02  NOT_USED_18                         PIC 9(999).
  02  NOT_USED_19                         PIC 9(999).
  02  NOT_USED_20                         PIC 9(999).
  02  NOT_USED_21                         PIC 9(999).

  02  ENTRY_MDE DISPLAY "Entry Mode"      PIC X(3) PACKED-DECIMAL.                                   !DE022.

  02  NOT_USED_23                         PIC 9(999).

  02  NETW_INTL_ID                        PIC X(3)  PACKED-DECIMAL DISPLAY "Network Intl ID".        !DE024

  02  POS_COND                            PIC X(2)  PACKED-DECIMAL DISPLAY "POS Condition".          !DE025

  02  NOT_USED_26                         PIC 9(999).
  02  NOT_USED_27                         PIC 9(999).
  02  NOT_USED_28                         PIC 9(999).
  02  NOT_USED_28                         PIC 9(999).
  02  NOT_USED_30                         PIC 9(999).
  02  NOT_USED_31                         PIC 9(999).
  02  NOT_USED_32                         PIC 9(999).
  02  NOT_USED_33                         PIC 9(999).
  02  NOT_USED_34                         PIC 9(999).

  02  TRACK2                                        DISPLAY "Track 2".                               !DE035
      04  LEN                             PIC 9(2)  PACKED-DECIMAL.
      04  DATA                            PIC X(37) PACKED-DECIMAL VLENGTH.

  02  NOT_USED_36                         PIC 9(999).

  02  RETRVL_REF_NUM                      PIC X(12) DISPLAY "RRN".                                   !DE037

  02  AUTH_ID_RESP                        PIC X(6)  DISPLAY "Auth ID Resp".                          !DE038
  02  RESP_CDE                            PIC X(2)  DISPLAY "Response Code".                         !DE039

  02  NOT_USED_40                         PIC 9(999).

  02  TERM_ID                             PIC X(8)  DISPLAY "Card Accptr Term ID".                   !DE041
  02  CRD_ACCPT_ID_CDE                    PIC X(15) DISPLAY "Card Accptr ID".                        !DE042
  02  CRD_ACCPT_NAME_LOC                  PIC X(40) DISPLAY "Card Accptr Name/Locn".                 !DE043

  02  NOT_USED_44                         PIC 9(999).

  02  TRACK1                                        DISPLAY "Track 1".                               !DE045
      04  LEN                             PIC 9(2) PACKED-DECIMAL.
      04  DATA                            PIC X(76) VLENGTH.

  02  NOT_USED_46                         PIC X(999).
  02  NOT_USED_47                         PIC X(999).

  02  ADD_DATA_PRVT                                DISPLAY "Additional Data".                        !DE048
       04  LEN                            PIC 9(3) PACKED-DECIMAL.
       04  DATA                           PIC X(999) VLENGTH.

  02  NOT_USED_49                         PIC 9(999).
  02  NOT_USED_50                         PIC 9(999).
  02  NOT_USED_51                         PIC 9(999).

  02  PIN_DATA                            PIC X(16) PACKED-DECIMAL          DISPLAY "Pin Block".     !DE052

  02  SEC_INFO                            PIC 9(16) PACKED-DECIMAL.                                  !DE053

  02  ADD_AMTS                                            DISPLAY "Additional Amounts".              !DE054
      04  LEN                             PIC 9(3) PACKED-DECIMAL.
      04  DATA                            PIC X(999) VLENGTH.

  02  EMV                                                 DISPLAY "EMV DATA".                        !DE055
      04  LEN                             PIC 9(3) PACKED-DECIMAL.
      04  EMV_DATA                        PIC X(999) VLENGTH.


      04  TLV                             REDEFINES EMV_DATA OCCURS 30 TIMES.
          06  TAG.
              08  KEY CHOICE.
                 10   C1 PIC  X(4)    PACKED-DECIMAL MUST BE EMV_LTAGS.
                 10   C2 PIC  X(2)    PACKED-DECIMAL MUST BE EMV_STAGS.


              08  TLEN      PIC X(2) PACKED-DECIMAL.
              08  TVALUE    PIC X(999) VLENGTH.
              08  NVALUE REDEFINES TVALUE PIC 9(4) PACKED-DECIMAL.

  02  NOT_USED_56                         PIC 9(999).
  02  NOT_USED_57                         PIC 9(999).
  02  NOT_USED_58                         PIC 9(999).
  02  NOT_USED_59                         PIC 9(999).


  02  ADD_DATA_PRIV60                                      DISPLAY "Additional Data Priv1".          !DE060
      04  DATA CHOICE.
          08 BATCH.
             10  LEN  PIC X(4) PACKED-DECIMAL MUST BE "0006".
             10  DATA PIC 9(6) DISPLAY "Batch Number".

          08 DEFAULT_VALUE.
             10  LEN                              PIC 9(4) PACKED-DECIMAL.
             10  DATA                             PIC X(999) VLENGTH.

  02  ADD_DATA_PRIV61                                      DISPLAY "Additional Data Priv2".          !DE061
      04  LEN                              PIC 9(3) PACKED-DECIMAL.
      04  DATA                             PIC X(999) VLENGTH.

  02  ADD_DATA_PRIV62                                      DISPLAY "Additional Data Priv3".          !DE062
      04  LEN                              PIC 9(3) PACKED-DECIMAL.
      04  DATA                             PIC X(999) VLENGTH.

      04  DETAILS                          CHOICE REDEFINES DATA.
           06  NMM DISPLAY "Network Management".
               08  PIN  PIC X(16) PACKED DISPLAY "Working PIN".
               08  MAC  PIC X(16) PACKED DISPLAY "Working MAC" MUST BE "0000000000000000".
               08  rcp1             PIC X(16) DISPLAY "Receipt line 1".
               08  rcp2             PIC X(16) DISPLAY "Receipt line 2".
               08  mrch_nam         PIC X(16) DISPLAY "Merchant name".
          06   INV     DISPLAY "Invoice Number".
               08  invoice          PIC X(6)  DISPLAY "Invoice Number".


  02  ADD_DATA_PRIV63                                      DISPLAY "Additional Data Priv4".          !DE063
      04  LEN                              PIC 9(3) PACKED-DECIMAL.
      04  DATA                             PIC X(999) VLENGTH.

      04  DETAILS                          CHOICE REDEFINES DATA OCCURS 15 TIMES.
           06  NMM.
               08  WTF PIC X(2)  MUST BE "KP".
               08  PIN PIC X(32) PACKED-DECIMAL DISPLAY "DOUBLE PIN KEY".

           06  T15.
               08  LEN      PIC X(4) PACKED-DECIMAL MUST BE "0003" DISPLAY "TABLE 15".
               08  TBL      PIC X(2) MUST BE "15" DISPLAY "Table15.Lvl II Comm crd ind.".
               08  CDE1     PIC X(1)              DISPLAY "Request/Resp comm card ind.".

           06  T16.
               08  LEN      PIC X(4) PACKED-DECIMAL DISPLAY "TABLE 16".
               08  TBL      PIC X(2) MUST BE "16" DISPLAY "Table16. CVV2 data".
               08  CVV2     PIC X(6)              DISPLAY "Request/Resp CVV2".

           06  T20.
               08  LEN      PIC X(4) PACKED-DECIMAL MUST BE "0026" DISPLAY "TABLE 20".
               08  TBL      PIC X(2) MUST BE "20" DISPLAY "Table20.Payment 2000".
               08  IND      PIC X(1)              DISPLAY "Indicator (by ACQ)".
               08  TRAN_ID  PIC X(15)             DISPLAY "Tran identifier by VISA".
               08  VAL_CDE  PIC X(4)              DISPLAY "Validation code by VISA".
               08  RSP_CDE  PIC X(2)              DISPLAY "Visa Response Code (by VISA)".
               08  ENT_MDE  PIC X(2)              DISPLAY "Entry Mode (from ACQ to Visa)".

           06  T48.
               08  LEN      PIC X(4) PACKED-DECIMAL MUST BE "0026" DISPLAY "TABLE 48".
               08  TBL      PIC X(2) MUST BE "48" DISPLAY "Table48.EBT Response Data".
               08  FOOD_BAL PIC X(8) DISPLAY "EBT Food Balance".
               08  CASH_BAL PIC X(8) DISPLAY "EBT Cash Balance".
!               08  CASE     PIC X(8) DISPLAY "Case number".

           06  T55.
               08  LEN      PIC X(4) PACKED-DECIMAL DISPLAY "TABLE 55".
               08  TBL      PIC X(2) MUST BE "55" DISPLAY "Table55. Addr verification".
               08  MATCH1   PIC X(1)              DISPLAY "Addr match".
               08  MATCH2   PIC X(1)              DISPLAY "Zip cde match".
               08  REST     PIC X(1)              DISPLAY "AVS resp cde".

!SETTLEMENT 0500!
          06   TOTALS.
               08  CAP_SALES  PIC 9(3)  MUST BE 0 THROUGH 999 DISPLAY "Captured sales count".
               08  TOT_SALES  PIC 9(12) DISPLAY "Sales amount".
               08  CAP_REFUND PIC 9(3)  MUST BE 0 THROUGH 999 DISPLAY "Refund count".
               08  TOT_REFUND PIC 9(12) DISPLAY "Refund amount".
               08  CAP_DEBIT  PIC 9(3)  MUST BE 0 THROUGH 999 DISPLAY "Debit sales count".
               08  TOT_DEBIT  PIC 9(12) DISPLAY "Sales amount".
               08  CAP_REF    PIC 9(3)  MUST BE 0 THROUGH 999 DISPLAY "Refund count".
               08  TOT_REF    PIC 9(12) DISPLAY "Refund amount".
               08  OPTIONS CHOICE.
                   10 AUTH_AND_REF.
                       12 AUT_SALES  PIC 9(3)  DISPLAY "Opt Auth Sales Count".
                       12 TOT_SALES  PIC 9(12) DISPLAY "Opt Sales amount".
                       12 REF_SALES  PIC 9(3)  DISPLAY "Opt Refund count".
                       12 REF_SALES  PIC 9(12) DISPLAY "Opt Refund amount".

                   10 AUTH.
                       12 AUT_SALES  PIC 9(3)  DISPLAY "Auth Sales Count".
                       12 TOT_SALES  PIC 9(12) DISPLAY "Sales amount".

!ADDITIONAL DATA!
          06   ADD_DATA CHOICE.
              08   DATA  PIC X(40) DISPLAY "Rest of the data".


  02  MAC                                  PIC X(16) PACKED-DECIMAL.                                 !DE64

END.

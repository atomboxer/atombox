var assert = require ("assert");
var console = require("console");
var system = require("system");
var fs = require("fs");


exports.testVLENDelimit = function () {
    var CSV = require("ddltest",".","ddl").CSV;
    var csv_row = new CSV();
    //console.dir(csv_row);
    assert.ok(csv_row.length != 0);

    assert.equal(csv_row.BLA.f1.length, 20);
    assert.equal(csv_row.BLA.f2.length, 20);
}

exports.testVLEN = function() {
    var VLEN = require("ddltest",".","ddl").VLEN;
    var vlen_row = new VLEN();
    assert.ok(vlen_row.length != 0);


    assert.equal(vlen_row.A.ll.value,0);
    assert.equal(vlen_row.A.val.bytes.decodeToString("hex"),'');

    assert.equal(vlen_row.B.ll.value, 13);
    assert.equal(vlen_row.B.ll.bytes.decodeToString("hex"),"0000000d");
    assert.equal(vlen_row.B.bytes.length, 4+13);

    assert.equal(vlen_row.C.ll.value, 10);
    assert.equal(vlen_row.C.ll.bytes.length, 3);
    assert.equal(vlen_row.C.ll.bytes.decodeToString(), "010");

    assert.equal(vlen_row.C.val.value,"          ");
    assert.equal(vlen_row.C.val.bytes.decodeToString(),"          ");
    
    assert.equal(vlen_row.D.ll.value,0);
    assert.equal(vlen_row.D.val.value,0);
    assert.equal(vlen_row.D.ll.bytes.decodeToString(),"000");
    assert.equal(vlen_row.D.val.bytes.decodeToString(),"");

    assert.equal(vlen_row.E.ll.bytes.decodeToString(),"003");
    assert.equal(vlen_row.E.ll.value,3);
    assert.equal(vlen_row.E.val.value, 0);
    assert.equal(vlen_row.E.val.length, 3);
    assert.equal(vlen_row.E.val.bytes.decodeToString(),"000");

    assert.equal(vlen_row.F.ll.bytes.decodeToString(),"003");
    assert.equal(vlen_row.F.ll.value, 3);
    assert.equal(vlen_row.F.val.value, 123)
    assert.equal(vlen_row.F.val.length, 3)
    assert.equal(vlen_row.F.val.bytes.decodeToString(), "123")

    assert.equal(vlen_row.G.ll.bytes.decodeToString("hex"), "13");
    assert.equal(vlen_row.G.ll.value, 13);
    assert.equal(vlen_row.G.val.value, "             ");
    assert.equal(vlen_row.G.ll.bytes.length, 1);

    assert.equal(vlen_row.H.ll.bytes.decodeToString("hex"), "20");
    assert.equal(vlen_row.H.ll.value, 20);
    assert.equal(vlen_row.H.val.value, "ANA ARE MERE BLA BLA");
    assert.equal(vlen_row.H.ll.bytes.length, 1);
}

exports.testBitmaps = function() {
    var BITMAP = require("ddltest",".","ddl").BITMAPS;
    var row = new BITMAP();
    assert.ok(row.length != 0);
    var msg = new ByteArray("B6A0","ascii"); //1011011010100000

    //
    //  Hex bitmap
    //
    row.BOX1.pack(msg.concat("ABCDEFGHIJKLMNOPQRS".toByteArray("ascii")));
    assert.equal(row.BOX1.DE[0].value, 'X');
    assert.equal(row.BOX1.DE[1].value, 'A');
    assert.equal(row.BOX1.DE[2].value, 'B');
    assert.equal(row.BOX1.DE[3].value, 'X');
    assert.equal(row.BOX1.DE[4].value, 'C');
    assert.equal(row.BOX1.DE7.value, 'D');  
    assert.equal(row.BOX1.DE8.value, 'x');      
    assert.equal(row.BOX1.SC[0].value, 'E');   
    assert.equal(row.BOX1.SC[1].value, 's');   
    assert.equal(row.BOX1.SC[2].value, 'F');
    
    //
    // Packed bitmap
    //
    var msg = new ByteArray("B6A0","hex"); //1011011010100000
    row.BOX2.pack(msg.concat("ABCDEFGHIJKLMNOPQRS".toByteArray("ascii")));

    assert.equal(row.BOX2.DE[0].value, 'X');
    assert.equal(row.BOX2.DE[1].value, 'A');
    assert.equal(row.BOX2.DE[2].value, 'B');
    assert.equal(row.BOX2.DE[3].value, 'X');
    assert.equal(row.BOX2.DE[4].value, 'C');
    assert.equal(row.BOX2.DE7.value, 'D');  
    assert.equal(row.BOX2.DE8.value, 'x');      
    assert.equal(row.BOX2.SC[0].value, 'E');   
    assert.equal(row.BOX2.SC[1].value, 's');   
    assert.equal(row.BOX2.SC[2].value, 'F');

}

exports.testFIXED = function() {
    var FIX = require("ddltest",".","ddl").FIX;
    var FIXEBC = require("ddltest",".","ddl").FIXEBC;

    var fix_row = new FIX();
    var fix_row_ebc = new FIXEBC();

    assert.ok(fix_row.length != 0);
    assert.equal(fix_row.fix1.value,"IS ");
    assert.equal(fix_row.fix2.value,"0IS");

    assert.equal(fix_row_ebc.ebcdic1.bytes.decodeToString("ebcdic"), "THIS      ");
    assert.deepEqual(fix_row_ebc.ebcdic1.bytes.get(2), "THIS      ".toByteArray("ebcdic").get(2));
    assert.equal(fix_row_ebc.ebcdic1.value, "THIS      ");
    //console.dir(fix_row_ebc);
}

exports.testOccurs = function() {
    
}


if (require.main == module.id)
    require("test").run(exports);

//file://tfd0001.js
var console = require("console");
var Box_FD = require('./_tfd001.ddl').Box_FD;    //load

console.log("--------BEFORE NEW----------------");
var box  = new Box_FD();
var box2 = new Box_FD();

injectChangeNotifier(box2);

console.log("----------------------------------");

//console.assert(box.fd01.value == "NO");         // the default
//console.assert(box.fd01.display == "one field");

// box.fd01.value = "  ";
// console.assert(box.state == WARNING_STATE);     // Warning state.

// box.fd01.value = "OK";
// console.assert(box.fd01.value == "OK"); 

// console.assert(box.state == OK_STATE);          // OK is still acceptable

// console.assert(box.fd02.value == 50);

// box.fd02.value = 49;
// console.assert(box.state == WARNING_STATE);    // 49 is less than 50

// box.fd02.value = 55;
// console.assert(box.state == OK_STATE);

box.email.value = "supasdport@ATOMBOX.org";
// console.assert(box.state == OK_STATE);            //the adddress is validated
// console.assert(box.email.value == "_______________________________support@ATOMBOX.org");

// box.one.len.value = 5;
// = "other@bla.bl"

//x.one.em.f1.value = '_';
//box.one.em.f2.value = 'jjjjj............';
//console.dir(box);

//box.pack("OK60__________________________________call@ATOMBOX.org30Xaaaaaaaaaaaaaaaabbbbbbbaaaaaaaaaaaaaaaaaaaaaabbaaaaaa".toByteArray());

//box.one.data.value = "this is changed data of length 33";
//box.one.em.f2.value = "KKKKKKKKKKKKKKKKKKKaaaKKKKKKKKKK";
//box.one.em_arr.arr[0].value = "??";
//box.one.em_alt.am_alt_alt.rf1.value = "What is this  bla bla";
box.one.len.value = 10;
box.one.red_data.value = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
//box.one.red_data_alt.value = "xxx";
//console.dir(box);
//console.dir(box2);

//box.pack("XX32XXXXXXXXXXXXXXXXYYYYYYYYYYYYYYYY".toByteArray());
//box.ZZROW[0].OVERRIDE_DEST.value = "asd";
//box.ZZROW[0].xx.value = 123123;
//box.SRV_INFO.value = "JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ".toByteArray().decodeToString();
//console.write(box.one.len.bytes.decodeToString("hex"));
console.dir(box);

////////////////////////////////////////////////////////////////////////////
function injectChangeNotifier(obj, curr_lvl) {

    if (curr_lvl == undefined)
        curr_lvl = 0;

    var skip = false;

    // if (obj != undefined &&
    //     obj.prototype != undefined)
    //     skip = true

    if (!skip) {
        if (obj.__proto__.hasOwnProperty("changed")) {
            obj.changed.connect(obj, function() {
                if (obj.hasOwnProperty("value"))
                    print("!changed! "+obj.name + '    ->:'+obj.bytes.decodeToString());
            });
        }

        if (obj.__proto__.hasOwnProperty("packed")) {
            obj.packed.connect(obj, function() {
                if (obj.hasOwnProperty("value"))
                    print("====!packed! "+obj.name + '    ->:'+obj.bytes.decodeToString());
            });
        }

    }

    for (var i in obj) {
        if (obj.hasOwnProperty(i)) {
            injectChangeNotifier(obj[i], ++curr_lvl);
        }
        curr_lvl--;
    }
}

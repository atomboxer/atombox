
DEF numeric-pictures.
     02 num-int PIC 9999(3)9(2) COMP.
END.

DEF binary-pictures .
    -- 02 binary-int PIC 9 (4) COMP .
    -- 02 binary-int-s PIC S9 (4) COMP .
    -- 02 binary-int_2 PIC 9 (5) COMP.
    -- 02 binary-int2-s PIC S9 (5) COMP .
    -- 02 binary-int4 PIC 9 (10) COMP .
    -- 02 binary-int4-s PIC S9(10) COMP .
END.

DEF ascii-pictures.
    -- 02 alpha-field PIC A (10) .
    -- 02 alphanum-2 PIC X (10) .
    -- 02 alphanum-1 PIC AAX (4) 9 (4) .
    -- 02 nat-field PIC N (5) .
    -- 02 unsigned PIC 9 (5) .
    -- 02 signed-1 PIC S9 (5) .
    -- 02 signed-1 PIC 9 (5)S .
    -- 02 signed-2 PIC T9 (5) .
    -- 02 signed-3 PIC 9 (5)T .
    -- 02 imp-decimal PIC 9 (3) V9 (2) .
    -- 02 df1 PIC 9(4) USAGE IS COMP-3.
END .

DEF type-clause-example.
--    02 chr TYPE CHARACTER 8.                !8 alpha!
--    02 bin-16 TYPE BINARY 16 .              !signed integer!
--    02 bin-16-u TYPE BINARY 16 UNSIGNED.    !unsigned integer!
--    02 bin-16-s TYPE BINARY 16,2.           !signed integer, two decimal positions!
--    02 bin-32 TYPE BINARY 32.               !signed double integer!
--    02 bin-64 TYPE BINARY 64,16.            !4 byte integer, 16 decimals!
--    02 flt TYPE FLOAT.                      !32 real number!
--    02 flt-64 TYPE FLOAT 64.                !64 real number!
END

DEF bitmap-pictures.
    --02 bits-8 TYPE BIT 8.
    --02 bits-3 Type BIT 3.
    --02 bits-2 Type BIT 2.
    --02 bits-10 Type BIT 10.
    --02 bits-1 Type BIT 1.
END.
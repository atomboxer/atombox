
definition box.
    02 header PIC X(9) VLENGTH DELIMITER "#".
    02 prefix PIC X(9) VLENGTH DELIMITER "#".

    02 one_of CHOICE.
        04 fb1.
            06 fc1 PIC X MUST BE "Y".
            06 msg PIC X(20) DEFAULT "This is OK".

        04 fb2.
            06 fc2 PIC X MUST BE "N".
            06 msg PIC X(20) DEFAULT "This is NOK".

        04 fb3.
            06 fc3 PIC X.
            06 msg PIC X(20) DEFAULT "This is default".

end.
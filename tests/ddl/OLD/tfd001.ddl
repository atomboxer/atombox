
!Field definition for: !
-- three digit field representing a generic age
-- left padded with 0
-- default value of 18
DEFINITION Age PIC 999 DISPLAY "age" LPAD '0' DEFAULT 18.

!Field definition for: !
-- three digit field representing a child age
-- left padded with 0
-- default value of 18
-- must be between 1 and 18
DEFINITION child_age TYPE AGE MUST BE 1 THROUGH 18.

!Field definition for: !
-- three digit field representing a child age
-- left padded with 0
-- default value of 18
-- must be a regular expression (starts with 2 followed by a digit)
DEFINITION twenties TYPE AGE MUST BE REGEX "^2[0-9]".

!Field definition for: !
-- three digit field representing a child age
-- left padded with 0
-- default value of 969
-- must be a 969
DEFINITION methuselah TYPE AGE VALUE 969 DEFAULT 969.


------------------------------------------------------------------------------------------------------
!Field definition for an email address validated through two regular expressions left padded with '_''!
DEFINITION email PIC X(50) MUST BE REGEX "[A-Za-z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}.*", 
                                   REGEX "\s*" LPAD '_' DEFAULT "support@atombox.com".

-------------------------------------------------------------------------------------------------------
DEFINITION options PIC X(2) OCCURS 3 TIMES MUST BE "OK", "NO", "  " DEFAULT "NO".

-- Group Definition -- 
definition testbox.
    02 age        TYPE *.
    02 child_age  TYPE *.
    02 twenties   TYPE *.
    02 methuselah TYPE *.
    --
    02 email TYPE *.
    -- 
    02 options type *.
end.

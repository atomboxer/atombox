**SEQ0.04  11/01/00  DDLFEMF 6002 DDL    BA60DDL  BA04000  B                 ***
**SYNC.04  12/09/98  DDLFEMF 5302 DDL    BA53DDL  BA04000  B                 ***
**SYNC.03  08/28/98  DDLFEMF 5302 DDL    BA53DDL  BA03000  B                 ***
**SYNC.03  08/22/97  DDLFEMF 5102 DDL    BA51DDL  BA03000  B                 ***
**SYNC.02  06/25/96  DDLFEMF 5102 DDL    BA51DDL  BA02000  B                 ***
**FIX2.00  06/06/95  DDLFEMF 5102 DDL    BA51DDL  BA51008  B                 ***
**FIX2.00  01/20/95  DDLFEMF 5101 DDL    BA51DDL  BA50273  A                 ***
!*SEQ2.00  10/20/92  DDLFEMF 5000 DDL    BA50DDL                               !
?nocomments
?PAGE "EMF - EXTERNAL MESSAGE FILE"
?deflist
?SECTION EMF
                                                                       !00009
**********************************************************************!!00010
*                         History Section                            *!!00011
**********************************************************************!!00012
                                                                       !00013B00
                                                                       !00013B01
                                                                       !00013B02
*********************************************************************  !00063A02
*               RESYNC OF BASE DDL/SRC SUBVOLUMES                   *  !00063A03
*********************************************************************  !00063A04
                                                                       !00063A05
* 22AUG94        TMH - JMS/583                                         !00063A06
* SYMPTOM:       BASE24-telebanking Initial Release.                   !00063A07
* PROBLEM:       <E> None.                                             !00063A08
* FIX:           Added new product number (14) and new interface type  !00063A09
*                (VRU) for BASE24-telebanking.                         !00063A0A
* DEPENDENCIES:  None.                                                 !00063A0B
*                                                                      !00063A0C
* REFERENCE:     BASE24-telebanking External Specifications,           !00063A0D
*                WO TB930301-01                                        !00063A0E
                                                                       !00063A0F
* 28NOV94        JAS - JMS/583                                         !00063A0G
* SYMPTOM:       Base24-telebanking enhancement.                       !00063A0H
* PROBLEM:       <E> None.                                             !00063A0I
* FIX:           Updated comments for the MSG-TYP field to include     !00063A0J
*                VRU message types 1804 and 1814.                      !00063A0K
* DEPENDENCIES:  None.                                                 !00063A0L
*                                                                      !00063A0M
* REFERENCE:     BASE24-telebanking External Specifications,           !00063A0N
*                WO TB930301-01                                        !00063A0O
                                                                       !00063B03
*********************************************************************  !00063B04
*                  RELEASE 5.1                                      *  !00063B05
*********************************************************************  !00063B06
* 01JUN95     JMS/583                                                  !00063B07
* SYMPTOM:    Base24 Release 5.1 Enhancement.                          !00063B08
* PROBLEM:    <E> None.                                                !00063B09
* FIX:        Removed old history sections not associated with a       !00063B0A
*             specific Fix level.                                      !00063B0B
* IMPLEMENT:  None.                                                    !00063B0C
* REFERENCE:  Base24 Release 5.1 Enhancements.                         !00063B0D
                                                                       !00063B0E
                                                                       !00063B0F
                                                                       !00063B0G
                                                                       !00063B0H
*#####################################################################!!00064
*#                EXTERNAL MESSAGE FILE                              #!!00065
*#####################################################################!!00066
*1  3.1.2.4  External Message File                                    !!00067
*1  3.1.2.4.1 Identification                                          !!00068
*1                                                                    !!00069
*1      The External Message File (EMF) contains one record for each  !!00070
*1      DPC/symbolic HISF/Product/Message Type used by the system.  Each
*1      record contains information regarding the presence or absence !!00073
*1      of specific ISO fields plus IMS/CICS host transaction codes.  !!00074
*1                                                                    !!00075
*1  3.1.2.4.2  Security                                               !!00076
*1                                                                    !!00077
*1      The EMF is secured under Tandem's group level security so that!!00078
*1      only authorized network operators may access (or start        !!00079
*1      programs which will access) the file.                         !!00080
*1                                                                    !!00081
*1      USER ID         = <Base24 node name>.*                        !!00082
*1      FILE CODE       = 0                                           !!00083
*1      ACCESS SECURITY = GGGG                                        !!00084
*1                                                                    !!00085
*1  3.1.2.4.3  Usage                                                  !!00086
*1                                                                    !!00087
*3    Host Interface                                                  !!00088
*3                                                                    !!00089
*3                                                                    !!00090
***********************************************************************
                                                                       !00093
                                                                       !00094
?comments
*                                                                     !!00097
*              External Message File (EMF) records specify changes to !!00098
*              data elements to be included in the BASE24 External    !!00099
*              Message for incoming and outgoing messages.  This file !!00100
*              is used with the ISO-based External Message format.    !!00101
*                                                                     !!00102
*              Normally, BASE24 assumes a standard set of data        !!00103
*              elements for its external messages.  As long as no     !!00104
*              changes are made to these standard (default) data      !!00105
*              elements, the EMF is empty.  However, if the defaults  !!00106
*              need changed, records must be added to the EMF.        !!00107
*                                                                     !!00108
*              For complete information on the format of the BASE24   !!00109
*              External Message, please reference the "BASE24 External!!00110
*              Message Manual."                                       !!00111
*                                                                     !!00112
*              The EMF can contain one record for each message type,  !!00113
*              interface process, and BASE24 product combination.     !!00114
*              The file is used by ISO Host Interface, From Host      !!00115
*              Maintenance, BIC ISO Interchange Interface, and        !!00116
*              BASE24-pos NCR/NDP Device Handler processes.           !!00117
*                                                                     !!00118
*              LCONF ASSIGN:       EMF                                !!00119
*                                                                     !!00120
                                                                       !00121
RECORD EMF.                       FILE IS "EMF" RELATIVE.              !00122
                                                                       !00123
  02 PRIKEY                       KEYTAG "PK"                          !00124
                                  DUPLICATES NOT ALLOWED.              !00125
                                                                       !00126
*             The type of interface to which this record applies.     !!00127
*             Valid values are:                                       !!00128
*                                                                     !!00129
*             HOST = ISO Host Interface                               !!00130
*             FHM  = From Host Maintenance                            !!00131
*             BIC  = BIC ISO Interchange Interface                    !!00132
*             NCR  = BASE24-pos NCR/NDP Device Handler                !!00133
                                                                       !00133A00
*             VRU  = Voice Response Unit                               !00133A01
                                                                       !00133A02
                                                                       !00134
                                                                       !00135
                                                                       !00136
     04 INTERFACE-TYP             PIC X(4).                            !00137
                                                                       !00138
                                                                       !00139
                                                                       !00140
*                                                                     !!00141
*             The number of the Data Processing Center (DPC) whose    !!00142
*             messages are controlled by this record.  This DPC is    !!00143
*             the host computer that processes messages received from !!00144
*             BASE24.  This DPC number must match a DPC number in the !!00145
*             Host Configuration File (HCF).                          !!00146
*                                                                     !!00147
                                                                       !00147A00
*             This field is only used when the PRIKEY.INTERFACE-TYP    !00147A01
                                                                       !00147A02
                                                                       !00148A00
                                                                       !00148A01
*             field contains the value HOST.                          !!00149
                                                                       !00150
     04 DPC-NUM                   TYPE BINARY 16.                      !00151
                                                                       !00152
                                                                       !00153
                                                                       !00154
                                                                       !00155
                                                                       !00156
*                                                                     !!00157
*             The symbolic name of the interface process that         !!00158
*             communicates with the DPC specified in the PRIKEY.DPC-  !!00159
*             NUM field.  For BIC ISO and ISO Host Interface processes!!00160
*             the name in this field must have a corresponding record !!00161
*             in the Interchange Configuration File or Host           !!00162
*             Configuration File (HCF).                               !!00163
*                                                                     !!00164
     04 PRO-NAME                  TYPE SYM-NAME.                       !00165
                                                                       !00166
                                                                       !00167
                                                                       !00168
                                                                       !00169
                                                                       !00170
                                                                       !00171
                                                                       !00172
*                                                                     !!00173
*             The BASE24 product to which the record applies.  Valid  !!00174
*             values are:                                             !!00175
*                                                                     !!00176
*             00 = Base                                               !!00177
*             01 = BASE24-atm                                         !!00178
*             02 = BASE24-pos                                         !!00179
*             03 = BASE24-teller                                      !!00180
*             04 = Not Used                                           !!00181
*             05 = Not used                                           !!00182
*             06 = Not used                                           !!00183
*             07 = Not used                                           !!00184
*             08 = BASE24-from host maintenance                       !!00185
*             09 = Not used                                           !!00186
*             10 = Not used                                           !!00187
*             11 = BASE24-mail                                        !!00188
*             12 = Not used                                           !!00189
                                                                       !00189A00
*             14 = BASE24-telebanking                                  !00189A01
                                                                       !00189A02
*                                                                     !!00190
     04 PROD-NUM                  PIC 9(2).                            !00191
*                                                                     !!00192
*             The external message type that is being defined by the  !!00193
*             record.  Any external message type allowed by BASE24 is !!00194
*             valid in this field.  However, the external message     !!00195
*             type can only be used with certain values in the PROD-  !!00196
*             NUM and IN-OUT-IND fields.  Tables showing the valid    !!00197
*             relationships are provided below.  Note that the        !!00198
*             products listed are those that currently support        !!00199
*             communications with the ISO-based external message.     !!00200
*                                                                     !!00201
*             ======================================================  !!00202
*             --                 HOST Interface                   --  !!00203
*             ------------------------------------------------------  !!00204
*             PROD-NUM  PRODUCT  --------MSG-TYP--------  IN-OUT-IND  !!00205
*             ------------------------------------------------------  !!00206
*                00      Base    0800, 0810                I, O, B    !!00207
*                                -----------------------------------  !!00208
*                                9000                      O          !!00209
*             ------------------------------------------------------  !!00210
*                01      ATM     0200, 0205, 0210, 0215    I, O, B    !!00211
*                                0220, 0230, 0420, 0430               !!00212
*             ------------------------------------------------------  !!00213
*                02      POS     0100, 0110, 0120, 0130    I, O, B    !!00214
*                                0200, 0210, 0220, 0230               !!00215
*                                0402, 0412, 0420, 0430               !!00216
*                                -----------------------------------  !!00217
*                                0500, 0520                O          !!00218
*                                -----------------------------------  !!00219
*                                0510, 0530                I          !!00220
*             ------------------------------------------------------  !!00221
*                03      TLR     0200, 0210, 0220, 0230    I, O, B    !!00222
*                                0300, 0310, 0320, 0330               !!00223
*                                0420, 0430                           !!00224
*                                0600, 0610, 0620, 0630               !!00225
*             ------------------------------------------------------  !!00226
*                08      FHM     0300, 0310                I, O, B    !!00227
*             ------------------------------------------------------  !!00228
*                11      MAIL    0620, 0630                I, O, B    !!00229
                                                                       !00229A00
*             ------------------------------------------------------   !00229A01
*                14      TB      0100, 0120, 0200, 0220,   O           !00229A02
*                                0420                                  !00229A03
*                                -----------------------------------   !00229A04
*                                0110, 0130, 0210, 0230,   I           !00229A05
*                                0430                                  !00229A06
*             ======================================================   !00229A07
*             --                 VRU Interface                    --   !00229A08
*             ======================================================   !00229A09
*             PROD-NUM  PRODUCT  --------MSG-TYP--------  IN-OUT-IND   !00229A0A
*             ------------------------------------------------------   !00229A0B
*                00      Base    1804                     I            !00229A0C
*                                -----------------------------------   !00229A0D
*                                1814                     O            !00229A0E
*             ------------------------------------------------------   !00229A0F
*                14      TB      1100, 1200, 1420         I            !00229A0G
*                                -----------------------------------   !00229A0H
*                                1110, 1210, 1430         O            !00229A0I
*                                -----------------------------------   !00229A0J
*                                1644                     I, O, B      !00229A0K
                                                                       !00229A0L
*             ======================================================  !!00230
*                                                                     !!00231
*             ======================================================  !!00232
*             --                  BIC Interface                   --  !!00233
*             ------------------------------------------------------  !!00234
*             PROD-NUM  PRODUCT  --------MSG-TYP--------  IN-OUT-IND  !!00235
*             ------------------------------------------------------  !!00236
*                00      Base    0500, 0502, 0510, 0512    I, O, B    !!00237
*                                0520, 0522, 0530, 0532    I, O, B    !!00238
*                                0800, 0810, 0820, 0830    I, O, B    !!00239
*                                -----------------------------------  !!00240
*                                9000                      O          !!00241
*             ------------------------------------------------------  !!00242
*                01      ATM     0200, 0210, 0220, 0230    I, O, B    !!00243
*                                0420, 0430                           !!00244
*             ------------------------------------------------------  !!00245
*                02      POS     0100, 0110, 0120, 0130    I, O, B    !!00246
*                                0200, 0210, 0220, 0230               !!00247
*                                0402, 0412, 0420, 0430               !!00248
*             ======================================================  !!00249
*                                                                     !!00250
*                                                                     !!00251
*             ======================================================  !!00252
*             --                 NCR/NDP Device Handler           --  !!00253
*             ------------------------------------------------------  !!00254
*             PROD-NUM  PRODUCT  --------MSG-TYP--------  IN-OUT-IND  !!00255
*             ------------------------------------------------------  !!00256
*                02      POS     0100, 0200, 0202, 0420    I          !!00257
*                                0800, 0810                           !!00258
*                                -----------------------------------  !!00259
*                                0110, 0210, 0430, 0800    O          !!00260
*             ======================================================  !!00261
*                                                                     !!00262
*             ======================================================  !!00263
*             --                    FHM Update Process            --  !!00264
*             ------------------------------------------------------  !!00265
*             PROD-NUM  PRODUCT  --------MSG-TYP--------  IN-OUT-IND  !!00266
*             ------------------------------------------------------  !!00267
*                08      FHM     0300, 0310                I, O, B    !!00268
*                                -----------------------------------  !!00269
*                                9000                      O          !!00270
*             ======================================================  !!00271
*                                                                     !!00272
*             NOTE:  Message types in the 9000 range are used to      !!00273
*             denote rejects.  For example, a message type of 0200    !!00274
*             would be changed to 9200 if it were rejected and a 0420 !!00275
*             would be changed to a 9420 if it were rejected.         !!00276
*             Setting up a record in the EMF for a message type of    !!00277
*             9000 is not to control message structure, but rather to !!00278
*             allow for assigning a single IMS/CICS transaction code  !!00279
*             equivalent to all reject messages returned to the host. !!00280
*             For further information on assigning an IMS/CICS        !!00281
*             transaction code equivalent to a 9000 message, please   !!00282
*             refer to the B24-TRAN-CDE field.                        !!00283
*                                                                     !!00284
     04 MSG-TYP                   PIC 9(4).                            !00285
                                                                       !00286
                                                                       !00287
*                                                                     !!00288
*             Specifies if the record defines an inbound and/or       !!00289
*             outbound message.  Inbound messages are those coming to !!00290
*             BASE24.  Outbound messages are those being sent by      !!00291
*             BASE24.  The value of this field can only be used with  !!00292
*             certain values in the PROD-NUM and MSG-TYP fields.      !!00293
*             Valid values are:                                       !!00294
*                                                                     !!00295
*             I = Incoming--the record controls only the incoming     !!00296
*                 version of the message type specified in the        !!00297
*                 PRIKEY.MSG-TYP field.                               !!00298
*             O = Outgoing--the record controls only the outgoing     !!00299
*                 version of the message type specified in the        !!00300
*                 PRIKEY.MSG-TYP field.                               !!00301
*             B = Both--the record controls both the incoming and     !!00302
*                 outgoing versions of the message type specified in  !!00303
*                 the PRIKEY.MSG-TYP field.                           !!00304
*                                                                     !!00305
     04 IN-OUT-IND                PIC X.                               !00306
                                                                       !00307
*                                                                     !!00308
*             This field is not used.                                 !!00309
*                                                                     !!00310
     04 USER-FLD1                 PIC X.                               !00311
                                                                       !00312
*                                                                     !!00313
*             A code indicating the presence or absence of the each   !!00314
*             of the 128 data elements defined in the BASE24 External !!00315
*             Message.  Valid values are:                             !!00316
*                                                                     !!00317
*             _ = Not included in the message (_s are blanks that     !!00318
*                 have been entered)                                  !!00319
*             M = Mandatory                                           !!00320
*             C = Conditional                                         !!00321
*                                                                     !!00322
*             These data elements, as well as the standard default    !!00323
*             values for these fields, are documented in the "BASE24  !!00324
*             External Message Manual" for messages exchanged with a  !!00325
*             host or in the "BASE24 BIC ISO Standards Manual" for    !!00326
*             messages exchanged with a BASE24 Interchange.  For      !!00327
*             NCR/NDP Device Handler messages are documented in the   !!00328
*             "BASE24-pos NCR/NDP Device Support Manual."             !!00329
*                                                                     !!00330
  02 FLD-MAP                      PIC X OCCURS 128 TIMES.              !00331
*                                                                     !!00332
*             A code that specifies whether the data element is to be !!00333
*             included in the MAC calculation.  This is configurable  !!00334
*             for each of the 128 data elements defined in the        !!00335
*             BASE24 External Message. Valid values are:              !!00336
*                                                                     !!00337
*             Y = Include this field in the MAC calculation           !!00338
*             N = Do NOT include this field in the MAC calculation    !!00339
*                                                                     !!00340
*             These data elements, as well as the standard default    !!00341
*             values for these fields, are documented in the "BASE24  !!00342
*             External Message Manual" for messages exchanged with a  !!00343
*             host or in the "BASE24 BIC ISO Standards Manual" for    !!00344
*             messages exchanged with a BASE24 Interchange.           !!00345
*                                                                     !!00346
*             This field is not used by the BASE24-pos NCR/NDP Device !!00347
*             Handler.                                                !!00348
                                                                       !00349
  02 MAC-FLD-MAP                  PIC X OCCURS 128 TIMES.              !00350
                                                                       !00351
                                                                       !00352
                                                                       !00353
*                                                                     !!00354
*             The number of BASE24 transaction codes included in the  !!00355
*             TRAN-CDE-TBL.  The maximum number of transaction codes  !!00356
*             which can occur in the table is 150.                    !!00357
*                                                                     !!00358
  02 NUM-TRAN-CDE                 TYPE BINARY 16 UNSIGNED.             !00359
                                                                       !00360
*                                                                     !!00361
*             An indicator that identifies the last file maintenance  !!00362
*             action on this record.  The value in this field         !!00363
*             includes the user who performed the update, the time at !!00364
*             which it was done, and the type of update.              !!00365
*                                                                     !!00366
  02 LAST-FM                      TYPE *.                              !00367
                                                                       !00368
*                                                                     !!00369
*             An identifier used to link an EMF record to the Token   !!00370
*             File (TKN).  The TKN is used to supecify the tokens sent!!00371
*             ISO external messages.                                  !!00372
*                                                                     !!00373
                                                                       !00374
  02 TKN-GRP                   PIC X(4).                               !00375
                                                                       !00376
*                                                                     !!00377
*             This field indicates whether the full message or only   !!00378
*             selected fields will be included in the MAC calculation.!!00379
*                                                                     !!00380
*             Valid values:  0 - Selected fields (based on the MAC    !!00381
*                                bit map)                             !!00382
*                            1 - Full message                         !!00383
                                                                       !00384
  02 FULL-MSG-MAC                 PIC X(1).                            !00385
                                                                       !00386
*                                                                     !!00387
*             This field is not used.                                 !!00388
*                                                                     !!00389
  02 USER-FLD2                    PIC X(1).                            !00390
                                                                       !00391
                                                                       !00392
  02 TRAN-CDE-TBL            !     OCCURS 150 TIMES.
                                  OCCURS 0 TO 150 TIMES                !00393
                                  DEPENDING ON NUM-TRAN-CDE.           !00394
                                                                       !00395
*             A BASE24 transaction code that is being assigned an IMS !!00396
*             or CICS equivalent in the IMS-TRAN-CDE field.  If an    !!00397
*             IMS or CICS equivalent is available, BASE24 will include!!00398
*             it in its outgoing external messages along with the     !!00399
*             BASE24 transaction code.  If no IMS or CICS equivalent  !!00400
*             has been assigned, BASE24 will not include an IMS or    !!00401
*             CICS transaction code equivalent in its outgoing        !!00402
*             external messages.                                      !!00403
*                                                                     !!00404
*             Asterisks can be used in any position of this field as  !!00405
*             wildcard characters.  Asterisks will match on any value.!!00406
*             For example, a value of 10**** would match on any       !!00407
*             withdrawal, where a value of 1001** would only match a  !!00408
*             withdrawal from a checking account.  Using the first    !!00409
*             example, the same IMS or CICS transaction code would be !!00410
*             sent for any withdrawal.                                !!00411
*                                                                     !!00412
*             Note:  If an IMS or CICS transaction is to be included  !!00413
*             for 0800, 0810, or 9000, one entry is required in the   !!00414
*             table with this field set to all asterisks.             !!00415
*                                                                     !!00416
     04 B24-TRAN-CDE              PIC X(6).                            !00417
                                                                       !00418
                                                                       !00419
*                                                                     !!00420
*             The IMS or CICS transaction code that equates to the    !!00421
*             BASE24 transaction code in the TRAN-CDE-TBL.B24-TRAN-   !!00422
*             CDE field.                                              !!00423
*                                                                     !!00424
     04 IMS-TRAN-CDE              PIC X(9).                            !00425
                                                                       !00426
                                                                       !00427
*                                                                     !!00428
*             The length of the IMS or CICS transaction code included !!00429
*             in TRAN-CDE-TBL.IMS-TRAN-CDE field.                     !!00430
                                                                       !00431
     04 IMS-TRAN-CDE-LGTH         PIC X.                               !00432
                                                                       !00433
                                                                       !00434
                                                                       !00435
                                                                       !00436
  END                                                                  !00437
?nocomments

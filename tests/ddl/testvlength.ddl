
def v1.
    02 len PIC 99.
    02 vari PIC 99 redefines len.
    02 sep PIC X default '.'.
    02 data VLENGTH DEPENDING ON len PIC X(99).
end.

def v2.
    02 st     PIC XXX DEFAULT "=+*".
    02 rd     PIC XX REDEFINES st.
    02 box.
        04 tm  PIC X default "?".
        04 len PIC 9999.
        04 alt PIC X default ",".
        04 red REDEFINES len PIC 9999.
        04 data VLENGTH DEPENDING ON red PIC X(20) default "hello".
end.


def v3.
    04 b1.
        06 l  PIC 99.
        06 r  PIC XX REDEFINES l.
        06 d  PIC 9(99) default 12 VLENGTH DEPENDING ON l.

    04 b2.
        06 l  TYPE BINARY 16 DEFAULT 18.
        06 r  PIC XX REDEFINES l.
        06 d  PIC X(99) VLENGTH DEPENDING ON l.

    04 b3.
        06 l  PIC 99 BCD.
        06 r  PIC XX PACKED REDEFINES l.
        06 d  PIC 9(99) default 12345 VLENGTH DEPENDING ON l.

    04 b4.
        06 l  PIC 99 BCD DEFAULT 13.
        06 r  PIC 99 PACKED REDEFINES l.
        06 d  PIC X(99) VLENGTH DEPENDING ON r DEFAULT "xxxxxxxxxx".

    04 alt redefines b1.
       06 f PIC X(99).
end.

def valt.
      02 len PIC 99 default 5.
      02 dat VLENGTH DEPENDING ON len PIC X(99) default "hello world".
      02 rlen redefines len.
          04 rlen1 PIC XX.
      02 rdat redefines dat.
         04 rdat1 PIC X(11).

      02 bst.
         04 bst1 PIC XXXX DEFAULT "JJGG".

      02 brd redefines bst.
          04 brd11 PIC XX.
          04 brd12 PIC XX.

      02 st     PIC XXX DEFAULT "=+*".
      02 rd     PIC XX REDEFINES st.

      02 box.
            04 box_st  PIC XXXXXXXX DEFAULT "XXXXYYYY".
            04 box_bstr REDEFINES box_st.
                06 box_f1 PIC XXXX.
                06 box_f2 PIC XXXX.
            04 box_rf1 redefines box_st.
                06 alt PIC XXXXXXXX.
     --02 box_r redefines box.
     --       04 alt PIC X(8).
end.

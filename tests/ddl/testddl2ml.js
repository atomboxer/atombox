var system  = require("system");
var console = require("console");
var fs = require("fs");

require("C:/tmpuser/sources/BASE24_src_new/BA60DEFS/ddlgdefs", "ddl");
require("C:/tmpuser/sources/BASE24_src_new/BP60CRTA/ddlgdefs", "ddl");
require("C:/tmpuser/sources/BASE24_src_new/BA60DDL/ddlgpstm", "ddl");

//var fns = require("C:/tmpuser/sources/BASE24_src_new/BA60DDL/ddlfecf", "ddl");
//var fns = require("C:/tmpuser/sources/BASE24_src_new/BA60DDL/ddlfconf", "ddl");
//var fns = require("C:/tmpuser/sources/BASE24_src_new/BP60DDL/DDLFPRF", "ddl");
//var fns = require("C:/tmpuser/sources/BASE24_src_new/PS60DDL/DDLFPRDF", "ddl");
//var fns = require("C:/Users/faekejl/Desktop/DDLFPRDF","ddl");
//var fns = require("C:/Users/faekejl/Desktop/DDLFCONF","ddl");

var o;
for (ctr in fns) {
    o = new (fns[ctr])();

    if (o.isRecord()) {
        console.writeln(o.name);
        break;
    }
}


function generate(o) {

    var key_component = undefined;
    var definition_lines     = [];
    var key_definition_lines = [];
    var key_prefix = "";

    function process(key, v, ln) {
        var line       = "    ";
        var definition = "";

        line += ln.join("_");
        
        if (v.isKey != undefined && v.isKey()) {
            key_component = v;
            var key_prefix_arr = ln.slice();

            if (v.pack != undefined && v.value != undefined)
                //Atom
                key_prefix_arr.pop();

            key_prefix = key_prefix_arr.join("_");
        }

        if (v.pack != undefined && v.value == undefined) {
            //This is a box
            return;
        }

        if (v.pack == undefined) {
            return;
        }

        if (v.isNumeric != undefined && v.isNumeric()) {
            if (v.isBinary()) {
                switch(v.length) {
                case(2): definition = "bt_int16s"; break; 
                case(4): definition = "bt_int32s"; break;
                case(8): definition = "bt_int64s"; break;
                default: console.log("unknown size of binary!"+v.length); system.exit(0); break;
                }
            } else {
                definition = "bt_stringf("+v.length+")";
            }
        } else { //this is a PIC X
            if (v.length == 1)
                definition = "bt_char"
            else 
                //definition = "bt_binaryf("+v.length+")";
                definition = "bt_stringf("+v.length+")";
        }

        definition_lines = 
            definition_lines.concat(
                line.slice(0,32).rpad(" ",80)+definition/*.lpad(" ",30)*/);
    }
    
    function traverse(o,func, long_name) {
        if (long_name == undefined)
            long_name = [];

        for (var i in o) {
            long_name = long_name.concat(i);
            func.apply(undefined,[i,o[i], long_name]);  
            if (o[i] !== null && typeof(o[i])=="object") {
                if (o[i].isRedefine != undefined && !o[i].isRedefine()) {
                    traverse(o[i],func,long_name);
                   
                } else if (o[i].isRedefine == undefined){
                    //Array
                    traverse(o[i], func, long_name);
                }

                long_name.pop();
            }
        }
    }

    traverse(o, process);

    if (key_component != undefined) {
        //is it an Atom
        if (key_component.value != undefined) {
            key_definition_lines = key_definition_lines.concat(
                "        "+(key_prefix+"_"+key_component.name).slice(0,32));
        } else {
            var key_definition_lines = [];
            for (var i in key_component) {
                key_definition_lines = key_definition_lines
                    .concat("        "+(key_prefix+"_"+key_component[i].name).slice(0,32));
            }
        }
    }

    //
    //  Now we build the text
    //
    var txt = "CREATE TABLE "+o.name+"\r\n";
    txt += "    "+"(\r\n";
    txt += "    "+"TABLE_VERSION 1.0,\r\n";
    txt += "    "+"SIS_TBL_SHORT_NAM "+o.name+",\r\n";
    txt += "    "+"SIS_TBL_LEGACY_IND Y,\r\n";
    txt += "    "+"SIS_TBL_MAX_ROW_LGTH "+o.length+",\r\n";  
    txt += definition_lines.join(",\r\n");
    if (key_definition_lines.length != 0) {
        txt += "\r\n\r\n    PRIMARY_KEY Key PK UNIQUE\r\n";
        txt += "       (\r\n";
        txt += key_definition_lines.join(",\r\n");
        txt += "\r\n       )\r\n";
    }

    txt += "    "+");\r\n"; 
    
    var stream = fs.open("ml"+o.name+".txt","wb");
    stream.write(txt.toByteArray());
    stream.close();

}

generate(o);

system.exit(0);

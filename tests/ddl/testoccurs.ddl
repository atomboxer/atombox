

def v1.
    02 len  PIC 99 default 3.
    02 data PIC X(99) VLENGTH depending on len.

    02 dat1 occurs 0 to 10 times depending on len REDEFINES data.
         04 f PIC X default ' '.
         
    02 dat2 PIC X OCCURS 0 TO 10 times depending on len redefines data.
    02 dat3 PIC X(10) REDEFINES data.
end.

-- def v2.
--     02 len  PIC 99 default 1.
--     02 data PIC X(99) VLENGTH depending on len.

--     02 dat1 occurs 1 to 20 times depending on len.
--         04 f PIC X default ' '.
-- end.
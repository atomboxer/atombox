
--Default ByteOrder in AtomBox is Network BO, Big Endian--

--Byte Ordering is a processor-related issue. Intel-based processors store numeric values least-significant byte first (LOW_HIGH), while other processors store numeric values most-significant byte first (HIGH_LOW). So a four-byte integer containing the value “16” stored on an LOW_HIGH system as the hexadecimal value 10 00 00 00 would be stored on a HIGH_LOW system as the hexadecimal value 00 00 00 10. The order of the bits is consistent within the bytes, but the order of the bytes in the numeric value is reversed. By default, c-treeACE files are stored in the native format to the operating system, so files created on a HIGH_LOW machine cannot be exchanged directly with applications on a LOW_HIGH machine
--
definition test.
    02 in32 TYPE BINARY 32 DEFAULT 16.
end.
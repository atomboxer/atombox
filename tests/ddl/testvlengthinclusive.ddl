
-- def t1.
--      02 f1 PIC 99 default 5.
--      02 f3 PIC 9.
--      02 data VLENGTH ALLINCLUSIVE DEPENDING ON f1 PIC X(20) default "hello".
--      02 f2 redefines f1 PIC XX.
-- end.

def tde.
    02 tag     TYPE BINARY 32.
    02 version TYPE BINARY 32.
    02 len     TYPE BINARY 32 DEFAULT 16.
    02 data    PIC X(999) VLENGTH           !variable length 
                          ALLINCLUSIVE      !length includes all atoms
                          DEPENDING ON len. !length dependent on len
end.

def JRNL.
    02 TDEs TYPE tde occurs 60 times.
end.
var system  = require("system");
var console = require("console");
var assert  = require("assert");


var b1l = false,b1r = false,b1d = false;
var b2l = false,b2r = false,b2d = false;
var b3l = false,b3r = false,b3d = false;
var b4l = false,b4r = false,b4d = false;
var rb1 = false,rf = false;

var b1 = false, b2 = false, b3 = false, b4 = false;
var of = false;

var f = require("testvlength.ddl","ddl");
var o = new f.v3();
o.b1.l.changed.connect(function() {print("c b1l");b1l = !b1l;})
o.b1.r.changed.connect(function() {print("c b1r");b1r = !b1r;})
o.b1.d.changed.connect(function() {print("c b1d");b1d = !b1d;})
o.b1.l.packed.connect(function()  {print("p b1l");b1l = !b1l;})
o.b1.r.packed.connect(function()  {print("p b1r");b1r = !b1r;})
o.b1.d.packed.connect(function()  {print("p b1d");b1d = !b1d;})

o.b2.l.changed.connect(function() {print("b2l");b2l = !b2l;})
o.b2.r.changed.connect(function() {print("b2r");b2r = !b2r;})
o.b2.d.changed.connect(function() {print("b2d");b2d = !b2d;})
o.b3.l.changed.connect(function() {print("b3l");b3l = !b3l;})
o.b3.r.changed.connect(function() {print("b3r");b3r = !b3r;})
o.b3.d.changed.connect(function() {print("b3d");b3d = !b3d;})
o.b4.l.changed.connect(function() {print("b4l");b4l = !b4l;})
o.b4.r.changed.connect(function() {print("b4r");b4r = !b4r;})
o.b4.d.changed.connect(function() {print("b4d");b4d = !b4d;})

o.b1.changed.connect(function() {print("b1");b1=!b1;});
o.b2.changed.connect(function() {print("b2");b2=!b2;});
o.b3.changed.connect(function() {print("b3");b3=!b3;});
o.b4.changed.connect(function() {print("b4");b4=!b4;});

o.b1.packed.connect(function() {print("p b1");b1=!b1;});
o.b2.packed.connect(function() {print("p b2");b2=!b2;});
o.b3.packed.connect(function() {print("p b3");b3=!b3;});
o.b4.packed.connect(function() {print("p b4");b4=!b4;});

o.changed.connect(function() {print("c o");of=!of;});
o.packed.connect(function()  {print("p o");of=!of;});

initVars = function() {
    b1l = false;b1r = false;b1d = false;
    b2l = false;b2r = false;b2d = false;
    b3l = false;b3r = false;b3d = false;
    b4l = false;b4r = false;b4d = false;
    rb1 = false;rf = false;
    
    b1 = false; b2 = false; b3 = false; b4 = false;
    of = false;    
}

exports.testV3E1 = function ()
{

    initVars();
    
    o.pack(o.unpack());

    assert.equal(of,  true);
    assert.equal(b1,  true);
    assert.equal(b1l, true);
    assert.equal(b1r, true); 
    assert.equal(b1d, true);   
    assert.equal(b2l, false);
    assert.equal(b2r, false);    

}

exports.testV3E2 = function ()
{
    initVars();

    o.b1.l.value = "5";
    console.dir(o);
    assert.equal(of,  true);
    assert.equal(b1,  true);
    assert.equal(b1l, true);
    assert.equal(b1r, true); 
    assert.equal(b1d, true);
    assert.equal(b2, false);    
}

exports.testV3E3 = function ()
{
    initVars();
    o.b1.d.value = "9999999999999999";

    console.dir(o);
    assert.equal(of,  true);
    assert.equal(b1,  true);
    assert.equal(b1l, true);
    assert.equal(b1r, true); 
    assert.equal(b1d, true);
    assert.equal(b2, false);    
}


if (require.main == module.id)
    require("test").run(exports);

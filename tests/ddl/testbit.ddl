def pbitmap.
    02 p1	TYPE BIT 1.
    02 p2	TYPE BIT 1.
    02 p3	TYPE BIT 1.
    02 p4	TYPE BIT 1.
    02 p5	TYPE BIT 1.
    02 p6	TYPE BIT 1.
    02 p7	TYPE BIT 1.
    02 p8	TYPE BIT 1.
    02 p9	TYPE BIT 1.
    02 p10	TYPE BIT 1.
    02 p11	TYPE BIT 1.
    02 p12	TYPE BIT 1.
    02 p13	TYPE BIT 1.
    02 p14	TYPE BIT 1.
    02 p15	TYPE BIT 1.
    02 p16	TYPE BIT 1.
    02 p17	TYPE BIT 1.
    02 p18	TYPE BIT 1.
    02 p19	TYPE BIT 1.
    02 p20	TYPE BIT 1.
    02 p21	TYPE BIT 1.
    02 p22	TYPE BIT 1.
    02 p23	TYPE BIT 1.
    02 p24	TYPE BIT 1.
    02 p25	TYPE BIT 1.
    02 p26	TYPE BIT 1.
    02 p27	TYPE BIT 1.
    02 p28	TYPE BIT 1.
    02 p29	TYPE BIT 1.
    02 p30	TYPE BIT 1.
    02 p31	TYPE BIT 1.
    02 p32	TYPE BIT 1.
    02 p33	TYPE BIT 1.
    02 p34	TYPE BIT 1.
    02 p35	TYPE BIT 1.
    02 p36	TYPE BIT 1.
    02 p37	TYPE BIT 1.
    02 p38	TYPE BIT 1.
    02 p39	TYPE BIT 1.
    02 p40	TYPE BIT 1.
    02 p41	TYPE BIT 1.
    02 p42	TYPE BIT 1.
    02 p43	TYPE BIT 1.
    02 p44	TYPE BIT 1.
    02 p45	TYPE BIT 1.
    02 p46	TYPE BIT 1.
    02 p47	TYPE BIT 1.
    02 p48	TYPE BIT 1.
    02 p49	TYPE BIT 1.
    02 p50	TYPE BIT 1.
    02 p51	TYPE BIT 1.
    02 p52	TYPE BIT 1.
    02 p53	TYPE BIT 1.
    02 p54	TYPE BIT 1.
    02 p55	TYPE BIT 1.
    02 p56	TYPE BIT 1.
    02 p57	TYPE BIT 1.
    02 p58	TYPE BIT 1.
    02 p59	TYPE BIT 1.
    02 p60	TYPE BIT 1.
    02 p61	TYPE BIT 1.
    02 p62	TYPE BIT 1.
    02 p63	TYPE BIT 1.
    02 p64	TYPE BIT 1.
end.

def sbitmap.
    02 s1	TYPE BIT 1.
    02 s2	TYPE BIT 1.
    02 s3	TYPE BIT 1.
    02 s4	TYPE BIT 1.
    02 s5	TYPE BIT 1.
    02 s6	TYPE BIT 1.
    02 s7	TYPE BIT 1.
    02 s8	TYPE BIT 1.
    02 s9	TYPE BIT 1.
    02 s10	TYPE BIT 1.
    02 s11	TYPE BIT 1.
    02 s12	TYPE BIT 1.
    02 s13	TYPE BIT 1.
    02 s14	TYPE BIT 1.
    02 s15	TYPE BIT 1.
    02 s16	TYPE BIT 1.
    02 s17	TYPE BIT 1.
    02 s18	TYPE BIT 1.
    02 s19	TYPE BIT 1.
    02 s20	TYPE BIT 1.
    02 s21	TYPE BIT 1.
    02 s22	TYPE BIT 1.
    02 s23	TYPE BIT 1.
    02 s24	TYPE BIT 1.
    02 s25	TYPE BIT 1.
    02 s26	TYPE BIT 1.
    02 s27	TYPE BIT 1.
    02 s28	TYPE BIT 1.
    02 s29	TYPE BIT 1.
    02 s30	TYPE BIT 1.
    02 s31	TYPE BIT 1.
    02 s32	TYPE BIT 1.
    02 s33	TYPE BIT 1.
    02 s34	TYPE BIT 1.
    02 s35	TYPE BIT 1.
    02 s36	TYPE BIT 1.
    02 s37	TYPE BIT 1.
    02 s38	TYPE BIT 1.
    02 s39	TYPE BIT 1.
    02 s40	TYPE BIT 1.
    02 s41	TYPE BIT 1.
    02 s42	TYPE BIT 1.
    02 s43	TYPE BIT 1.
    02 s44	TYPE BIT 1.
    02 s45	TYPE BIT 1.
    02 s46	TYPE BIT 1.
    02 s47	TYPE BIT 1.
    02 s48	TYPE BIT 1.
    02 s49	TYPE BIT 1.
    02 s50	TYPE BIT 1.
    02 s51	TYPE BIT 1.
    02 s52	TYPE BIT 1.
    02 s53	TYPE BIT 1.
    02 s54	TYPE BIT 1.
    02 s55	TYPE BIT 1.
    02 s56	TYPE BIT 1.
    02 s57	TYPE BIT 1.
    02 s58	TYPE BIT 1.
    02 s59	TYPE BIT 1.
    02 s60	TYPE BIT 1.
    02 s61	TYPE BIT 1.
    02 s62	TYPE BIT 1.
    02 s63	TYPE BIT 1.
    02 s64	TYPE BIT 1.
end.

def v1.
   02 pri PIC X(16) PACKED occurs 1.
   02 pbox type pbitmap redefines pri.  
   --02 sec.
   02 sec PIC X(16) PACKED  OCCURS 0 to 1 times depending on p2.
   02 sbox type sbitmap redefines sec.
   02 alt.
        04 pan PIC AAAA default "1810" occurs 0 to 1 times depending on p2.
end.


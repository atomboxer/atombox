definition testr.
 02 SUSPENSE                           .--OCCURS 9 TIMES.                !00546
                                                                       !00547
     04 FNAMES                          PIC X(24).                     !00548
                                                                       !00549
     04 FNAME                           PIC X(12) OCCURS 2 TIMES REDEFINES FNAMES.
--TYPE BINARY 16                 !00550
--                                        OCCURS 12 TIMES                !00551
--                                        REDEFINES FNAMES.              !00552
                                                                       !00553
*             An indicator that identifies the last file maintenance  !!00554
*             action on this record. The value in this field includes !!00555
*             the user who performed the update, the time it was      !!00556
*             done, and the type of update.                           !!00557
end.
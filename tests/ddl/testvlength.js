var system  = require("system");
var console = require("console");
var assert  = require("assert");


exports.testV3 = function ()
{
    var f = require("testvlength.ddl","ddl");
    
    var o = new f.v3();

    assert.equal(o.b1.l.value, 2);
    assert.equal(o.b1.r.value, "02");
    assert.equal(o.b1.d.value, 12);

    assert.equal(o.b1.unpack().decodeToString(), "0212");
    assert.equal(o.b1.unpack(o.b1.pack(o.b1.unpack())).decodeToString(),"0212");
    assert.equal(o.b2.l.value, 18);
    assert.equal(o.b2.r.bytes.decodeToString("hex"), "0012");
    assert.equal(o.b2.d.value, "                  ");
    assert.equal(o.b2.unpack().decodeToString("hex"), 
                 "0012202020202020202020202020202020202020");
    assert.equal(o.b2.unpack(o.b2.pack(o.b2.unpack())).decodeToString("hex"),
                 "0012202020202020202020202020202020202020");
    

    assert.equal(o.b3.l.value, 5);
    assert.equal(o.b3.r.value, "05");
    assert.equal(o.b3.d.value, 12345);
    assert.equal(o.b3.unpack().decodeToString("hex"), "053132333435");
    assert.equal(o.b3.unpack(o.b1.pack(o.b1.unpack())).decodeToString("hex"),
                 "053132333435");    
    assert.equal(o.b3.l.value, 5);
    assert.equal(o.b3.r.value, "05");
    
    assert.equal(o.b4.l.value, 10); /*second default takes precedence*/
    assert.equal(o.b4.r.value, "10");
    assert.equal(o.b4.d.value, "xxxxxxxxxx");
    assert.equal(o.b4.unpack().decodeToString("hex"), "1078787878787878787878");
    assert.equal(o.b4.unpack(o.b1.pack(o.b1.unpack())).decodeToString("hex"),
                 "1078787878787878787878");
    assert.equal(o.b4.l.value, 10);
    assert.equal(o.b4.r.value, "10");


    //
    //  Change some values
    //
    o.b1.r.value = "03";
    assert.equal(o.b1.l.value, 3);    
    assert.equal(o.b1.d.value, "120");


    o.b2.l.value = 15;
    assert.equal(o.b2.r.bytes.decodeToString("hex"), "000f");
    assert.equal(o.b2.d.value, "               ");

    o.b3.l.value = 2;
    assert.equal( o.b3.r.bytes.decodeToString("hex"), "02");
    assert.equal( o.b3.d.value, "12");
    assert.equal( o.b3.unpack().decodeToString("hex"),
                  "023132");

    o.b4.l.value = 20;
    assert.equal(o.b4.r.value, 20);
    assert.equal( o.b4.unpack().decodeToString("hex"),
                  "207878787878787878787820202020202020202020");


    assert.equal(o.unpack(o.pack(o.unpack())).decodeToString("hex"),
                 "3033313230"+
                 "000f202020202020202020202020202020"+
                 "023132" +
                 "207878787878787878787820202020202020202020");
}

exports.testV2 = function ()
{
    var f = require("testvlength.ddl","ddl");

    var o = new f.v2();

    assert.equal(o.st.value, "=+*");
    assert.equal(o.rd.value, "=+");
    assert.equal(o.box.bytes.decodeToString(), "?0005,hello");
    assert.equal(o.box.red.value, 5);

    o.box.red.value = 10;
    assert.equal(o.box.bytes.decodeToString(), "?0010,hello     ");
    assert.equal(o.box.len.value, 10);

    o = new f.v2();
    assert.equal(o.unpack().decodeToString(), "=+*?0005,hello");
    o.pack("123#0021=hello world                                ".toByteArray());
    assert.equal(o.box.data.value, "hello world          ");
}

exports.testV1 = function ()
{
    var f = require("testvlength.ddl","ddl");

    var o1 = new f.v1();
    
    assert.equal(o1.unpack().length, 3);
    o1.len.value = 3;
    assert.equal(o1.len.value, 3)
    assert.equal(o1.vari.value, 3)
    assert.equal(o1.data.value, "   ")
    assert.equal(o1.unpack().decodeToString(), "03.   ");

    o1.pack("09.1       9".toByteArray());

    assert.equal(o1.len.value, 9);
    assert.equal(o1.vari.value, 9);
    assert.equal(o1.data.value, "1       9");

    o1.vari.value = 4;
    assert.equal(o1.len.value, 4);    
    assert.equal(o1.data.value, "1   ");    
}

if (require.main == module.id)
    require("test").run(exports);

system.exit(0);

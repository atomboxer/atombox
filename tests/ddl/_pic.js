
var system = require("system");
var assert = require("assert");
var console = require("console");

exports.testComp = function() {
    //
    // AtomString mappings
    //
    var T = require("_pic.ddl","ddl").comp_pictures;
    assert.ok(T != undefined);
    var t = new T();
    assert.equal(t.bytes.decodeToString("hex"), "0102030405060708");
    assert.equal(t.c_2.value,5678);
    t.pack("0807060504030201".toByteArray("hex"));
    assert.equal(t.c_2.value,4321);
    assert.equal(t.unpack().decodeToString("hex"), "0807060504030201");
}

exports.testComp3 = function() {
    //
    // AtomString mappings
    //
    var T = require("_pic.ddl","ddl").comp3_pictures;
    assert.ok(T != undefined);
    var t = new T();
    assert.equal(t.bytes.decodeToString("hex"), "12345678");
    assert.equal(t.c_1.value, 1234)
    t.pack("87654321".toByteArray("hex"));
    assert.equal(t.c_2.value, 4321);
    assert.equal(t.unpack().decodeToString("hex"), "87654321");
}

exports.testPicStringNumeric = function() {
    //
    // AtomStringNumeric mappings
    //

    var T = require("_pic.ddl","ddl").ASCII_numeric_pictures;
    assert.ok(T != undefined);
    var t = new T();
    t.unsign.value = 130.0013;
    console.dir(t);
    t.pack("+0177-+888777-00130.001".toByteArray());
    assert.equal("+0177-+888777-00130.001",(t.unpack().decodeToString()));
}

exports.testPicString = function() {

    //
    // AtomString mappings
    //
    var T = require("_pic.ddl","ddl").ascii_pictures;
    assert.ok(T != undefined);
    var t = new T();
    //t.pack('0AAAAAAAXXXXXXXXXXXX12345666666666666666666666666666'.toByteString());
    t.pack(t.unpack());
    console.dir(t);
}

exports.testPicBinary = function() {
    //
    // AtomString mappings
    //
    var T = require("_pic.ddl","ddl").binary_pictures;
    assert.ok(T != undefined);
    var t = new T();
    t.rbin_32.value = 1.1;
    t.rbin_64.value = 1.1;

    t.pack(t.unpack().decodeToString("hex").toByteArray("hex"));
}


if (require.main == module.id)
    require('test').run(exports);

system.exit(0);

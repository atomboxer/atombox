var assert = require("assert");
var console = require("console");

//var dump = require("init").dump;

var Binary = require("binary").Binary,
ByteString = require("binary").ByteString,
ByteArray = require("binary").ByteArray;

exports.testHash = function()
{
    var string = "Ana are mere tata are pere baolasdas das d as das d as da sd as d as da sd a sd as da sd as d";

    console.log(string.toByteArray().hash32().decodeToString("hex"));
    console.log(string.toByteArray().hash64().decodeToString("hex"));
    console.log(string.toByteArray().hash128().decodeToString("hex"));
 console.log(string.toByteArray().hash64WithSeed( ByteArray("hello world")).decodeToString("base64"));
    console.log(string.toByteArray().hash128WithSeed( ByteArray("hello world")).decodeToString("base64"));





}

exports.testCompress = function()
{
    var string = "Ana are mere tata are pere baolasdas das d as das d as da sd as d as da sd a sd as da sd as d";
    console.assert((string.toByteArray().compress()).uncompress().decodeToString() == string);
}


// exports.testByteArrayConstructor = function() {
//     var testArray = [1,2,3,4],
//     b;
    
//     // ByteArray()
//     // New, empty ByteArray.
//     b = new ByteArray();
//     //assert.ok(b instanceof Binary, "not instanceof Binary");
//     assert.ok(b instanceof ByteArray, "not instanceof ByteArray");
//     assert.equal(0, b.length);
//     b.length = 123;
//     assert.equal(123, b.length);
//     assert.equal(0, b.get(4));
    
//     // ByteArray(length)
//     // New ByteArray filled with length zero bytes.
//     b = new ByteArray(10);
//     assert.equal(10, b.length);
//     for (var i = 0; i < 10; i++)
//         assert.equal(0, b.get(i));
//     assert.isNaN(b.get(10));
//     b.length = 234;
//     assert.equal(234, b.length);
//     assert.equal(0, b.get(10));
//     assert.equal(0, b.get(233));
//     assert.isNaN(b.get(234));
    
//     var a = new String("Ana are mere");

//     // ByteArray(byteString)
//     // Copy contents of byteString.

//     b = new ByteArray(new ByteString(testArray));

//     assert.equal(testArray.length, b.length);
//     b.length = 345;
//     assert.equal(345, b.length);
//     assert.equal(1, b.get(0));
//     assert.equal(4, b.get(3));
//     assert.equal(0, b.get(4));
   
//     // ByteArray(byteArray)
//     // Copy byteArray.
//     b = new ByteArray(new ByteArray(testArray));
//     assert.equal(testArray.length, b.length);
//     b.length = 456;
//     assert.equal(456, b.length);
//     assert.equal(1, b.get(0));
//     assert.equal(4, b.get(3));
//     assert.equal(0, b.get(4));
    
//     // ByteArray(arrayOfBytes)
//     // Use numbers in arrayOfBytes as contents.
//     // Throws an exception if any element is outside the range 0...255 (TODO).
//     b = new ByteArray(testArray);
//     assert.equal(testArray.length, b.length);
//     b.length = 567;
//     assert.equal(567, b.length);
//     assert.equal(1, b.get(0));
//     assert.equal(4, b.get(3));
//     assert.equal(0, b.get(4));
// };

// exports.testByteArrayResizing = function() {
//     var b1 = new ByteArray([0,1,2,3,4,5,6]);
//     assert.equal(7, b1.length);
//     assert.isNaN(b1.get(7));
    
//     b1.length = 10;
//     assert.equal(10, b1.length, "Length should change to 10");
//     assert.equal(5, b1.get(5));
//     assert.equal(0, b1.get(7));
    
//     b1.length = 3;
//     assert.equal(3, b1.length, "Length should change to 3");
//     assert.equal(0, b1.get(0));
//     assert.isNaN(b1.get(4));
    
//     b1.length = 9;
//     assert.equal(9, b1.length, "Length should change to 9");
//     assert.equal(0, b1.get(0));
//     assert.equal(0, b1.get(4));
// };

// exports.testToByteArray = function() {
//     var b1 = new ByteArray([1,2,3]),
//     b2 = b1.toByteArray();
    
//     assert.ok(b2 instanceof ByteArray, "not instanceof ByteArray");
//     assert.equal(b1.length, b2.length);
//     assert.equal(b1.get(0), b2.get(0));
//     assert.equal(b1.get(2), b2.get(2));
    
//     assert.equal(1, b1.get(0));
//     assert.equal(1, b2.get(0));
    
//     //b1.set(0, 10); //@non common
    
//     //assert.equal(10, b1.get(0));
//     //assert.equal(1, b2.get(0));
// };

// //exports.testToByteString = function() {
// //     var b1 = new ByteArray([1,2,3]),
// //         b2 = b1.toByteString();

// //     assert.equal(b1.length, b2.length);
// //     assert.equal(b1.get(0), b2.get(0));
// //     assert.equal(b1.get(2), b2.get(2));

// //     assert.equal(1, b1.get(0));
// //     assert.equal(1, b2.get(0));

// //     b1.set(0, 10);

// //     assert.equal(10, b1.get(0));
// //     assert.equal(1, b2.get(0));
// // };

// exports.testToArray = function() {
//     var testArray = [0,1,254,255],
//     b1 = new ByteArray(testArray),
//     a1 = b1.toArray();

//     assert.equal(testArray.length, a1.length,"not equal");
//     for (var i = 0; i < testArray.length; i++)
//         assert.equal(testArray[i], a1[i]);
// };

// exports.testIndexOf = function() {
//     var b1 = new ByteArray([0,1,2,3,4,5,0,1,2,3,4,5]);
    
//     assert.equal(-1, b1.indexOf(-1));
    
//     assert.equal(0,  b1.indexOf(0));
//     assert.equal(5,  b1.indexOf(5));
//     assert.equal(-1, b1.indexOf(12));
    
//     assert.equal(6,  b1.indexOf(0, 6));
//     assert.equal(11,  b1.indexOf(5, 6));
//     assert.equal(-1, b1.indexOf(12, 6));
    
//     // @mb extra
//     assert.equal(3,b1.indexOf(new ByteArray([3,4,5])));
// };

// exports.testLastIndexOf = function() {
//     var b1 = new ByteArray([0,1,2,3,4,5,0,1,2,3,4,5]);

//     assert.equal(-1, b1.lastIndexOf(-1));

//     assert.equal(6,  b1.lastIndexOf(0));
//     assert.equal(11,  b1.lastIndexOf(5));
//     assert.equal(-1, b1.lastIndexOf(12));
// };

// exports.testByteArrayReverse = function() {
//     var testArray = [0,1,2,3,4,5,6];
    
//     var b1 = new ByteArray(testArray);

//     var b2 = b1.reverse();
    
//     //assert.deepEqual(b1, b2);
//     // assert.equal(b1.length, b2.length);

//     // for (var i = 0; i < testArray.length; i++)
//     //     assert.equal(testArray[i], b2.get(testArray.length-i-1));

//     // testArray = [0,1,2,3,4,5,6,7];

//     // b1 = new ByteArray(testArray);
//     // b2 = b1.reverse();

//     // assert.deepEqual(b1, b2);
//     // assert.equal(b1.length, b2.length);

//     // for (var i = 0; i < testArray.length; i++)
//     //     assert.equal(testArray[i], b2.get(testArray.length-i-1));
// };

// exports.testByteArraySort = function() {
//     var testArray = [];
//     for (var i = 0; i < 1000 ; i++)
//         testArray.push(Math.floor(Math.random()*255));

//     var a = new ByteArray(testArray);
//     a.sort()

//     for (var i = 1; i < a.length; i++)
//         assert.ok(a.get(i-1) <= a.get(i), "index="+i+"("+a.get(i-1)+","+a.get(i)+")");
// };

// exports.testByteArraySortCustom = function() {
//     var testArray = [];
//     for (var i = 0; i < 1000; i++)
//         testArray.push(Math.floor(Math.random()*256));

//     var a = new ByteArray(testArray);
//     a.sort(function(o1, o2) { return o2-o1; });

//     for (var i = 1; i < a.length; i++)
//         assert.ok(a.get(i-1) >= a.get(i), "index="+i+"("+a.get(i-1)+","+a.get(i)+")");
// };

// exports.testByteArraySlice = function() {
//     var a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
//     var b = new ByteArray(a);
//     var s = b.slice();
//     assert.ok(s instanceof ByteArray);
//     assert.equal(10, s.length);
//     assert.deepEqual(a, s.toArray());

//     s = b.slice(3, 6);
//     assert.ok(s instanceof ByteArray);
//     assert.equal(3, s.length);
//     assert.deepEqual(a.slice(3, 6), s.toArray());

//     s = b.slice(3, 4);
//     assert.ok(s instanceof ByteArray);
//     assert.equal(1, s.length);
//     assert.deepEqual(a.slice(3, 4), s.toArray());

//     s = b.slice(3, 3);
//     assert.ok(s instanceof ByteArray);
//     assert.equal(0, s.length);
//     assert.deepEqual(a.slice(3, 3), s.toArray());

//     s = b.slice(3, 2);
//     assert.ok(s instanceof ByteArray);
//     assert.equal(0, s.length);
//     assert.deepEqual(a.slice(3, 2), s.toArray());

//     s = b.slice(7);
//     assert.ok(s instanceof ByteArray);
//     assert.equal(3, s.length);
//     assert.deepEqual(a.slice(7), s.toArray());

//     s = b.slice(3, -2);
//     assert.ok(s instanceof ByteArray);
//     assert.equal(5, s.length);
//     assert.deepEqual(a.slice(3, -2), s.toArray());

//     s = b.slice(-2);
//     assert.ok(s instanceof ByteArray);
//     assert.equal(2, s.length);
//     assert.deepEqual(a.slice(-2), s.toArray());

//     s = b.slice(50);
//     assert.ok(s instanceof ByteArray);
//     assert.equal(0, s.length);
//     assert.deepEqual(a.slice(50), s.toArray());

//     s = b.slice(-100, 100);
//     assert.ok(s instanceof ByteArray);
//     assert.equal(10, s.length);
//     // assert.deepEqual(a.slice(-100, 100), s.toArray());

//     s = b.slice("foo");
//     assert.ok(s instanceof ByteArray);
//     assert.equal(10, s.length);
//     assert.deepEqual(a.slice("foo"), s.toArray());

//     s = b.slice("foo", "bar");
//     assert.ok(s instanceof ByteArray);
//     //assert.equal(0, s.length);
//     //assert.deepEqual(a.slice("foo", "bar"), s.toArray());
// };

// exports.testSplit = function() {
//     var b1 = new ByteArray([0,1,2,3,4,5]), a1;
    
//     a1 = b1.split([]);
//     assert.equal(1, a1.length);
//     assert.ok(a1[0] instanceof ByteArray);
//     assert.equal(6, a1[0].length);
//     assert.equal(0, a1[0].get(0));
//     assert.equal(5, a1[0].get(5));
    
//     a1 = b1.split([2]);
//     assert.equal(2, a1.length);
//     assert.ok(a1[0] instanceof ByteArray);
//     assert.equal(2, a1[0].length);
//     assert.equal(0, a1[0].get(0));
//     assert.equal(1, a1[0].get(1));
//     assert.equal(3, a1[1].length);

//     assert.equal(3, a1[1].get(0));
//     assert.equal(5, a1[1].get(2));
    
//     a1 = b1.split([2], { includeDelimiter : true});
//     assert.equal(2, a1.length);
//     assert.ok(a1[0] instanceof ByteArray);

//     assert.equal(3, a1[0].length);
//     assert.equal(0, a1[0].get(0));
//     assert.equal(1, a1[0].get(1));
    
//     a1 = b1.split(new ByteArray([2,3]));
//     assert.equal(2, a1.length);
//     assert.ok(a1[0] instanceof ByteArray);

//     assert.equal(2, a1[0].length);    
//     assert.equal(0, a1[0].get(0));
//     assert.equal(1, a1[0].get(1));

//     assert.equal(2, a1[1].length);
//     assert.equal(4, a1[1].get(0));
//     assert.equal(5, a1[1].get(1));
// };

// exports.testByteArrayForEach = function() {

//     var b = new ByteArray([2, 3, 4, 5]),
//             log = [],
//             item;
//      var thisObj = {};

//      b.forEach(function() {
//          log.push({
//              thisObj: this,
//              args: arguments
//          });
//      }, thisObj);

//      assert.equal(4, log.length, "block called for each item");

//      item = log[0];
//      assert.ok(thisObj === item.thisObj, "block called with correct thisObj");
//      assert.equal(3, item.args.length, "block called with three args");
//      assert.equal(b.get(0), item.args[0], "block called with correct item 0");

//      item = log[3];
//      assert.equal(b.get(3), item.args[0], "block called with correct item 3");
// };

// exports.testByteArrayConcat = function() {

//     var b = new ByteArray();

//     var b1 = b.concat(new ByteArray([1,2,3]));
//     assert.equal(3, b1.length);
//     assert.equal(1, b1.get(0));
//     assert.equal(2, b1.get(1));
//     assert.equal(3, b1.get(2));

// //    var b2 = b1.concat(new ByteString([4,5,6]));
// //     assert.equal(6, b2.length);
// //     assert.equal(1, b2.get(0));
// //     assert.equal(3, b2.get(2));
// //     assert.equal(4, b2.get(3));
// //     assert.equal(6, b2.get(5));

// //     var b3 = b2.concat([b, b1, b2, new ByteString(), new ByteArray()]);
// //     assert.equal(b.length + b1.length + b2.length + b2.length, b3.length);

// };

// exports.testByteArrayPush = function() {
//     var b1 = new ByteArray();
//     assert.equal(3, b1.push(1,2,3));
//     assert.equal(3, b1.length);
//     assert.equal(1, b1.get(0));
//     assert.equal(2, b1.get(1));
//     assert.equal(3, b1.get(2));
    
//     var b2 = new ByteArray([7, 8, 9]);
//     assert.equal(6, b1.push.apply(b1, b2.toArray()));
//     assert.equal(6, b1.length);
//     assert.equal(7, b1.get(3));
//     assert.equal(8, b1.get(4));
//     assert.equal(9, b1.get(5));
// };

// exports.testByteArrayUnshift = function() {
//     var b1 = new ByteArray([10, 43]);
//     assert.equal(5, b1.unshift(9, 112, 67));
//     assert.equal(5, b1.length);
//     assert.equal(9, b1.get(0));
//     assert.equal(112, b1.get(1));
//     assert.equal(67, b1.get(2));
//     assert.equal(10, b1.get(3));
//     assert.equal(43, b1.get(4));

//     var b2 = new ByteArray();
//     assert.equal(5, b2.unshift.apply(b2, b1.toArray()));
//     assert.equal(5, b2.length);
//     assert.equal(9, b2.get(0));
//     assert.equal(112, b2.get(1));
//     assert.equal(67, b2.get(2));
//     assert.equal(10, b2.get(3));
//     assert.equal(43, b2.get(4));
//     assert.equal(6, b2.unshift.call(b2, 20));
//     assert.equal(20, b2.get(0));
// };

// exports.testByteArraySplice = function() {
//     var b = new ByteArray([1,2,3,4,5,6,7,8,9]);
//     b.splice(2,3,7);
// }

// exports.testByteArrayOther = function() {
//     var b = new ByteArray([1,2,3,4,5,6,7,8,9]);
//     b.push(1);
//     assert.equal(b.length,10);
//     assert.equal(b.get(9),1);
//     b.pop();
//     assert.equal(9,b.get(8));
    
//     var b2 = new ByteArray(b);
//     b2.fill(0);
//     assert.equal(b.length, b2.length);

//     b.extendRight(8,7,6);
//     b.extendLeft(0,0,1);

//     assert.equal(15,b.length);

//     function isBigEnough(element, index, array) {
// 	return (element >= 5);
//     }
    
//     var b2 = b.filter(isBigEnough).sort();
//     assert.equal(7,b2[3]);

//     var total1 = b2.reduceRight(function(a, b) {  
// 	return a + b;  
//     });

//     var total2 = b2.reduce(function(a, b) {  
// 	return a + b;  
//     });

//     assert.equal(56,total1);
//     assert.equal(56,total2);
// }

// exports.testByteArrayConstructorEncodings = function() {
//     // ByteString(string, charset)
//     // Convert a string. The ByteString will contain string encoded with charset.
//     var testString = "hello world";
//     var b = new ByteArray(testString, "UTF-8");
//     assert.strictEqual(testString.length, b.length);
//     b.length = 678;
//     assert.strictEqual(678, b.length);
//     assert.strictEqual(testString.charCodeAt(0), b.get(0));
//     assert.strictEqual(testString.charCodeAt(testString.length-1), b.get(testString.length-1));
//     assert.strictEqual(0, b.get(677));
// };

// exports.testToByteArrayEncodings = function() {
//     var testString = "I â™Ą JS";
//     assert.strictEqual(testString, new ByteArray(testString, "UTF-8").toByteArray("UTF-8", "UTF-16").decodeToString("UTF-16"));
// };

// exports.testToByteStringEncodings = function() {
//     var testString = "I â™Ą JS";
//     assert.strictEqual(testString, new ByteArray(testString, "UTF-8").toByteString("UTF-8", "UTF-16").decodeToString("UTF-16"));
// };

// exports.testToArrayEncodings = function() {
//     var a1;

//     a1 = new ByteArray("\u0024\u00A2\u20AC", "UTF-8").toArray("UTF-8");

//     assert.strictEqual(3, a1.length);
//     assert.strictEqual(0x24, a1[0]);
//     assert.strictEqual(0xA2, a1[1]);
//     //assert.strictEqual(0x20AC, a1[2]); //0-255! wtf!

   
//     a1 = new ByteArray("\u0024\u00A2\u20AC", "UTF-8").toArray("UTF-16");


//     assert.strictEqual(3, a1.length);
//     assert.strictEqual(0x24, a1[0]);
//     assert.strictEqual(0xA2, a1[1]);
//     //assert.strictEqual(0x20AC, a1[2]); //0-255! wtf!
// };

// exports.testDecodeToString = function() {
//     assert.strictEqual("hello world", new ByteArray("hello world", "UTF-8").decodeToString("UTF-8"));

//     assert.strictEqual("I â™Ą JS", new ByteArray("I â™Ą JS", "UTF-8").decodeToString("UTF-8"));

//     assert.strictEqual("\u0024", new ByteArray([0x24]).decodeToString("UTF-8"));
//     assert.strictEqual("\u00A2", new ByteArray([0xC2,0xA2]).decodeToString("UTF-8"));
//     assert.strictEqual("\u20AC", new ByteArray([0xE2,0x82,0xAC]).decodeToString("UTF-8"));
//     // FIXME:
//     //assert.strictEqual("\u10ABCD", (new ByteArray([0xF4,0x8A,0xAF,0x8D])).decodeToString("UTF-8"));

//     assert.strictEqual("\u0024", new ByteArray("\u0024", "UTF-8").decodeToString("UTF-8"));
//     assert.strictEqual("\u00A2", new ByteArray("\u00A2", "UTF-8").decodeToString("UTF-8"));
//     assert.strictEqual("\u20AC", new ByteArray("\u20AC", "UTF-8").decodeToString("UTF-8"));
//     assert.strictEqual("\u10ABCD", new ByteArray("\u10ABCD", "UTF-8").decodeToString("UTF-8"));

//     assert.strictEqual("\u0024", new ByteArray("\u0024", "UTF-16").decodeToString("UTF-16"));
//     assert.strictEqual("\u00A2", new ByteArray("\u00A2", "UTF-16").decodeToString("UTF-16"));
//     assert.strictEqual("\u20AC", new ByteArray("\u20AC", "UTF-16").decodeToString("UTF-16"));
//     assert.strictEqual("\u10ABCD", new ByteArray("\u10ABCD", "UTF-16").decodeToString("UTF-16"));
// };

if (require.main == module.id)
    //require("os").exit(require("test/runner").run(exports));
    require('test').run(exports);


var assert = require('assert');
var events = require('events');
var console = require("console");

var e = new events.EventEmitter();

var events_new_listener_emited = [];
var times_hello_emited = 0;

// sanity check
assert.equal(e.addListener, e.on);

e.on('newListener', function(event, listener) {
    console.log('newListener: ' + event);
    events_new_listener_emited.push(event);
});

e.on('hello', function(a, b) {
  console.log('hello');
  times_hello_emited += 1;
  assert.equal('a', a);
  assert.equal('b', b);
});

console.log('start');

e.emit('hello', 'a', 'b');


// just make sure that this doesn't throw:
var f = new events.EventEmitter();
f.setMaxListeners(0);


// process.on('exit', function() {
assert.deepEqual(['hello'], events_new_listener_emited);
assert.equal(1, times_hello_emited);
// });

 
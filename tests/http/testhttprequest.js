var assert  = require("assert");
var console = require("console");
var http    = require("http");
var system  = require("system");

exports.testClient = function()
{
    // var rqst = http.request({host:"10.57.4.69", method:'GET', path:"/first_data/", 
    //                          headers:{
    //                              "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0",
    //                              "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    //                              "Accept-Language":"en-US,en;q=0.5",
    //                              "Accept-Encoding":"gzip, deflate",
    //                              "Connection":"keep-alive"
    //                              /*"Content-Type":"application/x-www-form-urlencoded",
    //                              "Content-Length": "82"*/}});

    var rqst = http.request({host:"wks-czc2152xfw", method:'GET', path:"/up/uploads/cygwin.zip", 
                             headers:{
                                 "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0",
                                 "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                                 "Accept-Language":"en-US,en;q=0.5",
                                 "Accept-Encoding":"gzip, deflate",
                                 "Connection":"keep-alive"
                                 /*"Content-Type":"application/x-www-form-urlencoded",
                                   "Content-Length": "82"*/}});

    console.assert(rqst != undefined);
    //rqst.end("_method=POST&data%5BUser%5D%5Busername%5D=bem&data%5BUser%5D%5Bpassword%5D=1234567".toByteArray());
    //console.dir(rqst.headers);

    rqst.response.connect( function (resp) {
        var data = 0;

        resp.data.connect(function (d) {
             console.write("");
        });

        resp.finished.connect(function () {
            require("test").run(exports);
        });

        //resp.errror.connect(function( ) {
        //       console.writeln("we have error");
        //});

    });

    rqst.end();
}

if (require.main == module.id)
    require("test").run(exports);


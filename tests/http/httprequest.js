var console = require("console");
var http    = require("http");

//Create an instance of ClientRequest
var rqst    = http.request({host:"10.57.4.72", port:13579, method:'GET', path:"/",
                            headers:{
                                "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0",
                                "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                                "Accept-Language":"en-US,en;q=0.5",
                                "Accept-Encoding":"gzip, deflate",
                                "Connection":"keep-alive"}});

rqst.response.connect(function( resp ) {
    
    //resp is instance of ClientResponse

    resp.data.connect(function (data) {
        //write the data chunk to the console
        console.write(data.decodeToString());
    });
    
    resp.finished.connect( function() {
        //delete the request (in case the Garbage collector failed)
        rqst.deleteLater();
    });

    resp.error.connect(function(err) {
        console.writeln("Error:"+err);
    });
});


//END THE REQUEST (SENDS THE REQUEST OUT)
rqst.end();

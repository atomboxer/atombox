
var assert  = require("assert");
var console = require("console");
var http    = require("http");
var system  = require("system");

var idx = 0;
exports.testServer = function()
{
    var server = new http.HttpServer();
    server.listen(12345);
    print(server.isListening())
	
    server.requestReady.connect(function (rqst, resp) {
        print("-------------------------");
        print("rqst.url    :"+rqst.url);
        print("rqst.method :"+rqst.method);
        console.dir(rqst.headers);
        print("rqst.httpVersion:"+rqst.httpVersion);

        rqst.data.connect(function(data) {
            print("rqst.data emitted");
        });

        resp.finished.connect(function() {
             idx++;
             print("DDOOOONE");
        });

        rqst.end.connect(function() {
            resp.writeHeader(200, "bla bla", {'Content-Type': 'text/plain'});
            resp.write("Ana are mere");
            resp.write("Tata are pere".toByteArray());
            resp.end("bla bla:"+ idx);            
        });

        rqst.close.connect(function() {
            print("rqst.close emitted");
        });

        print("done requestReady");
    });
}

if (require.main == module.id)
    require("test").run(exports);

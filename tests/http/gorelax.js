var assert  = require("assert");
var console = require("console");
var http    = require("http");
var system  = require("system");

// var rqst = http.request({host:"127.0.0.1", port:13562, method:'GET', path:"/",
//                          headers:{
//                              "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0",
//                              "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
//                              "Accept-Language":"en-US,en;q=0.5",
//                              "Accept-Encoding":"gzip, deflate",
//                              "Connection":"keep-alive"
//                              /*"Content-Type":"application/x-www-form-urlencoded",
//                                "Content-Length": "82"*/}});


// var rqst = http.request({host:"10.57.4.72", port:13579, method:'GET', path:"/",
//                          headers:{
//                              "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0",
//                              "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
//                              "Accept-Language":"en-US,en;q=0.5",
//                              "Accept-Encoding":"gzip, deflate",
//                              "Connection":"keep-alive"
//                              /*"Content-Type":"application/x-www-form-urlencoded",
//                                "Content-Length": "82"*/}});

// var rqst = http.request({host:"10.57.4.72", port:13579, method:'DELETE', path:"/some_db/",
//                          headers:{
//                              "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0",
//                              "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
//                              "Accept-Language":"en-US,en;q=0.5",
//                              "Accept-Encoding":"gzip, deflate",
//                              "Connection":"keep-alive"}});

// rqst.end();

// var rqst = http.request({host:"10.57.4.72", port:13579, method:'PUT', path:"/some_db/",
//                          headers:{
//                              "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0",
//                              "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
//                              "Accept-Language":"en-US,en;q=0.5",
//                              "Accept-Encoding":"gzip, deflate",
//                              "Connection":"keep-alive"}});
// rqst.end();


function send_request() {
    var rqst = http.request({host:"127.0.0.1", port:13579, method:'POST', path:"/some_db/",
                             headers:{
                                 "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0",
                                 "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                                 "Accept-Language":"en-US,en;q=0.5",
                                 "Accept-Encoding":"gzip, deflate",
                                 "Connection":"keep-alive",
                                 "Content-Type":"application/json"
                                 /*"Content-Length": "245"*/}});

    rqst.write('{"hello":"world"}');

    rqst.response.connect(function( resp ) {

        resp.data.connect(function (d) {
            console.write(d.decodeToString());
        });
        
        resp.finished.connect( function() {
            rqst.deleteLater();
            send_request();
        });

        resp.error.connect(function(err) {
            //console.writeln("lelele:"+err);
        });
    });

    rqst.destroyed.connect( function() {
        system.exit(0);
    });

    rqst.end();
}

send_request();
send_request();
send_request();
send_request();
send_request();
send_request();

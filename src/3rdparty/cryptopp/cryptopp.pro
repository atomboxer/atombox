
isEmpty(ATOMBOXER_PRI_INCLUDED) {
    include(../../../atomboxer.pri)
}

DESTDIR = $$ATOMBOXER_SOURCE_TREE/lib/
DESTDIR_TARGET = $$ATOMBOXER_SOURCE_TREE/lib/
tandem {
    TARGET = ZCRYPTPP
} else {
    TARGET = cryptopp
}
QT          = core
QT         -= gui

TEMPLATE  = lib
!tandem {
    CONFIG += static
}
CONFIG   += console
CONFIG   -= debug
CONFIG   += release

mac:CONFIG -= app_bundle

SOURCES += \
    cryptlib.cpp                \
    cpu.cpp                     \
    esign.cpp                   \
    randpool.cpp                \
    sharkbox.cpp                \
    blowfish.cpp                \
    base32.cpp                  \
    files.cpp                   \
    fips140.cpp                 \
    dsa.cpp                     \
    dll.cpp                     \
    eprecomp.cpp                \
    vmac.cpp                    \
    hrtimer.cpp                 \
    md2.cpp                     \
rc5.cpp                     \
    mars.cpp                    \
    queue.cpp                   \
    xtrcrypt.cpp                \
    gf256.cpp                   \
    hmac.cpp                    \
    authenc.cpp                 \
    idea.cpp                    \
    gcm.cpp                     \
    squaretb.cpp                \
#    pkcspad.cpp                 \
emsa2.cpp                   \
    sha.cpp                     \
    network.cpp                 \
    eax.cpp                     \
    rijndael.cpp                \
    tea.cpp                     \
    dessp.cpp                   \
    algparam.cpp                \
    crc.cpp                     \
    elgamal.cpp                 \
    mqueue.cpp                  \
    zlib.cpp                    \
    dh2.cpp                     \
    camellia.cpp                \
    strciphr.cpp                \
    oaep.cpp                    \
    ecp.cpp                     \
    marss.cpp                   \
    adler32.cpp                 \
    wait.cpp                    \
    iterhash.cpp                \
    safer.cpp                   \
    gf2n.cpp                    \
    default.cpp                 \
    rc2.cpp                    \
    polynomi.cpp               \
    square.cpp                 \
    ida.cpp                    \
    shark.cpp                  \
    xtr.cpp                    \
    shacal2.cpp                \
    cmac.cpp                   \
    serpent.cpp                \
    eccrypto.cpp               \
    rdrand.cpp                 \
    misc.cpp                   \
    ripemd.cpp                 \
    pubkey.cpp                 \
    trdlocal.cpp               \
    salsa.cpp                  \
    seal.cpp                   \
    luc.cpp                    \
    ttmac.cpp                  \
    cbcmac.cpp                 \
    gf2_32.cpp                 \
    tigertab.cpp               \
    mqv.cpp                    \
    ccm.cpp                    \
    dh.cpp                     \
    md4.cpp                    \
    whrlpool.cpp               \
    asn.cpp                    \
    rw.cpp                     \
    cast.cpp                   \
    tiger.cpp                  \
    rng.cpp                    \
    zinflate.cpp               \
    pssr.cpp                   \
    algebra.cpp                \
    sha3.cpp                   \
    rc6.cpp                    \
    base64.cpp                 \
    zdeflate.cpp               \
    channels.cpp               \
    basecode.cpp               \
    des.cpp                    \
    blumshub.cpp               \
    ec2n.cpp                   \
    gfpcrypt.cpp               \
    gost.cpp                   \
    casts.cpp                  \
    modes.cpp                  \
    panama.cpp                 \
    nbtheory.cpp               \
    md5.cpp                    \
    socketft.cpp               \
    twofish.cpp                \
    tftables.cpp               \
    rsa.cpp                    \
    rdtables.cpp               \
    sosemanuk.cpp              \
    3way.cpp                   \
    gzip.cpp                   \
    rabin.cpp                  \
    bfinit.cpp                 \
    filters.cpp                \
    integer.cpp                \
    seed.cpp                   \
    wake.cpp                   \
    skipjack.cpp               \
    osrng.cpp                  \
    arc4.cpp                   \
    hex.cpp                    \
    pem-com.cpp                \
    pem-rd.cpp                 \
    pem-wr.cpp

!tandem {
    SOURCES += fipstest.cpp             
}

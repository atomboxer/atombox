TEMPLATE  = subdirs

isEmpty(ATOMBOXER_PRI_INCLUDED) {
	include(../../atomboxer.pri)
}


SUBDIRS   += \
    tufao \

#!tandem {
    SUBDIRS += cryptopp
#}

!isEmpty(USE_SCRIPT_CLASSIC) {
    SUBDIRS += qtscriptclassic
}

SUBDIRS+= qscripttools



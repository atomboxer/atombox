#include "ast.h"
#include "astvisitor.h"

#include <assert.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

#define DEBUG_DUMP_AST_DOT 0

namespace AST {

void Node::accept(Visitor *visitor)
{
    if (visitor->preVisit(this)) {
        accept0(visitor);
    }
    visitor->postVisit(this);
}

void Node::accept(Node *node, Visitor *visitor)
{
    if (node)
        node->accept(visitor);
}

/*
 *  literal
 */
void Literal::accept0(Visitor *visitor)
{   
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  literal_string
 */
void StringLiteral::accept0(Visitor *visitor)
{   
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  literal_string
 */
void NumericLiteral::accept0(Visitor *visitor)
{   
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  constraint
 */
void Constraint::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  constraint_list
 */
void ConstraintList::accept0(Visitor *visitor)
{
   if (visitor->visit(this)) {
        for (ConstraintList *it = this; it; it = it->next_) {
            accept(it->constraint_, visitor);
        }
    }
    visitor->endVisit(this);
}


/*
 *  constraint_range
 */
void ConstraintRange::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}


/*
 *  constraint_regex
 */
void ConstraintRegex::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  constraint_value
 */
void ConstraintValue::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  constraint_literal
 */
void ConstraintLiteral::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}


/*
 *  const_statement
 */
void ConstStatement::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }

    visitor->endVisit(this);
}


/*
 *  statement_list
 */
void StatementList::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
        for (StatementList *it = this; it; it = it->next_) {
            accept(it->statement_, visitor);
        }
    }
    visitor->endVisit(this);
}


/*
 *  pic_clause
 */
void PicOrType::accept0(Visitor *visitor)
{
    if (this->kind_ == AST::Node::k_pic_clause) {
        if (visitor->visit(dynamic_cast<PicClause*>(val_))) {
        }
        visitor->endVisit(dynamic_cast<PicClause*>(val_));
    } else if (this->kind_ == AST::Node::k_pic_comp_clause) {
        if (visitor->visit(dynamic_cast<PicCompClause*>(val_))) {
        }
        visitor->endVisit(dynamic_cast<PicCompClause*>(val_));
    }
    else {
        Q_ASSERT(0); //not implemented
    }
}

/*
 *  pic_clause
 */
void PicClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    
    visitor->endVisit(this);
}

/*
 *  pic_comp_clause
 */
void PicCompClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    
    visitor->endVisit(this);
}


/*
 *  pic_byte_clause
 */
void PicBinaryClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    
    visitor->endVisit(this);
}


/*
 *  pic_bit_clause
 */
void PicBitClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    
    visitor->endVisit(this);
}


/*
 *  type_clause
 */
void TypeClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    
    visitor->endVisit(this);
}

/*
 *  type_clause
 */
void TypeLiteralClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    
    visitor->endVisit(this);
}

/*
 *  type_clause
 */
void TypeStarClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    
    visitor->endVisit(this);
}


/*
 *  groupdef_ebcdic_clause
 */
void GroupDefEbcdicClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    
    visitor->endVisit(this);
}

/*
 *  groupdef_highlow_clause
 */
void GroupDefHighLowClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    
    visitor->endVisit(this);
}

/*
 *  groupdef_lowhigh_clause
 */
void GroupDefLowHighClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    
    visitor->endVisit(this);
}

/*
 *  groupdef_clauses
 */
void GroupDefClauseList::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
        for (GroupDefClauseList *it = this; it; it = it->next_) {
            accept(it->groupdef_clause_, visitor);
        }
    }
    visitor->endVisit(this);
}


/*
 *  LineItemList
 */
void LineItemList::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
        for (LineItemList *it = this; it; it = it->next_) {
            accept(it->line_item_, visitor);
        }
    }
    visitor->endVisit(this);
}

/*
 *  line_item
 */
void LineItem::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
        accept(clause_list_, visitor);
    }
    visitor->endVisit(this);
}

/*
 *  line_item_clauses
 */
void LineItemClauseList::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
        for (LineItemClauseList *it = this; it; it = it->next_) {
            //accept(it->line_item_clause_, visitor);
        }
    }
    visitor->endVisit(this);
}

/*
 *  clause_redefines
 */
void LineItemNotImplementedClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_redefines
 */
void LineItemRedefinesClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_occurs
 */
void LineItemOccursClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_occurs
 */
void LineItemOccursLiteralClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_occurs_depending_on
 */
void LineItemOccursDependingOnClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_as
 */
void LineItemAsClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_display
 */
void LineItemDisplayClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_default
 */
void LineItemDefaultClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_default_empty
 */
void LineItemDefaultEmptyClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_lpad
 */
void LineItemLPadClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_rpad
 */
void LineItemRPadClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_key
 */
void LineItemKeyClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_key_is
 */
void LineItemKeyIsClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}


/*
 *  clause_key_is_list
 */
void LineItemKeyIsClauseList::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
        for (LineItemKeyIsClauseList *it = this; it; it = it->next_) {
            accept(it->key_is_clause_, visitor);
        }
    }
    visitor->endVisit(this);
}

/*
 *  clause_vlength
 */
void LineItemVLengthClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_vlength_inclusive
 */
void LineItemVLengthInclusiveClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_vlength_allinclusive
 */
void LineItemVLengthAllInclusiveClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}


/*
 *  clause_vlength_delimited
 */
void LineItemVLengthDelimiterClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_isobitmap
 */
void LineItemIsoBitmapClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_packed_decimal
 */
void LineItemPackedDecimalClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}

/*
 *  clause_choice
 */
void LineItemChoiceClause::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
    }
    visitor->endVisit(this);
}


/*
 *  Field Definition
 */
void FieldDefStatement::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
        accept(clause_list_, visitor);
    }
    visitor->endVisit(this);
}


/*
 *  Enum Definition
 */
void EnumDefStatement::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
        accept(constraint_list_, visitor);
    }
    visitor->endVisit(this);
}

/*
 *  Group Definition
 */
void GroupDefStatement::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
        accept(line_item_list_, visitor);
    }
    visitor->endVisit(this);
}

/*
 *  Record Definition
 */
void RecordDefStatement::accept0(Visitor *visitor)
{
    if (visitor->visit(this)) {
        accept(line_item_list_, visitor);
        accept(keys_, visitor);
    }
    visitor->endVisit(this);
}

}  //namespace


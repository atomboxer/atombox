#include "itemcontainer.h"

#include <QtCore/QDebug>
#include <QtCore/QRegExp>

#include "constraintrange.h"
#include "constraintregex.h"
#include "constraintstring.h"
#include "constraintvalue.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

ContainerItem::ContainerItem( int kind )
    : CodeModelItemBase( kind )
{
    parent_ = NULL;
    length_ = 0;
    num_occurs_ = 1;
    dep_occurs_  = QString();
    dep_occurs_from_  = -1;
    dep_occurs_to_  = -1;
    choice_ = false;
    redefine_ = false;
    record_ = false;
    key_    = false;
    constraints_ = QList<Constraint *>();
}

ContainerItem::~ContainerItem()
{
   qDeleteAll(items_);
   items_.clear();

   qDeleteAll(redefines_);
   redefines_.clear();
   
   qDeleteAll(constraints_);
   constraints_.clear();
}

void
ContainerItem::add(ContainerItem *li)
{
    li->setParent(this);
    items_.push_back(li);
}

void
ContainerItem::addRedefine(ContainerItem *redefine)
{
    redefine->setRedefine(true);
    redefines_.push_back(redefine);
}

ContainerItem*
ContainerItem::findChild( QString name)
{
    ContainerItem *it;
    foreach(it, items_) {
    if (it->name() == name) return it;
    }
    return 0;
}

QList <ContainerItem*>
ContainerItem::childs() const
{
    return items_;
}

Definition
ContainerItem::definition() const
{
    return definition_;
}

bool
ContainerItem::hasChilds() const
{
    return !items_.isEmpty();
}

bool
ContainerItem::hasOccursDepend() const
{
    return (dep_occurs_.isEmpty())?false:true;
}

bool
ContainerItem::hasOccursNumber() const
{
    return (num_occurs_==1)?false:true;
}

bool
ContainerItem::hasRedefines() const
{
    return !redefines_.isEmpty();
}

void
ContainerItem::addConstraintRange( int from, int to, QString display )
{
    constraints_ << (Constraint *)new ConstraintRange(from, to, display);
}

void
ContainerItem::addConstraintValue( QVariant value, QString display )
{
    constraints_ << (Constraint*)new ConstraintValue(value, display);
}

void
ContainerItem::addConstraintRegex( QString value, QString display )
{
    constraints_ << (Constraint*)new ConstraintRegex(value, display);
}

bool
ContainerItem::isDefined()
{
    return false;
}

unsigned int
ContainerItem::length()
{
    ContainerItem *it;
    length_ = 0;
    foreach(it, items_) {
    length_+=it->length() * num_occurs_;
    }
    return length_;
}

ContainerItem *
ContainerItem::nextSibling()
{
    if ( !parent() )
      return NULL;

    int idx = 0;
    ContainerItem *it;
    foreach(it, parent()->childs()) {
if (it->name() == name() ) {
    if ( idx<parent()->childs().size() - 1) {
return  parent()->childs().at(idx+1);
    }
}
idx++;
    }
    return NULL;
}

unsigned int
ContainerItem::occursNumber() const
{
    return num_occurs_;
}

QString
ContainerItem::occursDepend() const
{
    return dep_occurs_;
}

int
ContainerItem::occursDependFrom() const
{
    return dep_occurs_from_;
}

int
ContainerItem::occursDependTo() const
{
    return dep_occurs_to_;
}

ContainerItem*
ContainerItem::parent() const
{
    if (!parent_)
    return NULL;

    return parent_;
}

ContainerItem *
ContainerItem::prevSibling()
{
    if ( !parent() )
      return NULL;

    int idx = 0;
    ContainerItem *it;
    foreach(it, parent()->childs()) {
    if (it->name() == name() ) {
        if ( idx < parent()->childs().size() - 1) {
        return  parent()->childs().at(idx - 1);
        }
    }
    idx++;
    }
    return NULL;
}

QList <ContainerItem*>
ContainerItem::redefines() const
{
    return redefines_;
}

void
ContainerItem::removeChild( QString name )
{
    ContainerItem *it;
    foreach(it, items_) {
    if (it->name() == name) {
        items_.removeAll(it);
        return;
    }
    }
}

void
ContainerItem::setParent(ContainerItem *parent)
{
    parent_ = parent;
}

void
ContainerItem::setOccursDepend(QString dep, int from, int to)
{
    dep_occurs_      = dep;
    dep_occurs_from_ = from;
    dep_occurs_to_   = to;
    //Q_ASSERT(from <= to);
    //Q_ASSERT(!dep.isEmpty());
}

void
ContainerItem::setOccursNumber(unsigned int num)
{
    num_occurs_ = num;
}


CodeModelItemBase*
ContainerItem::clone()
{
    qFatal("should not get here");
}

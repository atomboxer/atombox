#ifndef CSVPARSER
#define CSVPARSER

#include <QtCore/QByteArray>
#include <QtCore/QMap>
#include <QtCore/QMultiMap>
#include <QtCore/QString>
#include <QtCore/QUrl>
#include <QtCore/QList>

#include "error.h"
#include "codemodel.h"

struct RecTable
{ 
    short    GenTableId;
    QString  TableName;
    float    TableVersion;
    QString  SmallName;
    QString  KeyName;
};

struct RecElement
{
    short    GenTableId;
    short    LogicalCopyNum;
    short    PhysOrdinal;
    QString  ElmtName;
    QString  BaseTypeName;
    short    BaseTypeSize;
    bool     KeyFlg;
};

class DDLParser;
class MDBCSVParser
{
public:
    MDBCSVParser();
    virtual ~MDBCSVParser();

    bool parse(const QString &data, const QUrl &url );

    void setScriptFile(const QString &filename) { script_file_ = filename; }
    QString scriptFile() const { return script_file_; }
    DDLCodeModel *model() const { return model_; }

private:
    void helper_generateModel();
    void helper_generateDefinition(RecTable *);

    QString script_file_;
    QList<DDLError> errors_;
    DDLCodeModel *model_;

    QMap<short/*tableid*/,RecTable*> tables_;
    QMultiMap<QString, short/*id*/>  tablesByName_;
    QList <QString>                  tableNames_;
    QMultiMap<short/*tableid*/, RecElement*> elements_;
};

#endif //CSVPARSER

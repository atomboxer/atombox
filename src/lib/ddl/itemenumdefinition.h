#ifndef ENUMDEFINITIONITEM_H
#define ENUMDEFINITIONITEM_H

#include "itembase.h"
#include "ast.h"

class EnumDefinitionItem : public CodeModelItemBase
{
public:
    EnumDefinitionItem(int kind = kindEnumDefinition);    

    void setConstraintList (AST::ConstraintList *c) { ast_constraint_list_ = c; }
    AST::ConstraintList *astConstraintList() const { return ast_constraint_list_; }

    virtual CodeModelItemBase* clone();
private:
    AST::ConstraintList *ast_constraint_list_;
};

#endif //GROUPDEFINITIONITEM_H

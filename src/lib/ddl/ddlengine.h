#ifndef DDL_ENGINE_H
#define DDL_ENGINE_H

#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QVariant>
#include <QtCore/QString>
#include <QtCore/QVariant>
#include <QtCore/QList>
#include <QtCore/QPair>

#include "boxcomponent.h"

class DDLASTParser;
class MDBCSVParser;

class DDLEngine/* : public QObject*/
{
    //Q_OBJECT

public:
    static DDLEngine* instance();
    DDLEngine(QObject *parent = 0);
    virtual ~DDLEngine();

    QPair< QList<BoxComponent*>, QList< QPair <QString, QVariant> > >
       evaluate_ddl(const QString &text,
                        QString fileName = "<anonymous ddl>");
    QList<BoxComponent*> evaluate_mdb(const QString &text,
                                      QString fileName = "<anonymous mdbcsv>");

    DDLASTParser *astparser() const {return astparser_;}
    MDBCSVParser *csvparser() const {return csvparser_;}
private:
    DDLASTParser *astparser_;
    MDBCSVParser *csvparser_;

    static DDLEngine *instance_;
};

#endif

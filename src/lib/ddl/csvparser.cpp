#include <QUrl>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QRegExp>
#include <QtCore/QByteArray>
#include <QtCore/QTextStream>

#include <QtCore/QDebug>
#include <QtCore/QVariant>

#include "codemodel.h"
#include "csvparser.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

static 
bool less_than(const RecElement *r1, const RecElement *r2)
{
    return r1->PhysOrdinal < r2->PhysOrdinal;
}

/*
 * MDBCSV Parser
 */
MDBCSVParser::MDBCSVParser()
{
    model_ = new DDLCodeModel();
}

MDBCSVParser::~MDBCSVParser()
{
    if (model_)
        delete model_;
}

bool
MDBCSVParser::parse(const QString &data, const QUrl &url)
{
    QString fileName = url.toString();
    script_file_ = fileName;

    //
    // Capture the Tables
    //
    QRegExp rx;
    rx.setPattern("\"Tables\"(.*)\"Elements\"");
    rx.indexIn(data);

    QString tables = rx.cap(1).remove(" ");
    if (tables.length() == 0)
        return false;


    QStringList lines = tables.split("\n");
    foreach (QString line, lines) {
        QStringList values = line.split(",");
        if (values.isEmpty() || values.size() < 5)
            continue;
        
        bool ok;
        RecTable *obj      = new RecTable;
        obj->GenTableId    = values[1].toUShort(&ok);
        obj->TableName     = values[2].remove("\"");
        obj->TableVersion  = values[3].remove("\"").toFloat(&ok);
        obj->SmallName     = values[4].remove("\"");
        obj->KeyName       = values[9].remove("\"");

        if (!ok)
            continue;

        tables_.insert(obj->GenTableId, obj);
        tablesByName_.insert(obj->TableName, obj->GenTableId);
        if (!tableNames_.contains(obj->TableName))
            tableNames_.push_back(obj->TableName);
    }

    //
    // Capture the Elements
    //
    rx.setPattern("\"Elements\"(.*)\"KeyElements\"(.*)");
    rx.indexIn(data);
    QString elements = rx.cap(1).remove(" ");
    if (elements.length() == 0)
        return false;

    lines = elements.split("\n");

    foreach (QString line, lines) {
        QStringList values = line.split(",");
        if (values.isEmpty() || values.size() < 10)
            continue;
        
        bool ok;
        RecElement *obj      = new RecElement;
        obj->GenTableId     = values[1].toUShort(&ok);
        obj->LogicalCopyNum = values[4].toUShort(&ok);
        obj->PhysOrdinal    = values[5].toUShort(&ok);
        obj->ElmtName       = values[6].remove("\"");
        obj->BaseTypeName   = values[7].remove("\"");
        obj->BaseTypeSize   = values[9].toShort(&ok);
        obj->KeyFlg         = false;

        if (!ok)
            continue;

        elements_.insert(obj->GenTableId, obj);
    }

    //
    // Capture the KeyElements
    //
    elements = rx.cap(2).remove(" ");

    if (elements.length() == 0)
        return false;

    lines = elements.split("\n");

    foreach (QString line, lines) {
        QStringList values = line.split(",");
        if (values.isEmpty() || values.size() < 10)
            continue;
        
        //
        //  Get the key name
        //
        QMap<short/*tableid*/,RecTable*>::iterator tit = tables_.find(values[1].toUShort());
        if (tit == tables_.end()) {
            continue;
        }

        QMap<short, RecElement*>::iterator it = elements_.find(values[1].toUShort());

        while (it != elements_.end()) {
            if (tit.value()->KeyName == "")
                tit.value()->KeyName = values[4].remove("\"");

            if (it.value()->ElmtName  == values[9].remove("\"") &&
                tit.value()->KeyName  == values[4].remove("\"") ) {
                if (it.value()->LogicalCopyNum == 1) {
                    it.value()->KeyFlg = true;
                    break;
                }
            }
            it++;
        }
    }    

    helper_generateModel();

    //
    // destroy
    //
    QMultiMap<short/*tableid*/, RecElement*>::iterator el_it;
    for (el_it = elements_.begin(); el_it != elements_.end(); ++el_it) {
        delete el_it.value();
    }

    QMap<short/*tableid*/, RecTable*>::iterator tb_it;
    for (tb_it = tables_.begin(); tb_it != tables_.end(); ++tb_it) {
        delete tb_it.value();
    }

    elements_.clear();
    tableNames_.clear();
    tablesByName_.clear();
    tables_.clear();

    return true;
}

void
MDBCSVParser::helper_generateModel()
{
    // Iterate over the tables and keep only the higher version    
    QList<QString>::iterator it;

    for (it = tableNames_.begin(); it != tableNames_.end(); ++it) {       
        QList<short> id_values = tablesByName_.values(*it);

        QList<short>::iterator id_it;
        float keep = 0;
        for (id_it = id_values.begin(); id_it != id_values.end(); ++id_it) {
            RecTable *r = tables_.value(*id_it);
            if (!r) 
                qFatal("programmatic error at MDBCSVParser::helper_generateModel");
            
            if (keep < r->TableVersion)
                keep = r->TableVersion;
        }

        //cleanup the lower versions
        for (id_it = id_values.begin(); id_it != id_values.end(); ++id_it) {
            RecTable *r = tables_.value(*id_it);
            if (!r) 
                qFatal("programmatic error at MDBCSVParser::helper_generateModel");
            
            if (r->TableVersion != keep) {
                tablesByName_.remove(r->TableName, r->GenTableId);
                delete r;
                tables_.remove(*id_it);    
            }
        }
    }

    //Start with each table
    QMap<short/*tableid*/,RecTable*>::iterator tbl_it;
    for (tbl_it = tables_.begin(); tbl_it != tables_.end(); ++tbl_it) {
        helper_generateDefinition(tbl_it.value());
    } 
}

void
MDBCSVParser::helper_generateDefinition(RecTable *r)
{
    QList<RecElement*> fields = elements_.values(r->GenTableId);
    if (fields.isEmpty()) 
        qFatal("programmatic error at MDBCSVParser::helper_generateDefinition");

    QList<RecElement*>::iterator it;

    qSort(fields.begin(), fields.end(), less_than);

    GroupDefinitionItem *gd = new GroupDefinitionItem();//model()->create<GroupDefinitionItem>();
    gd->setName(r->TableName);

    for (it = fields.begin(); it != fields.end(); ++it) {
        LineDefinitionItem *li = new LineDefinitionItem();//model()->create<LineDefinitionItem>();
        if ((*it)->KeyFlg == true) {
            li->setKey(true);
        }

        if ((*it)->LogicalCopyNum != 1)
            li->setName("_"+QString::number((*it)->LogicalCopyNum)+(*it)->ElmtName);
        else {
            li->setName((*it)->ElmtName);
        }

        Definition def;
        // bt_binaryf = Fixed length string, binary
        // bt_binaryl = Two-byte data length followed by
        // bt_binaryv = Variable length string, binary
        // bt_char = Character
        // bt_flag = Boolean
        // bt_float32 = Four-byte decimal point
        // bt_float64 = Eight-byte decimal point
        // bt_int16s = Two-byte integer
        // bt_int32s = Four-byte integer
        // bt_int64s = Eight-byte integer
        // bt_stringf = Fixed length string
        // bt_stringv = Variable length string

        if ((*it)->BaseTypeName == "bt_binaryf") {
            def.data_type_ = ASCII;
            def.length_type_ = FIXED;
            def.length_    = (*it)->BaseTypeSize;
            li->define(def);
        } else if ((*it)->BaseTypeName == "bt_binaryl") {
            LineDefinitionItem *lli = new LineDefinitionItem();//model()->create<LineDefinitionItem>();
            lli->setName("len");

            Definition defl;
            defl.data_type_ = INT16;
            defl.length_type_ = FIXED;
            defl.length_    = 2;
            lli->define(defl);
            
            Definition defd;
            defd.data_type_   = ASCII;
            defd.length_type_ = FIXED;
            defd.length_      = (*it)->BaseTypeSize;
            
            LineDefinitionItem *dli = new LineDefinitionItem();//model()->create<LineDefinitionItem>();
            dli->setName("data");
            dli->define(defd);

            li->add(lli);
            li->add(dli);
        } else if ((*it)->BaseTypeName == "bt_binaryv") {
            def.data_type_   = ASCII;
            def.length_type_ = FIXED;
            def.length_      = (*it)->BaseTypeSize;
            li->define(def);
        } else if ((*it)->BaseTypeName == "bt_char") {
            def.data_type_   = ASCII;
            def.length_type_ = FIXED;
            def.length_      = 1;
            li->define(def);
        } else if ((*it)->BaseTypeName == "bt_flag") {
            def.data_type_ = ASCII;
            def.length_type_ = FIXED;
            def.length_    = 1;
            li->define(def);
        } else if ((*it)->BaseTypeName == "bt_float32") {
            def.data_type_ = FLOAT;
            def.length_type_ = FIXED;
            def.length_    = 4;
            li->define(def);
        } else if ((*it)->BaseTypeName == "bt_float64") {
            def.data_type_ = DOUBLE;
            def.length_type_ = FIXED;
            def.length_    = 8;
            li->define(def);
        } else if ((*it)->BaseTypeName == "bt_int16s") {
            def.data_type_ = INT16;
            def.length_type_ = FIXED;
            def.length_    = 2;
            li->define(def);
        } else if ((*it)->BaseTypeName == "bt_int32s") {
            def.data_type_ = INT32;
            def.length_type_ = FIXED;
            def.length_    = 4;
            li->define(def);
        } else if ((*it)->BaseTypeName == "bt_int64s") {
            def.data_type_ = INT64;
            def.length_type_ = FIXED;
            def.length_    = 8;
            li->define(def);
        } else if ((*it)->BaseTypeName == "bt_stringf") {
            def.data_type_ = ASCII;
            def.length_type_ = FIXED;
            def.length_    = (*it)->BaseTypeSize;
            li->define(def);
        } else if ((*it)->BaseTypeName == "bt_stringv") {
            def.data_type_   = ASCII;
            def.length_type_ = FIXED;
            def.length_       = (*it)->BaseTypeSize;
            li->define(def);
        } else {
            qFatal("programmatic error at MDBCSVParser::helper_generateDefinition. Unregognized base type");
        }

        gd->add(li);
    }

    model_->addGroupDefinition(gd);
}

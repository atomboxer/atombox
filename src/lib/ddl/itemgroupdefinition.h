#ifndef GROUPDEFINITIONITEM_H
#define GROUPDEFINITIONITEM_H

#include "itembase.h"
#include "itemcontainer.h"

#include <QtCore/QString>

class GroupDefinitionItem : public ContainerItem
{
public:
    GroupDefinitionItem(int kind = kindGroupDefinition);    
    virtual ~GroupDefinitionItem();

    virtual bool isEbcdic() const { return ebcdic_flg_;}
    virtual bool isDefined();
    virtual void setEbcdic( bool flg) { ebcdic_flg_ = flg; }
    virtual void setKeyName( QString k ) { key_name_ = k; }
    
    //for DDL Records
    virtual QString getKeyName( QString k ) const { return key_name_; }
    virtual bool hasKey() const { return !key_name_.isEmpty(); }

    virtual CodeModelItemBase* clone();

private:
    bool   ebcdic_flg_;
    QString key_name_;
};

#endif //GROUPDEFINITIONITEM_H

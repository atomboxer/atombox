#ifndef FIELDDEFINITIONITEM_H
#define FIELDDEFINITIONITEM_H

#include "itemlinedefinition.h"
#include "definition.h"

class FieldDefinitionItem : public LineDefinitionItem
{
public:
    FieldDefinitionItem(int kind = kindFieldDefinition);
};

#endif

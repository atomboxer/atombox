#ifndef DDL_CODE_MODEL
#define DDL_CODE_MODEL

#include <QtCore/QString>
#include <QtCore/QList>
#include <QtCore/QVariant>
#include <QtCore/QHash>
#include <QtCore/QSharedData>
#include <QtCore/QVariant>
#include <QtCore/QDebug>
#include <QtCore/QRegExp>

#include "boxcomponent.h"
#include "atom.h"

#include <assert.h>

#include "itembase.h"
#include "itemconstant.h"
#include "itemcontainer.h"
#include "itemenumdefinition.h"
#include "itemfielddefinition.h"
#include "itemgroupdefinition.h"
#include "itemlinedefinition.h"

class DDLCodeModel {
public:
    CodeModelItemBase *findItem( const QString &qualifiedName ) const;

    void addConstant(ConstantItem *item);
    void addEnumDefinition(EnumDefinitionItem *item);
    void addFieldDefinition(FieldDefinitionItem *item);
    void addGroupDefinition(GroupDefinitionItem *item);

    void removeConstant(QString name);
    void removeEnumDefinition(QString name);
    void removeFieldDefinition(QString name);
    void removeGroupDefinition(QString name);

    ConstantItem* findConstant(const QString &name) const;
    EnumDefinitionItem* findEnumDefinition(const QString &name) const;
    FieldDefinitionItem* findFieldDefinition(const QString &name) const;
    GroupDefinitionItem* findGroupDefinition(const QString &name) const;

    QHash<QString, ConstantItem*>  constantMap() const;

    QHash<QString, EnumDefinitionItem*>  enumDefinitionMap() const;
    QHash<QString, FieldDefinitionItem*> fieldDefinitionMap() const;
    QHash<QString, GroupDefinitionItem*> groupDefinitionMap() const;

    void dumpContainer( ContainerItem *item, QString padding);
    void dump();
    
    BoxComponent* dumpAtomBoxModel( ContainerItem *item, BoxComponent *root=0 );
    QList<BoxComponent*> dumpAtomBoxModel();
    QList< QPair <QString , QVariant > >      dumpConstants();

    void processRedefines( ContainerItem *item );
    void processRedefines();

public:
    DDLCodeModel();
    virtual ~DDLCodeModel();

    template <class T> T* create()
    {
        T *result = new T();
        return result;
    }

private:
    bool helper_dump_AtomBox_detect_VAR(ContainerItem *item, BoxComponent *root);
    DDLCodeModel(const DDLCodeModel &other);
    void operator= (const DDLCodeModel &other);

    QHash<QString, ConstantItem*> constants_;
    QHash<QString, EnumDefinitionItem*>  enum_definitions_;
    QHash<QString, FieldDefinitionItem*> field_definitions_;
    QHash<QString, GroupDefinitionItem*> group_definitions_;

    QHash<QString, GroupDefinitionItem*> dumped_definitions_;
    QHash<QString, ConstantItem*>        dumped_constants_;
};

#endif //DDL_CODE_MODEL


#line 122 "ddl.g"

#ifndef DDL_PARSER_H
#define DDL_PARSER_H

#include <QVariant>
#include <QRegExp>

#include "generatedgrammar_p.h"
#include "ddlengine.h"
#include "ast.h"

#include <QtCore/QList>
#include <QtCore/QString>

extern AST::Location yy_loc();

static int filler_ = 0;

class DDLParser: protected generatedgrammar
{
public:
union Value {
    char * val;
    qint64 dval;

    AST::Literal                      *Literal;
    AST::NumericLiteral               *NumericLiteral;
    AST::StringLiteral                *StringLiteral;
    AST::Statement                    *Statement;
    AST::ConstStatement               *ConstStatement;
    AST::StatementList                *StatementList;
    AST::FieldDefStatement            *FieldDefStatement;
    AST::GroupDefStatement            *GroupDefStatement;
    AST::GroupDefStatement            *RecordDefStatement;
    AST::EnumDefStatement             *EnumDefStatement;
    AST::Constraint                   *Constraint;
    AST::ConstraintList               *ConstraintList;
    AST::ConstraintRange              *ConstraintRange;
    AST::ConstraintRegex              *ConstraintRegex;
    AST::ConstraintValue              *ConstraintValue;
    AST::ConstraintLiteral            *ConstraintLiteral;

    AST::GroupDefClause               *GroupDefClause;
    AST::GroupDefClauseList           *GroupDefClauseList;

    AST::GroupDefEbcdicClause         *GroupDefEbcdicClause;    /*ddl enh*/

    AST::LineItem                     *LineItem;
    AST::LineItemList                 *LineItemList;
   
    AST::LineItemClause               *LineItemClause;
    AST::LineItemClauseList           *LineItemClauseList;

    AST::LineItemNotImplementedClause *LineItemNotImplementedClause;
    AST::LineItemAsClause             *LineItemAsClause;
    AST::LineItemDisplayClause        *LineItemDisplayClause;
    AST::LineItemDisplayClause        *LineItemPackedDecimalClause;
    AST::LineItemRedefinesClause      *LineItemRedefinesClause;
    AST::LineItemOccursClause         *LineItemOccursClause;
    AST::LineItemOccursDependingOnClause         *LineItemOccursDependingOnClause;
 

    AST::LineItemVLengthClause                 *LineItemVLengthClause;            /*ddl enh*/
    AST::LineItemVLengthInclusiveClause        *LineItemVLengthInclusiveClause;   /*ddl enh*/
    AST::LineItemVLengthAllInclusiveClause     *LineItemVLengthAllInclusiveClause;/*ddl enh*/
    AST::LineItemVLengthDelimiterClause        *LineItemVLengthDelimiterClause;   /*ddl enh*/

    AST::LineItemIsoBitmapClause      *LineItemIsoBitmapClause;      /*ddl enh*/
    AST::LineItemChoiceClause         *LineItemChoiceClause;         /*ddl enh*/
    AST::LineItemDefaultClause        *LineItemDefaultClause;        /*ddl enh*/
    AST::LineItemDefaultEmptyClause   *LineItemDefaultEmptyClause;   /*ddl enh*/
    AST::LineItemLPadClause           *LineItemLPadClause;           /*ddl enh*/
    AST::LineItemRPadClause           *LineItemRPadClause;           /*ddl enh*/

    AST::LineItemKeyClause           *LineItemKeyClause;      
    AST::LineItemKeyIsClause         *LineItemKeyIsClause;   
    AST::LineItemKeyIsClauseList     *LineItemKeyIsClauseList;   
    
    AST::PicOrType         *PicOrType;
    AST::PicClause         *PicClause;
    AST::PicClause         *PicCompClause;
    AST::PicBinaryClause   *PicBinaryClause;
    AST::PicBitClause      *PicBitClause;
    AST::TypeClause        *TypeClause;
    AST::TypeLiteralClause *TypeLiteralClause;
    AST::TypeStarClause    *TypeStarClause;
    AST::Node *Node;
};

public:
    DDLParser();
    ~DDLParser();

    bool parse();

    int yy_lex(); // ddl.l

    AST::Node *ast() const { 
        return AST_root_; }

    inline QString errorMessage() const {
        return error_message_; }

protected:

    void reallocateStack();

    inline Value &sym(int index)
    { return sym_stack [tos + index - 1]; }

    inline AST::Location &loc(int index)
    { return location_stack [tos + index - 1]; }

    inline MemoryPool* nodePool()  { return &node_pool_; }

protected:
    int            tos;
    int            stack_size;
    Value         *sym_stack;
    int           *state_stack;
    AST::Location *location_stack;

    AST::Node     *AST_root_;
    QString        error_message_;
    MemoryPool     node_pool_;

    // error recovery
    enum { TOKEN_BUFFER_SIZE = 3 };

};


#line 2226 "ddl.g"


#endif // DDLParser

%parser generatedgrammar
%decl generatedparser.h
%impl generatedparser.cpp

%expect         144
%expect-rr      0

%token_prefix T_

%token NUMBER     "number"
%token NUMBER_89  "89"
%token NUMBER_88  "88"
%token NAT_LIT    "NAT_LIT"
%token LITERAL    "literal"
%token STRING     "string"
%token BEGIN      "BEGIN"
%token END        "END"
%token LPAREN     "("
%token RPAREN     ")"
%token DOT        "."
%token STAR       "*"
%token COMMA      ","
%token CONSTANT   "CONSTANT"
%token VALUE      "VALUE"
%token IS         "IS"
%token TYPE       "TYPE"
%token UNSIGNED   "UNSIGNED"
%token BINARY     "BINARY"
%token FLOAT      "FLOAT"
%token COMPLEX    "COMPLEX"
%token LOGICAL    "LOGICAL"
%token NOT        "NOT"
%token ENUM       "ENUM"
%token BIT        "BIT"
%token DEFINITION "DEFINITION"
%token PICTURE    "PICTURE"
%token CHARACTER  "CHARACTER"
%token NULL       "NULL"
%token HEADING    "HEADING"
%token FILLER     "FILLER"
%token DISPLAY    "DISPLAY"
%token REDEFINES  "REDEFINES"
%token EXTERNAL   "EXTERNAL"
%token HELP       "HELP"
%token SQL_NULLABLE "SQL_NULLABLE"
%token USAGE      "USAGE"
%token VALUE      "VALUE" 
%token IS         "IS"
%token COMP       "COMPUTATIONAL"
%token INDEX      "INDEX"
%token PACKED_DECIMAL "PACKED-DECIMAL"
%token NOVALUE    "NOVALUE"
%token AS         "AS"
%token EDIT_PIC   "EDIT-PIC"
%token JUSTIFIED  "JUSTIFIED"
%token RIGHT      "RIGHT"
%token MUST       "MUST"
%token BE         "BE"
%token THROUGH    "THROUGH"
%token OCCURS     "OCCURS"
%token TIMES      "TIMES"
%token INDEXED    "INDEXED"
%token BY         "BY"
%token TO         "TO"
%token DEPENDING  "DEPENDING"
%token ON         "ON"
%token ASSIGNED   "ASSIGNED"
%token TEMPORARY  "TEMPORARY"
%token CREATION_KEYWORD "CREATION KEYWORD"
%token KEY_ASGN_KEYWORD "KEY ASSIGNMENT KEYWORD"
%token RECORD      "RECORD"
%token FILE        "FILE"
%token NO          "NO"
%token KEY         "KEY"
%token KEYTAG      "KEYTAG"
%token SPI_NULL    "SPI-NULL"
%token TOKEN_CODE  "TOKEN-CODE"
%token TOKEN_TYPE  "TOKEN-TYPE"
%token DUPLICATES  "DUPLICATES"
%token SSID        "SSID"
%token ARE         "ARE"
%token UPSHIFT     "UPSHIFT"
%token PIC_STRING  "PIC-STRING"

%token VLENGTH      "VLENGTH"
%token DELIMITER    "DELIMITER"
%token EXCLUSIVE    "EXCLUSIVE"
%token INCLUSIVE    "INCLUSIVE"
%token ALLINCLUSIVE "ALLINCLUSIVE"
%token DEFAULT      "DEFAULT"
%token ISOBITMAP    "ISOBITMAP"
%token CHARSET      "CHARSET"
%token CHOICE       "CHOICE"
%token DASH         "/"
%token ASCII        "ASCII"
%token EBCDIC       "EBCDIC"
%token HIGH_LOW     "HIGH_LOW"
%token LOW_HIGH     "LOW_HIGH"
%token REGEX        "REGEX"
%token LEFT         "LEFT"
%token LPAD         "LPAD"
%token RPAD         "RPAD"
%token EMPTY        "EMPTY"

%start statement_list

/.
#include <QtCore/QtDebug>

#include <string.h>
#include "generatedparser.h"

#define UPDATE_POSITION(node, startloc, endloc) do { \
    node->loc_.start_line = startloc.start_line;\
    node->loc_.start_column = startloc.start_column; \
    node->loc_.end_line = endloc.end_line; \
    node->loc_.end_column = endloc.end_column; \
} while (0)

./

/:
#ifndef DDL_PARSER_H
#define DDL_PARSER_H

#include <QVariant>
#include <QRegExp>

#include "generatedgrammar_p.h"
#include "ddlengine.h"
#include "ast.h"

#include <QtCore/QList>
#include <QtCore/QString>

extern AST::Location yy_loc();

static int filler_ = 0;

class DDLParser: protected $table
{
public:
union Value {
    char * val;
    qint64 dval;

    AST::Literal                      *Literal;
    AST::NumericLiteral               *NumericLiteral;
    AST::StringLiteral                *StringLiteral;
    AST::Statement                    *Statement;
    AST::ConstStatement               *ConstStatement;
    AST::StatementList                *StatementList;
    AST::FieldDefStatement            *FieldDefStatement;
    AST::GroupDefStatement            *GroupDefStatement;
    AST::GroupDefStatement            *RecordDefStatement;
    AST::EnumDefStatement             *EnumDefStatement;
    AST::Constraint                   *Constraint;
    AST::ConstraintList               *ConstraintList;
    AST::ConstraintRange              *ConstraintRange;
    AST::ConstraintRegex              *ConstraintRegex;
    AST::ConstraintValue              *ConstraintValue;
    AST::ConstraintLiteral            *ConstraintLiteral;

    AST::GroupDefClause               *GroupDefClause;
    AST::GroupDefClauseList           *GroupDefClauseList;

    AST::GroupDefEbcdicClause         *GroupDefEbcdicClause;    /*ddl enh*/

    AST::LineItem                     *LineItem;
    AST::LineItemList                 *LineItemList;
   
    AST::LineItemClause               *LineItemClause;
    AST::LineItemClauseList           *LineItemClauseList;

    AST::LineItemNotImplementedClause *LineItemNotImplementedClause;
    AST::LineItemAsClause             *LineItemAsClause;
    AST::LineItemDisplayClause        *LineItemDisplayClause;
    AST::LineItemDisplayClause        *LineItemPackedDecimalClause;
    AST::LineItemRedefinesClause      *LineItemRedefinesClause;
    AST::LineItemOccursClause         *LineItemOccursClause;
    AST::LineItemOccursDependingOnClause         *LineItemOccursDependingOnClause;
 

    AST::LineItemVLengthClause                 *LineItemVLengthClause;            /*ddl enh*/
    AST::LineItemVLengthInclusiveClause        *LineItemVLengthInclusiveClause;   /*ddl enh*/
    AST::LineItemVLengthAllInclusiveClause     *LineItemVLengthAllInclusiveClause;/*ddl enh*/
    AST::LineItemVLengthDelimiterClause        *LineItemVLengthDelimiterClause;   /*ddl enh*/

    AST::LineItemIsoBitmapClause      *LineItemIsoBitmapClause;      /*ddl enh*/
    AST::LineItemChoiceClause         *LineItemChoiceClause;         /*ddl enh*/
    AST::LineItemDefaultClause        *LineItemDefaultClause;        /*ddl enh*/
    AST::LineItemDefaultEmptyClause   *LineItemDefaultEmptyClause;   /*ddl enh*/
    AST::LineItemLPadClause           *LineItemLPadClause;           /*ddl enh*/
    AST::LineItemRPadClause           *LineItemRPadClause;           /*ddl enh*/

    AST::LineItemKeyClause           *LineItemKeyClause;      
    AST::LineItemKeyIsClause         *LineItemKeyIsClause;   
    AST::LineItemKeyIsClauseList     *LineItemKeyIsClauseList;   
    
    AST::PicOrType         *PicOrType;
    AST::PicClause         *PicClause;
    AST::PicClause         *PicCompClause;
    AST::PicBinaryClause   *PicBinaryClause;
    AST::PicBitClause      *PicBitClause;
    AST::TypeClause        *TypeClause;
    AST::TypeLiteralClause *TypeLiteralClause;
    AST::TypeStarClause    *TypeStarClause;
    AST::Node *Node;
};

public:
    DDLParser();
    ~DDLParser();

    bool parse();

    int yy_lex(); // ddl.l

    AST::Node *ast() const { 
        return AST_root_; }

    inline QString errorMessage() const {
        return error_message_; }

protected:

    void reallocateStack();

    inline Value &sym(int index)
    { return sym_stack [tos + index - 1]; }

    inline AST::Location &loc(int index)
    { return location_stack [tos + index - 1]; }

    inline MemoryPool* nodePool()  { return &node_pool_; }

protected:
    int            tos;
    int            stack_size;
    Value         *sym_stack;
    int           *state_stack;
    AST::Location *location_stack;

    AST::Node     *AST_root_;
    QString        error_message_;
    MemoryPool     node_pool_;

    // error recovery
    enum { TOKEN_BUFFER_SIZE = 3 };

};

:/

/.
//
// This file is automatically generated from ddl.g.
// Changes will be lost.
//

void DDLParser::reallocateStack()
{
    if (! stack_size)
        stack_size = 128;
    else
        stack_size <<= 1;
    
    sym_stack = reinterpret_cast<Value*> (qRealloc(sym_stack, stack_size * sizeof(Value)));
    state_stack = reinterpret_cast<int*> (qRealloc(state_stack, stack_size * sizeof(int)));
    location_stack = reinterpret_cast<AST::Location*> (qRealloc(location_stack, stack_size * sizeof(AST::Location)));
}

DDLParser::DDLParser():
    tos(0),
    stack_size(0),
    sym_stack(0),
    state_stack(0),
    location_stack(0),
    AST_root_(0)
{
}

DDLParser::~DDLParser()
{
    if (stack_size) {
        qFree(sym_stack);
        qFree(state_stack);
        qFree(location_stack);
    }
}

bool DDLParser::parse()
{
    const int INITIAL_STATE = 0;

    int yytoken = -1;
    int saved_yytoken = -1;

    reallocateStack();

    tos = 0;
    state_stack[++tos] = INITIAL_STATE;

    while (true) {
        const int state = state_stack [tos];
        if (yytoken == -1 && - TERMINAL_COUNT != action_index [state]) {
            if (saved_yytoken == -1) {
                location_stack [tos] = yy_loc();
                ++tos;
                yytoken = yy_lex();
                --tos;
            }
            else {
                yytoken = saved_yytoken;
                saved_yytoken = -1;
            }
        }
        
        int act = t_action (state, yytoken);
        
        if (act == ACCEPT_STATE)
            return true;
        
        else if (act > 0) {
            if (++tos == stack_size)
                reallocateStack();
            
            state_stack [tos] = act;
            location_stack [tos] = yy_loc();
            yytoken = -1;
        }
        
        else if (act < 0){
            int r = - act - 1;
            
            tos -= rhs [r];
            act = state_stack [tos++];
            
            switch (r) {
./

statement_list     : statement;
/.
    case $rule_number: {
        AST::StatementList *node = AST::makeAstNode<AST::StatementList>(nodePool(), sym(1).Statement);
        sym(1).Node = node;
        AST_root_ = node;
        node->finish();
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

statement_list     : statement_list statement;
/.
    case $rule_number: {
        AST::StatementList *node = AST::makeAstNode<AST::StatementList> (nodePool(), sym(1).StatementList, sym(2).Statement); 
        sym(1).Node = node;
        node->finish();
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

/./*
   * Constant Definitions
   *
   */./
statement          : const_statement;

const_statement    : CONSTANT r_literal VALUE IS val_def DOT;
/.
    case $rule_number: {

        AST::ConstStatement *node;
        node = AST::makeAstNode<AST::ConstStatement>(nodePool(), sym(2).Literal, sym(5).Node);
        sym(1).Node= node;
        UPDATE_POSITION(sym(1).Node, loc(1), loc(6));
    } break;
./

const_statement    : CONSTANT r_literal VALUE val_def DOT;
/.
    case $rule_number: {
        AST::ConstStatement *node;
        node = AST::makeAstNode<AST::ConstStatement>(nodePool(), sym(2).Literal, sym(4).Node );
        sym(1).Node= node;
        UPDATE_POSITION(sym(1).Node, loc(1), loc(5));
    } break;
./

const_statement    : CONSTANT r_literal clause_type VALUE val_def DOT;
/.
    case $rule_number: {

        AST::ConstStatement *node;
        node = AST::makeAstNode<AST::ConstStatement>(nodePool(), sym(2).Literal, sym(5).Node);
        sym(1).Node= node;
        UPDATE_POSITION(sym(1).Node, loc(1), loc(6));
    } break;
./

const_statement    : CONSTANT r_literal clause_type VALUE IS val_def DOT;
/.
    case $rule_number: {

        AST::ConstStatement *node;
        node = AST::makeAstNode<AST::ConstStatement>(nodePool(), sym(2).Literal, sym(6).Node);
        sym(1).Node= node;
        UPDATE_POSITION(sym(1).Node, loc(1), loc(7));
    } break;
./

val_def          : r_literal;
val_def          : r_string;
val_def          : r_number;

val_def           : r_number type_opt BINARY binary_opt unsigned_opt;

val_def           : r_literal type_opt BINARY binary_opt unsigned_opt;

type_opt           :;
type_opt           :TYPE;

binary_opt         :;
binary_opt         : r_number;
unsigned_opt       :;
unsigned_opt       : UNSIGNED;


/./************************
   * TOKEN DEFINITIONS    *
   *                      *
   ************************/./

statement               : token_type_statement;
token_type_statement    : TOKEN_TYPE LITERAL VALUE is_opt LITERAL DEFINITION is_opt LITERAL DOT;
  

statement               : token_code_statement;

token_code_statement    : TOKEN_CODE LITERAL
                          VALUE is_opt LITERAL
                          TOKEN_TYPE is_opt LITERAL
                          DOT;

token_code_statement    : TOKEN_CODE LITERAL
                          VALUE is_opt LITERAL
                          TOKEN_TYPE is_opt LITERAL
                          token_options
                          DOT;

token_code_statement    : TOKEN_CODE LITERAL
                          VALUE is_opt LITERAL
                          token_options
                          TOKEN_TYPE is_opt LITERAL
                          DOT;

token_options           : token_option;
token_options           : token_options token_option;

token_option            : SSID STRING;
token_option            : SSID LITERAL;

/./************************
   * Field Definitions    *
   *                      *
   ************************/./

def_statement      : DEFINITION r_literal
                     picture_or_type_clause
                     DOT;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::FieldDefStatement>(nodePool(), sym(2).Literal, sym(3).PicOrType);
        UPDATE_POSITION(sym(1).Node, loc(1),loc(4));
    }
    break;
./

def_statement      : DEFINITION r_literal
                     picture_or_type_clause
                     line_item_clauses
                     DOT;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::FieldDefStatement>(nodePool(),sym(2).Literal, 
                                                               sym(3).PicOrType, sym(4).LineItemClauseList->finish());
        UPDATE_POSITION(sym(1).Node, loc(1),loc(5));
    }
    break;
./

def_statement      : DEFINITION r_literal
                     picture_or_type_clause
                     clause_as
                     group_def_clauses
                     DOT;
/.
    case $rule_number: {
        qDebug()<<"field definitions not implemented";
        Q_ASSERT(0);
    } break;
./

def_statement      : DEFINITION r_literal
                     picture_or_type_clause
                     clause_as
                     BEGIN
                     group_def_clauses
                     DOT
                     END dot_opt;
/.
    case $rule_number: {
        qDebug()<<"field definitions not implemented";
        Q_ASSERT(0);
    } break;
./


/./************************
   * ENUM Definitions     *
   *                      *
   ************************/./
statement          : def_enum_statement;

def_enum_statement : DEFINITION r_literal TYPE ENUM BEGIN DOT
                     clause_89_list
                     END dot_opt;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::EnumDefStatement>(nodePool(), sym(2).Literal, sym(7).ConstraintList->finish());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(9));
    }
    break;
./


clause_89_list : clause_89;
/.
     case $rule_number: {
         AST::ConstraintList *node = AST::makeAstNode<AST::ConstraintList>(nodePool(), sym(1).Constraint);
         sym(1).Node = node;
         UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
     } break;
./

clause_89_list : clause_89_list clause_89;
/.
      case $rule_number : {
          AST::ConstraintList *node = AST::makeAstNode<AST::ConstraintList>( nodePool(), sym(1).ConstraintList, sym(2).Constraint);
          sym(1).Node = node;
          UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
      } break;
./

clause_89      : NUMBER_89 r_literal VALUE val_def clause_as DOT;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintValue>(nodePool(), sym(4).Node, sym(5).LineItemAsClause );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(6));
    }
    break;
./

clause_89      : NUMBER_89 r_literal VALUE val_def DOT;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintValue>(nodePool(), sym(4).Node);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(4));
    }
    break;
./


/./************************
   * Group Definitions    *
   *                      *
   ************************/./
statement          : def_statement;

def_statement      : DEFINITION r_literal
                     DOT END dot_opt;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::GroupDefStatement>(nodePool(), sym(2).Literal);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(4));
    }
    break;
./

def_statement      : DEFINITION r_literal
                     group_def_clauses
                     DOT
                     line_item_spec_list
                     END dot_opt;
/.
    case $rule_number: {

        sym(1).Node = AST::makeAstNode<AST::GroupDefStatement>(nodePool(),
                                             sym(2).Literal,
                                             sym(3).GroupDefClauseList->finish(),
                                             sym(5).LineItemList->finish());

        UPDATE_POSITION(sym(1).Node, loc(1), loc(7));

    }
    break;
./

def_statement      : DEFINITION r_literal DOT
                     line_item_spec_list
                     END dot_opt;

/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::GroupDefStatement>(nodePool(), 
                                                               sym(2).Literal,sym(4).LineItemList->finish());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(7));
    } break;
./


group_def_clauses : group_def_clause;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::GroupDefClauseList>(nodePool(), sym(1).GroupDefClause);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

group_def_clauses : group_def_clauses group_def_clause;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::GroupDefClauseList>(nodePool(), 
                                                                sym(1).GroupDefClauseList, sym(2).GroupDefClause);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

group_def_clause  : clause_display;
group_def_clause  : clause_external;
group_def_clause  : clause_heading;
group_def_clause  : clause_help;
group_def_clause  : clause_redefines;
group_def_clause  : clause_null;
group_def_clause  : clause_sql_nullable;
group_def_clause  : clause_usage;
group_def_clause  : clause_value;

group_def_clause :  clause_charset;
group_def_clause :  clause_highlow;
group_def_clause :  clause_lowhigh;

line_item_spec_list : line_item_spec;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemList>(nodePool(), sym(1).LineItem);
    }
    break;
./

line_item_spec_list : line_item_spec_list line_item_spec;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemList> (nodePool(), sym(1).LineItemList, sym(2).LineItem);
    }
    break;
./


r_number  : NUMBER;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::NumericLiteral> (nodePool(), sym(1).dval);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

r_number  : NUMBER_89;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::NumericLiteral> (nodePool(), sym(1).dval);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

r_number  : NUMBER_88;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::NumericLiteral> (nodePool(), sym(1).dval);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

r_literal_nokey : keywords_nokey;
r_literal_nokey : LITERAL;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

r_literal : LITERAL;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

r_string : STRING;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::StringLiteral> (nodePool(), sym(1).val);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

r_literal : keywords;

r_literal : LITERAL LITERAL SSID;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

/. //This is causing a lot of shift/reduce conflicts but there's no way to avoid it ./
line_item_clauses : ;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemClauseList>(nodePool());
    } break;
./

/. //This is causing a lot of shift/reduce conflicts but there's no way to avoid it ./

line_item_spec  : NUMBER_89 r_literal line_item_clauses DOT;
/.
    case $rule_number:
        sym(1).Node= AST::makeAstNode<AST::LineItem>(nodePool(), sym(1).dval, sym(2).Literal, sym(3).LineItemClauseList->finish() );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(4));
    break;
./

line_item_spec  : NUMBER_88 r_literal line_item_clauses DOT;
/.
    case $rule_number:
        sym(1).Node= AST::makeAstNode<AST::LineItem>(nodePool(), sym(1).dval, sym(2).Literal, sym(3).LineItemClauseList->finish() );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(4));
    break;
./

line_item_spec  : NUMBER r_literal
                  line_item_clauses
                  DOT;
/.
    case $rule_number:
        sym(1).Node= AST::makeAstNode<AST::LineItem>(nodePool(), sym(1).dval, sym(2).Literal, 
                                                     sym(3).LineItemClauseList->finish());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(4));
    break;
./

line_item_spec  : NUMBER r_literal
                  line_item_clauses
                  picture_or_type_clause
                  line_item_clauses
                  DOT;
/.
    case $rule_number:
        sym(1).Node= AST::makeAstNode<AST::LineItem>(nodePool(), sym(1).dval, sym(2).Literal, sym(4).PicOrType,
                                                     sym(5).LineItemClauseList->merge(sym(3).LineItemClauseList->finish()));
        UPDATE_POSITION(sym(1).Node, loc(1), loc(6));
    break;
./

line_item_clauses : line_item_clause;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemClauseList>(nodePool(), sym(1).LineItemClause);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

line_item_clauses : line_item_clauses line_item_clause;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemClauseList>(nodePool(), 
                                                                sym(1).LineItemClauseList, sym(2).LineItemClause);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

line_item_clause  : clause_vlength;
line_item_clause  : clause_vlength_inclusive;
line_item_clause  : clause_vlength_allinclusive;
line_item_clause  : clause_vlength_delimiter;
line_item_clause  : clause_isobitmap;
line_item_clause  : clause_as;
line_item_clause  : clause_display;
line_item_clause  : clause_default;
line_item_clause  : clause_default_empty;
line_item_clause  : clause_pad;
line_item_clause  : clause_edit_pic;
line_item_clause  : clause_heading;
line_item_clause  : clause_help;
line_item_clause  : clause_justified;
line_item_clause  : clause_keytag;
line_item_clause  : clause_null;
line_item_clause  : clause_mustbe;
line_item_clause  : clause_occurs;
line_item_clause  : clause_spi_null;
line_item_clause  : clause_redefines;
line_item_clause  : clause_usage;
line_item_clause  : clause_upshift;
line_item_clause  : clause_value;
line_item_clause  : clause_choice;

dot_opt : ;
dot_opt : DOT;

picture_or_type_clause : clause_pic;
/.
    case $rule_number: {
        sym(1).Node= AST::makeAstNode<AST::PicOrType>(nodePool(), sym(1).Node);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./


picture_or_type_clause : clause_type;
/.
    case $rule_number: {
        sym(1).Node= AST::makeAstNode<AST::PicOrType>(nodePool(), sym(1).Node);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

/./***************************
   * Record Definitions      *
   *                         *
   ***************************/./
def_statement      : RECORD r_literal DOT
                     file_creation_opts
                     line_item_spec_list
                     key_asgn_opt
                     END dot_opt;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::RecordDefStatement>(nodePool(), sym(2).Literal, sym(5).LineItemList->finish(),  sym(6).LineItemKeyIsClauseList->finish());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(8));
    }
    break;
./


def_statement      : RECORD r_literal DOT
                     line_item_spec_list
                     key_asgn_opt
                     END dot_opt;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::RecordDefStatement>(nodePool(), sym(2).Literal, sym(4).LineItemList->finish(), sym(5).LineItemKeyIsClauseList->finish());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(7));
    }
    break;
./


def_statement      : RECORD r_literal DOT
                     file_creation_opts
                     rec_reference
                     key_asgn_opt
                     END dot_opt;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::RecordDefStatement>(nodePool(), sym(2).Literal, sym(5).Literal, sym(6).LineItemKeyIsClauseList->finish());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(8));
    }
    break;
./

rec_reference      : DEFINITION IS r_literal DOT;
/.
    case $rule_number:
         sym(1).Node = sym(3).Literal;
         UPDATE_POSITION(sym(1).Node, loc(1), loc(4));
    break;
./
 
file_creation_opts : file_creation_opt;
/.
    case $rule_number:
    break;
./

file_creation_opts : file_creation_opts file_creation_opt;
/.
    case $rule_number:
    break;
./

file_creation_opt : FILE IS LITERAL   creation_attr_opt DOT;
file_creation_opt : FILE IS STRING    creation_attr_opt DOT;
file_creation_opt : FILE IS TEMPORARY creation_attr_opt DOT;
file_creation_opt : FILE IS ASSIGNED  creation_attr_opt DOT;

creation_attr_opt : record_attrs;
creation_attr_opt : ;

key_asgn_opt  : ;
/.
    case $rule_number:
        sym(1).Node = AST::makeAstNode<AST::LineItemKeyIsClauseList>(nodePool());
    break;
./

key_asgn_opt  : key_asgn;


key_asgn      : key_as;
/.
    case $rule_number:
        sym(1).Node = AST::makeAstNode<AST::LineItemKeyIsClauseList>(nodePool(), sym(1).LineItemKeyIsClause);
    break;
./

key_asgn      : key_asgn key_as;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemKeyIsClauseList>(nodePool(), sym(1).LineItemKeyIsClauseList, sym(2).LineItemKeyIsClause);
    } break;
./


key_as            : KEY r_string IS key_dot_spec;
/.
      case $rule_number: {
            sym(1).Node = AST::makeAstNode<AST::LineItemKeyIsClause> (nodePool(), sym(4).Literal,  sym(2).StringLiteral);
      } break;
./


key_as            : KEY r_number IS key_dot_spec;
/.

        case $rule_number: {
            sym(1).Node = AST::makeAstNode<AST::LineItemKeyIsClause> (nodePool(), sym(4).Literal,  sym(2).NumericLiteral);
            UPDATE_POSITION(sym(1).Node, loc(1), loc(4));
        } break;
./


key_as            : KEY IS key_dot_spec;
/.
        case $rule_number: {
            sym(1).Node = AST::makeAstNode<AST::LineItemKeyIsClause> (nodePool(), sym(3).Literal);
                
            UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
        } break;
./



key_dot_spec     : r_literal_nokey DOT;
/.            
    case $rule_number: {
        sym(1).Node = sym(1).Node;
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
        
./

key_dot_spec     : r_literal_nokey DOT r_literal_nokey DOT;
/.            
    case $rule_number: {
        sym(1).Node = sym(3).Node;
        UPDATE_POSITION(sym(1).Node, loc(1), loc(4));
    } break;
        
./

key_dot_spec     : r_literal_nokey DOT r_literal_nokey DOT r_literal_nokey DOT;
/.            
    case $rule_number: {
        sym(1).Node = sym(5).Node;
        UPDATE_POSITION(sym(1).Node, loc(1), loc(6));
    } break;        
./


key_dot_spec     : r_literal_nokey key_spec DOT;
/.            
    case $rule_number: {
        sym(1).Node = sym(1).Node;
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
        
./

key_dot_spec     : r_literal_nokey DOT r_literal_nokey key_spec DOT;
/.            
    case $rule_number: {
        sym(1).Node = sym(3).Node;
        UPDATE_POSITION(sym(1).Node, loc(1), loc(5));
    } break;
        
./

key_dot_spec     : r_literal_nokey DOT r_literal_nokey DOT r_literal_nokey key_spec DOT;
/.            
    case $rule_number: {
        sym(1).Node = sym(5).Node;
        UPDATE_POSITION(sym(1).Node, loc(1), loc(7));
    } break;        
./

key_spec      : comma_opt DUPLICATES not_opt KEY_ASGN_KEYWORD;
/.
    case $rule_number: {
        //UPDATE_POSITION(sym(1).Node, loc(1), loc(4));        
    } break;
./

record_attrs      : record_attr;
/.
    case $rule_number: {
        //UPDATE_POSITION(sym(1).Node, loc(1), loc(1));        
    } break;
./

record_attrs      : record_attrs record_attr;
/.
    case $rule_number: {
        ;//UPDATE_POSITION(sym(1).Node, loc(1), loc(2));        
    } break;
./



/. /* GENERIC CREATION ATTRIBUTE SYNTAX TO BE IGNORED */ ./
record_attr       : no_opt CREATION_KEYWORD comma_opt;
/.
    case $rule_number: {
        //UPDATE_POSITION(sym(1).Node, loc(1), loc(3));        
    } break;
./

record_attr       : no_opt CREATION_KEYWORD NUMBER comma_opt;
/.
    case $rule_number: {
        //UPDATE_POSITION(sym(1).Node, loc(1), loc(3));        
    } break;
./

record_attr       : no_opt CREATION_KEYWORD NUMBER COMMA NUMBER comma_opt;
/.
    case $rule_number: {
        //UPDATE_POSITION(sym(1).Node, loc(1), loc(6));        
    } break;
./

record_attr       : no_opt CREATION_KEYWORD LPAREN NUMBER COMMA NUMBER RPAREN comma_opt;
/.
    case $rule_number: {
        //UPDATE_POSITION(sym(1).Node, loc(1), loc(8));        
    } break;
./

no_opt            : ;
no_opt            : NO;
comma_opt         : ;
comma_opt         : COMMA;

/. /* GENERIC KEY ASSIGNMENT SYNTAX TO BE IGNORED */ ./

not_opt          : NOT;
not_opt          : ;



/./***************************************************************************
  *
  * CLAUSES:
  *
  ***************************************************************************/./

/./**
   * PICTURE Clause
   */./
clause_pic          : PICTURE PIC_STRING;
/.
    case $rule_number: {
        sym(1).Node= AST::makeAstNode<AST::PicClause>(nodePool(), QString(sym(2).val).replace(" ","").replace("\'",""));
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

/./**
   * Type Clause
   */./
clause_type         : type_opt clause_data_type;
/.
    case $rule_number: {
        sym(1).Node = sym(2).Node;
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

clause_type         : TYPE STAR;
/.
    case $rule_number: {
        sym(1).Node= AST::makeAstNode<AST::TypeStarClause>(nodePool());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

clause_type         : TYPE ENUM;
/.
    case $rule_number: {
        sym(1).Node= AST::makeAstNode<AST::PicBinaryClause>(nodePool(), QMetaType::UShort);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

clause_type         : TYPE r_literal;
/.
    case $rule_number: {
        sym(1).Node= AST::makeAstNode<AST::TypeLiteralClause>(nodePool(), sym(2).Literal);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

clause_data_type    : CHARACTER r_number;
/.
    case $rule_number: {
        QString num = QString::number(sym(2).NumericLiteral->val_);
        num.remove( QRegExp("^[0]*") );
        sym(1).Node= AST::makeAstNode<AST::PicClause>(nodePool(), QString("X(")+
                                                      num+QString(")").replace(" ","").replace("\'",""));
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

clause_data_type    : BINARY; /.//16./
/.
    case $rule_number: {
        sym(1).Node= AST::makeAstNode<AST::PicBinaryClause>(nodePool(), QMetaType::UShort);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

clause_data_type    : BINARY UNSIGNED; /.//16 unsigned./
/.
    case $rule_number: {
        sym(1).Node= AST::makeAstNode<AST::PicBinaryClause>(nodePool(), QMetaType::Short);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

clause_data_type    : BINARY r_number;
/.
    case $rule_number: {
        QMetaType::Type type;

        assert(sym(2).NumericLiteral->val_);
        switch( sym(2).NumericLiteral->val_ ) {
        case  8: type =  QMetaType::Char; break;
        case 16: type =  QMetaType::Short; break;
        case 32: type =  QMetaType::Int; break;
        case 64: type =  QMetaType::LongLong; break;
        default: {
            qFatal((QString("BINARY ")+ QString::number(sym(2).NumericLiteral->val_)
                    + QString(" clause is unsupported (8,16,32,64)")).toLatin1().data());;
        }
        }
        sym(1).Node= AST::makeAstNode<AST::PicBinaryClause>(nodePool(), type);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

clause_data_type    : BINARY r_number UNSIGNED;
/.
    case $rule_number: {
        QMetaType::Type type;
             
        assert(sym(2).NumericLiteral->val_);
        switch( sym(2).NumericLiteral->val_ ) {
        case  8: type =  QMetaType::UChar; break;
        case 16: type  = QMetaType::UShort; break;
        case 32: type =  QMetaType::UInt; break;
        default: {
            qFatal((QString("BINARY UNSIGNED ")+ QString::number(sym(2).NumericLiteral->val_)
                    + QString(" clause is unsupported (8,16,32)")).toLatin1().data());;
        }
        }
        sym(1).Node= AST::makeAstNode<AST::PicBinaryClause>(nodePool(), type);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

clause_data_type    : BINARY r_number COMMA NUMBER ;
/.
    case $rule_number: {
        QMetaType::Type type;
        assert(sym(2).NumericLiteral->val_);
        switch( sym(2).NumericLiteral->val_ ) {
        case  8: type =  QMetaType::Char; break;
        case 16: type  = QMetaType::Short; break;
        case 32: type =  QMetaType::Int; break;
        case 64: type =  QMetaType::LongLong; break;
        default: {
            qFatal((QString("BINARY ")+ QString::number(sym(2).NumericLiteral->val_)
                    + QString(" clause is unsupported (8,16,32,64)")).toLatin1().data());;
        }
        }
        sym(1).Node= AST::makeAstNode<AST::PicBinaryClause>(nodePool(), type);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(4));
    } break;
./

clause_data_type    : BINARY r_number COMMA NUMBER UNSIGNED;
/.
    case $rule_number: {
        qWarning(QString("BINARY clauses with scale are not supported").toLatin1().data());;
        
        QMetaType::Type type;
             
        assert(sym(2).NumericLiteral->val_);
        switch( sym(2).NumericLiteral->val_ ) {
        case  8: type =  QMetaType::UChar; break;
        case 16: type  = QMetaType::UShort; break;
        case 32: type =  QMetaType::UInt; break;
        default: {
            qFatal((QString("BINARY UNSIGNED ")+ QString::number(sym(2).NumericLiteral->val_)
                    + QString(" clause is unsupported (8,16,32)")).toLatin1().data());;
        }
        }
        sym(1).Node= AST::makeAstNode<AST::PicBinaryClause>(nodePool(), type);

        UPDATE_POSITION(sym(1).Node, loc(1), loc(5));
    } break;
./

clause_data_type    : FLOAT; /.//32./
/.
    case $rule_number: {
        sym(1).Node= AST::makeAstNode<AST::PicBinaryClause>(nodePool(), QMetaType::Float);
    } break;
./

clause_data_type    : FLOAT r_number;
/.
    case $rule_number: {
        switch(sym(2).NumericLiteral->val_) {
        case (32) : sym(1).Node= AST::makeAstNode<AST::PicBinaryClause>(nodePool(), QMetaType::Float); break;
        case (64) : sym(1).Node= AST::makeAstNode<AST::PicBinaryClause>(nodePool(), QMetaType::Double); break;
        default : {
            qFatal((QString("FLOAT ")+ QString::number(sym(2).NumericLiteral->val_)
                    + QString(" clause is unsupported (32,64)")).toLatin1().data());;
        }
        }

    } break;
./

clause_data_type    : COMPLEX;
/.
    case $rule_number: {
        qWarning("Clause TYPE COMPLEX not implemented");
    }break;
./

clause_data_type    : LOGICAL; /.//2./
/.
    case $rule_number: {
        qWarning("Clause TYPE LOGICAL not implemented");
    }break;
./

clause_data_type    : LOGICAL NUMBER;
/.
    case $rule_number: {
        qWarning("Clause TYPE LOGICAL not implemented");
    }break;
./

clause_data_type    : BIT r_number;
/.
    case $rule_number: {
        sym(1).Node= AST::makeAstNode<AST::PicBitClause>(nodePool(), sym(2).NumericLiteral->val_, true);
    }break;
./

clause_data_type    : BIT r_number UNSIGNED;
/.
    case $rule_number: {
        sym(1).Node= AST::makeAstNode<AST::PicBitClause>(nodePool(), sym(2).NumericLiteral->val_, false);
    }break;
./

/./**
   * AS Clause
   */./
clause_as           : AS r_string;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemAsClause>(nodePool(), sym(2).StringLiteral);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    }break;
./


/./**
   * DISPLAY Clause
   */./
clause_display      : DISPLAY r_string;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemDisplayClause> (nodePool(), sym(2).StringLiteral);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

clause_display      : DISPLAY LITERAL;

/.
    case $rule_number: {
        qFatal("clause:DISPLAY LITERAL not implemented yet");
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./


/./**
   * DEFAULT Clause
   */./
clause_default      : DEFAULT r_literal;
/. 
   case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemDefaultClause> (nodePool(), sym(2).Node);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
   } break;
./

clause_default      : DEFAULT r_string;
/. case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemDefaultClause> (nodePool(), sym(2).Node);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
   } break;
./

clause_default      : DEFAULT r_number;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemDefaultClause> (nodePool(), sym(2).Node);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

/./**
   * DEFAULT EMPTY Clause
   */./
clause_default_empty      : DEFAULT EMPTY;
/. 
   case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemDefaultEmptyClause> (nodePool());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
   } break;
./


/./**
   * EDIT-PIC Clause
   */./
clause_edit_pic     : EDIT_PIC STRING;
/.
    case $rule_number: {
        qFatal("clause not implemented");
    }break;
./

/./**
   * EXTERNAL Clause
   */./
clause_external      : EXTERNAL;
/.
    case $rule_number: {
        qFatal("clause not implemented");
    }break;
./

/./**
   * HEADING Clause
   */./
clause_heading      : HEADING STRING;
clause_heading      : HEADING LITERAL;


/./**
   * HELP Clause
   */./
clause_help         : HELP help_string;
help_string         : STRING;
help_string         : STRING COMMA help_string;

/./**
   * JUSTIFIED Clause
   */./
clause_justified    : JUSTIFIED RIGHT;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemLPadClause> (nodePool(), 
                                          AST::makeAstNode<AST::StringLiteral> (nodePool(), " "));

        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

clause_justified    : JUSTIFIED LEFT;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemRPadClause> (nodePool(), 
                                          AST::makeAstNode<AST::StringLiteral> (nodePool(), " "));

        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

clause_justified    : JUSTIFIED;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemLPadClause> (nodePool(), 
                                           AST::makeAstNode<AST::StringLiteral> (nodePool(), " "));
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./


clause_pad     : clause_lpad;
clause_pad     : clause_rpad;

/./**
   * LPAD Clause
   */./
clause_lpad    : LPAD r_string;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemLPadClause> (nodePool(), sym(2).StringLiteral);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

/./**
   * LPAD Clause
   */./
clause_rpad    : RPAD r_string;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemRPadClause> (nodePool(), sym(2).StringLiteral);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./


/. /**
    * VLENGTH DELIMITED Clause
    */
./
clause_vlength_delimiter     : VLENGTH DELIMITER r_literal;
/.
    case $rule_number:
./

clause_vlength_delimiter     : VLENGTH DELIMITER r_string;
/.
    case $rule_number:
./

clause_vlength_delimiter     : VLENGTH DELIMITER r_number;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemVLengthDelimiterClause> (nodePool(), sym(3).Literal);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

/. /**
    * VLENGTH DEPENDING ON Clause
    */
./
clause_vlength     : VLENGTH DEPENDING ON r_literal;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemVLengthClause> (nodePool(), sym(4).Literal);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(4));
    } break;
./

/. /**
    * VLENGTH EXCLUSIVE DEPENDING ON Clause
    */
./
clause_vlength     : VLENGTH EXCLUSIVE DEPENDING ON r_literal;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemVLengthClause> (nodePool(), sym(5).Literal);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(5));
    } break;
./

/. /**
    * VLENGTH INCLUSIVE DEPENDING ON Clause
    */
./
clause_vlength_inclusive     : VLENGTH INCLUSIVE DEPENDING ON r_literal;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemVLengthInclusiveClause> (nodePool(), sym(5).Literal);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(5));
    } break;
./

/. /**
    * VLENGTH ALLINCLUSIVE DEPENDING ON Clause
    */
./
clause_vlength_allinclusive     : VLENGTH ALLINCLUSIVE DEPENDING ON r_literal;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemVLengthAllInclusiveClause> (nodePool(), sym(5).Literal);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(5));
    } break;
./


/. /**
    * ISOBITMAP Clause
    */
./
clause_isobitmap   : ISOBITMAP;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemIsoBitmapClause> (nodePool());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./


/./**
   * KEYTAG Clause
   */./
clause_keytag       : KEYTAG r_number not_opt KEY_ASGN_KEYWORD;
/.
    case $rule_number: {
        if (sym(2).NumericLiteral->val_ == 0) {
            sym(1).Node = AST::makeAstNode<AST::LineItemKeyClause> (nodePool());
            UPDATE_POSITION(sym(1).Node, loc(1), loc(4));
        } else {
            sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
        }
        
    } break;  
./

clause_keytag       : KEYTAG r_number;
/.
    case $rule_number: {
        if (sym(2).NumericLiteral->val_ == 0) {
            sym(1).Node = AST::makeAstNode<AST::LineItemKeyClause> (nodePool());
        } else {
          sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
        }
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;  
./

clause_keytag       : KEYTAG STRING not_opt KEY_ASGN_KEYWORD KEY_ASGN_KEYWORD KEY_ASGN_KEYWORD;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
       UPDATE_POSITION(sym(1).Node, loc(1), loc(6));
    }break;
./

clause_keytag       : KEYTAG STRING not_opt KEY_ASGN_KEYWORD;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
      UPDATE_POSITION(sym(1).Node, loc(1), loc(4));
    }break;
./

clause_keytag       : KEYTAG STRING DUPLICATES not_opt KEY_ASGN_KEYWORD;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
      UPDATE_POSITION(sym(1).Node, loc(1), loc(5));
    }break;
./

clause_keytag       : KEYTAG r_number DUPLICATES not_opt KEY_ASGN_KEYWORD;
/.
    case $rule_number: {
        if (sym(2).NumericLiteral->val_ == 0) {
            sym(1).Node = AST::makeAstNode<AST::LineItemKeyClause> (nodePool());
            UPDATE_POSITION(sym(1).Node, loc(1), loc(5));
        } else {
            sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
             UPDATE_POSITION(sym(1).Node, loc(1), loc(5));
        }
    } break;  
./
clause_keytag       : KEYTAG STRING not_opt KEY_ASGN_KEYWORD KEY_ASGN_KEYWORD;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
    }break;
./

clause_keytag       : KEYTAG STRING;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
    }break;
./

/./**
   * NULL Clause
   */./
clause_null           : NULL STRING;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
    }break;
./
clause_null           : NULL NUMBER;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
    }break;
./
clause_null           : NULL LITERAL;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
    }break;
./

/./**
   * MUST-BE Clause
   */./

clause_mustbe       : MUST BE ENUM;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
    }break;
./

clause_mustbe       : MUST BE mustbe_values;
/.
    case $rule_number: {
        sym(1).Node = sym(3).ConstraintList->finish();
    }break;
./

mustbe_values       : mustbe_value;
/.
    case $rule_number: {
        AST::ConstraintList *node = AST::makeAstNode<AST::ConstraintList>(nodePool(), sym(1).Constraint);
        sym(1).Node = node;
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

mustbe_values       : mustbe_values COMMA mustbe_value;
/.
    case $rule_number : {
        AST::ConstraintList *node = AST::makeAstNode<AST::ConstraintList>( nodePool(), sym(1).ConstraintList, sym(3).Constraint);
        sym(1).Node = node;
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

mustbe_value        : r_literal THROUGH r_literal clause_as;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintRange> (nodePool(), sym(1).Node, sym(3).Node, sym(4).LineItemAsClause );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

mustbe_value        : r_literal THROUGH r_literal;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintRange> (nodePool(), sym(1).Node, sym(3).Node);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

mustbe_value        : r_number  THROUGH r_literal clause_as;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintRange> (nodePool(), sym(1).Node, sym(3).Node, sym(4).LineItemAsClause );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

mustbe_value        : r_number  THROUGH r_literal;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintRange> (nodePool(), sym(1).Node, sym(3).Node);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

mustbe_value        : r_literal THROUGH r_number clause_as;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintRange> (nodePool(), sym(1).Node, sym(3).Node, sym(4).LineItemAsClause );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

mustbe_value        : r_literal THROUGH r_number;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintRange> (nodePool(), sym(1).Node, sym(3).Node );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

mustbe_value        : r_number  THROUGH r_number clause_as;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintRange> (nodePool(), sym(1).NumericLiteral, sym(3).NumericLiteral, sym(4).LineItemAsClause );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

mustbe_value        : r_number  THROUGH r_number;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintRange> (nodePool(), sym(1).NumericLiteral, sym(3).NumericLiteral );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

mustbe_value        : r_string  THROUGH r_string;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintValue> (nodePool(), sym(1).Node );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./


mustbe_value        : r_string clause_as;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintValue> (nodePool(), sym(1).Node, sym(2).LineItemAsClause );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

mustbe_value        : r_number clause_as;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintValue> (nodePool(), sym(1).Node, sym(2).LineItemAsClause );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

mustbe_value        : r_literal clause_as;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintLiteral> (nodePool(), sym(1).Literal, sym(2).LineItemAsClause);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

mustbe_value        : r_string;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintValue> (nodePool(), sym(1).Node );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

mustbe_value        : r_number;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintValue> (nodePool(), sym(1).Node );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    } break;
./

mustbe_value        : r_literal;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintLiteral> (nodePool(), sym(1).Literal);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./


mustbe_value        : REGEX r_string;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::ConstraintRegex> (nodePool(), sym(2).StringLiteral );
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

/./**
   * OCCURS Clause
   */./
clause_occurs      : OCCURS r_number times_opt indexed_by_opt comma_opt;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemOccursClause> (nodePool(), sym(2).NumericLiteral->val_);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(5));
    } break;
./

clause_occurs      : OCCURS r_number TO r_number TIMES 
                     DEPENDING ON r_literal indexed_by_opt comma_opt;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemOccursDependingOnClause> (nodePool(), sym(2).NumericLiteral, sym(4).NumericLiteral, sym(8).Literal);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(10));
    } break;
./

clause_occurs      : OCCURS r_literal times_opt indexed_by_opt comma_opt;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemOccursLiteralClause> (nodePool(), sym(2).Literal);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(5));
    } break;
./

clause_occurs      : OCCURS r_literal TO NUMBER TIMES 
                     DEPENDING ON LITERAL indexed_by_opt comma_opt;
/.
    case $rule_number: {
        qDebug()<<"Clause not implemented"; Q_ASSERT(0);
    } break;
./

times_opt          : ;
times_opt          : TIMES;
indexed_by_opt     : ;
indexed_by_opt     : INDEXED BY LITERAL;


/./**
   * SPI-NULL Clause
   */./
clause_spi_null    : SPI_NULL STRING;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
    }break;
./
clause_spi_null    : SPI_NULL NUMBER;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
    }break;
./
clause_spi_null    : SPI_NULL LITERAL;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
    }break;
./

/./**
   * REDEFINES Clause
   */./
clause_redefines      : REDEFINES r_literal;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemRedefinesClause> (nodePool(), sym(2).Literal);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

/./**
   * SQL-NULLABLE Clause
   */./
clause_sql_nullable   : NOT SQL_NULLABLE;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemRedefinesClause> (nodePool(), sym(2).Literal);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    } break;
./

clause_sql_nullable   : SQL_NULLABLE;
/.
    case $rule_number: {
        qDebug()<<"Clause not implemented"; Q_ASSERT(0);
    }break;
./

/./**
   * UPSHIFT Clause
   */./
clause_upshift        : UPSHIFT;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
    }break;
./

/./**
   * USAGE Clause
   */./
clause_usage          : USAGE is_opt INDEX;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
    }break;
./

clause_usage          : INDEX;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
    }break;
./

clause_usage          : USAGE is_opt PACKED_DECIMAL;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemPackedDecimalClause> (nodePool());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    }break;
./

clause_usage          : PACKED_DECIMAL;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemPackedDecimalClause> (nodePool());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    }break;
./

clause_usage          : USAGE is_opt COMP;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::PicCompClause> (nodePool());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(3));
    }break;
./

clause_usage          : COMP;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::PicCompClause> (nodePool());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    }break;
./



/./**
   * VALUE Clause combined with 88_clause (ignored)
   */./
clause_value          : NOVALUE;
/.
    case $rule_number: {
      sym(1).Node = AST::makeAstNode<AST::LineItemNotImplementedClause>(nodePool());
    }break;
./

clause_value          : VALUE IS mustbe_values;
/.
    case $rule_number: {
        sym(1).Node = sym(3).ConstraintList->finish();
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

clause_value          : VALUE mustbe_values;
/.
    case $rule_number: {
        sym(1).Node = sym(2).ConstraintList->finish();
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./


/./*
values                : value;
values                : values COMMA value;

value                 : STRING through_opt;
value                 : LITERAL through_opt;
value                 : NUMBER through_opt;
*/./

is_opt                : IS;
is_opt                : ARE;
is_opt                : ;

/./*
through_opt           : ;
through_opt           : THROUGH NUMBER;
through_opt           : THROUGH STRING;
*/./

clause_charset        : CHARSET ASCII;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::GroupDefEbcdicClause> (nodePool());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    }break;
./

clause_charset        : CHARSET EBCDIC;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::GroupDefEbcdicClause> (nodePool());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(2));
    }break;
./

clause_highlow       : HIGH_LOW;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::GroupDefHighLowClause> (nodePool());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

clause_lowhigh       : LOW_HIGH;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::GroupDefLowHighClause> (nodePool());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

clause_choice        : CHOICE;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::LineItemChoiceClause> (nodePool());
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./


/./*************************************************************************
   * WORKAROUND FOR LITERALS DOUBLING AS KEYWORDS IN LINE NUMBER STATEMENT *
   *************************************************************************/./
keywords        : keywords_nokey;
keywords        : KEY;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

keywords_nokey : CREATION_KEYWORD;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

keywords_nokey : CHOICE ;
/.
    case $rule_number: {
        sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
        UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
    } break;
./

keywords_nokey : INDEX ;
/.
   case $rule_number: {
       sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
       UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
   } break;
./


keywords_nokey : KEY_ASGN_KEYWORD;
/.
   case $rule_number: {
       sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
       UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
   } break;
./

keywords_nokey : FILE;
/.
   case $rule_number: {
       sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
       UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
   } break;
./

keywords_nokey : FILLER;
/.
   case $rule_number: {
       sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), 
              (QString(sym(1).val)+"_"+QString::number(filler_++)).toLatin1().constData());
       UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
   } break;
./

keywords_nokey: RECORD;
/.
   case $rule_number: {
       sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
       UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
   } break;
./

keywords_nokey: TYPE;
/.
   case $rule_number: {
       sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
       UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
   } break;
./

keywords_nokey: SSID;
/.
   case $rule_number: {
       sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
       UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
   } break;
./

keywords_nokey: UNSIGNED;
/.
   case $rule_number: {
       sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
       UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
   } break;
./

keywords_nokey: VALUE;
/.
   case $rule_number: {
       sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
       UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
   } break;
./

keywords_nokey: EBCDIC;
/.
   case $rule_number: {
       sym(1).Node = AST::makeAstNode<AST::Literal> (nodePool(), sym(1).val);
       UPDATE_POSITION(sym(1).Node, loc(1), loc(1));
   } break;
./

/.
         } // switch

    state_stack [tos] = nt_action (act, lhs [r] - TERMINAL_COUNT);
        
    if (rhs[r] > 1) {
            location_stack[tos - 1].end_line = location_stack[tos + rhs[r] - 2].end_line;
            location_stack[tos - 1].end_column = location_stack[tos + rhs[r] - 2].end_column;
            location_stack[tos] = location_stack[tos + rhs[r] - 1];
    }
}

else {
    int ers = state;
    int shifts = 0;
    int reduces = 0;
    int expected_tokens [3];
    for (int tk = 0; tk < TERMINAL_COUNT; ++tk) {
            int k = t_action (ers, tk);
            if (!k)
                continue;
            else if (k < 0)
                ++reduces;
            else if (spell [tk]) {
                if (shifts < 3)
                    expected_tokens [shifts] = tk;
                ++shifts;
            }
    }

    error_message_.clear ();
    if (shifts && shifts < 3) {
            bool first = true;

            for (int s = 0; s < shifts; ++s) {
                if (first)
                    error_message_ += QLatin1String ("Expected ");
                else
                    error_message_ += QLatin1String (", ");

                first = false;
                error_message_ += QLatin1String("`");
                error_message_ += QLatin1String (spell [expected_tokens [s]]);
            }
    }

    error_message_ = QString().setNum(location_stack[tos].start_line) + QLatin1String(":") + 
            QString().setNum(location_stack[tos].start_column) + QLatin1String("  ")+error_message_;

    return false;
}   
    }

    return false;
   }

./
/:

#endif // DDLParser
:/

#ifndef CONTAINERITEM_H
#define CONTAINERITEM_H

#include "itembase.h"

#include "atom.h" //@I cannot include definition.h (cyclic include)

#include <QtCore/QList>
#include <QtCore/QString>

#include <assert.h>

class ContainerItem : public CodeModelItemBase
{
    friend class LineDefinitionItem;
    friend class GroupDefinitionItem;
public:
    ContainerItem( int kind_= kindUndefined ) ;
    virtual ~ContainerItem();

    // add an item to the container
    virtual void add( ContainerItem *li );

    // add a redefine to the container
    virtual void addRedefine( ContainerItem *redefine );

    // get all items contained
    QList <ContainerItem*> childs() const;

    // find a child by name
    virtual ContainerItem* findChild( QString name );

    virtual Definition definition() const;


    void addConstraintRange( int from, int to, QString display);
    void addConstraintValue( QVariant value, QString display);
    void addConstraintRegex( QString value, QString display);
    void setConstraintList(QList<Constraint *> l) { constraints_ = l; }
    QList<Constraint *> constraints() const { return constraints_; }


    bool hasChilds() const;
    bool hasOccursDepend() const;
    bool hasOccursNumber() const;
    bool hasRedefines() const;

    //will return false
    virtual bool isDefined();
    virtual bool isChoice() const { return choice_; }
    virtual bool isRedefine() const { return redefine_; }
    virtual bool isRecord()   const { return record_; }
    virtual bool isKey() const { return key_; }
    
    virtual unsigned int length();

    unsigned int occursNumber() const;
    QString occursDepend() const;
    int occursDependFrom() const;
    int occursDependTo() const;

    // get next sibling
    ContainerItem *nextSibling();

    // returns the parent
    ContainerItem* parent() const;

    // previous sibling
    ContainerItem *prevSibling();

    // get the redefines list
    QList <ContainerItem*> redefines() const;

    // remove a child by name
    void removeChild( QString name );
    void setChoice( bool t ) { choice_ = t; }
    void setOccursDepend( QString dep, int from = -1, int to = -1);
    void setOccursNumber( unsigned int num );
    void setParent( ContainerItem *parent );
    void setRedefine( bool t ) { redefine_ = t; }
    void setKey( bool k ) { key_ = k; }
    void setRecord( bool r ) { record_ = r; }
    
    virtual CodeModelItemBase* clone();

protected:
    Definition definition_;
    
private:
    ContainerItem *parent_;
    QList <ContainerItem*> items_;
    QList <ContainerItem*> redefines_;
    unsigned int length_;
    unsigned int num_occurs_;
    QString      dep_occurs_;
    int dep_occurs_from_;
    int dep_occurs_to_;
    QList<Constraint *> constraints_;
    bool choice_;
    bool redefine_;
    bool record_;
    bool key_;
};
#endif //CONTAINERITEM_H

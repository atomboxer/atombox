
#include "itemlinedefinition.h"

#include <QtCore/QDebug>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

LineDefinitionItem::LineDefinitionItem( int kind ) 
    : ContainerItem(kind)
{

}

LineDefinitionItem::~LineDefinitionItem()
{

}

void 
LineDefinitionItem::define( AtomLengthType lengthType, 
                            unsigned length, 
                            AtomDataType data_type_, 
                            QRegExp regex) 
{ 
    definition_.length_type_ = lengthType; 
    definition_.length_      = length;
    definition_.data_type_   = data_type_;
    definition_.regex_       = regex; 
    definition_.display_     = "";
    definition_.byte_order_  = DefaultByteOrder;
}

void 
LineDefinitionItem::define( Definition definition ) 
{ 
    definition_ = definition; 
}
    
bool 
LineDefinitionItem::isDefined() { 
    return definition_.isDefined(); 
}

unsigned int 
LineDefinitionItem::length() 
{ 
    if ( isDefined() ) {
        return definition_.length_ * occursNumber();
    }
    else { 
        return ContainerItem::length();
    }
}

void
LineDefinitionItem::setDefault(QVariant _default)
{
    definition_.default_ = _default;
}

void 
LineDefinitionItem::setDisplayString(QString display) 
{ 
    definition_.display_ = display; 
}

void 
LineDefinitionItem::setIsoBitmap( bool toogle ) 
{ 
    if (toogle) {
        definition_.isobitmap_flg_ = true;
    } else { 
        definition_.isobitmap_flg_ = false;
    }
}

void
LineDefinitionItem::setLPad(QChar c)
{
    definition_.lpadc_ = c;
}

void 
LineDefinitionItem::setByteOrder(int o) 
{ 
    definition_.byte_order_ = (ByteOrder)o;
}

void 
LineDefinitionItem::setBitmap(ssize_t size) 
{ 
    definition_.data_type_ = BITMAP;
    definition_.length_ = size;
}

void 
LineDefinitionItem::setUBitmap(ssize_t size) 
{ 
    definition_.data_type_ = UBITMAP;
    definition_.length_ = size;
}

void 
LineDefinitionItem::setBCD() 
{ 
    definition_.data_type_ = BCD;
}

void 
LineDefinitionItem::setPackedBCD() 
{ 
    definition_.data_type_ = BCD_PACKED;
}

void
LineDefinitionItem::setPackedHex() 
{ 
    definition_.data_type_ = HEX_PACKED;
}

void
LineDefinitionItem::setRPad(QChar c)
{
    definition_.rpadc_ = c;
}

void
LineDefinitionItem::setVariableLength(QString dependant) 
{ 
    definition_.length_type_ = VARIABLE_LENGTH;
    definition_.vlength_dependant_ = dependant;
}

void
LineDefinitionItem::setVariableLengthInclusive(QString dependant) 
{ 
    definition_.length_type_ = VARIABLE_LENGTH_INCLUSIVE;
    definition_.vlength_dependant_ = dependant;
}

void
LineDefinitionItem::setVariableLengthAllInclusive(QString dependant) 
{ 
    definition_.length_type_ = VARIABLE_LENGTH_ALLINCLUSIVE;
    definition_.vlength_dependant_ = dependant;
}

void
LineDefinitionItem::setVariableDelimited( QChar delimiter ) 
{
    definition_.length_type_ =  VARIABLE_DELIMITED;
    definition_.vlength_delimiter_ = delimiter;
}

CodeModelItemBase*
LineDefinitionItem::clone()
{
    LineDefinitionItem *_clone = new LineDefinitionItem(this->kind());

    QList<Constraint*> cl;
    foreach (Constraint *c, this->constraints()) {
        cl.push_back(c->clone());
    }

    _clone->setConstraintList(this->constraints());

    //items
    QList<ContainerItem*> items;

    foreach (ContainerItem *c,this->items_ ) {
        items.push_back((ContainerItem*)c->clone());
    }
    _clone->items_ = items;

    //redefines
    QList<ContainerItem*> redefines;
    foreach (ContainerItem *c,this->redefines_ ) {
        redefines.push_back((ContainerItem*)c->clone());
    }
    _clone->setName(this->name());

    _clone->choice_ = this->choice_;
    _clone->definition_ = this->definition_;
    _clone->dep_occurs_ = this->dep_occurs_;
    _clone->dep_occurs_from_ = this->dep_occurs_from_;
    _clone->dep_occurs_to_ = this->dep_occurs_to_;
    _clone->key_ = this->key_;
    _clone->length_ = this->length_;
    _clone->num_occurs_ = this->num_occurs_;
    _clone->parent_ = this->parent_;
    _clone->record_ = this->record_;
    _clone->redefine_ = this->redefine_;
    return _clone;
}

#include <QUrl>
#include <QtCore/QString>
#include <QtCore/QByteArray>
#include <QTextStream>
#include <QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QVariant>

#include "generatedparser.h"
#include "generatedgrammar_p.h"
#include "astparser.h"
#include "ast.h"
#include "astvisitor.h"
#include "codemodel.h"
#include "astprocessor.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

typedef struct yy_buffer_state *YY_BUFFER_STATE;
extern YY_BUFFER_STATE ddl_scan_string (const char *text);
extern void ddl_delete_buffer( YY_BUFFER_STATE);
extern int line;
extern int column;
extern QList<char *> yy_allocations; 
 
/*
 * DDL AST Parser
 */
DDLASTParser::DDLASTParser()
{
    model_ = new DDLCodeModel();
}

DDLASTParser::~DDLASTParser()
{
    if (model_)
        delete model_;
}

bool DDLASTParser::parse(const QString &data, const QUrl &url)
{
    QString fileName = url.toString();
    script_file_ = fileName;
    line = 1;  
    column = 1;
    errors_.clear();

    DDLParser parser;

    YY_BUFFER_STATE s;
    s = ddl_scan_string(data.toLatin1().data());

    if ( parser.parse() == false ) {
        qDebug()<<QLatin1String("ddl parse error... ")+
            fileName+" "+parser.errorMessage();
        QCoreApplication::exit(-1);
        exit(-1);
    }

    foreach(char *c, yy_allocations) {
       if (c) {
            free(c);
       }
   }

    yy_allocations.clear();

    ddl_delete_buffer(s);

    ProcessAST process(this, model_);
    process(data, parser.ast());

    QList<DDLError> list_err;

    foreach (DDLError e, process.errors() ) {
        DDLError er = e;
        er.setUrl(QLatin1String("file:") + fileName);
        list_err.push_back(er);
    }


    if ( !process.errors().isEmpty() ) {
        errors_ = list_err;
        return false;
    }

    model_->processRedefines();

    return process.errors().isEmpty();
}

#include "itembase.h"
#include <QtCore/QDebug>

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

CodeModelItemBase::CodeModelItemBase( int kind)
    :  kind_(kind)
{
}

CodeModelItemBase::~CodeModelItemBase()
{
}

int 
CodeModelItemBase::kind() const 
{ 
    return kind_; 
}

QString 
CodeModelItemBase::name() const 
{ 
    return name_; 
}

void
CodeModelItemBase::setKind(int kind) 
{
    kind_ = kind;
}

void
CodeModelItemBase::setName(const QString &name)
{
    name_ = name;
}

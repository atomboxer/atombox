
#ifndef DDL_AST_VISITOR_H
#define DDL_AST_VISITOR_H

#include "ast.h"
#include <QtCore/QDebug>

namespace AST {
    class Visitor;
    class Node;
    class Literal;
    class NumericLiteral;
    class StringLiteral;
    class StatementList;
    class ConstStatement;
    class Constraint;
    class ConstraintList;
    class ConstraintRange;
    class ConstraintRegex;
    class ConstraintValue;
    class ConstraintLiteral;
    class EnumDefStatement;
    class FieldDefStatement;
    class GroupDefStatement;
    class RecordDefStatement;
    class LineItemList;
    class LineItem;
    class GroupDefClause;
    class GroupDefClauseList;
    class LineItemNotImplementedClause;
    class GroupDefEbcdicClause;
    class GroupDefHighLowClause;
    class GroupDefLowHighClause;
    class LineItemClauseList;
    class LineItemAsClause;
    class LineItemRedefinesClause;
    class LineItemOccursClause;
    class LineItemOccursLiteralClause;
    class LineItemOccursDependingOnClause;
    class LineItemDisplayClause;
    class LineItemDefaultClause;
    class LineItemDefaultEmptyClause;
    class LineItemLPadClause;
    class LineItemRPadClause;
    class LineItemKeyClause;
    class LineItemKeyIsClause;
    class LineItemKeyIsClauseList;
    class LineItemVLengthClause;
    class LineItemVLengthInclusiveClause;
    class LineItemVLengthAllInclusiveClause;
    class LineItemVLengthDelimiterClause;
    class LineItemIsoBitmapClause;
    class LineItemPackedDecimalClause;
    class LineItemChoiceClause;
    class PicClause;
    class PicCompClause;
    class PicBinaryClause;
    class PicBitClause;
    class TypeClause;
    class TypeLiteralClause;
    class TypeStarClause;

    class Visitor
    {
      public:
        Visitor();
        virtual ~Visitor();

        virtual bool preVisit(Node *n) { return true; }
        virtual void postVisit(Node *n) {}

        //literal
        virtual bool visit(Literal *) {
            return true;
        }    
        virtual void endVisit(Literal *) {
        }

        //literal numeric
        virtual bool visit(NumericLiteral *) {
            return true;
        }    
        virtual void endVisit(NumericLiteral *) {
        }

        //literal string
        virtual bool visit(StringLiteral *) {
            return true;
        }    
        virtual void endVisit(StringLiteral *) {
        }

        //statement_list
        virtual bool visit(StatementList *) {
            return true;
        }    
        virtual void endVisit(StatementList *) {
        }

        //const_statement
        virtual bool visit(ConstStatement *) {
            return true; 
        }    

        virtual void endVisit(ConstStatement *) {
        }

        //constraint
        virtual bool visit(Constraint *) {
            return true; 
        }    

        virtual void endVisit(Constraint *) {
        }

        //constraint_list
        virtual bool visit(ConstraintList *) {
            return true; 
        }    

        virtual void endVisit(ConstraintList *) {
        }

        //constraint_range
        virtual bool visit(ConstraintRange *) {
            return true; 
        }    

        virtual void endVisit(ConstraintRange *) {
        }

        //constraint_regex
        virtual bool visit(ConstraintRegex *) {
            return true; 
        }    

        virtual void endVisit(ConstraintRegex *) {
        }

        //constraint_value
        virtual bool visit(ConstraintValue *) {
            return true; 
        }    

        virtual void endVisit(ConstraintValue *) {
        }

        //constraint_literal
        virtual bool visit(ConstraintLiteral *) {
            return true; 
        }    

        virtual void endVisit(ConstraintLiteral *) {
        }

        //Field Definition
        virtual bool visit(FieldDefStatement *) {
            return true; 
        }    

        virtual void endVisit(FieldDefStatement *) {
        }

        //Enum Definition
        virtual bool visit(EnumDefStatement *) {
            return true; 
        }    

        virtual void endVisit(EnumDefStatement *) {
        }

        //Group Definition
        virtual bool visit(GroupDefStatement *) {
            return true; 
        }    

        virtual void endVisit(GroupDefStatement *) {
        }

        //Record Definition
        virtual bool visit(RecordDefStatement *) {
            return true; 
        }    

        virtual void endVisit(RecordDefStatement *) {
        }


        //LineItemList
        virtual bool visit(LineItemList *) {
            return true; 
        }    

        virtual void endVisit(LineItemList *) {
        }

        //LineItem
        virtual bool visit(LineItem *) {
            return true; 
        }    

        virtual void endVisit(LineItem *) {
        }

//
        //GroupDefClauseList
        virtual bool visit(GroupDefClauseList*) {
            return true;
        }
        virtual void endVisit(GroupDefClauseList*) {
        }

        //GroupDefClause
        virtual bool visit(GroupDefClause*) {
            return true;
        }
        virtual void endVisit(GroupDefClause*) {
        }

        //GroupDefClause
        virtual bool visit(GroupDefEbcdicClause*) {
            return true;
        }
        virtual void endVisit(GroupDefEbcdicClause*) {
        }

        //GroupDefClause
        virtual bool visit(GroupDefHighLowClause*) {
            return true;
        }
        virtual void endVisit(GroupDefHighLowClause*) {
        }

        //GroupDefClause
        virtual bool visit(GroupDefLowHighClause*) {
            return true;
        }
        virtual void endVisit(GroupDefLowHighClause*) {
        }

//
        //LineItemClauseList
        virtual bool visit(LineItemClauseList*) {
            return true;
        }
        virtual void endVisit(LineItemClauseList*) {
        }

        //LineItemNotImplementedClause
        virtual bool visit(LineItemNotImplementedClause*) {
            return true;
        }
        virtual void endVisit(LineItemNotImplementedClause*) {
        }

        //LineItemAsClause
        virtual bool visit(LineItemAsClause*) {
            return true;
        }
        virtual void endVisit(LineItemAsClause*) {
        }

        //LineItemRedefinesClause
        virtual bool visit(LineItemRedefinesClause*) {
            return true;
        }
        virtual void endVisit(LineItemRedefinesClause*) {
        }

        //LineItemOccursClause
        virtual bool visit(LineItemOccursClause*) {
            return true;
        }
        virtual void endVisit(LineItemOccursClause*) {
        }

        //LineItemOccursLiteralClause
        virtual bool visit(LineItemOccursLiteralClause*) {
            return true;
        }
        virtual void endVisit(LineItemOccursLiteralClause*) {
        }

        //LineItemOccursClause
        virtual bool visit(LineItemOccursDependingOnClause*) {
            return true;
        }
        virtual void endVisit(LineItemOccursDependingOnClause*) {
        }

        //LineItemDisplayClause
        virtual bool visit(LineItemDisplayClause*) {
            return true;
        }
        virtual void endVisit(LineItemDisplayClause*) {
        }

        //LineItemDefaultClause
        virtual bool visit(LineItemDefaultClause*) {
            return true;
        }
        virtual void endVisit(LineItemDefaultClause*) {
        }

        //LineItemDefaultEmptyClause
        virtual bool visit(LineItemDefaultEmptyClause*) {
            return true;
        }
        virtual void endVisit(LineItemDefaultEmptyClause*) {
        }

        //LineItemLPadClause
        virtual bool visit(LineItemLPadClause*) {
            return true;
        }
        virtual void endVisit(LineItemLPadClause*) {
        }

        //LineItemRPadClause
        virtual bool visit(LineItemRPadClause*) {
            return true;
        }
        virtual void endVisit(LineItemRPadClause*) {
        }

        //LineItemKeyClause
        virtual bool visit(LineItemKeyClause*) {
            return true;
        }
        virtual void endVisit(LineItemKeyClause*) {
        }

        //LineItemKeyIsClause
        virtual bool visit(LineItemKeyIsClause*) {
            return true;
        }
        virtual void endVisit(LineItemKeyIsClause*) {
        }

        //LineItemKeyIsClauseList
        virtual bool visit(LineItemKeyIsClauseList*) {
            return true;
        }
        virtual void endVisit(LineItemKeyIsClauseList*) {
        }

        //LineItemVLengthClause
        virtual bool visit(LineItemVLengthClause*) {
            return true;
        }
        virtual void endVisit(LineItemVLengthClause*) {
        }

        //LineItemVLengthInclusiveClause
        virtual bool visit(LineItemVLengthInclusiveClause*) {
            return true;
        }
        virtual void endVisit(LineItemVLengthInclusiveClause*) {
        }

        //LineItemVLengthAllInclusiveClause
        virtual bool visit(LineItemVLengthAllInclusiveClause*) {
            return true;
        }
        virtual void endVisit(LineItemVLengthAllInclusiveClause*) {
        }

        //LineItemVLengthDelimiterClause
        virtual bool visit(LineItemVLengthDelimiterClause*) {
            return true;
        }
        virtual void endVisit(LineItemVLengthDelimiterClause*) {
        }

        //LineItemIsoBitmap
        virtual bool visit(LineItemIsoBitmapClause*) {
            return true;
        }
        virtual void endVisit(LineItemIsoBitmapClause*) {
        }

        //LineItemPackedDecimal
        virtual bool visit(LineItemPackedDecimalClause*) {
            return true;
        }
        virtual void endVisit(LineItemPackedDecimalClause*) {
        }

        //LineItemChoice
        virtual bool visit(LineItemChoiceClause*) {
            return true;
        }
        virtual void endVisit(LineItemChoiceClause*) {
        }

        //PicClause
        virtual bool visit(PicClause *) {
            return true; 
        }    

        virtual void endVisit(PicClause *) {
        }

        //PicCompClause
        virtual bool visit(PicCompClause *) {
            return true; 
        }    

        virtual void endVisit(PicCompClause *) {
        }

        //PicBinaryClause
        virtual bool visit(PicBinaryClause *) {
            return true; 
        }    

        virtual void endVisit(PicBinaryClause *) {
        }

        //PicBitClause
        virtual bool visit(PicBitClause *) {
            return true; 
        }    

        virtual void endVisit(PicBitClause *) {
        }



        //TypeClause
        virtual bool visit(TypeClause *) {
            return true; 
        }    
        virtual void endVisit(TypeClause *) {
        }

        virtual bool visit(TypeLiteralClause *) {
            return true; 
        }    
        virtual void endVisit(TypeLiteralClause *) {
        }

        virtual bool visit(TypeStarClause *) {
            return true; 
        }    
        virtual void endVisit(TypeStarClause *) {
        }

    };

} // namespace AST

#endif //DDL_AST_VISITOR_H

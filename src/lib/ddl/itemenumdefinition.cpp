#include "itemenumdefinition.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

EnumDefinitionItem::EnumDefinitionItem( int kind )
    :CodeModelItemBase(kind)
{}

CodeModelItemBase*
EnumDefinitionItem::clone()
{
    EnumDefinitionItem *_clone = new EnumDefinitionItem(this->kind());
    _clone->setConstraintList(this->astConstraintList());
    return _clone;
}

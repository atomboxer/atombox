#ifndef LINEITEM_H
#define LINEITEM_H

#include <QtCore/QString>

#include "itembase.h"
#include "itemcontainer.h"
#include "definition.h"

#include "constraint.h"

class LineDefinitionItem : public ContainerItem
{
public:
    LineDefinitionItem(int kind = kindLine);
    virtual ~LineDefinitionItem();

    void define( AtomLengthType lengthType, 
                 unsigned length, 
                 AtomDataType data_type_ = ASCII, 
                 QRegExp regex = QRegExp(""));
    void define( Definition definition );

    bool isDefined();
    virtual unsigned int length();

    CodeModelItemBase* clone();

    void setByteOrder(int o);
    void setDefault(QVariant _default=QVariant());
    void setDisplayString(QString display);
    void setIsoBitmap( bool toogle );
    void setLPad(QChar c);
    void setBitmap(ssize_t size);
    void setUBitmap(ssize_t size);
    void setBCD();
    void setPackedBCD();
    void setPackedHex();
    void setRPad(QChar c);
    void setVariableLength(QString dependant);
    void setVariableLengthInclusive(QString dependant);
    void setVariableLengthAllInclusive(QString dependant);
    void setVariableDelimited(QChar delimiter);

};

#endif //LINEITEM_H

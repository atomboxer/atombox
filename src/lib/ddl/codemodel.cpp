#include "codemodel.h"

#include <QtCore/QDebug>
#include <iostream>

#include "boxcomponent.h"
#include "box.h"
#include "atomstring.h"
#include "atomstringnumeric.h"
#include "atomstringalpha.h"
#include "atomnumericbcd.h"
#include "atomnumericpacked.h"
#include "atomhexpacked.h"
#include "atomisobitmaphex.h"
#include "atomisobitmapbinary.h"
#include "atombinary.h"
#include "atombitmap.h"

#include "wrapboxcomponent.h"
#include <assert.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif



//
//  DDLCodeModel
//
DDLCodeModel::DDLCodeModel()
{
}

DDLCodeModel::~DDLCodeModel()
{
    //destroy
    foreach (ConstantItem *v, constants_.values()) {
        if (v) {
            delete v;
            v = 0;
        }
    }
    constants_.clear();

    foreach (EnumDefinitionItem *v, enum_definitions_.values()) {
        if (v) {
            delete v;
            v = 0;
        }
    }
    enum_definitions_.clear();

    foreach (FieldDefinitionItem *v, field_definitions_.values()) {
        if (v) {
            delete v;
            v = 0;
        }
    }
    field_definitions_.clear();

    foreach (GroupDefinitionItem *v, group_definitions_.values()) {
        if (v) {

            delete v;
            v = 0;
        }
    }

    group_definitions_.clear();
}

void
DDLCodeModel::addConstant(ConstantItem *item)
{
    constants_.insert(item->name(),item);
}

void
DDLCodeModel::addEnumDefinition(EnumDefinitionItem *item)
{
    enum_definitions_.insert(item->name().replace("-","_"),item);
}

void
DDLCodeModel::addFieldDefinition(FieldDefinitionItem *item)
{
    field_definitions_.insert(item->name().replace("-","_"),item);
}

void
DDLCodeModel::addGroupDefinition(GroupDefinitionItem *item)
{
    group_definitions_.insert(item->name(),item);
}

//remove
void
DDLCodeModel::removeConstant(QString name)
{
    if (constants_.find(name) != constants_.end()) {
        delete constants_[name];
        constants_.remove(name);
    }
}

void
DDLCodeModel::removeEnumDefinition(QString name)
{
    if (enum_definitions_.find(name) != enum_definitions_.end()) {
        delete enum_definitions_[name];
        enum_definitions_.remove(name);
    }
}

void
DDLCodeModel::removeFieldDefinition(QString name)
{
    if (field_definitions_.find(name) != field_definitions_.end()) {
        delete field_definitions_[name];
        field_definitions_.remove(name);
    }
}

void
DDLCodeModel::removeGroupDefinition(QString name)
{
    if (group_definitions_.find(name) != group_definitions_.end()) {
        delete group_definitions_[name];
        group_definitions_.remove(name);

    }
}

ConstantItem*
DDLCodeModel::findConstant(const QString &name) const
{
    ConstantItem *ret = constants_.value(name);
    if (!ret)
        ret = constants_.value(name.toUpper());
    if (!ret)
        ret = constants_.value(name.toLower());
    return ret;
}

EnumDefinitionItem*
DDLCodeModel::findEnumDefinition(const QString &name) const
{
    EnumDefinitionItem *ret = enum_definitions_.value(name);
    if (!ret)
        ret = enum_definitions_.value(name.toUpper());
    if (!ret)
        ret = enum_definitions_.value(name.toLower());
    return ret;
}

FieldDefinitionItem*
DDLCodeModel::findFieldDefinition(const QString &name) const
{
    FieldDefinitionItem *ret = field_definitions_.value(name);
    if (!ret)
        ret = field_definitions_.value(name.toUpper());
    if (!ret)
        ret = field_definitions_.value(name.toLower());
    return ret;
}

GroupDefinitionItem*
DDLCodeModel::findGroupDefinition(const QString &name) const
{
    GroupDefinitionItem *ret = group_definitions_.value(name);
    if (!ret)
        ret = group_definitions_.value(name.toUpper());
    if (!ret)
        ret = group_definitions_.value(name.toLower());
    return ret;
}

QHash<QString, ConstantItem*>
DDLCodeModel::constantMap() const
{
    return constants_;
}

QHash<QString, EnumDefinitionItem*>
DDLCodeModel::enumDefinitionMap() const
{
    return enum_definitions_;
}

QHash<QString, FieldDefinitionItem*>
DDLCodeModel::fieldDefinitionMap() const
{
    return field_definitions_;
}

QHash<QString, GroupDefinitionItem*>
DDLCodeModel::groupDefinitionMap() const
{
    return group_definitions_;
}

void
DDLCodeModel::dumpContainer( ContainerItem *item, QString padding )
{
    std::cout<<padding.toAscii().data()<<item->name().toAscii().data();
    if (item->isDefined()) {
        LineDefinitionItem *li = dynamic_cast<LineDefinitionItem*>(item);
        Definition def = li->definition();
        if (def.length_ != 0) {
            switch (def.length_type_) {
            case FIXED: std::cout<<" FIXED"; break;
            default:  std::cout<<" VARIABLE";break;
            }
            switch (def.data_type_) {
            case ASCII: std::cout<<" ASCII"; break;
            case ASCII_NUMERIC: std::cout<<" ASCII_NUMERIC"; break;
            case ASCII_ALPHABETIC: std::cout<<" ASCII_ALPHABETIC"; break;
            case EBCDIC: std::cout<<" EBCDIC"; break;
            case EBCDIC_NUMERIC: std::cout<<" ENUMERIC"; break;
            case BITMAP: std::cout<<" BITMAP"; break;
            case UBITMAP: std::cout<<" UBITMAP"; break;
            case BCD: std::cout<<" BCD"; break;
            case BCD_PACKED: std::cout<<" BCD_PACKED"; break;
            case HEX_PACKED: std::cout<<" HEX_PACKED"; break;
            case INT8: std::cout<<" INT8"; break;
            case INT16: std::cout<<" INT16"; break;
            case INT32: std::cout<<" INT32"; break;
            case INT64: std::cout<<" INT64"; break;
            case UINT8: std::cout<<" UINT8"; break;
            case UINT16: std::cout<<" UINT16"; break;
            case UINT32: std::cout<<" UINT32"; break;
            case UINT64: std::cout<<" UINT64"; break;
            case FLOAT: std::cout<<" FLOAT"; break;
            case DOUBLE: std::cout<<" DOUBLE"; break;
            default: break;
            }
            std::cout <<"("<<def.length_<<","<<def.length_type_<<")";

            if (!li->constraints().isEmpty()) {
                foreach (Constraint *c, li->constraints()) {
                    std::cout << "c:" << c->display().toLatin1().data();
                }
            }
        }

    }
    else {
        if (item->isChoice())
            std::cout <<" CHOICE: "<<item->isChoice();
    }
    if (item->hasOccursNumber()) {
        std::cout<<":"<<item->length()<<" OCCURS "<<item->occursNumber()<<" TIMES";
    } else if (item->hasOccursDepend()) {
        std::cout<<":"<<item->length()<<" OCCURS "<<item->occursDependFrom()
                 <<" to "<< item->occursDependTo()<<" DEPENDING ON "
                 <<item->occursDepend().toLatin1().data();
    } else {
        std::cout<<":"<<item->length();
    }

    std::cout<<std::endl;

    if (item->hasChilds()) {
        ContainerItem *child;
        foreach (child, item->childs()) {
            padding.append("    ");
            dumpContainer(child,padding);
            padding.chop(4);
        }
    }

    if (item->hasRedefines()) {
        ContainerItem *r;
        foreach (r, item->redefines()) {
            padding.append("R   ");
            dumpContainer(r,padding);
            padding.chop(4);
        }
    }
}

void
DDLCodeModel::dump()
{
    QHash<QString, GroupDefinitionItem*>::Iterator it = group_definitions_.begin();
    qDebug()<<"======================CODE MODEL DUMP==============================";
    while (it != group_definitions_.end()) {
        dumpContainer(it.value(),QString());
        ++it;
    }
    qDebug()<<"===================================================================";
}

BoxComponent *
DDLCodeModel::dumpAtomBoxModel( ContainerItem *item, BoxComponent *root )
{
    bool top_lvl = false;

    if (root == 0) {
        top_lvl = true;
        root = new Box(item->name());
        ((Box*)root)->setChoice(item->isChoice());
        ((Box*)root)->setRecord(item->isRecord());
    }

    if (item->isDefined()) { //IS THIS AN ATOM?
        Definition def = item->definition();
        Atom *atom = NULL;
        if (def.length_ != 0) {
            if ( def.isIsoBitmap()) {
                if (def.data_type_ == BCD || def.data_type_ == BCD_PACKED ) {
                    atom = new AtomIsoBitmapBinary(item->name().toAscii(), def);
                }
                else if (def.data_type_ == ASCII || def.data_type_ == EBCDIC){
                    atom = new AtomIsoBitmapHex(item->name().toAscii(), def);
                }
                else {
                    qDebug()<<def.data_type_;
                    qFatal("codemodel.cpp Unsupported isobitmap construction");
                }

                root->add(atom);
                root = atom;
            }
            else
                switch (def.data_type_) {
                case ASCII: {
                    atom = new AtomString(item->name().toAscii(), def);
                    root->add(atom);
                    root = atom;
                } break;

                case ASCII_ALPHABETIC: {
                    atom = new AtomStringAlpha(item->name().toAscii(), def);
                    root->add(atom);
                    root = atom;
                } break;

                case ASCII_NUMERIC: {
                    atom = new AtomStringNumeric(item->name().toAscii(), def);
                    root->add(atom);
                    root = atom;
                } break;

                case EBCDIC: {
                    atom = new AtomString(item->name(),
                                          def, AtomString::EBCDIC);
                    root->add(atom);
                    root = atom;
                } break;

                case EBCDIC_NUMERIC: {
                    atom = new AtomStringNumeric(item->name().toAscii(),
                                                 def, AtomString::EBCDIC);
                    root->add(atom);
                    root = atom;
                } break;

                case EBCDIC_ALPHABETIC: {
                    atom = new AtomStringAlpha(item->name().toAscii(), def,
                                               AtomString::EBCDIC);
                    root->add(atom);
                    root = atom;
                } break;

                case BCD: {
                    atom = new AtomNumericBCD(item->name(), def);
                    root->add(atom);
                    root = atom;
                } break;

                case BCD_PACKED: {
                    atom = new AtomNumericPacked(item->name(), def);
                    root->add(atom);
                    root = atom;
                } break;

                case HEX_PACKED: {
                    atom = new AtomHexPacked(item->name(), def);
                    root->add(atom);
                    root = atom;
                } break;
                    
                case UBITMAP:
                case BITMAP: {
                    atom = new AtomBitmap(item->name(), def);
                    root->add(atom);
                    root = atom;
                } break;

                case INT8: {
                    atom = new AtomBinary(item->name(), def, AtomBinary::INT8);
                    root->add(atom);
                    root = atom;
                } break;

                case UINT8: {
                    atom = new AtomBinary(item->name(), def, AtomBinary::UINT8);
                    root->add(atom);
                    root = atom;
                } break;

                case INT16: {
                    atom = new AtomBinary(item->name(), def, AtomBinary::INT16);
                    root->add(atom);
                    root = atom;
                } break;

                case UINT16: {
                    atom = new AtomBinary(item->name(), def, AtomBinary::UINT16);
                    root->add(atom);
                    root = atom;
                } break;

                case INT32: {
                    atom = new AtomBinary(item->name(), def, AtomBinary::INT32);
                    root->add(atom);
                    root = atom;
                } break;

                case UINT32: {
                    atom = new AtomBinary(item->name(), def, AtomBinary::UINT32);
                    root->add(atom);
                    root = atom;
                } break;

                case INT64: {
                    atom = new AtomBinary(item->name(), def, AtomBinary::INT64);
                    root->add(atom);
                    root = atom;
                } break;

                case UINT64: {
                    atom = new AtomBinary(item->name(), def,  AtomBinary::UINT64);
                    root->add(atom);
                    root = atom;
                } break;

                case FLOAT: {
                    atom = new AtomBinary(item->name(), def,  AtomBinary::FLOAT);
                    root->add(atom);
                    root = atom;
                } break;

                case DOUBLE: {
                    atom = new AtomBinary(item->name(), def,  AtomBinary::DOUBLE);
                    root->add(atom);
                    root = atom;
                } break;
       
                default: qFatal("fatal error. codemodel.cpp"); break;
                }

            //
            //  Add the rest from definition
            //
            if (atom) {
                if (def.default_.isValid())
                    atom->setDefault(def.default_);
                if (def.lpadc_ != 0)
                    atom->setLpad(def.lpadc_);
                if (def.rpadc_ != 0)
                    atom->setRpad(def.rpadc_);
                if (!item->constraints().empty())
                    atom->setConstraintList(item->constraints());

                //
                //  Find the dependant
                //
                if (def.length_type_ == VARIABLE_LENGTH ||
                    def.length_type_ == VARIABLE_LENGTH_INCLUSIVE ||
                    def.length_type_ == VARIABLE_LENGTH_ALLINCLUSIVE) {

                    bool found = false;
                    BoxComponent *f;
                    if ( (f=BoxComponent::findComponentByName(def.vlength_dependant_,
                                                              root)))
                        found = true;

                    if (found) {
                        if (f->isAtom()) {
                            //
                            //  Set the dependant
                            //
                            if (!((Atom*)f)->isNumeric()) {
                                qFatal(QString("VLENGTH dependant:"+
                                               f->name()+" not numeric").toLatin1().data());

                            }
                            atom->setvDependent((Atom*)f);
                            ((Atom*)f)->setvDependee(atom);
                        }
                    }

                    if (!found) {
                        qFatal(QString("Unable to find the dependant:"+
                                       def.vlength_dependant_+" in VLENGTH DEPENDING ON clause").toLatin1().data());
                    }
                } 
            }
        }
        
    } else { //THIS IS A BOX
        if (!top_lvl) {
            Box *box = 0;
            box = new Box(item->name());
            box->setChoice(item->isChoice());
            root->add(box);
            root = box;
        }
    } //done with this box

    root->setKey(item->isKey());
    root->setDisplayString(item->definition().display_);

    if (item->hasChilds()) {
        ContainerItem *child;
        int idx = 0;
        while ( idx<item->childs().size() ) {
            child = item->childs().at(idx);
            dumpAtomBoxModel(child, root);
            idx++;
        }
    }

    
    // Any redefines?
    if (item->hasRedefines()) {
        QList <ContainerItem*> redefines = item->redefines();
        ContainerItem *r;
        foreach(r, redefines) {
            BoxComponent *ct = new Box("__temp__");
            ct->setParent(root);
            BoxComponent *br = dumpAtomBoxModel(r, ct);
            delete ct;
            br->setRedefine(true);
            root->addRedefine(br);
        }
    }

    if (item->hasOccursNumber()) {
        root->setOccurencesNumber(item->occursNumber());
        //root->buildOccurences();
    }

    if (item->hasOccursDepend()) {
        processRedefines();

        bool found = false;
        if (BoxComponent::findComponentByName(item->occursDepend(), root) != NULL)
            found = true;

        if (!found) {
            qFatal(QString("Unable to find the dependant:"+
                           item->occursDepend()+" in OCCURS DEPENDING ON clause").toLatin1().data());
        } else {
            root->setOccurencesNumber(item->occursDepend(),
                                      item->occursDependFrom(),
                                      item->occursDependTo());
            //root->buildOccurences();
        }
    }
    

    return root;
}

QList<BoxComponent*>
DDLCodeModel::dumpAtomBoxModel()
{
    QHash<QString, GroupDefinitionItem*>::Iterator it = group_definitions_.begin();
    QList<BoxComponent*> model_;

    while (it != group_definitions_.end()) {
        //we don't want to dump twice
        if (!dumped_definitions_.contains(it.key())) {
            ContainerItem *item = it.value();
            model_.push_back( dumpAtomBoxModel(item) );
            dumped_definitions_.insert(it.key(), it.value());
        }
        ++it;
    }

    return model_;
}

QList< QPair <QString, QVariant > >      
DDLCodeModel::dumpConstants() 
{
    QHash<QString, ConstantItem*>::Iterator it =  constants_.begin();
    QList< QPair <QString, QVariant > > constants;
    while(it != constants_.end()) {
        ConstantItem *item = it.value();
        QPair <QString, QVariant> c(item->name().replace("-","_").replace("^","_").toLower(), item->value());
        constants.push_back(c);
        ++it;
    }
    return constants;
}

void
DDLCodeModel::processRedefines( ContainerItem *item )
{
    if (item->hasRedefines()) {
        QList <ContainerItem*> redefines = item->redefines();
        ContainerItem *redefine;
        foreach(redefine, redefines) {
            ContainerItem *to_move = item->parent()->findChild(redefine->name());
            if (to_move) {
                redefine = to_move;
                item->parent()->removeChild(redefine->name());
            }
        }
    }

    if (item->hasChilds()) {
        ContainerItem *child;
        foreach (child, item->childs()) {
            processRedefines(child);
        }
    }
}

void
DDLCodeModel::processRedefines()
{
    QHash<QString, GroupDefinitionItem*>::Iterator it = group_definitions_.begin();
    while (it != group_definitions_.end()) {
        processRedefines(it.value());
        ++it;
    }
}

/*****************************************************************/


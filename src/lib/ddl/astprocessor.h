#ifndef DDL_PROCESS_AST
#define DDL_PROCESS_AST

#include <QtCore/QUrl>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QByteArray>
#include <QtCore/QTextStream>
#include <QtCore/QSet>


#include "astvisitor.h"
#include "codemodel.h"
#include "definition.h"

/*
 *  Helper class to visit/process the AST tree
 */
class ProcessAST : protected AST::Visitor
{
  public:
    enum Charset {
        A, /*ASCII*/
        E  /*EBCDIC*/
    };
    
    ProcessAST(DDLASTParser *parser, DDLCodeModel *model);
    virtual ~ProcessAST();

    void operator()(const QString &code, AST::Node *node);

    void accept(AST::Node *node);

    virtual bool visit(AST::LineItem *gs);
    virtual bool visit(AST::FieldDefStatement *fs);
    virtual bool visit(AST::EnumDefStatement *es);
    virtual bool visit(AST::GroupDefStatement *gs);
    virtual bool visit(AST::RecordDefStatement *rd);
    virtual bool visit(AST::ConstStatement *cs);
    virtual bool visit(AST::LineItemKeyIsClause *ki);

    inline DDLCodeModel *model() const { return model_; }
    inline QList<DDLError> errors () const {return errors_; }

  private:
    /*
     *  Helper classes invoked by visit(AST::LineItem *gs) to add a line item
     *  to the *model
     */
    bool addPICLineItemToModel(short level, AST::Literal *name, LineDefinitionItem *li,
       QList<AST::LineItemClause *> clauses);
                               
    /* Ex: 02  PAN REDEFINES DATA. */
    bool addLineItemToModel(short level, AST::Literal *name,
                               QList<AST::LineItemClause *>);
    /* Ex: 02 PROC-CDE PIC X(2) OCCURS 3 TIMES. */
    bool addPICLineItemToModel(short level, AST::Literal *name, QString pic_string,
                               QList<AST::LineItemClause *>);
    /* Ex: 02 FLD PIC 9999. */
    bool addPICStringLineItemToModel(short level, AST::Literal *name,
                                     QString pic_string, QList<AST::LineItemClause *>);
    /* Ex: 02 FLD TYPE BINARY 16. */
    bool addPICBinaryLineItemToModel(short level, AST::Literal *name,
                                     AST::PicBinaryClause *cc, QList<AST::LineItemClause *>);

    /* Ex: 02 FLD TYPE BIT 4. */
    bool addPICBitLineItemToModel(short level, AST::Literal *name,
                                  AST::PicBitClause *cc, QList<AST::LineItemClause *>);

    /* EX: 02 PAN TYPE *. */
    bool addTypeLineItemToModel(short level, AST::Literal *name, AST::Literal *type_name,
                                QList<AST::LineItemClause *>);

    /* Helper class to process the line item clause list */
    bool processLineItemClauses(LineDefinitionItem *li, AST::Literal *name/*for reporting error locations*/, 
                                QList<AST::LineItemClause *> clauses, short level = 1);

    /* Helper class to process the line item constraint list */
    void addConstraintToLineItem(LineDefinitionItem *li, AST::Constraint *c);

    bool isReserved(QString val);

    DDLASTParser        *parser_;
    QString              contents_;
    QList<DDLError>      errors_;
    DDLCodeModel        *model_;

    short                saved_level_;
    ContainerItem        *saved_item_;
    QList<int>           levels_; //list of all levels to know how many levels to go back when adding a lineitem.
    
    Charset              current_charset_;
    ByteOrder            current_byteorder_;
    QStringList          reservedKeywords_;
};

#endif //DDL_PROCESS_AST

%option case-insensitive noyywrap

%{
#include "generatedparser.h"
#include <cstdlib>

#include <QtEndian>
#include <QDebug>
#include <QString>
#include <QList>

#define YY_DECL int DDLParser::yy_lex()

QList<char *> yy_allocations;

int column = 1;
int line = 1;
static QString yy_prev;

AST::Location yy_loc() 
{
    AST::Location loc;

    loc.start_line = line; 
    loc.start_column = column; 
    loc.end_line = line; 
    loc.end_column = column + yyleng - 2; 

    return loc;
}

#define UPDATE_POS_AND_VALUE() do { \
    if (!QString(yytext).trimmed().isEmpty()) \
           yy_prev = QString(yytext);  \
    for (int i = 0; yytext[i] != '\0'; i++) \
       if (yytext[i] == '\n'){ \
           column = 0; \
           line++; }\
       else if (yytext[i] == '\t') \
           column += 4 - (column % 4); \
       else \
           column++; \
\
   yy_allocations.push_back(strdup(yytext)); \
   sym(1).val = yy_allocations.last(); \
} while(0)

%}

LETTER              [A-Za-z]
DIGIT               [0-9]


/* NUMBERS */
NUM_DEC                 [+-]{0,1}{DIGIT}*[.]{0,1}{DIGIT}+[+-]{0,1}
NUM_OCT                 [+-]{0,1}%0{DIGIT}+
NUM_HEX                 [+-]{0,1}%H[0-9A-Ha-h]+
NUM_BIN                 [+-]{0,1}%B[0-1]+
NUMBER                  {NUM_DEC}|{NUM_OCT}|{NUM_HEX}|{NUM_BIN}


/* STRINGS */
STRING_QUOTE        \'([^\']*(\'\')*[^\']*)*\'
STRING_DBLQUOTE     \"([^\"]*(\"\")*[^\"]*)*\"
STRING              {STRING_QUOTE}|{STRING_DBLQUOTE}

/* LITERALS */
LITERAL             [A-Za-z\-_0-9]+

/* NATIONAL LITERALS */
NAT_LIT             (n|N)(\"|\')[^\"]{2}(\"|\')

/* PICTURE STRING */
PIC_ALPHA_STRING        [A|X|9|N]+[ ]*([ ]*\([ ]*{NUMBER}[ ]*\)[ ]*){0,1}[ ]*
PIC_NUMERIC_STRING      [ST]{0,1}[9]+[ST]{0,1}[ ]*([ ]*\({NUMBER}[ ]*\)){0,1}[ ]*([V][9]+([ ]*\({NUMBER}[ ]*\)[ ]){0,1}){0,1}[ ]*[ST]{0,1}

PIC_STRING              ({PIC_ALPHA_STRING})+|({PIC_NUMERIC_STRING})+


/* DDL KEYWORDS */
KEYWORD              ALL|ALLOWED|ARE|AS|ASCENDING|ASSIGNED|AUDIT|AUDITCOMPRESS|BE|BEGIN|BINARY|BIT|BLOCK|BUFFERED|BUFFERSIZE|BY|CFIELDALIGN_MATCHED2|CHARACTER|C_MATCH_HISTORIC_TAL|CODE|COMP|COMP-3|COMPLEX|COMPRESS|COMPUTATIONAL|COMPUTATIONAL-3|CONSTANT|CRTPID|CURRENT|DATE|DATETIME|DAY|DCOMPRESS|DEF|DEFINITION|DELETE|DEPENDING|DESCENDING|DEVICE|DISPLAY|DUPLICATES|EDIT-PIC|END|ENTRY-SEQUENCED|ENUM|EXIT|EXT|EXTERNAL|FILE|FIELDALIGN_SHARED8|FILLER|FLOAT|FNAME32|FOR|FRACTION|HEADING|HELP|HIGH-NUMBER|HIGH-VALUE|HOUR|ICOMPRESS|INDEX|INDEXED|INTERVAL|IS|JUST|JUSTIFIED|KEY|KEY-SEQUENCED|KEYTAG|LOGICAL|LOW-NUMBER|LOW-VALUE|LOW-VALUES|MAXEXTENTS|MINUTE|MONTH|MUST|NO|NOT|NOVALUE|NOVERSION|NULL|OCCURS|ODDUNSTR|OF|ON|OUTPUT|PACKED|PACKED-DECIMAL|PHANDLE|PIC|PICTURE|QUOTE|QUOTES|RECORD|REDEFINES|REFRESH|RELATIVE|RENAMES|RIGHT|SECOND|SEQ|SEQUENCE|SERIALWRITES|SETLOCALENAME|SHOW|SPACE|SPACES|SPI-NULL|SQL|SQLNULL|SQL-NULLABLE|SSID|SUBVOL|SYSTEM|TACL|TALUNDERSCORE|TEMPORARY|THROUGH|THRU|TIME|TIMES|TIMESTAMP|TO|TOKEN-CODE|TOKEN-MAP|TOKEN-TYPE|TRANSID|TSTAMP|TYPE|UNSIGNED|UNSTRUCTURED|UPDATE|UPSHIFT|USAGE|USE|USERNAME|VALUE|VARCHAR|VARYING|VERIFIEDWRITESVERSION|YEAR|ZERO|ZEROES|ZEROS 

CREATION_KEYWORD    KEY-SEQUENCED|RELATIVE|ENTRY-SEQUENCED|UNSTRUCTURED|AUDIT|AUDITCOMPRESS|BLOCK|BUFFERED|BUFFERSIZE|CODE|COMPRESS|DCOMPRESS|ICOMPRESS|EXT|MAXEXTENTS|ODDUNSTR|REFRESS|SERIALWRITES|VERIFYWRITES

KEY_ASGN_KEYWORD    DUPLICATES|ALLOWED|UPDATE|SEQUENCE|ASCENDING|DESCENDING

%%


^\*.*$           { /*qDebug()<<"line1:"<<yytext;*/ UPDATE_POS_AND_VALUE(); }
\![^\!\n]*.*$    { /*qDebug()<<"line2:"<<yytext;*/ UPDATE_POS_AND_VALUE(); }
\![^\!\n]+.*\!   { /*qDebug()<<"line3:"<<yytext;*/ UPDATE_POS_AND_VALUE(); }
\?.*$            { /*qDebug()<<"line4:"<<yytext;*/ UPDATE_POS_AND_VALUE(); }
--.*$            { /*qDebug()<<"line5:"<<yytext;*/ UPDATE_POS_AND_VALUE(); }
[ \t\n]+         { /*qDebug()<<"line6:"<<yytext;*/ UPDATE_POS_AND_VALUE(); }

%
{PIC_STRING}      {  
                  if ((yy_prev != QString("PIC")) && (yy_prev != QString("pic"))) {
		          REJECT;
		          }
		          else {
		          UPDATE_POS_AND_VALUE();
		          return T_PIC_STRING;
                  }
                  }

{STRING}          {
                  UPDATE_POS_AND_VALUE();
                  return T_STRING;
                  }

{NAT_LIT}         {}

89                { UPDATE_POS_AND_VALUE(); QString syytext(yytext);sym(1).dval = syytext.toLongLong(); return T_NUMBER_89; }

88                { UPDATE_POS_AND_VALUE(); QString syytext(yytext);sym(1).dval = syytext.toLongLong(); return T_NUMBER_88; }


{NUMBER} {
                  UPDATE_POS_AND_VALUE();
                  bool ok = false;

		  QString syytext(yytext);
		  int pos = 0;
		  if ( (pos = syytext.indexOf("%B",0,Qt::CaseInsensitive))!=-1) {
		      sym(1).dval = syytext.mid(pos+2).toLongLong(&ok,2);
		      if (!ok) 
		          sym(1).dval = syytext.mid(pos+2).toULongLong(&ok, 2);
		      if (!ok) {
   		           REJECT;
		      }
                  } else if ( (pos = syytext.indexOf("%0",0,Qt::CaseInsensitive))!=-1) {
		      sym(1).dval = syytext.mid(pos+2).toLongLong(&ok,8);
		      if (!ok) 
		          sym(1).dval = syytext.mid(pos+2).toULongLong(&ok, 8);
		      if (!ok) {
		          REJECT;
		      }

                  } else if ( (pos = syytext.indexOf("%H",0,Qt::CaseInsensitive))!=-1) {
		      sym(1).dval = syytext.mid(pos+2).toLongLong(&ok,16);
		      if (!ok) 
		          sym(1).dval = syytext.mid(pos+2).toULongLong(&ok, 16);
                      assert(ok);
                  } else {
		      sym(1).dval = syytext.toLongLong(&ok);
		      if (!ok) 
		          sym(1).dval = syytext.toULongLong(&ok);
		      if (!ok) {
		          REJECT;
		      }
		  }
		  return T_NUMBER;
                  }

(BEGIN)           { UPDATE_POS_AND_VALUE(); return  T_BEGIN; }
(END)             { UPDATE_POS_AND_VALUE();
                    foreach(char *c, yy_allocations) {
                    if (c)
                       free(c);
                    }
                    yy_allocations.clear();
                    return  T_END; }

\(                { UPDATE_POS_AND_VALUE(); return  T_LPAREN; }
\)                { UPDATE_POS_AND_VALUE(); return  T_RPAREN; }
\.                { UPDATE_POS_AND_VALUE(); 
                    // yy_allocations.clear();
                    return  T_DOT; }
\*                { UPDATE_POS_AND_VALUE(); return  T_STAR; }
\,                { UPDATE_POS_AND_VALUE(); return  T_COMMA; }


ARE               { UPDATE_POS_AND_VALUE(); return  T_ARE; }
CONSTANT          { UPDATE_POS_AND_VALUE(); return  T_CONSTANT; }
IS                { UPDATE_POS_AND_VALUE(); return  T_IS; }
TYPE              { UPDATE_POS_AND_VALUE(); return  T_TYPE; }
UNSIGNED          { UPDATE_POS_AND_VALUE(); return  T_UNSIGNED; }

BINARY            { UPDATE_POS_AND_VALUE(); return  T_BINARY; }
FLOAT             { UPDATE_POS_AND_VALUE(); return  T_FLOAT; }
COMPLEX           { UPDATE_POS_AND_VALUE(); return  T_COMPLEX; }
LOGICAL           { UPDATE_POS_AND_VALUE(); return  T_LOGICAL; }
ENUM              { UPDATE_POS_AND_VALUE(); return  T_ENUM; }
BIT               { UPDATE_POS_AND_VALUE(); return  T_BIT; }

AS               { UPDATE_POS_AND_VALUE(); return  T_AS; }
Assigned         { UPDATE_POS_AND_VALUE(); return  T_ASSIGNED; }
BE               { UPDATE_POS_AND_VALUE(); return  T_BE;}
BY               { UPDATE_POS_AND_VALUE(); return  T_BY;}
CHARACTER        { UPDATE_POS_AND_VALUE(); return  T_CHARACTER; }
{CREATION_KEYWORD} { 
                   UPDATE_POS_AND_VALUE();
                   return T_CREATION_KEYWORD; }

(DEF|DEFINITION) { UPDATE_POS_AND_VALUE(); return  T_DEFINITION; }
DEPENDING        { UPDATE_POS_AND_VALUE(); return  T_DEPENDING; }
DISPLAY          { UPDATE_POS_AND_VALUE(); return  T_DISPLAY; }
DUPLICATES       { UPDATE_POS_AND_VALUE(); return  T_DUPLICATES; }
EDIT-PIC         { UPDATE_POS_AND_VALUE(); return  T_EDIT_PIC; }
EXTERNAL         { UPDATE_POS_AND_VALUE(); return  T_EXTERNAL; }
FILE             { UPDATE_POS_AND_VALUE(); return  T_FILE; }
FILLER           { UPDATE_POS_AND_VALUE(); return  T_FILLER; }
HEADING          { UPDATE_POS_AND_VALUE(); return  T_HEADING; }
HELP             { UPDATE_POS_AND_VALUE(); return  T_HELP; }
(COMP|COMPUTATIONAL) { UPDATE_POS_AND_VALUE();return T_COMP; }
INDEX            { UPDATE_POS_AND_VALUE(); return  T_INDEX; }
INDEXED          { UPDATE_POS_AND_VALUE(); return  T_INDEXED; }
(JUST|JUSTIFIED) { UPDATE_POS_AND_VALUE(); return  T_JUSTIFIED; }
KEY              { UPDATE_POS_AND_VALUE(); return  T_KEY; }
{KEY_ASGN_KEYWORD} {
                   UPDATE_POS_AND_VALUE();
                   return T_KEY_ASGN_KEYWORD; }
KEYTAG           { UPDATE_POS_AND_VALUE(); return  T_KEYTAG;}
(COMP-3|COMPUTATIONAL-3|PACKED|PACKED-DECIMAL)   { UPDATE_POS_AND_VALUE(); return  T_PACKED_DECIMAL; }
NO               { UPDATE_POS_AND_VALUE(); return  T_NO;}
NOT              { UPDATE_POS_AND_VALUE(); return  T_NOT; }
VALUE            { UPDATE_POS_AND_VALUE(); return  T_VALUE; }
NOVALUE          { UPDATE_POS_AND_VALUE(); return  T_NOVALUE;}
NULL             { UPDATE_POS_AND_VALUE(); return  T_NULL; }
MUST             { UPDATE_POS_AND_VALUE(); return  T_MUST; }
OCCURS           { UPDATE_POS_AND_VALUE(); return  T_OCCURS; }
ON               { UPDATE_POS_AND_VALUE(); return  T_ON; }
(PIC|PICTURE)    { UPDATE_POS_AND_VALUE(); return  T_PICTURE; }
RECORD           { UPDATE_POS_AND_VALUE(); return  T_RECORD; }
REDEFINES        { UPDATE_POS_AND_VALUE(); return  T_REDEFINES; }
RIGHT            { UPDATE_POS_AND_VALUE(); return  T_RIGHT; }
TOKEN\-CODE      { UPDATE_POS_AND_VALUE(); return  T_TOKEN_CODE; }
TOKEN\-TYPE      { UPDATE_POS_AND_VALUE(); return  T_TOKEN_TYPE; }
SPI-NULL         { UPDATE_POS_AND_VALUE(); return  T_SPI_NULL; }
SSID             { UPDATE_POS_AND_VALUE(); return  T_SSID; }
TEMPORARY        { UPDATE_POS_AND_VALUE(); return  T_TEMPORARY; }
(THROUGH|THRU)   { UPDATE_POS_AND_VALUE(); return  T_THROUGH; }
TIMES            { UPDATE_POS_AND_VALUE(); return  T_TIMES; }
TO               { UPDATE_POS_AND_VALUE(); return  T_TO; }
SQL_NULLABLE     { UPDATE_POS_AND_VALUE(); return  T_SQL_NULLABLE; }
USAGE            { UPDATE_POS_AND_VALUE(); return  T_USAGE; }
UPSHIFT          { UPDATE_POS_AND_VALUE(); return  T_UPSHIFT; }

(VALUE|VALUES)   { UPDATE_POS_AND_VALUE(); return  T_DEFAULT; }

VLENGTH          { UPDATE_POS_AND_VALUE(); return  T_VLENGTH;            /*ddl enhancement*/ }
EXCLUSIVE        { UPDATE_POS_AND_VALUE(); return  T_EXCLUSIVE;          /*ddl enhancement*/ }
INCLUSIVE        { UPDATE_POS_AND_VALUE(); return  T_INCLUSIVE;          /*ddl enhancement*/ }
ALLINCLUSIVE     { UPDATE_POS_AND_VALUE(); return  T_ALLINCLUSIVE;       /*ddl enhancement*/ }
DELIMITER        { UPDATE_POS_AND_VALUE(); return  T_DELIMITER;          /*ddl_enhancement*/ }
DEFAULT          { UPDATE_POS_AND_VALUE(); return  T_DEFAULT;            /*ddl enhancement*/ }
LEFT             { UPDATE_POS_AND_VALUE(); return  T_LEFT;               /*ddl enhancement*/ }
LPAD             { UPDATE_POS_AND_VALUE(); return  T_LPAD;               /*ddl enhancement*/ }
RPAD             { UPDATE_POS_AND_VALUE(); return  T_RPAD;               /*ddl enhancement*/ }
ISOBITMAP        { UPDATE_POS_AND_VALUE(); return  T_ISOBITMAP;          /*ddl enhancement*/ }
CHARSET          { UPDATE_POS_AND_VALUE(); return  T_CHARSET;            /*ddl enhancement*/ }
ASCII            { UPDATE_POS_AND_VALUE(); return  T_ASCII;              /*ddl enhancement*/ }
EBCDIC           { UPDATE_POS_AND_VALUE(); return  T_EBCDIC;             /*ddl enhancement*/ }
HIGH_LOW         { UPDATE_POS_AND_VALUE(); return  T_HIGH_LOW;           /*ddl enhancement*/ }
LOW_HIGH         { UPDATE_POS_AND_VALUE(); return  T_LOW_HIGH;           /*ddl enhancement*/ }
BCD              { UPDATE_POS_AND_VALUE(); return  T_PACKED_DECIMAL;     /*ddl enhancement*/ } 
REGEX            { UPDATE_POS_AND_VALUE(); return  T_REGEX;              /*ddl enhancement*/ }
CHOICE           { UPDATE_POS_AND_VALUE(); return  T_CHOICE;             /*ddl enhancement*/ }
EMPTY            { UPDATE_POS_AND_VALUE(); return  T_EMPTY;              /*ddl enhancement*/ }

[/]              { UPDATE_POS_AND_VALUE(); return  T_DASH; }

{LITERAL}         {

                  UPDATE_POS_AND_VALUE();
                  return T_LITERAL; 
                  }

.                {UPDATE_POS_AND_VALUE();}
%%

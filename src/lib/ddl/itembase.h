#ifndef CODEMODELITEM_H
#define CODEMODELITEM_H

#include <QtCore/QString>

class DDLCodeModel;

class CodeModelItemBase
{
public:
    enum Kind
    {
        kindUndefined = 0,
        kindConstant = 1,
        kindEnumDefinition,
        kindFieldDefinition,
        kindGroupDefinition,
        kindLine,
    };

    CodeModelItemBase(int kind);

    virtual ~CodeModelItemBase();

    int kind() const;
    QString name() const;
    void setKind(int kind);
    void setName(const QString &name);
    
    virtual CodeModelItemBase* clone() = 0;

private:
    int kind_;
    QString name_;

private:
    CodeModelItemBase(const CodeModelItemBase &other);
    void operator= (const CodeModelItemBase &other);
};

#endif

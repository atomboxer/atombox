#ifndef CONSTANTITEM_H
#define CONSTANTITEM_H

#include <QtCore/QVariant>

#include "itembase.h"

class ConstantItem : public CodeModelItemBase
{
public:    
    ConstantItem(int kind = kindConstant);

    bool isNumber() const;
    bool isString() const;
    void setNumber();
    void setString();
    void setValue(QVariant value);
    QVariant value() const;

    virtual CodeModelItemBase* clone();
private:
    bool is_number_;
    bool is_string_;
    QVariant value_;
};

#endif

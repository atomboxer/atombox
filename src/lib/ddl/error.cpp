#include "error.h"

#include <QtCore/QFile>
#include <QtCore/QByteArray>
#include <QtCore/QStringList>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

DDLError::DDLError()
    : url_(),
      line_(-1),
      column_(-1), 
      description_()
    
{
}

DDLError::DDLError(QUrl url, int line, int column, QString description)
    : url_(url),
      line_(line),
      column_(column), 
      description_(description)
    
{
}

DDLError::DDLError(int line, int column, QString description)
    : url_(QUrl("")),
      line_(line),
      column_(column), 
      description_(description)
    
{
}

DDLError::~DDLError()
{
}

QString
DDLError::description() const
{
    return description_;
}

void
DDLError::setDescription(const QString &description)
{
    description_ = description;
}

QUrl
DDLError::url() const
{
    return url_;
}

void
DDLError::setUrl(const QUrl &url)
{
    url_ = url;
}

int
DDLError::line() const
{
    return line_;
}

void
DDLError::setLine(int line)
{
    line_ = line;
} 

int
DDLError::column() const
{
    return column_;
}

void
DDLError::setColumn(int column)
{
    column_ = column;
}

QString
DDLError::toString() const
{
    QString rv;
    
    rv = url().toString() + QLatin1Char(':') + QString::number(line());
    
    if (column() != -1) {
rv += QLatin1Char(':') + QString::number(column());
    }
    rv += QLatin1String(": ") + description();

    return rv;
}

QDebug operator<<(QDebug debug, const DDLError &error)
{
    debug << qPrintable(error.toString());

    QUrl url = error.url();

    if (error.line() > 0 && url.scheme() == QLatin1String("file")) {
        QString file = url.toLocalFile();
        QFile f(file);
        if (f.open(QIODevice::ReadOnly|QIODevice::Text)) {
            QByteArray data = f.readAll();
            QTextStream stream(data, QIODevice::ReadOnly|QIODevice::Text);
            stream.setCodec("UTF-8");
            const QString code = stream.readAll();
            const QStringList lines = code.split(QLatin1Char('\n'));

            if (lines.count() >= error.line()) {
                const QString &line = lines.at(error.line() - 1);
                debug << "\n    " << qPrintable(line);

                if(error.column() > 0) {
                    int column = qMax(0, error.column() - 1);
                    column = qMin(column, line.length()); 

                    QByteArray ind;
                    ind.reserve(column);
                    for (int i = 0; i < column; ++i) {
                        const QChar ch = line.at(i);
                        if (ch.isSpace())
                            ind.append(ch.unicode());
                        else
                            ind.append(' ');
                    }
                    ind.append('^');
                    debug << "\n    " << ind.constData();
                }
            }
        }
    }
    return debug;
}

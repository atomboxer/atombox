#ifndef DDL_ERROR_H
#define DDL_ERROR_H

#include <QtCore/QDebug>
#include <QtCore/QUrl>
#include <QtCore/QString>
#include <QtCore/QDebug>


class DDLError
{
public:
    DDLError ();
    DDLError(QUrl url, int line, int column, QString description);
    DDLError(int line, int column, QString description);
    
    virtual ~DDLError();

    QString description() const;
    void setDescription( const QString &);

    QUrl url() const;
    void setUrl( const QUrl &);

    int line() const;
    void setLine(int);

    int column() const;
    void setColumn(int);

    QString toString() const;

private:
    QUrl url_;
    int line_;
    int column_;
    QString description_;
};

QDebug operator<<(QDebug debug, const DDLError &error);

#endif // DDL_ERROR

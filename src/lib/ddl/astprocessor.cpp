#include <QtCore/QDebug>
#include <QtCore/QStringList>

#include "astparser.h"
#include "astprocessor.h"
#include "codemodel.h"
#include "error.h"

#include "constraintrange.h"
#include "constraintvalue.h"
#include "definition.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

class PICString
{
public:
    PICString(QString str) {
        str = str.toUpper().remove(" ");

        definition_.length_type_ = FIXED;
        definition_.data_type_ = ASCII_NUMERIC;
        definition_.regex_ = QRegExp("");

        str.replace("T","S");
        bool have_s = (str.indexOf("S") != -1)?true:false;
        bool have_v = (str.indexOf("V") != -1)?true:false;

        if (str.indexOf("X") != -1 || str.indexOf("A") != -1) {
            definition_.data_type_ = ASCII;
        }

        if (str.indexOf("X") == -1 && str.indexOf("9") == -1 && 
            str.indexOf("A") != -1) {
            definition_.data_type_ = ASCII_ALPHABETIC;
        }


        if (have_s) {
            if (str.indexOf("S") != 0) 
                definition_.numeric_sign_type_ = EXPLICIT_RIGHT;
            else
                definition_.numeric_sign_type_ = EXPLICIT_LEFT;

            str.remove("S");
        }

        QRegExp rx("\\(\(\\d+)\\)");
        int  pos = 0;
        int  nr_cap = 0;
        int  lgth_cap = 0;
        bool ok;

        while ( (pos = rx.indexIn(str,pos)) != -1 ) {
            nr_cap++;
            lgth_cap += rx.cap(1).toInt(&ok,10);
            pos += rx.matchedLength();
        }

        definition_.length_ = QString(str).remove(rx).length() -
            nr_cap + lgth_cap;

        if (have_v) {
            int pos_v = str.indexOf("V");
            QString str_before_v = str.left(pos_v);
            while ( (pos = rx.indexIn(str_before_v,pos_v)) != -1 ) {
                nr_cap++;
                lgth_cap += rx.cap(1).toInt(&ok,10);
                pos += rx.matchedLength();
            }
            definition_.numeric_decimal_point_ = QString(str_before_v).remove(rx).length() - nr_cap +lgth_cap;
            if (have_s) 
                definition_.numeric_decimal_point_++;
        }


        // regex
        // pos = 0;
        // rx.setPattern("\(\\(\\d+\\))");
        // while ( (pos = rx.indexIn(str,pos)) != -1 ) {
        //     str = str.replace(rx.cap(1),rx.cap(1).replace(QRegExp("[9]"),"Z"));
        //     pos += rx.matchedLength();
        // }

        // str = str.replace(QRegExp("\\("),"{");
        // str = str.replace(QRegExp("\\)"),"}");
        // str = str.replace(QRegExp("[XA]"),".");
        
        // str = str.replace(QRegExp("[9]"),"\\d");
        // str = str.replace(QRegExp("V"),"\\.");

        // switch(definition_.numeric_sign_type_) {
        // case (EXPLICIT_RIGHT): str = str.append("[+-]{1}");break;
        // case (EXPLICIT_LEFT):  str = str.prepend("[+-]{1}");break;
        // }
        
        // definition_.regex_ = QRegExp(str);
        // Q_ASSERT(isValid());

        if (have_s)
            definition_.length_++;
    }

    Definition getDefinition() { return definition_; }
    bool isValid() { return (definition_.isDefined())?true:false;}
private:
    Definition definition_;
};

ProcessAST::ProcessAST(DDLASTParser *parser, DDLCodeModel *model)
    : parser_(parser),
      contents_(),
      errors_(),
      model_(model)
{
    saved_item_ = NULL;
    saved_level_ = 2;
    current_charset_ = A;

   
    reservedKeywords_ <<"name"<<"length"<<"size"<<"offset"<<"bytes"<<"value";

    reservedKeywords_ <<"break"<<"export"<<"return"<<"case"<<"for"<<"switchcomment"<<"function"<<"thiscontinue"<<"if"<<"typeof"<<"default"<<"import"<<"var"<<"delete"<<"in"<<"void"<<"do";
    reservedKeywords_<<"label"<<"while"<<"else"<<"new"<<"with"<<"abstract"<<"implements"<<"protected"<<"boolean";
    reservedKeywords_<<"instanceOf"<<"public"<<"byte"<<"int"<<"short"<<"char"/*<<"interface"*/<<"static"<<"double"<<"long"<<"synchronized"<<"false"<<"native"<<"throws"<<"final"<<"null"<<"transient"<<"float"<<"package"<<"true"<<"goto"<<"private"<<"catch"<<"enum"<<"throw"<<"class"<<"extends"<<"try"<<"const"<<"finally"<<"debugger"<<"super";

}

ProcessAST::~ProcessAST()
{
}

void
ProcessAST::operator()(const QString &code, AST::Node *node)
{
    contents_ = code;
    accept(node);
}

void
ProcessAST::accept(AST::Node *node)
{
    AST::Node::accept(node, this);
}

bool
ProcessAST::addPICLineItemToModel(short level, AST::Literal *name,
                                  LineDefinitionItem *li,
                                  QList<AST::LineItemClause *> clauses)
{
    if (levels_.indexOf(level) == -1)
        levels_.push_back(level);
    qSort(levels_.begin(), levels_.end());

    if ( saved_level_ < level) {
        saved_item_->add(li);
    }
    else if (saved_level_ == level) {
        if ( saved_item_->parent() )
            saved_item_->parent()->add(li);
    }
    else { //saved_level > level
        int levels_up = levels_.length() - levels_.indexOf(level);
        for (int i=0; i<levels_up; i++) {
            if (saved_item_)
                saved_item_ = saved_item_->parent();
        }

        if (saved_item_)
            saved_item_->add(li);

        for (int i=0; i<levels_.length()-levels_.indexOf(level);i++)
            levels_.pop_back();
    }

    saved_item_ = li;
    saved_level_ = level;

    return true;
}

/* Ex: 02  PAN REDEFINES DATA. */
bool
ProcessAST::addLineItemToModel(short level, AST::Literal *name,
                               QList<AST::LineItemClause *> clauses)
{
    LineDefinitionItem *li = model()->create<LineDefinitionItem>();
    name->val_.replace("-","_");
    name->val_ = name->val_.toLower();
    li->setName(name->val_);

    //
    //  Process clauses
    //
    processLineItemClauses(li, name, clauses, level);

    return addPICLineItemToModel(level, name, li, clauses);
}

/* Ex: 02 PROC-CDE PIC X(2) OCCURS 3 TIMES. */
bool
ProcessAST::addPICLineItemToModel(short level, AST::Literal *name, QString pic_string,
                                  QList<AST::LineItemClause *> clauses)
{

    QString sval = name->val_;
    if (reservedKeywords_.contains(sval.replace("-","_")
                                   .replace("^","_").toLower())) {
        name->val_ = sval.replace("-","_")
            .replace("^","_").toLower()+"_";
    }

    LineDefinitionItem *li = model()->create<LineDefinitionItem>();
    PICString pic(pic_string);
    name->val_.replace("-","_");
    name->val_ = name->val_.toLower();
    li->setName(name->val_);
    Definition def = pic.getDefinition();
    
    

    if ( def.data_type_ == ASCII && current_charset_ == E) {
        def.data_type_ = (AtomDataType)EBCDIC;
    }

    if ( def.data_type_ == ASCII_NUMERIC && current_charset_ == E)
        def.data_type_ = (AtomDataType)EBCDIC_NUMERIC;

    if ( def.data_type_ == ASCII_ALPHABETIC && current_charset_ == E)
        def.data_type_ = (AtomDataType)EBCDIC_ALPHABETIC;

    li->define(def);

    //
    //  Process clauses
    //
    processLineItemClauses(li, name, clauses, level);

    return addPICLineItemToModel(level, name, li, clauses);
}

/* Ex: 02 FLD PIC 9999. */
bool
ProcessAST::addPICStringLineItemToModel(short level,
                                        AST::Literal *name,
                                        QString pic_string,
                                        QList<AST::LineItemClause *> clauses)
{
    return addPICLineItemToModel(level,name,pic_string,clauses);
}

/* Ex: 02 FLD TYPE BINARY 16. */
bool
ProcessAST::addPICBinaryLineItemToModel(short level, AST::Literal *lname,
                                        AST::PicBinaryClause *cc, 
                                        QList<AST::LineItemClause *> clauses)
{
    Q_ASSERT(cc != NULL);

    Definition def;

    def.length_type_ = FIXED;

    switch(cc->type_) {
    case(QMetaType::Char): def.data_type_ = INT8;def.length_ = 1;    break;
    case(QMetaType::UChar):def.data_type_ = UINT8;def.length_ = 1;   break;
    case(QMetaType::Short):def.data_type_  = INT16;def.length_ = 2;  break;
    case(QMetaType::UShort):def.data_type_ = UINT16;def.length_ = 2; break;
    case(QMetaType::Int):def.data_type_ = INT32;def.length_ = 4;     break;
    case(QMetaType::UInt):def.data_type_ = UINT32;def.length_ = 4;   break;
    case(QMetaType::LongLong):def.data_type_ = INT64;def.length_ = 8;break;
    case(QMetaType::ULongLong):def.data_type_ = UINT64;def.length_ = 8;break;
    case(QMetaType::Float):def.data_type_ = FLOAT;def.length_ = 4;     break;
    case(QMetaType::Double):def.data_type_ = DOUBLE;def.length_ = 8;   break;
    default: qFatal("fatal error, addPicBinaryLineItemToModel, unknown type");break;
    }
    
    LineDefinitionItem *li = model()->create<LineDefinitionItem>();
    QString name = lname->value().replace("-","_").toLower();
    li->setName(name);
    li->define(def);

    if (current_byteorder_ != DefaultByteOrder)
        li->setByteOrder(current_byteorder_);

    //
    //  Process clauses
    //
    processLineItemClauses(li, lname, clauses, level);

    return addPICLineItemToModel(level, lname, li, clauses);
}

/* Ex: 02 FLD TYPE BITMAP 5. */
bool
ProcessAST::addPICBitLineItemToModel(short level, AST::Literal *lname,
                                        AST::PicBitClause *cc, 
                                        QList<AST::LineItemClause *> clauses)
{
    Q_ASSERT(cc != NULL);

    Definition def;
    def.length_type_ = FIXED;
    if (cc->signed_flg_)
        def.data_type_ = BITMAP;
    else
        def.data_type_ = UBITMAP;

    if (cc->size_ < 1 || cc->size_ > 16) {
        DDLError error( lname->location().start_line,
                        lname->location().start_column,
                        QString("Incorrect use of BIT clause. Expecting length 1..16"));
        errors_<<error;
        return false;
    }

    def.length_ = cc->size_;
    
    LineDefinitionItem *li = model()->create<LineDefinitionItem>();
    QString name = lname->value().replace("-", "_").toLower();
    li->setName(name);
    li->define(def);

    //
    //  Process clauses
    //
    processLineItemClauses(li, lname, clauses, level);

    return addPICLineItemToModel(level, lname, li, clauses);
}

/* EX: 02 PAN TYPE *.
   02 PAN TYPE ALT */
bool
ProcessAST::addTypeLineItemToModel(short level, AST::Literal *lname,
                                   AST::Literal *ltype_name,
                                   QList<AST::LineItemClause *> clauses)
{
    QString sval = lname->val_;
    if (reservedKeywords_.contains(sval.replace("-","_")
                                   .replace("^","_").toLower())) {
        lname->val_ = sval.replace("-","_")
            .replace("^","_").toLower()+"_";
    }

    QString tname = ltype_name->value().replace("-","_").replace("^","_");
    if (reservedKeywords_.contains(tname))
        tname += "_";

    GroupDefinitionItem *cc = model_->findGroupDefinition(tname);
    if (cc != NULL ) {
        model()->processRedefines(cc);
        LineDefinitionItem *li = model()->create<LineDefinitionItem>();

        QString name = lname->value().replace("-","_").toLower();
        li->setName(name);
        li->setConstraintList(cc->constraints());
        if (cc->hasOccursDepend())
            li->setOccursDepend(cc->occursDepend(), 
                                cc->occursDependFrom(),
                                cc->occursDependTo());
        if (cc->hasOccursNumber()) {
            li->setOccursNumber(cc->occursNumber());
        }
        if (cc->hasRedefines()) {
            foreach(ContainerItem *r, li->redefines()) 
                li->addRedefine(r);
        }

        QList <ContainerItem*> childs = cc->childs();
        ContainerItem *item;
        foreach(item, cc->childs()) {
            li->add((ContainerItem*)item->clone());
        }

        processLineItemClauses(li, ltype_name, clauses, level);
        return addPICLineItemToModel(level,lname,li,clauses);
    }

    //maybe this type is a field definition...
    FieldDefinitionItem *fc = model_->findFieldDefinition(tname);
    if (fc != NULL ) {
        LineDefinitionItem *li = model()->create<LineDefinitionItem>();
        QString name = lname->value().replace("-","_").toLower();
        li->setName(name);
        li->define(fc->definition());
        li->setConstraintList(fc->constraints());
        if (fc->hasOccursDepend())
            li->setOccursDepend(fc->occursDepend(), 
                                fc->occursDependFrom(),
                                fc->occursDependTo());
        if (fc->hasOccursNumber()) {
            li->setOccursNumber(fc->occursNumber());
        }
        if (fc->hasRedefines()) {
            if (fc->hasOccursNumber() && fc->hasOccursDepend()) {
                DDLError error( ltype_name->location().start_line,
                                ltype_name->location().start_column,
                                QString("REDEFINE cannot be used on a line item with OCCURS"));
                errors_ << error;
                return false;
            }

            foreach(ContainerItem *r, li->redefines()) 
                li->addRedefine(r);
        }

        processLineItemClauses(li, ltype_name, clauses, level);
        return addPICLineItemToModel(level,lname,li,clauses);
    }

    DDLError error( ltype_name->location().start_line,
                    ltype_name->location().start_column,
                    QString("Unknown type:")+lname->value());
    errors_ << error;
    return false;
}

bool
ProcessAST::processLineItemClauses( LineDefinitionItem *li, AST::Literal *name,
                                    QList<AST::LineItemClause *> clauses, short level )
{
    foreach(AST::LineItemClause *cl,clauses) {

        if (!cl) 
            continue;
        
        switch (cl->kind_) {

        /** Various constraints **/
        case AST::Node::k_constraint_list:
        case AST::Node::k_constraint_value: {
            AST::Constraint *c = static_cast<AST::Constraint*>(cl);
            Q_ASSERT(c != NULL);
            addConstraintToLineItem( li, c);
        } break;

        /** OCCURS CLAUSE**/
        case AST::Node::k_line_clause_item_occurs: {
            AST::LineItemOccursClause *ocl = static_cast<AST::LineItemOccursClause*>(cl);
            Q_ASSERT(ocl!=NULL);
            li->setOccursNumber(ocl->times_);
        } break;

        /** OCCURS LITERAL**/
        case AST::Node::k_line_clause_item_occurs_literal: {
            AST::LineItemOccursLiteralClause *ocl = static_cast<AST::LineItemOccursLiteralClause*>(cl);
            Q_ASSERT(ocl!=NULL);
            AST::Literal *lit = (AST::Literal*)(ocl->literal_);
            QString sval = lit->val_;
            if (reservedKeywords_.contains(sval.replace("-","_")
                                           .replace("^","_").toLower())) {
                lit->val_ = sval.replace("-","_")
                    .replace("^","_").toLower()+"_";
            }

            ConstantItem *cc = model_->findConstant(lit->val_.replace("-","_"));
            if (cc == NULL ) {
                DDLError error( lit->location().start_line,
                                lit->location().start_column,
                                "Literal not defined. (can be only constant)");
                errors_ << error;
                return false;
            }
            // we have a constant
            if (cc->isNumber()) {
                li->setOccursNumber(cc->value().toDouble());
            } else {
                DDLError error( lit->location().start_line,
                                lit->location().start_column,
                                QString("Incorrect use of OCCURS clause."));
                errors_<<error;
                return false;
            }
        } break;

        /** OCCURS DEPENDING ON **/
        case AST::Node::k_line_clause_item_occurs_depending_on: {
            AST::LineItemOccursDependingOnClause *ocl;
            ocl = (AST::LineItemOccursDependingOnClause*)cl;
            int from = -1;
            int to   = -1;
            if (ocl->from_)
                from = ocl->from_->value();
            if (ocl->to_)
                to  =  ocl->to_->value();

            li->setOccursDepend(ocl->depending_->value().replace("-","_").toLower(), from, to);
        } break;

        /** DISPLAY CLAUSE**/
        case AST::Node::k_line_clause_item_display:{
            AST::LineItemDisplayClause *dcl = (AST::LineItemDisplayClause*)cl;
            li->setDisplayString(dcl->display_->value());
        } break;

        /** DEFAULT EMPTY CLAUSE**/
        case AST::Node::k_line_clause_item_default_empty:{
            Definition definition = li->definition();

            li->setDefault(QByteArray());
            // } else {
            //     DDLError error( cl->location().start_line,
            //                     cl->location().start_column,
            //                     QString("Incorrect use of DEFAULT EMPTY clause. Only avilable on String based atoms."));
            //     errors_<<error;
            //     return false;
            // }
        } break;

        /** DEFAULT CLAUSE**/
        case AST::Node::k_line_clause_item_default:{
            AST::LineItemDefaultClause *c = (AST::LineItemDefaultClause*)cl;
            Definition definition = li->definition();

            if (c->default_->kind_ == AST::Node::k_literal_numeric) {
                AST::NumericLiteral *l = (AST::NumericLiteral*)(c->default_);
                Q_ASSERT(l);

                li->setDefault(l->value());
            } else if (c->default_->kind_ == AST::Node::k_literal_string) {
                AST::StringLiteral *l = (AST::StringLiteral*)(c->default_);
                Q_ASSERT(l);

                if (definition.isString()) {
                    QString v = l->value();
                    v = v.mid(v.indexOf("'")+1, v.lastIndexOf("'")-1);
                    v = v.mid(v.indexOf("\"")+1, v.lastIndexOf("\"")-1);

                    if (v.length() > definition.length_) {
                        DDLError error( l->location().start_line,
                                        l->location().start_column,
                                        QString("Incorrect use of DEFAULT clause. Invalid Length."));
                        errors_<<error;
                        return false;
                    }
                    li->setDefault(v);
                } else {
                    DDLError error( cl->location().start_line,
                                    cl->location().start_column,
                                    QString("Incorrect use of DEFAULT clause. Expecting number."));
                    errors_<<error;
                    return false;
                }

            } else if (c->default_->kind_ == AST::Node::k_literal) {
                AST::Literal *lit = (AST::Literal*)(c->default_);
                QString sval = lit->val_;
                if (reservedKeywords_.contains(sval.replace("-","_")
                                               .replace("^","_").toLower())) {
                    lit->val_ = sval.replace("-","_")
                        .replace("^","_").toLower()+"_";
                }

                ConstantItem *cc = model_->findConstant(lit->val_.replace("-","_"));
                if (cc == NULL ) {
                    qDebug()<<lit->val_;
                    DDLError error( lit->location().start_line,
                                    lit->location().start_column,
                                    "Literal not defined. (can be only constant)");
                    errors_ << error;
                    return false;
                }
                // we have a constant
                if (cc->isNumber()) {
                    if (definition.isNumber()) {
                        li->setDefault(cc->value().toDouble());
                    } else {
                        DDLError error( lit->location().start_line,
                                        lit->location().start_column,
                                        QString("Incorrect use of DEFAULT clause."));
                        errors_<<error;
                        return false;
                    }
                } else {
                    if (definition.isString()) {
                        li->setDefault(cc->value().toString());
                    } else {
                        DDLError error( lit->location().start_line,
                                        lit->location().start_column,
                                        QString("Incorrect use of DEFAULT clause."));
                        errors_<<error;
                        return false;
                    }
                }
            }
        } break;

        /** LPAD CLAUSE**/
        case AST::Node::k_line_clause_item_lpad:{
            AST::LineItemLPadClause *c = (AST::LineItemLPadClause*)cl;
            QString pad = c->pad_->value();
            pad = pad.mid(pad.indexOf("'")+1, pad.lastIndexOf("'")-1);
            pad = pad.mid(pad.indexOf("\"")+1, pad.lastIndexOf("\"")-1);

            if (pad.length() != 1) {
                DDLError error( c->pad_->location().start_line,
                                c->pad_->location().start_column,
                                QString("Incorrect use of LPAD clause. Pad character should be 1 in length."));
                errors_<<error;
                return false;
            }
            li->setLPad(pad[0]);

        } break;

        /** RPAD CLAUSE**/
        case AST::Node::k_line_clause_item_rpad:{
            AST::LineItemRPadClause *c = (AST::LineItemRPadClause*)cl;
            QString pad = c->pad_->value();
            pad = pad.mid(pad.indexOf("'")+1, pad.lastIndexOf("'")-1);
            pad = pad.mid(pad.indexOf("\"")+1, pad.lastIndexOf("\"")-1);

            if (pad.length() != 1) {
                DDLError error( c->pad_->location().start_line,
                                c->pad_->location().start_column,
                                QString("Incorrect use of LPAD clause. Pad character should be 1 in length."));
                errors_<<error;
                return false;
            }
            li->setRPad(pad[0]);
        } break;

        /** VLENGTH **/
        case AST::Node::k_line_clause_item_vlength:{
            AST::LineItemVLengthClause *c = (AST::LineItemVLengthClause*)cl;
            Definition definition = li->definition();

            if (!c)
                qFatal("programmatic error AST::Node::k_line_clause_item_vlength");

            DDLError error( name->location().start_line,
                            name->location().start_column,
                            QString("Incorrect use of VLENGTH clause."));

            if (!definition.isDefined()) {
                errors_<< DDLError( name->location().start_line,
                                    name->location().start_column,
                                    QString("Incorrect use of VLENGTH clause. OCCURS cannot be used together with VLENGTH."));
                return false;
            }

            if (li->occursNumber() > 1) {
                errors_<< DDLError( name->location().start_line,
                                    name->location().start_column,
                                    QString("Incorrect use of VLENGTH clause. OCCURS cannot be used together with VLENGTH."));
                return false;
            }

            li->setVariableLength(c->dependant_->value().replace("-","_").replace("^","_").toLower());

        } break;

        /** VLENGTH INCLUSIVE **/
        case AST::Node::k_line_clause_item_vlength_inclusive:{
            AST::LineItemVLengthInclusiveClause *c = (AST::LineItemVLengthInclusiveClause*)cl;
            Definition definition = li->definition();

            if (!c)
                qFatal("programmatic error AST::Node::k_line_clause_item_vlengthinclusive");

            DDLError error( name->location().start_line,
                            name->location().start_column,
                            QString("Incorrect use of VLENGTH INCLUSIVE clause."));

            if (!definition.isDefined()) {
                errors_<< DDLError( name->location().start_line,
                                    name->location().start_column,
                                    QString("Incorrect use of VLENGTH INCLUSIVE clause. OCCURS cannot be used together with VLENGTH INCLUSIVE."));
                return false;
            }

            if (li->occursNumber() > 1) {
                errors_<< DDLError( name->location().start_line,
                                    name->location().start_column,
                                    QString("Incorrect use of VLENGTHINCLUSIVE clause. OCCURS cannot be used together with VLENGTH INCLUSIVE."));
                return false;
            }

            li->setVariableLengthInclusive(c->dependant_->value().replace("-","_").replace("^","_").toLower());

        } break;

        /** VLENGTH ALLINCLUSIVE **/
        case AST::Node::k_line_clause_item_vlength_allinclusive:{
            AST::LineItemVLengthAllInclusiveClause *c = (AST::LineItemVLengthAllInclusiveClause*)cl;
            Definition definition = li->definition();

            if (!c)
                qFatal("programmatic error AST::Node::k_line_clause_item_vlengthinclusive");

            DDLError error( name->location().start_line,
                            name->location().start_column,
                            QString("Incorrect use of VLENGTH INCLUSIVE clause."));

            if (!definition.isDefined()) {
                errors_<< DDLError( name->location().start_line,
                                    name->location().start_column,
                                    QString("Incorrect use of VLENGTH ALLINCLUSIVE clause. OCCURS cannot be used together with VLENGTH INCLUSIVE."));
                return false;
            }

            if (li->occursNumber() > 1) {
                errors_<< DDLError( name->location().start_line,
                                    name->location().start_column,
                                    QString("Incorrect use of VLENGTH ALLINCLUSIVE clause. OCCURS cannot be used together with VLENGTH ALLINCLUSIVE."));
                return false;
            }

            li->setVariableLengthAllInclusive(c->dependant_->value().replace("-","_").replace("^","_").toLower());

        } break;

        /** VLENGTH DELIMITER **/
        case AST::Node::k_line_clause_item_vlength_delimiter:{
            AST::LineItemVLengthDelimiterClause *c = (AST::LineItemVLengthDelimiterClause*)cl;
            if (li->occursNumber() > 1) {
                errors_<< DDLError( name->location().start_line,
                                    name->location().start_column,
                                    QString("Incorrect use of VLENGTH clause. OCCURS cannot be used together with VLENGTH."));
            }

            if (c->delimiter_->kind_ == AST::Node::k_literal_numeric) {
                AST::NumericLiteral *l = (AST::NumericLiteral*)(c->delimiter_);
                Q_ASSERT(l);
                if (l->value() > 256) {
                    DDLError error( l->location().start_line,
                                    l->location().start_column,
                                    QString("Incorrect use of VLENGTH DELIMITER clause. Delimiter should be one character."));

                    errors_<<error;
                    return false;
                }
                li->setVariableDelimited(QChar((qint8)(l->value())));
            } else if (c->delimiter_->kind_ == AST::Node::k_literal_string) {
                AST::StringLiteral *l = (AST::StringLiteral*)(c->delimiter_);
                Q_ASSERT(l);
                QString val = l->value();
                val = val.mid(val.indexOf("'")+1, val.lastIndexOf("'")-1);
                val = val.mid(val.indexOf("\"")+1, val.lastIndexOf("\"")-1);
                if (val.length() != 1) {
                    DDLError error( l->location().start_line,
                                    l->location().start_column,
                                    QString("Incorrect use of VLENGTH DELIMITER clause. Delimiter should be one character."));
                    errors_<<error;
                    return false;
                }
                li->setVariableDelimited(val[0]);

            } else if (c->delimiter_->kind_ == AST::Node::k_literal) {
                AST::Literal *l = (AST::Literal*)(c->delimiter_);
                ConstantItem *cc = model_->findConstant(l->val_.replace("-","_"));
                if (cc == NULL ) {
                    DDLError error( l->location().start_line,
                                    l->location().start_column,
                                    "Literal not defined. (can be only constant)");
                    errors_ << error;
                    return false;
                }
                if (cc->isNumber()) {
                    qint32 number = cc->value().toUInt();
                    if (number > 256) {
                        DDLError error( l->location().start_line,
                                        l->location().start_column,
                                        QString("Incorrect use of VLENGTH DELIMITER clause. Delimiter should be one character."));

                        errors_<<error;
                        return false;
                    }
                    li->setVariableDelimited(number);
                } else {
                    QString val = cc->value().toString();
                    val = val.mid(val.indexOf("'")+1, val.lastIndexOf("'")-1);
                    val = val.mid(val.indexOf("\"")+1, val.lastIndexOf("\"")-1);
                    if (val.length() != 1) {
                        DDLError error( l->location().start_line,
                                        l->location().start_column,
                                        QString("Incorrect use of VLENGTH DELIMITER clause. Delimiter should be one character."));
                        errors_<<error;
                        return false;
                    }
                    li->setVariableDelimited(val[0]);
                }

            } else {
                qFatal("should never get into here. VLENGTH DELIMITER");
            }
        } break;

        /** ISOBITMAP **/
        case AST::Node::k_line_clause_item_isobitmap: {
            li->setIsoBitmap(true);
        } break;

        // /** BIT **/
        // case AST::Node::k_pic_bit_clause: {
        //     AST::PicBitClause *c = (AST::PicBitClause*)cl;
        //     if (!c)
        //         qFatal("programmatic error AST::Node::k_pic_bit_clause");

        //     if (c->size_ < 1 && c->size_ > 15) {
        //         DDLError error( name->location().start_line,
        //                         name->location().start_column,
        //                         QString("Incorrect use of BIT clause. Expecting length 1..15"));
        //         errors_<<error;
        //         return false;
        //     }

        //     if (c->signed_flg_)
        //         li->setBitmap(c->size_);
        //     else
        //         li->setUBitmap(c->size_);

        // } break;

        /** COMP **/
        case (AST::Node::k_pic_comp_clause): {
            Definition def = li->definition();
            if (def.data_type_ == ASCII_NUMERIC || def.data_type_ == EBCDIC_NUMERIC) {
                li->setBCD();
            } else {
                DDLError error( cl->location().start_line,
                                cl->location().start_column,
                                QString("Incorrect use of PACKED-DECIMAL clause. Should be NUMERIC (9's)."));
                errors_<<error;
                return false;
            }
            
            if ( def.numeric_sign_type_ == UNSIGNED ) {
                if ( def.length_ <= 5 )
                    def.data_type_ = UINT16;
                else if (def.length_ <= 10)
                    def.data_type_ = UINT32;
                else
                    def.data_type_ = UINT64;
            } else {
                 if ( def.length_ <= 5 )
                    def.data_type_ = INT16;
                 else if (def.length_ <= 10)
                     def.data_type_ = INT32;
                 else
                     def.data_type_ = INT64;
            }

            li->define(def);

            if (current_byteorder_ != DefaultByteOrder) {
                li->setByteOrder(current_byteorder_);
            }

        } break;

        /** COMP-3 **/
        case AST::Node::k_line_clause_item_packed_decimal: {
            Definition def = li->definition();
            if (def.data_type_ == ASCII_NUMERIC || def.data_type_ == EBCDIC_NUMERIC) {
                li->setPackedBCD();
            } else if (def.data_type_ == ASCII || def.data_type_ == EBCDIC) {
                li->setPackedHex();
            } else {
                DDLError error( cl->location().start_line,
                                cl->location().start_column,
                                QString("Incorrect use of PACKED-DECIMAL clause. Should be NUMERIC (9's)."));
                errors_<<error;
                return false;
            }
        } break;

        /** CHOICE **/
        case AST::Node::k_line_clause_item_choice: {
            li->setChoice(true);
        } break;

        /* REDEFINES */
        case AST::Node::k_line_clause_item_redefines: {
            addPICLineItemToModel(level, name, li, clauses);
            AST::LineItemRedefinesClause *rcl = (AST::LineItemRedefinesClause*)cl;
            if (!rcl) return false;
            // Find what to redefine
            ContainerItem *data;
            bool found = false;
            QList<ContainerItem*> items;
            if (saved_item_->parent())
                items = saved_item_->parent()->childs();

            foreach (data, items) {
                if (reservedKeywords_.contains(rcl->name_->value().replace("-","_").replace("^","_").toLower())) {
                    rcl->name_->val_ = rcl->name_->value()
                        .replace("-","_").replace("^","_").toLower()+"_";
                }

                if (data->name().toLower() == rcl->name_->value().replace("-","_").replace("^","_").toLower()) {
                    found = true;
                    break;
                }
            }

            if (found == false) {
                DDLError error( rcl->name_->location().start_line,
                                rcl->name_->location().start_column,
                                "Item in redefines clause not found.");
                errors_ << error;
                return false;
            }
            
            if (data->isRedefine()) {
                DDLError error( rcl->name_->location().start_line,
                                rcl->name_->location().start_column,
                                "REDEFINE clause on REDEFINE component is not supported.");
                errors_ << error;
                return false;
            }

            if (data->hasOccursNumber() && data->hasOccursDepend()) {
                DDLError error( rcl->name_->location().start_line,
                                rcl->name_->location().start_column,
                                QString("REDEFINE cannot be used on a line item with OCCURS"));
                errors_ << error;
                return false;
            }
            data->addRedefine(li);
        } break;
        
        /** KEY **/
        case AST::Node::k_line_clause_item_key: {
            li->setKey(true);
        } break;
            
        default :/*qWarning("Unknown clause in DDL");*/ break;
        }
    } 


    return true;
}


void
ProcessAST::addConstraintToLineItem( LineDefinitionItem *li,
                                     AST::Constraint *c )
{

    QList<AST::Constraint *> constraints;

    if (c->kind_ == AST::Node::k_constraint_list) {
        for (AST::ConstraintList *it = (AST::ConstraintList*)c; it; it = it->next_) {
            constraints << it->constraint_;
        }
    } else
        constraints << c;

    foreach (AST::Constraint *c, constraints) {
        switch (c->kind_) {
            //
            //  Range Constraint
            //
        case (AST::Node::k_constraint_range) : {
            AST::ConstraintRange *r = (AST::ConstraintRange*)c;
            Q_ASSERT(r);

            qint32 from = 0;
            qint32 to   = 0;

            switch (r->from_->kind_) {

            case (AST::Node::k_literal_numeric): {from = ((AST::NumericLiteral*)r->from_)->val_;}break;
            case (AST::Node::k_literal) : { // this should be a constant
                // FROM
                AST::Literal *from_lit = (AST::Literal*)(r->from_);
                ConstantItem *cc = model_->findConstant(from_lit->val_.replace("-","_"));
                if (cc == NULL ) {
                    DDLError error( from_lit->location().start_line,
                                    from_lit->location().start_column,
                                    "Literal not defined");
                    errors_ << error;
                    return;
                }
                // we have a constant
                if (!cc->isNumber()) {
                    DDLError error( from_lit->location().start_line,
                                    from_lit->location().start_column,
                                    QString("Literal %1 refers to a non numeric constant").arg(from_lit->val_));
                    errors_ << error;
                    return;
                }
                bool ok = false;
                from = cc->value().toInt(&ok);
                Q_ASSERT(ok);
            } break;
            default: break;
            }

            switch (r->to_->kind_) {
            case (AST::Node::k_literal_numeric): to = ((AST::NumericLiteral*)r->to_)->val_;break;
            case (AST::Node::k_literal) : { // this should be a constant
                // TO
                AST::Literal *to_lit = (AST::Literal*)(r->to_);
                ConstantItem *cc = model_->findConstant(to_lit->val_.replace("-","_"));
                if (cc == NULL ) {
                    DDLError error( to_lit->location().start_line,
                                    to_lit->location().start_column,
                                    "Literal not defined");
                    errors_ << error;
                    return;
                }
                // we have a constant
                if (!cc->isNumber()) {
                    DDLError error( to_lit->location().start_line,
                                    to_lit->location().start_column,
                                    QString("Literal %1 refers to a non numeric constant").arg(to_lit->val_));
                    errors_ << error;
                    return;
                }
                bool ok = false;
                to = cc->value().toInt(&ok);
                Q_ASSERT(ok);
            } break;

            default: break;
            }

            //
            //  We have from and to now;
            //
            if (from > to) {
                DDLError error( c->location().start_line,
                                c->location().start_column,
                                QString("<FROM> value in RANGE must be <= than <TO>"));
                errors_ << error;
                return;
            }

            if ( !c->as_ )
                li->addConstraintRange( from,to, "VALUE "+QString::number(from)+" THROUGH "+QString::number(to));
            else
                li->addConstraintRange( from, to, c->as_->display_ );

        } break; // range

            //
            //  Value Constraint
            //
        case (AST::Node::k_constraint_value) : {
            AST::ConstraintValue *v = (AST::ConstraintValue*)c;
            Q_ASSERT(v);
            bool is_numeric = false;

            qint32 number = 0;
            QString string;

            switch (v->value_->kind_) {
            case (AST::Node::k_literal_numeric): {
                number = ((AST::NumericLiteral*)v->value_)->val_;
                is_numeric = true;
            } break;
            case (AST::Node::k_literal_string): {
                string = ((AST::StringLiteral*)v->value_)->val_;
                is_numeric = false;
            } break;
            case (AST::Node::k_literal) : {
                AST::Literal *lit = (AST::Literal*)(v->value_);
                ConstantItem *cc = model_->findConstant(lit->val_.replace("-","_"));
                if (cc == NULL ) {
                    DDLError error( lit->location().start_line,
                                    lit->location().start_column,
                                    "Literal not defined");
                    errors_ << error;
                    return;
                }
                if (cc->isNumber()) {
                    bool ok = false;
                    number = cc->value().toInt(&ok);
                    Q_ASSERT(ok);
                    is_numeric = true;
                } else {
                    string = cc->value().toString();
                }
            } break;
            default: break;
            };
            //
            //  We have the value
            //
            if ( is_numeric ) {
                if ( !c->as_ )
                    li->addConstraintValue( number,
                                            "VALUE " + QString::number(number));
                else {
                    li->addConstraintValue( number, c->as_->display_ );
                }
            }
            else { // is_string
                if ( !c->as_ )
                    li->addConstraintValue( string,
                                            "VALUE " + string );
                else {
                    li->addConstraintValue( string, c->as_->display_ );
                }
            }

        } break;

            //
            //  Literal Constraint
            //
        case (AST::Node::k_constraint_literal) : {
            AST::ConstraintLiteral *cc = (AST::ConstraintLiteral *)c;
            if (!cc)
                break;

            EnumDefinitionItem *ed = model()->findEnumDefinition(cc->name_->value().replace("-","_"));
            if (ed == NULL ) {
                //maybe is a constant
                ConstantItem *constant = model()->findConstant(cc->name_->value().replace("-","_"));
                if (!constant) {
                    DDLError error( cc->name_->location().start_line,
                                    cc->name_->location().start_column,
                                    "Unknown enum definition.");
                    errors_ << error;
                    return;
                } else {
                    li->addConstraintValue(constant->value(), constant->name());
                }
            } else {
                addConstraintToLineItem( li,
                                         ed->astConstraintList());
            }
        } break;

            //
            //  REGEX Constraint
            //
        case (AST::Node::k_constraint_regex) : {
            AST::ConstraintRegex *cc = (AST::ConstraintRegex *)c;
            if (!cc)
                break;
            li->addConstraintRegex(cc->regex_->value(),"REGEX:"+cc->regex_->value());

        } break;

        default : qDebug()<<"::WTF"<<c->kind_; Q_ASSERT(0); break;
        }
    }
}

bool
ProcessAST::visit(AST::LineItem *li)
{
    AST::PicOrType *cl = li->def_;

    QList<AST::LineItemClause *> clauses;


    if (reservedKeywords_.contains(li->name_->value())) {
        li->name_->val_ = li->name_->value()+"_";
    }

    for (AST::LineItemClauseList *it = li->clause_list_; it; it = it->next_) {
        if (it->line_item_clause_) {
            clauses << it->line_item_clause_;
        }
    }


    //
    //  Enumerations are processed separately
    //
    if (li->lvl_ == 88) {
        //@todo!
        return false;
    }

    if ( li->lvl_ == 89 || li->lvl_ == 88 ) { //Enumeration
        AST::ConstraintValue  *val = NULL;
        AST::LineItemAsClause *as  = NULL;
        foreach (AST::LineItemClause *c, clauses) {
            if (c->kind_ == AST::Node::k_constraint_value) {
                val = (AST::ConstraintValue*)c;
                continue;
            }

            if (c->kind_ == AST::Node::k_line_clause_item_as) {
                as = (AST::LineItemAsClause*)c;
                continue;
            }
        }

        if ( val && as )
            val->as_ = as;

        Q_ASSERT(saved_item_);

        //
        //  Add the constraint to the parent box
        //
        LineDefinitionItem *item = dynamic_cast<LineDefinitionItem*>(saved_item_);
        if (!item) {
            DDLError error( li->name_->location().start_line,
                            li->name_->location().start_column,
                            "Incorrect use of 89 clause");
            errors_ << error;
            return false;
        }

        if (val)
            addConstraintToLineItem( item, val );

        return false;

    } // if (li->lvl_ == 89)

    if (cl == NULL) {
        addLineItemToModel(li->lvl_, li->name_, clauses);
        return true;
    }

    AST::PicOrType *def = cl->val_;

    switch (def->kind_) {
    case (AST::Node::k_pic_clause) : {
        addPICLineItemToModel(li->lvl_,
                              li->name_,
                              static_cast<AST::PicClause*>(def)->string_,
                              clauses);
    } break;
    case (AST::Node::k_pic_bit_clause): {
        addPICBitLineItemToModel(li->lvl_,
                                 li->name_,
                                 (AST::PicBitClause*)(def),
                                 clauses);
    } break;
    case (AST::Node::k_pic_binary_clause): {
        addPICBinaryLineItemToModel(li->lvl_,
                                    li->name_,
                                    (AST::PicBinaryClause*)(def),
                                    clauses);
    } break;
    case (AST::Node::k_pic_comp_clause): {
        addPICStringLineItemToModel(li->lvl_,
                                    li->name_,
                                    static_cast<AST::PicClause*>(def)->string_,
                                    clauses);
    } break;
    case AST::Node::k_type_literal_clause: {
        addTypeLineItemToModel(li->lvl_,
                               li->name_,
                               static_cast<AST::TypeLiteralClause*>(def)->name_,
                               clauses);
    } break;
    case AST::Node::k_type_star_clause: {
        addTypeLineItemToModel(li->lvl_, li->name_, li->name_, clauses);
    } break;
    default : { qDebug()<<"type "<<def->kind_<<"is unsupported!";Q_ASSERT(0); }
    }
    return true;
}

bool
ProcessAST::visit(AST::FieldDefStatement *fs)
{
    FieldDefinitionItem *fd = model()->create<FieldDefinitionItem>();

    
    fs->name_->val_.replace("-","_");
    fs->name_->val_ = fs->name_->val_.toLower();
    if (reservedKeywords_.contains(fs->name_->val_)) {
        fs->name_->val_+="_";
    }

    fd->setName(fs->name_->value());

    FieldDefinitionItem *cc = model_->findFieldDefinition(fs->name_->value());
    GroupDefinitionItem *gc = model_->findGroupDefinition(fs->name_->value());

    if (cc != NULL || gc !=NULL) {
        DDLError error( fs->name_->location().start_line,
                        fs->name_->location().start_column,
                        "Previously defined literal.");
        //errors_ << error;
        //return false;
        //qWarning(error.toString().toLatin1().data());
        //if (cc)
        //    model_->removeFieldDefinition(cc->name());
        //else
        //    model_->removeGroupDefinition(cc->name());
    }


    AST::PicOrType *def = fs->def_->val_;

    switch (def->kind_) {
    case (AST::Node::k_pic_clause): {
        PICString pic(static_cast<AST::PicClause*>(def)->string_);
        fd->define(pic.getDefinition());
    } break;
    case (AST::Node::k_pic_binary_clause): {
        Definition d;
        AST::PicBinaryClause* cc =static_cast<AST::PicBinaryClause*>(def);
        d.length_type_ = FIXED;
        switch(cc->type_) {
        case(QMetaType::Char): d.data_type_ = INT8;d.length_ = 1;    break;
        case(QMetaType::UChar):d.data_type_ = UINT8;d.length_ = 1;   break;
        case(QMetaType::Short):d.data_type_  = INT16;d.length_ = 2;  break;
        case(QMetaType::UShort):d.data_type_ = UINT16;d.length_ = 2; break;
        case(QMetaType::Int):d.data_type_ = INT32;d.length_ = 4;     break;
        case(QMetaType::UInt):d.data_type_ = UINT32;d.length_ = 4;   break;
        case(QMetaType::LongLong):d.data_type_ = INT64;d.length_ = 8;break;
        case(QMetaType::ULongLong):d.data_type_ = UINT64;d.length_ = 8;break;
        case(QMetaType::Float):d.data_type_ = FLOAT;d.length_ = 4;     break;
        case(QMetaType::Double):d.data_type_ = DOUBLE;d.length_ = 8;   break;
        default: qFatal("fatal error, addPicBinaryLineItemToModel, unknown type");break;
        }

        fd->define(d);
        if (current_byteorder_ != DefaultByteOrder)
            fd->setByteOrder(current_byteorder_);

    } break;
    case AST::Node::k_type_literal_clause: {
        AST::TypeLiteralClause *tc = static_cast<AST::TypeLiteralClause*>(def);
        if (reservedKeywords_.contains(tc->name_->value().replace("-","_")
                                       .replace("^","_").toLower())) {
            tc->name_->val_ = tc->name_->value().replace("-","_")
                .replace("^","_").toLower()+"_";
        }

        FieldDefinitionItem *cc = model_->findFieldDefinition(tc->name_->value().replace("-","_").replace("^","_").toLower());
        bool found = false;
        if (!cc) {
            GroupDefinitionItem *cc = model_->findGroupDefinition(tc->name_->value().replace("-","_").replace("^","_").toLower());
            if (!cc) {
            DDLError error( tc->name_->location().start_line,
                            tc->name_->location().start_column,
                            ("Type "+tc->name_->value()+" is undefined").toLatin1().data());
            errors_ << error;
            return false;
            } else {
                fd->define(cc->definition());
            }
        } else {
            fd->define(cc->definition());
        }
    } break;
    case AST::Node::k_type_star_clause: {
        DDLError error( fs->name_->location().start_line,
                        fs->name_->location().start_column,
                        "TYPE * is unsupported on field definitions.");
        errors_ << error;
        return false;
    } break;

    default : { qDebug()<<"type "<<def->kind_<<"is unsupported!"; qFatal("fatal error"); }
    }

    //
    //  Process the clauses list
    //
    QList<AST::LineItemClause *> clauses;
    for (AST::LineItemClauseList *it = fs->clause_list_; it; it = it->next_) {
        if (it->line_item_clause_) {
            clauses << it->line_item_clause_;
        }
    }

    processLineItemClauses(fd, fs->name_, clauses);
    model_->addFieldDefinition(fd);

    return true;
}

bool
ProcessAST::visit(AST::EnumDefStatement *es)
{
    EnumDefinitionItem *ed = model()->create<EnumDefinitionItem>();

    ed->setName(es->name_->value().replace("-","_").toLower());

    EnumDefinitionItem *edd = model_->findEnumDefinition(es->name_->value());
    if (edd != NULL ) {
        DDLError error( es->name_->location().start_line,
                        es->name_->location().start_column,
                        "Previously defined enum definition.");
        //qWarning(error.toString().toLatin1().data());
        //errors_ << error;
        //return false;
        model_->removeEnumDefinition(ed->name());
    }

    AST::ConstraintList *cl = es->constraint_list_;
    Q_ASSERT(cl);

    ed->setConstraintList(cl);

    model()->addEnumDefinition(ed);
    return true;
}

bool
ProcessAST::visit(AST::GroupDefStatement *gs)
{
    QList<AST::GroupDefClause *> clauses;
    for (AST::GroupDefClauseList *it = gs->clause_list_; it; it = it->next_) {
        if (it->groupdef_clause_) {
            clauses << it->groupdef_clause_;
        }
    }

    GroupDefinitionItem *gd = model()->create<GroupDefinitionItem>();

    QString name = gs->name_->value().replace("-","_").replace("^","_").toLower();
    if (reservedKeywords_.contains(name)) {
        name += "_";
    }

    gd->setName(name.toLower());

    GroupDefinitionItem *cc = model_->findGroupDefinition(name);
    if (cc != NULL ) {
        DDLError error( gs->name_->location().start_line,
                        gs->name_->location().start_column,
                        "Previously defined group definition.");

        //model_->removeGroupDefinition(name);
        //qWarning(error.toString().toLatin1().data());
        //errors_ << error;
        //return false;
    }

    model_->addGroupDefinition(gd);

    current_charset_   = A;
    current_byteorder_ = DefaultByteOrder;

    foreach( AST::GroupDefClause *cl ,clauses ) {
        switch (cl->kind_) {
        case (AST::Node::k_groupdef_ebcdic_clause): {
            current_charset_ = E;
        } break;

        case (AST::Node::k_groupdef_highlow_clause): {
            current_byteorder_ = BigEndian;
        } break;

        case (AST::Node::k_groupdef_lowhigh_clause): {
            current_byteorder_ = LittleEndian;
        } break;

        default: break;
        }
    }

    saved_level_ = 1;
    saved_item_  = gd;
    levels_.clear();
    levels_.push_back(1);

    return true;
}

bool
ProcessAST::visit(AST::LineItemKeyIsClause *ki)
{
    
    //We support only PRIMARY KEYS
    if (ki->spec_->kind_ == AST::Node::k_literal_numeric) {
        if (!((ki->spec_ == 0) ||
              (((AST::NumericLiteral*)(ki->spec_))->val_ == 0 ))) {
            return true;
        }
    } else {
        return true;
    }

    ContainerItem *it = saved_item_;
    while(it->parent()) {
        it = it->parent();
    }

    ContainerItem *found = NULL;
    foreach (ContainerItem *i, it->childs()) {
        if (i->name() == ki->name_->val_.replace("-","_").replace("^","_").toLower())
            found = i;
    }

    if (!found && it->childs().size() != 0) {
        foreach (ContainerItem *i, (it->childs().at(0))->childs()) {
            if (i->name() == ki->name_->val_.replace("-","_").replace("^","_").toLower())
                found = i;
        }
    }

    if (!found) {
        DDLError error( ki->name_->location().start_line,
                        ki->name_->location().start_column,
                        "Invalid clause KEY IS, unable to find key.");
        errors_ << error;
        return false;
    }

    found->setKey(true);
    
    return true;
}

bool
ProcessAST::visit(AST::RecordDefStatement *rd)
{
    GroupDefinitionItem *gd = model()->create<GroupDefinitionItem>();

    QString name = rd->name_->value().replace("-","_").toLower();
    gd->setName(name.toLower());

    GroupDefinitionItem *cc = model_->findGroupDefinition(name);
    if (cc != NULL ) {
        DDLError error( rd->name_->location().start_line,
                        rd->name_->location().start_column,
                        "Previously defined group definition.");

        model_->removeConstant(name);
        //errors_ << error;
        //return false;
    }

    gd->setRecord(true);
    
    if (rd->rec_ref_) {
        QString name_ref = rd->rec_ref_->value().replace("-","_").replace("^","_").toLower();
        if (reservedKeywords_.contains(name_ref))
            name_ref += "_";

        GroupDefinitionItem *cc = model_->findGroupDefinition(name_ref);

        if ( cc!= NULL ) {
            cc->setName(name.toLower());
            model_->addGroupDefinition(cc);
            return true;
        } else {
            DDLError error( rd->rec_ref_->location().start_line,
                            rd->rec_ref_->location().start_column,
                            "Could not find record definition.");
            errors_ << error;
            return false;
        }
    } else {
        model_->addGroupDefinition(gd);
    }

    saved_level_ = 1;
    saved_item_  = gd;
    levels_.clear();
    levels_.push_back(1);
    return true;
}

bool
ProcessAST::visit(AST::ConstStatement *cs)
{
    ConstantItem *c = model()->create<ConstantItem>();

    c->setName(cs->name_->val_.replace("-","_").toLower());

    ConstantItem *cc = model_->findConstant(cs->name_->val_.replace("-","_"));
    if (cc != NULL ) {
        DDLError error( cs->name_->location().start_line,
                        cs->name_->location().start_column,
                        "Previously defined literal.");
        model_->removeConstant(c->name());
        //errors_ << error;
        //return false;
    }

    switch (cs->value_->kind_) {
    case (AST::Node::k_literal_string) : {
        c->setString();
        c->setValue(dynamic_cast<AST::StringLiteral*>(cs->value_)->val_);
    } break;
    case (AST::Node::k_literal_numeric) : {
        c->setNumber();
        c->setValue(dynamic_cast<AST::NumericLiteral*>(cs->value_)->val_);
    } break;
    case (AST::Node::k_literal) : {
        ConstantItem *cc;
        cc = model_->findConstant(dynamic_cast<AST::Literal*>(cs->value_)->val_.replace("-","_"));
        if (cc != NULL) {
            cc->isNumber() ? c->setNumber() : c->setString();
            c->setValue(cc->value());
        }
        else {
            DDLError error( cs->value_->location().start_line,
                            cs->value_->location().start_column,
                            "Unknown literal.");
            errors_ << error;
        }
    } break;
    default : {
        Q_ASSERT(0);
        //error handling
    } break;
    }

    model_->addConstant(c);

    return false;
}

#ifndef ASTPARSER
#define ASTPARSER

#include <QtCore/QByteArray>
#include <QtCore/QString>
#include <QtCore/QUrl>
#include <QtCore/QList>

#include "ast.h"
#include "astvisitor.h"
#include "error.h"
#include "codemodel.h"
#include "generatedparser.h"

// @see QDeclarativeScriptParser

class DDLParser;
class DDLASTParser
{
public:
    DDLASTParser();
    virtual ~DDLASTParser();

    bool parse(const QString &data, const QUrl &url );

    void setScriptFile(const QString &filename) { script_file_ = filename; }
    QString scriptFile() const { return script_file_; }
    DDLCodeModel *model() const { return model_; }
    QList<DDLError> errors() const { return errors_; }

private:
    QString script_file_;
    QList<DDLError> errors_;
    DDLCodeModel *model_;
    DDLParser    *parser_;
};

#endif //ASTPARSER

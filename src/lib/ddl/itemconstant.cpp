#include "itemconstant.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

ConstantItem::ConstantItem(int kind) 
    : CodeModelItemBase(kind),
      is_number_(false),
      is_string_(false),
      value_(0)
{

}

CodeModelItemBase*
ConstantItem::clone()
{
    ConstantItem *_clone = new ConstantItem(this->kind());
    _clone->is_number_ = this->isNumber();
    _clone->is_string_ = this->isString();
    _clone->value_ = this->value();
    return _clone;
}

bool
ConstantItem::isNumber() const
{
    return is_number_;
}

bool
ConstantItem::isString() const
{
    return is_string_;
}

void
ConstantItem::setNumber()
{
    is_number_  = true;
    is_string_  = false;
}

void
ConstantItem::setString()
{
    is_number_  = false;
    is_string_  = true;
}

void
ConstantItem::setValue(QVariant value)
{
    value_ = value;
}

QVariant
ConstantItem::value() const
{
    return value_;
}

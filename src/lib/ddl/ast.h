#ifndef DDL_AST_H
#define DDL_AST_H

#include <QtCore/QString>
#include <QtCore/QByteArray>
#include <QtCore/QVariant>
#include <QtCore/QDebug>
#include <QtCore/qglobal.h>
#include <QtCore/qshareddata.h>

#include "astvisitor.h"

#include <assert.h>

#ifdef __TANDEM
#include <sys/types.h>
#endif


Q_DECLARE_METATYPE ( QMetaType::Type )

class MemoryPool : public QSharedData {
public:
    enum {
        maxBlockCount = -1
    };
    enum {
        defaultBlockSize = 1 << 12
    };

    MemoryPool() {
        m_blockIndex = maxBlockCount;
        m_currentIndex = 0;
        m_storage = 0;
        m_currentBlock = 0;
        m_currentBlockSize = 0;
    }

    virtual ~MemoryPool() {
        for ( int index = 0; index < m_blockIndex + 1; ++index )
            qFree(m_storage[index]);

        qFree(m_storage);
    }

    char *allocate(int bytes) {
        bytes += (8 - bytes) & 7; // ensure multiple of 8 bytes (maintain alignment)
        if ( m_currentBlock == 0 || m_currentBlockSize < m_currentIndex + bytes ) {
            ++m_blockIndex;
            m_currentBlockSize = defaultBlockSize << m_blockIndex;

            m_storage = reinterpret_cast<char**>(qRealloc(m_storage, sizeof(char*) * (1 + m_blockIndex)));
            m_currentBlock = m_storage[m_blockIndex] = reinterpret_cast<char*>(qMalloc(m_currentBlockSize));
            ::memset(m_currentBlock, 0, m_currentBlockSize);

            m_currentIndex = (8 - quintptr(m_currentBlock)) & 7; // ensure first chunk is 64-bit aligned
            Q_ASSERT(m_currentIndex + bytes <= m_currentBlockSize);
        }

        char *p = reinterpret_cast<char *>
                  (m_currentBlock + m_currentIndex);

        m_currentIndex += bytes;

        return p;
    }

    int bytesAllocated() const {
        int bytes = 0;
        for ( int index = 0; index < m_blockIndex; ++index )
            bytes += (defaultBlockSize << index);
        bytes += m_currentIndex;
        return bytes;
    }

private:
    int m_blockIndex;
    int m_currentIndex;
    char *m_currentBlock;
    int m_currentBlockSize;
    char **m_storage;

private:
    Q_DISABLE_COPY(MemoryPool)
};

namespace AST {

    template <typename NodeType>
    inline NodeType *makeAstNode(MemoryPool *storage)
    {
        NodeType *node = new (storage->allocate(sizeof(NodeType))) NodeType();
        return node;
    }

    template <typename NodeType, typename Arg1>
    inline NodeType *makeAstNode(MemoryPool *storage, Arg1 arg1)
    {
        NodeType *node = new (storage->allocate(sizeof(NodeType))) NodeType(arg1);
        return node;
    }

    template <typename NodeType, typename Arg1, typename Arg2>
    inline NodeType *makeAstNode(MemoryPool *storage, Arg1 arg1, Arg2 arg2)
    {
        NodeType *node = new (storage->allocate(sizeof(NodeType))) NodeType(arg1, arg2);
        return node;
    }

    template <typename NodeType, typename Arg1, typename Arg2, typename Arg3>
    inline NodeType *makeAstNode(MemoryPool *storage, Arg1 arg1, Arg2 arg2, Arg3 arg3)
    {
        NodeType *node = new (storage->allocate(sizeof(NodeType))) NodeType(arg1, arg2, arg3);
        return node;
    }

    template <typename NodeType, typename Arg1, typename Arg2, typename Arg3, typename Arg4>
    inline NodeType *makeAstNode(MemoryPool *storage, Arg1 arg1, Arg2 arg2, Arg3 arg3, Arg4 arg4)
    {
        NodeType *node = new (storage->allocate(sizeof(NodeType))) NodeType(arg1, arg2, arg3, arg4);
        return node;
    }

    struct Location {
        Location() : start_line(-1), start_column(-1), end_line(-1), end_column(-1) {}

        int start_line;
        int start_column;
        int end_line;
        int end_column;
    };

    class Visitor;
    class TypeClause;
    class PicClause;

    class Node {
    public:
        enum Kind {
            k_undefined = 1,
            k_literal ,
            k_literal_numeric,
            k_literal_string,
            k_statement,
            k_statement_list,
            k_const_statement,
            k_constraint,
            k_constraint_list,
            k_constraint_range,
            k_constraint_regex,
            k_constraint_value,
            k_constraint_literal,            
            k_enumdef_statement,
            k_groupdef_clause,
            k_groupdef_clause_list,
            k_groupdef_ebcdic_clause,
            k_groupdef_highlow_clause,
            k_groupdef_lowhigh_clause,
            k_line_item,
            k_line_item_list,
            k_line_item_clause,
            k_line_item_clause_list,
            k_line_clause_item_as,
            k_line_clause_item_redefines,
            k_line_clause_item_occurs,
            k_line_clause_item_occurs_literal,
            k_line_clause_item_occurs_depending_on,
            k_line_clause_item_default,
            k_line_clause_item_default_empty,
            k_line_clause_item_display,
            k_line_clause_item_vlength,
            k_line_clause_item_vlength_inclusive,
            k_line_clause_item_vlength_allinclusive,
            k_line_clause_item_vlength_delimiter,
            k_line_clause_item_value,
            k_line_clause_item_isobitmap,
            k_line_clause_item_packed_decimal,
            k_line_clause_item_choice,
            k_line_clause_item_lpad,
            k_line_clause_item_rpad,
            k_line_clause_item_key,
            k_line_clause_item_key_is,
            k_line_clause_item_key_is_list,
            k_line_clause_item_not_implemented,
            k_pic_or_type,
            k_pic_clause,
            k_pic_comp_clause,
            k_pic_binary_clause,
            k_pic_bit_clause,
            k_type_clause ,
            k_type_literal_clause,
            k_type_star_clause,

            NODE_KIND_COUNT
        };

        Node() : kind_(k_undefined) {}
        virtual ~Node() {}

        Location location() const { return loc_;}
        void setLocation(Location &loc) { loc_ = loc;}

        void accept(Visitor *visitor);
        static void accept(Node *node, Visitor *visitor);

        virtual void accept0(Visitor *visitor) = 0;

        //attributes
        Kind          kind_;
        Location      loc_;
    };

    /*
     * literal
     */
    class Literal : public Node {
    public:
        Literal(const char *val) { kind_ = k_literal; val_ = QString(QByteArray(val)).remove("\"");}
        virtual ~Literal() {}

        inline QString value() const { return val_;}

        virtual void accept0(Visitor *visitor);

        //attributes
        QString val_;
    };

    /*
     * numeric_literal
     */
    class NumericLiteral : public Node {
    public:
        NumericLiteral(qint64 val) { kind_ = k_literal_numeric; val_ = val;}
        virtual ~NumericLiteral() {}

        inline qint64 value() const { return val_;}

        virtual void accept0(Visitor *visitor);

        //attributes
        qint64 val_;
    };

    /*
     * string_literal
     */
    class StringLiteral : public Node {
    public:
        StringLiteral(const char *val) { kind_ = k_literal_string; val_ = QString(QByteArray(val)).remove("\"").remove("\'");}
        virtual ~StringLiteral() {}

        inline QString value() const { return val_;}

        virtual void accept0(Visitor *visitor);

        //attributes
        QString val_;
    };


    /*
     *  statement
     */
    class Statement : public Node {
    public:
        Statement() { kind_ = k_statement;}
        virtual ~Statement() {}
    };

    /*
     *  statement_list
     */
    class StatementList : public Node {
    public:
        StatementList(Statement *stmt) : 
        statement_(stmt), next_(this) {
            kind_ = k_statement_list;
        }

        StatementList(StatementList *prev, Statement *stmt) :
        statement_(stmt) {
            kind_ = k_statement_list;
            next_ = prev->next_;
            prev->next_ = this;
        }

        virtual void accept0(Visitor *visitor);

        inline StatementList *finish() {
            StatementList *front = next_;
            next_ = 0;
            return front;
        }

        //atributes
        Statement *statement_;
        StatementList *next_;
    };


    /*
     *  const_statement
     */
    class ConstStatement : public Statement {
    public:
        ConstStatement(Literal *name, Node *value) : 
        name_(name), value_(value)
        {
            kind_ = k_const_statement;

            Q_ASSERT((value->kind_ == k_literal)        ||
                     (value->kind_ == k_literal_numeric)||
                     (value->kind_ == k_literal_string));

        }
        virtual ~ConstStatement() {}
        virtual void accept0(Visitor *visitor);

        Literal       *name_;
        Node          *value_;
    };

    /*
     *  pic_or_type_clause
     */
    class PicOrType : public Node {
    public:
        PicOrType( Node *n = NULL ) : val_((PicOrType*)n) {}

        virtual ~PicOrType() {}

        virtual void accept0(Visitor *visitor);

        // attributes
        PicOrType *val_;
    };

    class PicClause : public PicOrType {
    public:
       PicClause(QString &str): string_(str) { 
            kind_ = k_pic_clause; }
        virtual ~PicClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
        QString string_;
    };

    class PicCompClause : public PicOrType {
    public:
        PicCompClause() { 
            kind_ = k_pic_comp_clause; 
        }
        virtual ~PicCompClause() {}

        virtual void accept0(Visitor *visitor);
    };

    class PicBinaryClause : public PicOrType {
    public:
        PicBinaryClause(QMetaType::Type t): type_(t) { 
            kind_ = k_pic_binary_clause; 
        }
        virtual ~PicBinaryClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
        QMetaType::Type type_;
    };

    class PicBitClause : public PicOrType {
    public:
       PicBitClause(ssize_t size, bool signed_flg): size_(size), signed_flg_(signed_flg) { 
            kind_ = k_pic_bit_clause; 
        }

        virtual ~PicBitClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
        ssize_t    size_;
        bool       signed_flg_;
    };

    class TypeClause : public PicOrType {
    public:
        TypeClause( TypeClause *type = NULL): type_((TypeClause *)type) { 
            kind_ = k_type_clause; 
        }
        virtual ~TypeClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
        TypeClause *type_;
    };

    class TypeLiteralClause: public TypeClause {
    public:
        TypeLiteralClause( Literal *literal ) : name_(literal) { kind_ = k_type_literal_clause;}
        virtual ~TypeLiteralClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
        Literal *name_;
    };

    class TypeStarClause: public TypeClause {
    public:
        TypeStarClause()  { kind_ = k_type_star_clause;}
        virtual ~TypeStarClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
    };


    class LineItem : public Node {
    public:
        LineItem(int lvl, 
                 Literal *name, 
                 LineItemClauseList *clause_list = NULL) : 
            lvl_(lvl), 
            name_(name), 
            def_(NULL),
            clause_list_(clause_list){ 
                kind_ = k_line_item; 
        }

        LineItem(int lvl, 
                 Literal *name, 
                 PicOrType *def, 
                 LineItemClauseList *clause_list = NULL) : lvl_(lvl), 
        name_(name), 
        def_(def),
        clause_list_(clause_list){ 
            kind_ = k_line_item; 
        }

        virtual ~LineItem() {}

        virtual void accept0(Visitor *visitor);

        unsigned           lvl_;
        Literal            *name_;
        PicOrType          *def_;
        LineItemClauseList *clause_list_;
    };

    /*
     *  line_item list
     */
    class LineItemList : public Node {
    public:
        LineItemList( LineItem *item ) :
        line_item_(item), next_(this) { 
            kind_ = k_line_item_list; 
        }

        LineItemList( LineItemList *prev, LineItem *item ) :
        line_item_(item) {
            kind_ = k_line_item_list;
            next_ = prev->next_;
            prev->next_ = this;
        }

        virtual ~LineItemList() {}

        virtual void accept0(Visitor *visitor);

        inline LineItemList *finish() {
            LineItemList *front = next_;
            next_ = 0;
            return front;
        }

        //atributes
        LineItem         *line_item_;
        LineItemList     *next_;
    };

    // groupdef_clause

    class GroupDefClause : public Node {
    public:
        GroupDefClause() { 
            kind_ = k_groupdef_clause; 
        }

        virtual ~GroupDefClause() {}
    };
    
    /*
     * groupdef_ebcdic_clause
     */
    class GroupDefEbcdicClause : public GroupDefClause {
    public:
        GroupDefEbcdicClause ()  { 
            kind_ = k_groupdef_ebcdic_clause; 
        }
        virtual ~GroupDefEbcdicClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
    };

    /*
     * groupdef_highlow_clause
     */
    class GroupDefHighLowClause : public GroupDefClause {
    public:
        GroupDefHighLowClause ()  { 
            kind_ = k_groupdef_highlow_clause; 
        }
        virtual ~GroupDefHighLowClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
    };

    /*
     * groupdef_lowhigh_clause
     */
    class GroupDefLowHighClause : public GroupDefClause {
    public:
        GroupDefLowHighClause ()  { 
            kind_ = k_groupdef_lowhigh_clause; 
        }
        virtual ~GroupDefLowHighClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
    };

    /*
     *  groupdef_clause_list
     */
    class GroupDefClauseList : public Node {
    public:

        GroupDefClauseList( /*null clause*/ ) :
        groupdef_clause_(NULL), next_(this) { 
            kind_ = k_groupdef_clause_list;
        }

        GroupDefClauseList( GroupDefClause *item ) :
        groupdef_clause_(item), next_(this) { 
            kind_ = k_groupdef_clause_list; 
        }

        GroupDefClauseList( GroupDefClauseList *prev, GroupDefClause *item ) :
        groupdef_clause_(item) {
            kind_ = k_groupdef_clause_list;
            next_ = prev->next_;
            prev->next_ = this;
        }

        GroupDefClauseList* merge( GroupDefClauseList *alt) 
        {
            GroupDefClauseList *it = alt;
            while ( it->next_ ) {
                it = it->next_; 
            };
            GroupDefClauseList *front = next_;
            it->next_ = front;
            next_ = 0;

            return alt;
        }

        virtual ~GroupDefClauseList() {}

        virtual void accept0(Visitor *visitor);

        inline GroupDefClauseList *finish() {
            GroupDefClauseList *front = next_;
            next_ = 0;
            return front;
        }

        //atributes
        GroupDefClause         *groupdef_clause_;
        GroupDefClauseList     *next_;
    };


    // line_item_clause
    class LineItemClause : public Node {
    public:
        LineItemClause() { 
            kind_ = k_line_item_clause; 
        }

        virtual ~LineItemClause() {}
    };
    
    // line_item_clause
    class LineItemNotImplementedClause : public LineItemClause {
    public:
        LineItemNotImplementedClause() { 
            kind_ =  k_line_clause_item_not_implemented; 
        }
       
        virtual void accept0(Visitor *visitor);
 
        virtual ~LineItemNotImplementedClause() {}
    };


    /*
     *  as_clause
     */    
    class LineItemAsClause : public LineItemClause {
    public:
        LineItemAsClause (StringLiteral *val) : display_(val->val_) { 
            kind_ = k_line_clause_item_as; }

        LineItemAsClause (QString val) : display_(val) { 
            kind_ = k_line_clause_item_as; }

        virtual ~LineItemAsClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
        QString display_;
    };

    /*
     *  must_be_clause
     */
    class Constraint : public LineItemClause {
    public:
        Constraint(LineItemAsClause *as = NULL) : as_(as) { kind_ = k_constraint; }
        virtual ~Constraint() {}
        virtual void accept0(Visitor *visitor);

    //atributes
    LineItemAsClause *as_;  
    };

    /*
     *  must_be_clause_list
     */
    class ConstraintList : public Constraint {
    public:
        ConstraintList(Constraint *c) :  
        constraint_(c), next_(this) {
            kind_ = k_constraint_list;
        }

        ConstraintList(ConstraintList *prev, Constraint *c) :
        constraint_(c) {
            kind_ = k_constraint_list;
            next_ = prev->next_;
            prev->next_ = this;
        }

        virtual void accept0(Visitor *visitor);

        inline ConstraintList *finish() {
            ConstraintList *front = next_;
            next_ = 0;
            return front;
        }

        //atributes
        Constraint *constraint_;
        ConstraintList *next_;
    };

    /*
     *  constraint_range
     */
    class ConstraintRange : public Constraint {
    public:
        ConstraintRange( Node *from, Node *to, LineItemAsClause *as = 0 ) : 
              Constraint(as), from_(from), to_(to) { kind_ = k_constraint_range;}
        virtual ~ConstraintRange() {}

        virtual void accept0(Visitor *visitor);
    public:
        Node *from_;
        Node *to_;
    };

    /*
     *  constraint_regex
     */
    class ConstraintRegex : public Constraint {
    public:
        ConstraintRegex( StringLiteral *regex ) : 
              regex_ (regex) { kind_ = k_constraint_regex;}

        virtual ~ConstraintRegex() {}

        virtual void accept0(Visitor *visitor);
    public:
        StringLiteral *regex_;
    };

    /*
     *  constraint_value
     */
    class ConstraintValue : public Constraint {
    public:
        ConstraintValue( Node *value, LineItemAsClause *as = 0 ) : 
        Constraint(as), value_(value) { 
            kind_ = k_constraint_value;     
           }

        virtual ~ConstraintValue() {}
        virtual void accept0(Visitor *visitor);
    public:
        Node *value_;
    };

    /*
     *  constraint_literal
     */
    class ConstraintLiteral : public Constraint {
    public:
        ConstraintLiteral( Literal *name, LineItemAsClause *as = 0) :
        Constraint(as), name_(name) { kind_ = k_constraint_literal; }

        virtual ~ConstraintLiteral() {}

        virtual void accept0(Visitor *visitor);
        
        //attributes
        Literal *name_;
    };


    /*
     *  redefines_clause
     */
    class LineItemRedefinesClause : public LineItemClause {
    public:
        LineItemRedefinesClause (Literal *name) : name_(name)  { 
            kind_ = k_line_clause_item_redefines; 
        }
        virtual ~LineItemRedefinesClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
        Literal *name_;
    };

    /*
     *  occurs_clause
     */
    class LineItemOccursClause : public LineItemClause {
    public:
        LineItemOccursClause (unsigned int times) : times_(times)  { 
            kind_ = k_line_clause_item_occurs; /*qFree(times)*/}        
        virtual ~LineItemOccursClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
        unsigned int  times_;
    };

    /*
     *  occurs_clause
     */
    class LineItemOccursLiteralClause : public LineItemClause {
    public:
        LineItemOccursLiteralClause (Literal *literal) : literal_(literal)  { 
            kind_ = k_line_clause_item_occurs_literal; /*qFree(times)*/}        
        virtual ~LineItemOccursLiteralClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
        Literal  *literal_;
    };

    class LineItemOccursDependingOnClause : public LineItemClause {
    public:
        LineItemOccursDependingOnClause (NumericLiteral *from,
                     NumericLiteral *to,
                     Literal        *depending) 
        : from_(from),
        to_(to),
        depending_(depending) { 
        kind_ = k_line_clause_item_occurs_depending_on; }

    virtual ~LineItemOccursDependingOnClause() {}
    virtual void accept0(Visitor *visitor);

    //attributes
    NumericLiteral      *from_;
    NumericLiteral      *to_;
    Literal  *depending_;
    };
    
    /*
     *  clause_dispkay
     */    
    class LineItemDisplayClause : public LineItemClause {
    public:
        LineItemDisplayClause (StringLiteral *val) : display_(val) { 
            kind_ = k_line_clause_item_display; /*qFree(times)*/}
        virtual ~LineItemDisplayClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
        StringLiteral  *display_;
    };

    /*
     *  clause_default
     */    
    class LineItemDefaultClause : public LineItemClause {
    public:
        LineItemDefaultClause (Node *val) : default_(val) { 
            kind_ = k_line_clause_item_default; /*qFree(times)*/}
        virtual ~LineItemDefaultClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
        Node  *default_;
    };

    /*
     *  clause_default
     */    
    class LineItemDefaultEmptyClause: public LineItemClause {
    public:
        LineItemDefaultEmptyClause ()  { 
            kind_ = k_line_clause_item_default_empty; /*qFree(times)*/}

        virtual ~LineItemDefaultEmptyClause() {}
        virtual void accept0(Visitor *visitor);
    };

    /*
     *  clause_lpad
     */   
    class LineItemLPadClause : public LineItemClause {
    public:
    LineItemLPadClause (StringLiteral *pad) : pad_(pad) {
        kind_ = k_line_clause_item_lpad;
    }
    virtual ~LineItemLPadClause() {}

    virtual void accept0(Visitor *visitor);

    //attributes
    StringLiteral *pad_;
    };

    /*
     *  clause_rpad
     */ 
    class LineItemRPadClause : public LineItemClause {
    public:
    LineItemRPadClause (StringLiteral *pad) : pad_(pad) {
        kind_ = k_line_clause_item_rpad;
    }
    virtual ~LineItemRPadClause() {}

    virtual void accept0(Visitor *visitor);

    //attributes
    StringLiteral *pad_;
    };


    /*
     *  clause_key
     */ 
    class LineItemKeyClause : public LineItemClause {
    public:
    LineItemKeyClause () {
            kind_ = k_line_clause_item_key;
    }
    virtual ~LineItemKeyClause() {}
    virtual void accept0(Visitor *visitor);
        
    //attributes
    };

    /*
     *  clause_key_is
     */ 
    class LineItemKeyIsClause : public LineItemClause {
    public:
        LineItemKeyIsClause (Literal *name, Literal *spec) :
        name_(name) {
            spec_ = NULL;
            kind_ = k_line_clause_item_key_is;
        }

        LineItemKeyIsClause (Literal *name, NumericLiteral *spec = new NumericLiteral(0)) :
        name_(name), spec_(spec) {
            kind_ = k_line_clause_item_key_is;
        }

        LineItemKeyIsClause (Literal *name, StringLiteral *spec) :
        name_(name), spec_(spec) {
            kind_ = k_line_clause_item_key_is;
        }


        LineItemKeyIsClause () :
        name_(NULL), spec_(NULL) {
            kind_ = k_line_clause_item_key_is;
        }

    virtual ~LineItemKeyIsClause() {}
    virtual void accept0(Visitor *visitor);
        
    //attributes
    Literal *name_;
    Node    *spec_ /*is either numeric or string*/;
    };

    /*
     *  line_item_clause_list
     */
    class LineItemKeyIsClauseList : public Node {
    public:

        LineItemKeyIsClauseList( /*null clause*/ ) :
        key_is_clause_(NULL), next_(this) { 
            kind_ = k_line_clause_item_key_is_list;
        }

        LineItemKeyIsClauseList( LineItemKeyIsClause *item ) :
        key_is_clause_(item), next_(this) { 
            kind_ =  k_line_clause_item_key_is_list; 
        }

        LineItemKeyIsClauseList( LineItemKeyIsClauseList *prev, 
                                 LineItemKeyIsClause *item ) :
        key_is_clause_(item) {
            kind_ =  k_line_clause_item_key_is_list;
            next_ = prev->next_;
            prev->next_ = this;
        }

        LineItemKeyIsClauseList* merge( LineItemKeyIsClauseList *alt) 
        {
            LineItemKeyIsClauseList *it = alt;
            while ( it->next_ ) {
                it = it->next_; 
            };

            LineItemKeyIsClauseList *front = next_;
            it->next_ = front;
            next_ = 0;

            return alt;
        }

        virtual ~LineItemKeyIsClauseList() {}

        virtual void accept0(Visitor *visitor);

        inline LineItemKeyIsClauseList *finish() {
            LineItemKeyIsClauseList *front = next_;
            next_ = 0;
            return front;
        }

        //atributes
        LineItemKeyIsClause         *key_is_clause_;
        LineItemKeyIsClauseList     *next_;
    };


    /*
     *  vlength_clause
     */    
    class LineItemVLengthClause : public LineItemClause {
    public:
        LineItemVLengthClause (Literal *dependant) { 
            dependant_ = dependant; kind_ = k_line_clause_item_vlength;}
        virtual ~LineItemVLengthClause() {}

        virtual void accept0(Visitor *visitor);

    //attributes
    Literal *dependant_;
    };

    /*
     *  vlength_inclusive_clause
     */    
    class LineItemVLengthInclusiveClause : public LineItemClause {
    public:
        LineItemVLengthInclusiveClause (Literal *dependant) { 
            dependant_ = dependant; kind_ = k_line_clause_item_vlength_inclusive;}
        virtual ~LineItemVLengthInclusiveClause() {}

        virtual void accept0(Visitor *visitor);

    //attributes
    Literal *dependant_;
    };

    /*
     *  vlength_allinclusive_clause
     */    
    class LineItemVLengthAllInclusiveClause : public LineItemClause {
    public:
        LineItemVLengthAllInclusiveClause (Literal *dependant) { 
            dependant_ = dependant; kind_ = k_line_clause_item_vlength_allinclusive;}
        virtual ~LineItemVLengthAllInclusiveClause() {}

        virtual void accept0(Visitor *visitor);

    //attributes
    Literal *dependant_;
    };

    /*
     *  vlength_clause_delimiter
     */    
    class LineItemVLengthDelimiterClause : public LineItemClause {
    public:
        LineItemVLengthDelimiterClause (Node *delimiter) {
            delimiter_ = delimiter;
            kind_ = k_line_clause_item_vlength_delimiter;}
        virtual ~LineItemVLengthDelimiterClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
        Node *delimiter_;
    };

    /*
     *  isobitmap_clause
     */    
    class LineItemIsoBitmapClause : public LineItemClause {
    public:
        LineItemIsoBitmapClause () { 
            kind_ = k_line_clause_item_isobitmap;}
        virtual ~LineItemIsoBitmapClause() {}

        virtual void accept0(Visitor *visitor);

        //attributes
    };

    /*
     *  packed_decimal_clause
     */
    class LineItemPackedDecimalClause : public LineItemClause {
    public:
        LineItemPackedDecimalClause () {
            kind_ = k_line_clause_item_packed_decimal;}
        virtual ~LineItemPackedDecimalClause() {}

        virtual void accept0(Visitor *visitor);
        //attributes
    };

    /*
     * choice_clause
     */
    class LineItemChoiceClause : public LineItemClause {
    public:
        LineItemChoiceClause () {
            kind_ = k_line_clause_item_choice;}
        virtual ~LineItemChoiceClause() {}

        virtual void accept0(Visitor *visitor);
        //attributes
    };

    /*
     *  line_item_clause_list
     */
    class LineItemClauseList : public Node {
    public:

        LineItemClauseList( /*null clause*/ ) :
        line_item_clause_(NULL), next_(this) { 
            kind_ = k_line_item_clause_list;
        }

        LineItemClauseList( LineItemClause *item ) :
        line_item_clause_(item), next_(this) { 
            kind_ = k_line_item_clause_list; 
        }

        LineItemClauseList( LineItemClauseList *prev, LineItemClause *item ) :
        line_item_clause_(item) {
            kind_ = k_line_item_clause_list;
            next_ = prev->next_;
            prev->next_ = this;
        }

        LineItemClauseList* merge( LineItemClauseList *alt) 
        {
            LineItemClauseList *it = alt;
            while ( it->next_ ) {
                it = it->next_; 
            };
            LineItemClauseList *front = next_;
            it->next_ = front;
            next_ = 0;

            return alt;
        }

        virtual ~LineItemClauseList() {}

        virtual void accept0(Visitor *visitor);

        inline LineItemClauseList *finish() {
            LineItemClauseList *front = next_;
            next_ = 0;
            return front;
        }

        //atributes
        LineItemClause         *line_item_clause_;
        LineItemClauseList     *next_;
    };

    /*
     *  Group Definition Statement
     */
    class FieldDefStatement : public Statement {

    public:
        FieldDefStatement(Literal *name) : name_(name) {}
        FieldDefStatement(Literal *name, 
                          PicOrType *def,
                          LineItemClauseList *clauses = NULL) : 
        name_(name), def_(def), clause_list_(clauses) {
        }

        virtual void accept0(Visitor *visitor);

        Literal   *name_;
        PicOrType *def_;
        LineItemClauseList *clause_list_;
    };


    /*
     * Enum Definition Statement
     */
    class EnumDefStatement : public Statement {
    
    public:
      EnumDefStatement(Literal *name) : name_(name) {}
      EnumDefStatement(Literal *name, ConstraintList *cl) 
          : name_(name),constraint_list_(cl) {
        }

        virtual void accept0(Visitor *visitor);
  
        Literal *name_;
        ConstraintList *constraint_list_;
    };

    /*
     *  Group Definition Statement
     */
    class GroupDefStatement : public Statement {
    public:
      GroupDefStatement(Literal *name) : name_(name) {}
      GroupDefStatement(Literal *name, LineItemList *lil) 
          : name_(name),line_item_list_(lil) {
      }

      GroupDefStatement(Literal *name, GroupDefClauseList *cl, LineItemList *lil) : 
        name_(name),clause_list_(cl), line_item_list_(lil) {
      }


        virtual void accept0(Visitor *visitor);

        Literal *name_;
        GroupDefClauseList *clause_list_;
        LineItemList *line_item_list_;
    };

    /*
     *  Record Definition Statement
     */
    class RecordDefStatement : public Statement {

    public:
        RecordDefStatement(Literal *name, Literal *rec_ref, 
                       LineItemKeyIsClauseList *kil)
        : name_(name), rec_ref_(rec_ref), line_item_list_(NULL),
            keys_(kil) { Q_ASSERT(rec_ref); }

        RecordDefStatement(Literal *name, LineItemList *lil, 
                       LineItemKeyIsClauseList *kil) : 
        name_(name),rec_ref_(NULL),line_item_list_(lil), keys_(kil) {}

        virtual void accept0(Visitor *visitor);
    
        Literal *name_;
        Literal *rec_ref_;
        LineItemList *line_item_list_;
        LineItemKeyIsClauseList *keys_;
    };

} //namespace AST



#endif //DDL_AST_H

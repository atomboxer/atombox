#include <QFile>
#include <QtCore/QList>

#include "ddlengine.h"
#include "generatedgrammar_p.h"

#include "astparser.h"
#include "csvparser.h"

#include "boxcomponent.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

extern void * ddl_scan_string (const char *text);

DDLEngine* DDLEngine::instance_ = 0;

DDLEngine* 
DDLEngine::instance()
{
    if ( !instance_ ) {
        instance_ = new DDLEngine();
    }
    return instance_;
}

DDLEngine::DDLEngine(QObject *parent)/*
                     : QObject(parent)*/
{ 
   astparser_ = new DDLASTParser();
   csvparser_ = new MDBCSVParser();
}

DDLEngine ::~DDLEngine()
{
    delete astparser_;
    delete csvparser_;
}

QPair <QList<BoxComponent *>, QList< QPair < QString, QVariant > > >
DDLEngine::evaluate_ddl(const QString &text, const QString fileName)
{
    QList<BoxComponent*> definitions;
    QList< QPair < QString, QVariant> >       constants;

    if (!astparser_->parse(text, fileName)) {
        return QPair <QList<BoxComponent *>, QList< QPair < QString, QVariant> > > 
                  (definitions, constants);
    }

    //DDL Code Model to AtomBox Code Model
    definitions = astparser_->model()->dumpAtomBoxModel();
    constants   = astparser_->model()->dumpConstants();


    //astparser_->model()->dump();

    return QPair <QList<BoxComponent *>, QList< QPair < QString, QVariant> > >
                  (definitions, constants);
}

QList<BoxComponent *> 
DDLEngine::evaluate_mdb(const QString &text, const QString fileName)
{
    QList<BoxComponent*> definitions;

    csvparser_->parse(text, fileName);

    //DDL Code Model to AtomBox Code Model
    definitions = csvparser_->model()->dumpAtomBoxModel();

    //csvparser_->model()->dump();

    return definitions;
}


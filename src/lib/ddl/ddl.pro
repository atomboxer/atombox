TEMPLATE = lib

isEmpty(ATOMBOXER_PRI_INCLUDED) {
  include(../../../atomboxer.pri)
}

QT += core

isEmpty(USE_SCRIPT_CLASSIC) {
     QT	     += script
}

DESTDIR = $$ATOMBOXER_SOURCE_TREE/lib/

CONFIG += static

HEADERS += ast.h \
           astparser.h \
           csvparser.h \
           astprocessor.h \
           astvisitor.h \
           codemodel.h \
           ddlengine.h \
           error.h \
           generatedgrammar_p.h \
           generatedparser.h \
           itembase.h \
           itemconstant.h \
           itemcontainer.h \
           itemenumdefinition.h \
           itemfielddefinition.h \
           itemgroupdefinition.h \
           itemlinedefinition.h

SOURCES += ast.cpp \
           astparser.cpp \
           csvparser.cpp \
           astprocessor.cpp \
           astvisitor.cpp \
           codemodel.cpp \
           ddlengine.cpp \
           error.cpp \
           generatedgrammar.cpp \
           generatedparser.cpp \
           itembase.cpp \
           itemconstant.cpp \
           itemcontainer.cpp \
           itemenumdefinition.cpp \
           itemfielddefinition.cpp \
           itemgroupdefinition.cpp \
           itemlinedefinition.cpp \
           ddl_lex.cpp 


#LEXSOURCES  += ddl.l
#QMAKE_EXT_CPP += .ddl.c


LIBS       += -L$$ATOMBOXER_SOURCE_TREE/lib/ -latombox

INCLUDEPATH += .

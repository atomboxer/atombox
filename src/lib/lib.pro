isEmpty(ATOMBOXER_PRI_INCLUDED) {
    include(../../atomboxer.pri)
}

TEMPLATE  = subdirs

!isEmpty(ATOMBOXER_BUILD_DDL) {
    SUBDIRS   =  \
             atombox \
             ddl
}

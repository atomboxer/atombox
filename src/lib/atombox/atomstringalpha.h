#ifndef ATOM_STRING_ALPHA
#define ATOM_STRING_ALPHA

#include <QtCore/QString>
#include <QtCore/QIODevice>
#include <QtCore/QByteArray>

#include "atomstring.h"

class AtomStringAlpha : public AtomString
{
    Q_OBJECT
public:
    AtomStringAlpha();
    AtomStringAlpha( QString name, Definition &d,
                     Charset c = ASCII );
    ~AtomStringAlpha();

    virtual void clear();
    virtual BoxComponent* clone();

    virtual bool pack( QDataStream *ds );
    virtual QByteArray unpack();
    virtual void resetBytes();
    virtual void resize(size_t new_size);
    virtual bool setValue(QVariant val, bool emit_changed_flg);

private:
    Q_DISABLE_COPY(AtomStringAlpha)
};

#endif //ATOM_STRING_ALPHA

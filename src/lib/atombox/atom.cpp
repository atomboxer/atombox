#include <QtCore/QBuffer>
#include <QtCore/QDebug>
#include <QtCore/QString>
#include <QtCore/QList>

#include <QtCore/QDataStream>
#include <QtCore/QStringList>

#include "atom.h"
#include "atomclass.h"
#include "atomstring.h"

#include <iostream>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


//#define ATOM_DEBUG

Atom::Atom(QString name) :
    BoxComponent(name),
    bytes_(),
    default_(QVariant()),
    constraints_(),
    length_(0),
    length_type_(UNDEFINED),
    lpadc_(0),
    rpadc_(0),
    value_(QVariant()),
    _vlength_dependent_(NULL),
    _vlength_dependee_(NULL)
{
    if (!isSigned()) {
        if (!default_.canConvert( QVariant::ULongLong)) {
            qWarning(("negative DEFAULT value for unsigned atom:"+name).toLatin1().data()); 
        }
    }
}

Atom::Atom(QString name, Definition &definition) :
    BoxComponent(name),
    bytes_(),
    default_(definition.default_),
    definition_(definition),
    constraints_(),
    length_(definition.length_),
    length_type_(definition.length_type_),
    lpadc_(definition.lpadc_),
    rpadc_(definition.rpadc_),
    value_(QVariant()),
    _vlength_dependent_(NULL),
    _vlength_dependee_(NULL)
{
    display_ = definition.display_;
}


Atom::~Atom()
{
    //qDebug()<<"Atom::~Atom()"<<name();
}

QByteArray
Atom::bytes()
{
    QByteArray ret;

    if (!isArray())
        ret = bytes_;
    else {
        QList<BoxComponent *> occ = occurences();
        for (size_t i=0;i<occurencesNumber();i++) {
            ret.push_back(((Atom*)occ.at(i))->bytes_);
        }
    }
    return ret;
}

void
Atom::clear()
{
    resetState();
    
    if (isString()) {
        if (lengthType() == VARIABLE_LENGTH) {
            if (vDependent()) {
                if (haveDefault()) {
                    length_ = defaultValue().toString().size();
                } else {
                    length_ = vDependent()->value().toLongLong();
                }
            }
        }

        if (lengthType() == VARIABLE_LENGTH_INCLUSIVE) {
            if (vDependent()) {
                if (haveDefault()) {
                    length_ = defaultValue().toString().size();
                } else {
                    length_ = vDependent()->value().toLongLong() -
                        vDependent()->length();
                }
            }
        }

        if (lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE) {
            if (vDependent()) {
                if (haveDefault()) {
                    length_ = defaultValue().toString().size();
                } else {
                    length_ = vDependent()->value().toLongLong() - 
                        (parent()->length() - length());
                }
            }
        }

        if (lengthType() == VARIABLE_DELIMITED && haveDefault()) {
            length_ = defaultValue().toString().size();          
        }
    }

    if (vDependee() &&  !vDependee()->haveDefault()) {
        if (vDependee()->lengthType() == VARIABLE_LENGTH)
            vDependee()->setLength(value().toLongLong());
    
        if (vDependee()->lengthType() == VARIABLE_LENGTH_INCLUSIVE)
            vDependee()->setLength(value().toLongLong() - length());
        
        if (vDependee()->lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE)
            vDependee()->setLength(value().toLongLong() - (parent()->length() - length()));
    }
}

void
Atom::connectDependents() 
{
    //Invoke the base class
    BoxComponent::connectDependents();

    if (vDependent()) {
#ifdef ATOM_DEBUG
        qDebug()<<"connect::"<<this->name()<<" -> "<<vDependent()->name();
#endif
        QObject::connect(this, SIGNAL(changed(BoxComponent*)),
                         vDependent(), SLOT(vDependeeChanged(BoxComponent*)),
                         Qt::DirectConnection);
    }

    if (vDependee()) {
#ifdef ATOM_DEBUG
        qDebug()<<"connect::"<<this->name()<<" -> "<<vDependee()->name();
#endif
        QObject::connect(this, SIGNAL(changed(BoxComponent*)),
                         vDependee(), 
                         SLOT(vDependentChanged(BoxComponent*)),
                         Qt::DirectConnection);
    }
}

size_t
Atom::computevLength() const 
{
    Atom *c = vDependent();
    if (!c) {
        return -1;
    }

    size_t ret = -1;

    if (length_type_ == VARIABLE_LENGTH) {
        bool ok = false;

        if (c->isHexPacked())
            ret =  c->value().toByteArray().toLongLong(&ok,16);
        else
            ret = c->value().toByteArray().toLongLong(&ok);

        if (!ok) {
            return -1;
        }

        return ret;

    } else if (length_type_ == VARIABLE_LENGTH_INCLUSIVE){
        bool ok = false;

        if (c->isHexPacked())
            ret =  c->value().toByteArray().toLongLong(&ok,16);
        else
            ret = c->value().toByteArray().toLongLong(&ok);

        ret -= c->length();

        if (!ok) {
            return -1;
        }

        return ret;

    } else if (length_type_ == VARIABLE_LENGTH_ALLINCLUSIVE){
        bool ok = false;
        if (c->isHexPacked())
            ret =  c->value().toByteArray().toLongLong(&ok,16);
        else
            ret = c->value().toByteArray().toLongLong(&ok);

        ret -= (parent()->length() - c->length());
        
        if (ret%2 == 1)
            ret+=1;

        if (!ok) {
            return -1;
        }

        return ret;

    } else {
        return length_;
    }
}

Atom* 
Atom::vDependent() const
{
    return _vlength_dependent_;
}

Atom* 
Atom::vDependee() const
{
    return _vlength_dependee_;
}

void 
Atom::setvDependent(Atom *dependent)
{     
    Q_ASSERT(dependent);
    _vlength_dependent_ = dependent; 
}

void
Atom::setvDependee(Atom *dependee) 
{ 
    Q_ASSERT(dependee);
    _vlength_dependee_ = dependee; 
}

void
Atom::resetvDependencies()
{
    bool found = false;
    if (parent()) {
        BoxComponent *p = parent();

        while(!found && p!= NULL && p != p->parent()) {
            foreach(BoxComponent *c, p->childs()) {
                if (c->isAtom() &&
                    (c->name() == _vlength_dependent_->name())) {
                    ((Atom*)c)->setvDependee(this);
                    _vlength_dependent_ = (Atom*)c;
                    return;
                }

                if (c->hasRedefines()) {
                    foreach (BoxComponent *r, c->redefines()) {
                        if (r->isAtom() &&
                            (r->name() == _vlength_dependent_->name())) {
                            ((Atom*)c)->setvDependee(this);
                            _vlength_dependent_ = (Atom*)r;
                            return;
                        } //if
                    }    
                }
                if (p  && p->parent())
                    p = p->parent();
                else
                    p = NULL;

            } //foreach
        } //while
    }

    qFatal("Unable to find the VLENGTH dependant for :%s",name().toLatin1().data());
}

void
Atom::vDependeeChanged(BoxComponent *c)
{
    Q_ASSERT(this->isNumeric());
    Q_ASSERT(c->isAtom());

#ifdef ATOM_DEBUG
    qDebug()<<"Atom::vDependeeChanged for:"<<name();
#endif
 
    AtomString *d = dynamic_cast<AtomString*>(c);

    if (!d) 
        qFatal("Atom::vDependeeChanged programmatic error");

    if (d->lengthType() == VARIABLE_LENGTH) {
        if (d->length() != value().toLongLong()) {
            setValue(qMin<int>(d->maxLength(), d->length()), true);
        }
    } else if (d->lengthType() == VARIABLE_LENGTH_INCLUSIVE) {
        if (d->length() + length () != value().toLongLong()) {
            setValue(qMin<int>(d->maxLength(), d->length()+length()), true);
        }

    } else if (d->lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE) {
        if (d->length() + (parent()->length() - d->length()) != value().toLongLong()) {
            setValue(qMin<int>(d->maxLength(), d->length() + (parent()->length() - d->length())), true);
        }
    }

}

void
Atom::vDependentChanged(BoxComponent *c)
{
    Q_ASSERT(lengthType() == VARIABLE_LENGTH ||
             lengthType() == VARIABLE_LENGTH_INCLUSIVE ||
             lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE);

    Q_ASSERT(c!=NULL);

    ssize_t dep_length = 0;
    bool ok;

    Atom *dc = dynamic_cast<Atom*>(c);
    Q_ASSERT(dc);

    if (vDependent()->isHexPacked()) {
        dep_length = dc->value().toString().toLongLong(&ok,16);
        Q_ASSERT(ok);
    } else {
        dep_length = dc->value().toLongLong();
    }

#ifdef ATOM_DEBUG
    qDebug()<<"Atom::vDependentChanged for:"<<name()<<"::length::"<<length()<<"::dep_length::"<<dep_length;
#endif    

    if (lengthType() == VARIABLE_LENGTH) {
        if (dep_length != length()) {
            AtomString *d = dynamic_cast<AtomString*>(this);
            d->resize(dep_length);
            d->emitChanged();
        }
    } else if (lengthType() == VARIABLE_LENGTH_INCLUSIVE) {
        if ( dep_length - c->length() != length()  ) {
            AtomString *d = dynamic_cast<AtomString*>(this);
            d->resize(dep_length - dc->length());
            d->emitChanged();
        }
    } else if (lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE) {
        if ( dep_length - (parent()->length() - length()) != length()  ) {
            AtomString *d = dynamic_cast<AtomString*>(this);
            d->resize(dep_length - (parent()->length() - length()));
            d->emitChanged();
        }
    }
}

void
Atom::reindex()
{
    ;//nothing
}

void
Atom::setDefault(QVariant _default)
{
    if (!isSigned()) {
        if (!_default.canConvert( QVariant::ULongLong)) {
            qWarning(("negative DEFAULT value for unsigned atom:"+name()).toLatin1().data()); 
        }
    }

    default_ = _default;
    definition_.default_flg_ = true;
}

void
Atom::dump( ushort levels ) const
{
    std::cout<<name().toLatin1().data()<<"\t:\t"
             <<value().toString().toLatin1().data()<<"\n";
}

QVariant
Atom::value() const
{
    return value_;
}

bool
Atom::changeValue(QVariant val)
{
    setValue(val, true);
    return true;
}

void 
Atom::setByteOrder(ByteOrder o)
{
    if (hasRedefines()) {
        foreach (BoxComponent *r, redefines())
            r->setByteOrder(o);
    }
}

bool
Atom::setValue(QVariant val, bool emit_changed_flg)
{
    Q_ASSERT(0);
    return false;
}

ssize_t
Atom::length() const
{    
    ssize_t ret = 0;

    if (isArray()) {
        ret = (occurences().size()) * length_;
    } else {
        ret =  length_;
    }    

    return ret;
}

void
Atom::setLength( ssize_t length )
{
    length_ = length;
}

// void
// Atom::setBytes(QByteArray bytes)
// {

//     bytes_ = bytes;
// }

/*
  void
  Atom::exportJS( QScriptEngine *engine, QScriptValue &obj, bool clone )
  {
  AtomClass *atom;
  QScriptValue ns;


  atom = new AtomClass(engine, this);

  ns = atom->constructor();
  obj.setProperty(name(), ns);
  setNS(ns);
  }
*/

Constraint*
Atom::findConstraintByValue( QVariant v ) const
{
    foreach (Constraint *c, constraints()) {
        if (c->isMatch(v)) {
            return c;
        }
    }
    return NULL;
}

QString 
Atom::toJSON( bool use_redefines )
{
    QString txt;
    QStringList sl;

    if (isNumeric()) {
        if (isArray()) {
            QString v("[");
            QStringList vl;
            foreach (BoxComponent *occ, occurences()) {
                if (occ != this) {
                    vl << QString::number(((Atom*)(occ))->value().toLongLong());
                }
            }

            v+= vl.join(",");
            v+="]";
            sl << "\""+name()+"\""+":"+v;
        } else {
            sl << "\""+name()+"\""+":"+QString::number(value().toLongLong());
        }
    } else {
        bool all_print = true;
        QByteArray b = bytes();
        for (int i=0;i<b.size();i++) {
            if (!QChar(b[i]).isPrint()) {
                all_print = false;
                break;
            }
        }
        if (all_print) {
            if (isArray()) {
                QStringList vl;
                foreach (BoxComponent *occ, occurences()) {
                    if (occ != this) {
                        vl << ((Atom*)(occ))->value()
                            .toString().replace("\\","\\\\")
                            .replace("\"","\\\"");
                    }
                }
                QString v("[");
                for (int i=0;i<vl.length();i++) {
                    vl[i]="\""+vl[i]+"\"";
                }

                v+= vl.join(",");
                v+="]";
                sl << "\""+name()+"\""+":"+v;
            } else {
                QString v("\""
                    +value().toString().replace("\\","\\\\").
                          replace("\"","\\\"") +"\"");

                sl << "\""+name()+"\""+":"+v;
            }
        } else { 
            if (isArray()) {
                QString v("[");
                QStringList vl;
                foreach (BoxComponent *occ, occurences()) {
                    if (occ != this) {
                        vl << "\""+("0x"+(occ->bytes().toHex()+"\""));
                    }
                }

                v+= vl.join(",");
                v+="]";
                sl << "\""+name()+"\""+":"+v;
            } else {
                sl << "\""+name()+"\""+":"+"\""+"0x"+bytes().toHex()+"\"";
            }
        }
    }
    
    if (use_redefines) {
        foreach(BoxComponent *r, redefines()) {
            sl << r->toJSON(use_redefines);
        }
    }
    txt += sl.join(",");

    return txt;
}


#include <QtCore/QString>
#include <QtCore/QDebug>
#include <QtCore/QVariant>
#include <QtCore/QBuffer>

#include "atomhexpacked.h"
#include "atomclass.h"
#include "atomstring.h"

#include "utils.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


AtomHexPacked::AtomHexPacked() :
    Atom("undefined")
{
    if (lpad() != 0 || rpad() != 0) {
        qFatal("Fatal error. LPAD/RPAD clause with PACKED DECIMAL atom.");
    }

    value_ = QVariant();
}


AtomHexPacked::AtomHexPacked( QString name,
                              Definition &definition )
    : Atom(name, definition)
{
    if (lpad() != 0 || rpad() != 0) {
        qFatal("Fatal error. LPAD/RPAD clause with PACKED DECIMAL atom.");
    }

    value_ = QVariant();
}

AtomHexPacked::~AtomHexPacked()
{

}

void
AtomHexPacked::clear()
{
    Atom::clear();

    QString v(length(), ' ');

    if (haveDefault()) {
        v = defaultValue().toString();
    }

    if (lpad() != 0) {
        if (lengthType() != VARIABLE_LENGTH &&
            lengthType() != VARIABLE_LENGTH_INCLUSIVE &&
            lengthType() != VARIABLE_LENGTH_ALLINCLUSIVE)
            v = v.rightJustified(length(),lpad());
    } else if (rpad() != 0) {
        if (lengthType() != VARIABLE_LENGTH &&
            lengthType() != VARIABLE_LENGTH_INCLUSIVE &&
            lengthType() != VARIABLE_LENGTH_ALLINCLUSIVE)
            v = v.leftJustified(length(),rpad());
    }

    setValue(v, true); //most cases
    if (state() == OK_State)
        setDefaultState();
}

BoxComponent *
AtomHexPacked::clone()
{
    AtomHexPacked *cc = new AtomHexPacked(name(), definition_);
    cc->setParent(parent());
    if (vDependent())
        cc->setvDependent(vDependent());
    if (vDependee())
        cc->setvDependee(vDependee());
    cc->setPrevSibling(prevSibling());
    cc->setNextSibling(nextSibling());
    cc->setRedefine(isRedefine());
    cc->setConstraintList(constraints());
    cc->setState(state());
    cc->setKey(isKey());
   
    if (isArray()) {
        cc->setOccurencesNumber(occurencesNumber());
        cc->setOccurencesNumber(occurencesDepend(),
                                 occurencesDependFrom(),
                                 occurencesDependTo());
        //cc->buildOccurences();
    }
    
    foreach (BoxComponent *r, redefines()) {
        BoxComponent *rc = r->clone();
        cc->addRedefine(rc);
    }

    return cc;
}

bool
AtomHexPacked::pack( QDataStream *ds )
{
    Q_ASSERT(ds != NULL);
    setState(OK_State);
    
    if (length_type_ == VARIABLE_LENGTH ||
        length_type_ == VARIABLE_LENGTH_INCLUSIVE ||
        length_type_ == VARIABLE_LENGTH_ALLINCLUSIVE) {
        
        length_ = computevLength();

        if (length_ < 0) {
            setErrorState("Invalid Length. Packing suspended");
            return false;
        } else if (length_ == 0)
            return true;
    }


    size_t bin_len = (length()/2) ;

    char *buf = new char[bin_len+1];
    bool  has_fill = (length()%2 == 0)?false:true;

    if (has_fill) {
        size_t rl = ds->readRawData(buf, bin_len+1);
        if (rl != bin_len+1) {
            delete []buf;
            setErrorState("Not enough data.Expecting "+QString::number(bin_len)+" Packing suspended");
            return false;
        }

        setValue(QByteArray(buf, bin_len+1).toHex(), false);
    } else {
        size_t rl = ds->readRawData(buf, bin_len);
        if (rl != bin_len) {
            delete[]buf;
            qDebug()<<"rl"<<rl;
            setErrorState("Not enough data.Expecting "+QString::number(bin_len)+" Packing suspended");
            return false;
        }
        setValue(QByteArray(buf, bin_len).toHex(), false);
    }

    delete []buf;

    emit packed(this);
    if (constraints().isEmpty()) {
        return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints())
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }

    if (!c_ok) {
        setExceptionState("Value does not match constraints");
    }

    // qDebug()<<"HEX MATCHED:"<<c_ok<<" n::"<<name().toLatin1().data()
    //              <<"::\""<<value().toString().toLatin1().data()<<"\""
    //              <<"::\""<<QString::fromLatin1(bytes().toHex())<<"\""
    //              <<"::lgth::"<<length();

    return false;
}

QByteArray
AtomHexPacked::unpack()
{
    return bytes();
}

void
AtomHexPacked::resetBytes()
{
    if (length() == 0 || value_.isNull() || !value_.isValid()) {
        bytes_ = QByteArray();
        length_ = 0;
        return;
    }

    bytes_ = QByteArray::fromHex(value().toString().toAscii());
}

bool
AtomHexPacked::setValue(QVariant val, bool emit_changed_flg)
{
    //resetState();

    if (val.isValid()) {
    
        size_t bin_length = length();
       
        if ( bin_length%2 )
            bin_length+=1;

        QString value_s(bin_length,'0');
        QString v = val.toString();
        value_s = value_s.replace(bin_length - v.length(),v.length(), v);

        //
        //  Seting the value
        //
        value_ = value_s;
        if (lengthType() == VARIABLE_LENGTH ||
            lengthType() == VARIABLE_LENGTH_INCLUSIVE ||
            lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE ||
            lengthType() == VARIABLE_DELIMITED ) {
            length_ = value_s.size();
        }
       
        setOKState();

        resetBytes();
        if (emit_changed_flg)
            emit changed(this);

    } else {
        setErrorState("Invalid value in set method");
        return false;
    }

    if (constraints().isEmpty()) {
        return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints()) {
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }
    }

    if (!c_ok) {
        setExceptionState("Value does not match constraints");
        return false;
    }
    return false;
}

#include <QtCore/QList>

#include "constraintlist.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

ConstraintList::ConstraintList() :
    Constraint("")
{
    kind_ = k_list;
}

ConstraintList::~ConstraintList()
{
    qDeleteAll(constraints_);
}


Constraint*
ConstraintList::clone()
{
    //ConstraintList * _clone = new ConstraintList();
    //_clone->kind_ = this->kind();
    //_clone->matched_value_ = this->matchedValue();

    //QList<Constraint*> list;
    //foreach (Constraint *c, this->constraints_) {
    //    list.push_back(c->clone());
    //}

    //_clone->constraints_ = list;

    qFatal("programmatic error in ConstraintList::clone");
    return this;
}

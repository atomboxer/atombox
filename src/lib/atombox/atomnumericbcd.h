#ifndef ATOM_NUMERIC_BCD
#define ATOM_NUMERIC_BCD

#include <QtCore/QString>
#include <QtCore/QIODevice>
#include <QtCore/QByteArray>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptEngine>

#include "atom.h"

class AtomNumericBCD : public Atom
{
    Q_OBJECT
public:
    AtomNumericBCD();
    AtomNumericBCD(QString name, Definition &definition);

    ~AtomNumericBCD();

    virtual void clear();
    virtual BoxComponent* clone();
    virtual bool isString() const { return false; }
    virtual bool isStringNumeric() const { return true; }

    virtual bool pack( QDataStream *ds );
    virtual QByteArray unpack();
    virtual void resetBytes();
    virtual bool setValue( QVariant val, bool emit_changed_flg );

private:
    Q_DISABLE_COPY(AtomNumericBCD)
};

#endif //ATOM_NUMERIC_BCD

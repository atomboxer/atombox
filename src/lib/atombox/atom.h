#ifndef ATOM_H
#define ATOM_H

#include <QtCore/QObject>
#include <QtCore/QVariant>
#include <QtCore/QDebug>

#include "boxcomponent.h"
#include "constraint.h"

#include "definition.h"

/*
 *  Implements Atom("Leaf") within the Composite Pattern
 */
class Atom : public BoxComponent
{
    Q_OBJECT

public:
    Atom(QString name = "undefined");
    Atom(QString name, Definition &definition);

    virtual ~Atom();

    virtual QByteArray bytes();

    virtual void clear();
    QList<BoxComponent*> childs() const { return QList<BoxComponent*>(); }

    virtual void reindex();

    virtual Atom* vDependent() const;
    virtual Atom* vDependee() const;
    void setvDependent(Atom *dependent);
    void setvDependee(Atom *dependee);
    void resetvDependencies();
    virtual void connectDependents();
    virtual size_t computevLength() const;
    virtual QVariant defaultValue() const { return default_; }
    virtual void dump(ushort max_levels = 1) const;
    virtual bool haveDefault() const { return definition_.default_flg_; }
    virtual QVariant value() const;
    virtual void setByteOrder(ByteOrder o);
    void setDefault(QVariant _default);
    void setLpad(QChar c) { lpadc_ = c; }
    void setRawBytes(QByteArray bytes);
    void setRpad(QChar c) { rpadc_ = c; }
    virtual ssize_t length() const;
    virtual void setLength(ssize_t length);
    virtual QChar lpad() const { return lpadc_; }
    virtual QChar rpad() const { return rpadc_; }
    virtual bool isAtom() const  { return true; }
    virtual bool isBinary() const { return false; }
    virtual bool isBitmap() const { return false;} 
    virtual bool isIsoBitmap() const { return false; }
    virtual bool isHexPacked() const { return false; }
    virtual bool isString() const { return false; }
    virtual bool isStringNumeric() const { return false; }
    virtual bool isNumeric() const { if (isBinary() || isBitmap() || isIsoBitmap() || isStringNumeric()) return true; else return false;}
    virtual bool isSigned() const { return false; }
    virtual AtomLengthType lengthType() const { return length_type_; }
    virtual void setConstraintList(QList< Constraint *> c) { constraints_ = c; }
    virtual QList<Constraint *> constraints() const { return constraints_; }
    virtual Constraint* findConstraintByValue( QVariant v ) const;
public: //abstract
    virtual bool pack( QDataStream *d ) = 0;
    virtual QByteArray unpack() = 0;
    virtual void resetBytes() = 0;

    virtual bool changeValue(QVariant v);
    virtual void setValueValue(QVariant value) { value_ = value; }
    virtual bool setValue(QVariant value, bool emit_changed_flg);
    virtual QString toJSON( bool use_redefines = false );

public slots:
    virtual void vDependeeChanged(BoxComponent *c);
    virtual void vDependentChanged(BoxComponent *c);

protected:
    QByteArray bytes_;
    QVariant   default_;
    Definition definition_;
    QList< Constraint * > constraints_;
    size_t length_;
    AtomLengthType length_type_;
    QChar lpadc_;
    QChar rpadc_;
    QVariant value_;
    Atom *_vlength_dependent_;
    Atom *_vlength_dependee_;

private:
    Q_DISABLE_COPY(Atom)
};

#endif // ATOM_H

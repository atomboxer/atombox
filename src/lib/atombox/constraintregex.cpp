#include "constraintregex.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

ConstraintRegex::ConstraintRegex( QString value, QString &display )
    : Constraint (display), value_(value)
{
    kind_ = k_regex;
}

ConstraintRegex::~ConstraintRegex()
{}

bool
ConstraintRegex::isMatch(QVariant v)
{
    QString sval = v.toString();

    return value_.exactMatch(sval);
}

Constraint*
ConstraintRegex::clone()
{
    ConstraintRegex * _clone = new ConstraintRegex(this->value_.pattern(), this->display_);
    _clone->kind_ = this->kind();
    _clone->matched_value_ = this->matchedValue();
    return _clone;
}

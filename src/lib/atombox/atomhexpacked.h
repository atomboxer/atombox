#ifndef ATOM_HEX_PACKED
#define ATOM_HEX_PACKED

#include <QtCore/QString>
#include <QtCore/QIODevice>
#include <QtCore/QByteArray>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptEngine>

#include "atom.h"

class AtomHexPacked : public Atom
{
    Q_OBJECT
public:
    AtomHexPacked();
    AtomHexPacked(QString name, Definition &definition);

    ~AtomHexPacked();

    virtual void clear();
    virtual BoxComponent* clone();
    virtual bool isString() const { return true; }
    virtual bool isHexPacked() const { return true; }

    virtual bool pack( QDataStream *ds );
    virtual QByteArray unpack();
    virtual void resetBytes();
    virtual bool setValue( QVariant val, bool emit_changed_flg );

private:
    Q_DISABLE_COPY(AtomHexPacked)

};

#endif //ATOM_HEX_PACKED

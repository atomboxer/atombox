#ifndef CONSTRAINT_REGEX
#define CONSTRAINT_REGEX

#include <QtCore/QRegExp>
#include "constraint.h"

class ConstraintRegex : public Constraint
{
public:
    ConstraintRegex(QString regex, QString &display);
    virtual ~ConstraintRegex();
    virtual bool isMatch(QVariant v);
    virtual Constraint* clone();
private:
    QRegExp value_;
};
#endif

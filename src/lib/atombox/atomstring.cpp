#include <QtCore/QBuffer>
#include <QtCore/QDebug>
#include <QtCore/QString>
#include <QtCore/QVariant>

#include "atomstring.h"
#include "atomclass.h"

#include <iostream>
#include <assert.h>

#include "utils.h"
#include "constraint.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define ATOMSTRING_DEBUG

AtomString::AtomString() :
    Atom("undefined")
{
    //clear();
}

AtomString::AtomString(QString name, Definition &definition, Charset c)
    : Atom(name, definition),
      vlength_delimiter_(definition.vlength_delimiter_)
{
    charset_ = c;
    max_length_ = length_;

    //
    //  String atoms are right padded with spaces
    //
    if (rpad() == 0 && lpad() == 0) {
        setRpad(' ');
    }
    //clear();
}


AtomString::~AtomString()
{
}

BoxComponent *
AtomString::clone()
{
    AtomString *cc = new AtomString(name(), definition_, charset_); 
    cc->setParent(parent()); 
    if (vDependent())
        cc->setvDependent(vDependent());
    if (vDependee())
        cc->setvDependee(vDependee());
    cc->setPrevSibling(prevSibling()); 
    cc->setNextSibling(nextSibling());
    cc->setRedefine(isRedefine());
    cc->setConstraintList(constraints());
    cc->setState(state());
    cc->setKey(isKey());
   
    if (isArray()) {
        cc->setOccurencesNumber(occurencesNumber());
        cc->setOccurencesNumber(occurencesDepend(),
                                 occurencesDependFrom(),
                                 occurencesDependTo());
        //cc->buildOccurences();
    }

    foreach (BoxComponent *r, redefines()) {
        BoxComponent *rc = r->clone();
        cc->addRedefine(rc);
    }

    return cc;
}

void
AtomString::clear()
{
    Atom::clear();

#ifdef ATOMSTRING_DEBUG
    qDebug()<<"AtomString::clear::"<<name();
#endif

    QString v(length(), ' ');

    if (haveDefault()) {
        v = defaultValue().toByteArray();
    }

    if (lpad() != 0) {
        if (lengthType() != VARIABLE_LENGTH &&
            lengthType() != VARIABLE_LENGTH_INCLUSIVE &&
            lengthType() != VARIABLE_LENGTH_ALLINCLUSIVE)
            v = v.rightJustified(length(),lpad());
    } else if (rpad() != 0){
        if (lengthType() != VARIABLE_LENGTH &&
            lengthType() != VARIABLE_LENGTH_INCLUSIVE &&
            lengthType() != VARIABLE_LENGTH_ALLINCLUSIVE)
            v = v.leftJustified(length(),rpad());
    }

    setValue(v, true);
    if (state() == OK_State)
        setDefaultState();
    
}

bool
AtomString::pack( QDataStream *ds )
{
    Q_ASSERT(ds!=NULL);
    setState(OK_State);
    
    char  *buf = NULL;
    ssize_t rl = 0;

    if ( length_type_ == VARIABLE_LENGTH ||
         length_type_ == VARIABLE_LENGTH_INCLUSIVE ||
         length_type_ == VARIABLE_LENGTH_ALLINCLUSIVE ) {

        length_ = computevLength();
        
        
        if (length_ == 0 ) {
            return true;
        }
        
        if (length() < 0 || length() > 65535) {
           setErrorState("Invalid VARIABLE length");
           return false;
        }

        buf = new char[length()+1];

        Q_ASSERT(length() > 0);
      
        rl = ds->readRawData(buf, length());

        if (rl < length()) {
            setErrorState("Not enough data. Packing suspended");
            delete[]buf;
            return false;
        }

        buf[rl]='\0';
    } else if (length_type_ == VARIABLE_DELIMITED) {
        QIODevice *d = ds->device();
        Q_ASSERT( d != NULL);
        size_t pos = d->pos();
        qint8 c;
        ssize_t c_length = 0;
        bool found = false;

        while ( !found && !d->atEnd()) {
            *ds >> c;
            c_length++;
            if (QChar(c) == vlength_delimiter_) {
                found = true;           
                break;
            }
        }

        d->seek(pos); //go back to the begining now

        buf = new char[c_length];
        rl = ds->readRawData(buf, c_length);
        if (rl < c_length) {
            setErrorState("Not enough data. Packing suspended");
            delete[]buf;
            return false;
        }

        if (buf[rl - 1] == vlength_delimiter_)
            rl--;


        length_ = rl; //set the new length
    } else {
        buf = new char[length()+1];
        Q_ASSERT(length() > 0);
        rl = ds->readRawData(buf, length());
        if (rl < length()) {
            bytes_ = QByteArray((const char*)buf,rl).append(QByteArray(length()-rl,' '));
            
            if ( charset_ == AtomString::EBCDIC ) {
                EBCDIC2ASCII(buf,length());
            }

            //
            //  Set the value
            //
            value_ = QByteArray((const char*)bytes_, length());
            delete []buf;

            setErrorState("Not enough data. Packing suspended");

            //
            //  Emit packed()
            //
            emit packed(this);

            return false;
        }
//        buf[rl]='\0';
    }
    
    // if (rl == 0 || buf == 0) {
    //     delete []buf;
    //     setErrorState("Empty atom");
    //     return false;
    // }

    //
    //  Set the bytes. (avoid using setValue()/setBytes() for charset related
    //  performance reasons)
    //
    bytes_ = QByteArray((const char*)buf,rl);

    if ( charset_ == AtomString::EBCDIC ) {
        EBCDIC2ASCII(buf,length());
    }

    //
    //  Set the value
    //
    value_ = QByteArray((const char*)buf, length());
    delete []buf;

    emit packed(this);

    if (constraints().isEmpty()) {
         // std::cout <<"a::"<<name().toLatin1().data()
         //     <<"::\""<<value().toString().toLatin1().data()<<"\""
         //     <<"::\""<<QString::fromLatin1(bytes().toHex()).toLatin1().data()<<"\""
         //     <<"::lgth::"<<length()<<std::endl;
        return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints()) {
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }
    }

    if (!c_ok) {
        // qDebug()<<"a::"<<name().toLatin1().data()
        //  <<"::\""<<value().toString().toLatin1().data()<<"\""
        //  <<"::\""<<QString::fromLatin1(bytes().toHex())<<"\""
        //  <<"::lgth::"<<length();
        setExceptionState("Value does not match constraints");
    }

    return false;
}

QByteArray
AtomString::unpack()
{
    QByteArray ret(bytes());
    if (length_type_ == VARIABLE_DELIMITED)
        if (nextSibling())
            ret.append(vlength_delimiter_);

    if ( charset_ == AtomString::EBCDIC ) {
        ASCII2EBCDIC(ret.data(),ret.length());
    }

    return ret;
}

void
AtomString::resetBytes()
{

    if (length() == 0 || value_.isNull() || !value_.isValid() ||
        !value_.canConvert(QVariant::ByteArray)) {
        bytes_ = QByteArray();
        return;
    }

    bytes_ = value().toByteArray();
    
    if ( charset_ == AtomString::EBCDIC ) {
        ASCII2EBCDIC(bytes_.data(), length());
    }    
}

void
AtomString::resize(size_t new_size)
{
    if (new_size <= 0) {
        length_ = 0;
        bytes_ = QByteArray();
        value_ = QVariant();
        return;
    }

    if (lengthType() == VARIABLE_LENGTH ||
        lengthType() == VARIABLE_LENGTH_INCLUSIVE ||
        lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE ||
        lengthType() == VARIABLE_DELIMITED) {
        length_ = new_size;
    }

    //FIXED with default
    QString v(new_size, ' ');
    v.replace(0, qMin<int>(bytes().length(), new_size),
              bytes().mid(0, qMin<int>(bytes().length(), new_size))); 


    if (lpad() != 0) {
        if (lengthType() != VARIABLE_LENGTH &&
            lengthType() != VARIABLE_LENGTH_INCLUSIVE &&
            lengthType() != VARIABLE_LENGTH_ALLINCLUSIVE)
            v = v.rightJustified(new_size, lpad());
    } else if (rpad() != 0) {
        if (lengthType() != VARIABLE_LENGTH &&
            lengthType() != VARIABLE_LENGTH_INCLUSIVE &&
            lengthType() != VARIABLE_LENGTH_ALLINCLUSIVE)
            v = v.leftJustified(new_size, rpad());
    }    

    value_ = v;
    bytes_ = value_.toByteArray();

    resetBytes();
}

bool
AtomString::setValue(QVariant val, bool emit_changed_flg)
{
    //resetState();

    if (val.isValid() &&
        val.canConvert(QVariant::ByteArray)) {

        QByteArray vval = val.toByteArray();
        if (vval.length() != length() && vval.length() < length()) {
            if (rpad() != 0)
                vval.append(QString(length() - vval.length(), rpad()));
            else if (lpad() != 0) 
                vval.prepend(QString(length()-vval.length(), lpad()).toLatin1().data());
        }

        if (lengthType() == VARIABLE_LENGTH ||
            lengthType() == VARIABLE_LENGTH_INCLUSIVE ||
            lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE ||
            lengthType() == VARIABLE_DELIMITED) {
            
            length_ = qMin<int>(maxLength(), val.toByteArray().size());
        }

        value_ = vval.mid(0,length());
        resetBytes();
        setOKState();
        
        if (emit_changed_flg) {
            emit changed(this);
        }
    } else {
        return false;
    }
 
    if (constraints().isEmpty()) {
        return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints()) {
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }
    }

    if (!c_ok) {
        setExceptionState("Value does not match constraints");
        return false;
    }
   
    return true;
}

QVariant
AtomString::value() const
{
    return value_.toByteArray();
}

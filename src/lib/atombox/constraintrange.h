#ifndef CONSTRAINT_RANGE
#define CONSTRAINT_RANGE

#include "constraint.h"

class ConstraintRange : public Constraint
{
public:
    ConstraintRange(int from, int to, QString &description);
    virtual ~ConstraintRange();
    virtual bool isMatch(QVariant v);
    virtual Constraint* clone();
private:
    int from_;
    int to_;
};
#endif

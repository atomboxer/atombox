#ifndef ATOM_STRING
#define ATOM_STRING

#include <QtCore/QString>
#include <QtCore/QIODevice>
#include <QtCore/QByteArray>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptEngine>

#include "atom.h"
#include "definition.h"

class AtomString : public Atom
{
    Q_OBJECT

public:
    enum Charset {
        ASCII,
        EBCDIC,
    };

    AtomString();
    AtomString(QString name, Definition &definition, Charset c = ASCII);
    ~AtomString();

    virtual Charset charset() const { return charset_; }
    virtual void clear();
    virtual BoxComponent* clone();

    virtual bool isString() const { return true; }

    size_t maxLength() const { return max_length_; }

    virtual bool pack( QDataStream *ds );
    virtual QByteArray unpack();
    virtual void resetBytes();
    virtual void resize(size_t new_size);
    virtual bool setValue(QVariant v, bool emit_changed_flg);
    virtual QVariant value() const;
    virtual void setCharset(Charset c) { charset_ = c; }

protected:
    Charset charset_;
    size_t  max_length_; //the length_ fluctuates for VLENGTH

protected:
    QChar   vlength_delimiter_;

private:
    Q_DISABLE_COPY(AtomString)

};

#endif //ATOM_STRING

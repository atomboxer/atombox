#ifndef CONSTRAINT_STRING
#define CONSTRAINT_STRING

#include <QtCore/QString>
#include "constraint.h"

class ConstraintString : public Constraint
{
  public:
    ConstraintString(QString string, QString &display);
    virtual ~ConstraintString();
    virtual bool isMatch(QVariant v);
    QString value() const { return value_; }
    virtual Constraint* clone();
private:
    QString value_;
};
#endif

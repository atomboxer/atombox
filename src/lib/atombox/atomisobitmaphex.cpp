
#include <QtEndian>
#include <QtCore/QString>
#include <QtCore/QDebug>
#include <QtCore/QVariant>

#include "atomisobitmaphex.h"
#include "atomclass.h"

#include <assert.h>
#include <iostream>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


AtomIsoBitmapHex::AtomIsoBitmapHex() :
    AtomIsoBitmap()
{
    clear();
}

AtomIsoBitmapHex::AtomIsoBitmapHex(QString name, Definition &definition)
    : AtomIsoBitmap(name, definition)
{
    //from parent
    bit_array_.resize((definition.length_ * 8)/2);
    length_ = definition.length_;
}

AtomIsoBitmapHex::~AtomIsoBitmapHex()
{
}

BoxComponent *
AtomIsoBitmapHex::clone()
{
    AtomIsoBitmapHex *cc = new AtomIsoBitmapHex(name(), definition_);
    cc->setParent(parent());
    if (vDependent())
        cc->setvDependent(vDependent());
    if (vDependee())
        cc->setvDependee(vDependee());
    cc->setPrevSibling(prevSibling());
    cc->setNextSibling(nextSibling());
    cc->setRedefine(isRedefine());
    cc->setConstraintList(constraints());
    cc->setState(state());
    cc->setKey(isKey());
   
    if (isArray()) {
        cc->setOccurencesNumber(occurencesNumber());
        cc->setOccurencesNumber(occurencesDepend(),
                                 occurencesDependFrom(),
                                 occurencesDependTo());
        //cc->buildOccurences();
    }
    
    foreach (BoxComponent *r, redefines()) {
        BoxComponent *rc = r->clone();
        cc->addRedefine(rc);
    }

    return cc;
}
bool
AtomIsoBitmapHex::pack( QDataStream *ds )
{
    Q_ASSERT( ds != NULL );
    
    setState(OK_State);
    
    if (length_type_ == VARIABLE_LENGTH ||
        length_type_ == VARIABLE_LENGTH_INCLUSIVE ||
        length_type_ == VARIABLE_LENGTH_ALLINCLUSIVE) {
        length_ = computevLength();
        if (length() <= 0)
            return false;
    }
 

    char *buf = new char[length()+1];
    ds->readRawData(buf, length());

    bool ok;

    setValue(QString::fromAscii(buf, length()).toULongLong(&ok, 16), false);

    emit packed(this);

    if (!ok) {
        delete []buf;
        qDebug()<<"buffer:"<<QString::fromAscii(buf, length());
        qDebug()<<name()+ " isobitmap is invalid. Packing suspended.";
        setErrorState(name()+ " isobitmap is invalid. Packing suspended.");
        return false;
    }

    delete []buf;

    //    qDebug()<<"atom isobitmap hex pack "<<"val="<<value().toString()
    //        <<" name="<<name()/*<<"buf="<<b*/;
    return true;
}

QByteArray
AtomIsoBitmapHex::unpack()
{
    return bytes();
}

QBitArray
AtomIsoBitmapHex::bytesToBits(QByteArray bytes)
{
    QBitArray bits(bytes.count()*8);
    // Convert from QByteArray to QBitArray
    for(int i=0; i<bytes.count(); ++i)
        for(int b=0; b<8; ++b)
            bits.setBit(i*8+b, bytes.at(i)&(1<<b));
    return bits;
}

QByteArray
AtomIsoBitmapHex::bitsToBytes(QBitArray bits)
{
    QByteArray bytes;
    bytes.resize(bits.count()/8+1);
    bytes.fill(0);
    // Convert from QBitArray to QByteArray
    for(int b=0; b<bits.count(); ++b)
        bytes[b/8] = ( bytes.at(b/8) | ((bits[b]?1:0)<<(b%8)));
    return bytes;
}

void
AtomIsoBitmapHex::resetBytes()
{
    //
    //  Reset the bit_map_ field in parent
    //
    qulonglong lval = value().toULongLong();
    QString bits = QString::number(lval, 2);

    bits = bits.rightJustified((length()*8)/2,'0',false);

    for (int i=0; i < bit_array_.size(); i++)
        if ( bits[i] == '1' )
            bit_array_.setBit(i);
        else
            bit_array_.clearBit(i);

    bytes_ = QString(QByteArray::number(lval, 16)).rightJustified(length(),'0').toAscii();
}

bool
AtomIsoBitmapHex::setValue( QVariant val, bool emit_changed_flg )
{
   // resetState();

    if (val.isValid() &&
        val.canConvert(QVariant::ULongLong)) {

        bool ok = false;
        qulonglong lval = val.toULongLong(&ok);

        if (!ok)
            return false;

        value_ = (qlonglong)lval;

        resetBytes();
        setOKState();

        if (emit_changed_flg)
            emit changed(this);

        return true;
    }

    return false;

}

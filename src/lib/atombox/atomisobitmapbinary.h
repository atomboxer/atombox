#ifndef ATOM_ISOBITMAP_BINARY
#define ATOM_ISOBITMAP_BINARY

#include <QtCore/QString>
#include <QtCore/QIODevice>
#include <QtCore/QByteArray>
#include <QtCore/QByteArray>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptEngine>

#include "atomisobitmap.h"

class AtomIsoBitmapBinary : public AtomIsoBitmap
{
    Q_OBJECT
public:
    AtomIsoBitmapBinary();
    AtomIsoBitmapBinary(QString name, Definition &definition);
    virtual ~AtomIsoBitmapBinary();

    virtual BoxComponent* clone();

    virtual bool isIsoBitmap() const { return true; }
    virtual bool isString() const { return false; }
    virtual bool isStringNumeric() const { return false; }

    virtual bool pack( QDataStream *ds );
    virtual QByteArray unpack();
    virtual void resetBytes();
    virtual bool setValue( QVariant val, bool emit_changed_flg );

private:
    Q_DISABLE_COPY(AtomIsoBitmapBinary)
};

#endif //ATOM_ISOBITMAP_BINARY

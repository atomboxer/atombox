#ifndef BOXEXCEPTION_H
#define BOXEXCEPTION_H

#include <QTextStream>
#include <QtCore/QString>

class BoxException
{
public:
    //
    //  Constructs a BoxException with no detail message
    //
    BoxException();

    //
    //  Constructs a BoxException with the specified detail message
    //
    BoxException(QString s);

    //
    //  Dumps the exeption to the QTextStream
    //
    void dump(QTextStream s);

    //
    //  Returns the message as a QString
    //
    QString toString();

private:
    QString message_;

};

#endif // BOXEXCEPTION_H

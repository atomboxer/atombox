
#include <QtCore/QString>
#include <QtCore/QDebug>
#include <QtCore/QVariant>
#include <QtCore/QBuffer>

#include "atomnumericbcd.h"
#include "atomclass.h"
#include "atomstring.h"

#include "utils.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


AtomNumericBCD::AtomNumericBCD() :
  Atom("undefined")
{
    if (lpad() != 0 || rpad() != 0) {
        qFatal("Fatal error. LPAD/RPAD clause with BCD DECIMAL atom.");
    }

    value_ = QVariant();
}


AtomNumericBCD::AtomNumericBCD( QString name,
                                Definition &definition )
    : Atom(name, definition)
{
    if (lpad() != 0 || rpad() != 0) {
        qFatal("Fatal error. LPAD/RPAD clause with BCD DECIMAL atom.");
    }

    value_ = QVariant();
}

AtomNumericBCD::~AtomNumericBCD()
{

}

void
AtomNumericBCD::clear()
{
    Atom::clear();

    bool ok;

    setValue(defaultValue().toLongLong(&ok), true);
    if (state() == OK_State)
        setDefaultState();
    
    if (lpad() != 0 ||
        rpad() != 0) {
        qWarning(("LPAD/RPAD clause cannot be used with BCD DECIMAL data type. Around" +
                  name()+" atom").toAscii().data());
    }
 
}

BoxComponent *
AtomNumericBCD::clone()
{
    AtomNumericBCD *cc = new AtomNumericBCD(name(), definition_);
    cc->setParent(parent());
    if (vDependent())
        cc->setvDependent(vDependent());
    if (vDependee())
        cc->setvDependee(vDependee());
    cc->setPrevSibling(prevSibling());
    cc->setNextSibling(nextSibling());
    cc->setRedefine(isRedefine());
    cc->setConstraintList(constraints());
    cc->setState(state());
    cc->setKey(isKey());
   
    if (isArray()) {
        cc->setOccurencesNumber(occurencesNumber());
        cc->setOccurencesNumber(occurencesDepend(),
                                 occurencesDependFrom(),
                                 occurencesDependTo());
        //cc->buildOccurences();
    }
    
    foreach (BoxComponent *r, redefines()) {
        BoxComponent *rc = r->clone();
        cc->addRedefine(rc);
    }

    return cc;
}

bool
AtomNumericBCD::pack( QDataStream *ds )
{
    Q_ASSERT(ds != NULL);
    
    setState(OK_State);
    
    if (length_type_ == VARIABLE_LENGTH ||
        length_type_ == VARIABLE_LENGTH_INCLUSIVE ||
        length_type_ == VARIABLE_LENGTH_ALLINCLUSIVE) {
        
        length_ = computevLength();

        if (length_ <= 0) {
            setErrorState("Invalid Length. Packing suspended");
            return false;
        }
    }

    size_t bin_len = length();

    char *buf = new char[bin_len];

    bin_len = ds->readRawData(buf, bin_len);

    QString s("");
    for (int i=0; i<bin_len; i+=1) {
        s.append(QString::number((int)(buf[i])));
    }

    setValue(s, false);

    emit packed(this);

    delete []buf;

    if (constraints().isEmpty()) {
        // qDebug()<<"NP NO CONSTRAINTS: n::"<<name().toLatin1().data()
        //       <<"::\""<<value().toString().toLatin1().data()<<"\""
        //      <<"::\""<<QString::fromLatin1(bytes().toHex())<<"\""
        //      <<"::lgth::"<<length();
        return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints())
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }

    if (!c_ok) {
        setExceptionState("Value does not match constraints");
    }

    // qDebug()<<"NP MATCHED:"<<c_ok<<" n::"<<name().toLatin1().data()
    //           <<"::\""<<value().toString().toLatin1().data()<<"\""
    //           <<"::\""<<QString::fromLatin1(bytes().toHex())<<"\""
    //           <<"::lgth::"<<length();
    return c_ok;
}

QByteArray
AtomNumericBCD::unpack()
{
    return bytes();
}

void
AtomNumericBCD::resetBytes()
{
    if (length() == 0 || value_.isNull() || !value_.isValid()) {
        bytes_ = QByteArray();
        return;
    }

    size_t bin_length = length();

    QString   value_s(bin_length,'0');
    QByteArray v = value().toString().toAscii();
    value_s = value_s.replace(bin_length - v.length(),v.length(), v);
    
    bytes_.resize(length());
    for (int i=0; i<bytes_.length(); ++i) {
        bytes_[i] = value_s[i].digitValue();
    }

    //bytes_ = QByteArray::fromHex(value_s.toAscii());
}

bool
AtomNumericBCD::setValue(QVariant val, bool emit_changed_flg)
{
    //resetState();

    if (val.isValid() &&
        val.canConvert(QVariant::LongLong)) {
	
        bool ok;
        qlonglong lval = val.toLongLong(&ok);
        if (!ok) {
            setErrorState("Invalid value in set method");
            return false;
        }

        QString sval = QString::number(lval);

        value_ = lval;

        resetBytes();
        setOKState();
        
        if (emit_changed_flg)
            emit changed(this);

    } else {
        setErrorState("Invalid value in set method");
        return false;
    }

    if (constraints().isEmpty()) {
        return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints()) {
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }
    }

    if (!c_ok) {
        setExceptionState("Value does not match constraints");
        return false;
    }

    return false;
}

#ifndef CONSTRAINT_LIST
#define CONSTRAINT_LIST

#include <QtCore/QList>

#include "constraint.h"

class ConstraintList : Constraint
{
public:
    ConstraintList();
    virtual ~ConstraintList();

    void addConstraint(Constraint *c) { constraints_.push_back(c); }
    QList<Constraint*> constraints() const { return constraints_; }
    
    virtual Constraint* clone();

private:
    QList<Constraint*> constraints_;
};
#endif

#ifndef CONSTRAINT_H
#define CONSTRAINT_H

#include <QtCore/QString>
#include <QtCore/QVariant>
#include <QtCore/QDebug>

class Constraint
{
    //for clone()
    friend class ConstraintList;
    friend class ConstraintValue;
    friend class ConstraintString;
    friend class ConstraintRegex;
    friend class ConstraintRange;

public: 
    enum Kind {
        k_value,
        k_range,
        k_string,
        k_regex,
        k_list,
        k_undefined
    };
    Constraint( QString description );
    virtual ~Constraint();

    QString display() const { return display_; }

    virtual bool isMatch(QVariant v) = 0;
    virtual QVariant matchedValue() const { return matched_value_; }
    virtual Kind kind() const { return kind_; }

    virtual Constraint* clone()  = 0;

protected:
    virtual void setMatchedValue(QVariant v) { matched_value_ = v; }

protected:
    Kind kind_;

private:
    QString display_;
    QVariant matched_value_;
};
#endif

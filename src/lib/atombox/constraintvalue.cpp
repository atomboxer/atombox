#include "constraintvalue.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

ConstraintValue::ConstraintValue( QVariant value, QString &display )
    : Constraint (display), value_(value)
{
    kind_ = k_value;
}

ConstraintValue::~ConstraintValue()
{}

bool 
ConstraintValue::isMatch(QVariant v)
{
    
    bool matched = false;

    switch(value().type()) {
    case (QVariant::String) :
        matched = (value().toString().toUpper() == v.toString().toUpper())?true:false;
        break;

    case(QVariant::Int):
        matched = (value().toInt() == v.toInt())?true:false;
        break;
    case(QVariant::UInt):
        matched = (value().toInt() == v.toUInt())?true:false;
        break;

    case(QVariant::LongLong):
        matched = (value().toLongLong() == v.toLongLong())?true:false;
        break;
    case(QVariant::ULongLong):
        matched = (value().toLongLong() == v.toULongLong())?true:false;
        break;

    default: Q_ASSERT(0); break;
    }
        
    if (matched)
        setMatchedValue(v);
    
    //qDebug()<<value().toString().toUpper() <<"==" <<v.toString().toUpper()<<":"<<matched;
    return matched;
}

Constraint*
ConstraintValue::clone()
{
    ConstraintValue * _clone = new ConstraintValue(this->value_, this->display_);
    _clone->kind_ = this->kind();
    _clone->matched_value_ = this->matchedValue();
    return _clone;
}

#include "constraintrange.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif



ConstraintRange::ConstraintRange ( int from, int to, QString &description)
    : Constraint( description ),
      from_(from),
      to_(to)
{
    kind_ = k_range;
}

ConstraintRange::~ConstraintRange()
{}

bool 
ConstraintRange::isMatch(QVariant v)
{
    bool ok = false;
    qint32 val = v.toInt(&ok);

    if (!ok) {
        return false;
    }
    
    if ( (val >= from_) && (val <= to_) ) {
        setMatchedValue(v);
        return true;
    }

    return false;
}


Constraint*
ConstraintRange::clone()
{
    ConstraintRange * _clone = new ConstraintRange(this->from_, this->to_, this->display_);
    _clone->kind_ = this->kind();
    _clone->matched_value_ = this->matchedValue();
    return _clone;
}

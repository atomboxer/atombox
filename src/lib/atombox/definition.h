#ifndef DEFINITION_H
#define DEFINITION_H

#include <QtCore/QString>
#include <QtCore/QRegExp>
#include <QtCore/QList>
#include <QtCore/QVariant>

#ifdef __TANDEM
#include <sys/types.h>
#endif

#include "constraint.h"

enum AtomDataType {
    DEFAULT,

    ASCII,
    ASCII_NUMERIC,
    ASCII_ALPHABETIC,
    EBCDIC,
    EBCDIC_NUMERIC,
    EBCDIC_ALPHABETIC,
    BCD,
    BCD_PACKED,
    HEX_PACKED,

    BITMAP,
    UBITMAP,

    INT8,
    INT16,
    INT32,
    INT64,
    UINT8,
    UINT16,
    UINT32,
    UINT64,
    FLOAT,
    DOUBLE
};

enum AtomLengthType {
    UNDEFINED,
    FIXED,
    VARIABLE_LENGTH,
    VARIABLE_LENGTH_INCLUSIVE,
    VARIABLE_LENGTH_ALLINCLUSIVE,
    VARIABLE_DELIMITED,
};

enum AtomNumericSignType {
    UNSIGNED,
    EXPLICIT_LEFT,
    EXPLICIT_RIGHT
};

enum ByteOrder {
    BigEndian,
    LittleEndian,
    DefaultByteOrder
};

class Definition
{
public:
    Definition() :
        isobitmap_flg_(false),
        data_type_(DEFAULT),
        numeric_sign_type_(UNSIGNED),
        numeric_decimal_point_(0),
        byte_order_(DefaultByteOrder),
        default_(QVariant()),
        default_flg_(false),
        display_(""),
        length_(0),
        length_type_(UNDEFINED),
        lpadc_(0),
        regex_(""),
        rpadc_(0),
        vlength_delimiter_(0),
        vlength_dependant_(""){}

    bool isIsoBitmap() const { return isobitmap_flg_; }
    bool isDefined() const { return !(length_type_ == UNDEFINED); }
    bool isNumber() const {
        switch(data_type_) {
        case(ASCII_NUMERIC): case(EBCDIC_NUMERIC): case(BCD):
        case(INT8): case(UINT8): case(INT16): case(UINT16):
        case(INT32): case(UINT32): case(INT64): case(UINT64): 
        case(FLOAT): case(DOUBLE): case (HEX_PACKED): 
        case(BITMAP) : case(UBITMAP): return true; break;

        default: return false; break;
        }
    }
    bool isString() const { return !isNumber(); }

 public:
    bool                   isobitmap_flg_;   /*ISOBITMAP*/
    AtomDataType           data_type_;
    AtomNumericSignType    numeric_sign_type_;
    short                  numeric_decimal_point_;
    ByteOrder              byte_order_;
    QVariant               default_;
    bool                   default_flg_;
    QString                display_;
    ssize_t                length_;
    AtomLengthType         length_type_;
    QChar                  lpadc_;
    QRegExp                regex_;
    QChar                  rpadc_;
    QChar                  vlength_delimiter_;
    QString                vlength_dependant_;
};

#endif //DEFINITION_H

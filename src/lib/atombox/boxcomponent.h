#ifndef BOXCOMPONENT_H
#define BOXCOMPONENT_H

#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QDebug>
#include <QtCore/QDataStream>

#include "definition.h"

#ifdef __TANDEM
#include <sys/types.h>
#endif

class BoxComponent : public QObject
{
    Q_OBJECT

public:
    enum State {
        OK_State          = 0,
        Exception_State   = 1,
        Error_State       = 2,
        Default_State     = 3,
    };

    BoxComponent( QObject *parent=0 );
    BoxComponent( QString name, QObject *parent=0 );
    virtual ~BoxComponent();

    virtual void add( BoxComponent *c);
    virtual void addRedefine(BoxComponent *redefine);
    virtual void buildOccurences();
    BoxComponent *child( QString name ) const;
    virtual void connectDependents();
    virtual BoxComponent* componentByIdx( int idx );

    void dump();
    QString displayString() const { if (display_ == "") return name();
                                    else return display_; }

    virtual QString getQualifiedName() const;
    virtual bool hasRedefines() const;
    virtual int  idx() const;
    virtual bool isOccurence() const { return occurence_flg_; }
    virtual bool isRedefine() const { return redefine_flg_; }
    virtual bool isTopLevel() const;
    virtual bool isArray() const {  return array_flg_; }
    virtual bool isRecord() const {  return record_flg_; }
    virtual bool isKey() const {  return key_flg_; }
    virtual QString name() const { return name_ ; }
    virtual BoxComponent *nextSibling() const { return next_; }
    virtual BoxComponent* parent() const;
    virtual BoxComponent *prevSibling() const { return prev_; }
    virtual unsigned offset() const;
    virtual QList<BoxComponent*> redefines() const { return redefines_; }
    virtual void resetState() { setState(Default_State); state_reason_ = ""; }
    virtual void setDisplayString(QString display) { display_ = display; }
    virtual void setIdx(int idx);
    virtual void setParent(BoxComponent *p);
    virtual void setOccurence(bool occ) { occurence_flg_ = occ; }
    virtual void setOccurenceParent(BoxComponent* occ_parent) { occurenceParent_ = occ_parent; }
    virtual void setOKState() { state_ = OK_State; }
    virtual void setErrorState(QString r);
    virtual void setExceptionState(QString r);
    virtual void setDefaultState() { state_ = Default_State; }
    virtual void setNextSibling(BoxComponent *c) { next_ = c; }
    virtual void setPrevSibling(BoxComponent *c) { prev_ = c; }
    virtual void setState(State s) { state_ = s; }
    virtual void setOccurencesNumber(size_t o);
    virtual void setOccurencesNumber(QString dep, int from, int to); 
    virtual void setRedefine(bool redefine_flg) { redefine_flg_ = redefine_flg; }
    virtual void setRecord(bool r) { record_flg_ = r;}
    virtual void setKey(bool k) { key_flg_ = k;}
    virtual State state() const { return state_; }
    virtual QString stateReason() const { return state_reason_; }
    virtual QList<BoxComponent*> occurences() const;
    virtual size_t occurencesNumber();
    virtual const QString&  occurencesDepend() const { return occurencesDepend_; }
    virtual size_t occurencesDependFrom() const { return occurencesDependFrom_; }
    virtual size_t occurencesDependTo() const { return occurencesDependTo_; }
    virtual BoxComponent* occurenceParent() const { return occurenceParent_; }
    virtual size_t maxOccurencesNumber() const;

    virtual void insertOccurence(unsigned idx, BoxComponent *c) { occurences_.insert(idx,c); }
    virtual void addOccurence(BoxComponent *c);
public:
    virtual QByteArray bytes() = 0;
    virtual void clear() = 0;
    virtual QList<BoxComponent*> childs() const = 0;
    virtual BoxComponent *clone() = 0;
    virtual void dump(ushort max_levels = 1) const = 0;
    virtual void setByteOrder(ByteOrder o) = 0;
    virtual bool isAtom() const = 0;
    virtual ssize_t length() const = 0;
    virtual bool pack( QIODevice *d );
    virtual bool pack( QDataStream *ds ) = 0;
    virtual QByteArray unpack() = 0;
    virtual void reindex() = 0;
    virtual QString toJSON( bool use_redefines = false ) = 0;

    virtual void emitChanged() { emit changed(this); }
    virtual void emitPacked() { emit packed(this); }
//
// Public signals
//
signals:
    void changed(BoxComponent *);
    void packed(BoxComponent *);
    void unpacked(BoxComponent *);

    //void stateChanged( State state);

public slots:
    void reset(BoxComponent *c = NULL);
    void childChanged( BoxComponent *c = NULL);
private:
    QString                name_;
    BoxComponent          *parent_;
    BoxComponent          *next_;
    BoxComponent          *prev_;

    QList<BoxComponent*>  redefines_;
    int                   idx_;
    State                 state_;
    QString               state_reason_;

    bool                  redefine_flg_;
    bool                  occurence_flg_;
    BoxComponent*         occurenceParent_;
    bool                  array_flg_;
    bool                  record_flg_;
    bool                  key_flg_;

protected:
    QString               display_;
    size_t                occurencesNumber_;
    QString               occurencesDepend_;
    int                   occurencesDependFrom_;
    int                   occurencesDependTo_;
    BoxComponent*         _occurs_dependent_;
    QList<BoxComponent*>  occurences_;

    bool                  is_connected_;

//STATIC
public:
    static void dumpABTree(BoxComponent *box, QString prefix="");
    static BoxComponent* findComponentByName(QString name, BoxComponent * root);

private:
    Q_DISABLE_COPY(BoxComponent)
};

#endif // BOXCOMPONENTBASE_H

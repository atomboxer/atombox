
#include <QtCore/QBuffer>
#include <QtGlobal>
#include <QtCore/QString>
#include <QtCore/QDebug>
#include <QtCore/QVariant>

#include "atomstringnumeric.h"
#include "atomclass.h"
#include "utils.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


AtomStringNumeric::AtomStringNumeric() :
    AtomString()
{
    //clear();
}


AtomStringNumeric::AtomStringNumeric(QString name,
                                     Definition &definition,
                                     Charset c)
    : AtomString(name,definition, c)
{
    
    //
    //  String atoms are left padded with zeroes
    //
    if (lpad() == 0) {
        setLpad('0');
    }
    
    //clear();
}


BoxComponent *
AtomStringNumeric::clone()
{
    AtomStringNumeric *cc = new AtomStringNumeric(name(), definition_, charset_); 
    cc->setParent(parent());
    if (vDependent())
        cc->setvDependent(vDependent());
    if (vDependee())
        cc->setvDependee(vDependee());
    cc->setPrevSibling(prevSibling()); 
    cc->setNextSibling(nextSibling());
    cc->setRedefine(isRedefine());
    cc->setConstraintList(constraints());
    cc->setState(state());
    cc->setKey(isKey());
   
    if (isArray()) {
        //foreach (BoxComponent *occ, occurences()) {
        //    BoxComponent *occ_copy = occ->clone();
        //    cc->addOccurence(occ_copy);
        //}
        cc->setOccurencesNumber(occurencesNumber());
        cc->setOccurencesNumber(occurencesDepend(),
                                occurencesDependFrom(),
                                occurencesDependTo());
    } 

    foreach (BoxComponent *r, redefines()) {
        BoxComponent *rc = r->clone();
        cc->addRedefine(rc);
    }    

    return cc;
}

AtomStringNumeric::~AtomStringNumeric()
{
}

void
AtomStringNumeric::clear()
{
    Atom::clear();

    QString v(length(), '0');

    v = QString::number(defaultValue().toString().left(length()).toLongLong());

    if (lpad() != 0) {
        if (lengthType() != VARIABLE_LENGTH &&
            lengthType() != VARIABLE_LENGTH_INCLUSIVE &&
            lengthType() != VARIABLE_LENGTH_ALLINCLUSIVE)
            v = v.rightJustified(length(), lpad());
    } else if (rpad() != 0) {
        if (lengthType() != VARIABLE_LENGTH &&
            lengthType() != VARIABLE_LENGTH_INCLUSIVE &&
            lengthType() != VARIABLE_LENGTH_ALLINCLUSIVE)
            v = v.leftJustified(length(), rpad());
    }

    bool ok = false;
    
    long long val = v.toLongLong(&ok);
    if (!ok)
        val = 0;

    setValue(val, true);
    if (state() == OK_State)
        setDefaultState();
        
    if (!ok) {
        qFatal(("Invalid use of DEFAULT clause while building "+
                name()+" atom").toAscii().data());
    }

}

bool
AtomStringNumeric::pack( QDataStream *ds )
{
    Q_ASSERT( ds != NULL );

    AtomString::pack(ds);

    QString val = value_.toString();
    if (val.right(1)=="+" ||
        val.right(1)=="-") {
        return setValue(val.right(1)+val.left(length()-1),false);
    } else if (definition_.numeric_decimal_point_ != 0) {
        return setValue(QVariant(val.remove(QRegExp("^[0 ]+"))),false);
    } else { 
        return setValue(value(),false);
    }

    // bool ok = false;
    // qlonglong val = value().toLongLong(&ok);

    // if (QString(value().toString()).remove(" ").length() == 0) {
    //     val = 0;
    //     ok = true;
    // }

    // if (!ok) {
    //     // qDebug()<<"a::"<<name().toLatin1().data()
    //     //      <<"::\""<<value().toString().toLatin1().data()<<"\""
    //     //      <<"::\""<<QString::fromLatin1(bytes().toHex())<<"\""
    //     //      <<"::lgth::"<<length();
    //     setExceptionState(name()+" is not numeric.");
    //     value_ = value();
    //     return false;
    // }

    // value_ = val;

    // if (constraints().isEmpty()) {
    //     return true;
    // }

    // bool c_ok = false;
    // foreach (Constraint *c, constraints()) {
    //     if (c->isMatch(QVariant(value()))) {
    //         c_ok = true;
    //         break;
    //     }
    // }

    // if ( !c_ok ) {
    //     // qDebug()<<"a::"<<name().toLatin1().data()
    //     //          <<"::\""<<value().toString().toLatin1().data()<<"\""
    //     //          <<"::\""<<QString::fromLatin1(bytes().toHex())<<"\""
    //     //          <<"::lgth::"<<length();

    //     setExceptionState("Value does not match constraints");
    // }

    return true;
}

QByteArray
AtomStringNumeric::unpack()
{
    QByteArray ret(bytes());
    if (length_type_ == VARIABLE_DELIMITED)
        if (nextSibling())
            ret.append(vlength_delimiter_);

    if ( charset_ == AtomString::EBCDIC ) {
        ASCII2EBCDIC(ret.data(),ret.length());
    }
    return ret;
}

void
AtomStringNumeric::resetBytes()
{
    if (length() == 0 || value_.isNull() || !value_.isValid() ||
        !value_.canConvert(QVariant::LongLong)) {
        bytes_ = QByteArray();
        return;
    }
    
    bool ok = false;
    
    //
    //  Decimal Point?
    //
    if (definition_.numeric_decimal_point_ != 0) {
        double real = value().toDouble(&ok);
        QChar sign;
    
        if (real < 0 ) {
            sign ='-';
            real *= -1;
        } else {
            sign =' ';
        }

        QString svalue = QString::number(real);
        int num_dec = definition_.numeric_decimal_point_;
        int num_rea = length()-definition_.numeric_decimal_point_-1;

        if (definition_.numeric_sign_type_ == EXPLICIT_LEFT)
            num_dec--;

        if (definition_.numeric_sign_type_ == EXPLICIT_RIGHT)
            num_rea--;

        QString sreal="";
    
        if (svalue.contains('.')) {
            sreal=svalue.mid(0,qMin(num_dec,svalue.indexOf('.'))).rightJustified(num_dec,'0')+".";
            sreal+=svalue.mid(svalue.indexOf('.')+1).mid(0,num_rea).leftJustified(num_rea,'0');
        } else {
            sreal=svalue.mid(0,num_dec).rightJustified(num_dec,'0')+".";
            sreal+=QString(num_rea,'0');
        }

        //
        // Append the sign
        //  
        if (definition_.numeric_sign_type_ == EXPLICIT_LEFT) {
            if (lpad() != 0) {
                sreal = sign+sreal.rightJustified(length()-1, lpad());
            } else {
                sreal = sign+sreal.rightJustified(length()-1, '0');
            }
        } else if (definition_.numeric_sign_type_ == EXPLICIT_RIGHT) {
            if (lpad() != 0) {
                sreal = sreal.rightJustified(length()-1, lpad())+sign;
            } else {
                sreal = sreal.rightJustified(length()-1, '0')+sign;
            }
        } else {
            if (lpad() != 0) {
                sreal = sreal.rightJustified(length(), lpad());
            } else {
                sreal = sreal.rightJustified(length(), '0');
            }
        }
    
    
        bytes_ = sreal.toAscii();
        if ( charset_ == AtomString::EBCDIC ) {
            ASCII2EBCDIC(bytes_.data(), length());
        }
    
        return;
    }


    qlonglong lvalue = value().toLongLong(&ok);
    if (!ok)
        qFatal(("Fatal error while setting atom:"+name()).toAscii().data());

    QChar sign;

    if (lvalue < 0 ) {
        sign ='-';
        lvalue=(-1)*lvalue;
    } else {
        sign =' ';
    }

    QString svalue = QString::number(lvalue);


    if (definition_.numeric_sign_type_ == EXPLICIT_LEFT) {
        if (lpad() != 0) {
            svalue = sign+svalue.rightJustified(length()-1, lpad());
        } else {
            svalue = sign+svalue.rightJustified(length()-1, '0');
        }
    } else if (definition_.numeric_sign_type_ == EXPLICIT_RIGHT) {
        if (lpad() != 0) {
            svalue = svalue.rightJustified(length()-1, lpad())+sign;
        } else {
            svalue = svalue.rightJustified(length()-1, '0')+sign;
        }
    } else {
        if (lpad() != 0) {
            svalue = svalue.rightJustified(length(), lpad());
        } else {
            svalue = svalue.rightJustified(length(), '0');
        }
    
    }

    bytes_ = svalue.toAscii();

    if ( charset_ == AtomString::EBCDIC ) {
        ASCII2EBCDIC(bytes_.data(), length());
    }
}

void
AtomStringNumeric::resize(size_t new_size)
{
    if (new_size <= 0) {
        length_ = 0;
        bytes_ = QByteArray();
        value_ = QVariant();
        return;
    }

    //FIXED with default
    QString v(new_size, '0');
    v.replace(0, qMin<int>(bytes().length(), new_size),
              bytes().mid(0, qMin<int>(bytes().length(), new_size))); 

    if (lpad() != 0) {
        if (lengthType() != VARIABLE_LENGTH &&
            lengthType() != VARIABLE_LENGTH_INCLUSIVE &&
            lengthType() != VARIABLE_LENGTH_ALLINCLUSIVE)
            v = v.rightJustified(new_size, lpad());
    } else if (rpad() != 0) {
        if (lengthType() != VARIABLE_LENGTH &&
            lengthType() != VARIABLE_LENGTH_INCLUSIVE &&
            lengthType() != VARIABLE_LENGTH_ALLINCLUSIVE)
            v = v.leftJustified(new_size, rpad());
    }

    bool ok = false;
    qlonglong vv = v.toLongLong(&ok);
    if (!ok) {
        setExceptionState("Invalid atom");
        return;
    }

    if (lengthType() == VARIABLE_LENGTH ||
        lengthType() == VARIABLE_LENGTH_INCLUSIVE ||
        lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE ) {
        length_ = new_size;
    }
    
    
    value_ = vv;
    bytes_ = v.toAscii();
  
}

bool
AtomStringNumeric::setValue(QVariant val, bool emit_changed_flg) 
{
    //resetState();

    if (val.isValid() &&
        val.canConvert(QVariant::LongLong)) {
        
        bool    ok = false;
        QString sval="";
        qlonglong lval = val.toLongLong(&ok);


        if (definition_.numeric_decimal_point_ !=0) {
            double real = val.toDouble(&ok);
            sval = QString::number(real);
            if (sval.contains('e') || sval.contains('E'))
                value_ = 0;
            else  {
                value_ = sval.toDouble();
            }
        } else {
            sval = QString::number(lval);
            value_ = sval.toLongLong();
        }

        if (lengthType() == VARIABLE_LENGTH ||
            lengthType() == VARIABLE_LENGTH_INCLUSIVE ||
            lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE ||
            lengthType() == VARIABLE_DELIMITED) {
            length_ = qMin<int>(maxLength(), value().toString().size());
        }

        resetBytes();
        setOKState();

        if (emit_changed_flg)
            emit changed(this); 


    } else {
        setErrorState("Invalid value in set method");
        return false;
    }   
            
    if (constraints().isEmpty()) {
        return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints()) {
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }
    }

    if (!c_ok) {
        setExceptionState("Value does not match constraints");
        return false;
    }
   
    return true;
}


QVariant
AtomStringNumeric::value() const
{
    return value_.toLongLong();
}

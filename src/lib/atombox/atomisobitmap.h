#ifndef ATOM_ISOBITMAP
#define ATOM_ISOBITMAP

#include <QtCore/QString>
#include <QtCore/QIODevice>
#include <QtCore/QByteArray>
#include <QtCore/QBitArray>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptEngine>

#include "atom.h"

class AtomIsoBitmap : public Atom
{
    Q_OBJECT
public:
    AtomIsoBitmap();
    AtomIsoBitmap(QString name, Definition &definition);
    virtual ~AtomIsoBitmap();

    virtual QBitArray  bitArray() const { return bit_array_; };
    virtual void clear();
    virtual bool isIsoIsoBitmap() const { return true; }
    virtual bool isString() const { return false; }
    virtual bool isStringNumeric() const { return false; }

    virtual QByteArray bytes();
    virtual bool pack( QDataStream *ds ) = 0;
    virtual QByteArray unpack() = 0;
    virtual void resetBytes() = 0;
    virtual bool setValue(QVariant val, bool emit_changed_flg) = 0;

protected:
    QBitArray bit_array_;

private:
    Q_DISABLE_COPY(AtomIsoBitmap)
};

#endif //ATOM_ISOBITMAP

#include "boxcomponent.h"
#include "atom.h"
#include "atomstring.h"

#include <QtCore/QBuffer>
#include <QtCore/QDebug>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define BOXCOMPONENT_DEBUG

BoxComponent* 
BoxComponent::findComponentByName(QString name, BoxComponent * root )
{

#ifdef BOXCOMPONENT_DEBUG
    qDebug()<<"findComponentByName::"<<root->name()<<"!="<<name;
#endif

    if (root == NULL)
        return NULL;

    foreach (BoxComponent *r, root->redefines()) {
        if (r->childs().size()!=0) {
            BoxComponent *n = findComponentByName(name, r->childs().last());
            if ( n != NULL )
                return n;
        } else {
            BoxComponent *n = findComponentByName(name, r);
            if ( n != NULL )
                return n;
        }
    }

    foreach (BoxComponent *c, root->childs()) {
        if (c->name() == name)
            return c;
    }
    
    if (root->name() == name)
        return root;
    else {
        if (root->prevSibling()) {
            BoxComponent *n = findComponentByName(name, root->prevSibling());
            if ( n != NULL )
                return n;

        } else {
            if (root->parent() && !root->isRedefine()) {
                BoxComponent *n = findComponentByName(name, root->parent());
                if ( n != NULL )
                    return n;
            }
        }
    }
    return NULL;
}

BoxComponent::BoxComponent( QObject *parent ) : QObject(parent),
                                                name_("undefined"),
                                                parent_(NULL),
                                                next_(NULL),
                                                prev_(NULL),
                                                redefines_(QList<BoxComponent*>()),
                                                display_(""),
                                                idx_(0),
                                                state_(Default_State),
      state_reason_(""),
      redefine_flg_(false),
      array_flg_(false),
      
      occurencesNumber_(0),
      _occurs_dependent_(NULL),
      occurences_(QList<BoxComponent*>()),
      is_connected_(false)

{
    occurence_flg_ = false;
    occurenceParent_ = NULL;
}

BoxComponent::BoxComponent(QString name, QObject *parent) : QObject(parent),
                                                            name_(name),
                                                            parent_(NULL),
                                                            next_(NULL),
                                                            prev_(NULL),
                                                            redefines_(QList<BoxComponent*>()),
                                                            display_(""),
    idx_(0),
    state_(Default_State),
    state_reason_(""),
    redefine_flg_(false),
    array_flg_(false),
    record_flg_(false),
    key_flg_(false),
    occurencesNumber_(0),
    _occurs_dependent_(NULL),
    occurences_(QList<BoxComponent*>()),
    is_connected_(false)
{
    occurence_flg_ = false;
    occurenceParent_ = NULL;
}

BoxComponent::~BoxComponent()
{

}


void 
BoxComponent::add( BoxComponent *c )
{
    if (c->isAtom() || !c->isRedefine())
        Q_ASSERT(0);

    if (c->isRedefine())
        addRedefine(c);
}

void 
BoxComponent::addRedefine(BoxComponent *r)
{
    r->setParent(this);
    
    if (!redefines_.isEmpty()) {
        BoxComponent *last = redefines().last();
        last->setNextSibling(r);
        r->setPrevSibling(last);
    } else {
        r->setNextSibling(NULL);
    }

    redefines_.push_back(r);
    r->setRedefine(true);
}

void
BoxComponent::addOccurence(BoxComponent *c) { 
    if (array_flg_ == false)
        array_flg_ = true;

    c->setOccurence(true);
    c->setOccurenceParent(this);
    occurences_.push_back(c);

}

void
BoxComponent::buildOccurences() {
    if (!array_flg_)
        qFatal("buildOccurences called on a non array!");

    if (occurences_.length() == 0) {
        for (int i=0;i<maxOccurencesNumber(); i++) {
            array_flg_ = false;
            BoxComponent *cl = clone();
            this->addOccurence(cl);
        }
    }
    array_flg_ = true;
}

bool
BoxComponent::pack( QIODevice *device)
{
    if (!device->isOpen()) 
        device->open(QIODevice::ReadOnly);

    QDataStream ds(device);
    return pack(&ds);
}


BoxComponent *
BoxComponent::child( QString name ) const
{
    foreach(BoxComponent *c, childs()) 
        if (c->name() == name)
            return c;

    return NULL;
}

// QList<BoxComponent*>
// BoxComponent::childs() const
// {
//     qFatal("boxcomponent.cpp childs() should not get here");
//     return QList<BoxComponent*>();
// }

void
BoxComponent::connectDependents()
{
    if (is_connected_)
        return;

    foreach(BoxComponent *c, childs()) {
        QObject::connect(c, SIGNAL(changed(BoxComponent*)),
                         this, SLOT(childChanged(BoxComponent*)),
                         Qt::DirectConnection);
        //QObject::connect(this, SIGNAL(changed(BoxComponent*)),
        //                 c, SLOT(reset(BoxComponent*)),
        //                 Qt::DirectConnection);

#ifdef BOXCOMPONENT_DEBUG
        qDebug()<<"connect::"<<c->name()<<" -> "<<name();
#endif
        c->connectDependents();

    }
            
    foreach(BoxComponent *r, redefines()) {
        if ( r != this ) {
            QObject::connect(r, SIGNAL(changed(BoxComponent*)),
                             this, SLOT(childChanged(BoxComponent*)),
                             Qt::DirectConnection);
            QObject::connect(this, SIGNAL(changed(BoxComponent*)),
                             r, SLOT(reset(BoxComponent*)),
                             Qt::DirectConnection);

#ifdef BOXCOMPONENT_DEBUG
            qDebug()<<"connect::"<<r->name()<<" -> "<<name();
#endif
            r->connectDependents();
        }
 
    }
    
    if (isArray())
        foreach(BoxComponent *o, occurences_) {
            if(o!=this) {
#ifdef BOXCOMPONENT_DEBUG
                qDebug()<<"connect::"<<o->name()<<"[*] -> "<<name();
#endif
                o->connectDependents();
                QObject::connect(o, SIGNAL(changed(BoxComponent*)),
                                 this, SLOT(childChanged(BoxComponent*)),
                                 Qt::DirectConnection);
                QObject::connect(this, SIGNAL(changed(BoxComponent*)),
                                 o, SLOT(reset(BoxComponent*)),
                                 Qt::DirectConnection);
            }
        }

    if (!is_connected_) {
        is_connected_ = true;
    }

    return;
}

BoxComponent* 
BoxComponent::componentByIdx( int idx )
{
    BoxComponent *c;
    foreach(c,childs()) {
        if (c->idx() == idx)
            return c;
    }
    return NULL;
}

QString 
BoxComponent::getQualifiedName() const
{
    QString q_string("."+name());
  
    BoxComponent *p = parent();
    while ( p != NULL ) {
        q_string.push_front("."+p->name());
        p = p->parent();
    }
    q_string.push_front("DDL");
    return q_string;
}

bool 
BoxComponent::hasRedefines() const 
{ 
    return !redefines_.empty(); 
}

int 
BoxComponent::idx() const
{
    return idx_;
}

void
BoxComponent::dump() 
{
}

bool
BoxComponent::isTopLevel() const
{
    if (parent() && parent()->parent() == NULL) 
        return true;
    return false;
}

BoxComponent* 
BoxComponent::parent() const
{
    return parent_;
}

void
BoxComponent::setExceptionState(QString r)
{ 
    foreach(BoxComponent *c, childs()) {
        c->setExceptionState(r);
    }

    state_reason_ = r;
    state_ = Exception_State;
}

void 
BoxComponent::setErrorState(QString r)
{

#ifdef BOXCOMPONENT_DEBUG
    qDebug()<<"Setting error state for:"<<name()<<"::"<<r;
#endif

    foreach(BoxComponent *c, childs()) {
        c->setErrorState(r);
        if (c->hasRedefines()) {
            foreach(BoxComponent *rr, c->redefines()) {
                rr->setErrorState(r);
            }
        }
    }

    state_reason_ = r;
    state_ = Error_State;
}

void
BoxComponent::setIdx( int idx )
{
    idx_ = idx;
}


void 
BoxComponent::setParent(BoxComponent *p)
{
    parent_ = p;
}

void
BoxComponent::setOccurencesNumber(size_t o)
{
    occurencesNumber_ = o;
    array_flg_ = true;
    buildOccurences();
}

void
BoxComponent::setOccurencesNumber(QString dep, int from, int to) 
{
    if ( dep  == "" ) 
        return;
    
    occurencesDepend_ = dep; 
    occurencesDependFrom_ = from;
    occurencesDependTo_ = to;
    array_flg_ = true;
    buildOccurences();
}

size_t
BoxComponent::maxOccurencesNumber() const
{
    if (occurencesDepend_ == "") {
        return occurencesNumber_;
    }

    return occurencesDependTo_;
}

QList<BoxComponent*>
BoxComponent::occurences() const
{ 
    return occurences_;
}

size_t
BoxComponent::occurencesNumber()
{ 
    if (occurencesDepend_.isEmpty())
        return occurencesNumber_;

    if (_occurs_dependent_ != NULL) {
        Atom *atom = static_cast<Atom*>(_occurs_dependent_);
        occurencesNumber_ = atom->value().toInt();
        return occurencesNumber_;
    }

    _occurs_dependent_ = BoxComponent::findComponentByName(occurencesDepend_, this);

    if (!_occurs_dependent_) {
        return occurencesNumber_;
    }

    if (_occurs_dependent_->isAtom()) {
        Atom *atom = static_cast<Atom*>(_occurs_dependent_);
        if (!atom->isNumeric()) {
            qWarning("BoxComponent::occurencesNumber dependent is not a number!");
            return 0;
        }

        occurencesNumber_  = atom->value().toInt();
    }

    return occurencesNumber_;
}

unsigned 
BoxComponent::offset() const
{
    unsigned ofst = 0;
    BoxComponent *n = NULL;
    if (isRedefine()) {
        Q_ASSERT(parent());
        n = parent()->prevSibling();
    }
    else { 
        n = prevSibling();
    }

    while(n != NULL) {
        ofst += n->length();
        n = n->prevSibling();
    }
    return ofst;
}

void
BoxComponent::dumpABTree(BoxComponent *c, QString indent)
{
    if (c->isRedefine()) {
        qDebug()<<"R"+indent+c->name()<<":::"<<c;
    } else {
        qDebug()<<indent+c->name()<<":::"<<c<<c;
    }

    if (c->isRedefine()) {
        foreach (BoxComponent *ch, c->childs()) {
            BoxComponent::dumpABTree(ch, "R"+indent+"    ");
        }
        foreach (BoxComponent *ch, c->redefines()) {
            BoxComponent::dumpABTree(ch, "R"+indent+"    ");
        }
    } else if (c->isAtom()) {
        if (c->hasRedefines()) {
            foreach (BoxComponent *r, c->redefines()) {
                BoxComponent::dumpABTree(r, indent+"    ");
            }
        }
    } else {
        foreach (BoxComponent *ch, c->childs()) {
            BoxComponent::dumpABTree(ch, indent +"    ");
        }

        if (c->hasRedefines()) {
            foreach (BoxComponent *r, c->redefines()) {
                BoxComponent::dumpABTree(r, indent+"    ");
            }
        }
    }
}

// !Slots //
void
BoxComponent::reset(BoxComponent *c)
{
#ifdef BOXCOMPONENT_DEBUG
    qDebug()<<"BoxComponent::reset::"<<name()<<"<--"<<c->name();
#endif    
    
    foreach(BoxComponent *r, redefines()) {
        if ( r!=c ) {
#ifdef BOXCOMPONENT_DEBUG
            qDebug()<<"BoxComponent::reset overriding"<<r->name();
#endif   
            if (!r->isArray()) {
                QBuffer b;
                b.setData(bytes());
                b.open(QBuffer::ReadWrite);
                r->pack(&b);
            } else {
                QBuffer b;
                b.setData(bytes());
                b.open(QBuffer::ReadWrite);
                foreach(BoxComponent *o, r->occurences()) {
                    if (o != r) {
                        o->resetState();
                        if (!b.atEnd()) {
                            o->pack(&b);
                        } else {
                            o->setErrorState("No more data. Packing suspended");
                        }
                    }
                }
            }
        }
    }

    if (isRedefine()) {
        if (!isArray()) {
            QBuffer b;
            b.setData(parent()->bytes());
            b.open(QBuffer::ReadWrite);
            pack(&b);
        } else {
            QBuffer b;
            b.setData(parent()->bytes());
            b.open(QBuffer::ReadWrite);
            foreach(BoxComponent *o, occurences()) {
                if (o != this) {
                    o->resetState();
                    if (!b.atEnd()) {
                        o->pack(&b);
                    } else {
                        o->setErrorState("No more data. Packing suspended");
                    }
                }
            }
        }        
    }
}

void
BoxComponent::childChanged(BoxComponent *c)
{
#ifdef BOXCOMPONENT_DEBUG
    qDebug()<<"BoxComponent::childChanged::"<<name()<<"("<<this<<")"<<"<--"<<c->name()<<"("<<c<<")";
#endif    

    if (!c->isRedefine()) {
        foreach(BoxComponent *r, redefines()) {
            if (!r->isArray()) {
                QBuffer b;
                b.setData(bytes());
                b.open(QBuffer::ReadWrite);
                r->pack(&b);
            } else {
                QBuffer b;
                b.setData(bytes());
                b.open(QBuffer::ReadWrite);
                foreach(BoxComponent *o, occurences()) {
                    o->resetState();
                    if (!b.atEnd()) {
                        o->pack(&b);
                    } else {
                        o->setErrorState("No more data. Packing suspended");
                    }
                }
            }
        }
    }  else { // redefine special case
#ifdef BOXCOMPONENT_DEBUG
        qDebug()<<"BoxComponent::childChanged:: with redefine child"<<name()<<"("<<this<<")"<<"<--"<<c->name()<<"("<<c<<")";
#endif    
        if (c->isAtom()) { //atom redefine
            Q_ASSERT(!((Atom*)c)->isBitmap());
            QByteArray bytes = c->bytes();
            QBuffer b;
            b.setData(bytes);
            b.open(QBuffer::ReadWrite);
#ifdef BOXCOMPONENT_DEBUG
            qDebug()<<"BoxComponent::childChanged:: packing"<<name()<<"with::"<<bytes;
#endif    
            this->pack(&b);
        } else { //box
#ifdef BOXCOMPONENT_DEBUG
            qDebug()<<"BoxComponent::childChanged:: with redefine child"<<name()<<"("<<this<<")"<<"<--"<<c->name()<<"("<<c<<")";
#endif
            if (!this->isArray()) {
                QBuffer b;
                b.setData(c->unpack());
                b.open(QBuffer::ReadWrite);
                pack(&b);
                b.close();
            } else {
                QBuffer b;
                b.setData(c->unpack());
                b.open(QBuffer::ReadWrite);
                foreach(BoxComponent *o, occurences()) {
                    o->resetState();
                    if (!b.atEnd()) {
                        o->pack(&b);
                    } else {
                        o->setErrorState("No more data. Packing suspended");
                    }
                }
                b.close();
            }
        }
    }

    if (c->isAtom()) {
        Atom *atom = (Atom*)(c);
        if (atom && !atom->vDependent() /*&& !atom->vDependee()*/) {
            emitChanged();
        }
    } else {
        emitChanged();
    }
}

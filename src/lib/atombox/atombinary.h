#ifndef ATOM_BINARY
#define ATOM_BINARY

#include <QtCore/QString>
#include <QtCore/QIODevice>
#include <QtCore/QByteArray>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptEngine>

#include "atom.h"

class AtomBinary : public Atom
{
    Q_OBJECT

public:
    enum Type { 
    INT8,
    INT16,
    INT32,
    INT64,
    UINT8,
    UINT16,
    UINT32,
    UINT64,
    FLOAT,
    DOUBLE
    };

    AtomBinary();
    AtomBinary(QString name, Definition &definition, Type type = UINT16);
    virtual ~AtomBinary();

    virtual ByteOrder byteOrder() { return byte_order_; }
    virtual void setByteOrder(ByteOrder o) { byte_order_ = o; resetBytes(); }

    virtual void clear();
    virtual BoxComponent* clone();
    virtual bool isBinary() const { return true; }
    virtual bool isSigned() const;

    Type getType() const { return type_; }
    void setType(Type type) { type_ = type; }

    virtual bool pack( QDataStream *ds );
    virtual QByteArray unpack();

    virtual void resetBytes();
    virtual bool setValue( QVariant val, bool emit_changed_flg );

private:
    Type      type_;
    ByteOrder byte_order_;

private:
    Q_DISABLE_COPY(AtomBinary)
};

#endif //ATOM_BINARY

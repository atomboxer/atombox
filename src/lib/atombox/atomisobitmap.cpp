
#include <QtCore/QString>
#include <QtCore/QDebug>
#include <QtCore/QVariant>

#include "atomisobitmap.h"
#include "atomclass.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


AtomIsoBitmap::AtomIsoBitmap() :
    Atom("undefined")
{
}

AtomIsoBitmap::AtomIsoBitmap(QString name, Definition &definition) :
    Atom(name, definition)
{
}

AtomIsoBitmap::~AtomIsoBitmap()
{
}


QByteArray
AtomIsoBitmap::bytes()
{
    return QByteArray::fromHex(bytes_);
}

void
AtomIsoBitmap::clear()
{
    Atom::clear();

    qlonglong v = 0;

    if (haveDefault()) {
        v = defaultValue().toLongLong();
    } 

    if (lpad() != 0 ||
        rpad() != 0) {
        qWarning(("LPAD/RPAD clause cannot be used with ISOBITMAP data type. Around"+
                  name()+" atom").toAscii().data());
    }

    setValue(v, true);
    if (state() == OK_State)
        setDefaultState();
}


#include <QtCore/QString>
#include <QtCore/QDebug>
#include <QtCore/QVariant>

#include "atomisobitmapbinary.h"
#include "atomclass.h"

#include <assert.h>
#include <iostream>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


//#define ATOMISOBITMAPBINARY_DEBUG

AtomIsoBitmapBinary::AtomIsoBitmapBinary() :
    AtomIsoBitmap()
{
}

AtomIsoBitmapBinary::AtomIsoBitmapBinary(QString name, Definition &definition)
    : AtomIsoBitmap(name, definition)
{
    bit_array_.resize((definition.length_ * 8)/2);
    length_ = definition.length_;
}

AtomIsoBitmapBinary::~AtomIsoBitmapBinary()
{
}

BoxComponent *
AtomIsoBitmapBinary::clone()
{
    AtomIsoBitmapBinary *cc = new AtomIsoBitmapBinary(name(), definition_);
    cc->setParent(parent());
    if (vDependent())
        cc->setvDependent(vDependent());
    if (vDependee())
        cc->setvDependee(vDependee());
    cc->setPrevSibling(prevSibling());
    cc->setNextSibling(nextSibling());
    cc->setRedefine(isRedefine());
    cc->setConstraintList(constraints());
    cc->setState(state());
    cc->setKey(isKey());
   
    if (isArray()) {
        cc->setOccurencesNumber(occurencesNumber());
        cc->setOccurencesNumber(occurencesDepend(),
                                 occurencesDependFrom(),
                                 occurencesDependTo());
        //cc->buildOccurences();
    }
    
    foreach (BoxComponent *r, redefines()) {
        BoxComponent *rc = r->clone();
        cc->addRedefine(rc);
    }

    return cc;
}

bool
AtomIsoBitmapBinary::pack( QDataStream *ds )
{
    Q_ASSERT(ds != NULL);
#ifdef ATOMISOBITMAPBINARY_DEBUG
    qDebug()<<"AtomIsoBitmapBinary::pack";
#endif
    setState(OK_State);
    
    if (length_type_ == VARIABLE_LENGTH ||
        length_type_ == VARIABLE_LENGTH_INCLUSIVE ||
        length_type_ == VARIABLE_LENGTH_ALLINCLUSIVE) {
        length_ = computevLength();
        if (length_ <=0 )
            return false;
    }
    
    char *buf = new char[length()+1];
    ds->readRawData(buf, length()/2);

    bool ok;

    setValue(QString(QByteArray(buf, length()/2).toHex()).toULongLong(&ok,16), false);

    emit packed(this);

    if (!ok) {
        delete []buf;
        setErrorState(name()+ " binary isobitmap is invalid. Packing suspended.");
        return false;
    }

    delete []buf;
    return true;
}

QByteArray
AtomIsoBitmapBinary::unpack()
{
    return bytes();
}


void
AtomIsoBitmapBinary::resetBytes()
{
    //
    //  Reset the bit_map_ field in parent
    //
    bool ok = false;
    qulonglong lval = value().toULongLong(&ok);
    Q_ASSERT(ok);

    QString bits = QString::number(lval, 2);

    bits = bits.rightJustified((length()*8)/2,'0',false);


    for (int i=0; i < bit_array_.size(); i++)
        if ( bits[i] == '1' )
            bit_array_.setBit(i);
        else
            bit_array_.clearBit(i);

    bytes_ = QString(QByteArray::number(lval, 16)).rightJustified(length(),'0').toAscii();
}

bool
AtomIsoBitmapBinary::setValue( QVariant val, bool emit_changed_flg )
{
    //resetState();

    if (val.isValid() &&
        val.canConvert(QVariant::ULongLong)) {

        bool ok = false;
        qulonglong lval = val.toULongLong(&ok);

        if (!ok)
            return false;

        value_ = (qlonglong)lval;

        resetBytes();
        setOKState();

        if (emit_changed_flg)
            emit changed(this);

        return true;
    }

    return false;
}

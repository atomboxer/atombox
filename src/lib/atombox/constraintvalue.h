#ifndef CONSTRAINT_VALUE
#define CONSTRAINT_VALUE

#include "constraint.h"

class ConstraintValue : public Constraint
{
public:
    ConstraintValue(QVariant value, QString &display );
    virtual ~ConstraintValue();
    virtual bool isMatch(QVariant v);
    QVariant value() const { return value_; }
    virtual Constraint* clone();
private:
    QVariant value_;
};
#endif

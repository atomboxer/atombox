#ifndef ATOM_NUMERIC_PACKED
#define ATOM_NUMERIC_PACKED

#include <QtCore/QString>
#include <QtCore/QIODevice>
#include <QtCore/QByteArray>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptEngine>

#include "atom.h"

class AtomNumericPacked : public Atom
{
    Q_OBJECT
public:
    AtomNumericPacked();
    AtomNumericPacked(QString name, Definition &definition);

    ~AtomNumericPacked();

    virtual void clear();
    virtual BoxComponent* clone();
    virtual bool isString() const { return false; }
    virtual bool isStringNumeric() const { return true; }

    virtual bool pack( QDataStream *ds );
    virtual QByteArray unpack();
    virtual void resetBytes();
    virtual bool setValue( QVariant val, bool emit_changed_flg );

private:
    Q_DISABLE_COPY(AtomNumericPacked)
};

#endif //ATOM_NUMERIC_PACKED

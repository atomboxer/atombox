#ifndef ATOM_ISOBITMAP_HEX
#define ATOM_ISOBITMAP_HEX

#include <QtCore/QString>
#include <QtCore/QBitArray>
#include <QtCore/QIODevice>
#include <QtCore/QByteArray>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptEngine>

#include "atomisobitmap.h"

class AtomIsoBitmapHex : public AtomIsoBitmap
{
    Q_OBJECT
public:
    AtomIsoBitmapHex();
    AtomIsoBitmapHex(QString name, Definition &definition);
    virtual ~AtomIsoBitmapHex();

    virtual BoxComponent* clone();
    virtual bool isIsoBitmap() const { return true; }
    virtual bool isString() const { return false; }
    virtual bool isStringNumeric() const { return false; }
    virtual bool pack( QDataStream *ds );
    virtual QByteArray unpack();
    virtual void resetBytes();
    virtual bool setValue( QVariant val, bool emit_changed_flg );

private:
    QByteArray bitsToBytes(QBitArray ba);
    QBitArray  bytesToBits(QByteArray hex);

private:
    Q_DISABLE_COPY(AtomIsoBitmapHex)
};

#endif //ATOM_ISOBITMAP_HEX

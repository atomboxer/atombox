
#include <QtCore/QBuffer>
#include <QtGlobal>
#include <QtCore/QString>
#include <QtCore/QDebug>
#include <QtCore/QVariant>

#include "atomstringalpha.h"
#include "atomclass.h"
#include "utils.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


AtomStringAlpha::AtomStringAlpha() :
    AtomString()
{
    //clear();
}


AtomStringAlpha::AtomStringAlpha(QString name,
                                 Definition &definition,
                                 Charset c)
    : AtomString(name,definition, c)
{
    if (lpad() == 0) {
        setRpad(' ');
    }
}


BoxComponent *
AtomStringAlpha::clone()
{
    AtomStringAlpha *cc = new AtomStringAlpha(name(), definition_, charset_);
    cc->setParent(parent());
    if (vDependent())
        cc->setvDependent(vDependent());
    if (vDependee())
        cc->setvDependee(vDependee());
    cc->setPrevSibling(prevSibling());
    cc->setNextSibling(nextSibling());
    cc->setRedefine(isRedefine());
    cc->setConstraintList(constraints());
    cc->setState(state());
    cc->setKey(isKey());
   
    if (isArray()) {
        //foreach (BoxComponent *occ, occurences()) {
        //    BoxComponent *occ_copy = occ->clone();
        //    cc->addOccurence(occ_copy);
        //}
        cc->setOccurencesNumber(occurencesNumber());
        cc->setOccurencesNumber(occurencesDepend(),
                                occurencesDependFrom(),
                                occurencesDependTo());
    }
    
    foreach (BoxComponent *r, redefines()) {
        BoxComponent *rc = r->clone();
        cc->addRedefine(rc);
    }

    return cc;
}

AtomStringAlpha::~AtomStringAlpha()
{
}

void
AtomStringAlpha::clear()
{
    AtomString::clear();
}

bool
AtomStringAlpha::pack( QDataStream *ds )
{
    AtomString::pack(ds);

    bool ok = true;
    for (int i=0; i<bytes().size();i++) {
        if (!(QChar(bytes()[i]).isPrint())) {
            ok = false;
            break;
        }
    }
    
    if (!ok) {
        setExceptionState(name()+" is not alpha.");
        value_ = value();
        return false;
    }


    if (constraints().isEmpty()) {
        return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints()) {
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }
    }

    if ( !c_ok ) {
        // qDebug()<<"a::"<<name().toLatin1().data()
        //          <<"::\""<<value().toString().toLatin1().data()<<"\""
        //          <<"::\""<<QString::fromLatin1(bytes().toHex())<<"\""
        //          <<"::lgth::"<<length();

        setExceptionState("Value does not match constraints");
    }

    return true;
}

QByteArray
AtomStringAlpha::unpack()
{
    QByteArray ret(bytes());
    if (length_type_ == VARIABLE_DELIMITED)
        if (nextSibling())
            ret.append(vlength_delimiter_);

    if ( charset_ == AtomString::EBCDIC ) {
        ASCII2EBCDIC(ret.data(),ret.length());
    }

    return ret;
}

void
AtomStringAlpha::resetBytes()
{
    if (length() == 0 || value_.isNull() || !value_.isValid() ||
        !value_.canConvert(QVariant::LongLong)) {
        bytes_ = QByteArray();
        return;
    }

    bool ok = false;
    qlonglong lvalue = value().toLongLong(&ok);
    if (!ok)
        qFatal(("Fatal error while setting atom:"+name()).toAscii().data());

    QString svalue = QString::number(lvalue);

    if (lpad() != 0) {
        svalue = svalue.rightJustified(length(), lpad());
    } else {
        svalue = svalue.rightJustified(length(), '0');
    }

    bytes_ = svalue.toAscii();

    if ( charset_ == AtomString::EBCDIC ) {
        ASCII2EBCDIC(bytes_.data(), length());
    }
}

void
AtomStringAlpha::resize(size_t new_size)
{
    if (new_size <= 0) {
        length_ = 0;
        bytes_ = QByteArray();
        value_ = QVariant();
        return;
    }

    //FIXED with default
    QString v(new_size, '0');
    v.replace(0, qMin<int>(bytes().length(), new_size),
              bytes().mid(0, qMin<int>(bytes().length(), new_size)));

    if (lpad() != 0) {
        if (lengthType() != VARIABLE_LENGTH && 
            lengthType() != VARIABLE_LENGTH_INCLUSIVE &&
            lengthType() != VARIABLE_LENGTH_ALLINCLUSIVE)
            v = v.rightJustified(new_size, lpad());
    } else if (rpad() != 0) {
        if (lengthType() != VARIABLE_LENGTH && 
            lengthType() != VARIABLE_LENGTH_INCLUSIVE &&
            lengthType() != VARIABLE_LENGTH_ALLINCLUSIVE)
            v = v.leftJustified(new_size, rpad());
    }

    bool ok = false;
    qlonglong vv = v.toLongLong(&ok);
    if (!ok) {
        setExceptionState("Invalid atom");
        return;
    }

    if (lengthType() == VARIABLE_LENGTH || 
        lengthType() == VARIABLE_LENGTH_INCLUSIVE ||
        lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE) {
        length_ = new_size;
    }

    value_ = vv;
    bytes_ = v.toAscii();
}

bool
AtomStringAlpha::setValue(QVariant val, bool emit_changed_flg)
{
    //resetState();

    if (val.isValid() &&
        val.canConvert(QVariant::LongLong)) {

        bool ok = false;
        qlonglong lval = val.toLongLong(&ok);
        if (!ok)
            return false;

        QString sval = QString::number(lval);

        if (lengthType() == VARIABLE_LENGTH ||
            lengthType() == VARIABLE_LENGTH_INCLUSIVE ||
            lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE ||
            lengthType() == VARIABLE_DELIMITED) {
            length_ = qMin<int>(maxLength(), value().toString().size());
        }
         
        sval = sval.rightJustified(length(), lpad());
        value_ = sval.toLongLong();

        resetBytes();
        setOKState();
        
        if (emit_changed_flg)
            emit changed(this);
    } else {
        setErrorState("Invalid value in set method");
        return false;
    }

    if (constraints().isEmpty()) {
        return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints()) {
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }
    }

    if (!c_ok) {
        setExceptionState("Value does not match constraints");
        return false;
    }

    return true;
}

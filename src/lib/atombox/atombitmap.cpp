#include "utils.h"

#include "atombitmap.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


//#define ATOMBITMAP_DEBUG

AtomBitmap::AtomBitmap() :
    Atom("undefined")
{
}

AtomBitmap::AtomBitmap(QString name, Definition &definition) :
    Atom(name, definition)
{
    if (definition_.data_type_ == UBITMAP)
        signed_flg_ = false;
    else if (definition_.data_type_ == BITMAP)
        signed_flg_ = true;
    else 
        qFatal("AtomBitmap::AtomBitmap programmatic error");

    length_ = definition.length_;
    bit_array_ = QBitArray(length_);
    value_ = 0;
}

AtomBitmap::~AtomBitmap()
{
}

QByteArray
AtomBitmap::bytes()
{
    //qFatal("AtomBitmap::bytes programmatic error");
    return QByteArray();
}

QBitArray
AtomBitmap::bits()
{
    return bit_array_;
}


void
AtomBitmap::clear()
{
    Atom::clear();

    qlonglong v = 0;

    if (haveDefault()) {
        v = defaultValue().toLongLong();
    } 

    if (lpad() != 0 ||
        rpad() != 0) {
        qWarning(("LPAD/RPAD clause cannot be used with BITMAP data type. Around"+
                  name()+" atom").toAscii().data());
    }

    setValue(v, true);
    if (state() == OK_State)
        setDefaultState();
}

BoxComponent *
AtomBitmap::clone()
{
    AtomBitmap *cc = new AtomBitmap(name(), definition_);
    cc->setParent(parent());
    if (vDependent())
        cc->setvDependent(vDependent());
    if (vDependee())
        cc->setvDependee(vDependee());
    cc->setPrevSibling(prevSibling());
    cc->setNextSibling(nextSibling());
    cc->setRedefine(isRedefine());
    cc->setConstraintList(constraints());
    cc->setState(state());
    cc->setKey(isKey());
   
    if (isArray()) {
        cc->setOccurencesNumber(occurencesNumber());
        cc->setOccurencesNumber(occurencesDepend(),
                                 occurencesDependFrom(),
                                 occurencesDependTo());
        //cc->buildOccurences();
    }
    
    foreach (BoxComponent *r, redefines()) {
        BoxComponent *rc = r->clone();
        cc->addRedefine(rc);
    }
    return cc;
}

bool
AtomBitmap::pack( QDataStream *ds)
{
    qFatal("AtomBitmap::pack programmatic error");
    return false;
}

bool
AtomBitmap::bpack( QBitArray &barr )
{
#ifdef ATOMBITMAP_DEBUG
#endif

    bit_array_ = barr;

    bool ok = false;
    int v = 0;
	for(uint i = 0; i < barr.size(); i++) {
		v <<= 1;
        if (signed_flg_)
            v += (qint32)barr.at(i);
        else
            v += (quint32)barr.at(i);
	}

    value_ = v;

    //emit packed();

#ifdef ATOMBITMAP_DEBUG
    QString text;
    for (int i = 0; i < bit_array_.size(); ++i)
        text += bit_array_.testBit(i) ? "1": "0";
    qDebug()<<"AtomBitmap::pack::"<<text;
#endif

    if (constraints().isEmpty()) {
       return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints()) {
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }
    }

    if (!c_ok) {
        setExceptionState("Value does not match constraints");
    }

    return true;
}


QByteArray
AtomBitmap::unpack()
{
#ifdef ATOMBITMAP_DEBUG
#endif
    return bitsToBytes(bit_array_);
}

QBitArray
AtomBitmap::bunpack()
{
#ifdef ATOMBITMAP_DEBUG
#endif
    return bit_array_;
}

void 
AtomBitmap::resetBits()
{
    if (signed_flg_) {
        qint32 v = value().toInt();
        bit_array_ = toQBit(v);
    } else {
        quint32 v = value().toUInt();
        bit_array_ = toQBit(v);
    }
    bit_array_.resize(length_);

#ifdef ATOMBITMAP_DEBUG
    QString text;
    for (int i = 0; i < bit_array_.size(); ++i)
        text += bit_array_.testBit(i) ? "1": "0";
    qDebug()<<"AtomBitmap::resetBits::"<<text;
#endif
}

void 
AtomBitmap::resetBytes()
{
#ifdef ATOMBITMAP_DEBUG
#endif
    qFatal("AtomBitmap::resetBytes programmatic error");
}

bool 
AtomBitmap::setValue(QVariant val, bool emit_changed_flg)
{
#ifdef ATOMBITMAP_DEBUG
#endif
    if (val.isValid() &&
        val.canConvert(QVariant::Int)) {
        
        bool    ok = false;
        qint32 lval = val.toInt(&ok);

        value_ = lval;
        resetBits();

        if (emit_changed_flg)
            emit changed(this); 


    } else {
        setErrorState("Invalid value in set method");
        return false;
    }   

    setOKState();
         
    if (constraints().isEmpty()) {
        return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints()) {
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }
    }

    if (!c_ok) {
        setExceptionState("Value does not match constraints");
        return false;
    }
   
    return true;

}


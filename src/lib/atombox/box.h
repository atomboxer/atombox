#ifndef BOX_H
#define BOX_H

#include <QtCore/QDebug>
#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QIODevice>
#include <QtCore/QByteArray>
#include <QtCore/QBitArray>
#include <QtScript/QScriptEngine>

#include "boxcomponent.h"

class Box : public BoxComponent
{
    Q_OBJECT

public:
    Box();
    Box(QString name);
    ~Box();
    //
    //  Add a nested Box within this Box
    //
    void add( BoxComponent *c);
   
    virtual QByteArray bytes();

    virtual QList<BoxComponent *> childs() const;
    virtual void clear();
    virtual void reindex();
    virtual BoxComponent* clone();
    virtual void setByteOrder(ByteOrder o);
    virtual void setChoice(bool t) { choice_ = t; }
    virtual void dump(ushort max_levels = 1) const;
    BoxComponent::State state() const;
    virtual bool isAtom() const { return false; }
    virtual bool isChoice() const { return choice_; }
    virtual bool pack( QDataStream *d );
    virtual bool pack( QIODevice *i ) { return BoxComponent::pack(i); }
    virtual QByteArray unpack();
    virtual ssize_t length() const;
    virtual QString toJSON( bool use_redefines = false );

private:
    QList<BoxComponent*> childs_;
    bool choice_;

private:
    Q_DISABLE_COPY(Box)
};

#endif // BOX_H


#include <QtCore/QString>
#include <QtCore/QDebug>
#include <QtCore/QVariant>
#include <QtCore/QBuffer>

#include "atomnumericpacked.h"
#include "atomclass.h"
#include "atomstring.h"

#include "utils.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

AtomNumericPacked::AtomNumericPacked() :
  Atom("undefined")
{
    if (lpad() != 0 || rpad() != 0) {
        qFatal("Fatal error. LPAD/RPAD clause with PACKED DECIMAL atom.");
    }

    value_ = QVariant();
}


AtomNumericPacked::AtomNumericPacked( QString name,
                                      Definition &definition )
    : Atom(name, definition)
{
    if (lpad() != 0 || rpad() != 0) {
        qFatal("Fatal error. LPAD/RPAD clause with PACKED DECIMAL atom.");
    }

    value_ = QVariant();
}

AtomNumericPacked::~AtomNumericPacked()
{

}

void
AtomNumericPacked::clear()
{
    Atom::clear();

    bool ok;
    setValue(defaultValue().toLongLong(&ok), true);
    if (state() == OK_State)
        setDefaultState();
    
    if (lpad() != 0 ||
        rpad() != 0) {
        qWarning(("LPAD/RPAD clause cannot be used with PACKED DECIMAL data type. Around" +
                  name()+" atom").toAscii().data());
    }    
}

BoxComponent *
AtomNumericPacked::clone()
{
    AtomNumericPacked *cc = new AtomNumericPacked(name(), definition_);
    cc->setParent(parent());
    if (vDependent())
        cc->setvDependent(vDependent());
    if (vDependee())
        cc->setvDependee(vDependee());
    cc->setPrevSibling(prevSibling());
    cc->setNextSibling(nextSibling());
    cc->setRedefine(isRedefine());
    cc->setConstraintList(constraints());
    cc->setState(state());
    cc->setKey(isKey());
   
    if (isArray()) {
        cc->setOccurencesNumber(occurencesNumber());
        cc->setOccurencesNumber(occurencesDepend(),
                                 occurencesDependFrom(),
                                 occurencesDependTo());
        //cc->buildOccurences();
    }
    
    foreach (BoxComponent *r, redefines()) {
        BoxComponent *rc = r->clone();
        cc->addRedefine(rc);
    }

    return cc;
}

bool
AtomNumericPacked::pack( QDataStream *ds )
{
    Q_ASSERT(ds != NULL);
    
    setState(OK_State);
    
    if (length_type_ == VARIABLE_LENGTH ||
        length_type_ == VARIABLE_LENGTH_INCLUSIVE ||
        length_type_ == VARIABLE_LENGTH_ALLINCLUSIVE) {
        
        length_ = computevLength();

        if (length_ < 0) {
            setErrorState("Invalid Length. Packing suspended");
            return false;
        } else if (length_ == 0)
            return true;
    }


    size_t bin_len = (length()/2) ;

    char *buf = new char[bin_len+1];
    bool  has_fill = (length()%2 == 0)?false:true;

    bool ok = false;
    if (has_fill) {
        ds->readRawData(buf, bin_len+1);
        setValue(QByteArray(buf, bin_len+1).toHex().toLongLong(&ok), false);
    } else {
        ds->readRawData(buf, bin_len);
        setValue(QByteArray(buf, bin_len).toHex().toLongLong(&ok), false);
    }

    emit packed(this);

    if ( !ok ) {
        delete []buf;
        setExceptionState("Value does not match constraints");
        return false;
    }

    delete []buf;

    if (constraints().isEmpty()) {
        // qDebug()<<"NP NO CONSTRAINTS: n::"<<name().toLatin1().data()
        //       <<"::\""<<value().toString().toLatin1().data()<<"\""
        //      <<"::\""<<QString::fromLatin1(bytes().toHex())<<"\""
        //      <<"::lgth::"<<length();
        return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints())
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }

    if (!c_ok) {
        setExceptionState("Value does not match constraints");
    }

    // qDebug()<<"NP MATCHED:"<<c_ok<<" n::"<<name().toLatin1().data()
    //           <<"::\""<<value().toString().toLatin1().data()<<"\""
    //           <<"::\""<<QString::fromLatin1(bytes().toHex())<<"\""
    //           <<"::lgth::"<<length();
    return c_ok;
}

QByteArray
AtomNumericPacked::unpack()
{
    return bytes();
}

void
AtomNumericPacked::resetBytes()
{
    if (length() == 0 || value_.isNull() || !value_.isValid()) {
        bytes_ = QByteArray();
        return;
    }

    size_t bin_length = length();
    if ( bin_length%2 )
        bin_length+=1;

    QString value_s(bin_length,'0');
    QString v = value().toString();
    value_s = value_s.replace(bin_length - v.length(),v.length(), v);
    bytes_ = QByteArray::fromHex(value_s.toAscii());
}

bool
AtomNumericPacked::setValue(QVariant val, bool emit_changed_flg)
{
    //resetState();

    if (val.isValid() &&
        val.canConvert(QVariant::LongLong)) {
        
        bool ok;
        qlonglong lval = val.toLongLong(&ok);
        if (!ok) {
            setErrorState("Invalid value in set method");
            return false;
        }

        QString sval = QString::number(lval);

        value_ = lval;

        resetBytes();
        setOKState();
        
        if (emit_changed_flg)
            emit changed(this);

    } else {
        setErrorState("Invalid value in set method");
        return false;
    }

    if (constraints().isEmpty()) {
        return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints()) {
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }
    }

    if (!c_ok) {
        setExceptionState("Value does not match constraints");
        return false;
    }

    return false;
}

#include "constraintstring.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

ConstraintString::ConstraintString( QString value, QString &display )
    : Constraint (display), value_(value)
{
    kind_ = k_string;
}

ConstraintString::~ConstraintString()
{}

bool 
ConstraintString::isMatch(QVariant v)
{ 
    if (value_== v.toString().simplified()) {
        setMatchedValue(v);
        return true;
    }

    return false;
}

Constraint*
ConstraintString::clone()
{
    ConstraintString * _clone = new ConstraintString(this->value_, this->display_);
    _clone->kind_ = this->kind();
    _clone->matched_value_ = this->matchedValue();
    return _clone;
}

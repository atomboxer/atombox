#include <QtCore/QString>
#include <QtCore/QDebug>
#include <QtCore/QVariant>
#include <QtCore/QBuffer>

#include "atombinary.h"
#include "atomclass.h"
#include "atomstring.h"

#include <assert.h>
#include <iostream>

#include <stdio.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

AtomBinary::AtomBinary() :
    Atom()
{
    clear();
}

AtomBinary::AtomBinary(QString name, Definition &definition, Type type)
    : Atom(name, definition),
      type_(type)
{
    size_t length = 0;
    switch(type) {
    case INT8:
    case UINT8:  length = 1; break;
    case INT16:
    case UINT16: length = 2; break;
    case INT32:
    case UINT32:
    case FLOAT:  length = 4; break;
    case INT64:
    case UINT64:
    case DOUBLE: length = 8; break;
    }

    definition.length_ = length;
    setLength(length);
    value_ = QVariant();
    byte_order_ = definition_.byte_order_;

    clear();
}

AtomBinary::~AtomBinary()
{
}

void
AtomBinary::clear()
{
    Atom::clear();

    QVariant v(char(0));

    if (haveDefault()) {
        v = defaultValue();
    }

    if (lpad() != 0 ||
        rpad() != 0) {
        qWarning(("LPAD/RPAD clause cannot be used with Binary data type. Around"+
                  name()+" atom").toAscii().data());
    }

    setValue(v, true);
    if (state() == OK_State)
        setDefaultState();
}

BoxComponent*
AtomBinary::clone()
{
    AtomBinary *cc = new AtomBinary(name(), definition_, type_);
    cc->setParent(parent());
    if (vDependent())
        cc->setvDependent(vDependent());
    if (vDependee())
        cc->setvDependee(vDependee());

    cc->setPrevSibling(prevSibling());
    cc->setNextSibling(nextSibling());
    cc->setRedefine(isRedefine());
    cc->setConstraintList(constraints());
    cc->setState(state());
    cc->setKey(isKey());
    if (isArray()) {
        cc->setOccurencesNumber(occurencesNumber());
        cc->setOccurencesNumber(occurencesDepend(),
                                 occurencesDependFrom(),
                                 occurencesDependTo());
        //cc->buildOccurences();
    }
    
    foreach (BoxComponent *r, redefines()) {
        BoxComponent *rc = r->clone();
        cc->addRedefine(rc);
    }
    
    return cc;
}

bool
AtomBinary::isSigned() const 
{
    switch(type_) {
    case INT8:
    case INT16:
    case INT32:
    case INT64: return true;
    default:    return false;
    }

    return false;
}

bool
AtomBinary::pack( QDataStream *ds )
{
    Q_ASSERT( ds != NULL );
    Q_ASSERT( length_type_ != VARIABLE_LENGTH ||
              length_type_ != VARIABLE_LENGTH_INCLUSIVE ||
              length_type_ != VARIABLE_LENGTH_ALLINCLUSIVE  );

    setState(OK_State);
    
    char *buf = new char[length()+1];
    int rl = ds->readRawData(buf, length());

    if (rl == 0 || buf == 0) {
        delete []buf;
        setErrorState("Empty atom");
        return false;
    }

    QByteArray ba(buf,rl);

    if (ba.length() ==0 ) {
        delete []buf;
        setErrorState("Empty atom");
        return false;
    }

    bytes_ = ba;

    QVariant lval;

    QDataStream _tmp(&bytes_, QIODevice::ReadOnly );
    if (byte_order_ != DefaultByteOrder) {
        if (byte_order_ == BigEndian) {
            _tmp.setByteOrder(QDataStream::BigEndian);
        } else {
            _tmp.setByteOrder(QDataStream::LittleEndian);
        }
    }

    if (_tmp.status() != QDataStream::Ok) {
        delete []buf;
        return false;
    }

    switch(type_) {
    case INT8: {
        qint8 val;
        _tmp >> val;
        lval = QVariant(val);
    } break;
    case UINT8: {
        quint8 val;
        _tmp >> val;
        lval = QVariant(val);
    } break;
    case INT16: {
        qint16 val;
        _tmp >> val;
        lval = QVariant(val);
    } break;
    case UINT16: {
        quint16 val;
        _tmp >> val;
        lval = QVariant(val);
    } break;
    case INT32: {
        //qDebug()<<"name::"<<name<<"INT32";
        qint32 val;
        _tmp >> val;
        lval = QVariant(val);
    } break;
    case UINT32: {
        //qDebug()<<"name::"<<name<<"UINT32";
        quint32 val;
        _tmp >> val;
        lval = QVariant(val);
    }  break;
    case INT64: {
        qint64 val;
        _tmp >> val;
        lval = QVariant(val);
    } break;
    case UINT64: {
        quint64 val;
        _tmp >> val;
        lval = QVariant(val);
    }break;
    case FLOAT: {
        float val;
        _tmp.setFloatingPointPrecision(QDataStream::SinglePrecision);
        _tmp >> val;
        lval = QVariant(val);
    } break;
    case DOUBLE:{
        double val;
        _tmp.setFloatingPointPrecision(QDataStream::DoublePrecision);
        _tmp >> val;
        lval = QVariant(val);
    }break;
    }

    //
    //  Set the value
    //
    value_ = lval;

    emit packed(this);

    delete []buf;

    if (constraints().isEmpty()) {
        // std::cout <<"ab::"<<name().toLatin1().data()
        //           <<"::"<<value().toString().toLatin1().data()
        //           <<"::lgth::"<<length()<<std::endl;
        return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints()) {
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }
    }

    if (!c_ok) {
        setExceptionState("Value does not match constraints");
    }

    // std::cout<<"ab::"<<name().toLatin1().data()
    //          <<"::"<<value().toString().toLatin1().data()
    //          <<"::lgth::"<<length()<<std::endl;

    return false;
}

QByteArray
AtomBinary::unpack()
{
    return bytes();
}

void
AtomBinary::resetBytes()
{

    bytes_.clear();
    QDataStream _tmp(&bytes_, QIODevice::WriteOnly );

    if (byte_order_ != DefaultByteOrder) {
        if (byte_order_ == BigEndian) {
            _tmp.setByteOrder(QDataStream::BigEndian);
        } else {
            _tmp.setByteOrder(QDataStream::LittleEndian);
        }
    }

    switch(type_) {
    case INT8: {
        qint8 val = value().toInt();
        _tmp << val;
    } break;
    case UINT8: {
        quint8 val = value().toUInt();;
        _tmp << val;
    } break;
    case INT16: {
        qint16 val = value().toInt();
        _tmp << val;
    } break;
    case UINT16: {
        quint16 val = value().toUInt();
        _tmp << val;
    } break;
    case INT32: {
        qint32 val = value().toInt();
        _tmp << val;
    } break;
    case UINT32: {
        quint32 val = value().toUInt();
        _tmp << val;
    }  break;
    case INT64: {
        qint64 val = value().toLongLong();
        _tmp << val;
    } break;
    case UINT64: {
        quint64 val = value().toULongLong();
        _tmp << val;
    }  break;
    case FLOAT: {
        float val = value().toFloat();
        _tmp.setFloatingPointPrecision(QDataStream::SinglePrecision);
        _tmp << val;
    } break;
    case DOUBLE: {
        double val = value().toDouble();
        _tmp.setFloatingPointPrecision(QDataStream::DoublePrecision);
        _tmp << val;
    } break;
    }
}


bool
AtomBinary::setValue(QVariant val, bool emit_changed_flg)
{
    //resetState();

    if (val.isValid()) {
        
        bool ok = true;
     
        switch(type_) {
        case INT8: {
            value_ = QVariant((char)val.toInt(&ok));
        } break;
        case UINT8: {
            value_ = QVariant((uchar)val.toInt(&ok));
        } break;
        case INT16: {
            value_ = QVariant((short)val.toInt(&ok));
        } break;
        case UINT16: {
            value_ = QVariant((ushort)val.toUInt(&ok));
        } break;
        case INT32: {
            value_ = QVariant(val.toInt(&ok));
        } break;
        case UINT32: {
            value_ = QVariant(val.toUInt(&ok));
        }  break;
        case INT64: {
            value_ = QVariant(val.toLongLong(&ok));
        } break;
        case UINT64: {
            value_ = QVariant(val.toULongLong(&ok));
        } break;
        case FLOAT: {
            value_ = QVariant(val.toDouble(&ok));
        } break;
        case DOUBLE: {
            value_ = QVariant(val.toDouble(&ok));
        } break;
        }

        if (!ok) {
            setExceptionState("Invalid assignment");
            return false;
        } else {
            setOKState();
        }
        
        resetBytes();
        if (emit_changed_flg)
            emit changed(this);

    } else {
        setErrorState("Invalid value in set method");
        return false;
    }

    if (constraints().isEmpty()) {
        return true;
    }

    bool c_ok = false;
    foreach (Constraint *c, constraints()) {
        if (c->isMatch(QVariant(value()))) {
            c_ok = true;
            break;
        }
    }

    if (!c_ok) {
        setExceptionState("Value does not match constraints");
        return false;
    }

    return true;
}

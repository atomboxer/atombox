#ifndef ATOM_BITMAP
#define ATOM_BITMAP

#include <QtCore/QString>
#include <QtCore/QIODevice>
#include <QtCore/QByteArray>
#include <QtCore/QBitArray>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptEngine>

#include "atom.h"

class AtomBitmap : public Atom
{
    Q_OBJECT
public:
    AtomBitmap();
    AtomBitmap(QString name, Definition &definition);
    virtual ~AtomBitmap();

    virtual QBitArray  bitArray() const { return bit_array_; };
    virtual void clear();
    virtual BoxComponent* clone();
    virtual bool isIsoBitmap() const { return false; }
    virtual bool isBitmap() const { return true; }
    virtual bool isString() const { return false; }
    virtual bool isStringNumeric() const { return false; }

    virtual QByteArray bytes();
    virtual QBitArray  bits();
    virtual bool bpack( QBitArray &barr );
    virtual bool pack( QDataStream *ds );
    virtual QByteArray unpack();
    virtual QBitArray  bunpack();
    virtual void resetBytes();
    virtual void resetBits();

    virtual bool setValue(QVariant val, bool emit_changed_flg);

protected:
    QBitArray bit_array_;
    
private:
    bool signed_flg_;

private:
    Q_DISABLE_COPY(AtomBitmap)
};

#endif //ATOM_BITMAP

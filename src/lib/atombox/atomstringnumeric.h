#ifndef ATOM_STRING_NUMERIC
#define ATOM_STRING_NUMERIC

#include <QtCore/QString>
#include <QtCore/QIODevice>
#include <QtCore/QByteArray>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptEngine>

#include "atomstring.h"

class AtomStringNumeric : public AtomString 
{
    Q_OBJECT
public:
    AtomStringNumeric();
    AtomStringNumeric( QString name, Definition &d,
               Charset c = ASCII );
    ~AtomStringNumeric();

    virtual void clear();
    virtual BoxComponent* clone();
    virtual bool isStringNumeric() const { return true; }

    virtual bool pack( QDataStream *ds );
    virtual QByteArray unpack();
    virtual void resetBytes();
    virtual void resize(size_t new_size);
    virtual bool setValue(QVariant val, bool emit_changed_flg);
    virtual QVariant value() const;

private:
    Q_DISABLE_COPY(AtomStringNumeric)
};

#endif //ATOM_STRING_NUMERIC

#include <QtCore/QBuffer>
#include <QtCore/QBitArray>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QDebug>

#include <assert.h>

#include "atom.h"
#include "atombitmap.h"
#include "atomisobitmap.h"
#include "box.h"
#include "boxclass.h"
#include "utils.h"

#include <iostream>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define BOX_DEBUG

static void
appendBitArray(QBitArray &a1, QBitArray a2)
{
    if (a1.count() == 0) {
        a1 = a2;
        return;
    }

    int idx_a1 = a1.count();
    a1.resize(a1.count()+a2.count());
    int idx_a2 = 0;

    while (idx_a2<a2.count()) {
        a1.setBit(idx_a1,a2.testBit(idx_a2));
        idx_a1++;
        idx_a2++;
    }
}

Box::Box():
    BoxComponent("undefined"),
    choice_(false)
{
    childs_ = QList<BoxComponent*>();

    
}

Box::Box( QString name ):
    BoxComponent(name),
    choice_(false)
{
    childs_ = QList<BoxComponent*>();
}

Box::~Box()
{
    //qDebug()<<"Box::~Box()"<<name();
}

void
Box::add( BoxComponent *c )
{
    if (c->isRedefine()) {
        addRedefine(c);
        return;
    }

    //else {
    if (!childs_.isEmpty()) {
        BoxComponent *last = childs_.last();
        last->setNextSibling(c);
        c->setPrevSibling(last);
    } else {
        c->setNextSibling(NULL);
        c->setPrevSibling(NULL);
    }

    childs_.push_back(c);
    //}
    c->setParent(this);

}

QByteArray
Box::bytes()
{
    QByteArray ret = unpack();
    //Q_ASSERT(ret.length()!=0);
    return ret;

    // QByteArray ret;

    // if (!isArray())
    //     foreach(BoxComponent *c,childs()) {
    //         ret.push_back(c->bytes());
    //     }
    // else {
    //     QList<BoxComponent *> occ = occurences();
    //     for (int i=0;i<occurencesNumber();i++) { 
    //         ret.push_back(occ.at(i)->bytes());
    //     }
    // }

    // Q_ASSERT(ret.length() != 0);
    //return ret;
}

QList<BoxComponent *>
Box::childs() const
{
    return childs_;
}


void
Box::clear()
{

    connectDependents();
    
    QBitArray bitmap;
    int  de_idx = 0;
    bool count_de = false;

    resetState();

    foreach(BoxComponent *c, childs()) {
        if (!count_de) { //elements are not counted
            if (!c->isArray()) {
                c->clear();
            } else {
                QList<BoxComponent *> lst_o = c->occurences();
                
                for (size_t i=0;i<c->occurencesNumber();i++) {
                    if(c != lst_o.at(i)) {
                        lst_o.at(i)->clear();
                    }
                }
                
                for (size_t i=c->occurencesNumber();i<c->maxOccurencesNumber();i++) {
                    if(c != lst_o.at(i)) {
                        lst_o.at(i)->setState(Default_State);
                    }
                }
            }
        } else { //elements are counted
            if (!c->isArray()) {
                if ( de_idx < bitmap.count() && bitmap.testBit(de_idx) ) {
                    c->clear();
                } else { //flag is off
                    de_idx++;
                    c->setErrorState("Bit not set");
                }
                de_idx++;
            } else {
                QList<BoxComponent *> occ = c->occurences();
                int nocc = c->occurencesNumber();
                for (int i=0;i<occ.length();i++) {
                    if ( i<nocc && de_idx < bitmap.count() && bitmap.testBit(de_idx) ) {
                        occ.at(i)->clear();
                    } else {
                        occ.at(i)->setErrorState("Bit not set");
                        //qDebug()<<"state:"<<occ.at(i)->state();
                    }

                    de_idx++;
                }
            }
        } //else

        //
        //  ISO Bitmap processing
        //
        if (c->isAtom() && ((Atom*)c)->isIsoBitmap()) {
            AtomIsoBitmap *abitmap = dynamic_cast<AtomIsoBitmap*>(c);
            Q_ASSERT(abitmap);
            appendBitArray(bitmap, abitmap->bitArray());
#ifdef BOX_DEBUG
                QString dis;
                for (int i=0; i<abitmap->bitArray().size();i++) {
                    if ((abitmap->bitArray()).testBit(i))
                        dis+="1";
                    else
                        dis+="0";
                }           
                qDebug()<<"bitmap:"<<dis;
#endif
            if (!count_de)
                count_de = true;
        }
    } //foreach
}


void
Box::reindex()
{
    unsigned idx = 1;
    foreach(BoxComponent *c, childs()) {
        if (c->isAtom() && ((Atom*)c)->isIsoBitmap()) {
            idx = 1;
        }

        if (c->isArray()) {
            QList<BoxComponent *> lst_o = c->occurences();
            for (size_t i=0;i<c->occurencesNumber();i++) {
                if(c != lst_o.at(i)) {
                    lst_o.at(i)->setIdx(idx++);
                }
            }

            for (size_t i=c->occurencesNumber();i<c->maxOccurencesNumber();i++) {
                if(c != lst_o.at(i)) {
                    lst_o.at(i)->setIdx(idx++);
                }
            }
        } else {
            c->setIdx(idx++);        
        }
    }
}

BoxComponent*
Box::clone()
{
    Box *box = new Box(name());
    box->setParent(parent());
    box->setPrevSibling(prevSibling());
    box->setNextSibling(nextSibling());
    box->setChoice(isChoice());
    box->setRedefine(isRedefine());
    box->setState(state());
    box->setDisplayString(displayString());
    box->setKey(isKey());
    box->setRecord(isRecord());

    if (isArray()) {
        box->setOccurencesNumber(occurencesNumber());
        box->setOccurencesNumber(occurencesDepend(),
                                 occurencesDependFrom(),
                                 occurencesDependTo());
        //box->buildOccurences();

        for (int i=0; i<this->maxOccurencesNumber();i++) {
            BoxComponent *occ = box->occurences().at(i);
            foreach (BoxComponent *child, childs()) {
                BoxComponent *copy = child->clone();
                occ->add(copy);
                if (copy->isAtom() &&
                    (((Atom*)copy)->lengthType() == VARIABLE_LENGTH ||
                     ((Atom*)copy)->lengthType() == VARIABLE_LENGTH_INCLUSIVE ||
                     ((Atom*)copy)->lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE))
                    ((Atom*)copy)->resetvDependencies();
            }
        }

    } else {
        foreach (BoxComponent *r, redefines()) {
            BoxComponent *copy = r->clone();
            box->addRedefine(copy);
            if (copy->isAtom() && 
                (((Atom*)copy)->lengthType() == VARIABLE_LENGTH ||
                 ((Atom*)copy)->lengthType() == VARIABLE_LENGTH_INCLUSIVE ||
                 ((Atom*)copy)->lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE))

                ((Atom*)copy)->resetvDependencies();
        }

        foreach (BoxComponent *child, childs()) {
            BoxComponent *copy = child->clone();
            box->add(copy);
            if (copy->isAtom() && 
                (((Atom*)copy)->lengthType() == VARIABLE_LENGTH ||
                 ((Atom*)copy)->lengthType() == VARIABLE_LENGTH_INCLUSIVE ||
                 ((Atom*)copy)->lengthType() == VARIABLE_LENGTH_ALLINCLUSIVE))
                ((Atom*)copy)->resetvDependencies();
        }

    }

    return box;
}

static ushort cur_dump_lvl = 0;

void
Box::dump( ushort levels ) const
{
    if (cur_dump_lvl < levels) {
        foreach(BoxComponent *c, childs()) {
            c->dump();
        }
        cur_dump_lvl++;
    }
}

bool
Box::pack( QDataStream *ds )
{
    if ( ds == NULL ) {
        qFatal("fatal error");
    }

    QBitArray bitmap;
    int de_idx = 0;
    bool count_de = false;
    setOKState();
    size_t choice_sav_pos = 0;

    QIODevice *device = ds->device();

#ifdef BOX_DEBUG
    qDebug()<<"box.cpp > packing box:"+name();
#endif

    if (device->atEnd()) {
        //emit packed();
        return false;
    }

    if (isChoice()) {
        choice_sav_pos = device->pos();
    }

    if (isArray()) {
        QList<BoxComponent *> occ = occurences();
        int nocc = occurencesNumber();
        for (int i=0;i<occ.length();i++) {
            occ.at(i)->setOKState();
            if (i<nocc && !device->atEnd()) {
                occ.at(i)->pack(ds);
            } else {
                occ.at(i)->clear();
                occ.at(i)->setDefaultState();
            }
        }
    } else {
        for(int idx=0; idx < childs().size();idx++) {
            BoxComponent *c = childs().at(idx);

            c->setOKState();
            
            // BIT Groups
            if (c->isAtom() && ((Atom*)c)->isBitmap()) {
                Atom *bb = (Atom*)c;
                AtomBitmap *ab = (AtomBitmap*)c;
                size_t gsize = bb->length();

                while (bb->nextSibling() && bb->nextSibling()->isAtom() &&
                       ((Atom*)(bb->nextSibling()))->isBitmap()) {
                    bb = (Atom*)(bb->nextSibling());
                    gsize += bb->length();
                    idx ++;
                }

                if (gsize % 8 != 0)
                    qFatal("Box::unpack, a bit array group need to be \% 8"); 

                char *chunk = new char[gsize/8+1];
                int rl = ds->readRawData(chunk, gsize/8);
                if (rl != gsize/8 || chunk == 0) {
                    delete []chunk;
                    setErrorState("Box::pack Fatal error (around BIT groups)");
                    //emit packed();
                    return false;
                }

                QBitArray allbits = bytesToBits(QByteArray(chunk, rl));
#ifdef BOX_DEBUG
                QString text;
                for (int i = 0; i < allbits.size(); ++i)
                    text += allbits.testBit(i) ? "1": "0";

                qDebug()<<"allbits::"<<text;
#endif
                delete []chunk;

                bb = (Atom*)c;
                ab = (AtomBitmap*)c;

                int  from = 0;
                int  to = 0;
                bool done = false;
                do {
                    to = (from + bb->length());

                    if (!(bb->nextSibling() && bb->nextSibling()->isAtom() &&
                          ((Atom*)(bb->nextSibling()))->isBitmap())) {
                        done = true;
                    }

                    QBitArray tmp(to-from);
                    for(int i=0;i<to-from;i++) {
                        tmp.setBit(i, allbits.at(from+i));
                    }
                    ab->bpack(tmp);

                    from = to;

                    if (!done) {
                        bb = (Atom*)(bb->nextSibling());
                        ab = (AtomBitmap*)(bb);
                    }
                } while (!done);
                continue;
            }


#ifdef BOX_DEBUG
            qDebug()<<"pack: processing:"<<c->name();
#endif  
            if ( !count_de ) { //elements are not counted
                if (!c->isArray()) {
                    c->pack(ds);
                } else {
                    QList<BoxComponent *> occ = c->occurences();
                    int nocc = c->occurencesNumber();
#ifdef BOX_DEBUG
                    qDebug()<<"pack:nocc"<<nocc;
#endif
                    //occ.at(0)->setErrorState("No data");
                    for (int i=0;i<occ.length();i++) {
                        occ.at(i)->resetState();
                        if (i<nocc && !device->atEnd()) {
                            occ.at(i)->pack(ds);
                        } else {
                            occ.at(i)->clear();
                            //occ.at(i)->setErrorState("No more data. Packing suspended");
#ifdef BOX_DEBUG
                            qDebug()<<occ.at(i)->name()<<"no more data. Packing suspended";
#endif
                        }                        
                    }         
                }
            } else { //elements are counted
                if (!c->isArray()) {
                    if ( de_idx < bitmap.count() && bitmap.testBit(de_idx) ) {
#ifdef BOX_DEBUG
                        qDebug()<<"bit "<<de_idx<<"is on:"<<"packing"<<c->name();
#endif
                        c->pack(ds);
                    } else { //flag is not on
                        de_idx++;
                        c->setErrorState("Bitmap bit not set");
                        continue;
                    }
                    de_idx++;
                } else {
                    QList<BoxComponent *> occ = c->occurences();
                    int nocc = c->occurencesNumber();
                    //occ.at(0)->setErrorState("No data");
                    for (int i=0;i<occ.length();i++) {
                        occ.at(i)->resetState();
                        if ( i<nocc && !device->atEnd() &&
                             de_idx < bitmap.count() && bitmap.testBit(de_idx) ) {
#ifdef BOX_DEBUG
                            qDebug()<<"bit "<<de_idx<<"is on:"<<"packing"<<c->name()<<"["<<i<<"]";
#endif
                            occ.at(i)->pack(ds);
                        } else {
                            occ.at(i)->setErrorState("Bitmap bit not set");
                        }

                        de_idx++;
                    }
                }
            }

            //
            //  Choice processing
            //
            if (isChoice()) {
                if (c->state() == BoxComponent::OK_State) {
                    foreach (BoxComponent *cc, childs()) {
                        if (cc != c) {
                            cc->clear();
                            cc->setErrorState("not selected in CHOICE");
                        }
                    }
                    break;

                } else {
                    if (c == childs().last()) { //no more options
                        setErrorState("No more options. Packing suspended");
                        //emit packed();
                        return false;
                    }
                    
                    device->seek(choice_sav_pos);
                    //c->clear();
                    c->setErrorState("not selected in CHOICE");
                    continue;
                }
            }

            
            //
            //  Redefines processing
            //
            foreach (BoxComponent *r, c->redefines()) {
#ifdef BOX_DEBUG
                qDebug()<<"box.cpp > "+c->name()+" has redefines."+" data>"<<c->bytes();;
#endif
                QBuffer buf;
                buf.setData(c->bytes());
                buf.open(QBuffer::ReadWrite);
                
                Q_ASSERT(r);
                if (!r->isArray()) {
                    r->pack(&buf);
                } else {
                    int idx = 0;            
                    foreach (BoxComponent *oc, r->occurences() ) {
                        oc->pack(&buf);
                        idx++;                 
                        if (buf.atEnd()) {                 
                            break;
                        }
                    }
                }
            }

            if (c->state() == BoxComponent::Error_State) {
#ifdef BOX_DEBUG
                qDebug()<<c->name()<<" is in Error_State";
#endif
                //emit packed();
                return false;
            }

            AtomIsoBitmap *ci = (AtomIsoBitmap*)(c);

            //
            //  Bitmap processing
            //
            if (c->isAtom() && ((Atom*)c)->isIsoBitmap()) {
#ifdef BOX_DEBUG
                qDebug()<<"bitmap processing:"<<c->name();
#endif
                AtomIsoBitmap *abitmap = dynamic_cast<AtomIsoBitmap*>(c);
                Q_ASSERT(abitmap);

                appendBitArray(bitmap, abitmap->bitArray());

#ifdef BOX_DEBUG
                QString dis;
                for (int i=0; i<abitmap->bitArray().size();i++) {
                    if ((abitmap->bitArray()).testBit(i))
                        dis+="1";
                    else
                        dis+="0";
                }           
                qDebug()<<"bitmap:"<<dis;
#endif
                if (!count_de) 
                    count_de = true;
            }
        } //foreach (BoxComponent *bc...
    }

    emit packed(this);
    return true;
}

QByteArray
Box::unpack()
{
    QByteArray ba;

    QBitArray bitmap;
    int de_idx = 0;
    bool count_de = false;

    if (isArray()) {
        QList<BoxComponent *> occ = occurences();
        int nocc = occurencesNumber();
        for (int i=0;i<occ.length();i++) {
            if (i<nocc) {
                ba.append(occ.at(i)->unpack());
            }
        }
    } else {
        for(int idx=0; idx < childs().size(); idx++) {                 
            BoxComponent *c = childs().at(idx);

            if (isChoice()) {
                if ((c->state() != OK_State) && (c->state() != Default_State))
                    continue;
            }
            
            // BIT Groups
            if (c->isAtom() && ((Atom*)c)->isBitmap()) {
                Atom *bb = (Atom*)c;
                AtomBitmap *ab = (AtomBitmap*)c;
                size_t gsize = bb->length();
                QBitArray barray(ab->bits());

                while (bb->nextSibling() && bb->nextSibling()->isAtom() &&
                       ((Atom*)(bb->nextSibling()))->isBitmap()) {
                    bb = (Atom*)(bb->nextSibling());
                    gsize += bb->length();
                    appendBitArray(barray, ((AtomBitmap*)bb)->bits());
                    idx ++;
                }
#ifdef BOX_DEBUG
                qDebug()<<"Box::unpack:bit group size::"<<gsize;
                qDebug()<<"Box::unpack:bitmap size::"<<barray.size();
                qDebug()<<"Box::unpack:bitsToBytes"<<bitsToBytes(barray).toHex();
#endif
                if (gsize % 8 != 0)
                    qFatal("Box::unpack, a bit array group need to be \% 8"); 

                ba.append(bitsToBytes(barray));

                continue;
            }

            if ( !count_de ) { //elements are not counted
                if (!c->isArray()) {
                    ba.append(c->unpack());
                } else {
                    QList<BoxComponent *> occ = c->occurences();
                    int nocc = c->occurencesNumber();
                    for (int i=0;i<occ.length();i++) {
                        if (i<nocc) {
                            ba.append(occ.at(i)->unpack());
                        }
                    }
                }
            } else { //elements are counted
                if (!c->isArray()) {
                    if ( de_idx < bitmap.count() && bitmap.testBit(de_idx) ) {
                        ba.append(c->unpack());
                    } else { //flag is not on
                        de_idx++;
                        continue;
                    }
                    de_idx++;
                } else {
                    QList<BoxComponent *> occ = c->occurences();
                    int nocc = c->occurencesNumber();
                    for (int i=0;i<occ.length();i++) {
                        if ( i<nocc && de_idx < bitmap.count() && bitmap.testBit(de_idx) ) {
                            ba.append(occ.at(i)->unpack());
                        }
                        de_idx++;
                    }
                }
            }
            
            //
            //  Bitmap processing
            //
            if (c->isAtom() && ((Atom*)c)->isIsoBitmap()) {
                AtomIsoBitmap *abitmap = dynamic_cast<AtomIsoBitmap*>(c);
                Q_ASSERT(abitmap);
                appendBitArray(bitmap, abitmap->bitArray());
                if (!count_de) 
                    count_de = true;
            }

        } //foreach
    }
    return ba;
}

BoxComponent::State
Box::state() const
{
    State ret = BoxComponent::state();

    foreach(BoxComponent *c, childs()) {

        if (!isChoice()) {
            if (c->state() == Exception_State) {
                ret = Exception_State;
            }
            
            if (c->state() == Error_State)
                return Error_State;
        }
    }

    return ret;
}

ssize_t
Box::length() const
{
    ssize_t ret = 0;
    BoxComponent *c = NULL;

    if (isArray()) {
        ret += (occurences().size()) * occurences().at(0)->length();
    } else {
        for(int idx=0; idx < childs().size(); idx++) {      
            BoxComponent *c = childs().at(idx);
            ret += c->length();
        }
    }

    return ret;
}

void 
Box::setByteOrder( ByteOrder o ) {
    if (isArray()) {
        foreach (BoxComponent *occ, occurences())
            foreach (BoxComponent *c, occ->childs()) {
                c->setByteOrder(o);
                if (c->hasRedefines()) {
                    foreach (BoxComponent *r, c->redefines())
                        r->setByteOrder(o);
                }
        }
    } else {
        foreach (BoxComponent *c, childs()) {
            c->setByteOrder(o);
            if (c->hasRedefines()) {
                foreach (BoxComponent *r, c->redefines())
                    r->setByteOrder(o);
            }
        }
    }
}


QString 
Box::toJSON( bool use_redefines )
{
    QString txt;
    QStringList sl;

    if (!parent() == NULL)
        txt += "\""+name()+"\":";
    
    if (isArray()) {
        txt += "[";
        foreach (BoxComponent *occ, occurences()) {
            sl.clear();
            if ( occ!= this) {
                foreach (BoxComponent *c, occ->childs()) {
                    sl << c->toJSON(use_redefines);
                }
             }
            txt += ("{"+sl.join(",")+"},");
        }
        txt = txt.mid(0,txt.size()-1); //remove last ,
        txt += "]";
        return txt;
    } else {
        txt += "{";
    }

    sl.clear();
    foreach (BoxComponent *c, childs()) {
        sl << c->toJSON(use_redefines);
    }

    if (use_redefines) {
         foreach(BoxComponent *r, redefines()) {
             sl << r->toJSON(use_redefines);
         }
    }

    txt += sl.join(",");

    txt += "}";

    return txt;
}

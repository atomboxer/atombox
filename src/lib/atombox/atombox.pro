isEmpty(ATOMBOXER_PRI_INCLUDED) {
      include(../../../atomboxer.pri)
}

TEMPLATE = lib

QT	= core 

isEmpty(USE_SCRIPT_CLASSIC) {
    QT+=script
}

DESTDIR = $$ATOMBOXER_SOURCE_TREE/lib/


CONFIG += static

HEADERS += atom.h \
           atombinary.h \
           atombitmap.h \
           atomisobitmap.h \
           atomisobitmapbinary.h \
           atomisobitmaphex.h \
           atomhexpacked.h \
           atomnumericbcd.h \
           atomnumericpacked.h \
           atomstring.h \
           atomstringnumeric.h \
           atomstringalpha.h \
           box.h \
           boxcomponent.h \
           boxexception.h \
           constraint.h \
           constraintlist.h \
           constraintrange.h \
           constraintregex.h \
           constraintstring.h \
           constraintvalue.h \
           definition.h \
           utils.h

SOURCES += atom.cpp \
           atombinary.cpp \
           atombitmap.cpp \
           atomisobitmap.cpp \
           atomisobitmapbinary.cpp \
           atomisobitmaphex.cpp \
           atomhexpacked.cpp \
           atomnumericbcd.cpp \
           atomnumericpacked.cpp \
           atomstring.cpp \
           atomstringnumeric.cpp \
           atomstringalpha.cpp \
           box.cpp \
           boxcomponent.cpp \
           constraint.cpp \
           constraintlist.cpp \
           constraintrange.cpp \
           constraintregex.cpp \
           constraintstring.cpp \
           constraintvalue.cpp 

INCLUDEPATH += .

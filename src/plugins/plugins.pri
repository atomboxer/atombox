isEmpty(ATOMBOXER_PRI_INCLUDED) {
    include(../../atomboxer.pri)
}

TEMPLATE     = lib
QT           = core


LIBS += -L$$ATOMBOXER_SOURCE_TREE/plugins/script 

!isEmpty(ATOMBOXER_BUILD_DDL) {
    LIBS += -lddl -latombox
}

!isEmpty(USE_SCRIPT_CLASSIC) {
    !unix {
	CONFIG(debug) {
	    LIBS += -lQtScriptClassicd1
	} else {
	    LIBS += -lQtScriptClassic1
	}
    } else {
	LIBS += -L$$ATOMBOXER_SOURCE_TREE/lib/ -lQtScriptClassic
    }
}

tandem {
    CONFIG += plugin
} else {
    CONFIG      += plugin
#    unix {
#	DESTDIR = $$ATOMBOXER_SOURCE_TREE/lib/
#    } else { 
	DESTDIR = $$ATOMBOXER_SOURCE_TREE/plugins/script
#    }
}

!isEmpty(AB_STATIC_PLUGINS) {
    CONFIG += static
}


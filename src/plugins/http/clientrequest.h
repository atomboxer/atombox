#ifndef CLIENTREQUEST_H
#define CLIENTREQUEST_H

#include <QtCore/QObject>
#include <QtCore/QUrl>
#include <QtCore/QString>
#include <QtCore/QMultiMap>

#include <QtNetwork/QSslError>

#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>

class GlobalAccessManager
{
   public:
    static QNetworkAccessManager* getInstance( );
    ~GlobalAccessManager();

    static QNetworkAccessManager* instance;
    private:
    GlobalAccessManager();
      
};

class ClientResponse;
class ClientRequest : public QObject
{
    Q_OBJECT
    enum HttpMethod {
        GET_METHOD,
        POST_METHOD,
        PUT_METHOD,
        DELETE_METHOD,
        HEAD_METHOD
    };

public:
    ClientRequest( const QUrl &url, const QString &method, 
                   QMultiMap<QString, QString> &headers, 
                   const QString &proxy_user="", 
                   const QString &proxy_pass="",
                   QObject *parent = 0);

    virtual ~ClientRequest();

    virtual bool write(const QByteArray &chunk);
    virtual bool end(const QByteArray &chunk=QByteArray());

    virtual  QMultiMap<QString, QString> headers() const;

signals:
    void response( ClientResponse *r );

private slots:
    void proxyAuthenticationRequired ( const QNetworkProxy & proxy, 
                                       QAuthenticator * authenticator );
    void slot_finished(QNetworkReply *);
    void slot_sslErrors(QList<QSslError>);

private:
    QNetworkRequest       *rqst_;
    ClientResponse        *resp_;
    QNetworkAccessManager *manager_;

    QUrl                                   url_;
    HttpMethod                             method_;
    QMultiMap<QString, QString>            headers_;

    QString                                proxy_user_;
    QString                                proxy_pass_;

    QByteArray                             data_;
};

#endif

#include "server.h"

#include "serverrequest.h"
#include "serverresponse.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(ServerRequest*)
Q_DECLARE_METATYPE(ServerResponse*)

#define SERVER_DEBUG 1

Server::Server(QObject *parent): Tufao::HttpServer(parent)
{
    QObject::connect( this,SIGNAL(_requestReady(Tufao::HttpServerRequest*, Tufao::HttpServerResponse*)),
                        this, SLOT(slot_requestReady(Tufao::HttpServerRequest*, Tufao::HttpServerResponse*)),
                        Qt::DirectConnection);

}

Server::~Server()
{
    if (priv_)
        delete priv_;
}

void
Server::slot_requestReady(Tufao::HttpServerRequest *rqst, 
                          Tufao::HttpServerResponse *resp)
{
    Q_ASSERT(rqst);
    Q_ASSERT(resp);

    //these will be deleted by the script gc()
    
    ServerRequest  *rq =  new ServerRequest(rqst, resp);
    ServerResponse *rs =  new ServerResponse(resp, resp);
 
    emit requestReady(rq, rs);
}

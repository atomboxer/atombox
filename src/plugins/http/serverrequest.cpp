#include "serverrequest.h"

#include <tufao/src/ibytearray.h>
#include <tufao/src/headers.h>

#include <QtCore/QMultiMap>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define SERVERREQUEST_DEBUG

Q_DECLARE_METATYPE(Tufao::HttpServerRequest*)


ServerRequest::ServerRequest(Tufao::HttpServerRequest *rqst, QObject *parent) : QObject(parent)
{
    Q_ASSERT(rqst);
    priv_ = rqst;

#ifdef _GUARDIAN_TARGET
    QObject::connect( priv_,SIGNAL(data(QByteArray)), this, SIGNAL(data(QByteArray)),Qt::QueuedConnection);
    QObject::connect( priv_,SIGNAL(end()), this, SIGNAL(end()),Qt::QueuedConnection);
    QObject::connect( priv_,SIGNAL(close()), this, SIGNAL(close()),Qt::DirectConnection);
    QObject::connect( priv_,SIGNAL(upgrade(QByteArray)), this, SIGNAL(upgrade(QByteArray)),Qt::QueuedConnection);
#else
    QObject::connect( priv_,SIGNAL(data(QByteArray)), this, SIGNAL(data(QByteArray)),Qt::DirectConnection);
    QObject::connect( priv_,SIGNAL(end()), this, SIGNAL(end()),Qt::DirectConnection);
    QObject::connect( priv_,SIGNAL(close()), this, SIGNAL(close()),Qt::DirectConnection);
    QObject::connect( priv_,SIGNAL(upgrade(QByteArray)), this, SIGNAL(upgrade(QByteArray)),Qt::DirectConnection);
#endif
    
}

void
ServerRequest::slot_close()
{
#ifdef  SERVERREQUEST_DEBUG
    qDebug()<<"ServerRequest::slot_close";
#endif
    priv_->disconnect();
    emit close();
}

ServerRequest::~ServerRequest()
{
#ifdef  SERVERREQUEST_DEBUG
    qDebug()<<"ServerRequest::~ServerRequest";
#endif
    
}

QMultiMap<QString, QString>
ServerRequest::headers() const
{
    QMapIterator<Tufao::IByteArray, QByteArray> i(priv_->headers());
    QMultiMap<QString, QString> ret;

    while (i.hasNext()) {
        i.next();
        ret.insert(QString(i.key().data()).toLower(), QString(i.value().data()));
    }

    return ret;
}

QString
ServerRequest::httpVersion() const
{
    switch(priv_->httpVersion()) {
    case(Tufao::HttpServerRequest::HTTP_1_0): return "1.0";
    case(Tufao::HttpServerRequest::HTTP_1_1): return "1.1";
    default: break;
    }
}

QMultiMap<QString, QString>
ServerRequest::trailers() const
{
    QMapIterator<Tufao::IByteArray, QByteArray> i(priv_->trailers());
    QMultiMap<QString, QString> ret;

    while (i.hasNext()) {
        i.next();
        ret.insert(QString(i.key().data()).toLower(), QString(i.value().data()));
    }

    return ret;
}



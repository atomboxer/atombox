#include "clientresponseprototype.h"
#include "binary.h"

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptValueIterator>

#include <QtCore/QMultiMap>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(ClientResponse*)
Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)

ClientResponsePrototype::ClientResponsePrototype(QObject *parent) :
    QObject(parent)
{
}


ClientResponsePrototype::~ClientResponsePrototype()
{
}

// bool
// ClientResponsePrototype::write(QScriptValue chunk) const
// {
//     QByteArray *buf = NULL;
//     if (!chunk.isUndefined()) {
//         if (chunk.isString())
//             return thisClientResponse()->write(chunk.toString().toAscii());

//         buf = qscriptvalue_cast<ByteArray*>(chunk.data());
//         if ( !buf )
//             buf = qscriptvalue_cast<ByteString*>(chunk.data());

//         if (buf)
//             return thisClientResponse()->write(*buf);
//     }
    
//     context()->throwError("ClientResponse.prototype.write parameter should be String or instanceof Binary");
//     return false;
// }

// bool
// ClientResponsePrototype::end(QScriptValue chunk) const
// {
//     QByteArray *buf = NULL;

//     if (!chunk.isUndefined()) {
//         if (chunk.isString())
//             return thisClientResponse()->end(chunk.toString().toAscii());
        
//         buf = qscriptvalue_cast<ByteArray*>(chunk.data());
//         if ( !buf )
//             buf = qscriptvalue_cast<ByteString*>(chunk.data());
        
//         if (buf)
//             return thisClientResponse()->end(*buf);
//     }

//     return thisClientResponse()->end(QByteArray());
// }

QScriptValue
ClientResponsePrototype::headers() const
{
    QMapIterator<QString, QString> it(thisClientResponse()->headers());
    QScriptValue obj = engine()->newObject();

    while(it.hasNext()) {
        it.next();
        obj.setProperty(it.key().toLower(), it.value());
    }
    return obj;
}

qint32
ClientResponsePrototype::status() const
{
    return thisClientResponse()->status();
}

ClientResponse *
ClientResponsePrototype::thisClientResponse() const
{
    ClientResponse *r = qscriptvalue_cast<ClientResponse*>(thisObject());
    if (!r)
        qFatal("Programmatic error ClientResponsePrototype::thisClientResponse");

    return r;
}

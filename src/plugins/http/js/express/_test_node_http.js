//Lets require/import the HTTP module
var http = require('node_http');
var console = require('console');
var system  = require('system');

//Lets define a port we want to listen to
const PORT=13579; 

//We need a function which handles requests and send response
function handleRequest(request, response){
    console.log("handleRequest!");
    //console.dir(request);
    
    request.on('end', function() {
        console.log('[signal end] intercepted');    
    });
    
    request.on('data', function(ba) {
        console.log('[signal data] intercepted:'+ba);    
    });
    
    request.on('close', function() {
        console.log('[signal close] intercepted:');    
    });

//////////////////////////
    response.write("some chunk",'binary', function () {
        console.log("UUUUHUUU");
        response.getHeader();
    });
    
    response.on('finished', function() {
        console.log("[signal] finished intercepted");
    });
    
    response.end('It Works!! Path Hit: ' + request.url);
}

//Create a server
var server = http.createServer(handleRequest);

//Lets start our server
server.listen(PORT, function(){
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:%s", PORT);
});

server.on("request", function() {
    console.log('got a request event!');    
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// now do a client request
//Create an instance of HttpClientRequest


var rqst    = require("http").request({host:"127.0.0.1", port:PORT, method:'POST', path:"/",
                                headers:{
                                "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0",
                                "Accept-Language":"en-US,en;q=0.5",
                                "Accept-Encoding":"gzip, deflate",
                                "Connection":"keep-alive",
                                "Content-Type": "application/x-www-form-urlencoded",
                                "Content-Length": 30
                                }});
 
rqst.response.connect(function( resp ) {
     
    //resp is instance of HttpClientResponse
 
    resp.data.connect(function (data) {
        //write the data chunk to the console
        console.write("got a response" +data.decodeToString());
    });
     
    resp.finished.connect( function() {
        console.write("response has ended\n");
        //system.exit(0);
    });
 
    resp.error.connect(function(err) {
        console.writeln("Error:"+err);
    });
});
 
rqst.write("username=zurfyx&pass=password".toByteArray());
//END THE REQUEST (SENDS THE REQUEST OUT)
rqst.end();

//Lets require/import the HTTP module
var http = require('node_http');
var console = require('console');
var system  = require('system');

//Lets define a port we want to listen to
const PORT=13579; 
var connect = require('connect');

var app = connect();

// parse urlencoded request bodies into req.body
var bodyParser = require('body-parser').urlencoded;

app.use(bodyParser());


app.use('/foo/bar', function fooMiddleware(req, res, next) {
  console.dir(req.body);
  
  res.end('Hello from Connect!\n');
  next();
});

app.use(function onerror(err, req, res, next) {
   // an error occurred!
   console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
   res.end(err.toString().toByteArray());
   next();
});

//create node.js http server and listen on port
http.createServer(app).listen(PORT);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// now do a client request
//Create an instance of HttpClientRequest


var rqst    = require("http").request({host:"127.0.0.1", port:PORT, method:'POST', path:"/foo/bar",
                                headers:{
                                "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0",
                                "Accept-Language":"en-US,en;q=0.5",
                                "Accept-Encoding":"gzip, deflate",
                                "Connection":"keep-alive",
                                "Content-Type": "application/x-www-form-urlencoded",
                                "Content-Length": 30
                                }});

// var rqst    = require("http").request({host:"127.0.0.1", port:PORT, method:'POST', path:"/foo",
//                                 headers:{
//                                 "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0",
//                                 "Accept-Language":"en-US,en;q=0.5",
//                                 "Accept-Encoding":"gzip, deflate",
//                                 "Connection":"keep-alive",
//                                 "Content-Type": "application/json",
//                                 "Content-Length": 21
//                                 }});
 
rqst.response.connect(function( resp ) {
     
    //resp is instance of HttpClientResponse
 
    resp.data.connect(function (data) {
        //write the data chunk to the console
        //console.dir(resp.headers);
        //console.dir(resp.status);
        console.write("got a response!\n" +data.decodeToString());
    });

    resp.finished.connect( function() {
        console.write("response has ended\n");
        //system.exit(0);
    });

    resp.error.connect(function(err) {
        console.writeln("Error:"+err);
    });
});
 
//rqst.write("username=zurfyxa&pass=password".toByteArray());
rqst.write("username=zurfyxa&pass=password".toByteArray());
//rqst.write('   { "user": "tobi" }'.toByteArray());
//END THE REQUEST (SENDS THE REQUEST OUT)
rqst.end();

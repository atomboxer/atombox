
'use strict';

const util         = require('util');
const Stream       = require('io').Stream;
var debug          = require('debug')('_http_incoming');
const EventEmitter = require('events');
var console        = require('console');

function readStart(socket) {
    console.error('[not implemented] readStart');
    
}
exports.readStart = readStart;

function readStop(socket) {
    console.error('[not implemented] readStop');
    
}

exports.readStop = readStop;


/* Abstract base class for ServerRequest and ClientResponse. */
var IncomingMessage = function (ab_ServerRequest /*not used*/) {
    EventEmitter.call(this);
    
    if (!ab_ServerRequest) {
        console.error('IncomingMessage, ab_ServerRequest undefined');
        return;
    }
    
    this.ab_ServerRequest = ab_ServerRequest;
    
    //this.httpVersionMajor = null;
    //this.httpVersionMinor = null;
    this.httpVersion = ab_ServerRequest.httpVersion;

    this.complete = false;
    this.headers = ab_ServerRequest.headers;
    //this.rawHeaders = [];
    this.trailers = ab_ServerRequest.trailers;
    //this.rawTrailers = [];

    this.readable = true;

    this.upgrade = null;

    // request (server) only
    this.url = ab_ServerRequest.url;
    this.method = ab_ServerRequest.method;

    // response (client) only
    //this.statusCode = null;
    //this.statusMessage = null;

    // flag for when we decide that this message cannot possibly be
    // read by the user, so there's no point continuing to handle it.
    this._dumped = false;
    
    var self = this;
    this.onEnd = function() {
        self.emit('end');  
    }
    
    this.onData = function(ba) 
    {
        self.emit('data',ba);
    }

    this.onClose = function() 
    {
        self.complete = true;
        self.emit('close');

    }

    this.onUpgrade = function(ba) 
    {
        self.emit('upgrade', ba);
    }
    
    this.disconnectAll = function()
    {
        var self = this;
        try {
        self.ab_ServerRequest.end.disconnect(self.onEnd);
        self.ab_ServerRequest.data.disconnect(self.onData);
        self.ab_ServerRequest.close.disconnect(self.onClose);    
        self.ab_ServerRequest.upgrade.disconnect(self.onUpgrade);
        } catch(e) {
            console.error("erorr:"+e);
        }
    }
    
    this.connectAll = function()
    {
        try {
            //do AtomBox specific signal conversion
            ab_ServerRequest.end.connect(this, self.onEnd);
            ab_ServerRequest.data.connect(this, self.onData);
            ab_ServerRequest.close.connect(this, self.onClose);
            ab_ServerRequest.upgrade.connect(this, self.onUpgrade);
            
        } catch(e) { console.error(e); }
    }
    
    this.connectAll();
}

util.inherits(IncomingMessage, EventEmitter);


exports.IncomingMessage = IncomingMessage;

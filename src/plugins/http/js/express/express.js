/*!
 * express
 * Copyright(c) 2009-2013 TJ Holowaychuk
 * Copyright(c) 2013 Roman Shtylman
 * Copyright(c) 2014-2015 Douglas Christopher Wilson
 * MIT Licensed
 */

'use strict';

/**
 * Module dependencies.
 */

var console = require('console');
var EventEmitter = require('events');

var mixin = require('merge-descriptors');
var proto = require('express-application');
var Route = require('express-router-route');
var Router = require('express-router-router');
var req = require('express-request');
var res = require('express-response');

/**
* Expose `createApplication()`.
*/

exports = module.exports = createApplication;

/**
* Create an express application.
*
* @return {Function}
* @api public
*/

function createApplication() {
  var app = function(req, res, next) {
    app.handle(req, res, next);
  };

  mixin(app, EventEmitter.prototype, false);
  mixin(app, proto, false);

  app.request = { __proto__: req, app: app };
  app.response = { __proto__: res, app: app };
  app.init();
  return app;
}

/**
* Expose the prototypes.
*/

exports.application = proto;
exports.request = req;
exports.response = res;

/**
* Expose constructors.
*/

exports.Route = Route;
exports.Router = Router;

/**
* Expose middleware
*/

exports.query = require('express-middleware-query');
//exports.static = require('serve-static');

/**
* Replace removed middleware with an appropriate error message.
*/

[
  'json',
  'urlencoded',
  'bodyParser',
  'compress',
  'cookieSession',
  'session',
  'logger',
  'cookieParser',
  'favicon',
  'responseTime',
  'errorHandler',
  'timeout',
  'methodOverride',
  'vhost',
  'csrf',
  'directory',
  'limit',
  'multipart',
  'staticCache',
].forEach(function (name) {
  Object.defineProperty(exports, name, {
    get: function () {
      throw new Error('Most middleware (like ' + name + ') is no longer bundled with Express and must be installed separately. Please see https://github.com/senchalabs/connect#middleware.');
    },
    configurable: true
  });
});

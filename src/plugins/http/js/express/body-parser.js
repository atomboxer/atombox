/*!
 * body-parser
 * Copyright(c) 2014-2015 Douglas Christopher Wilson
 * MIT Licensed
 */

'use strict'

/**
 * Module dependencies.
 * @private
 */

//var deprecate = require('depd')('body-parser')

/**
 * Cache of loaded parsers.
 * @private
 */

var parsers = Object.create(null)

/**
 * @typedef Parsers
 * @type {function}
 * @property {function} json
 * @property {function} raw
 * @property {function} text
 * @property {function} urlencoded
 */

/**
 * Module exports.
 * @type {Parsers}
 */

//exports = module.exports;

//exports.urlencoded = function(){}; //{}createParserGetter('urlencoded')();

exports.json = createParserGetter('json');
exports.raw  = createParserGetter('raw');
exports.text  = createParserGetter('text');
exports.urlencoded  = createParserGetter('urlencoded');

/**
 * Create a middleware to parse json and urlencoded bodies.
 *
 * @param {object} [options]
 * @return {function}
 * @deprecated
 * @public
 */

function bodyParser(options){
  var opts = {}

  // exclude type option
  if (options) {
    for (var prop in options) {
      if ('type' !== prop) {
        opts[prop] = options[prop]
      }
    }
  }

  var _urlencoded = exports.urlencoded(opts)
  var _json = exports.json(opts)

  return function bodyParser(req, res, next) {
    _json(req, res, function(err){
      if (err) return next(err);
      _urlencoded(req, res, next);
    });
  }
}

/**
 * Create a getter for loading a parser.
 * @private
 */

function createParserGetter(name) {
    return loadParser(name)
}

/**
 * Load a parser module.
 * @private
 */

function loadParser(parserName) {
  
  var parser = parsers[parserName]
  
  if (parser !== undefined) {
    return parser
  }

  // this uses a switch for static require analysis
  switch (parserName) {
    case 'json':
      parser = require('_body-parser-json')
      break
    case 'raw':
      parser = require('_body-parser-raw')
      break
    case 'text':
      parser = require('_body-parser-text')
      break
    case 'urlencoded':
      parser = require('_body-parser-urlencoded')
      break
  }

  // store to prevent invoking require()
  return parsers[parserName] = parser
}

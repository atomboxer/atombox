//Lets require/import the HTTP module
var http = require('node_http');
var console = require('console');
var system  = require('system');

//Lets define a port we want to listen to
const PORT=8088; 

var express = require('express');

var app = express();

// parse urlencoded request bodies into req.body
var bodyParser = require('body-parser').json;
app.use(bodyParser());

app.set('etag', false);

// respond with "hello world" when a GET request is made to the homepage
app.all('/fo*/:param', function(req, res) {
  console.dir(req.body);
  
  //res.status(200).json({ error:                     'message' });
  
  res.set('Content-Type', 'text/html');
  res.send(new ByteArray('<p>some html</p>'));
});

app.get('/:param', function(req, res) {
   res.header('Date' , new Date().toUTCString() );
   console.dir(req.params);
   console.dir(req.body);
   res.status(200).json({ok:true, message:"Hello "+req.params.param+" from AtomBox", date:(new Date()).toUTCString()}); 
});

app.listen(PORT, function () {
  console.log('Example app listening on port +'+PORT);
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// now do a client request
//Create an instance of HttpClientRequest


// var rqst    = require("http").request({host:"127.0.0.1", port:PORT, method:'POST', path:"/der",
//                                 headers:{
//                                 "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0",
//                                 "Accept-Language":"en-US,en;q=0.5",
//                                 "Accept-Encoding":"gzip, deflate",
//                                 "Connection":"keep-alive",
//                                 "Content-Type": "application/x-www-form-urlencoded",
//                                 "Content-Length": 30
//                                 }});
                                
var rqst    = require("http").request({host:"127.0.0.1", port:PORT, method:'POST', path:"/foo",
                                 headers:{
                                 "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0",
                                 "Accept-Language":"en-US,en;q=0.5",
                                 "Accept-Encoding":"gzip, deflate",
                                 "Connection":"keep-alive",
                                 "Content-Type": "application/json",
                                 "Content-Length": 34
                                 }});
 
rqst.response.connect(function( resp ) {
     
    //resp is instance of HttpClientResponse
 
    resp.data.connect(function (data) {
        //write the data chunk to the console
        console.write("got a response>" +data.decodeToString());
        console.dir(resp.headers);
        console.dir(resp.status)
    });
     
    resp.finished.connect( function() {
        console.write("response has ended\n");
        //system.exit(0);
    });
 
    resp.error.connect(function(err) {
        console.writeln("Error:"+err);
    });
});
 
//rqst.write("username=zurfyx&pass=password".toByteArray());
rqst.write('   { "kaka" : { "user": "tobi" } }'.toByteArray());
//END THE REQUEST (SENDS THE REQUEST OUT)
rqst.end();


#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QMultiMap>
#include <QtCore/QMapIterator>
#include <QtCore/QCoreApplication>
#include <QtNetwork/QSslError>

#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QAuthenticator>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkProxyFactory>
#include <QtNetwork/QNetworkProxyQuery>

#include "clientrequest.h"
#include "clientresponse.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define CLIENTREQUEST_DEBUG

QNetworkAccessManager* GlobalAccessManager::instance = NULL;

QNetworkAccessManager* GlobalAccessManager::getInstance( ) {
    if (instance == NULL)
        instance = new QNetworkAccessManager();
    return instance;
}

ClientRequest::ClientRequest( const QUrl &url, const QString &method, 
                              QMultiMap<QString, QString> &headers, 
                              const QString &proxy_user, 
                              const QString &proxy_pass,
                              QObject *parent):
    QObject(parent),
    url_(url),
    headers_(headers),
    proxy_user_(proxy_user),
    proxy_pass_(proxy_pass)
{
    if (method.toUpper() == "GET")
        method_ = GET_METHOD;
    else if (method.toUpper() == "POST")
        method_ = POST_METHOD;
    else if (method.toUpper() == "PUT")
        method_ = PUT_METHOD;
    else if (method.toUpper() == "DELETE")
        method_ = DELETE_METHOD;
    else if (method.toUpper() == "HEAD")
        method_ = HEAD_METHOD;

    manager_ = GlobalAccessManager::getInstance();
    rqst_    = new QNetworkRequest(url);
    
    QMapIterator<QString, QString> it(headers);
    while (it.hasNext()) {
        it.next();
        rqst_->setRawHeader(it.key().toLatin1(), it.value().toLatin1());
    }

#ifndef _GUARDIAN_TARGET
    QNetworkProxyFactory::setUseSystemConfiguration(true);
    
    QList<QNetworkProxy> pl = 
        QNetworkProxyFactory::proxyForQuery(QNetworkProxyQuery(url));

    foreach (QNetworkProxy p, pl) {
        if (p.type() != QNetworkProxy::NoProxy)
            manager_->setProxy(p);
    }
#endif
    resp_ = NULL;
}

ClientRequest::~ClientRequest()
{
#if defined(CLIENTREQUEST_DEBUG)
    qDebug()<<"++++++++++++++++++++++++++++++++ClientRequest::~ClientRequest";
#endif

    //if (manager_)
    //    manager_->deleteLater();
    
   if (resp_)
       resp_->deleteLater();
   
   if (rqst_)
       delete rqst_;
}

bool
ClientRequest::write(const QByteArray &chunk)
{
    data_.append(chunk);
    return true;
}

bool
ClientRequest::end(const QByteArray &chunk)
{
#if defined(CLIENTREQUEST_DEBUG)
    qDebug()<<"ClientRequest::end";
#endif

    data_.append(chunk);

    //
    //  Ready to send the request
    //
    Q_ASSERT(manager_!=NULL);
    Q_ASSERT(rqst_!=NULL);

    switch(method_) {
    case(GET_METHOD): {
        resp_ = new ClientResponse(manager_->get(*rqst_), this);
    } break;
    case(POST_METHOD): {
        resp_ = new ClientResponse(manager_->post(*rqst_, data_), this);
    } break;
    case(PUT_METHOD): {
        resp_ = new ClientResponse(manager_->put(*rqst_, data_), this);
    } break;
    case(DELETE_METHOD): {
        resp_ = new ClientResponse(manager_->deleteResource(*rqst_), this);
    } break;
    case(HEAD_METHOD): {
        resp_ = new ClientResponse(manager_->head(*rqst_),this);
    } break;

    default: {
        qFatal("programmatic error in ClientRequest::end");
    }
    }

    QObject::connect(manager_, SIGNAL(finished(QNetworkReply *)), 
                     this, SLOT(slot_finished(QNetworkReply *)));

#ifndef _GUARDIAN_TARGET
    QObject::connect(manager_, SIGNAL(proxyAuthenticationRequired(const QNetworkProxy &, QAuthenticator *)), 
                     this, SLOT(proxyAuthenticationRequired (const QNetworkProxy &,QAuthenticator *)));
#endif

    emit response(resp_);

//    QObject::connect(manager_, SIGNAL(sslErrors(QList<QSslError>)),
//                     this, SLOT(slot_sslErrors(QList<QSslError>)));
    return true;
}

QMultiMap<QString, QString>
ClientRequest::headers() const
{
    QMultiMap<QString, QString> retVal;

    QList<QByteArray> hlist = rqst_->rawHeaderList(); 
    foreach(QByteArray v, hlist) {
        retVal.insert(v.toLower(), rqst_->rawHeader(v));
    }

    return retVal;
}

void
ClientRequest::slot_finished(QNetworkReply *r)
{
#if defined(CLIENTREQUEST_DEBUG)
    qDebug()<<"ClientRequest::slot_finished"<<r;
#endif
}

void 
ClientRequest::proxyAuthenticationRequired ( const QNetworkProxy & proxy, 
        QAuthenticator * authenticator )
{
#if defined(CLIENTREQUEST_DEBUG)
    qDebug()<<"proxyAuthenticationRequired";
#endif
    authenticator->setUser( proxy_user_);
    authenticator->setPassword( proxy_pass_ );
}

void ClientRequest::slot_sslErrors(QList<QSslError> l)
{
#if defined(CLIENTREQUEST_DEBUG)
    qDebug()<<"ClientRequest::slot_sslErrors";
#endif
}


#ifndef SERVER_H
#define SERVER_H

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>
#include <tufao/src/httpserver.h>

#include "serverrequest.h"
#include "serverresponse.h"

typedef QSharedPointer<ServerRequest>  WrapServerRequest;
typedef QSharedPointer<ServerResponse> WrapServerResponse;

class Server : public Tufao::HttpServer
{
    Q_OBJECT

public:
    Server(QObject *p=0);
    ~Server(); 

    inline void close() { Tufao::HttpServer::close(); }
    inline bool isListening() { return Tufao::HttpServer::isListening(); }
    inline bool listen(const QHostAddress &address, quint16 port) { return Tufao::HttpServer::listen(address, port); } 
    inline quint64 serverPort() const { return Tufao::HttpServer::serverPort(); }

signals:
    void requestReady( ServerRequest  *rqst,
                       ServerResponse *resp);

public slots:
    void slot_requestReady(Tufao::HttpServerRequest *, Tufao::HttpServerResponse *);

private:
    Tufao::HttpServer *priv_;
    ServerRequest*  rqst_;
    ServerResponse* resp_;
};

#endif // server.h 


#include <QtCore/QDebug>
#include <QtScript/QScriptContextInfo>
#include "console.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


QScriptValue 
__console_assertHelper(QScriptContext *ctx, QScriptEngine *eng)
{
    QScriptValue expression = ctx->argument(0);
    if (expression.toBool())
	return QScriptValue();

    QString msg = "<[ASSERTION]>";
    if (ctx->argumentCount()>0) {
	msg += ":"+ctx->argument(1).toString();
    }
    ctx->throwError(msg);
    return QScriptValue();
}

#include "timerprototype.h"

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(Timer*)

TimerPrototype::TimerPrototype(QObject *parent)
{
}

TimerPrototype::~TimerPrototype()
{

}

Timer*
TimerPrototype::thisTimer() const
{
    Timer *t = qscriptvalue_cast<Timer*>(thisObject());
    Q_ASSERT(t);
    return t;
}

/** 
 *  @module timers
 *  @desc  The Timers Module provides repetitive and single-shot timers. A single-shot timer fires only once, non-single-shot timers fire every interval seconds.The Timer class provides a high level programming interface for timers.

@example
var console = require("console");
var Timer = require("timers").Timer;
//...

var t = new Timer();

//Connect the timeout() signal
t.timeout.connect(function() { 
    console.write("Timeout! \n");
});

t.start(2); // Will display "Timeout!" every two seconds

 * @note The accuracy of the Timer class is +- 250 milliseconds 
 */

/**
 * @class
 * @name Timer
 * @memberof module:timers
 * @classdesc The Timers class provides repetitive and single-shot timers. A timer object can be repetitive or single-shot and emits timeout signal. Therefore, in order to use timers, timeout signal needs to be connected to a function.

@example
var console = require("console");
var Timer = require("timers").Timer;
//...

var t = new Timer();

//Connect the timeout() signal
t.timeout.connect(function() { 
    console.write("Timeout! \n");
});

t.start(2); // Will display "Timeout!" every two seconds
 * @note The accuracy of the Timer class is +- 250 milliseconds 
 */

/**
 *  This method returns the timeout interval in milliseconds.
 *  @method module:timers.Timer.prototype.interval
 *  @return {Number}
 */

/**
 *  Returns true if the timer is running; false otherwise.
 *  @method module:timers.Timer.prototype.isActive
 *  @return {Boolean}
 */

/**
 *  Returns true if this timer object is single-shot. 
 *  @method module:timers.Timer.prototype.isSingleShot
 *  @return {Boolean}
 */

/**
 *  Set the interval for this timer object.
 *  @method module:timers.Timer.prototype.setInterval
 *  @arg {String} sec - seconds
 */

/**
 *  Change the timer object behaviour.
 *  @method module:timers.Timer.prototype.setSingleShot
 *  @arg {Boolean} singleShot - true - single shot, false - repetitive
 */

/**
 *  Starts this timer.
 *  @method module:timers.Timer.prototype.start
 *  @arg {Number} [sec] - seconds
 */

/**
 *  Stops this timer.
 *  @method module:timers.Timer.prototype.stop
 */

/**
 *  Timeout event, fired by the timer objects.
 *  @event module:timers.Timer.timeout
 */
exports.Timer = __internal__.Timer;
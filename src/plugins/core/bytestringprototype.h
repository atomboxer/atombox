#ifndef BYTEARRAYPROTOTYPE_H
#define BYTEARRAYPROTOTYPE_H

#include <QtCore/QByteArray>
#include <QtCore/QObject>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

#include "binary.h"

class ByteStringPrototype : public QObject, public QScriptable
{
    Q_OBJECT

public:
    ByteStringPrototype(QObject *parent = 0);
    ~ByteStringPrototype();

public slots:
    ByteString byteAt(int offset) const;  
    ByteString charAt(int offset) const;    
    QScriptValue charCodeAt(int offset) const;
    ByteString compress() const;
    ByteString uncompress() const;
    QScriptValue codeAt(int offset) const;    
    ByteString concat( QScriptValue /*other*/) const;
    QString decodeToString(QScriptValue charset=QScriptValue()) const;
    QScriptValue get(int offset) const;
    ByteString hash32() const;
    ByteString hash64() const;
    ByteString hash64WithSeed(ByteString seed) const;
    ByteString hash128() const;
    ByteString hash128WithSeed(ByteString seed) const;
    int indexOf ( QScriptValue arg, int from = 0 ) const ;
    bool isPrint() const;
    int  lastIndexOf( QScriptValue arg, int from = -1 ) const;
    ByteString slice() const;
    ByteString slice(QScriptValue /*args*/) const;
    QList<int> toArray(QString charset="utf-8")  const;
    ByteArray  toByteArray() const;
    ByteArray  toByteArray(QString sourceCharset, 
	              	   QString targetCharset) const;
    ByteString toByteString() const;
    ByteString toByteString(QString sourceCharset, 
	        	    QString targetCharset) const;
    ByteString valueAt(int offset) const;
    QScriptValue valueOf() const;

    //
    // Non CommonJS
    //
    ByteString replace( int pos, 
    			int len,
    			QScriptValue arg) const;

    QScriptValue readInt8( qint32 offset = 0 ) const;
    QScriptValue readInt16( qint32 offset = 0 ) const;
    QScriptValue readInt32( qint32 offset = 0 ) const;
    QScriptValue readUInt8( qint32 offset = 0 ) const;
    QScriptValue readUInt16 (qint32 offset = 0 ) const;
    QScriptValue readUInt32( qint32 offset = 0 ) const;

    QScriptValue writeInt8 ( qint8 value, qint32 offset = 0 ) const;
    QScriptValue writeInt16 ( qint16 value, qint32 offset = 0 ) const;
    QScriptValue writeInt32 ( qint32 value, qint32 offset = 0 ) const;
    QScriptValue writeUInt8 ( quint8 value, qint32 offset = 0 ) const;
    QScriptValue writeUInt16( quint16 value, qint32 offset = 0 ) const;
    QScriptValue writeUInt32( quint32 value, qint32 offset = 0 ) const;

    QScriptValue toNumber() const;
 
 private:
    ByteString *thisByteString() const;
};

#endif //BYTEARRAYPROTOTYPE_H

#include <QtScript/QScriptClassPropertyIterator>
#include <QtScript/QScriptValueIterator>
#include <QtScript/QScriptEngine>

#include <QtCore/QTextCodec>
#include <QtCore/QDebug>

#include "bytearrayclass.h"
#include "bytestringclass.h"
#include "bytestringprototype.h"

#include "utils.h"

#include <stdlib.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(ByteString)
Q_DECLARE_METATYPE(ByteString*)
Q_DECLARE_METATYPE(ByteStringClass*)
Q_DECLARE_METATYPE(ByteArray*)

class ByteStringClassPropertyIterator : public QScriptClassPropertyIterator
{
public:
    ByteStringClassPropertyIterator(const QScriptValue &object);
    ~ByteStringClassPropertyIterator();

    bool hasNext() const;
    void next();

    bool hasPrevious() const;
    void previous();

    void toFront();
    void toBack();

    QScriptString name() const;
    uint id() const;

private:
    int m_index;
    int m_last;
};

ByteStringClass::ByteStringClass(QScriptEngine *engine)
    : QObject(engine), QScriptClass(engine)
{
    qScriptRegisterMetaType<ByteString>(engine, 
                                        toScriptValue, 
                                        fromScriptValue);

    length_ = engine->toStringHandle(QLatin1String("length"));

    proto_ = engine->newQObject(new ByteStringPrototype(this),
                                QScriptEngine::ScriptOwnership,
                                QScriptEngine::SkipMethodsInEnumeration
                                | QScriptEngine::ExcludeSuperClassMethods
                                | QScriptEngine::ExcludeSuperClassProperties);
    QScriptValue global = engine->globalObject();
    proto_.setPrototype(global.property("Object").property("prototype"));

    ctor_ = engine->newFunction(construct, proto_);
    ctor_.setData(engine->toScriptValue(this));
}

ByteStringClass::~ByteStringClass()
{
}

QScriptClass::QueryFlags
ByteStringClass::queryProperty(const QScriptValue &object,
                               const QScriptString &name,
                               QueryFlags flags, uint *id)
{
    ByteString *ba = qscriptvalue_cast<ByteString*>(object.data());
    if (!ba)
        return 0;
    if (name == length_) {
        return flags;
    }
    return 0;
}

QScriptValue
ByteStringClass::property(const QScriptValue &object,
                          const QScriptString &name, uint id)
{
    ByteString *ba = qscriptvalue_cast<ByteString*>(object.data());
    if (!ba)
        return QScriptValue();
    if (name == length_) {
        return ba->length();
    } else {
        qint32 pos = id;
        if ((pos < 0) || (pos >= ba->size()))
            return QScriptValue();
        return uint(ba->at(pos)) & 255;
    }
    return QScriptValue();
}

void
ByteStringClass::setProperty(QScriptValue &object,
                             const QScriptString &name,
                             uint id, const QScriptValue &value)
{
    ByteString *ba = qscriptvalue_cast<ByteString*>(object.data());
    if (!ba)
        return;
}

QScriptValue::PropertyFlags 
ByteStringClass::propertyFlags(const QScriptValue &, 
                               const QScriptString &name, uint )
{
    if (name == length_) {
        return QScriptValue::Undeletable
            | QScriptValue::SkipInEnumeration;
    }
    return QScriptValue::Undeletable;
}

QScriptClassPropertyIterator *
ByteStringClass::newIterator(const QScriptValue &object)
{
    return new ByteStringClassPropertyIterator(object);
}

QString
ByteStringClass::name() const
{
    return QLatin1String("ByteString");
}

QScriptValue
ByteStringClass::prototype() const
{
    return proto_;
}

QScriptValue
ByteStringClass::constructor()
{
    return ctor_;
}

QScriptValue
ByteStringClass::newInstance()
{
    //engine()->reportAdditionalMemoryCost(size);
    return newInstance(ByteString());
}

QScriptValue 
ByteStringClass::newInstance(const ByteString &ba)
{
    QScriptValue data = engine()->newVariant(QVariant::fromValue(ba));
    return engine()->newObject(this, data);
}


QScriptValue 
ByteStringClass::newInstance(const ByteArray &ba)
{
    ByteString alt(ba);
    QScriptValue data = engine()->newVariant(QVariant::fromValue(alt));
    return engine()->newObject(this, data);
}

QScriptValue 
ByteStringClass::newInstance(QScriptValue string, QScriptValue charset)
{
    if (charset.toString() == "hex") {
        return newInstance(ByteString(QByteArray::fromHex(string.toString().toLatin1().data())));
    }

    if (charset.toString() == "base64") {
        return newInstance(ByteString(QByteArray::fromBase64(string.toString().toLatin1().data())));
    }

    if (charset.toString().toLower() == "base94") {
        char out[1024];
        size_t out_len = 0;
        if (base94_decode(string.toString().toLatin1().data(), 
                          string.toString().size(), 1024, out, out_len) == true) {
            QByteArray copy(out, out_len);
            return newInstance(ByteString(copy));
        } else {
            engine()->currentContext()->throwError("base94_decode failed");
            return QScriptValue();
        }
    }

    if (charset.toString().toLower() == "ascii")
   	    charset = "utf-8";

    if (charset.toString().toLower() == "ebcdic") {
        QByteArray ba = string.toString().toAscii();
        ASCII2EBCDIC(ba.data(), ba.length());
        return newInstance(ByteString(ba));
    }

    QTextCodec *codec = QTextCodec::codecForName(
        charset.toString().toLatin1().data());

    if (!codec) {
        engine()->currentContext()->throwError("Invalid codec");
        return QScriptValue();
    }
    
    return newInstance(ByteString(codec->fromUnicode(string.toString())));
}

QScriptValue
ByteStringClass::construct(QScriptContext *ctx,
                           QScriptEngine  *eng)
{
    ByteStringClass *cls = NULL;
    cls = qscriptvalue_cast<ByteStringClass*>(ctx->callee().data());

    QScriptValue arg = ctx->argument(0);
    if (arg.instanceOf(ctx->callee()))
        return cls->newInstance(qscriptvalue_cast<ByteString>(arg));

    //
    //  String and charset
    //
    if (ctx->argumentCount() == 2) {
        return cls->newInstance(ctx->argument(0), ctx->argument(1));
    }
    if (ctx->argumentCount() == 1 && arg.isString()) {
        return cls->newInstance(ctx->argument(0), "utf-8");
    }

    //
    //  Maybe the argument is a ByteArray
    //
    ByteArray *ba = NULL;
    ba = qscriptvalue_cast<ByteArray*>(arg.data());
    if ( ba ) {
        return cls->newInstance(*ba);
    }

    if (arg.isArray()) {
        ByteString alt;

        QScriptValueIterator it(arg);

        while (it.hasNext()) {
            it.next();
            if (it.flags() & QScriptValue::SkipInEnumeration)
                continue;
            if (it.value().isNumber()) {
                int value = it.value().toInt32();
                if ( value < 0 || value > 255) {
                    ctx->throwError("ByteArray accepts only 0-255 values"); 	     
                    return QScriptValue();
                }
                alt.push_back((unsigned char)value);
            }
        }
        return cls->newInstance(alt);
    }

    return cls->newInstance();
}

QScriptValue
ByteStringClass::toScriptValue( QScriptEngine *eng,
                                const ByteString &ba)
{
    QScriptValue ctor_ = eng->globalObject().property("__internal__").property("ByteString");
    ByteStringClass *cls = qscriptvalue_cast<ByteStringClass*>(ctor_.data());
    if (!cls) {
        return eng->newVariant(QVariant::fromValue(ba));
    }
    return cls->newInstance(ba);
}

void
ByteStringClass::fromScriptValue(const QScriptValue &obj,
                                 ByteString &ba)
{
    ba = qvariant_cast<ByteString>(obj.data().toVariant());
}

void
ByteStringClass::resize(ByteString &ba, int newSize)
{
    int oldSize = ba.size();

    if (oldSize < newSize) {
        ByteString newba(newSize,0);
        newba.replace(0,ba.size(),ba);
        ba = newba;
    } else {
        ba.resize(newSize);
    }

    // if (newSize > oldSize)
    // 	engine()->reportAdditionalMemoryCost(newSize - oldSize);
}

ByteStringClassPropertyIterator::ByteStringClassPropertyIterator(
    const QScriptValue &object)
    : QScriptClassPropertyIterator(object)
{
    toFront();
}

ByteStringClassPropertyIterator::~ByteStringClassPropertyIterator()
{
}

bool
ByteStringClassPropertyIterator::hasNext() const
{
    ByteString *ba = qscriptvalue_cast<ByteString*>(object().data());
    return m_index < ba->size();
}

void
ByteStringClassPropertyIterator::next()
{
    m_last = m_index;
    ++m_index;
}

bool
ByteStringClassPropertyIterator::hasPrevious() const
{
    return (m_index > 0);
}

void
ByteStringClassPropertyIterator::previous()
{
    --m_index;
    m_last = m_index;
}

void
ByteStringClassPropertyIterator::toFront()
{
    m_index = 0;
    m_last = -1;
}

void
ByteStringClassPropertyIterator::toBack()
{
    ByteString *ba = qscriptvalue_cast<ByteString*>(object().data());
    m_index = ba->size();
    m_last = -1;
}

QScriptString
ByteStringClassPropertyIterator::name() const
{
    return object().engine()->toStringHandle(QString::number(m_last));
}

uint
ByteStringClassPropertyIterator::id() const
{
    return m_last;
}

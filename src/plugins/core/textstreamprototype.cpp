#include "stream.h"
#include "textstreamprototype.h"

#include <QtCore/QByteArray>
#include <QtCore/QFile>
#include <QtCore/QDebug>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(Stream*)
Q_DECLARE_METATYPE(TextStream*)
Q_DECLARE_METATYPE(ByteString*)
Q_DECLARE_METATYPE(ByteArray*)


TextStreamPrototype::TextStreamPrototype(QObject *parent)
    : QObject(parent)
{
}

TextStreamPrototype::~TextStreamPrototype()
{
}

bool 
TextStreamPrototype::hasNext() const
{
    return !thisIODevice()->atEnd();
}

void
TextStreamPrototype::copy(Stream *io) const
{
    QByteArray ba = thisIODevice()->readAll();
    if (io->priv_->write(ba) == -1) {
        context()->throwError("TextStream.prototype.copy: error:"+
                              io->priv_->errorString());

    }
}

QString
TextStreamPrototype::readLine() const
{
    return thisStream()->readLine();
}

QStringList
TextStreamPrototype::readLines() const
{
    QStringList sl;
    QString line;

    while( !(line = thisStream()->readLine()).isNull() ) {
        sl.push_back(line);
    };

    return sl;
}

QString
TextStreamPrototype::next() const
{
    return thisStream()->readLine();
}

void
TextStreamPrototype::print(QScriptValue args) const
{
    QScriptValueIterator it(args);
    while (it.hasNext()) {
        it.next();
        writeLine(it.value().toString());
    }
    thisStream()->flush();
}

void
TextStreamPrototype::write(QString txt) const
{
    *thisStream() << txt;
    thisStream()->flush();
}

void
TextStreamPrototype::writeLine(QString line) const
{
    *thisStream() << line << endl;
}

void
TextStreamPrototype::writeLines(QStringList lines) const
{
    QStringListIterator it(lines);
    while (it.hasNext()) 
        writeLine(it.next());
}

QIODevice*
TextStreamPrototype::thisIODevice() const
{
    TextStream *stream_ptr = qscriptvalue_cast<TextStream*>(thisObject());
    if (!stream_ptr) {
        qFatal("programatic error TextStreamPrototype::thisIODevice");
        return 0;
    }
   
    return stream_ptr->raw_->priv_;
}

QTextStream*
TextStreamPrototype::thisStream() const
{   
    TextStream *s_ptr = qscriptvalue_cast<TextStream*>(thisObject());

    if (!s_ptr) {
        qFatal("programatic error TextStreamPrototype::thisStream");
        return 0;
    }

    return s_ptr->ts_;
}

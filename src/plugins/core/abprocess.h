#ifndef ABPROCESS_H
#define ABPROCESS_H

#include <QtCore/QString>
#include <QtCore/QObject>
#include <QtCore/QProcess>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptContext>
#include <QtScript/QScriptValue>


class Process : public QObject
{
    friend class ProcessPrototype;
    
    Q_OBJECT

public:
    Process(QProcess *priv, QObject *parent = 0);
    ~Process();

signals:
    void error(QString e);
    void finished(int status);

#ifndef _GUARDIAN_TARGET
    void readyReadStandardError();
    void readyReadStandardOutput();
#endif
    void started();

private slots:
    void slotError(QProcess::ProcessError);
    void slotFinished(int, QProcess::ExitStatus);    
    void slotStarted();

private:
    QProcess *priv_;
};

#endif //PROCESS_H

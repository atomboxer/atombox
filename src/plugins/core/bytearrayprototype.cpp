#include "bytearrayclass.h"
#include "bytearrayprototype.h"

#include <QtCore/QBuffer>
#include <QtCore/QDebug>
#include <QtCore/QTextCodec>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#include "utils.h"

#include "cityhash/city.h"

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)

static ByteArray arrayToByteArray(QScriptValue arg)
{
    ByteArray alt;

    QScriptValueIterator it(arg);

    while (it.hasNext()) {
        it.next();
        if (it.flags() & QScriptValue::SkipInEnumeration)
            continue;
        if (it.value().isNumber()) {
            int value = it.value().toInt32();
            if ( value < 0 || value > 255) {
                Q_ASSERT(0);
                return ByteArray();
            }
            alt.push_back((unsigned char)value);
        }
    }
    return alt;
}


ByteArrayPrototype::ByteArrayPrototype(QObject *parent)
    : QObject(parent)
{
}

ByteArrayPrototype::~ByteArrayPrototype()
{
}

ByteArray *
ByteArrayPrototype::thisByteArray() const
{
    return qscriptvalue_cast<ByteArray*>(thisObject().data());
}

QScriptValue
ByteArrayPrototype::codeAt(int offset) const
{
    ByteArray *ba = thisByteArray();
    if (offset < 0 || offset >= ba->size()) {
        return QScriptValue();
    }

    return ((unsigned char)ba->at(offset));
}

ByteArray
ByteArrayPrototype::concat( QScriptValue /*other*/) const
{
    ByteArray ba((char*)thisByteArray()->data(), thisByteArray()->length());
    for (int i = 0; i < context()->argumentCount(); ++i) {
        QScriptValue arg = context()->argument(i);
        ByteArray  *ba_arg = 0;
        ba_arg = qscriptvalue_cast<ByteArray*>(arg.data());
        ByteString *bs_arg = 0;
        if (!ba_arg) {
            bs_arg = qscriptvalue_cast<ByteString*>(arg.data());
        }

        if ( ba_arg ) {
            ba.append(*ba_arg);
        } else if (bs_arg) {
            ba.append(*bs_arg);
        } else if(arg.isNumber()) {
            ba.push_back((unsigned char)arg.toInt32());
        } else if (arg.isArray()) {
            ba.append(arrayToByteArray(arg));
        } else if (arg.isString()) {
            ba.append(arg.toString().toLatin1().data());
        } else {
            Q_ASSERT_X(0,"error","It should never get here.");
        }
    }
    return ba;
}

QScriptValue
ByteArrayPrototype::copy(QScriptValue target,
                         int startIndex,
                         int endIndex,
                         int targetIndex ) const
{
    context()->throwError("copy:Not implemented");
    return QScriptValue();
}

ByteArray
ByteArrayPrototype::compress() const
{
    ByteArray *ba = thisByteArray();
    return qCompress(*ba);
}

ByteArray
ByteArrayPrototype::uncompress() const
{
    ByteArray *ba = thisByteArray();
    return qUncompress(*ba);
}

ByteArray
ByteArrayPrototype::hash32() const
{
    ByteArray *ba = thisByteArray();

    ByteArray ret;
    quint32 hash = CityHash32(ba->data(), ba->length());
    QDataStream(&ret,QIODevice::WriteOnly)<<hash;
    return ret;
}

ByteArray
ByteArrayPrototype::hash64() const
{
    ByteArray *ba = thisByteArray();

    ByteArray ret;
    quint64 hash = CityHash64(ba->data(), ba->length());
    QDataStream(&ret,QIODevice::WriteOnly)<<hash;
    return ret;
}

ByteArray
ByteArrayPrototype::hash64WithSeed(ByteArray seed) const
{
    quint64 num_seed=0;
    QDataStream(seed.mid(0))>>num_seed;

    ByteArray *ba = thisByteArray();
    ByteArray ret;
    quint64 hash = CityHash64WithSeed(ba->data(), ba->length(),num_seed);
    QDataStream(&ret,QIODevice::WriteOnly)<<hash;
    return ret;
}


ByteArray
ByteArrayPrototype::hash128() const
{
    ByteArray *ba = thisByteArray();

    ByteArray ret;
    std::pair<quint64,quint64> hash = CityHash128(ba->data(), ba->length());
    QDataStream(&ret,QIODevice::WriteOnly)<<hash.first<<hash.second;
    return ret;
}

ByteArray
ByteArrayPrototype::hash128WithSeed(ByteArray seed) const
{
    quint64 num_seed1=0;
    quint64 num_seed2=0;
    QDataStream(seed.mid(0))>>num_seed1>>num_seed2;

    ByteArray *ba = thisByteArray();
    ByteArray ret;
    std::pair<quint64,quint64> hash = CityHash128WithSeed(ba->data(), ba->length(),
                                                          std::pair<quint64, quint64>(num_seed1, num_seed2));
    QDataStream(&ret,QIODevice::WriteOnly)<<hash.first<<hash.second;
    return ret;
}


QString
ByteArrayPrototype::decodeToString(QScriptValue charset) const
{
    ByteArray *ba = thisByteArray();

    QString encoding = charset.toString();
    if (encoding.isEmpty() || encoding.toLower() == "ascii")
        encoding = "utf-8";

    if (encoding.toLower() == "hex") {
        return QString(ba->toHex());
    }

    if (encoding.toLower() == "base64") {
        return QString(ba->toBase64());
    }

    if (encoding.toLower() == "base94") {
        char out[1024];
        size_t out_len = 0;
        if (base94_encode(ba->constData(), ba->size(), 1024, out, out_len) == true) {
            QByteArray copy(out, out_len);
            return QString(copy);
        } else {
            context()->throwError("base94 encoding failed!");
            return QString();
        }
    }

    if (encoding.toLower() == "ebcdic") {
        QByteArray copy(ba->data(), ba->length());
        EBCDIC2ASCII((char*)copy.data(), ba->length());
        return QString(copy);
    }

    QTextCodec *codec = QTextCodec::codecForName(encoding.toLatin1().data());
    if (!codec) {
        context()->throwError(QString("codec:")+encoding+" not found!");
        return QString();
    }
    return codec->toUnicode(*ba).remove(QChar(0x00));
}

QScriptValue
ByteArrayPrototype::get(int offset) const
{
    return codeAt(offset);
}

int
ByteArrayPrototype::indexOf( QScriptValue arg,
                             int from) const
{
    if (arg.isNumber()) {
        return thisByteArray()->indexOf(arg.toInt32(),from);
    }

    ByteArray *ba_arg = qscriptvalue_cast<ByteArray*>(arg.data());
    if (ba_arg) {
        return thisByteArray()->indexOf(*ba_arg, from);
    }

    if (arg.isArray()) {
        return thisByteArray()->indexOf(arrayToByteArray(arg), from);
    } else {
        context()->throwError("Unsupported argument in ByteArray.indexOf");
        return -1;
    }

    return -1;
}

bool
ByteArrayPrototype::isPrint( ) const
{
    ByteArray *ba = thisByteArray();

    for (int i=0; i<ba->length(); i++) {
        const unsigned char c = ba->at(i);
        if (!isprint(c))
            return false;
    }

    return true;
}

int
ByteArrayPrototype::lastIndexOf( QScriptValue arg,
                                 int from) const
{
    if (arg.isNumber()) {
        return thisByteArray()->lastIndexOf(arg.toInt32(), from);
    }

    ByteArray *ba_arg = qscriptvalue_cast<ByteArray*>(arg.data());
    if (!ba_arg) {
        context()->throwError("Unsupported argument in ByteArray.lastIndexOf");
        return -1;
    }

    return thisByteArray()->lastIndexOf(*ba_arg, from);
}

int
ByteArrayPrototype::pop() const
{
    ByteArray *ba = thisByteArray();
    int ret = ba->at(ba->length()-1);
    ba->remove(ba->length()-1,1);
    return ret;
}

int
ByteArrayPrototype::push(QScriptValue /*values*/) const
{
    ByteArray *ba = thisByteArray();
    for (int i = 0; i < context()->argumentCount(); ++i) {
        QScriptValue num = context()->argument(i);
        if (!num.isNumber()) {
            context()->throwError("ByteArray.push(n1,n2...)");
            return ba->length();
        }
        ba->push_back(num.toNumber());
    }

    return ba->length();
}

ByteArray
ByteArrayPrototype::reverse() const
{
    ByteArray *ba = thisByteArray();
    ByteArray alt;

    for(int i=ba->size()-1;i>=0; i--) {
        alt.push_back((*ba)[i]);
    }
    ba->replace(0,ba->size(), alt);
    return *ba;
}

int
ByteArrayPrototype::shift() const
{
    ByteArray *ba = thisByteArray();
    if ( ba->length()>0 ) {
        context()->throwError("shift on an empty array");
        return 0;
    }
    int ret = ba->at(0);
    ba->remove(0,1);
    return ret;
}


ByteArray
ByteArrayPrototype::slice() const
{
    return slice(0);
}

ByteArray
ByteArrayPrototype::slice(QScriptValue /*args*/) const
{
    QScriptValue begin = context()->argument(0);
    QScriptValue end = context()->argument(1);

    ByteArray *ba = thisByteArray();

    int from = 0;
    //if (begin.isNumber()) {
    from = begin.toInt32();
    //} else {
    //return ByteArray(*thisByteArray());
    //}

    if (from < 0) {
        from += ba->length();
    }
    from = qMin(ba->length(), qMax(0, from));
    int to = ba->length();

    //if (end.isNumber()) {
    to = end.toInt32();
    //} else {
    //to = ba->length();
    //}

    if ( to < 0 ) {
        to += ba->length();
    }
    int len = qMax(0,qMin(ba->length()-from, to - from));

    return ByteArray(*thisByteArray()).mid(from,len);
}

QList<int>
ByteArrayPrototype::toArray(QString charset) const
{
    QList<int> arr;
    QTextCodec *codec = QTextCodec::codecForName(charset.toLatin1().data());

    if (!codec) {
        context()->throwError(QString("codec not found!"));
        return arr;
    }

    QString enc = codec->toUnicode(*thisByteArray());
    for (int i=0;i<enc.length();i++) {
        arr.push_back((unsigned char)enc.at(i).unicode());
    }

    return arr;
}

ByteArray
ByteArrayPrototype::toByteArray() const
{
    ByteArray *ba = thisByteArray();
    return ByteArray(*ba);
}

ByteArray
ByteArrayPrototype::toByteArray(QString src_charset, QString dst_charset) const
{
    QTextCodec *sc = QTextCodec::codecForName(src_charset.toLatin1().data());
    QTextCodec *dc = QTextCodec::codecForName(dst_charset.toLatin1().data());
    if (!sc || !dc) {
        context()->throwError(QString("codec not found!"));
        return QByteArray();
    }

    return dc->fromUnicode(sc->toUnicode(*thisByteArray()));
}

ByteString
ByteArrayPrototype::toByteString() const
{
    ByteArray *ba = thisByteArray();
    return ByteString(*ba);
}

ByteString
ByteArrayPrototype::toByteString(QString src_charset, QString dst_charset) const
{
    QTextCodec *sc = QTextCodec::codecForName(src_charset.toLatin1().data());
    QTextCodec *dc = QTextCodec::codecForName(dst_charset.toLatin1().data());
    if (!sc || !dc) {
        context()->throwError(QString("codec not found!"));
        return QByteArray();
    }

    return dc->fromUnicode(sc->toUnicode(*thisByteArray()));
}

int
ByteArrayPrototype::unshift(QScriptValue /*values*/) const
{
    ByteArray *ba = thisByteArray();
    for (int i = context()->argumentCount() - 1; i >=0  ; --i) {
        QScriptValue num = context()->argument(i);
        if (!num.isNumber()) {
            context()->throwError("values should be [0-255]");
            return ba->length();
        }
        ba->push_front((unsigned char)num.toInt32());
    }

    return ba->length();
}

QScriptValue
ByteArrayPrototype::valueOf() const
{
    return thisObject().data();
}

ByteArray
ByteArrayPrototype::replace( int pos,
                             int len,
                             QScriptValue arg ) const
{
    ByteArray *ba = qscriptvalue_cast<ByteArray*>(arg.data());
    if (!ba) {
        context()->throwError("Unsupported argument in ByteArray.set");
        return ByteArray();
    }

    return thisByteArray()->replace(pos,len,*ba);
}

QScriptValue
ByteArrayPrototype::readInt8( qint32 offset ) const
{
    if (offset>thisByteArray()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }
    qint8 ret;
    QDataStream(thisByteArray()->mid(offset))>>ret;
    return ret;
}

QScriptValue
ByteArrayPrototype::readInt16( qint32 offset ) const
{
    if (offset>thisByteArray()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }
    qint16 ret;
    QDataStream(thisByteArray()->mid(offset))>>ret;
    return ret;
}

QScriptValue
ByteArrayPrototype::readInt32( qint32 offset ) const
{
    if (offset>thisByteArray()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }
    qint32 ret;
    QDataStream(thisByteArray()->mid(offset))>>ret;
    return ret;
}

QScriptValue
ByteArrayPrototype::readUInt8( qint32 offset ) const
{
    if (offset>thisByteArray()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }
    quint8 ret;
    QDataStream(thisByteArray()->mid(offset))>>ret;
    return ret;
}

QScriptValue
ByteArrayPrototype::readUInt16( qint32 offset ) const
{
    if (offset>thisByteArray()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }
    quint16 ret;
    QDataStream(thisByteArray()->mid(offset))>>ret;
    return ret;

}

QScriptValue
ByteArrayPrototype::readUInt32( qint32 offset ) const
{
    if (offset>thisByteArray()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }
    quint16 ret;
    QDataStream(thisByteArray()->mid(offset))>>ret;
    return ret;

}

QScriptValue
ByteArrayPrototype::writeInt8 ( qint8 value, qint32 offset ) const
{
    QByteArray *ba = thisByteArray();

    if (offset>thisByteArray()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }

    if (offset == 0)
        QDataStream(ba,QIODevice::WriteOnly)<<value;
    else {
        QByteArray aux;
        QDataStream(&aux,QIODevice::WriteOnly)<<value;
        ba->replace(offset,sizeof(value), aux);
    }
    return value;
}

QScriptValue
ByteArrayPrototype::writeInt16 ( qint16 value, qint32 offset ) const
{
    QByteArray *ba = thisByteArray();

    if (offset>thisByteArray()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }

    if (offset == 0)
        QDataStream(ba,QIODevice::WriteOnly)<<value;
    else {
        QByteArray aux;
        QDataStream(&aux,QIODevice::WriteOnly)<<value;
        ba->replace(offset,sizeof(value), aux);
    }
    return value;
}

QScriptValue
ByteArrayPrototype::writeInt32 ( qint32 value, qint32 offset ) const
{
    QByteArray *ba = thisByteArray();

    if (offset>thisByteArray()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }

    if (offset == 0)
        QDataStream(ba,QIODevice::WriteOnly)<<value;
    else {
        QByteArray aux;
        QDataStream(&aux,QIODevice::WriteOnly)<<value;
        ba->replace(offset,sizeof(value), aux);
    }
    return value;
}

QScriptValue
ByteArrayPrototype::writeUInt8 ( quint8 value, qint32 offset ) const
{
    QByteArray *ba = thisByteArray();

    if (offset>thisByteArray()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }

    if (offset == 0)
        QDataStream(ba,QIODevice::WriteOnly)<<value;
    else {
        QByteArray aux;
        QDataStream(&aux,QIODevice::WriteOnly)<<value;
        ba->replace(offset,sizeof(value), aux);
    }
    return value;
}

QScriptValue
ByteArrayPrototype::writeUInt16( quint16 value, qint32 offset ) const
{
    QByteArray *ba = thisByteArray();

    if (offset>thisByteArray()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }

    if (offset == 0)
        QDataStream(ba,QIODevice::WriteOnly)<<value;
    else {
        QByteArray aux;
        QDataStream(&aux,QIODevice::WriteOnly)<<value;
        ba->replace(offset,sizeof(value), aux);
    }
    return value;
}

QScriptValue
ByteArrayPrototype::writeUInt32( quint32 value, qint32 offset ) const
{
    QByteArray *ba = thisByteArray();

    if (offset>thisByteArray()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }

    if (offset == 0)
        QDataStream(ba,QIODevice::WriteOnly)<<value;
    else {
        QByteArray aux;
        QDataStream(&aux,QIODevice::WriteOnly)<<value;
        ba->replace(offset,sizeof(value), aux);
    }
    return value;
}

QScriptValue
ByteArrayPrototype::toNumber() const
{
    QByteArray *ba = thisByteArray();
    int length = ba->length();

    if (length == 1) {
        QDataStream str(*ba);
        qint8 val;
        str >> val;
        return QScriptValue(val);
    }

    if (length == 2) {
        QDataStream str(*ba);
        qint16 val;
        str >> val;
        return QScriptValue(val);
    }

    if (length <= 4) {
        if (length == 3)
            ba->push_front((char)0x00);
        QDataStream str(*ba);
        qint32 val;
        str >> val;
        return QScriptValue(val);
    }

    if (length <= 8) {
        for(int i=0;i<8-length;i++)
            ba->push_front((char)0x00);
        QDataStream str(*ba);
        qlonglong val;
        str >> val;
        return QScriptValue((qsreal)val);
    }

    return QScriptValue(QScriptValue::UndefinedValue);
}

#include "streamprototype.h"
#include "bytearrayclass.h"
#include "bytearrayprototype.h"

#include <QtCore/QByteArray>
#include <QtCore/QDebug>
#include <QtCore/QFile>

#include <QtNetwork/QAbstractSocket>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>


#ifdef _GUARDIAN_TARGET
#include "guardian/gfsfileengine.h"
#endif
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(Stream*)
Q_DECLARE_METATYPE(ByteString*)
Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteArray)

StreamPrototype::StreamPrototype(QObject *parent)
    : QObject(parent)
{
}

StreamPrototype::~StreamPrototype()
{
}

void
StreamPrototype::close()
{
#ifdef _GUARDIAN_TARGET //if we want to delete records
    QFile *file = dynamic_cast<QFile*>(thisIODevice());
    if (file) {
        GFSFileEngine *fs_eng = dynamic_cast<GFSFileEngine*>(file->fileEngine());
        if (fs_eng && fs_eng->isEnscribe()) {
            fs_eng->close();
            return;
        }
    }
#endif

    thisIODevice()->close();
}

void
StreamPrototype::copy(QIODevice *io)
{
    QByteArray ba = thisIODevice()->readAll();
    if (io->write(ba) == -1) {
        context()->throwError("Stream.prototype.copy: error:"+
                              io->errorString());
    }
}

void
StreamPrototype::flush()
{
    QIODevice *io = thisIODevice();

    QFile *f = dynamic_cast<QFile*>(io);
    if (f) {
        f->flush();
        return;
    }

    // QAbstractSocket *s = dynamic_cast<QAbstractSocket*>(io);
    // if (s) {
    //     s->flush();
    // }    
}

bool
StreamPrototype::atEnd()
{
#ifdef _GUARDIAN_TARGET
    QFile *file = dynamic_cast<QFile*>(thisIODevice());
    if (file) {
        file->fileEngine();
        GFSFileEngine *fs_eng = dynamic_cast<GFSFileEngine*>(file->fileEngine());
        if (fs_eng && fs_eng->isEnscribe()) {
            return fs_eng->atEnd();
        }
    }
#endif
    return thisIODevice()->atEnd();
}

bool
StreamPrototype::isClosed()
{
    return !thisIODevice()->isOpen();
}

bool
StreamPrototype::isReadable()
{
    return thisIODevice()->isReadable();
}

bool
StreamPrototype::isSequential()
{
    return thisIODevice()->isSequential();
}

bool
StreamPrototype::isWritable()
{
    return thisIODevice()->isWritable();
}

bool 
StreamPrototype::lockRecord()
{
#ifdef _GUARDIAN_TARGET
    QFile *file = dynamic_cast<QFile*>(thisIODevice());
    if (file) {
        file->fileEngine();
        GFSFileEngine *fs_eng = dynamic_cast<GFSFileEngine*>(file->fileEngine());
        if (fs_eng && fs_eng->isEnscribe()) {
            return fs_eng->lockRecord();
        }
    }
#endif
    return false;
}

bool
StreamPrototype::unlockRecord()
{
#ifdef _GUARDIAN_TARGET
    QFile *file = dynamic_cast<QFile*>(thisIODevice());
    if (file) {
        file->fileEngine();
        GFSFileEngine *fs_eng = dynamic_cast<GFSFileEngine*>(file->fileEngine());
        if (fs_eng && fs_eng->isEnscribe()) {
            return fs_eng->unlockRecord();
        }
    }
#endif
    return false;
}

QScriptValue
StreamPrototype::read(unsigned bytes)
{
#ifdef _GUARDIAN_TARGET
    QFile *file = dynamic_cast<QFile*>(thisIODevice());
    if (file) {
        file->fileEngine();
        GFSFileEngine *fs_eng = dynamic_cast<GFSFileEngine*>(file->fileEngine());
        if (fs_eng && fs_eng->isEnscribe()) {
            // we invoke this because we don't need more than a record.
            QByteArray ba((int)bytes,(char)0);
            qint64 ret = fs_eng->read(ba.data(), bytes);            
            return ByteArrayClass::toScriptValue(engine(), ba.mid(0,ret));
        }
    }
#endif
    QByteArray ba(thisIODevice()->read(bytes));
    return ByteArrayClass::toScriptValue(engine(), ba);
}

QScriptValue
StreamPrototype::readInto( QScriptValue buffer, 
                           QScriptValue begin, 
                           QScriptValue end)
{
    ByteArray *ba = qscriptvalue_cast<ByteArray*>(buffer.data());
    if (!ba) {
        context()->throwError("Stream.prototype.readInto: arg(0) "  \
                              "not instance of class ArrayBuffer");
        return QScriptValue();
    }
    int start = 0;
    if (begin.isNumber()) {
        start = begin.toNumber();
    }
    int stop = ba->length();
    if (end.isNumber()) {
        stop = end.toNumber();
    }
    
    QByteArray arr = qvariant_cast<ByteArray>(read( stop-start ).data().toVariant());
    ba->replace(start,arr.length(),arr);
    
    if (thisIODevice()->atEnd()) {
        return -1;
    }

    return arr.length();
}

bool
StreamPrototype::skip(int bytes)
{
    
    return thisIODevice()->seek(thisIODevice()->pos() + bytes);
}

void
StreamPrototype::write( QScriptValue binary,
                        QScriptValue begin,
                        QScriptValue end)
{
    ByteArray  *ba = qscriptvalue_cast<ByteArray*>(binary.data());
    ByteString *bs = 0;
    QByteArray *b  = ba;

    QIODevice *io = thisIODevice();
#ifdef _GUARDIAN_TARGET //if we want to delete records
    if (binary.isNumber() && binary.toInt32() == 0) {
        QFile *file = dynamic_cast<QFile*>(io);
        if (file) {
            GFSFileEngine *fs_eng = dynamic_cast<GFSFileEngine*>(file->fileEngine());
            if (fs_eng /*&& fs_eng->isEnscribe()*/) {
                bool flg = fs_eng->isUpdateMode();
                fs_eng->setUpdateMode(true);
                if( fs_eng->write(0, 0) < 0) {
                    context()->throwError("Record delete."+fs_eng->errorString());
                }
                fs_eng->setUpdateMode(flg);
                return;
            }
        }
    }
#endif

    if (!b) {
        // Maybe is a ByteString
        bs = qscriptvalue_cast<ByteString*>( binary.data() );
        b = bs;
    }

    if (!b) {
        context()->throwError("Stream.prototype.write: arg(0) "\
                              "not instance of ByteArray or ByteString");
        return;
    }

    int start = 0;
    if (begin.isNumber()) {
        start = begin.toNumber();
        Q_ASSERT(start > b->length());
        return;
    }
    int stop = b->length();
    if (end.isNumber()) {
        stop = end.toNumber();
        Q_ASSERT(stop-start> b->length());
        return;
    }

    char *data = b->data();
#ifdef _GUARDIAN_TARGET
    QFile *file = dynamic_cast<QFile*>(io);
    if (file) {
        GFSFileEngine *fs_eng = dynamic_cast<GFSFileEngine*>(file->fileEngine());
        if (fs_eng /*&& fs_eng->isEnscribe()*/) {
            if( fs_eng->write(data+start, stop-start)<0) {
                context()->throwError(fs_eng->errorString());
                return;
            }
        }
        return;
    }
#endif
    io->write(data+start, stop-start );

    return;
}

//
//  Extra
//
qint64 
StreamPrototype::bytesAvailable()
{
#ifdef _GUARDIAN_TARGET
    QFile *file = dynamic_cast<QFile*>(thisIODevice());
    if (file) {
        GFSFileEngine *fs_eng = dynamic_cast<GFSFileEngine*>(file->fileEngine());
        if (fs_eng && fs_eng->isEnscribe()) {
            engine()->currentContext()->throwError("Stream.prototype.bytesAvailable is not available for Enscribe files opened with structured access");
            return -1;
        }
    }
#endif
    return thisIODevice()->bytesAvailable();
}

QString
StreamPrototype::errorString()
{
    return thisIODevice()->errorString();
}

bool
StreamPrototype::setKey(QScriptValue key, short spec)
{
#ifdef _GUARDIAN_TARGET
    QIODevice *io = thisIODevice();
    QFile *file = dynamic_cast<QFile*>(io);
    if (file) {
        GFSFileEngine *fs_eng = dynamic_cast<GFSFileEngine*>(file->fileEngine());
        if (fs_eng && fs_eng->isEnscribe() ) {
            //
            //  Looking to see if the parameter is Binary
            //
            ByteArray   *ba = NULL;
            ByteString  *bs = NULL;

            bool ret = false;
            ba = qscriptvalue_cast<ByteArray*>(key.data());
            if (!ba) {
                bs = qscriptvalue_cast<ByteString*>(key.data());
            }

            if (!ba && !bs) {
                engine()->currentContext()->throwError("Stream.prototype.setKey. should be instanceof Binary");
                ret = false;
            }

            if (ba) {
                ret =  fs_eng->setKey(*ba,spec);
            } else if (bs) {
                ret =  fs_eng->setKey(*bs,spec);
            }

            if (ret == false) {
                engine()->currentContext()->throwError("Stream.prototype.setKey. positioning failed.");
            }
            return ret;
        }
    }
#endif
    engine()->currentContext()->throwError("Stream.prototype.setKey not implemented for this file type");
    return false;
}

QScriptValue
StreamPrototype::getKey()
{
#ifdef _GUARDIAN_TARGET
    QIODevice *io = thisIODevice();
    QFile *file = dynamic_cast<QFile*>(io);
    if (file) {
        GFSFileEngine *fs_eng = dynamic_cast<GFSFileEngine*>(file->fileEngine());
        if (fs_eng && fs_eng->isEnscribe()) {
            return ByteArrayClass::toScriptValue(engine(), ByteArray(fs_eng->currentKeyValue()));

        }
    }
#endif
    engine()->currentContext()->throwError("Stream.prototype.setKey not implemented for this file type");
    return QScriptValue();
}

qint64
StreamPrototype::__internal__getPosition( )
{
    QIODevice *io = thisIODevice();

    if( io->isSequential() ) {
        engine()->currentContext()->throwError("Stream.prototype.position error. Device is sequential.");
        return 0;
    }

#ifdef _GUARDIAN_TARGET
    QFile *file = dynamic_cast<QFile*>(io);
    if (file) {
        GFSFileEngine *fs_eng = dynamic_cast<GFSFileEngine*>(file->fileEngine());
        Q_ASSERT(fs_eng);
        if (!fs_eng->isEnscribe()) {
#endif //_TANDEM
            return io->pos();
#ifdef _GUARDIAN_TARGET
        } else { // Enscribe position
            QByteArray pos_ba(fs_eng->currentKeyValue());
            bool ok;
            long long pos = QString(pos_ba.toHex()).toInt(&ok);
            if (!ok) {
                engine()->currentContext()->throwError("Stream.prototype.position error (enscribe).");
                return 0;
            }
            return pos;
        }
    }
#endif
    return io->pos();
}

void
StreamPrototype::__internal__setPosition( QScriptValue value )
{
    QIODevice *io = thisIODevice();

    if( io->isSequential() ) {
        engine()->currentContext()->throwError("Stream.prototype.position error. Device is sequential.");
        return;
    }

#ifdef _GUARDIAN_TARGET
    QFile *file = dynamic_cast<QFile*>(io);
    if ( file ) {
        GFSFileEngine *fs_eng = dynamic_cast<GFSFileEngine*>(file->fileEngine());
        Q_ASSERT(fs_eng);
        if (fs_eng->isEnscribe()) {
            if (value.isNumber()) {
                fs_eng->setPosition(value.toUInt32());
            } else {
                context()->throwError("Stream.prototype.position. Illegal usage.");
                return;
            }
        }
        else 
#endif
            if (!value.isNumber()) {
                engine()->currentContext()->throwError("Stream.prototype.position error");
            }
            else if (!io->seek(value.toInt32())) {
                engine()->currentContext()->throwError("Stream.prototype.position error positioning.");
            }
#ifdef _GUARDIAN_TARGET
    }
#endif
    return;
}

QIODevice*
StreamPrototype::thisIODevice()
{
    Stream *stream_ptr = qscriptvalue_cast<Stream*>(thisObject());
    if (!stream_ptr) {
         qFatal("programatic error StreamPrototype::thisIODevice");
         return 0;
    }

    if (!stream_ptr->priv_->isOpen()) {
        engine()->currentContext()->throwError("This stream is not opened anymore!");
    }

    QIODevice *io_ptr = stream_ptr->priv_;
    Q_ASSERT(io_ptr);

    return io_ptr;
}


include(../plugins.pri)

isEmpty(USE_SCRIPT_CLASSIC) {
    TARGET         = abscriptnet
} else {
    TARGET         = abscriptnet_compat
}

DESTDIR        = $$ATOMBOXER_SOURCE_TREE/plugins/script
!isEmpty(USE_SCRIPT_CLASSIC) {
    tandem {
        QT            -= core
    }
} else {
    QT            += core script
}


DEPENDPATH += .
INCLUDEPATH += . ../core

HEADERS  = \
           netplugin.h \
           tcpserver.h \
           tcpserverprototype.h \
           tcpsocket.h \
           tcpsocketprototype.h

# !isEmpty(ATOMBOXER_BUILD_SSL) {
# HEADERS +=   \
#            sslserver.h \
#            sslserverprototype.h \
#            sslsocket.h \
#            sslsocketprototype.h
# }

SOURCES  = \
           netplugin.cpp \
           tcpserver.cpp \
           tcpserverprototype.cpp \
           tcpsocket.cpp \
           tcpsocketprototype.cpp 

# !isEmpty(ATOMBOXER_BUILD_SSL) {
# SOURCES +=   \
#           sslserver.cpp \
#           sslserverprototype.cpp \
#           sslsocket.cpp \
#           sslsocketprototype.cpp
#}

RESOURCES += jsnetfiles.qrc

static {
    RESOURCES += net.qrc
}
else {
    CONFIG   += qt plugin
    QT       += network
    isEmpty(USE_SCRIPT_CLASSIC) {
         QT += script
         LIBS += -labscriptcore
    } else {
         LIBS += -labscriptcore_compat
    }

}

OTHER_FILES += \
    js/net.js

#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QtCore/QObject>
#include <QtNetwork/QTcpServer>

class TcpServer : public QObject
{
    friend class TcpServerPrototype;

    Q_OBJECT

public:
    TcpServer(QTcpServer *priv, QObject *parent = 0);
    ~TcpServer();

signals:
    void newConnection();

private:
    QTcpServer *priv_;
};

#endif //TCPSERVER_H


#include "tcpserver.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

TcpServer::TcpServer(QTcpServer *priv, QObject *parent) :
    QObject(parent)
{
    if (!priv) {
        qFatal("TcpServer::TcpServer programatic error");
    }

    priv_ = priv;

    QObject::connect(priv_, SIGNAL(newConnection()), 
                     this,  SIGNAL(newConnection()));
}


TcpServer::~TcpServer()
{
    if (priv_)
        delete priv_;
}


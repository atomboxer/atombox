#ifndef QT_NO_OPENSSL

#ifndef SSL_SERVER_H
#define SSL_SERVER_H

#include <QtNetwork/QTcpServer>
#include <QtNetwork/QSslSocket>
#include <QtNetwork/QSslKey>
#include <QtNetwork/QSslCertificate>
#include <QtNetwork/QAbstractSocket>

class QSslSocket;
class SslServer : public QTcpServer
{
    Q_OBJECT

public:
    SslServer(QObject *parent = 0);     
    QSslSocket *nextPendingConnection();
 
    void setPrivateKey(const QByteArray &key, QSsl::KeyAlgorithm algo = QSsl::Rsa,
                       QSsl::EncodingFormat format = QSsl::Pem, QSsl::KeyType type = QSsl::PrivateKey,
                const QByteArray &passPhrase = QByteArray() ) {
        key_ = QSslKey(key, algo, format, type, passPhrase);
    }

    void setLocalCertificate(const QByteArray &cert, QSsl::EncodingFormat format = QSsl::Pem) {
        cert_ = QSslCertificate(cert, format);
    }

protected:
   void incomingConnection (int socketDescriptor);

private slots:
   void slot_encrypted ();
   void slot_encryptedBytesWritten (qint64 written);
   void slot_modeChanged (QSslSocket::SslMode mode);
   void slot_peerVerifyError (const QSslError &error);
   void slot_sslErrors (const QList<QSslError> &errors);

   void slot_connected ();
   void slot_disconnected ();
   void slot_error (QAbstractSocket::SocketError);
   void slot_hostFound ();
   void slot_proxyAuthenticationRequired (const QNetworkProxy &, QAuthenticator *);
   void slot_stateChanged (QAbstractSocket::SocketState);
   void slot_readyRead ();

private:
   void _startServerEncryption ();
   void _connectSocketSignals ();

private:
    QSslKey           key_;
    QSslCertificate   cert_;
    QSslCertificate   ca_cert_;
    QSslSocket       *socket;
};

#endif //sslserver.h

#endif

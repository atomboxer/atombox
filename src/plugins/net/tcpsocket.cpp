
#include <QtNetwork/QTcpSocket>

#include "tcpsocket.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

TcpSocket::TcpSocket(QTcpSocket *priv, QObject *parent) :
    QObject(parent)
{
    if (!priv) {
        qFatal("TcpSocket::TcpSocket programatic error");
    }

    priv_ = priv;

    QObject::connect(priv_, SIGNAL(connected()), 
                     this,  SIGNAL(connected()));
    QObject::connect(priv_, SIGNAL(disconnected()), 
                     this,  SIGNAL(disconnected()));
    QObject::connect(priv_, SIGNAL(error(QAbstractSocket::SocketError)), 
                     this,  SIGNAL(error()));
    QObject::connect(priv_, SIGNAL(bytesWritten(qint64)), 
                     this,  SIGNAL(bytesWritten(qint64)));
    QObject::connect(priv_, SIGNAL(readyRead()), 
                     this,  SIGNAL(readyRead()));
}


TcpSocket::~TcpSocket()
{
    if (priv_)
        delete priv_;
}


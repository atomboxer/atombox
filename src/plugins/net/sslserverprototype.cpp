#include "sslserver.h"
#include "sslserverprototype.h"


#include <QtCore/QDebug>
#include <QtCore/QList>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#include <QtNetwork/QNetworkProxy>
#include <QtNetwork/QNetworkProxyQuery>
#include <QtNetwork/QNetworkProxyFactory>
#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QAuthenticator>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(SslServer*)
Q_DECLARE_METATYPE(QSslSocket*)

SslServerPrototype::SslServerPrototype(QObject *parent)
: QObject(parent)
{
    one_time_ = false;
}

SslServerPrototype::~SslServerPrototype()
{
}

QString
SslServerPrototype::address()
{
    return thisSslServer()->serverAddress().toString();
}

void
SslServerPrototype::close()
{
    return thisSslServer()->close();
}

QString
SslServerPrototype::errorString()
{
    return thisSslServer()->errorString();
}

bool
SslServerPrototype::isListening()
{
    return thisSslServer()->isListening();
}

bool
SslServerPrototype::listen( quint16 port, QScriptValue host)
{
    QHostAddress addr = QHostAddress::Any;
    if (!host.isUndefined()) {
        addr = QHostAddress(host.toString());
    }

    if (port == 0) {
        context()->throwError("Server.prototype.listen, invalid port specified");
        return false;
    }

    return thisSslServer()->listen(addr, port);
}

QScriptValue
SslServerPrototype::nextPendingConnection()
{          
    return engine()->newQObject((QObject*)(thisSslServer()->nextPendingConnection()), 
                                QScriptEngine::AutoOwnership,
                                QScriptEngine::SkipMethodsInEnumeration
                                /*| QScriptEngine::ExcludeSuperClassMethods
                                  | QScriptEngine::ExcludeSuperClassProperties*/);
}

quint16
SslServerPrototype::port()
{
    return thisSslServer()->serverPort();
}

SslServer *
SslServerPrototype::thisSslServer()
{
    SslServer *s = qscriptvalue_cast<SslServer*>(thisObject());
    Q_ASSERT(s);

    if (one_time_ == false) {
	one_time_ = true;
	QObject::connect(s, SIGNAL(newConnection()), 
			 this, SLOT(notify_newConnection()));	
    }
    return s;
}

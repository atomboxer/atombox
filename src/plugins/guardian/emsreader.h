#ifndef EMSREADER_H
#define EMSREADER_H

#include <QtCore/QObject>
#include <QtCore/QFile>
#include <QtCore/QProcess>
#include <QtCore/QMap>
#include <QtCore/QPair>
#include <QtScript/QScriptValue>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QDateTime>

#include "ZSPIC"
#include "ZEMSC"
#include "ZSYSC" 

#include "binary.h"

/* These defines are used for zems structs to shorten
   names */
#define EMSBUFDEF zems_ddl_msg_buffer_def
#define SPIERRDEF zspi_ddl_error_def

enum EMSSourceType {
    EMSSourceType_col = 0,
    EMSSourceType_log = 1
};

class EMSReader : public QObject
{
    Q_OBJECT
    
public:
    EMSReader( QString dist_ppd = "$ABX1", short dist_cpu = -1, short dist_pri = -1, QObject *parent = 0);
    ~EMSReader();
    QList<EMSBUFDEF *> readEvents(int max=1);
    bool eventToQScriptValue(EMSBUFDEF *, QScriptValue &value);
    bool eventToQString(EMSBUFDEF *event_buf, QString &value);

    bool getPosition( QDateTime &dt); 
    bool setPosition( QDateTime &dt); 

public slots:
    void    connectSource(QString source);
    void    disconnectSource();

    void           destroy();
    inline bool    atEnd() const { return eof_; }

    void    slot_emsdist_error(QProcess::ProcessError error);
    void    slot_emsdist_finished(int exitCode, QProcess::ExitStatus exitStatus);
    void    slot_emsdist_started();
    QString lastError() { return last_err_;  }

signals:
    void    connected();
    void    disconnected();
    void    error(QString e);

private:
    //
    bool startDistributorProcess();

    bool spi_cmd_set_source_col(const char *src);
    bool spi_cmd_set_source_log(const char *src);
    bool spi_cmd_disconnect_source(const char *src, int token);
    bool spi_cmd_set_position(long long ts);

    QList<EMSBUFDEF *> spi_cmd_read_next(int max=1);
    bool send_spi_cmd();
    void setLastError(QString err) { last_err_ = err; }

    
private:
    QProcess       *ems_dist_;
    QFile          *dist_;
    bool            eof_;
    bool            eof_stop_;

    QString         last_err_;
    short           last_ems_err_;
    long long       last_ts_;

    QString         dist_ppd_;
    short           dist_cpu_;
    short           dist_pri_;
    bool            dist_starting_;

    bool            connected_;
    QString         source_;
    EMSSourceType   source_type_;

    EMSBUFDEF *spi_buf_;
    EMSBUFDEF *sav_buf_;
    SPIERRDEF *err_buf_;

    /* Declare the ssids using the typedefs from the DDL output */
    zems_val_ssid_def zems_val_ssid_;
};

#endif //EMSREADER_H

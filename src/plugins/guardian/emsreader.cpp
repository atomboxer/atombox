#include "emsreader.h"

#include <cextdecs.h>
#include <tal.h>
#include <memory.h>

#include <QtCore/QDebug>
#include <QtCore/QFileInfo>
#include <QtCore/QStringList>
#include <QtCore/QCoreApplication>

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define EMSREADER_DEBUG


static QDateTime 
__julianToDateTime( long long ts ) 
{
    long long conv = CONVERTTIMESTAMP(ts);
    short     ts_arr[8];

    INTERPRETTIMESTAMP(conv, (short*)ts_arr);
    return QDateTime(QDate(ts_arr[0], ts_arr[1], ts_arr[2]), 
                     QTime(ts_arr[3], ts_arr[4], ts_arr[5], ts_arr[6]));
}

static long long 
__dateTimeToJulian( QDateTime dt ) 
{
    short ts_arr[8];

    ts_arr[0] = dt.date().year();
    ts_arr[1] = dt.date().month();
    ts_arr[2] = dt.date().day();

    ts_arr[3] = dt.time().hour();
    ts_arr[4] = dt.time().minute();
    ts_arr[5] = dt.time().second();
    ts_arr[6] = dt.time().msec();
    ts_arr[7] = 0;
    
    long long ts  = COMPUTETIMESTAMP(ts_arr);
    long long gts = CONVERTTIMESTAMP(ts, 2);
    return gts;
}

static bool __exists( QString path )
{
    short out_list[5];

    short error = FILE_GETINFOBYNAME_ (path.toLatin1().data(),
                                       path.size(),
                                       out_list  );
    if (error) {
        return false;
    }
   
    return true;
}

static bool __isFile( QString path )
{
    short out_list[5];

    short error = FILE_GETINFOBYNAME_ (path.toLatin1().data(),
                                       path.size(),
                                       out_list  );
    if (out_list[4] != -1) 
        return true;

    return false;
}

QString spi_error (short p_spi_err,
                   short p_spi_proc,
                   long p_tkn_code,
                   short p__debug)
{
    char msg[20];
    memset(msg, 0, 20);

#ifdef EMSREADER_DEBUG
    qDebug()<<"spi_error!!!";
#endif

    switch (p_spi_proc)
    {
    case ZSPI_VAL_SSINIT : sprintf(msg,"SSINIT"); break;
    case ZSPI_VAL_SSGET : sprintf(msg,"SSGET"); break;
    case ZSPI_VAL_SSGETTKN : sprintf(msg,"SSGETTKN"); break;
    case ZSPI_VAL_SSMOVE : sprintf(msg,"SSMOVE"); break;
    case ZSPI_VAL_SSMOVETKN : sprintf(msg,"SSMOVETKN"); break;
    case ZSPI_VAL_SSNULL : sprintf(msg,"SSNULL"); break;
    case ZSPI_VAL_SSPUT : sprintf(msg,"SSPUT"); break;
    case ZSPI_VAL_SSPUTTKN : sprintf(msg,"SSPUTTKN"); break;
    case ZSPI_VAL_BUFFER_FORMATSTART : sprintf(msg,"FORMATSTART"); break;
    case ZSPI_VAL_BUFFER_FORMATNEXT : sprintf(msg,"FORMATNEXT"); break;
    case ZSPI_VAL_BUFFER_FORMATFINISH : sprintf(msg,"FORMATFINISH"); break;
    case ZSPI_VAL_FORMAT_CLOSE : sprintf(msg,"FORMATCLOSE"); break;
    default : sprintf(msg,"???Unknown???"); break;
    } /* of switch (p_spi_proc) */

    sprintf(msg," (%d, ", p_spi_err);
    switch (p_spi_err)
    {
    case ZSPI_ERR_INVBUF : sprintf(msg,"Invalid Buffer"); break;
    case ZSPI_ERR_ILLPARM : sprintf(msg,"Illegal Param"); break;
    case ZSPI_ERR_MISPARM : sprintf(msg,"Missing Param"); break;
    case ZSPI_ERR_BADADDR : sprintf(msg,"Illegal Address"); break;
    case ZSPI_ERR_NOSPACE : sprintf(msg,"Buffer full"); break;
    case ZSPI_ERR_XSUMERR : sprintf(msg,"Invalid Checksum"); break;
    case ZSPI_ERR_INTERR : sprintf(msg,"Internal Error"); break;
    case ZSPI_ERR_MISTKN : sprintf(msg,"Missing Token"); break;
    case ZSPI_ERR_ILLTKN : sprintf(msg,"Illegal Token"); break;
    case ZSPI_ERR_BADSSID : sprintf(msg,"Bad SSID"); break;
    case ZSPI_ERR_NOTIMP : sprintf(msg,"Not implemented"); break;
    case ZSPI_ERR_NOSTACK : sprintf(msg,"Insufficient Stack"); break;
    case ZSPI_ERR_ZFIL_ERR : sprintf(msg,"File system error"); break;
    case ZSPI_ERR_ZGRD_ERR : sprintf(msg,"OS Kernel error"); break;
    case ZSPI_ERR_INV_FILE : sprintf(msg,"Invalid template file"); break;
    case ZSPI_ERR_CONTINUE : sprintf(msg,"Continue"); break;
    case ZSPI_ERR_NEW_LINE : sprintf(msg,"New line"); break;
    case ZSPI_ERR_NO_MORE : sprintf(msg,"No more"); break;
    case ZSPI_ERR_MISS_NAME : sprintf(msg,"Missing name"); break;
    case ZSPI_ERR_DUP_NAME : sprintf(msg,"Duplicate name"); break;
    case ZSPI_ERR_MISS_ENUM : sprintf(msg,"Missing enumeration"); break;
    case ZSPI_ERR_MISS_STRUCT : sprintf(msg,"Missing STRUCT"); break;
    case ZSPI_ERR_MISS_OFFSET : sprintf(msg,"Missing offset"); break;
    case ZSPI_ERR_TOO_LONG : sprintf(msg,"Too long"); break;
    case ZSPI_ERR_MISS_FIELD : sprintf(msg,"Missing field"); break;
    case ZSPI_ERR_NO_SCANID : sprintf(msg,"No SCAN ID"); break;
    case ZSPI_ERR_NO_FORMATID : sprintf(msg,"No Format ID"); break;
    case ZSPI_ERR_OCCURS_DEPTH : sprintf(msg,"Occurs depth"); break;
    case ZSPI_ERR_MISS_LABEL : sprintf(msg,"Missing label"); break;
    case ZSPI_ERR_BUF_TOO_LARGE: sprintf(msg,"Buffer is too big"); break;
    case ZSPI_ERR_OBJFORM : sprintf(msg,"Object form"); break;
    case ZSPI_ERR_OBJCLASS : sprintf(msg,"Object class"); break;
    case ZSPI_ERR_BADNAME : sprintf(msg,"Bad name"); break;
    case ZSPI_ERR_TEMPLATE : sprintf(msg,"Template"); break;
    case ZSPI_ERR_ILL_CHAR : sprintf(msg,"Illegal character"); break;
    case ZSPI_ERR_NO_TKNDEFID : sprintf(msg,"No TKNDEF ID"); break;
    case ZSPI_ERR_INCOMP_RESP : sprintf(msg,"Incomplete response"); break;
    default : sprintf(msg,"???Unknown???"); break;
    } /* of switch (p_spi_err) */
    sprintf(msg,")\n");
    /* Write a blank line for output clarity */

    if (p__debug) DEBUG();
#ifdef  EMSREADER_DEBUG
    qDebug()<<msg;
#endif
    return QString(msg);
}

EMSReader::EMSReader(QString dist_ppd, short dist_cpu, short dist_pri, QObject *parent) :
    QObject(parent)
{
    ems_dist_ = NULL;
    spi_buf_  = NULL;
    sav_buf_  = NULL;
    err_buf_  = NULL;

    dist_          = NULL;
    dist_ppd_      = dist_ppd;
    dist_cpu_      = dist_cpu;
    dist_pri_      = dist_pri;
    dist_starting_ = false;

    eof_           = false;
    eof_stop_      = false;
    connected_     = false;
    last_ems_err_  = 0;
}

void 
EMSReader::connectSource(QString src)
{
#ifdef EMSREADER_DEBUG
    qDebug()<<"EMSReader::connectSourc(,"<<src;
#endif

    if (connected_) {
        last_err_ = "allready connected";
        emit error(lastError());        
        return;
    }

    if (!__exists(src)) {
        last_err_ = "file or process does not exist";
        connected_ = false;
        emit error(lastError());
        return;
    }

    if (__isFile(src))
        source_type_ = EMSSourceType_log;
    else
        source_type_ = EMSSourceType_col;

    QString lsrc(24,' ');
    lsrc = lsrc.replace(0,(src.size()<24)?src.size():24, src);
    source_ = src;

#ifdef EMSREADER_DEBUG
    if (source_type_ ==EMSSourceType_log) 
        qDebug()<<"LOG::"<<src;
    else
        qDebug()<<"COL::"<<src;
#endif
    
    if (!dist_) {             
        if (!dist_starting_) {
            dist_starting_ = true;
            startDistributorProcess();
        } else {
            last_err_ = "distributor is starting";
            emit error(lastError());
            connected_ = false;            
        }
        return;
    } else {
        // distributor is already started
        if (source_type_ == EMSSourceType_col) {
            if (!spi_cmd_set_source_log(source_.toLatin1().data())) {
                emit error(lastError());
                connected_ = false;
            } else {
                emit connected();
                connected_ = true;
            }
        } else {
            //collector
            if (!spi_cmd_set_source_col(source_.toLatin1().data())) {
                emit error(lastError());
                connected_ = false;
            } else {
                emit connected();
                connected_ = true;
            }
        }
    }
}

void 
EMSReader::disconnectSource()
{
#ifdef EMSREADER_DEBUG
    qDebug()<<"EMSReader::disconnect,"<<source_;
#endif
    if (!connected_ || dist_starting_) {
        last_err_ = "Source is not connected";
        emit error(lastError());
        return;
    }

     if (source_type_ == EMSSourceType_col) {
         spi_cmd_disconnect_source(source_.toLatin1().data(), ZEMS_TKN_DISCONNECT_SRC_COLL);
     } else {
         spi_cmd_disconnect_source(source_.toLatin1().data(), ZEMS_TKN_DISCONNECT_LOG);
     }
     emit disconnected();
     connected_ = false;
}

QList<EMSBUFDEF*> 
EMSReader::readEvents( int max )
{
    if (!dist_->isOpen()) {
#ifdef EMSREADER_DEBUG
        qDebug()<<"EMSReader::read  Not Opened";
#endif
        return QList<EMSBUFDEF*>();
    }

#ifdef EMSREADER_DEBUG
    qDebug()<<"EMSReader::read";
#endif

    return spi_cmd_read_next( max );
}

// this should destroy the event_buf
bool
EMSReader::eventToQScriptValue(EMSBUFDEF *event_buf, QScriptValue &value)
{
    //
    // Test DSM
    //
    short dsm_status;
    short dsm_format;
    short status1, status2;

    dsm_status = SPI_BUFFER_FORMATSTART_(
        &dsm_format,
        0, /*decimal default*/
        -2, /*char 32-255*/
        0, /*no override*/
        1, /*displ redefines*/
        1, /*skip hidden*/
        0, /*represent header*/
        1, /*label*/
        0, /*buffer-values-only*/
        40,
        0, /*label len*/
        &status1, /*field label len*/
        &status2
        );

    if (dsm_status != 0) {
        last_err_ = "SPI_BUFFER_FORMATSTART_ error"+QString::number(dsm_status);
        return false;
    }
        
#define EVT_TEXT_LEN  120
#define NUM_EVT_LINES 10

    char  dsm_text[EVT_TEXT_LEN * NUM_EVT_LINES+1];
    memset(dsm_text,0, EVT_TEXT_LEN * NUM_EVT_LINES+1);
    short dsm_lengths[NUM_EVT_LINES+1];
    memset(dsm_lengths,0,  NUM_EVT_LINES+1);

    short dsm_size;
    QList<QString> lines;
    do {
        dsm_status = SPI_BUFFER_FORMATNEXT_(
            dsm_format,
            (short *)event_buf,
            dsm_text,
            EVT_TEXT_LEN * NUM_EVT_LINES,
            NUM_EVT_LINES,
            dsm_lengths
            );

        if (dsm_status != 0 && dsm_status != -16) {
            last_err_ = "SPI_BUFFER_FORMATNEXT_ error"+QString::number(dsm_status);
            delete event_buf;
            return false;
        }

        for (int i=0;i<NUM_EVT_LINES;i++) {
            if (dsm_lengths[i]>=0) {
                char line[EVT_TEXT_LEN+1];
                memcpy(line, &dsm_text[i*EVT_TEXT_LEN],dsm_lengths[i]);
                line[ dsm_lengths[i] ] = 0;
                lines.push_back(line);
            }
        }
    } while (dsm_status == -16);

    dsm_status = SPI_BUFFER_FORMATFINISH_(&dsm_format);
    if (dsm_status != 0) {
        last_err_ = "SPI_BUFFER_FORMATFINISH_ error"+QString::number(dsm_status);
        delete event_buf;
        return false;
    }

    QHash<QString, QString> vp;
    for (int i=0; i<lines.size(); i++) {

        QString k,v;
        if (lines[i].lastIndexOf(':') == -1) {
            k = lines[i].section(' ',0,0).trimmed().replace("-","_");       
            v = lines[i].section(' ',1).trimmed();
        } else {
            k = lines[i].section(':',0,0).trimmed().replace("-","_");       
            v = lines[i].section(':',1).trimmed();
        }

        if (k.size() == 0)
            continue;

        //
        // Look for spaces
        //
        int p = 0;
        if ((p = k.lastIndexOf(' ')) != -1) {
            k = k.mid(p);
        }

        if (k[0] == '(') {
            k = "wid_"+k.mid(1,k.size()-2).section(',',2);
        }
        if (value.property(k).isValid()) {
            value.setProperty(k,((value.property(k)).toString())+v);
        } else {
            value.setProperty(k,v);        
        }
    }

    delete event_buf;
    return true;
}

// this should destroy the event_buf
bool
EMSReader::eventToQString(EMSBUFDEF *event_buf, QString &value)
{
    const int evt_text_len  = 1024;
    const int num_evt_lines = 10;

    //
    //  TEXTOUT
    //
    short estext_status;
    char  eto_text[evt_text_len * num_evt_lines+1];
    memset(eto_text,0, evt_text_len * num_evt_lines+1);
    short eto_lengths[num_evt_lines+1];
    memset(eto_lengths,0,  num_evt_lines+1);

    short eto_size;

    estext_status = EMSTEXT( (short *)event_buf,
                             eto_text,
                             evt_text_len,
                             num_evt_lines,
                             (short*)&eto_lengths);

    if (estext_status != 0) {
        last_err_ = "EMSTEXT error:"+QString::number(estext_status);
        delete event_buf;
        return false;            
    }

    QString text;
    for (int i=0;i<num_evt_lines;i++) {
        if (eto_lengths[i]>=0) {
            char line[evt_text_len+1];
            memcpy(line, &eto_text[i*evt_text_len],eto_lengths[i]);
            line[ eto_lengths[i] ] = 0;
            text+=line;
        }
    }

    value =  text;
    delete event_buf;
    return true;
}


bool 
EMSReader::getPosition( QDateTime &dt )
{
    if (last_ts_ <=0 )
        return false;

    dt = __julianToDateTime(last_ts_);
    return true;
}
 
bool
EMSReader::setPosition( QDateTime &dt ) 
{
    long long ts = __dateTimeToJulian(dt);
    return spi_cmd_set_position(ts);
}

void 
EMSReader::destroy()
{
#ifdef EMSREADER_DEBUG
    qDebug()<<"EMSReader::destroy";
#endif

    if (spi_buf_)
        free (spi_buf_);

    if (sav_buf_)
        free (sav_buf_);

    if (err_buf_)
        free (err_buf_);

    if (dist_)
        delete dist_;

    if (ems_dist_) {
        ems_dist_->close();
        delete ems_dist_;
    }    
}


void 
EMSReader::slot_emsdist_error(QProcess::ProcessError err)
{
#ifdef EMSREADER_DEBUG
    qDebug()<<"slot_emsdist_error";
#endif
    last_err_ = "Cannot start distributor";
    emit error(lastError());        
}

void 
EMSReader::slot_emsdist_finished(int exitCode, QProcess::ExitStatus exitStatus)
{
#ifdef EMSREADER_DEBUG
    qDebug()<<"slot_emsdist_finished";
#endif
    last_err_ = "distributor has finished";
    emit error(lastError());        
}

void
EMSReader::slot_emsdist_started()
{
#ifdef EMSREADER_DEBUG
    qDebug()<<"slot_emsdist_started"<<ems_dist_->pid();
#endif

    dist_starting_ = false;
    QCoreApplication::processEvents();
    dist_ = new QFile((QString(dist_ppd_)+".#ZSPI"));

    unsigned io_opt = 0;
    io_opt = QIODevice::ReadOnly|QIODevice::WriteOnly;
    if (!dist_->open((QIODevice::OpenModeFlag)io_opt)) {

#ifdef EMSREADER_DEBUG
        qDebug()<<"EMSReader::slot_emsdist_started cannot open distributor process"
                <<dist_->errorString();
#endif
        emit error("Cannot open distributor process");
        connected_ = false;
        return;
    }

    if (source_type_ == EMSSourceType_log) {
        if (!spi_cmd_set_source_log(source_.toLatin1().data())) {
            emit error(lastError());
            connected_ = false;
        } else {
            emit connected();
            connected_ = true;
        }
    } else {
        //collector
        if (!spi_cmd_set_source_col(source_.toLatin1().data())) {
            emit error(lastError());
            connected_ = false;
        } else {
            emit connected();
            connected_ = true;
        }
    }
}

//private
bool
EMSReader::startDistributorProcess()
{
#ifdef EMSREADER_DEBUG
    qDebug()<<"EMSReader::startDistributorProcess";
#endif

    QString guardian_hometerm = "";
    QString guardian_in_file = "";
    QString guardian_out_file = "";
    QString guardian_lib = "";
    QString guardian_name = "";
    

    ems_dist_ = new QProcess();

    ems_dist_->setGuardianOptions(guardian_hometerm, 
                                  guardian_in_file, 
                                  guardian_out_file, 
                                  dist_cpu_, 
                                  dist_pri_,
                                  guardian_lib,
                                  dist_ppd_.toLatin1().data());

    QObject::connect(ems_dist_, SIGNAL(error(QProcess::ProcessError)), 
                     this,  SLOT(slot_emsdist_error(QProcess::ProcessError)));

    QObject::connect(ems_dist_, SIGNAL(finished(int, QProcess::ExitStatus)), 
                     this,  SLOT(slot_emsdist_finished(int, QProcess::ExitStatus)));

    QObject::connect(ems_dist_, SIGNAL(started()), 
                     this,  SLOT(slot_emsdist_started()));

 
    /* Initialize subsystem IDs */
    strncpy(zems_val_ssid_.u_z_filler.z_filler,ZSPI_VAL_TANDEM, 8);
    zems_val_ssid_.z_number  = ZSPI_SSN_ZEMS;
    zems_val_ssid_.z_version = ZEMS_VAL_VERSION;


    /* malloc some memory for spi buffers */
    spi_buf_ = (EMSBUFDEF *) malloc( sizeof(EMSBUFDEF));
    memset(spi_buf_, 0, sizeof(SPIERRDEF));

    if (spi_buf_ == NULL) {
        return false;
    }
        
    sav_buf_ = (EMSBUFDEF *) malloc( sizeof(EMSBUFDEF) );
    memset(sav_buf_, 0, sizeof(SPIERRDEF));

    if (sav_buf_ == NULL) {
        return false;
    }

    err_buf_ = (SPIERRDEF *) malloc( sizeof(SPIERRDEF) );
    memset(err_buf_, 0, sizeof(SPIERRDEF));

    if (err_buf_ == NULL) {
        return false;
    }

    ems_dist_->start("$SYSTEM.SYSTEM.EMSDIST", QStringList()<<"TYPE C");
    
    return true;
}

bool
EMSReader::spi_cmd_set_source_col(const char *src)
{ 
#ifdef EMSREADER_DEBUG
    qDebug()<<"EMSReader::spi_cmd_set_source_col::"<<src;
#endif

    char  expanded[12];
    short temp_defaults[9];

    int len =  FNAMEEXPAND ( (char*)src,
        (short*)expanded,
        temp_defaults); 

    if (len == 0) {
        last_err_ = "FNAMEEXPAND error";
        return false;
    }

   /* spi buffers and such */
    int spi_err;

    spi_err = SSINIT( (short *) spi_buf_, ZEMS_VAL_BUFLEN,
                      (short *) &zems_val_ssid_,
                      ZSPI_VAL_CMDHDR, ZEMS_CMD_CONTROL);
    
    if (spi_err != ZSPI_ERR_OK) {
        last_err_ = spi_error(spi_err, ZSPI_VAL_SSINIT, 0L, false); 
        return false;
    }
    
    /* place the connect-source-collector token in buffer */
    spi_err = SSPUTTKN( (short *)spi_buf_,
                        ZEMS_TKN_CONNECT_SRC_COLL,
                        expanded );

    if (spi_err != ZSPI_ERR_OK) {
        last_err_ = spi_error(spi_err, ZSPI_VAL_SSPUTTKN, 0L, false); 
        return false;
    }

    return send_spi_cmd();
}

bool
EMSReader::spi_cmd_set_source_log(const char *src)
{
#ifdef EMSREADER_DEBUG
    qDebug()<<"EMSReader::spi_cmd_set_source_log::"<<src;
#endif

    /* spi buffers and such */
    int  spi_err;

    char  expanded[12];
    short temp_defaults[9];

    strcpy((char *)temp_defaults, "$SYSTEM SYSTEM ");

    int len =  FNAMEEXPAND ( (char*)src,
                             (short*)expanded,
                             temp_defaults); 

    if (len == 0) {
        last_err_ = "FNAMEEXPAND error";
        return false;
    }

    spi_err = SSINIT( (short *) spi_buf_, ZEMS_VAL_BUFLEN,
                      (short *) &zems_val_ssid_,
                      ZSPI_VAL_CMDHDR, ZEMS_CMD_CONTROL);
    
    if (spi_err != ZSPI_ERR_OK) {
        last_err_ = spi_error(spi_err, ZSPI_VAL_SSINIT, 0L, false); 
        return false;
    }
    
    /* place the connect-source-collector token in buffer */
    spi_err = SSPUTTKN( (short *)spi_buf_,
                        ZEMS_TKN_CONNECT_LOG,
                        expanded );

    if (spi_err != ZSPI_ERR_OK) {
        last_err_ = spi_error(spi_err, ZSPI_VAL_SSPUTTKN, 0L, false); 
        return false;
    }

    eof_stop_ = true;
    
    return send_spi_cmd();
}

bool
EMSReader::spi_cmd_disconnect_source(const char *src, int token)
{

    /* spi buffers and such */
    int  spi_err;

    char  expanded[12];
    short temp_defaults[9];

    strcpy((char *)temp_defaults, "$SYSTEM SYSTEM ");

    int len =  FNAMEEXPAND ( (char*)src,
                             (short*)expanded,
                             temp_defaults); 

    if (len == 0) {
        last_err_ = "FNAMEEXPAND error";
        return false;
    }

    spi_err = SSINIT( (short *) spi_buf_, ZEMS_VAL_BUFLEN,
                      (short *) &zems_val_ssid_,
                      ZSPI_VAL_CMDHDR, ZEMS_CMD_CONTROL);
    
    if (spi_err != ZSPI_ERR_OK) {
        last_err_ = spi_error(spi_err, ZSPI_VAL_SSINIT, 0L, false); 
        return false;
    }
    
    /* place the connect-source-collector token in buffer */
    spi_err = SSPUTTKN( (short *)spi_buf_,
                        token,
                        expanded );

    if (spi_err != ZSPI_ERR_OK) {
        last_err_ = spi_error(spi_err, ZSPI_VAL_SSPUTTKN, 0L, false); 
        return false;
    }
    
    return send_spi_cmd();
}

QList<EMSBUFDEF *>
EMSReader::spi_cmd_read_next(int max)
{
#ifdef EMSREADER_DEBUG
    qDebug()<<"EMSReader::spi_cmd_read_msg";
#endif

    QList<EMSBUFDEF*> ret_list;

    short     spi_err;
    int       msgcount = 0;
    __int32_t tkn;
    short     byteoffset = 0;
    long long ts = 0;

    EMSBUFDEF *event_buf = NULL;

    spi_err = SSINIT( (short *) spi_buf_, ZEMS_VAL_BUFLEN,
                      (short *) &zems_val_ssid_,
                      ZSPI_VAL_CMDHDR, ZEMS_CMD_GETEVENT);

    if (spi_err != ZSPI_ERR_OK) {
        last_err_ =  spi_error(spi_err, ZSPI_VAL_SSINIT, 0L, false); 
        return ret_list;
    }

    if (eof_stop_) {
        tkn = ZSPI_VAL_TRUE;
        spi_err = SSPUTTKN( (short *)spi_buf_, ZEMS_TKN_EOFSTOP,
                            (char*)&tkn );

        if (spi_err != ZSPI_ERR_OK) {
            last_err_ = spi_error(spi_err, ZSPI_VAL_SSPUTTKN, 0L, false); 
            return ret_list;
        }
    }

    /* Save the original command */
    memcpy( (char *)sav_buf_, (char *)spi_buf_, sizeof(EMSBUFDEF) );

    while ((msgcount < max) && !(eof_)) {
        msgcount++;
        
        eof_ = false;

        if (!send_spi_cmd()) {
            eof_ = true;
            return ret_list;
        }
        
        /* Find the event message within the GETEVENT response */
        tkn = ZEMS_TKN_EVENT;

        /* Returns the offset of the event in the */
        /* spi buffer via event_buf_loc. */
        spi_err = SSGETTKN( (short *)spi_buf_, ZSPI_TKN_OFFSET,
                            (char*)&tkn, 1, (short*)&byteoffset);

 
        if (spi_err != ZSPI_ERR_OK) {
            if (spi_err == -8/*not found*/) {
                eof_ = true;
            }
            last_err_ =  spi_error(spi_err, ZSPI_VAL_SSGETTKN, 0L, false); 
            return ret_list;
        }

        event_buf = new EMSBUFDEF(); /*this will be deleted after 
                                       converting to QScriptValue*/

        memcpy(event_buf, ((char *)(spi_buf_) +byteoffset+2), 
               (sizeof(EMSBUFDEF) - byteoffset + 2));
        
        ret_list.push_back(event_buf);

        /* Save CONTEXT token from this GETEVENT response for */
        /* the next GETEVENT request */
        spi_err = SSMOVETKN(ZSPI_TKN_CONTEXT,
                            (short *)spi_buf_, 1,  /* Source */
                            (short *)sav_buf_, 1); /* Destination */

        if (spi_err != ZSPI_ERR_OK) {
            last_err_ = spi_error(spi_err, ZSPI_VAL_SSMOVETKN, 0L, false);
            return ret_list;
        }


        // Move the updated command to spi_buf for next time 
        // through the loop 
        memcpy( (char *)spi_buf_, (char *)sav_buf_,
                sizeof(EMSBUFDEF) );


        /* read the ZEMS_TKN_GENTIME */
        spi_err = SSGETTKN( (short *)event_buf, ZEMS_TKN_GENTIME, (char*)&ts);

        if (spi_err != ZSPI_ERR_OK) {
            last_err_ =  spi_error(spi_err, ZSPI_VAL_SSGETTKN, 0L, false); 
            return ret_list;
        }

        last_ts_ = ts;
    }
    return ret_list;
}


bool 
EMSReader::spi_cmd_set_position(long long ts)
{
#ifdef EMSREADER_DEBUG
    qDebug()<<"EMSReader::spi_cmd_set_position()";
#endif
    /* spi buffers and such */
    int  spi_err;
    __int32_t tkn;

    spi_err = SSINIT( (short *) spi_buf_, ZEMS_VAL_BUFLEN,
                      (short *) &zems_val_ssid_,
                      ZSPI_VAL_CMDHDR, ZEMS_CMD_CONTROL);

    if (spi_err != ZSPI_ERR_OK) {
        last_err_ = spi_error(spi_err, ZSPI_VAL_SSINIT, 0L, false); 
        return false;
    }
    
    eof_stop_ = false;

    spi_err = SSPUTTKN( (short *)spi_buf_,
                        ZEMS_TKN_GMTTIME,
                        (char *)&ts );

    if (spi_err != ZSPI_ERR_OK) {
        last_err_ = spi_error(spi_err, ZSPI_VAL_SSPUTTKN, 0L, false); 
        return false;
    }

    return send_spi_cmd();
}

bool 
EMSReader::send_spi_cmd()
{
#ifdef EMSREADER_DEBUG
    qDebug()<<"EMSReader::send_spi_cmd()";
#endif

    short ccval; /* for cc return from writeread */
    short spi_err, used_len;
    short ibuflen = ZEMS_VAL_BUFLEN;

    /* Determine how much buffer was used */
    spi_err = SSGETTKN( (short *)spi_buf_, ZSPI_TKN_USEDLEN,
                        (char *)&used_len );

    if (spi_err != ZSPI_ERR_OK) {
        last_err_ = spi_error(spi_err, ZSPI_VAL_SSGETTKN, 0L, false); 
        return false;
    }

    qint64 wr = 0;
    qint64 br = 0;

    wr = dist_->write( (char*)spi_buf_, used_len);
    memset((char*)spi_buf_, 0, sizeof(EMSBUFDEF));
    br = dist_->read((char*)spi_buf_,4096/*, ZEMS_VAL_BUFLEN*/ );

#ifdef EMSREADER_DEBUG
    qDebug()<<"wr="<<wr<<","<<"br="<<br;
#endif

    if (br <= 0 ) {
        last_err_ = "dist_->read(";
        return false;
    }

    /* reset the buffer length to what was declared for spi_buf_ */
    spi_err = SSPUTTKN( (short *)spi_buf_, ZSPI_TKN_RESET_BUFFER,
                        (char *)ibuflen );

    if (spi_err != ZSPI_ERR_OK) {
        last_err_ = spi_error(spi_err, ZSPI_VAL_SSPUTTKN, 0L, false); 
        return false;
    }

    /* Response is in the buffer--check for return code */
    spi_err = SSGETTKN( (short *)spi_buf_, ZSPI_TKN_RETCODE,
                        (char *)&last_ems_err_, 1 );

    if (spi_err != ZSPI_ERR_OK) {
        last_err_ =  spi_error(spi_err, ZSPI_VAL_SSGETTKN, 0L, false);
#ifdef EMSREADER_DEBUG
        qDebug()<<"spi_err :"<<last_ems_err_;;
#endif
        return false;
    }

    if (last_ems_err_ != 0) {
        if (last_ems_err_ == 1045) {
            spi_err = SSGETTKN( (short *)spi_buf_, ZEMS_ERR_DEVTYPE,
                    (char *)&last_ems_err_, 1 );
            if (spi_err != ZSPI_ERR_OK) {
#ifdef EMSREADER_DEBUG
        qDebug()<<"spi_err :"<<last_ems_err_;;
#endif
        return false;
    }
        }
        last_err_ = "ems error="+QString::number(last_ems_err_);
#ifdef EMSREADER_DEBUG
        qDebug()<<"last_ems_err :"<<last_ems_err_;;
#endif
        return false;
    }

#ifdef EMSREADER_DEBUG
    qDebug()<<"send_spi_cmd OK";
#endif

    return true;
}

EMSReader::~EMSReader()
{
#ifdef EMSREADER_DEBUG
    qDebug()<<"EMSReader::~EMSReader()";
#endif
    destroy();
}

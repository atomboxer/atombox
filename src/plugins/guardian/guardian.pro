include(../plugins.pri)

tandem {
    TARGET        = abscriptguardian
    DESTDIR        = $$ATOMBOXER_SOURCE_TREE/plugins/script

    DEPENDPATH  += . 
    INCLUDEPATH += . $$ATOMBOXER_SOURCE_TREE/src/main/ $$ATOMBOXER_SOURCE_TREE/src/plugins/core .

DESTDIR        = $$ATOMBOXER_SOURCE_TREE/plugins/script
!isEmpty(USE_SCRIPT_CLASSIC) {
    QT            -= core
} else {
    QT            += core script
}

    HEADERS  = \
        gfsfileengine.h   \ 
        gfsfileengine_p.h \
        gfsfileenginehandler.h \
        guardianplugin.h \
        gfilesystem.h \
        emsreader.h \
        emsreaderprototype.h \
        pathway.h \
        pathwayprototype.h
        
    SOURCES  = gfsfileengine.cpp \
               gfsfileengine_p.cpp \
               gfsfileenginehandler.cpp \
               guardianplugin.cpp \
               gfilesystem.cpp \
               emsreader.cpp \
               emsreaderprototype.cpp \
               pathway.cpp \
               pathwayprototype.cpp

    RESOURCES += guardianfiles.qrc

    static {
        RESOURCES += guardian.qrc
        LIBS       += -L$$ATOMBOXER_SOURCE_TREE/lib/
    } else {
        CONFIG += qt plugin
    }
}

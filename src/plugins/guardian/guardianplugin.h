#ifndef GUARDIANPLUGIN_H
#define GUARDIANPLUGIN_H

#include <QScriptExtensionPlugin>

class GuardianPlugin : public QScriptExtensionPlugin
{
public:
    GuardianPlugin( QObject *parent = 0);
    ~GuardianPlugin();

    virtual void initialize(const QString &key, QScriptEngine *engine);
    virtual QStringList keys() const;

private:
    // nothing
};

#endif //GUARDIANPLUGIN_H

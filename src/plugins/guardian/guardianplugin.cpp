#include <QtCore/QDebug>
#include <QtCore/QCoreApplication>

#include <QtScript/QScriptEngine>

#include "guardianplugin.h"
#include "gfilesystem.h"

#include "pathway.h"
#include "pathwayprototype.h"

#include "emsreader.h"
#include "emsreaderprototype.h"

#include <cextdecs.h>
#include <tal.h>

#include <gfsfileenginehandler.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_EXPORT_PLUGIN2( abscriptguardian, GuardianPlugin )

GuardianPlugin::GuardianPlugin( QObject *parent )
    : QScriptExtensionPlugin( parent )
{
}
 	
GuardianPlugin::~GuardianPlugin()
{
}

Q_DECLARE_METATYPE(Pathway*)
Q_DECLARE_METATYPE(EMSReader*)

QScriptValue 
Pathway_Constructor(QScriptContext *ctx, QScriptEngine *eng)
{
    if (ctx->argumentCount() != 2) {
        ctx->throwError("<<guardian/pathway.Pathway>> constructor require two arguments");
        return QScriptValue();
    }

    QString processName     = ctx->argument(0).toString();
    QString serverClassName = ctx->argument(1).toString();

    return eng->newQObject(new Pathway(processName, 
                                       serverClassName),
                           QScriptEngine::AutoOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| QScriptEngine::ExcludeSuperClassMethods(*/
                           /*| QScriptEngine::ExcludeSuperClassProperties*/);
}

QScriptValue 
EMSReader_Constructor(QScriptContext *ctx, QScriptEngine *eng)
{
    if (ctx->argumentCount() > 3) {
        ctx->throwError("<<guardian/pathway.EMSReader>> invalid arguments");
        return QScriptValue();
    }
    
    QString ppd = "$ABX1";
    qint32  cpu = -1;
    qint32  pri = -1;

    if (!ctx->argument(0).isUndefined())
        ppd = ctx->argument(0).toString();

    if (!ctx->argument(1).isUndefined())
        cpu = ctx->argument(1).toInt32();

    if (!ctx->argument(2).isUndefined())
        pri = ctx->argument(2).toInt32();

    return eng->newQObject(new EMSReader(ppd, cpu, pri),
                           QScriptEngine::AutoOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| QScriptEngine::ExcludeSuperClassMethods(*/
                           /*| QScriptEngine::ExcludeSuperClassProperties*/);
}

void myMessageOutput(QtMsgType type, const char *msg)
{
     //in this function, you can write the message to any stream!
    switch (type) {
     case QtDebugMsg:
         fprintf(stdout, "%s\n", msg);
         break;
     case QtWarningMsg:
         fprintf(stdout, "%s\n", msg);
         break;
     case QtCriticalMsg:
         fprintf(stdout, "%s\n", msg);
         break;
     case QtFatalMsg:
         fprintf(stdout, "%s\n", msg);
         abort();
     }
     fflush(stdout);
     fflush(stderr);
}
 	
void
GuardianPlugin::initialize( const QString &key, QScriptEngine *engine )
{
    QScriptValue globalObject = engine->globalObject();
    if ( key == QString("guardian") ) {
#ifdef AB_STATIC_PLUGINS
	Q_INIT_RESOURCE(guardian);
#endif
	Q_INIT_RESOURCE(guardianfiles);
    }

    TandemEngineHandler::instance();
    qInstallMsgHandler(myMessageOutput);

    engine->globalObject().property("__internal__").
	setProperty("isEntrySequenced", engine->newFunction(__fs_g_isEntrySequenced));
    engine->globalObject().property("__internal__").
	setProperty("isKeySequenced", engine->newFunction(__fs_g_isKeySequenced));
    engine->globalObject().property("__internal__").
	setProperty("isUnstructured", engine->newFunction(__fs_g_isUnstructured));
    engine->globalObject().property("__internal__").
	setProperty("isRelative", engine->newFunction(__fs_g_isRelative));
    engine->globalObject().property("__internal__").
	setProperty("isEdit", engine->newFunction(__fs_g_isEdit));
    
    engine->globalObject().property("__internal__").
        setProperty("fileCode", engine->newFunction(__fs_g_fileCode));
    engine->globalObject().property("__internal__").
        setProperty("physicalRecordSize", engine->newFunction(__fs_g_physicalRecordSize));
    engine->globalObject().property("__internal__").
        setProperty("logicalRecordSize", engine->newFunction(__fs_g_logicalRecordSize));
    engine->globalObject().property("__internal__").
        setProperty("blockSize", engine->newFunction(__fs_g_blockSize));

    engine->globalObject().property("__internal__").
        setProperty("beginTransaction", engine->newFunction(__fs_g_beginTransaction));

    engine->globalObject().property("__internal__").
        setProperty("endTransaction", engine->newFunction(__fs_g_endTransaction));

    engine->globalObject().property("__internal__").
        setProperty("abortTransaction", engine->newFunction(__fs_g_abortTransaction));

    engine->globalObject().property("__internal__").
        setProperty("logEMS", engine->newFunction(__fs_g_logEMS));

    engine->setDefaultPrototype(qMetaTypeId<EMSReader*>(),
                                engine->newQObject(new EMSReaderPrototype(this)));

    engine->globalObject().property("__internal__").
        setProperty("EMSReader", engine->newFunction(EMSReader_Constructor));

    engine->globalObject().property("__internal__").property("EMSReader").
        setProperty("prototype", engine->defaultPrototype(qMetaTypeId<EMSReader*>()))
    ;

}
 	
QStringList
GuardianPlugin::keys() const
{
    QStringList keys;
    keys << QString("guardian");
    return keys;
}

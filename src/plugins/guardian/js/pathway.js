
/**
 * @module guardian/pathway
 * @since version 0.3
 * @desc This module provides access to Pathway (Pathsend) Guardian API through the Pathway class. It supports both conversational modes and context-free calls.
 * @example
/..
//converational mode (dialog)
var Pathway = require("guardian/pathway").Pathway;

var d = new Pathway("$D5PMN", "SERVER-NCPI-5A");

try {
var res = d.dialogBegin("hello world".toByteArray());
catch(e) {}

d.dialogEnd();

 * @example
//context-free mode
/..
//converational mode (dialog)
var Pathway = require("guardian/pathway").Pathway;

var d = new Pathway("$D5PMN", "SERVER-NCPI-5A");

try {
    var res = d.send("hello world".toByteArray()); 
} catch (e) {}
 */

/**
 * @class
 * @since version 0.3
 * @name Pathway
 * @classdesc This class provides access to Guardian Pathsend API (both conversational and context-free modes).
 * @summary Guardian Pathsend API
 * @arg {String} processName - the PATHMON process name
 * @arg {String} serverClassName - the serverclass name
 * @memberof module:guardian/pathway
 * @example

//context-free mode
/..
//converational mode (dialog)
var Pathway = require("guardian/pathway").Pathway;

var d = new Pathway("$D5PMN", "SERVER-NCPI-5A");

try {
    var res = d.send("hello world".toByteArray()); 
} catch (e) {}
 */

/**
 * This method aborts the current dialog
 * @method module:guardian/pathway.Pathway.prototype.dialogAbort
 * @returns {Boolean}
 */

/**
 * This method begins a dialog (conversational mode), sends the request, and returns the pathsend response. In case of an error, it throws an exception with the error details.
 * @method module:guardian/pathway.Pathway.prototype.dialogBegin
 * @arg {ByteArray|ByteString} rqst The pathway request
 * @returns {ByteArray} the pathway response as a ByteArray
 * @throws Error
 */

/**
 * This method ends the current dialog
 * @method module:guardian/pathway.Pathway.prototype.dialogEnd
 * @returns {Boolean}
 */

/**
 * This method sends the request for an already opened dialog, and returns the pathsend response. In case of an error, it throws an exception with the error details.
 * @method module:guardian/pathway.Pathway.prototype.dialogBegin
 * @arg {ByteArray|ByteString} rqst The pathway request
 * @returns {ByteArray} the pathway response as a ByteArray
 * @throws Error
 */

/**
 * This method returns the current Pathway error in string format
 * @method module:guardian/pathway.Pathway.prototype.errorString
 * @returns {String}
 */

/**
 * This method is used for free-context Pathway server requests. It sends the request, and returns the pathsend response. In case of an error, it throws an exception with the error details.
 * @method module:guardian/pathway.Pathway.prototype.send
 * @arg {ByteArray|ByteString} rqst The pathway request
 * @returns {ByteArray} the pathway response as a ByteArray
 * @throws Error
 */
exports.Pathway = __internal__.Pathway;

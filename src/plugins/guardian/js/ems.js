
/**
 * @module guardian/ems
 * @since version 0.7
 * @desc This module provides access to EMS.
 * @example
 */

/**
 * @class
 * @since version 0.7
 * @name EMSReader
 * @classdesc This class provides access to EMS
 * @summary EMS
 * @memberof module:guardian/ems
 * @example

 */

exports.EMSReader = __internal__.EMSReader;

/**
 *  @method module:guardian/ems.logEMS
 *  @desc Logs an event to Tandem EMS. The severity of the event is based on the event_number value as follows: 0-99 - Informative, 100-199 - Major, 300-399 - Fatal 
 *  @arg {String} collector
 *  @arg {String} text
 *  @arg {Number} event_number
 *  @example
var system  = require("system");
var ems     = require("guardian/ems");

ems.logEMS("$0", "Hello world!", 50);
system.exit(0);
 */
exports.logEMS    = __internal__.logEMS;

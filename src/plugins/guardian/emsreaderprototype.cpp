#include "emsreader.h"
#include "emsreaderprototype.h"

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#include <QtCore/QDebug>

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(EMSReader*)
Q_DECLARE_METATYPE(ByteArray*)

EMSReaderPrototype::EMSReaderPrototype(QObject *parent)
    : QObject(parent)
{

}

EMSReaderPrototype::~EMSReaderPrototype()
{

}

QScriptValue
EMSReaderPrototype::readSPI(int max) const
{
    QList<EMSBUFDEF*> event_list;
    event_list = thisEMSReader()->readEvents(max);
    QScriptValue ret;
    if (max > 1)
        ret = engine()->newArray();
    else
        ret = engine()->newObject();

    if (max > 1) {
        for (int i = 0; i < event_list.size(); ++i) {
            if (event_list.at(i) != NULL) {
                QScriptValue val = engine()->newObject();
                if (!thisEMSReader()->eventToQScriptValue(event_list.at(i), val)) {
                    ;//
                }
                
                ret.setProperty(i,val);
            }
        }
    } else {
        if (!thisEMSReader()->eventToQScriptValue(event_list.at(0), ret)) {
            ;//
        }
    }
        
    return ret;
}

QScriptValue
EMSReaderPrototype::readText(int max) const
{
    QList<EMSBUFDEF*> event_list;
    event_list = thisEMSReader()->readEvents(max);

    QScriptValue ret;
    if (max > 1)
        ret = engine()->newArray();

    if (max > 1) {
        for (int i = 0; i < event_list.size(); ++i) {
            if (event_list.at(i) != NULL) {
                QString val;
                if (!thisEMSReader()->eventToQString(event_list.at(i), val)) {
                    ;//
                }
                
                ret.setProperty(i,val);
            }
        }
    } else {
        QString val;
        if (!thisEMSReader()->eventToQString(event_list.at(0), val)) {
            ;//
        }
        ret = engine()->newVariant(val);
    }
        
    return ret;
}

QScriptValue 
EMSReaderPrototype::getPosition() const
{
    QDateTime dt;

    if (! thisEMSReader()->getPosition(dt)) {
        return QScriptValue();
    }

    return engine()->newVariant(dt);
}

QScriptValue
EMSReaderPrototype::setPosition(QScriptValue obj) const
{
    if (!obj.isDate()) {
        context()->throwError("invalid parameter. Must be instanceof Date");
        return QScriptValue();
    }

    QDateTime dt = obj.toDateTime();

    if (!thisEMSReader()->setPosition(dt)) {
        context()->throwError(thisEMSReader()->lastError());
        return false;
    }

    return true;
}

EMSReader *
EMSReaderPrototype::thisEMSReader() const
{
    EMSReader *p = qscriptvalue_cast<EMSReader*>(thisObject());
    if (!p) {
        qFatal("Programmatic error EMSReaderPrototype::thisEMSReader");
    }

    return p;
}

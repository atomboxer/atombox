#ifndef GFSFILEENGINE_H
#define GFSFILEENGINE_H

#include <QtCore/qabstractfileengine.h>

class GFSFileEnginePrivate;

class  GFSFileEngine : public QAbstractFileEngine
{
    Q_DECLARE_PRIVATE(GFSFileEngine)
public:
    GFSFileEngine();
    explicit GFSFileEngine(const QString &file);
    virtual ~GFSFileEngine();

    bool open(QIODevice::OpenMode openMode);
    bool open(QIODevice::OpenMode flags, FILE *fh);
    bool close();
    bool flush();
    qint64 size() const;
    qint64 pos() const;
    bool seek(qint64);
    bool isSequential() const;
    bool remove();
    bool copy(const QString &newName);
    bool rename(const QString &newName);
    bool link(const QString &newName);
    bool mkdir(const QString &dirName, bool createParentDirectories) const;
    bool rmdir(const QString &dirName, bool recurseParentDirectories) const;
    bool setSize(qint64 size);
    bool caseSensitive() const;
    bool isRelativePath() const;
    QStringList entryList(QDir::Filters filters, const QStringList &filterNames) const;
    FileFlags fileFlags(FileFlags type) const;
    bool setPermissions(uint perms);
    QString fileName(FileName file) const;
    uint ownerId(FileOwner) const;
    QString owner(FileOwner) const;
    QDateTime fileTime(FileTime time) const;
    void setFileName(const QString &file);
    int handle() const;

    Iterator *beginEntryList(QDir::Filters filters, const QStringList &filterNames);
    Iterator *endEntryList();

    qint64 read(char *data, qint64 maxlen);
    qint64 readLine(char *data, qint64 maxlen);
    qint64 write(const char *data, qint64 len);

    bool extension(Extension extension, const ExtensionOption *option = 0, ExtensionReturn *output = 0);
    bool supportsExtension(Extension extension) const;

    //FS only!!
    bool open(QIODevice::OpenMode flags, int fd_);
    static bool setCurrentPath(const QString &path);
    static QString currentPath(const QString &path = QString());
    static QString homePath();
    static QString rootPath();
    static QString tempPath();
    static QFileInfoList drives();

    //
    //  Tandem specific
    //
    bool atEnd() const; 
    QByteArray currentKeyValue() const;
    bool isEnscribe() const; 
    bool isKeySequenced() const; 
    bool isUpdateMode() const;
    bool lockRecord();
    bool unlockRecord();
    bool setKey(char *key_val, short key_len, short key_spec = 0) const;
    bool setKey(QByteArray key, short key_spec = 0 ) const;
    bool setPosition(long long rec_spec) const;
    void setUpdateMode(bool flg);
    bool waitForReadyRead(qint32 msecs);
protected:
    GFSFileEngine(GFSFileEnginePrivate &dd);
};

#endif // GFSFILEENGINE_H

#ifndef EMSREADERPROTOTYPE_H
#define EMSREADERPROTOTYPE_H

#include "emsreader.h"
#include "binary.h"

#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

class EMSReaderPrototype : public QObject, public QScriptable
{
    Q_OBJECT
    
public:
    EMSReaderPrototype(QObject *parent = 0);
    ~EMSReaderPrototype();

public slots:
    QScriptValue readText(int max=1) const;
    QScriptValue readSPI(int max=1) const;

    QScriptValue getPosition() const;
    QScriptValue setPosition(QScriptValue dt) const; 

private:
    EMSReader * thisEMSReader() const;
};

#endif //EMSREADERPROTOTYPE_H


#include <QtScript/QScriptEngine>
#include <QtCore/QObject>
#include <QtCore/QStringList>
#include <QtCore/QBitArray>
#include <QtCore/QIODevice>
#include <QtCore/QBuffer>

#include <assert.h>
#include <iostream>

#include "atom.h"
#include "atomclass.h"
#include "atomisobitmap.h"
#include "atomstring.h"
#include "boxprototype.h"

#include "utils.h"

#include "bytestringclass.h"
#include "bytearrayclass.h"

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


Q_DECLARE_METATYPE(BoxClass*)
Q_DECLARE_METATYPE(WrapBoxPointer*)
Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)
Q_DECLARE_METATYPE(IODevicePtr*)

BoxPrototype::BoxPrototype(QObject *parent)
    : QObject(parent)
{

}

BoxPrototype::~BoxPrototype()
{
}

void
BoxPrototype::destroy()
{
    delete thisBox();
}

WrapBox*
BoxPrototype::thisBox() const
{
    WrapBoxPointer *ptr = qscriptvalue_cast<WrapBoxPointer*>(thisObject().data());
    if (!ptr) {
        qWarning("programmatic error in BoxPrototype::thisBox");
        return 0;
    }
    return ptr->data();
}

void
BoxPrototype::clear() const
{
    thisBox()->c()->clear();
}

void
BoxPrototype::pack(ByteArray ba)
{

    size_t cost = 0;

    QBuffer buffer(&ba);
    buffer.open(QIODevice::ReadOnly);
    
    cost = ba.length();

    engine()->reportAdditionalMemoryCost(cost);

    thisBox()->c()->clear();
    thisBox()->pack((QIODevice*)(&buffer));
    buffer.close();
}

ByteArray
BoxPrototype::unpack()
{
    return thisBox()->unpack();
}

QScriptValue
BoxPrototype::toString() const
{
    if (thisBox())
        return QString::fromLatin1("object");

    return QScriptValue(QScriptValue::UndefinedValue);
}

//private helpers
void
BoxPrototype::appendBitArray(QBitArray &a1, QBitArray a2)
{
    if (a1.count() == 0) {
        a1 = a2;
        return;
    }

    int idx_a1 = a1.count();
    a1.resize(a1.count()+a2.count());
    int idx_a2 = 0;

    while (idx_a2<a2.count()) {
        a1.setBit(idx_a1,a2.testBit(idx_a2));
        idx_a1++;
        idx_a2++;
    }
}

void
BoxPrototype::setByteOrder(unsigned short o) const
{
    if ( (ByteOrder)o != BigEndian &&
         (ByteOrder)o != LittleEndian ) {
          context()->throwError("Box.prototype.setByteOrder invalid value");
          return;
    }

    thisBox()->c()->setByteOrder((ByteOrder)o);
}

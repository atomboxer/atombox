#ifndef BOXCLASS_H
#define BOXCLASS_H

#include <QtCore/QObject>
#include <QtCore/QMap>
#include <QtCore/QWeakPointer>
#include <QtCore/QSharedPointer>
#include <QtCore/QPointer>

#include <QtCore/QString>
#include <QtCore/QIODevice>

#include <QtScript/QScriptClass>
#include <QtScript/QScriptString>

#include "wrapbox.h"

typedef QSharedPointer<WrapBox> WrapBoxPointer;

class BoxPrototype;
class BoxClass : public QObject, public QScriptClass
{
public:
    BoxClass(QScriptEngine *engine, WrapBox *model);
    ~BoxClass();

    QScriptValue constructor() const { return ctor_; }
    WrapBox* model() const { return model_; }
    QString name() const { return QLatin1String("Box"); }

    QScriptValue newInstance(const WrapBoxPointer &box);
    
    QScriptValue property(const QScriptValue &object,
                          const QScriptString &name, uint id);

    QHash<QString, QScriptValue> properties() const {return properties_; }
    QScriptValue::PropertyFlags propertyFlags( const QScriptValue &object,
                           const QScriptString &name,
                           uint id);

    QueryFlags queryProperty(const QScriptValue &object,
                             const QScriptString &name,
                             QueryFlags flags, 
                             uint *id);
    void resetState(); 
    void setProperty(QScriptValue &object, const QScriptString &name, 
             uint id, const QScriptValue &value);     
    
    //QScriptClassPropertyIterator *newIterator(const QScriptValue &object);
    
    QScriptValue prototype() const { return proto_; }

    static QScriptValue construct(QScriptContext *ctx, QScriptEngine *eng);
    static QScriptValue toScriptValue(QScriptEngine *eng, const WrapBoxPointer &box);
    static void fromScriptValue(const QScriptValue &obj, WrapBoxPointer &box);
    static QScriptValue toWBScriptValue(QScriptEngine *eng, const WrapBox &box);
    static void fromWBScriptValue(const QScriptValue &obj, WrapBox &box);
private:

    QScriptString bytes_;
    QScriptString display_;
    QScriptString index_;
    QScriptString length_;
    QScriptString name_;
    QScriptString offset_;
    QScriptString state_;

    QScriptValue  proto_;
    BoxPrototype *box_proto_;
    QScriptValue  ctor_;
   
    WrapBox*     model_;
    QHash<QString, QScriptValue> properties_;
};
#endif //boxclass.h


/**
 *  @module ddl
 *  @desc The "DDL" plugin provides an API, parser and interpretor for the Atom/Box DDL files. 
 *  @summary AtomBox DDL mappings
 *  @note This documentation is a just a stub and it is in progress.
 *  @see {@link JSON.toAtomBox}
 *  @see {@link module:ddl.Box.prototype.toJSON}
 */


/**
 * This constructor is not available to the AtomBox scripting interpretor.
 * @class
 * @name Box
 * @requires module:ddl
 * @abstract
 * @classdesc Box objects are produced by the Atom/Box DDL module, and cannot be instantiated directly.
 * @summary Box
 * @memberof module:ddl
 * @note This documentation is a just a stub and it is in progress.
 */

/**
 * Fired when the value of one it's contents (atom or box) changes it's value
 * @event module:ddl.Box.prototype.changed
 */

/**
 * Fired when all it's contents (atom or box) finished packing
 * @event module:ddl.Box.prototype.packed
 */

/**
 * Returns the Box representation
 * @member {ByteArray} module:ddl.Box.prototype.bytes
 */

/**
 * Returns the Box state (0 = OK_STATE, 1 = WARNING_STATE, 2 = ERROR_STATE, 3 = DEFAULT_STATE)
 * @member {Number} module:ddl.Box.prototype.state
 */

/**
 * Returns the Box offset
 * @member {Number} module:ddl.Box.prototype.offset
 */

/**
 * Pack
 * @method module:ddl.Box.prototype.pack
 * @arg {ByteString|ByteArray} ba
 */

/**
 * Unpack
 * @method module:ddl.Box.prototype.unpack
 * @returns {ByteString|ByteArray}
 */

/**
 * Returns true if the Box is a redefine, false otherwise.
 * @method module:ddl.Box.prototype.isRedefine
 * @returns {Boolean}
 */

/**
 * Returns true if the Box has at least one redefine, false otherwise.
 * @method  module:ddl.Box.prototype.hasRedefines
 * @returns {Boolean}
 */

/**
 * Returns true if the Box is a record (DDL and toplevel only)
 * @method module:ddl.Box.prototype.isRecord
 * @returns {Boolean}
 */

/**
 * Returns true if the contents of this Box are forming a key (DDL)
 * @method {ByteArray} module:ddl.Box.prototype.isKey
 * @returns {Boolean}
 */
 
 /**
 * AtomBox model to JSON conversion
 * @method module:ddl.Box.prototype.toJSON
 * @arg {Boolean} include_redefines
 * @returns {String} the JSON string
 */


/**
 * Sets the Byte Order to all of the boxes and (numeric binary) atoms it contains. 
 * @method {ByteArray} module:ddl.Box.prototype.setByteOrder
 * @arg {BIG_ENDIAN|LITTLE_ENDIAN} value - Two constants, BIG_ENDIAN=0, and LITTLE_ENDIAN=1
 * @note By default, AtomBox has a BigEndian byte order, but in some special circumstances may need to be changed
 */

/**************************************************************************************************************/


/**
 * This constructor is not available to the AtomBox scripting interpretor.
 * @class
 * @name Atom
 * @abstract
 * @classdesc  Atom objects are produced by the Atom/Box DDL module, and cannot be instantiated directly.
 * @summary Atom
 * @memberof module:ddl
 * @note This documentation is a just a stub and it is in progress
 */

/**
 * Fired when the value of this atom changes
 * @event module:ddl.Atom.prototype.changed
 */

/**
 * Fired when it packing has been finished
 * @event module:ddl.Atom.prototype.packed
 */

/**
 * Mutable property for getting/setting the value of this atom
 * @member {ByteArray} module:ddl.Atom.prototype.value
 * @throws Error Throws Error when trying to access a value of an atom that is not in OK_STATE state
 */

/**
 * Returns the Atom representation in bytes
 * @member {ByteArray} module:ddl.Atom.prototype.bytes
 */

/**
 * Returns true if the Atom is numeric, false otherwise.
 * @method  module:ddl.Atom.prototype.isNumeric
 * @returns {Boolean}
 */

/**
 * Returns true if the Atom is a redefine, false otherwise.
 * @method module:ddl.Atom.prototype.isRedefine
 * @returns {Boolean}
 */

/**
 * Returns true if the Atom has at least one redefine, false otherwise.
 * @method  module:ddl.Atom.prototype.hasRedefines
 * @returns {Boolean}
 */

/**
 * Returns the Atom state (0 = OK_STATE, 1 = WARNING_STATE, 2 = ERROR_STATE, 3 = DEFAULT_STATE)
 * @member {Number} module:ddl.Atom.prototype.state
 */

/**
 * Returns the Atom offset
 * @member {Number} module:ddl.Atom.prototype.offset
 */

/**
 * Pack
 * @method module:ddl.Atom.prototype.pack
 * @arg {ByteString|ByteArray} ba
 */

/**
 * Unpack
 * @method module:ddl.Atom.prototype.unpack
 * @returns {ByteString|ByteArray}
 */

/**
 * Returns true if the Atom is a key
 * @method {ByteArray} module:ddl.Box.prototype.isKey (DDL)
 * @returns {Boolean}
 */

/**
 * Returns true if the Atom is numeric signed
 * @method {ByteArray} module:ddl.Box.prototype.isSigned
 * @returns {Boolean}
 */

/**
 * Returns true if the Atom uses a binary representation
 * @method {ByteArray} module:ddl.Box.prototype.isBinary
 * @returns {Boolean}
 */

#ifndef BOXPROTOTYPE_H
#define BOXPROTOTYPE_H

#include <QtCore/QObject>
#include <QtCore/QDebug>

#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

#include "boxclass.h"
#include "binary.h"
#include "wrapbox.h"

class BoxPrototype : public QObject, public QScriptable
{
    Q_OBJECT

friend class BoxClass;

public:
    BoxPrototype(QObject *parent = 0);
    ~BoxPrototype();

public slots:
    void pack(ByteArray ba);
    ByteArray unpack();
    void clear() const;
    QScriptValue toString() const;
    bool isRedefine() const { return thisBox()->c()->isRedefine(); }
    bool isRecord() const { return thisBox()->c()->isRecord(); }
    bool isKey() const { return thisBox()->c()->isKey(); }
    bool hasRedefines() const { return thisBox()->c()->hasRedefines(); }
    void setByteOrder(unsigned short o) const;
    QString toJSON( bool need_redefines = false) const { return thisBox()->c()->toJSON( need_redefines ); }
    void destroy();

signals:
    void changed(QScriptValue);
    void packed(QScriptValue);
    void unpacked(QScriptValue);

private slots:
    void slot_changed(BoxComponent *c) { /*emit changed(thisObject());*/ }
    void slot_packed(BoxComponent *c) { /*emit packed(thisObject());*/ }
    void slot_unpacked(BoxComponent *c) { /*emit unpacked(thisObject());*/ }

private:
    WrapBox* thisBox() const;

    void appendBitArray(QBitArray &a1, QBitArray a2);
};
#endif //BOXPROTOTYPE_H

#ifndef WRAPATOM_H
#define WRAPATOM_H

#include "wrapboxcomponent.h"

class Atom;
class WrapAtom : public WrapBoxComponent
{
public:
    WrapAtom( BoxComponent *atom );
    virtual ~WrapAtom();

    virtual void exportJS( QScriptEngine *engine,
                           QScriptValue &ns);

    void pack( QIODevice *device );
    QByteArray unpack( );
};
#endif //WRAPATOM

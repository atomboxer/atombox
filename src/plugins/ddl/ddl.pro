include(../plugins.pri)

isEmpty(USE_SCRIPT_CLASSIC) {
    TARGET         = abscriptddl
} else {
    TARGET         = abscriptddl_compat
}

DESTDIR        = $$ATOMBOXER_SOURCE_TREE/plugins/script
!isEmpty(USE_SCRIPT_CLASSIC) {
    tandem {
        QT            -= core
    }
} else {
    QT            += core script
}



PRE_TARGETDEPS  = \
        $$ATOMBOXER_SOURCE_TREE/lib/libddl.a \
        $$ATOMBOXER_SOURCE_TREE/lib/libatombox.a

DEPENDPATH  += . 
INCLUDEPATH += . $$ATOMBOXER_SOURCE_TREE/src/main/ $$ATOMBOXER_SOURCE_TREE/src/plugins/core

HEADERS  = \
           atomclass.h \
           atomprototype.h \
           boxclass.h \
           boxprototype.h \
           ddlplugin.h \
           ddlplugin.h \
           wrapatom.h \
           wrapbox.h \
           wrapboxcomponent.h

!tandem {
           HEADERS += $$ATOMBOXER_SOURCE_TREE/src/main/scriptengineapp.h
}

SOURCES  = \
           atomclass.cpp \
           atomprototype.cpp \
           boxclass.cpp \
           boxprototype.cpp \
           ddlplugin.cpp \
           wrapatom.cpp \
           wrapbox.cpp \
           wrapboxcomponent.cpp

!tandem {
    SOURCES += $$ATOMBOXER_SOURCE_TREE/src/main/scriptengineapp.cpp
}

static {
PRE_TARGETDEPS  = \
             $$ATOMBOXER_SOURCE_TREE/lib/libatombox.a \
             $$ATOMBOXER_SOURCE_TREE/lib/libddl.a

LIBS       += -L$$ATOMBOXER_SOURCE_TREE/lib/ -lddl -latombox

} else {
    isEmpty(USE_SCRIPT_CLASSIC) {
         QT += script
         LIBS += -labscriptcore
    } else {
         LIBS += -labscriptcore_compat
    }
    CONFIG   += qt plugin
    LIBS     +=  -L$$ATOMBOXER_SOURCE_TREE/lib/ -lddl -latombox
}

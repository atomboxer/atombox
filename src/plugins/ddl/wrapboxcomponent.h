#ifndef WRAPBOXCOMPONENT_H
#define WRAPBOXCOMPONENT_H

#include <QtCore/QList>
#include <QtCore/QWeakPointer>
#include <QScriptEngine>
#include <QScriptValue>

#include "boxcomponent.h"

typedef QWeakPointer<QIODevice> IODevicePtr;

class WrapBoxComponent;
class WrapBoxComponent //: public QObject
{
    //Q_OBJECT

public:
    WrapBoxComponent( BoxComponent *b/*, QObject *parent = 0*/);
    virtual ~WrapBoxComponent();
    
    BoxComponent *c() const { return component_; }
    virtual QScriptValue& ns() { return ns_; }
    virtual void setNamespace(QScriptValue &ns) {ns_ = ns; }

public: // tree structure
    virtual void add( WrapBoxComponent *w) { Q_ASSERT(0); }
    virtual void addOccurence( WrapBoxComponent *o) { occurences_.push_back(o); o->setOccurenceParent(this);}
    virtual void setOccurenceParent(WrapBoxComponent *parent) { occurenceParent_ = parent;}
    virtual void addRedefine( WrapBoxComponent *r);
    virtual QList<WrapBoxComponent *> childs() const { return childs_; }

    virtual WrapBoxComponent* parent() const { return parent_; }
    virtual QList<WrapBoxComponent* > occurences() const { return occurences_; }
    virtual WrapBoxComponent* occurenceParent() const { return occurenceParent_;}

    virtual QList<WrapBoxComponent*> redefines() const { return redefines_; }
    QScriptValue scriptObject() const { return scriptObject_; }
    virtual void setParent(WrapBoxComponent *p) { parent_ = p; }
    void setScriptObject(QScriptValue &object) { scriptObject_ = object;}

//signals:
//    void packed();
//    void unpacked();
//    void changed();

public: // pure virtual
    virtual void exportJS( QScriptEngine *engine,
                           QScriptValue &ns) = 0;

    virtual void pack( QIODevice *device ) = 0;
    virtual QByteArray unpack( ) = 0;

//STATIC
public:
    static WrapBoxComponent* createWrapper(BoxComponent *box/*, QObject *parent*/);
    static void destroyWrapper(WrapBoxComponent *root);
    static void dumpABTree(BoxComponent *box, QString prefix="");
    static void dumpABTree(WrapBoxComponent *box, QString prefix="");

protected:
    BoxComponent *component_;
    QList<WrapBoxComponent *>   childs_;

private:
    QScriptValue ns_;
    QScriptValue class_instance_;

// for the tree structure
    WrapBoxComponent *parent_;
    WrapBoxComponent *occurenceParent_;
    QList< WrapBoxComponent* >  occurences_;
    QList< WrapBoxComponent* >  redefines_;


    QScriptValue scriptObject_;

};
#endif //WRAPBOXCOMPONENT_H

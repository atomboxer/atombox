#include <QtScript/QScriptEngine>
#include <QtCore/QObject>
#include <QtCore/QDebug>
#include <QtCore/QBuffer>

#include "atomprototype.h"
#include "atomclass.h"

#include "atomstring.h"
#include "utils.h"

#include "bytearrayclass.h"
#include "bytestringclass.h"

#include <assert.h>
#include <iostream>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


Q_DECLARE_METATYPE(AtomClass*)
Q_DECLARE_METATYPE(WrapAtomPointer*)
Q_DECLARE_METATYPE(ByteString*)
Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteArrayClass*)
Q_DECLARE_METATYPE(ByteStringClass*)
Q_DECLARE_METATYPE(IODevicePtr*)

AtomPrototype::AtomPrototype(QObject *parent, WrapBoxComponent *model)
: QObject(parent)
{
}

AtomPrototype::~AtomPrototype()
{}

WrapAtom*
AtomPrototype::thisAtom() const
{
    WrapAtomPointer *ptr = qscriptvalue_cast<WrapAtomPointer*>(thisObject().data());
    if (!ptr) {
        qWarning("programmatic error in BoxPrototype::thisBox");
        return 0;
    }
    return ptr->data();
}

void
AtomPrototype::clear() const
{
    thisAtom()->c()->clear();
}

// void
// AtomPrototype::display(ushort lvls, ushort cur_dump_lvl) const
// {
//     if (thisAtom()->c()->idx() == -1)
//         return; //this element was excluded because is not in bitmap

//     QString disp_value;
//     if (thisAtom()->c()->isArray())
//         disp_value = thisAtom()->c()->ascii().data();
//     else
//         disp_value = thisAtom()->c()->ascii();

//     bool can_print = true;

//     if (((Atom*)thisAtom()->c())->isBitmap())
//         can_print = false;
//     else
//         for (int i = 0; i< disp_value.length();i++) {
//             if ( !disp_value[i].isPrint() )
//                 can_print = false;
//         }

//     if (!can_print)
//         disp_value = "0x"+QString::fromLatin1(thisAtom()->c()->bytes().toHex()).toUpper();

//     QString idx(3);
//     if (((Atom*)thisAtom()->c())->isBitmap() || thisAtom()->c()->idx() == 0 )
//         idx = "   ";
//     else
//         idx = QString::number(thisAtom()->c()->idx());

//     QString out  = QString("%1%2 %3 : %4")
//         .arg(QString((cur_dump_lvl-1)*4,' ')) //padding
//         .arg(idx,3)                           //DE
//         .arg(thisAtom()->c()->displayString(),-24,' ')
//         .arg(disp_value);

//     if ( !((Atom*)thisAtom()->c())->constraints().isEmpty()) {
//         Constraint *c = 0;
//      c = ((Atom*)thisAtom()->c())->findConstraintByValue(((Atom*)thisAtom()->c())->value());
//         if (!c)
//             return;

//         out += QString(" - %1 ")
//             .arg(c->display());
//     }

//     std::cout<<out.toLatin1().data()<<std::endl;
// }

void
AtomPrototype::pack(ByteArray ba)
{
    size_t cost = 0;

    QBuffer buffer(&ba);
    buffer.open(QIODevice::ReadOnly);
    
    cost = ba.length();

    engine()->reportAdditionalMemoryCost(cost);

    thisAtom()->c()->clear();
    thisAtom()->pack((QIODevice*)(&buffer));
    buffer.close();
}

ByteArray
AtomPrototype::unpack()
{
    return thisAtom()->unpack();
}

QScriptValue
AtomPrototype::toString() const
{
    if (thisAtom())
        return QString::fromLatin1("object");

    return QScriptValue(QScriptValue::UndefinedValue);
}

// QScriptValue
// AtomPrototype::toString() const
// {
//     if (!thisAtom()) {
//         return QScriptValue(QScriptValue::UndefinedValue);
//     }

//     Atom *atom = (Atom*)thisAtom()->c();
//     assert(atom);
//     bool ok = false;
//     int val=0;

//     if (atom->isBinary() || atom->isStringNumeric()) {
//     val = atom->value().toInt(&ok);
//     if (!ok) {
//         return QScriptValue(0);
//     }
//     return QScriptValue(val);
//     }

//     return QScriptValue(atom->value().toString());
// }

// QScriptValue
// AtomPrototype::valueOf() const
// {
//     Atom *atom = thisAtom();
//     assert(atom);
//     bool ok = false;
//     int val=0;

//     if (atom->isString()) {
//      if (atom->isStringNumeric()) {
//          val = atom->value().toInt(&ok);
//          if (!ok) {
//              return QScriptValue(0);
//          }
//          return QScriptValue(val);
//      }
//      return atom->value().toString();
//     }
//     if (atom->isBinary()) {
//      val = atom->value().toInt(&ok);
//      if (!ok) {
//          return QScriptValue(0);
//      }
//      return QScriptValue(val);
//     }
//     return QScriptValue(val);
// }


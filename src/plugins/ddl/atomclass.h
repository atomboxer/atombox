#ifndef ATOMCLASS_H
#define ATOMCLASS_H

#include <QtCore/QObject>
#include <QtCore/QString>


#include <QScriptClass>
#include <QScriptContext>
#include <QtScript/QScriptEngine>
#include <QScriptString>
#include <QtCore/QSharedPointer>
#include <QtCore/QWeakPointer>
#include <QtCore/QPointer>

#include "wrapatom.h"

typedef QSharedPointer<WrapAtom> WrapAtomPointer;

class AtomClass : public QObject, public QScriptClass
{
public:
    AtomClass(QScriptEngine *engine, WrapAtom *model);
    ~AtomClass();

    QScriptValue constructor() const { return ctor_; }
    WrapAtom* model() const { return model_; }
    QString name() const { return QLatin1String("Atom"); }

    QScriptValue newInstance(const WrapAtomPointer &atom);

    QScriptValue ns() const { return ns_; }
    QScriptValue property(const QScriptValue &object,
                          const QScriptString &name, 
                          uint id);
    QScriptValue::PropertyFlags propertyFlags( const QScriptValue &object,
                           const QScriptString &name,
                           uint id );

    QScriptValue prototype() const { return proto_; }

    QueryFlags queryProperty(const QScriptValue &object,
                             const QScriptString &name,
                             QueryFlags flags, 
                             uint *id);

    void setNamespace(QScriptValue &ns) { ns_ = ns; }
    void setProperty(QScriptValue &object, 
                     const QScriptString &name,
                     uint id, 
                     const QScriptValue &value);

public:
    static QScriptValue construct(QScriptContext *ctx, QScriptEngine *eng);

private:
    QScriptString bytes_;
    QScriptString constraints_;
    QScriptString display_;
    QScriptString index_;
    QScriptString length_;
    QScriptString name_;
    QScriptString offset_;
    QScriptString state_;
    QScriptString value_;

    QScriptValue proto_;
    QScriptValue ctor_;
    QScriptValue ns_;

    WrapAtom *model_;
};
#endif //atomclass.h

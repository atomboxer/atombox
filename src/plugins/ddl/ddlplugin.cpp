#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QFileInfo>
#include <QtCore/QPair>
#include <QtCore/QVariant>
#include <QtCore/QStringList>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptValueIterator>

#include <boxclass.h>
#include <atomclass.h>

#ifndef AB_STATIC_PLUGINS
#include <license.h>
#endif
#include <scriptengineapp.h>

#include "boxcomponent.h" //lib/atombox
#include "ddlengine.h"    //lib/ddl

#include "ddlplugin.h"
#include "wrapbox.h"

#include "ddlengine.h"
#include "astparser.h"
#include "csvparser.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


Q_DECLARE_METATYPE(BoxClass*);
Q_DECLARE_METATYPE(AtomClass*);

#if defined(AB_USE_SCRIPT_CLASSIC)
Q_EXPORT_PLUGIN2( abscriptddl_compat, DDLPlugin)
#else
Q_EXPORT_PLUGIN2( abscriptddl, DDLPlugin)
#endif


QScriptValue _require(QScriptContext *context, QScriptEngine *engine);


//#define DEBUG_REQUIRE
//#define DEBUG_UNREQUIRE

QString
resolveDDLModule(QString &name, QStringList paths)
{
    QString ret;
    QFileInfo f;

    if (QFileInfo(name).isDir()) {
        fprintf(stderr,"[resolveDDLModule] directories are not supported in require(...)\n");
        return QString();
    }

    if (!QFileInfo(QString(":/atombox/")+name).isDir() &&
        (f = QFileInfo(QString(":/atombox/")+name)).exists() ||
        (f = QFileInfo(QString(":/atombox/")+name+".ddl")).exists()) {
        return f.absoluteFilePath();  
    }
  
    if ((f = QFileInfo(name)).exists() ||
        (f = QFileInfo(name+".ddl")).exists() ||
        (f = QFileInfo(name+"ddl")).exists() ||
        (f = QFileInfo("ddl"+name)).exists()) {
 
        if (f.absoluteFilePath().isEmpty())
            ret = name;
        else
            ret = f.absoluteFilePath();
    } else { // we need to search it in .paths
        for(int i=0;i<paths.size();i++) {
            QFileInfoList li= QDir(paths[i]).entryInfoList();
            QRegExp rx ("(ddl){0,1}"+name+"[\\.]{0,1}(ddl){0,1}");
            for (int j=0;j<li.size();j++) {
                if (rx.exactMatch(li[j].baseName()+"."+li[j].completeSuffix())) {
                    ret = li[j].absoluteFilePath();
                    break;
                }
            }
        }
    }

#ifdef DEBUG_REQUIRE
    qDebug()<<"[resolveDDLModule] name="<<name<<"resolved to:"<<ret;
#endif
    return ret;

}

QScriptValue
require( ScriptEngineApp *scApp, QString name, QString type )
{
    QScriptValue g = scApp->currentContext()->activationObject();
    bool flg_js  = false;
    bool flg_ddl = false;
    bool flg_csv = false;

#ifdef DEBUG_REQUIRE
    qDebug()<<"[DDL require!]("<<name<<","<<type<<")";
#endif

    QString  moduleName;
    moduleName = scApp->resolveModule(name);
    if (!moduleName.size() || type == "ddl") {
        flg_ddl = true;
        moduleName= resolveDDLModule(name, scApp->paths());
    }
    else if (moduleName.endsWith(".ddl")) {
        flg_ddl = true;
        moduleName= resolveDDLModule(name, scApp->paths());
    } else if (type == "mdbcsv") {
        flg_csv = true;
        moduleName = resolveDDLModule(name, scApp->paths());;
    } else {
        flg_js = true;
    }

    if (!moduleName.size()) {
        return scApp->currentContext()->throwError("(require) Unable to locate module:"+QString(name));
    }

    //Search in cache first
    if ( scApp->cache().contains(moduleName) ) {
#ifdef DEBUG_REQUIRE
        qDebug()<<"[DDL require](returned from cache)";
#endif
        return scApp->cache().find(moduleName).value();
    }

    QScriptValue sv_require = scApp->newFunction(_require);

    //  Build the require function
    if ( !g.property("require").isFunction()) {
        g.setProperty("require", sv_require);

        QScriptValue arr = scApp->newArray();
        for (int i=0;i<scApp->paths().size();i++) {
            arr.setProperty(QString::number(i),scApp->paths()[i]);
        }
        sv_require.setProperty("paths",arr);
        sv_require.setProperty("main", 
                    QFileInfo(scApp->mainFile()).absoluteFilePath());
    }
    
    // Add to cache
    QScriptValue sv_exports = scApp->newObject();
    scApp->addToCache(moduleName, sv_exports);

    //create/use the module variable
    QScriptValue sv_module;

    if (name == scApp->mainFile()) {
        sv_module = scApp->mainModule();
    } else {
        sv_module = scApp->newObject();
    }

    sv_module.setProperty("id", moduleName);

    QFile scriptFile;

    scApp->addToPaths(QFileInfo(moduleName).absolutePath());

    scriptFile.setFileName(moduleName);
    if (!scriptFile.open(QIODevice::ReadOnly|QIODevice::Text)) {
        qDebug()<<moduleName<<" file cannot be opened. error:"<<scriptFile.errorString();
        return QScriptValue();
    }

    QTextStream stream(&scriptFile);
    QString code(stream.readAll());
    code += "\n";
    scriptFile.close();

    if ( flg_js ) {
        QString closure = "(function(require,exports,module){";
        closure += code;
        closure += ";})";

        QScriptValue req = scApp->evaluate( closure, moduleName, 1);
        QScriptValueList args;
        
        args << sv_require;
        args << sv_exports;
        args << sv_module;
        QScriptValue ret = req.call(sv_exports, args);
    } else if (flg_ddl) { //ddl
        QPair<QList<BoxComponent *>, QList< QPair< QString, QVariant> > > model;
        model = DDLEngine::instance()->evaluate_ddl( code, 
                                                     moduleName );
        if (DDLEngine::instance()->astparser()->errors().length() != 0) {
            QList<DDLError> err_list = DDLEngine::instance()->astparser()->errors();
            QStringList err;
            foreach (DDLError e, err_list) {
                err << e.url().toString()<<e.toString();
            }
            qFatal(err.join("\n").toLatin1().data());
            return sv_exports;
        }

        QList<BoxComponent*> definitions                     = model.first;
        QList< QPair < QString, QVariant> >      constants   = model.second;

        QPair< QString, QVariant> v;

        foreach(v, constants) {
            QScriptValue sv = scApp->newVariant(v.second);
            sv_exports.setProperty(v.first, sv);
        }

        foreach(BoxComponent *c, definitions) {
            WrapBoxComponent *w = WrapBoxComponent::createWrapper(c);
            w->exportJS(scApp, sv_exports);
            w->c()->clear();
        }

    } else if (flg_csv) { //mdbcsv
        QList<BoxComponent*> definitions;
        definitions = DDLEngine::instance()->evaluate_mdb( code, 
                                                           moduleName );
 
        foreach(BoxComponent *c, definitions) {
            //skip the first
            WrapBoxComponent *w = WrapBoxComponent::createWrapper(c);
            w->exportJS(scApp, sv_exports);
            w->c()->clear();
        }
    }

    if (sv_module.property("exports").isValid()) {
        scApp->addToCache(moduleName, sv_module.property("exports"));
        return sv_module.property("exports");
    } else {
        return sv_exports;
    }
}

void destroyAtomBoxTree(AtomClass *root)
{
    delete root;
}

void destroyAtomBoxTree(BoxClass *root)
{
     foreach(WrapBoxComponent *c, root->model()->childs()) {
         if ( c->redefines().length()>0 ) {
             foreach(WrapBoxComponent *r, c->redefines()) {
                 if (r->c()->isArray()) {
                      if (r->c()->isAtom()) {
                          int num = r->c()->maxOccurencesNumber();
                          for (int i=0; i<num;i++) {
                              QScriptValue ns = root->model()->ns().property(c->c()->name());
                              AtomClass *atom_class = qscriptvalue_cast<AtomClass*>(
                                  ns.property(r->c()->name()).property(i).data());
                              if ( atom_class ) {
                                  destroyAtomBoxTree(atom_class);
                              } else {
                                  qWarning("fatal error. Cannot find array atom:");
                              }
                          }
                      } else { /*box array*/
                          int num = r->c()->maxOccurencesNumber();
                          for (int i=0; i<num;i++) {
                              QScriptValue ns = root->model()->ns().property(c->c()->name());
                              BoxClass *box_class = qscriptvalue_cast<BoxClass*>(
                                  ns.property(r->c()->name()).property(i).data());
                              if ( box_class ) {
                                  QScriptValue val =box_class->newInstance(WrapBoxPointer((WrapBox*)(r->occurences().at(i))));
                                  delete box_class;
                              } else {
                                  qWarning("fatal error. Cannot find array box:");
                              }
                          }
                      }
                 } else {
                     QScriptValue ns = root->model()->ns().property(c->c()->name());
                     if (r->c()->isAtom()) {
                         AtomClass *a = qscriptvalue_cast<AtomClass*>(ns.property(r->c()->name()).data());
                         if ( !a) {
                             qFatal(("Unable to obtain atom"+c->c()->name()+" dot "+r->c()->name()).toLatin1().data());
                         }
                         destroyAtomBoxTree(a);

                     } else { /*box*/
                         BoxClass *b = qscriptvalue_cast<BoxClass*>(ns.property(r->c()->name()).data());
                         if ( !b) {
                             qFatal(("Unable to obtain box"+c->c()->name()+" dot "+r->c()->name()).toLatin1().data());
                         }
                         destroyAtomBoxTree(b);
                     }
                 }
             }
         }
        //done processing redefines
        if (c->c()->isAtom()) {
            if (c->c()->isArray()) {
                int num = c->c()->maxOccurencesNumber();
                for (int i=0; i<num;i++) {
                    AtomClass *atom_class = qscriptvalue_cast<AtomClass*>(
                        root->model()->ns().property(c->c()->name()).property(i).data());
                    if ( atom_class ) {
                        destroyAtomBoxTree(atom_class);
                    } else {
                        qWarning(("fatal error. Cannot find array atom:"+c->c()->name()+
                                  " from:"+root->model()->c()->name()).toLatin1().data());
                    }
                }
            } else {
                AtomClass *atom_class =  qscriptvalue_cast<AtomClass*>(
                    root->model()->ns().property(c->c()->name()).data());

                if ( atom_class ) {
                    destroyAtomBoxTree(atom_class);
                } else {
                    qWarning(("fatal error. Cannot find atom:"+c->c()->name()+
                              " from:"+root->model()->c()->name()).toLatin1().data());
                }
            }
        }  else { /*isBox()*/
            if (c->c()->isArray()) {
                int num = c->c()->maxOccurencesNumber();
                for (int i=0; i<num;i++) {
                    BoxClass *box_class = qscriptvalue_cast<BoxClass*>(
                        root->model()->ns().property(c->c()->name()).property(i).data());
                    if ( box_class ) {
                        destroyAtomBoxTree(box_class);
                    } else {
                        qWarning(("fatal error. Cannot find array box:"+c->c()->name()+
                                  " from:"+root->model()->c()->name()).toLatin1().data());
                    }
                }
            } else {
                BoxClass *box_class =  qscriptvalue_cast<BoxClass*>(
                    root->model()->ns().property(c->c()->name()).data());
                if ( box_class ) {
                    destroyAtomBoxTree(box_class);
                } else {
                    qWarning(("fatal error. Cannot find box:"+c->c()->name()+
                              " from:"+root->model()->c()->name()).toLatin1().data());
                }
            }
        }
    } //for

    delete root;
}

void destroyAtomBoxClassTree (QScriptValue exports, QStringList &keep)
{
   QScriptValueIterator it(exports);
   while(it.hasNext()) {
       it.next();
       if (it.value().isObject() && !keep.contains(it.name())) {
           BoxClass   *root = qscriptvalue_cast<BoxClass*>(it.value().data());
           if (!root) {
               continue;
           }

           destroyAtomBoxTree(root);
           it.setValue(QScriptValue::UndefinedValue);

       }
   }
   exports = QScriptValue(QScriptValue::UndefinedValue);
}
    
QScriptValue
unrequire( ScriptEngineApp *scApp, QString name, QStringList keep = QStringList())
{
#ifdef DEBUG_UNREQUIRE
    qDebug()<<"[DDL unrequire!]("<<name<<")";
 #endif

    QString  moduleName;
    moduleName = resolveDDLModule(name, scApp->paths());
    if (!moduleName.size()) {
        return QScriptValue();
    }

    //Search in cache first
    if ( !scApp->cache().contains(moduleName) ) {
        qWarning("moduleName cound not be found in cache");
        return QScriptValue();
    }

#ifdef DEBUG_UNREQUIRE
    qDebug()<<"[DDL unrequire (found)";
#endif

    QScriptValue exports = scApp->cache().find(moduleName).value();
    if (!exports.isObject()) {
        qFatal("Programmatic error in 'unrequire', expecting Object");
        return QScriptValue();
    }

    QScriptValueIterator it(exports);
    while(it.hasNext()) {
        it.next();

        DDLEngine::instance()->astparser()->model()->removeGroupDefinition(it.name());
        DDLEngine::instance()->csvparser()->model()->removeGroupDefinition(it.name());

    }
    destroyAtomBoxClassTree(exports, keep);
    scApp->removeFromCache(moduleName);

    return QScriptValue();

}

QScriptValue
_unrequire(QScriptContext *context, QScriptEngine *engine)
{
     ScriptEngineApp *ptr = static_cast<ScriptEngineApp*>(engine);
     Q_ASSERT(ptr);
     if (context->argumentCount() == 1) {
         return unrequire( ptr,
                           context->argument(0).toString() );
     } else if (context->argumentCount() != 2) {
         context->throwError("unrequire function accepts maximum two arguments");
         return QScriptValue();
     }

     QStringList keep;

     if (context->argument(1).isString()) {
        keep << context->argument(1).toString();
     } else if (context->argument(1).isArray()){
         QScriptValueIterator it(context->argument(1));
         while(it.hasNext()) {
             it.next();
             keep <<it.value().toString();
         }
     } else {
           context->throwError("Invalid parameter to unrequire");
           return QScriptValue();
     }

     return unrequire(ptr, context->argument(0).toString(), keep);
}

#ifdef ATOMBOXER_BUILD_DDL
QScriptValue
_require(QScriptContext *context, QScriptEngine *engine)
{
     ScriptEngineApp *ptr = static_cast<ScriptEngineApp*>(engine);
     Q_ASSERT(ptr);

     if (context->argumentCount() == 1) {
         return require(ptr, context->argument(0).toString(),"");
     } else if (context->argumentCount() == 2) {
         return require( ptr,
                         context->argument(0).toString(),
                         context->argument(1).toString());
     }

     return QScriptValue();
}
#endif 

DDLPlugin::DDLPlugin( QObject *parent )
    : QScriptExtensionPlugin( parent )
{
}

DDLPlugin::~DDLPlugin()
{
}
        
void
DDLPlugin::initialize( const QString &key, QScriptEngine *engine )
{
#if defined(AB_USE_SCRIPT_CLASSIC)
    if ( key == QString("ddl_compat") ) {
#else
    if ( key == QString("ddl") ) {
#endif
#ifdef AB_STATIC_PLUGINS
        //Q_INIT_RESOURCE(ddl);
#endif
        QScriptValue sv_require = engine->newFunction(_require);
        QScriptValue sv_unrequire = engine->newFunction(_unrequire);
        
        //  Build the require function
        engine->globalObject().setProperty("require", sv_require);
        //  Build the unrequire function
        engine->globalObject().setProperty("unrequire", sv_unrequire);

    }
}
       
QStringList
DDLPlugin::keys() const
{
    QStringList keys;
#if defined(AB_USE_SCRIPT_CLASSIC)
    keys << QString("ddl_compat");
#else
    keys << QString("ddl");
#endif
    return keys;
}

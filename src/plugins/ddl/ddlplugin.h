#ifndef DDL_PLUGIN
#define DDL_PLUGIN

#include <qscriptextensionplugin.h>

class DDLPlugin : public QScriptExtensionPlugin
{
public:
    DDLPlugin( QObject *parent = 0);
    ~DDLPlugin();

    virtual void initialize(const QString &key, QScriptEngine *engine);
    virtual QStringList keys() const;

private:
    // nothing
};

#endif //DDL_PLUGIN_H

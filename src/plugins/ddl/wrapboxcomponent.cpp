
#include "wrapatom.h"
#include "wrapbox.h"
#include "wrapboxcomponent.h"
#include "boxcomponent.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define WRAPBOXCOMPONENT_DEBUG

void
WrapBoxComponent::destroyWrapper(WrapBoxComponent *root)
{
    if (root->c()->isArray()) {
        foreach(WrapBoxComponent *occ, root->occurences()) {
            foreach (WrapBoxComponent *c, occ->childs()) {
                destroyWrapper(c);
            }
        }
    }

    foreach (WrapBoxComponent *c, root->childs()) {
        destroyWrapper(c);
    }

    delete root;
}


WrapBoxComponent*
WrapBoxComponent::createWrapper(BoxComponent *c)
{
    WrapBoxComponent *wc;

    if (c->isAtom()) {
        if (c->isArray()) {
            wc = new WrapAtom(c);
            for (unsigned i=0;i<c->maxOccurencesNumber(); i++) {
                WrapAtom *occ = new WrapAtom(c->occurences().at(i));
                if (occ->c()->hasRedefines()) {
                    foreach (BoxComponent *r, occ->c()->redefines()) {
                        occ->addRedefine((WrapBoxComponent*)(WrapBoxComponent::createWrapper(r)));
                    }
                }
                wc->addOccurence(occ);
            }
        } else {
            wc = new WrapAtom(c);
            if (c->hasRedefines()) {
                foreach (BoxComponent *r, c->redefines()) {
                    wc->addRedefine((WrapBoxComponent*)(WrapBoxComponent::createWrapper(r)));
                }
            }
        }
    } else { /*BOX*/
        if (c->isArray()) {
            wc = new WrapBox(c);
            for (unsigned i=0;i<c->maxOccurencesNumber(); i++) {
                WrapBox *occ = new WrapBox(c->occurences().at(i));
                foreach (BoxComponent *ch, c->occurences().at(i)->childs()) {
                    occ->add(WrapBoxComponent::createWrapper(ch));
                }
                wc->addOccurence(occ);
            }
        } else {
            wc = new WrapBox(c);

            foreach (BoxComponent *ch, c->childs()) {
                wc->add(WrapBoxComponent::createWrapper(ch));
            }
        }
    
        if (c->hasRedefines()) {
            foreach (BoxComponent *r, c->redefines()) {
                wc->addRedefine((WrapBoxComponent*)(WrapBoxComponent::createWrapper(r)));
            } 
        }
    }

    return wc;
}


void
WrapBoxComponent::dumpABTree(WrapBoxComponent *c, QString indent)
{
    if (c->c()->isRedefine()) {
        qDebug()<<"R"+indent+c->c()->name()<<":::"<<c;
    } else {
        qDebug()<<indent+c->c()->name()<<":::"<<c<<c->c();
    }

    if (c->c()->isRedefine()) {
        foreach (WrapBoxComponent *ch, c->childs()) {
            WrapBoxComponent::dumpABTree(ch, "R"+indent+"    ");
        }
        foreach (WrapBoxComponent *ch, c->redefines()) {
            WrapBoxComponent::dumpABTree(ch, "R"+indent+"    ");
        }
    } else if (c->c()->isAtom()) {
        if (c->c()->hasRedefines()) {
            foreach (WrapBoxComponent *r, c->redefines()) {
                WrapBoxComponent::dumpABTree(r, indent+"    ");
            }
        }
    } else {
        foreach (WrapBoxComponent *ch, c->childs()) {
            WrapBoxComponent::dumpABTree(ch, indent +"    ");
        }
    
        if (c->c()->hasRedefines()) {
            foreach (WrapBoxComponent *r, c->redefines()) {
                WrapBoxComponent::dumpABTree(r, indent+"    ");
            } 
        }
    }
}


WrapBoxComponent::WrapBoxComponent( BoxComponent *w ) 
    : /*QObject(parent),*/
      component_(w),
      parent_(NULL)
{
    //QObject::connect(w, SIGNAL(packed(w)),   this, SIGNAL(packed()));
    //QObject::connect(w, SIGNAL(unpacked(w)), this, SIGNAL(unpacked()));
    //QObject::connect(w, SIGNAL(changed(w)),  this, SIGNAL(changed()));
}

WrapBoxComponent::~WrapBoxComponent()
{
#ifdef WRAPBOXCOMPONENT_DEBUG
    qDebug()<<"WrapBoxComponent::~WrapBoxComponent() "<<component_->name()<<component_;
#endif

    if (c()->isOccurence()) {
        if (c()->occurenceParent()) {
            if ((BoxComponent*)(c()->occurenceParent()->occurences().last())
                == ((BoxComponent*)(component_))) {
                delete occurenceParent();
            }

        } else {
            qDebug()<<"Who wiped out my parent?"<<c()->name();
        }
    }

    component_->deleteLater();
}

void
WrapBoxComponent::addRedefine( WrapBoxComponent* r)
{ 
    Q_ASSERT(r);
    r->setParent(this);
    redefines_.push_back(r);
}

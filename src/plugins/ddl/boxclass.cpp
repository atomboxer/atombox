#include "atomclass.h"
#include "boxclass.h"
#include "boxprototype.h"
#include "wrapbox.h"

#include "bytearrayclass.h"

#include <QtCore/QObject>
#include <QtCore/QDebug>
#include <QtCore/QStringList>
#include <QtCore/QPointer>

#include <QtScript/QScriptClassPropertyIterator>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#include <iostream>
#include <assert.h>
#include <stdlib.h>

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define BOXCLASS_DEBUG

Q_DECLARE_METATYPE(BoxClass*)
Q_DECLARE_METATYPE(AtomClass*)
Q_DECLARE_METATYPE(WrapBoxPointer*)
Q_DECLARE_METATYPE(WrapBoxPointer)

static QScriptValue 
ReturnTrue(QScriptContext *ctx, QScriptEngine *eng)
{
    return true;
}

static QScriptValue 
ReturnFalse(QScriptContext *ctx, QScriptEngine *eng)
{
    return false;
}

//class BoxClassPropertyIterator : public QScriptClassPropertyIterator
//{
//public:
//    BoxClassPropertyIterator(const QScriptValue &object);
//    ~BoxClassPropertyIterator();
//
//    bool hasNext() const;
//    void next();
//
//    bool hasPrevious() const;
//    void previous();
//
//    void toFront();
//    void toBack();
//
//    QScriptString name() const;
//    uint id() const;
//
//private:
//    int m_index;
//    int m_last;
//};



BoxClass::BoxClass(QScriptEngine *engine, WrapBox *model)
    : QObject(engine), QScriptClass(engine)
{

    qScriptRegisterMetaType<WrapBoxPointer>(engine, toScriptValue,
                                            fromScriptValue);

    model_      = model;
    properties_ = QHash<QString, QScriptValue>();

    bytes_   = engine->toStringHandle(QLatin1String("bytes"));
    display_ = engine->toStringHandle(QLatin1String("display"));
    index_   = engine->toStringHandle(QLatin1String("index"));    
    length_  = engine->toStringHandle(QLatin1String("length"));
    name_    = engine->toStringHandle(QLatin1String("name"));
    offset_  = engine->toStringHandle(QLatin1String("offset"));
    state_   = engine->toStringHandle(QLatin1String("state"));

    QScriptValue global = engine->globalObject();
    box_proto_ = new BoxPrototype(this);
    proto_ = engine->newQObject(box_proto_,
                                QScriptEngine::QtOwnership,
                                QScriptEngine::SkipMethodsInEnumeration
                                |QScriptEngine::ExcludeSuperClassMethods
                                |QScriptEngine::ExcludeSuperClassProperties);

    proto_.setPrototype(global.property("Object").property("prototype"));

    ctor_ = engine->newFunction(construct, proto_);
    ctor_.setData(engine->toScriptValue(this));
}

BoxClass::~BoxClass()
{
#ifdef BOXCLASS_DEBUG
    qDebug()<<"BoxClass::~BoxClass() "<< model_->c()->name()<<","<<model_;
#endif

    if ( model_ ) {
        delete model_;
        model_ = 0;
    }
}

//QScriptClassPropertyIterator *
//BoxClass::newIterator(const QScriptValue &object)
//{
//    return new BoxClassPropertyIterator(object);
//}

QScriptClass::QueryFlags
BoxClass::queryProperty(const QScriptValue &object,
                        const QScriptString &name,
                        QueryFlags flags,
                        uint *id)
{
    if ( name == length_  ||
         name == bytes_   ||
         name == state_   ||
         name == name_    ||
         name == offset_  ||
         name == index_   ||
         name == display_) {

        return flags;
    }

    // foreach(BoxComponent *c, model()->c()->childs()) {
    //     if ( c->name()==name.toString() ) {
    //         return flags;
    //     }
    // }

    flags &= ~HandlesReadAccess;
    flags &= ~HandlesWriteAccess;
    return flags;
}


QScriptValue
BoxClass::property(const QScriptValue &object,
                   const QScriptString &name, uint id)
{
    WrapBoxPointer *ptr = qscriptvalue_cast<WrapBoxPointer*>(object.data());
    if (!ptr) {
        qFatal("BoxClass::property, cannot convert");
    }

    WrapBox* wrapbox = ptr->data();
    if (!wrapbox) {
       return QScriptValue();
    }
    
    if (name == bytes_) {
        return ByteArrayClass::toScriptValue(engine(), wrapbox->c()->bytes());
    } else if (name == display_ ) {
        return wrapbox->c()->displayString();
    } else if (name == length_) {
        return (int)(wrapbox->c()->length());
    } else if (name == name_ ) {
        return wrapbox->c()->name();
    } else if (name == offset_) {
        return wrapbox->c()->offset();
    } else if (name == state_ ) {
        return wrapbox->c()->state();
    } else if (name == index_ ) {
        return wrapbox->c()->idx();
    } else {
        qFatal("Fatal error in BoxClass::property");

        Q_ASSERT(name.isValid());
        Q_ASSERT(properties_.contains(name.toString()));

        properties_.value(name.toString()).toString();
        return properties_.value(name.toString());
    }

    return QScriptValue();
}

void
BoxClass::setProperty(QScriptValue &object,
                      const QScriptString &name,
                      uint id,
                      const QScriptValue &value)
{
    WrapBoxPointer *ptr = qscriptvalue_cast<WrapBoxPointer*>(object.data());
    if (!ptr) {
        qFatal("BoxClass::property, cannot convert");
    }

    WrapBox* wrapbox = ptr->data();
    if (!wrapbox) {
       return;
    }

    properties_.insert(name.toString(), value);
}

QScriptValue::PropertyFlags
BoxClass::propertyFlags(const QScriptValue &object,
                        const QScriptString &name,
                        uint id)
{
    if ( name == bytes_  ||
         name == display_||
         name == index_  || 
         name == length_ ||
         name == name_   ||
         name == offset_ ||
         name == state_  )
        return QScriptValue::Undeletable
            | QScriptValue::SkipInEnumeration;

    return QScriptValue::Undeletable | QScriptValue::ReadOnly;
}

QScriptValue
BoxClass::newInstance(const WrapBoxPointer &box)
{
    QScriptValue data = engine()->newVariant(QVariant::fromValue(box));
    QScriptValue ret  = engine()->newObject(this, data);

    // QObject::connect(box.data()->c(), SIGNAL(packed(BoxComponent*)),
    //                  box_proto_, SLOT(slot_packed(BoxComponent*)));

    // QObject::connect(box.data()->c(), SIGNAL(unpacked(BoxComponent*)),
    //                  box_proto_, SLOT(slot_unpacked(BoxComponent*)));

    // QObject::connect(box.data()->c(), SIGNAL(changed(BoxComponent*)),
    //                  box_proto_, SLOT(slot_changed(BoxComponent*)));


    foreach(WrapBoxComponent *c, box->childs()) {
        if (c->c()->isAtom()) {
            if (c->c()->isArray()) {
                QScriptValue arr = engine()->newArray();
                for (int i=0; i<c->c()->maxOccurencesNumber();i++) {
                    AtomClass *atom_class = qscriptvalue_cast<AtomClass*>(
                        model()->ns().property(c->c()->name()).property(i).data());
                    if ( atom_class ) {
                        QScriptValue val =atom_class->newInstance(WrapAtomPointer((WrapAtom*)(c->occurences().at(i))));
                        arr.setProperty(i,val);
                    } else {
                        qWarning(("fatal error. Cannot find array atom:"+c->c()->name()+
                                  " from:"+model()->c()->name()).toLatin1().data());
                    }
                }
                ret.setProperty(c->c()->name(),arr);

            } else {
                AtomClass *atom_class =  qscriptvalue_cast<AtomClass*>(
                    model()->ns().property(c->c()->name()).data());

                if ( atom_class ) {
                    ret.setProperty(c->c()->name(),atom_class->newInstance(WrapAtomPointer((WrapAtom*)c)));
                } else {
                    qWarning(("fatal error. Cannot find atom:"+c->c()->name()+
                              " from:"+model()->c()->name()).toLatin1().data());
                }
            }
        }  else { /*isBox()*/
            if (c->c()->isArray()) {
                QScriptValue arr = engine()->newArray();
                for (int i=0; i<c->c()->maxOccurencesNumber();i++) {
                    BoxClass *box_class = qscriptvalue_cast<BoxClass*>(
                        model()->ns().property(c->c()->name()).property(i).data());
                    if ( box_class ) {
                        QScriptValue val = box_class->newInstance(WrapBoxPointer((WrapBox*)(c->occurences().at(i))));
                        arr.setProperty(i,val);
                    } else {
                        qWarning(("fatal error. Cannot find array box:"+c->c()->name()+
                                  " from:"+model()->c()->name()).toLatin1().data());
                    }
                }
                ret.setProperty(c->c()->name(),arr);

            } else {
                BoxClass *box_class =  qscriptvalue_cast<BoxClass*>(
                    model()->ns().property(c->c()->name()).data());
                if ( box_class ) {
                    ret.setProperty(c->c()->name(),box_class->newInstance(WrapBoxPointer((WrapBox*)c)));
                } else {
                    qWarning(("fatal error. Cannot find box:"+c->c()->name()+
                              " from:"+model()->c()->name()).toLatin1().data());
                }
            }
        }

        if ( c->c()->hasRedefines() ) {
            foreach(WrapBoxComponent *r, c->redefines()) {
                if (r->c()->isArray()) {
                     if (r->c()->isAtom()) {
                         QScriptValue arr = engine()->newArray();
                         for (int i=0; i<r->c()->maxOccurencesNumber();i++) {
                             QScriptValue ns = model()->ns().property(c->c()->name());
                             AtomClass *atom_class = qscriptvalue_cast<AtomClass*>(
                                 ns.property(r->c()->name()).property(i).data());
                             if ( atom_class ) {
                                 QScriptValue val =atom_class->newInstance(WrapAtomPointer((WrapAtom*)(r->occurences().at(i))));
                                 arr.setProperty(i,val);
                             } else {
                                 qWarning("fatal error. Cannot find array atom:");
                             }
                         }
                         ret.setProperty(r->c()->name(),arr);
                     } else { /*box array*/
                         QScriptValue arr = engine()->newArray();
                         for (int i=0; i<r->c()->maxOccurencesNumber();i++) {
                             QScriptValue ns = model()->ns().property(c->c()->name());
                             BoxClass *box_class = qscriptvalue_cast<BoxClass*>(
                                 ns.property(r->c()->name()).property(i).data());
                             if ( box_class ) {
                                 QScriptValue val =box_class->newInstance(WrapBoxPointer((WrapBox*)(r->occurences().at(i))));
                                 arr.setProperty(i,val);
                             } else {
                                 qWarning("fatal error. Cannot find array box:");
                             }
                         }
                         ret.setProperty(r->c()->name(),arr);
                     }
                } else {
                    QScriptValue ns = model()->ns().property(c->c()->name());
                    if (r->c()->isAtom()) {
                        AtomClass *a = qscriptvalue_cast<AtomClass*>(ns.property(r->c()->name()).data());
                        if ( !a) {
                            qFatal(("Unable to obtain atom"+c->c()->name()+" dot "+r->c()->name()).toLatin1().data());
                        }
                        ret.setProperty(r->c()->name(),a->newInstance(WrapAtomPointer((WrapAtom*)r)));
                    } else { /*box*/
                        BoxClass *b = qscriptvalue_cast<BoxClass*>(ns.property(r->c()->name()).data());
                        if ( !b) {
                            qFatal(("Unable to obtain box"+c->c()->name()+" dot "+r->c()->name()).toLatin1().data());
                        }
                        ret.setProperty(r->c()->name(),b->newInstance(WrapBoxPointer((WrapBox*)r)));

                    }
                }
            }
        }

    } //for

    return ret;
}

QScriptValue
BoxClass::construct(QScriptContext *ctx, QScriptEngine *eng)
{

    BoxClass *cls = qscriptvalue_cast<BoxClass*>(ctx->callee().data());

    if (!cls) {
        return QScriptValue();
    }

    BoxComponent *bc = cls->model()->c()->clone();
    eng->reportAdditionalMemoryCost(cls->model()->c()->length());

    bc->reindex();
    bc->clear();

    WrapBox* wc ((WrapBox*)(WrapBoxComponent::createWrapper(bc)));

    WrapBoxPointer ptr(wc);
    //WrapBoxComponent::dumpABTree(wc.data());

    QScriptValue ret = cls->newInstance(ptr);

    return ret;
}

// BoxClassPropertyIterator::BoxClassPropertyIterator(const QScriptValue &object)
//     : QScriptClassPropertyIterator(object)
// {
//     toFront();
// }

// BoxClassPropertyIterator::~BoxClassPropertyIterator()
// {
// }


// bool
// BoxClassPropertyIterator::hasNext() const
// {
//     WrapBoxPointer *b = qscriptvalue_cast<WrapBoxPointer*>(object().data());
//     if (!b) {
//         return false;
//     }

//     return m_index < b->propertiesList().count();
// }

// void
// BoxClassPropertyIterator::next()
// {
//     m_last = m_index;
//     ++m_index;
// }

// bool
// BoxClassPropertyIterator::hasPrevious() const
// {
//     return (m_index > 0);
// }

// void
// BoxClassPropertyIterator::previous()
// {
//     --m_index;
//     m_last = m_index;
// }

// void
// BoxClassPropertyIterator::toFront()
// {
//     m_index = 0;
//     m_last = -1;
// }

// void
// BoxClassPropertyIterator::toBack()
// {
//     WrapBoxPointer *b = qscriptvalue_cast<WrapBoxPointer*>(object().data());
//     assert(b);
//     m_index = b->propertiesList().count();
//     m_last = -1;
// }

// QScriptString
// BoxClassPropertyIterator::name() const
// {

//     WrapBoxPointer *b = qscriptvalue_cast<WrapBoxPointer*>(object().data());
//     assert(b);
//     return object().engine()->toStringHandle( b->propertiesList().at(m_last));
// }

// uint
// BoxClassPropertyIterator::id() const
// {
//     return m_last;
// }


QScriptValue 
BoxClass::toScriptValue(QScriptEngine *eng, const WrapBoxPointer &box)
{
    QScriptValue  ctor = eng->globalObject().property("Box");
    BoxClass     *cls  = qscriptvalue_cast<BoxClass*>(ctor.data());
    if (!cls)
        return eng->newVariant(QVariant::fromValue(box));

    return cls->newInstance(box);
}

void
BoxClass::fromScriptValue(const QScriptValue &obj, WrapBoxPointer &box)
{
    box = qvariant_cast<WrapBoxPointer>(obj.data().toVariant());
}

#include "atom.h"
#include "atomisobitmap.h"
#include "box.h"

#include "wrapbox.h"
#include "wrapatom.h"

#include <QtCore/QBitArray>
#include <QtCore/QBuffer>
#include <QtCore/QDebug>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptValueIterator>

#include "boxclass.h"
#include "boxcomponent.h"

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define WRAPBOX_DEBUG

static void
appendBitArray(QBitArray &a1, QBitArray a2)
{
    if (a1.count() == 0) {
        a1 = a2;
        return;
    }

    int idx_a1 = a1.count();
    a1.resize(a1.count()+a2.count());
    int idx_a2 = 0;

    while (idx_a2<a2.count()) {
        a1.setBit(idx_a1,a2.testBit(idx_a2));
        idx_a1++;
        idx_a2++;
    }
}

WrapBox::WrapBox( BoxComponent *w/*, QObject *parent*/ ) :
    WrapBoxComponent (w/*, parent*/)
{
    //qDebug()<<"#b"<<w->name();
}

WrapBox::~WrapBox()
{
#ifdef WRAPBOX_DEBUG
    qDebug()<<"WrapBox::~WrapBox()";
#endif
}

void
WrapBox::add(WrapBoxComponent *c)
{
    childs_.push_back(c);
    c->setParent(this);
}

void
WrapBox::exportJS( QScriptEngine *engine, QScriptValue &obj)
{
    QScriptValue ns;
    if (this->c()->isArray()) {
        ns = engine->newArray();
        for (int i=0;i<this->c()->maxOccurencesNumber();i++) {
            BoxClass *box_class = new BoxClass(engine, (WrapBox*)this->occurences().at(i));
            QScriptValue val = box_class->constructor();
            this->occurences().at(i)->setNamespace(val);
            foreach(WrapBoxComponent *r, this->occurences().at(i)->redefines()) {
                r->exportJS(engine, val);
            }

            foreach(WrapBoxComponent *w, this->occurences().at(i)->childs()) {
                w->exportJS(engine,val);
            }

            ns.setProperty(i, val);
       }
    } else {
        BoxClass *box_class = new BoxClass(engine, this/*template*/);
        ns = box_class->constructor();

        foreach(WrapBoxComponent *r, redefines()) {
            r->exportJS(engine, ns);
        }

        foreach(WrapBoxComponent *w, childs()) {
            w->exportJS(engine,ns);
        }
    }

    obj.setProperty(c()->name(), ns);
    setNamespace(ns);
    return;
}


void
WrapBox::pack( QIODevice *device )
{
    QBitArray bitmap;
    int de_idx = 0;
    bool count_de = false;

    size_t choice_sav_pos = 0;
    Q_ASSERT((Box*)(this->c()));

    this-c()->pack(device);
}

QByteArray
WrapBox::unpack()
{
    QBitArray bitmap;
    int de_idx = 0;
    bool count_de = false;

    size_t choice_sav_pos = 0;
    Q_ASSERT((Box*)(this->c()));

    QByteArray ba_ret = this->c()->unpack();

    return ba_ret;
}

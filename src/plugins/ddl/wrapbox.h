#ifndef WRAPBOX_H
#define WRAPBOX_H

#include <QtCore/QList>
#include <QtCore/QPointer>

#include "wrapboxcomponent.h"
#include "box.h"

class WrapBoxRedefine;
class WrapBox : public WrapBoxComponent
{
public:
    WrapBox( BoxComponent *box/*, QObject *parent*/ );
    virtual ~WrapBox();

    virtual void exportJS( QScriptEngine *engine,
                           QScriptValue &ns);
public: //tree structure
    void add( WrapBoxComponent *c);
//    QList<WrapBoxComponent *> childs() const { return childs_; }
    void pack( QIODevice *device );
    QByteArray unpack();
};
#endif //WRAPBOX

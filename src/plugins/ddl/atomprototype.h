#ifndef ATOMPROTOTYPE_H
#define ATOMPROTOTYPE_H

#include <QtCore/QObject>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

#include "atomclass.h"
#include "atom.h"
#include "binary.h"

class AtomPrototype: public QObject, public QScriptable
{
    Q_OBJECT
public:
    AtomPrototype(QObject *parent = 0, WrapBoxComponent *model=0);
    ~AtomPrototype();

public slots:
    //void display(ushort lvls = 1, ushort cur_dump_lvl = 0) const;
    void pack(ByteArray b);
    ByteArray unpack();
    bool isNumeric() const { return ((Atom*)thisAtom()->c())->isNumeric(); }
    bool isRedefine() const { return thisAtom()->c()->isRedefine(); }
    bool isRecord() const { return thisAtom()->c()->isRecord(); }
    bool isKey() const { return thisAtom()->c()->isKey(); }
    bool isSigned() const { return ((Atom*)thisAtom()->c())->isSigned(); }
    bool isBinary() const { return ((Atom*)thisAtom()->c())->isBinary(); }
    bool hasRedefines() const { return thisAtom()->c()->hasRedefines(); }
    QScriptValue toString() const;
    void clear() const;
    QString toJSON( bool need_redefines = false) const { return thisAtom()->c()->toJSON( need_redefines ); }

signals:
    void changed(QScriptValue);
    void packed(QScriptValue);
    void unpacked(QScriptValue);

private slots:
    void slot_changed(BoxComponent *c) { /*emit changed(thisObject());*/ }
    void slot_packed(BoxComponent *c) {  /*emit packed(thisObject());*/ }
    void slot_unpacked(BoxComponent *c) { /*emit unpacked(thisObject());*/ }

private:
    WrapAtom *thisAtom() const;

};

#endif //atomstringprototype.h

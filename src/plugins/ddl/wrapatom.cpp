#include "wrapatom.h"

#include <QtCore/QBuffer>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>

#include "atom.h"
#include "atomclass.h"
#include "boxcomponent.h"

#include "wrapbox.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


WrapAtom::WrapAtom( BoxComponent *atom) 
    : WrapBoxComponent (atom)
{
    Q_ASSERT(static_cast<Atom*>(atom));
}

WrapAtom::~WrapAtom()
{
}

void
WrapAtom::exportJS( QScriptEngine *engine, QScriptValue &obj)
{
    QScriptValue ns;
    if (this->c()->isArray()) {
        ns = engine->newArray();
        for (int i=0;i<this->c()->maxOccurencesNumber();i++) {
            AtomClass *atom_class = new AtomClass(engine, (WrapAtom*)this->occurences().at(i));
            QScriptValue val = atom_class->constructor();
            this->occurences().at(i)->setNamespace(val);

            foreach(WrapBoxComponent *r, this->occurences().at(i)->redefines()) {
                r->exportJS(engine, val);
            }

            ns.setProperty(i, val);
       }
    } else {
        AtomClass *atom = new AtomClass(engine, this);
        ns = atom->constructor();

        if ( c()->hasRedefines() ) {
            foreach(WrapBoxComponent *r, redefines()) {
                r->exportJS(engine, ns);
            }
        }
    }
    
    obj.setProperty(c()->name(), ns);
    setNamespace(ns);

    return;
}

void
WrapAtom::pack( QIODevice *device )
{
    QBitArray bitmap;
    int de_idx = 0;
    bool count_de = false;

    size_t choice_sav_pos = 0;
    Q_ASSERT((Box*)(this->c()));

    this-c()->pack(device);

    // QScriptValue beginPack = scriptObject().property("__proto__").property("beginPack");
    // if (beginPack.isFunction()) {
    //     beginPack.call(scriptObject());
    // }

    //foreach (BoxComponent *o, c()->occurences())
    //    o->pack(&device);

    // if (c()->state() == BoxComponent::OK_State) {
    //  qDebug()<<"    ok:atom:"<<c()->name()<<c()->name()<<c()->bytes().toHex();   
    // } else {
    //  qDebug()<<"    notok:atom:"<<c()->name()<<c()->bytes().toHex(); 
    // }

    //
    // Process redefines
    //
    //Atom *atom = static_cast<Atom*>(this->c());
    //Q_ASSERT(atom);
    // if (atom->hasRedefines()) {
    //     // the element which this redefines is done packing.
    //  QBuffer *buf = new QBuffer();
    //  buf->setData(atom->bytes());

    //     BoxRedefine *r;
    //     foreach (WrapBoxRedefine *wr, redefines()) {
    //      qDebug()<<"packing redefine:"<<wr->c()->name();
    //      wr->pack(*buf);
    //     }
    //  delete buf;
    // }

    // QScriptValue endPack = scriptObject().property("__proto__").property("endPack");
    // if (endPack.isFunction()) {
    //     endPack.call(scriptObject());
    // }
}

QByteArray
WrapAtom::unpack()
{
    QScriptValue beginUnpack = scriptObject().property("__proto__").property("beginUnpack");
    if (beginUnpack.isFunction()) {
    beginUnpack.call(scriptObject());
    }

    QByteArray ba_ret = this->c()->unpack();

    QScriptValue endUnpack = scriptObject().property("__proto__").property("endUnpack");

    if (endUnpack.isFunction()) {
        endUnpack.call(scriptObject());
    }
   
    return ba_ret;
}

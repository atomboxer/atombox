#include <QtScript/QScriptClassPropertyIterator>
#include <QtScript/QScriptEngine>
#include <QtCore/QDebug>
#include <QtCore/QBuffer>
#include <QtCore/QPointer>

#include "atomstring.h"
#include "atomclass.h"
#include "atomprototype.h"
#include "boxclass.h"

#include <assert.h>

#include "constraintrange.h"
#include "constraintvalue.h"
#include "constraintstring.h"
#include "constraintregex.h"

#include "bytearrayclass.h"
#include "binary.h"

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif
#include <QtCore/QPointer>
//#define ATOMCLASS_DEBUG

Q_DECLARE_METATYPE(AtomClass*)
Q_DECLARE_METATYPE(BoxClass*)
Q_DECLARE_METATYPE(WrapAtomPointer*)
Q_DECLARE_METATYPE(WrapAtomPointer)

Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteArray)
Q_DECLARE_METATYPE(ByteString)
Q_DECLARE_METATYPE(ByteString*)


static QScriptValue 
ReturnTrue(QScriptContext *ctx, QScriptEngine *eng)
{
    return true;
}

static QScriptValue 
ReturnFalse(QScriptContext *ctx, QScriptEngine *eng)
{
    return false;
}

AtomClass::AtomClass(QScriptEngine *engine, WrapAtom *model)
            :QObject(engine), QScriptClass(engine)
{
    model_ = model;
    
    bytes_  = engine->toStringHandle(QLatin1String("bytes"));
    constraints_  = engine->toStringHandle(QLatin1String("constraints"));
    display_ = engine->toStringHandle(QLatin1String("display"));
    index_  = engine->toStringHandle(QLatin1String("index"));
    length_ = engine->toStringHandle(QLatin1String("length"));
    name_   = engine->toStringHandle(QLatin1String("name"));
    offset_ = engine->toStringHandle(QLatin1String("offset"));
    state_  = engine->toStringHandle(QLatin1String("state"));
    value_  = engine->toStringHandle(QLatin1String("value"));

    QScriptValue global = engine->globalObject();
    proto_ = engine->newQObject(new AtomPrototype(this, model),
                                QScriptEngine::ScriptOwnership,
                                QScriptEngine::SkipMethodsInEnumeration
                                | QScriptEngine::ExcludeSuperClassMethods
                                | QScriptEngine::ExcludeSuperClassProperties);
    proto_.setPrototype(global.property("Object").property("prototype"));

    ctor_ = engine->newFunction(construct, proto_);
    ctor_.setData(engine->toScriptValue(this));
}

AtomClass::~AtomClass()
{
#ifdef ATOMCLASS_DEBUG
    qDebug()<<"AtomClass::~AtomClass"<<model_->c()->name();
#endif

   if (model_) {
       delete model_;
       model_ = 0;
   }

}

QScriptValue 
AtomClass::newInstance(const WrapAtomPointer &atom)
{

    QScriptValue data = engine()->newVariant(QVariant::fromValue(atom));
    QScriptValue ret =  engine()->newObject(this, data);

    // QObject::connect(atom.data()->c(), SIGNAL(packed(BoxComponent*)),
    //                  (AtomPrototype*)(ret.prototype().toQObject()), SLOT(slot_packed(BoxComponent*)));

    // QObject::connect(atom.data()->c(), SIGNAL(unpacked(BoxComponent*)),
    //                  (AtomPrototype*)(ret.prototype().toQObject()), SLOT(slot_unpacked(BoxComponent*)));

    // QObject::connect(atom.data()->c(), SIGNAL(changed(BoxComponent*)),
    //                  (AtomPrototype*)(ret.prototype().toQObject()), SLOT(slot_changed(BoxComponent*)));

    return ret;
}

QScriptValue 
AtomClass::construct(QScriptContext *ctx, QScriptEngine *eng)
{
    AtomClass *cls = qscriptvalue_cast<AtomClass*>(ctx->callee().data());

    if (!cls)
        return QScriptValue();

    qWarning("AtomClass constructor should not be invoked directly!");
    return QScriptValue();
}

QScriptClass::QueryFlags 
AtomClass::queryProperty(const QScriptValue &object,
                         const QScriptString &name,
                         QueryFlags flags, uint *id)
{
    if ( name == bytes_   ||
         name == constraints_ ||
         name == index_   ||
         name == length_  ||
         name == name_    ||
         name == offset_  ||
         name == state_   ||
         name == display_ ||
         name == value_ ) {
        return flags;
    } else {
        flags &= ~HandlesReadAccess;
        return flags;
    }
}

QScriptValue 
AtomClass::property(const QScriptValue &object,
                    const QScriptString &name, uint id)
{
    WrapAtomPointer *ptr = qscriptvalue_cast<WrapAtomPointer*>(object.data());
    if (!ptr) {
        qFatal("AtomClass::property, cannot convert");
    }

    WrapAtom* wrapatom = ptr->data();
    if (!wrapatom) {
       return QScriptValue();
    }

    Atom *atom = (Atom*)wrapatom->c();
    if (!wrapatom) {
        qFatal("AtomClass::property, cannot convert atom");
    }

    if (name == length_) {
        return (int)atom->length();
    } else if (name == value_) {
        // if (atom->state() == BoxComponent::Error_State)
        //     //
        //     //  Invalid access to atom
        //     //
        //     engine()->currentContext()->throwError(".value invalid access. Atom is not in OK_State");

        bool ok = false;

        // if (atom->isNumeric()) {
        //     qint32 v = atom->value().toInt(&ok);
        //     if (!ok) {
        //         return engine()->newVariant(atom->value());
        //     } else {
        //         return engine()->newVariant(v);
        //     }
            
        // } else {
        if (atom)
            return engine()->newVariant(atom->value());
        //}

        // } else 
        //     return QString(atom->bytes().toHex().data());
    } else if (name == bytes_) {
        return ByteArrayClass::toScriptValue(engine(), atom->bytes());
    } else if (name == offset_) {
        return atom->offset();
    } else if (name == state_) {
        return atom->state();
    } else if (name == index_) {
        return atom->idx();
    } else if (name == name_) {
        return atom->name();
    } else if (name == display_) {
        return atom->displayString();
    } else if (name == constraints_) {
        QScriptValue arr = engine()->newObject();
        foreach (Constraint *c, atom->constraints()) {
            switch (c->kind()) {
            case Constraint::k_value: 
                arr.setProperty(((ConstraintValue*)c)->value().toString(), c->display());
                break;
            case Constraint::k_range: 
                arr.setProperty(((ConstraintRange*)c)->matchedValue().toInt(), c->display()); break;
            case Constraint::k_regex:  
                arr.setProperty(((ConstraintRange*)c)->matchedValue().toString(), c->display()); break;
            case Constraint::k_list:
                arr.setProperty(((ConstraintRange*)c)->matchedValue().toString(), c->display()); break;
            default: assert(0); break;
            }
        }
        return arr;
    }


    return QScriptValue();
}

void 
AtomClass::setProperty(QScriptValue &object,
                       const QScriptString &name,
                       uint id, const QScriptValue &value)
{
    WrapAtomPointer *ptr = qscriptvalue_cast<WrapAtomPointer*>(object.data());
    if (!ptr) {
        qFatal("AtomClass::setProperty, cannot convert");
    }

    WrapAtom* wrapatom = ptr->data();
    if (!wrapatom) {
        qFatal("AtomClass::setProperty, cannot convert wrapatom");
    }

    BoxComponent *atom = wrapatom->c();
    if (!wrapatom) {
        qFatal("AtomClass::setProperty, cannot convert atom");
    }


    if (name == state_) {
        int v = value.toInt32(); 
        switch (v) {
        case BoxComponent::OK_State: {
            atom->setState(BoxComponent::OK_State); 
            return;
        } break;
        case BoxComponent::Exception_State: {
            atom->setState(BoxComponent::Exception_State); 
            return;
        } break;
        case BoxComponent::Error_State: {
            atom->setState(BoxComponent::Error_State); 
            return;
        } break; 
        }
    } else if (name == length_) {
         qFatal("You cannot set the length of an atom");
    } else if (name == value_) {
        ByteArray  *ba = qscriptvalue_cast<ByteArray*>(value.data());
        ByteString *bs = qscriptvalue_cast<ByteString*>(value.data());

        if (ba)
            static_cast<Atom*>(atom)->changeValue(qvariant_cast<ByteArray>(value.data().toVariant()));
        else if (bs)
            static_cast<Atom*>(atom)->changeValue(qvariant_cast<ByteString>(value.data().toVariant()));
        else
            static_cast<Atom*>(atom)->changeValue(value.toVariant());

        return;
    }
}



QScriptValue::PropertyFlags 
AtomClass::propertyFlags(
    const QScriptValue &/*object*/, const QScriptString &name, uint /*id*/)
{
    if ( 
         name == bytes_    ||
         name == constraints_ || 
         name == display_  ||
         name == index_    ||
         name == length_   ||
         name == name_     ||
         name == offset_   ||
         name == state_    ||
         name == value_  ) {
        return QScriptValue::Undeletable
            | QScriptValue::SkipInEnumeration;
    }
    return QScriptValue::Undeletable;
}

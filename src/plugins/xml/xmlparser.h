#ifndef XMLPARSER_H
#define XMLPARSER_H

#include <QtCore/QObject>

#include "stream.h"

#include <QtXml/QXmlSimpleReader>
#include <QtXml/QXmlInputSource>

class XmlParser : public QObject
{
    friend class XmlParserPrototype;

    Q_OBJECT

public:
    XmlParser(Stream *stream = 0, QObject *parent = 0);
    ~XmlParser();

    //signals:
    //processinginstruction

private:
    Stream *stream_;

    //QXmlParser *priv_;
};

#endif //XMLPARSER_H

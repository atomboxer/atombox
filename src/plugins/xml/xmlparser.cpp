
#include "binary.h"

#include "xmlparser.h"

#include <QtCore/QBuffer>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

XmlParser::XmlParser(Stream *stream, QObject *parent) :
    QObject(parent)
{
    if (!stream) {
        QBuffer  *b = new QBuffer();
        b->open(QIODevice::ReadOnly);
        stream = new Stream(b);
    }

    stream_ = stream;
}


XmlParser::~XmlParser()
{
    if (stream_)
        delete stream_;
}


#include "database.h"

#include <QtCore/QDebug>

#include <QtSql/QSqlDatabase>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Database::Database(const QString dbType)
{
    
    if (dbType.toLower() == "sqlite")
        db_ = QSqlDatabase::addDatabase("QSQLITE");
    else if (dbType.toLower() == "db2")
        db_ = QSqlDatabase::addDatabase("QDB2");
    else {
        qWarning((QString("Unknown database driver ")+dbType.toLower()).toLatin1().data());
        //@todo add more
        db_ = QSqlDatabase();
    }
}

Database::Database(const QString dbType, const QString conn)
{   
    if (dbType.toLower() == "sqlite")
        db_ = QSqlDatabase::addDatabase("QSQLITE", conn);    
    else if (dbType.toLower() == "db2")
        db_ = QSqlDatabase::addDatabase("QDB2", conn);
    else {
        qWarning((QString("Unknown database driver ")+dbType.toLower()).toLatin1().data());
        //@todo add more
        db_ = QSqlDatabase();
    }
}


Database::~Database()
{
}


/**
 *  @module sql
 *  @desc This module provides support for SQL databases
 *  @note SQLITE (v3) and DB2 drivers only. For DB2 the IBM DB2 client libraries need to be installed and in PATH (or LD_LIBRARY_PATH on UNIX)
 *  @example


var console = require("console");
var system  = require("system");
var sql     = require("sql");

var db1 = new sql.Database("db2");
db1.setDatabaseName("maudb");
db1.setHostName("35.156.4.171");
db1.setPort(50000);
db1.open("db2inst1", "mypassword");

if (!db1.isOpen()) {
    console.writeln("unable to open database"+db1.error());
    system.exit(0);
}

var q = new sql.Query(db1);

try {
    q.exec("DROP TABLE maudb.Persons;");
} catch(e){};

try {
    q.prepare('CREATE TABLE maudb.Persons(P_Id int,LastName varchar(255),FirstName varchar(255));');
    q.exec();

    q.exec("INSERT INTO maudb.Persons (P_Id, LastName, FirstName) VALUES(1, 'JOE', 'GANDI')");
    q.exec("INSERT INTO maudb.Persons (P_Id, LastName, FirstName) VALUES(2, 'DOE', 'JOHN')");
} catch (e) {
    console.error("error +"+q.error());
}

var qs = new sql.Query(db1);
try {
    qs.exec("SELECT * from maudb.Persons");
} catch (e) {
    console.error("error +"+qs.error());
}

while(qs.next()) {
    console.write("last name:"+qs.value(1)+"\n");
}

system.exit(0);

//outputs: 
//
//last name:JOE
//last name:DOE


 */

/**
 * @class
 * @name Database 
 * @summary The Database class represents a connection to a database 
 * @memberof module:sql
 * @classdesc The Database class provides a common interface for accessing  a database through a connection. An instance of this class represents the connection. The connection provides access to the database via one of the supported database drivers.
If connectionName is not specified, the new connection becomes the default connection for the application
 * @note SQLITE (v3) and DB2 drivers only. For DB2 the IBM DB2 client libraries need to be installed and in PATH (or LD_LIBRARY_PATH on UNIX)
 * @note On Guardian OS, the sqlite databases are opened with the follwing pragmas: journal_mode = MEMORY and locking_mode = EXCLUSIVE

 * @arg {String} driver The database driver. One of the following: sqlite, db2
 * @arg {String} connectionName The connection name that is used to refer to the database connection being constructed
 * @example
var sql     = require("sql");
var db1 = new sql.Database("sqlite");
db1.setDatabaseName("testdb1");
db1.open();
//..
 */

/**
 * Closes the database connection
 * @method module:sql.Database.prototype.close
 */
exports.Database = __internal__.Database;

/**
 * Commits the current transaction
 * @method module:sql.Database.prototype.commit
 * @returns {Boolean}
 */

/**
 * Database name
 * @method module:sql.Database.prototype.databaseName
 * @returns {String}
 */

/**
 * Execute the Query object
 * @method module:sql.Database.prototype.exec
 * @throws Error
 * @returns {Query}
 */

/**
 * Returns the host name of this connection
 * @method module:sql.Database.prototype.hostName
 * @returns {String}
 */

/**
 * Returns true if the database connection is opened, false otherwise
 * @method module:sql.Database.prototype.isOpen
 * @returns {Boolean}
 */

/**
 * Returns error string (in case of an error)
 * @method module:sql.Database.prototype.error
 * @returns {String}
 */


/**
 * Opens the database connection using the current connection values. Returns true on success; otherwise returns false.  Error information can be retrieved using error().
 * @method module:sql.Database.prototype.open
 * @returns {Boolean}
 */

/**
 * Opens the database connection using the provided username and password. Returns true on success; otherwise returns false.  Error information can be retrieved using error().
 * @method module:sql.Database.prototype.open
 * @arg {String} user The username
 * @arg {String} password The password
 * @returns {Boolean}
 */


/**
 * Returns the password associated to this database connection
 * @method module:sql.Database.prototype.password
 * @returns {String}
 */

/**
 * Sets the connection's database name. To have effect, the database name must be set before the connection is opened.
 * @method module:sql.Database.prototype.setDatabaseName
 * @arg {String} name The database name
 */

/**
 * Sets the connection's host name. To have effect, the host name must be set before the connection is opened.
 * @method module:sql.Database.prototype.setHostName
 * @arg {String} host The host name
 */

/**
 * Sets the connection's host name. To have effect, the host name must be set before the connection is opened.
 * @method module:sql.Database.prototype.setPort
 * @since 0.6
 * @arg {Number} port The port name
 */
 
/**
 * Sets the connection's password. To have effect, the password name must be set before the connection is opened.
 * @method module:sql.Database.prototype.setPassword
 * @arg {String} password
 */

/**
 * Sets the connection's username. To have effect, the username must be set before the connection is opened.
 * @method module:sql.Database.prototype.setUserName
 * @arg {String} user The username
 */

/**
 * Returns a list of the database's tables, system tables and views.
 * @method module:sql.Database.prototype.tables
 * @returns {Array} A string array containing all the available tables
 */

/** 
 * Begins a transaction on the database if the driver supports transactions. Returns * true if the operation succeeded. Otherwise it returns false.
 * @method  module:sql.Database.prototype.transaction
 * @returns {Boolean}
 */

/** 
 * Returns the username associated with this connection
 * @method  module:sql.Database.prototype.userName
 * @returns {String}
 */

/** 
 * Returns true if the object has a valid driver
 * @method  module:sql.Database.prototype.isValid
 * @returns {Boolean}
 */





/**
 * @class
 * @name Query 
 * @summary A means of executing and manipulating SQL statements
 * @memberof module:sql
 * @classdesc The Query class provides a means of executing and manipulating SQL statements. This class encapsulates the functionality involved in creating, navigating and retrieving data from SQL queries that are executed on a Database object. It can be used to execute SQL DML commands like: SELECT, INSERT, UPDATE and DELETE and SQL DDL commands like: CREATE TABLE.
Navigating through the sets are performed using: next(), previous(), first(), last(), seek()

 * @example
 

var console = require("console");
var system  = require("system");
var sql     = require("sql");

var db1 = new sql.Database("db2");
db1.setDatabaseName("maudb");
db1.setHostName("35.156.4.171");
db1.setPort(50000);
db1.open("db2inst1", "mypassword");

if (!db1.isOpen()) {
    console.writeln("unable to open database"+db1.error());
    system.exit(0);
}

var q = new sql.Query(db1);

try {
    q.exec("DROP TABLE maudb.Persons;");
} catch(e){};

try {
    q.prepare('CREATE TABLE maudb.Persons(P_Id int,LastName varchar(255),FirstName varchar(255));');
    q.exec();

    q.exec("INSERT INTO maudb.Persons (P_Id, LastName, FirstName) VALUES(1, 'JOE', 'GANDI')");
    q.exec("INSERT INTO maudb.Persons (P_Id, LastName, FirstName) VALUES(2, 'DOE', 'JOHN')");
} catch (e) {
    console.error("error +"+q.error());
}

var qs = new sql.Query(db1);
try {
    qs.exec("SELECT * from maudb.Persons");
} catch (e) {
    console.error("error +"+qs.error());
}

while(qs.next()) {
    console.write("last name:"+qs.value(1)+"\n");
}

system.exit(0);

//outputs: 
//
//last name:JOE
//last name:DOE

 */

/** 
 * Returns the current internal position of the query. The first record is at position zero.
 * @method  module:sql.Query.prototype.at
 * @returns {Number}
 */

/** 
 * Clears the result set and releases any resources held by the query
 * @method  module:sql.Query.prototype.clear
 */

/** 
 * Executes an SQL query
 * @method  module:sql.Query.prototype.exec
 * @arg {String} query - The query to execute
 * @throws Error
 * @returns {Boolean} 
 */

/** 
 * Executes a previously prepared SQL query
 * @method  module:sql.Query.prototype.exec
 * @throws Error
 * @returns {Boolean} 
 */

/** 
 * Returns the last query that was successfully executed
 * @method  module:sql.Query.prototype.executedQuery
 * @returns {String}
 */

/** 
 * Retrieves the first record in the result set
 * @method  module:sql.Query.prototype.first
 * @returns {Boolean}
 */

/** 
 * Returns true if the query is active. A query is considered active if it has been executed but it has not finished yet.
 * @method  module:sql.Query.prototype.isActive
 * @returns {Boolean}
 */

/** 
 * Returns true if the query is active and positioned on a valid record and the field is NULL; otherwise returns false.
 * @method  module:sql.Query.prototype.isNull
 * @returns {Boolean}
 */

/** 
 * Returns true if the current query is a SELECT statement; otherwise returns false.
 * @method  module:sql.Query.prototype.isSelect
 * @returns {Boolean}
 */

/** 
 * Returns true if the current query is a SELECT statement; otherwise returns false.
 * @method  module:sql.Query.prototype.isValid
 * @returns {Boolean}
 */

/** 
 * Retrieves the last record in the result, if available, and positions the query on the retrieved record.
 * @method  module:sql.Query.prototype.last
 * @returns {Boolean}
 */

/** 
 * Returns error information about the last error (if any) that occurred with this query
 * @method  module:sql.Query.prototype.error
 * @returns {String}
 */

/** 
 * Retrieves the next record in the result, if available, and positions the query on the retrieved record. If the result is currently located before the first record, an attempt is made to retrieve the first record
 * @method  module:sql.Query.prototype.next
 * @returns {Boolean}
 */

/** 
 * Returns the number of rows affected by the result's SQL statement, or -1 if it cannot be determined.
 * @method  module:sql.Query.prototype.numRowsAffected
 * @returns {Number}
 */

/** 
 * Retrieves the previous record in the result, if available, and positions the query on the retrieved record.
 * @method  module:sql.Query.prototype.previous
 * @returns {Boolean}
 */

/** 
 * Prepares the SQL query query for execution. Returns true if the query is prepared successfully; otherwise returns false.
 * @method  module:sql.Query.prototype.prepare
 * @arg {String} query
 * @returns {Boolean}
 */
/** 
 * Retrieves the record at position index, if available, and positions the query on the retrieved record. The first record is at position 0.
 * @method  module:sql.Query.prototype.seek
 * @returns {Boolean}
 */

/** 
 * Returns the value of field index in the current record.The fields are numbered from left to right using the text of the SELECT statement.
 * @method  module:sql.Query.prototype.value
 * @arg {Number} index The field index to return the value of
 * @returns {Object}  the value at the index
 */

exports.Query = __internal__.Query;

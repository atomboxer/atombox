#include "query.h"

#include <QtSql/QSqlQuery>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Query::Query()
{
    q_ = QSqlQuery();
}

Query::Query(const QString &q)
{
    q_ = QSqlQuery(q);
}

Query::Query(const QString &q, Database *d)
{
    Q_ASSERT(d);
    q_ = QSqlQuery(q, d->db());
}

Query::Query(Database *d)
{
    Q_ASSERT(d);
    q_ = QSqlQuery(d->db());
}

Query::~Query()
{
}

#ifndef SQLPLUGIN_H
#define SQLPLUGIN_H

#include <QScriptExtensionPlugin>

class SqlPlugin : public QScriptExtensionPlugin
{
public:
    SqlPlugin( QObject *parent = 0);
    ~SqlPlugin();

    virtual void initialize(const QString &key, QScriptEngine *engine);
    virtual QStringList keys() const;

private:
    // nothing
};

#endif //SQLPLUGIN_H

#ifndef MQPLUGIN_H
#define MQPLUGIN_H

#include <QScriptExtensionPlugin>

class MQPlugin : public QScriptExtensionPlugin
{
public:
    MQPlugin( QObject *parent = 0);
    ~MQPlugin();

    virtual void initialize(const QString &key, QScriptEngine *engine);
    virtual QStringList keys() const;

private:
    // nothing
};

#endif //MQPLUGIN_H

#ifndef MQQUEUEPROTOTYPE_H
#define MQQUEUEPROTOTYPE_H

#include <QtScript/QScriptValue>
#include <QtScript/QScriptable>

#include "mqqueue.h"

class MQQueuePrototype: public QObject, public QScriptable
{
    Q_OBJECT

public:
    MQQueuePrototype(QObject *parent = 0);
    ~MQQueuePrototype();

public slots:
    bool       close() const;

    bool    isOpened() const        { return thisMQQueue()->isOpened(); }
    QString lastErrorString() const { return thisMQQueue()->lastErrorString(); }
    long    lastReasonCode()  const { return thisMQQueue()->lastReasonCode(); }

signals:
    //


private slots:
    //

private:
    MQQueue *thisMQQueue() const;
};

#endif //MQQUEUEPROTOTYPE_H

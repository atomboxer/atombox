
#include <QtCore/QDebug>

#include "mqqueue.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

#define MQQUEUE_DEBUG

MQQueue::MQQueue( QObject *parent )
    :QObject(parent),
     is_opened_(false)
{
    timer_ = NULL;
}

MQQueue::~MQQueue()
{
    if (timer_)
        delete timer_;
}

bool
MQQueue::close()
{
    MQLONG   ccode;
    MQLONG   creason;

    MQCLOSE( hcon_,
             &hobj_,
             MQCO_NONE,
             &ccode,
             &creason);

    if (creason != MQRC_NONE) {
        is_opened_  = false;
        last_error_ = "MQCLOSE ended with reason code "+QString::number(creason);
        return false;
    }

    return true;
}

MQRequestMessage*
MQQueue::createRequest()
{
    MQRequestMessage *m = new MQRequestMessage(hcon_, hobj_);
    return m;
}

MQMessage*       
MQQueue::createDatagram()
{
    MQMessage *m = new MQMessage(hcon_, hobj_);
    return m;
}

bool
MQQueue::init(MQHCONN &hcon, MQHOBJ &hobj, QString &queue)
{
#ifdef MQQUEUE_DEBUG
    qDebug()<<"Queue "<<queue<<" opened";
#endif
    hcon_      = hcon;
    hobj_      = hobj;
    queue_     = queue;
    is_opened_ = true;

    if (timer_)
        delete timer_;

    timer_ = new QTimer(this);
    QObject::connect(timer_, SIGNAL(timeout()), this, SLOT(__internal__timeout()));
    timer_->start(200); /*milliseconds*/
    return true;
}

void
MQQueue::__internal__timeout()
{
    //
    //  Read a message from the queue
    //
    MQBYTE   buffer[65536];              /* message buffer                */
    MQLONG   buflen = sizeof(buffer);    /* buffer size available for GET */
    MQLONG   messlen;                    /* message length received       */
    MQGMO    gmo = {MQGMO_DEFAULT};      /* get message options           */
    MQMD     md  = {MQMD_DEFAULT};       /* Message Descriptor            */
    MQLONG   ccode;
    MQLONG   creason;

    gmo.Options = MQGMO_NO_WAIT           /* wait for new messages       */
                 | MQGMO_FAIL_IF_QUIESCING
                 | MQGMO_CONVERT;         /* convert if necessary        */

    ccode = MQCC_OK;

    while (ccode != MQCC_FAILED) {

        memcpy(md.MsgId,    MQMI_NONE, sizeof(md.MsgId));
        memcpy(md.CorrelId, MQCI_NONE, sizeof(md.CorrelId));

        md.Encoding       = MQENC_NATIVE;
        md.CodedCharSetId = MQCCSI_Q_MGR;

        MQGET(hcon_,               /* connection handle                 */
              hobj_,               /* object handle                     */
              &md,                 /* message descriptor                */
              &gmo,                /* get message options               */
              buflen,              /* buffer length                     */
              buffer,              /* message buffer                    */
              &messlen,            /* message length                    */
              &ccode,              /* completion code                   */
              &creason);           /* reason code                       */

        if ( creason != MQRC_NONE ) {
            if ( creason == MQRC_NO_MSG_AVAILABLE)
                return;
            else {
                emit error(creason);
		if (timer_) {
		    timer_->stop();
		    is_opened_ = false;
		    delete timer_;
		    timer_ = 0;
		}

                if (creason == MQRC_TRUNCATED_MSG_FAILED) {
                    ccode = MQCC_FAILED;
                }
            }
        }
        
        if (ccode != MQCC_FAILED) {
            if (md.MsgType == MQMT_REQUEST) {
                requestReady( md, QByteArray((char*)buffer, (size_t)messlen));
            } else if(md.MsgType == MQMT_DATAGRAM){
                datagramReady(md, QByteArray((char*)buffer, (size_t)messlen));
            } else {
                qWarning("Unknown message type");
            }
        }
    }
   
    return;
}

void 
MQQueue::requestReady(MQMD md, QByteArray data)
{
    MQMessage         *mrq = new MQRequestMessage(hcon_, hobj_, data); 
    MQResponseMessage *mrs = new MQResponseMessage(hcon_, hobj_, md);

    emit requestReady(mrq, mrs);
}

void 
MQQueue::datagramReady(MQMD md, QByteArray data)
{
    MQMessage *m = new MQMessage(hcon_, hobj_,  data);
    emit datagramReady(m);
}



#include <QtCore/QDebug>
#include <QtCore/QCoreApplication>

#include <QtScript/QScriptEngine>

#include "mqplugin.h"

#include "mqmanager.h"
#include "mqmanagerprototype.h"

#include "mqqueue.h"
#include "mqqueueprototype.h"

#include "mqmessage.h"
#include "mqmessageprototype.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

#if defined(AB_USE_SCRIPT_CLASSIC)
Q_EXPORT_PLUGIN2( abscriptmq_compat, MQPlugin )
#else
Q_EXPORT_PLUGIN2( abscriptmq, MQPlugin )
#endif


MQPlugin::MQPlugin( QObject *parent )
    : QScriptExtensionPlugin( parent )
{
}
    
MQPlugin::~MQPlugin()
{
}

Q_DECLARE_METATYPE(MQManager*)
Q_DECLARE_METATYPE(MQQueue*)

Q_DECLARE_METATYPE(MQMessage*)
Q_DECLARE_METATYPE(MQRequestMessage*)
Q_DECLARE_METATYPE(MQResponseMessage*)

QScriptValue 
MQManager_Constructor(QScriptContext *ctx, QScriptEngine *eng)
{
    if (ctx->argumentCount() != 0) {
        ctx->throwError("<<mq/websphere.MQManager>> constructor does not require arguments");
        return QScriptValue();
    }

    return eng->newQObject(new ::MQManager(),
                           QScriptEngine::AutoOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| QScriptEngine::ExcludeSuperClassMethods(*/
                           /*| QScriptEngine::ExcludeSuperClassProperties*/);
}

// QScriptValue 
// MQMessage_Constructor(QScriptContext *ctx, QScriptEngine *eng)
// { 
//     if (ctx->argumentCount() != 0) {
//         ctx->throwError("<<mq/websphere.MQMessage>> constructor does not require arguments");
//         return QScriptValue();
//     }

//     return eng->newQObject(new MQMessage(),QScriptEngine::ScriptOwnership,
//                            QScriptEngine::SkipMethodsInEnumeration); 
// }

QScriptValue 
MQMessage_toScriptValue(QScriptEngine *engine, MQMessage* const &in)
{ 
    return engine->newQObject(in,QScriptEngine::AutoOwnership,
                              QScriptEngine::SkipMethodsInEnumeration); 
}

void 
MQMessage_fromScriptValue(const QScriptValue &object, MQMessage* &out)
{ 
    out = qobject_cast<MQMessage*>(object.toQObject()); 
}


QScriptValue 
MQRequestMessage_toScriptValue(QScriptEngine *engine, MQRequestMessage* const &in)
{ 
    return engine->newQObject(in,QScriptEngine::AutoOwnership,
                              QScriptEngine::SkipMethodsInEnumeration); 
}

void 
MQRequestMessage_fromScriptValue(const QScriptValue &object, MQRequestMessage* &out)
{ 
    out = qobject_cast<MQRequestMessage*>(object.toQObject()); 
}

QScriptValue 
MQResponseMessage_toScriptValue(QScriptEngine *engine, MQResponseMessage* const &in)
{ 
    return engine->newQObject(in,QScriptEngine::AutoOwnership,
                              QScriptEngine::SkipMethodsInEnumeration); 
}

void 
MQResponseMessage_fromScriptValue(const QScriptValue &object, MQResponseMessage* &out)
{ 
    out = qobject_cast<MQResponseMessage*>(object.toQObject()); 
}

    
void
MQPlugin::initialize( const QString &key, QScriptEngine *engine )
{
    QScriptValue globalObject = engine->globalObject();
#if defined(AB_USE_SCRIPT_CLASSIC)
    if ( key == QString("mq_compat") ) {
#else
    if ( key == QString("mq") ) {
#endif

#ifdef AB_STATIC_PLUGINS
        Q_INIT_RESOURCE(mq);
#endif
        Q_INIT_RESOURCE(mqfiles);
    }

    QScriptValue __internal__;
    __internal__ = engine->globalObject().property("__internal__");
    if (!__internal__.isObject()) {
      __internal__ = engine->newObject();
      engine->globalObject()
    .setProperty("__internal__",__internal__);
    }

    engine->setDefaultPrototype(qMetaTypeId<MQManager*>(),
                                engine->newQObject(new MQManagerPrototype(this)));

    engine->setDefaultPrototype(qMetaTypeId<MQQueue*>(),
                                engine->newQObject(new MQQueuePrototype(this)));

    qScriptRegisterMetaType(engine, MQMessage_toScriptValue, 
                            MQMessage_fromScriptValue);
    engine->setDefaultPrototype(qMetaTypeId<MQMessage*>(),
                                engine->newQObject(new MQMessagePrototype(this)));

    qScriptRegisterMetaType(engine, MQRequestMessage_toScriptValue, 
                            MQRequestMessage_fromScriptValue);
    engine->setDefaultPrototype(qMetaTypeId<MQRequestMessage*>(),
                                engine->newQObject(new MQMessagePrototype(this)));

    qScriptRegisterMetaType(engine, MQResponseMessage_toScriptValue, 
                            MQResponseMessage_fromScriptValue);
    engine->setDefaultPrototype(qMetaTypeId<MQResponseMessage*>(),
                                engine->newQObject(new MQMessagePrototype(this)));

    engine->globalObject().property("__internal__").
         setProperty("MQManager", engine->newFunction(MQManager_Constructor));

    engine->globalObject().property("__internal__").property("MQManager").
       setProperty("prototype", engine->defaultPrototype(qMetaTypeId<MQManager*>()));

}
    
QStringList
MQPlugin::keys() const
{
    QStringList keys;
#if defined(AB_USE_SCRIPT_CLASSIC)
    keys << QString("mq_compat");
#else
    keys << QString("mq");
#endif
    return keys;
}

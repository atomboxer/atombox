#ifndef CTREEPROTOTYPE_H
#define CTREEPROTOTYPE_H

#include <QtCore/QString>

#include <QtScript/QScriptValue>
#include <QtScript/QScriptable>

#include "ctreeclass.h"

class CtreePrototype : public QObject, public QScriptable
{
    Q_OBJECT

public:
    CtreePrototype(QObject *parent = 0);
    ~CtreePrototype();

public slots:
    void connect(QString server, QString username="", QString password="") const;    
    void disconnect() const;
    bool isConnected() const;
    QScriptValue open( QString filename, bool exclusive = false, QString pass="");

    void beginTransaction();
    void endTransaction();
    void abortTransaction();

private:
    Ctree   *thisCtree() const;
};

#endif //CTREEPROTOTYPE_H


#include <QtCore/QDebug>

#include "ctreeclass.h"

#define NO_ctFeatOPENSSL
#include <ctreep.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define CTREE_DEBUG

Ctree::Ctree( QObject * parent)
    :QObject(parent),
     last_error_(""),
     is_connected_(false)

{

}


Ctree::~Ctree()
{

}

void
Ctree::connect(QString server, QString username, QString password)
{

    sname_ = server;
    username_ = username;
    password_ =password;

#ifdef ctThrds
    /* initialize c-tree multi-thread environment */
    if (ctThrdInit(3, 0L, NULL) != 0) {
       last_error_ = QString("ctThrdInit: nisam_err = %1, isam_fil = %2, sysiocod = %3\n").arg(isam_err).arg(isam_fil).arg(sysiocod);
       return;
    }
#endif

#ifdef CTREE_DEBUG
    qDebug()<<"InitISAMXtd:"<<sname_.toLatin1().data();
#endif
    if (InitISAMXtd(16, 16, 16, 16, 0, 
        username_.toLatin1().data(), 
        password_.toLatin1().data(),
        sname_.toLatin1().data())) {
        //
        last_error_ = QString("InitISAMXtd: nisam_err = %1, isam_fil = %2, sysiocod = %3\n").arg(isam_err).arg(isam_fil).arg(sysiocod);
        return;
    }
    
    is_connected_ = true;
}


void
Ctree::disconnect()
{
   CloseISAM();
#ifdef ctThrds
    ctThrdTerm();
#endif

    is_connected_ = false;
}

bool
Ctree::beginTransaction()
{
    Begin(ctTRNLOG);
    return true;
}

bool
Ctree::endTransaction()
{
    Commit(ctKEEP);
    return true;
}

bool
Ctree::abortTransaction()
{
    AbortXtd(ctKEEP);
    return true;
}

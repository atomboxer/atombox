#ifndef CTREE_H
#define CTREE_H

#include <QtCore/QObject>
#include <QtCore/QIODevice>

#define NO_ctFeatOPENSSL
#include <ctreep.h>


class Ctree : public QObject
{
    Q_OBJECT

public:
    Ctree( QObject *parent = 0);

    ~Ctree();

    void    connect(QString server, QString username, QString password);    
    void    disconnect();
    bool    isConnected() const { return is_connected_; }
    QString lastErrorString() const { return last_error_; }

    COUNT   openShared(QString filename, 
                       QString pass="") { return open(filename, ctSHARED, pass); }
    COUNT   openExclusive(QString filename, 
			  QString pass="") { return open(filename, ctEXCLUSIVE, pass); }
    bool    beginTransaction();
    bool    endTransaction();
    bool    abortTransaction();
private:
    COUNT open(QString fn, COUNT mode, QString pass);

    QString sname_;
    QString username_;
    QString password_;
    QString last_error_;
    bool    is_connected_;
};

#endif

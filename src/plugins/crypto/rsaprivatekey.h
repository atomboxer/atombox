#ifndef RSAPRIVATEKEY_H
#define RSAPRIVATEKEY_H


#include <QtScript/QScriptEngine>
#include <QtScript/QScriptContext>
#include <QtScript/QScriptValue>

#include <QtCore/QObject>
#include <QtCore/QString>

#include <core/binary.h>

#include <cryptopp/rsa.h>
#include <cryptopp/pkcspad.h>

class RSAPrivateKey : public QObject
{
    Q_OBJECT
public:
    RSAPrivateKey(CryptoPP::InvertibleRSAFunction key, QObject *parent=NULL);
    ~RSAPrivateKey();

public:
    CryptoPP::InvertibleRSAFunction getRSAKey() const { return key_; }
    
//public key
    Q_INVOKABLE ByteArray getPublicExponent() const;  /*e*/
    Q_INVOKABLE ByteArray getModulus() const;         /*n*/

//private key
    Q_INVOKABLE ByteArray getPrivateExponent() const; /*d*/
    Q_INVOKABLE ByteArray getPrime1() const;          /*p*/
    Q_INVOKABLE ByteArray getPrime2() const;          /*q*/
    Q_INVOKABLE ByteArray toPEM() const;
    Q_INVOKABLE ByteArray toDER() const;
    Q_INVOKABLE bool      isValid() const;
    Q_INVOKABLE bool      isPublicKey() const { return false; }
    Q_INVOKABLE bool      isPrivateKey() const { return true; }
    ByteArray encryptOAEP(QByteArray *ba) const;
    ByteArray decryptOAEP(QByteArray *ba) const;
    ByteArray encryptPKCS(QByteArray *ba) const;
    ByteArray decryptPKCS(QByteArray *ba) const;
    ByteArray signSHA(QByteArray *ba) const;
    bool verifySHA(QByteArray *ba, QByteArray *sig) const;

    ByteArray signSHA256(QByteArray *ba) const;
    bool verifySHA256(QByteArray *ba, QByteArray *sig) const;

private:
    CryptoPP::RSA::PrivateKey key_;
};

QScriptValue __crypto_rsa_generatePrivateKey(QScriptContext *, QScriptEngine *);
QScriptValue __crypto_rsa_createPrivateKeyFromComponents(QScriptContext *, QScriptEngine *);
QScriptValue __crypto_rsa_createPrivateKeyFromPEM(QScriptContext *, QScriptEngine *);
QScriptValue __crypto_rsa_createPrivateKeyFromBER(QScriptContext *, QScriptEngine *);

#endif

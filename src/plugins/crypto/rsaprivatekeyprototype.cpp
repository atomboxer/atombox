#include "rsaprivatekey.h"
#include "rsaprivatekeyprototype.h"

#include <QtCore/QDebug>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(RSAPrivateKey*)
Q_DECLARE_METATYPE(ByteString*)
Q_DECLARE_METATYPE(ByteArray*)

RSAPrivateKeyPrototype::RSAPrivateKeyPrototype(QObject *parent)
                : QObject(parent)
{
}

RSAPrivateKeyPrototype::~RSAPrivateKeyPrototype()
{
}

ByteArray
RSAPrivateKeyPrototype::encryptOAEP(QScriptValue msg) const
{
    ByteArray   *ba = qscriptvalue_cast<ByteArray*>(msg.data());
    ByteString  *bs = 0;
    if (!ba)
        bs = qscriptvalue_cast<ByteString*>(msg.data());
    if (!ba && !bs) {
        context()->throwError("RSAPrivateKey.encryptOAEP: arg(0) "\
                                  "not instance of ByteArray or ByteString");
        return QByteArray();
    }
    if (ba)
        return thisKey()->encryptOAEP((QByteArray*)ba);
    else
        return thisKey()->encryptOAEP((QByteArray*)bs);

    return ByteArray();
}

ByteArray
RSAPrivateKeyPrototype::decryptOAEP(QScriptValue msg) const
{
    ByteArray   *ba = qscriptvalue_cast<ByteArray*>(msg.data());
    ByteString  *bs = 0;
    if (!ba)
        bs = qscriptvalue_cast<ByteString*>(msg.data());
    if (!ba && !bs) {
        context()->throwError("RSAPrivateKey.decryptOAEP: arg(0) "\
                                  "not instance of ByteArray or ByteString");
        return QByteArray();
    }
    try {
    if (ba)
        return thisKey()->decryptOAEP((QByteArray*)ba);
    else
        return thisKey()->decryptOAEP((QByteArray*)bs);
}catch(const CryptoPP::Exception& e) {
        context()->throwError(e.what());
    }
    return ByteArray();
}

ByteArray
RSAPrivateKeyPrototype::encryptPKCS(QScriptValue msg) const
{
    ByteArray   *ba = qscriptvalue_cast<ByteArray*>(msg.data());
    ByteString  *bs = 0;
    if (!ba)
        bs = qscriptvalue_cast<ByteString*>(msg.data());
    if (!ba && !bs) {
        context()->throwError("RSAPrivateKey.encryptPKCS: arg(0) "\
                                  "not instance of ByteArray or ByteString");
        return QByteArray();
    }
    if (ba)
        return thisKey()->encryptPKCS((QByteArray*)ba);
    else
        return thisKey()->encryptPKCS((QByteArray*)bs);

    return ByteArray();
}

ByteArray
RSAPrivateKeyPrototype::decryptPKCS(QScriptValue msg) const
{
    ByteArray   *ba = qscriptvalue_cast<ByteArray*>(msg.data());
    ByteString  *bs = 0;
    if (!ba)
        bs = qscriptvalue_cast<ByteString*>(msg.data());
    if (!ba && !bs) {
        context()->throwError("RSAPrivateKey.decryptPKCS: arg(0) "\
                                  "not instance of ByteArray or ByteString");
        return QByteArray();
    }
    try {
    if (ba)
        return thisKey()->decryptPKCS((QByteArray*)ba);
        else
        return thisKey()->decryptPKCS((QByteArray*)bs);
    } catch(const CryptoPP::Exception& e) {
        context()->throwError(e.what());
    }
    return ByteArray();
}

ByteArray
RSAPrivateKeyPrototype::signSHA(QScriptValue msg) const
{
    ByteArray   *ba = qscriptvalue_cast<ByteArray*>(msg.data());
    ByteString  *bs = 0;
    if (!ba)
        bs = qscriptvalue_cast<ByteString*>(msg.data());
    if (!ba && !bs) {
        context()->throwError("RSAPrivateKey.signSHA: arg(0) "\
                                  "not instance of ByteArray or ByteString");
        return QByteArray();
    }
    try {
    if (ba)
        return thisKey()->signSHA((QByteArray*)ba);
        else
        return thisKey()->signSHA((QByteArray*)bs);
    } catch(const CryptoPP::Exception& e) {
        context()->throwError(e.what());
    }
    return ByteArray();
}

ByteArray
RSAPrivateKeyPrototype::signSHA256(QScriptValue msg) const
{
    ByteArray   *ba = qscriptvalue_cast<ByteArray*>(msg.data());
    ByteString  *bs = 0;
    if (!ba)
        bs = qscriptvalue_cast<ByteString*>(msg.data());
    if (!ba && !bs) {
        context()->throwError("RSAPrivateKey.signSHA: arg(0) "\
                                  "not instance of ByteArray or ByteString");
        return QByteArray();
    }
    try {
    if (ba)
        return thisKey()->signSHA256((QByteArray*)ba);
        else
        return thisKey()->signSHA256((QByteArray*)bs);
    } catch(const CryptoPP::Exception& e) {
        context()->throwError(e.what());
    }
    return ByteArray();
}

bool
RSAPrivateKeyPrototype::verifySHA(QScriptValue msg, QScriptValue sig) const
{
    ByteArray   *ba = qscriptvalue_cast<ByteArray*>(msg.data());
    ByteString  *bs = 0;
    ByteArray   *bsig = qscriptvalue_cast<ByteArray*>(sig.data());;

    if (!ba)
        bs = qscriptvalue_cast<ByteString*>(msg.data());

    if (ba==0 && bs == 0) {
        context()->throwError("RSAPrivateKey.verifySHA: arg(0) "\
                                  "not instance of ByteArray or ByteString");
        return false;
    }

    if (!bsig) {
        context()->throwError("RSAPrivateKey.verifySHA: arg(1) "\
                                  "not instance of ByteArray");
        return false;
    }

    try {
    if (ba)
        return thisKey()->verifySHA((QByteArray*)ba, (QByteArray*)bsig);
        else
        return thisKey()->verifySHA((QByteArray*)bs, (QByteArray*)bsig);
    } catch(const CryptoPP::Exception& e) {
        context()->throwError(e.what());
    }
    return false;
}

bool
RSAPrivateKeyPrototype::verifySHA256(QScriptValue msg, QScriptValue sig) const
{
    ByteArray   *ba = qscriptvalue_cast<ByteArray*>(msg.data());
    ByteString  *bs = 0;
    ByteArray   *bsig = qscriptvalue_cast<ByteArray*>(sig.data());;

    if (!ba)
        bs = qscriptvalue_cast<ByteString*>(msg.data());

    if (ba==0 && bs == 0) {
        context()->throwError("RSAPrivateKey.verifySHA256: arg(0) "\
                                  "not instance of ByteArray or ByteString");
        return false;
    }

    if (!bsig) {
        context()->throwError("RSAPrivateKey.verifySHA256: arg(1) "\
                                  "not instance of ByteArray");
        return false;
    }

    try {
    if (ba)
        return thisKey()->verifySHA256((QByteArray*)ba, (QByteArray*)bsig);
        else
        return thisKey()->verifySHA256((QByteArray*)bs, (QByteArray*)bsig);
    } catch(const CryptoPP::Exception& e) {
        context()->throwError(e.what());
    }
    return false;
}

RSAPrivateKey *
RSAPrivateKeyPrototype::thisKey() const
{
    RSAPrivateKey *p = qscriptvalue_cast<RSAPrivateKey*>(thisObject());
    if (!p) {
        qFatal("Programmatic error ProcessPrototype::thisIODevice");
    }

    return p;
}

#ifndef RSAPRIVATEKEYPROTOTYPE_H
#define RSAPRIVATEKEYPROTOTYPE_H

#include <QtCore/QObject>

#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

#include "binary.h"
#include "rsaprivatekey.h"

#ifdef QT_PLUGIN
#define EXPORT Q_DECL_EXPORT
#else
#define EXPORT
#endif

class EXPORT RSAPrivateKeyPrototype : public QObject, public QScriptable
{
    Q_OBJECT
public:
    RSAPrivateKeyPrototype(QObject *parent = 0);
    ~RSAPrivateKeyPrototype();

public slots:
    ByteArray encryptOAEP(QScriptValue msg) const;
    ByteArray decryptOAEP(QScriptValue msg) const;
    ByteArray encryptPKCS(QScriptValue msg) const;
    ByteArray decryptPKCS(QScriptValue msg) const;

    ByteArray signSHA(QScriptValue msg) const;
    ByteArray signSHA256(QScriptValue msg) const;

    bool verifySHA(QScriptValue msg, QScriptValue sig) const;
    bool verifySHA256(QScriptValue msg, QScriptValue sig) const;


private:
    virtual RSAPrivateKey*   thisKey() const;
};

#endif //STREAMPROTOTYPE_H

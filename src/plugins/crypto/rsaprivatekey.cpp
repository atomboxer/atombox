#include "rsaprivatekey.h"

#include <core/bytearrayclass.h>
#include <core/bytestringclass.h>

#include <cryptopp/hex.h>
#include <cryptopp/sha.h>
#include <cryptopp/base64.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/osrng.h>
#include <cryptopp/rsa.h>
#include <cryptopp/pem.h>
#ifdef __TANDEM
#include <cryptopp/pkcspad.cpp>
#endif
#include <string>
#include <iostream>
#include <sstream>

Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)

//#define RSA_DEBUG 1

typedef CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA256>::Signer RSASSA_PKCS1v15_SHA256_Signer;
typedef CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA256>::Verifier RSASSA_PKCS1v15_SHA256_Verifier;


static std::string IntegerToString(const CryptoPP::Integer& n)
{
    // Send the CryptoPP::Integer to the output stream string
    std::ostringstream os;
    os <<std::hex<<n;
    // Convert the stream to std::string
    return os.str();
}

static ByteArray IntegerToBinary(const CryptoPP::Integer& n)
{
    // Send the CryptoPP::Integer to the output stream string
    std::ostringstream os;
    os << std::hex<<n;
    // Convert the stream to std::string
    return ByteArray::fromHex(os.str().data());
}

RSAPrivateKey::RSAPrivateKey(CryptoPP::InvertibleRSAFunction key, QObject *parent) :
              QObject(parent)
{
    key_ = key;

#if 0
    qDebug()<<"RSAPrivateKey::RSAPrivateKey";
    const CryptoPP::Integer n = key_.GetModulus();
    const CryptoPP::Integer p = key_.GetPrime1();
    const CryptoPP::Integer q = key_.GetPrime2();
    const CryptoPP::Integer d = key_.GetPrivateExponent();
    const CryptoPP::Integer e = key_.GetPublicExponent();

    qDebug() << "RSA Parameters:" << endl;
    qDebug() << " n: " << IntegerToString(n).data();
    qDebug() << " p: " << IntegerToString(p).data();
    qDebug() << " q: " << IntegerToString(q).data();
    qDebug() << " d: " << IntegerToString(d).data();
    qDebug() << " e: " << IntegerToString(e).data();
#endif

}

RSAPrivateKey::~RSAPrivateKey()
{
}

//public key
ByteArray
RSAPrivateKey::getPublicExponent() const  /*e*/
{
    return IntegerToBinary(key_.GetPublicExponent());
}

ByteArray
RSAPrivateKey::getModulus() const          /*n*/
{
    return IntegerToBinary(key_.GetModulus());
}

//private key
ByteArray
RSAPrivateKey::getPrivateExponent() const  /*d*/
{
    return IntegerToBinary(key_.GetPrivateExponent());
}

ByteArray
RSAPrivateKey::getPrime1() const           /*p*/
{
    return IntegerToBinary(key_.GetPrime1());
}

ByteArray
RSAPrivateKey::getPrime2() const           /*q*/
{
    return IntegerToBinary(key_.GetPrime2());
}

ByteArray
RSAPrivateKey::toPEM() const
{
    std::string rsaPrivateMaterial;
    CryptoPP::StringSink sink(rsaPrivateMaterial);
    PEM_Save(sink.Ref(), key_);
    return QByteArray(rsaPrivateMaterial.data(), rsaPrivateMaterial.size());
}

ByteArray
RSAPrivateKey::toDER() const
{
    std::string rsaPrivateMaterial;
    CryptoPP::StringSink sink(rsaPrivateMaterial);
    CryptoPP::ByteQueue queue;
    key_.DEREncodePrivateKey(queue);
    queue.CopyTo(sink);
    sink.MessageEnd();
    return QByteArray(rsaPrivateMaterial.data(), rsaPrivateMaterial.size());
}

bool
RSAPrivateKey::isValid() const
{
    CryptoPP::AutoSeededRandomPool rnd;
    return key_.Validate(rnd,3);
}

ByteArray
RSAPrivateKey::encryptOAEP(QByteArray *ba) const
{
#ifdef RSA_DEBUG
    qDebug()<<"RSAPrivateKey::encryptOAEP"<<ba->toHex();
#endif
    CryptoPP::AutoSeededRandomPool rng;
    CryptoPP::RSAES_OAEP_SHA_Encryptor e(key_);
    std::string data((char*)ba->data(), ba->length());
    std::string cipher;

    CryptoPP::StringSource ss1( data, true,
        new CryptoPP::PK_EncryptorFilter( rng, e,
            new CryptoPP::StringSink( cipher )
        ) // PK_EncryptorFilter
    ); // StringSource
    return ByteArray(cipher.data(), cipher.size());
}

ByteArray
RSAPrivateKey::decryptOAEP(QByteArray *ba) const
{
#ifdef RSA_DEBUG
    qDebug()<<"RSAPrivateKey::decryptOAEP"<<ba->toHex();
#endif
    CryptoPP::AutoSeededRandomPool rng;
    CryptoPP::RSAES_OAEP_SHA_Decryptor e(key_);
    std::string data((char*)ba->data(), ba->length());
    std::string cipher;

    CryptoPP::StringSource ss1( data, true,
        new CryptoPP::PK_DecryptorFilter( rng, e,
            new CryptoPP::StringSink( cipher )
        ) // PK_EncryptorFilter
    ); // StringSource
    return ByteArray(cipher.data(), cipher.size());
}


ByteArray
RSAPrivateKey::encryptPKCS(QByteArray *ba) const
{
#ifdef RSA_DEBUG
    qDebug()<<"RSAPrivateKey::encryptPKCS"<<ba->toHex();
#endif
    CryptoPP::AutoSeededRandomPool rng;
    CryptoPP::RSAES_PKCS1v15_Encryptor e(key_);
    std::string data((char*)ba->data(), ba->length());
    std::string cipher;

    CryptoPP::StringSource ss1( data, true,
        new CryptoPP::PK_EncryptorFilter( rng, e,
            new CryptoPP::StringSink( cipher )
        ) // PK_EncryptorFilter
    ); // StringSource
    return ByteArray(cipher.data(), cipher.size());
}

ByteArray
RSAPrivateKey::decryptPKCS(QByteArray *ba) const
{
#ifdef RSA_DEBUG
    qDebug()<<"RSAPrivateKey::decryptPKCS"<<ba->toHex();
#endif
    CryptoPP::AutoSeededRandomPool rng;
    CryptoPP::RSAES_PKCS1v15_Decryptor e(key_);
    std::string data((char*)ba->data(), ba->length());
    std::string cipher;

    CryptoPP::StringSource ss1( data, true,
        new CryptoPP::PK_DecryptorFilter( rng, e,
            new CryptoPP::StringSink( cipher )
        ) // PK_EncryptorFilter
    ); // StringSource
    return ByteArray(cipher.data(), cipher.size());
}

ByteArray
RSAPrivateKey::signSHA(QByteArray *ba) const
{
#ifdef RSA_DEBUG
    qDebug()<<"RSAPrivateKey::signSHA"<<ba->toHex();
#endif
    CryptoPP::RSASSA_PKCS1v15_SHA_Signer signer(key_);
    // Create signature space
    size_t length = signer.MaxSignatureLength();
    CryptoPP::SecByteBlock signature(length);

    CryptoPP::AutoSeededRandomPool rng;
    std::string data((char*)ba->data(), ba->length());

    // Sign message
    length = signer.SignMessage(rng, (const byte*) data.c_str(),
            data.length(), signature);

    // Resize now we know the true size of the signature
    signature.resize(length);

    return ByteArray((const char*)signature.data(), signature.size());
}

bool
RSAPrivateKey::verifySHA(QByteArray *ba, QByteArray *sig) const
{
#ifdef RSA_DEBUG
    qDebug()<<"RSAPrivateKey::verifySHA data"<<ba->toHex()
        <<" signature:"<<sig->toHex();
#endif
    CryptoPP::RSASSA_PKCS1v15_SHA_Verifier verifier(key_);
    CryptoPP::SecByteBlock signature((byte*)sig->data(), sig->length());

    CryptoPP::AutoSeededRandomPool rng;
    std::string data((char*)ba->data(), ba->length());

    return verifier.VerifyMessage((const byte*) data.c_str(),
            data.length(), signature, signature.size());
}

ByteArray
RSAPrivateKey::signSHA256(QByteArray *ba) const
{
#ifdef RSA_DEBUG
    qDebug()<<"RSAPrivateKey::signSHA256"<<ba->toHex();
#endif
    RSASSA_PKCS1v15_SHA256_Signer signer(key_);
    // Create signature space
    size_t length = signer.MaxSignatureLength();
    CryptoPP::SecByteBlock signature(length);

    CryptoPP::AutoSeededRandomPool rng;
    std::string data((char*)ba->data(), ba->length());

    // Sign message
    length = signer.SignMessage(rng, (const byte*) data.c_str(),
            data.length(), signature);

    // Resize now we know the true size of the signature
    signature.resize(length);

    return ByteArray((const char*)signature.data(), signature.size());
}


bool
RSAPrivateKey::verifySHA256(QByteArray *ba, QByteArray *sig) const
{
#ifdef RSA_DEBUG
    qDebug()<<"RSAPrivateKey::verifySHA data"<<ba->toHex()
        <<" signature:"<<sig->toHex();
#endif
    RSASSA_PKCS1v15_SHA256_Verifier verifier(key_);
    CryptoPP::SecByteBlock signature((byte*)sig->data(), sig->length());

    CryptoPP::AutoSeededRandomPool rng;
    std::string data((char*)ba->data(), ba->length());

    return verifier.VerifyMessage((const byte*) data.c_str(),
            data.length(), signature, signature.size());
}

////////////////////////////////////////////////////////////////////////////////

QScriptValue
__crypto_rsa_generatePrivateKey(QScriptContext *ctx, QScriptEngine *eng)
{
    CryptoPP::AutoSeededRandomPool rnd;
    CryptoPP::RSA::PrivateKey rsaPrivate;

    if (ctx->argumentCount() < 1) {
        ctx->throwError("generateKeyPair modulus argument required");
        return QScriptValue();
    }

    int modulus = ctx->argument(0).toInt32();
	rsaPrivate.Initialize(rnd, modulus, 65537);

    return eng->newQObject(new RSAPrivateKey(rsaPrivate), QScriptEngine::ScriptOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| ,QScriptEngine::ExcludeSuperClassMethods(*/
                           /*|  QScriptEngine::ExcludeSuperClassProperties*/);
}

QScriptValue
__crypto_rsa_createPrivateKeyFromComponents(QScriptContext *ctx, QScriptEngine *eng) /*n, p, q, d, e*/
{
    CryptoPP::AutoSeededRandomPool rnd;
	CryptoPP::RSA::PrivateKey rsaPrivate;

    if (ctx->argumentCount() != 5) {
        ctx->throwError("createPrivateKeyFromComponents invalid argument count (n,p,q,d,e)");
        return QScriptValue();
    }

    ByteArray  *bn = qscriptvalue_cast<ByteArray*>(ctx->argument(0).data());
    ByteArray  *bp = qscriptvalue_cast<ByteArray*>(ctx->argument(1).data());
    ByteArray  *bq = qscriptvalue_cast<ByteArray*>(ctx->argument(2).data());
    ByteArray  *bd = qscriptvalue_cast<ByteArray*>(ctx->argument(3).data());
    ByteArray  *be = qscriptvalue_cast<ByteArray*>(ctx->argument(4).data());

    if (!(bn && bp && bq &&  bd && be)) {
        ctx->throwError("createPrivateKeyFromComponents invalid arguments, mustbe instanceof  ByteArray");
        return QScriptValue();
    }

    const CryptoPP::Integer n ((std::string(bn->toHex())+"h").c_str());
    const CryptoPP::Integer p ((std::string(bp->toHex())+"h").c_str());
    const CryptoPP::Integer q ((std::string(bq->toHex())+"h").c_str());
    const CryptoPP::Integer d ((std::string(bd->toHex())+"h").c_str());
    const CryptoPP::Integer e ((std::string(be->toHex())+"h").c_str());

    CryptoPP::InvertibleRSAFunction params;
    params.SetModulus(n);
    params.SetPrime1(p);
    params.SetPrime2(q);
    params.SetPrivateExponent(d);
    params.SetPublicExponent(e);

    return eng->newQObject(new RSAPrivateKey(params), QScriptEngine::ScriptOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| ,QScriptEngine::ExcludeSuperClassMethods(*/
                           /*|  QScriptEngine::ExcludeSuperClassProperties*/);
}


QScriptValue __crypto_rsa_createPrivateKeyFromBER(QScriptContext *ctx, QScriptEngine *eng)
{
    if (ctx->argumentCount() != 1) {
        ctx->throwError("__crypto_rsa_createPrivateKeyFromPEM invalid argument count");
        return QScriptValue();
    }

    ByteArray  *ba = qscriptvalue_cast<ByteArray*>(ctx->argument(0).data());
    if (!ba) {
        ctx->throwError("__crypto_rsa_createPrivateKeyFromPEM invalid argument, mustbe instanceof ByteArray");
        return QScriptValue();
    }

    CryptoPP::RSA::PrivateKey rsaPrivate;
    CryptoPP::ByteQueue queue;
    std::string skey(ba->toHex().data());
    CryptoPP::StringSource stringSource(skey,true, new CryptoPP::HexDecoder(new CryptoPP::Redirector(queue)));

    rsaPrivate.BERDecodePrivateKey(queue, false /*paramsPresent*/, queue.MaxRetrievable());



    return eng->newQObject(new RSAPrivateKey(rsaPrivate), QScriptEngine::ScriptOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| ,QScriptEngine::ExcludeSuperClassMethods(*/
                           /*|  QScriptEngine::ExcludeSuperClassProperties*/);

}

QScriptValue __crypto_rsa_createPrivateKeyFromPEM(QScriptContext *ctx, QScriptEngine *eng)
{
    if (ctx->argumentCount() != 1) {
        ctx->throwError("__crypto_rsa_createPrivateKeyFromPEM invalid argument count");
        return QScriptValue();
    }

    ByteArray  *ba = qscriptvalue_cast<ByteArray*>(ctx->argument(0).data());
    if (!ba) {
        ctx->throwError("__crypto_rsa_createPrivateKeyFromDER invalid argument, mustbe instanceof ByteArray");
        return QScriptValue();
    }
    try {
    CryptoPP::InvertibleRSAFunction params;
    std::string rsaPrivateMaterial(ba->constData());
    CryptoPP::StringSource stringSource(rsaPrivateMaterial,true);
    CryptoPP::PEM_Load(stringSource.Ref(), params);
    CryptoPP::RSA::PrivateKey rsaPrivate(params);
    return eng->newQObject(new RSAPrivateKey(rsaPrivate), QScriptEngine::ScriptOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| ,QScriptEngine::ExcludeSuperClassMethods(*/
                           /*|  QScriptEngine::ExcludeSuperClassProperties*/);

    } catch(const CryptoPP::Exception& e) {
        ctx->throwError(e.what());
        return QScriptValue();
    }
    return QScriptValue();
}


#include <QtCore/QDebug>
#include <QtCore/QCoreApplication>

#include <QtScript/QScriptEngine>

#include <openssl/ssl.h>

#include "cryptoplugin.h"

#include "cipher.h"
#include "crypto.h"
#include "decipher.h"
#include "hash.h"
#include "rsaprivatekey.h"
#include "rsaprivatekeyprototype.h"
#include "rsapublickey.h"
#include "rsapublickeyprototype.h"

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

#if defined(AB_USE_SCRIPT_CLASSIC)
Q_EXPORT_PLUGIN2( abscriptcrypto_compat, CryptoPlugin )
#else
Q_EXPORT_PLUGIN2( abscriptcrypto, CryptoPlugin )
#endif

Q_DECLARE_METATYPE(RSAPrivateKey*)
Q_DECLARE_METATYPE(RSAPublicKey*)


CryptoPlugin::CryptoPlugin( QObject *parent )
    : QScriptExtensionPlugin( parent )
{
}

CryptoPlugin::~CryptoPlugin()
{
}

void
CryptoPlugin::initialize( const QString &key, QScriptEngine *engine )
{
    QScriptValue globalObject = engine->globalObject();
#if defined(AB_USE_SCRIPT_CLASSIC)
    if ( key == QString("crypto_compat") ) {
#else
    if ( key == QString("crypto") ) {
#endif
#ifdef AB_STATIC_PLUGINS
	Q_INIT_RESOURCE(crypto);
#endif
	Q_INIT_RESOURCE(jscryptofiles);

	OpenSSL_add_all_algorithms();

	QScriptValue __internal__;

	__internal__ = engine->globalObject().property("__internal__");
	if (!__internal__.isObject()) {

	    __internal__ = engine->newObject();
	    engine->globalObject()
		.setProperty("__internal__",__internal__);
	}

	//
	//  __internal__.Cipher
	//
	__internal__.setProperty("Cipher", engine->newFunction(Cipher_ctor));

	//
	//  __internal__.Decipher
	//
	__internal__.setProperty("Decipher", engine->newFunction(Decipher_ctor));

	//
	//  __internal__.Hash
	//
	__internal__.setProperty("Hash", engine->newFunction(Hash_ctor));

	//
	//  __internal__.checkBlockParity
	//
	__internal__.setProperty("checkBlockParity",
				 engine->newFunction(__crypto_checkBlockParity));

	//
	//  __internal__.fixBlockParity
	//
	__internal__.setProperty("fixBlockParity",
				 engine->newFunction(__crypto_fixBlockParity));

	//
	//  __internal__.generateRandomByteArray
	//
	__internal__.setProperty("generateRandomByteArray",
				 engine->newFunction(__crypto_generateRandomByteArray));

	//
	//  __internal__.generateSingleKey
	//
	__internal__.setProperty("generateSingleKey",
				 engine->newFunction(__crypto_generateSingleKey));

	//
	//  __internal__.generateDoubleKey
	//
	__internal__.setProperty("generateDoubleKey",
				 engine->newFunction(__crypto_generateDoubleKey));

	//
	//  __internal__.generateTripleKey
	//
	__internal__.setProperty("generateTripleKey",
				 engine->newFunction(__crypto_generateTripleKey));

	//
	//  __internal__.Hash
	//

    engine->setDefaultPrototype(qMetaTypeId<RSAPrivateKey*>(),
                          engine->newQObject(new RSAPrivateKeyPrototype(this)));

    engine->setDefaultPrototype(qMetaTypeId<RSAPublicKey*>(),
                          engine->newQObject(new RSAPublicKeyPrototype(this)));

	__internal__.setProperty("__crypto_rsa_generatePrivateKey",
				 engine->newFunction(__crypto_rsa_generatePrivateKey));


	__internal__.setProperty("__crypto_rsa_createPrivateKeyFromComponents",
			  engine->newFunction(__crypto_rsa_createPrivateKeyFromComponents));

	__internal__.setProperty("__crypto_rsa_createPrivateKeyFromPEM",
			  engine->newFunction(__crypto_rsa_createPrivateKeyFromPEM));

	__internal__.setProperty("__crypto_rsa_createPrivateKeyFromBER",
			  engine->newFunction(__crypto_rsa_createPrivateKeyFromBER));

  	__internal__.setProperty("__crypto_rsa_createPublicKeyFromPrivateKey",
			  engine->newFunction(__crypto_rsa_createPublicKeyFromPrivateKey));

  	__internal__.setProperty("__crypto_rsa_createPublicKeyFromComponents",
			  engine->newFunction(__crypto_rsa_createPublicKeyFromComponents));

	__internal__.setProperty("__crypto_rsa_createPublicKeyFromPEM",
			  engine->newFunction(__crypto_rsa_createPublicKeyFromPEM));

	__internal__.setProperty("__crypto_rsa_createPublicKeyFromBER",
			  engine->newFunction(__crypto_rsa_createPublicKeyFromBER));

    }
}

QStringList
CryptoPlugin::keys() const
{
    QStringList keys;
#if defined(AB_USE_SCRIPT_CLASSIC)
    keys << QString("crypto_compat");
#else
    keys << QString("crypto");
#endif
    return keys;
}

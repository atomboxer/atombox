#include "rsapublickey.h"
#include "rsaprivatekey.h"
#include "rsapublickeyprototype.h"

#include <QtCore/QDebug>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(RSAPublicKey*)
Q_DECLARE_METATYPE(RSAPrivateKey*)
Q_DECLARE_METATYPE(ByteString*)
Q_DECLARE_METATYPE(ByteArray*)

RSAPublicKeyPrototype::RSAPublicKeyPrototype(QObject *parent)
                : QObject(parent)
{
}

RSAPublicKeyPrototype::~RSAPublicKeyPrototype()
{
}

ByteArray
RSAPublicKeyPrototype::encryptOAEP(QScriptValue msg) const
{
    ByteArray   *ba = qscriptvalue_cast<ByteArray*>(msg.data());
    ByteString  *bs = 0;
    if (!ba)
        bs = qscriptvalue_cast<ByteString*>(msg.data());
    if (!ba && !bs) {
       context()->throwError("RSAPublicKey.encryptOAEP: arg(0) "\
                                  "not instance of ByteArray or ByteString");
        return QByteArray();
    }
    if (ba)
        return thisKey()->encryptOAEP((QByteArray*)ba);
    else
        return thisKey()->encryptOAEP((QByteArray*)bs);

    return ByteArray();
}

ByteArray
RSAPublicKeyPrototype::encryptPKCS(QScriptValue msg) const
{
    ByteArray   *ba = qscriptvalue_cast<ByteArray*>(msg.data());
    ByteString  *bs = 0;
    if (!ba)
        bs = qscriptvalue_cast<ByteString*>(msg.data());
    if (!ba && !bs) {
        context()->throwError("RSAPublicKey.encryptPKCS: arg(0) "\
                                  "not instance of ByteArray or ByteString");
        return QByteArray();
    }
    if (ba)
        return thisKey()->encryptPKCS((QByteArray*)ba);
    else
        return thisKey()->encryptPKCS((QByteArray*)bs);

    return ByteArray();
}

bool
RSAPublicKeyPrototype::verifySHA(QScriptValue msg, QScriptValue sig) const
{
    ByteArray   *ba = qscriptvalue_cast<ByteArray*>(msg.data());
    ByteString  *bs = 0;
    ByteArray   *bsig = qscriptvalue_cast<ByteArray*>(sig.data());;

    if (!ba)
        bs = qscriptvalue_cast<ByteString*>(msg.data());

    if (ba==0 && bs == 0) {
        context()->throwError("RSAPublicKey.verifySHA: arg(0) "\
                                  "not instance of ByteArray or ByteString");
        return false;
    }

    if (!bsig) {
        context()->throwError("RSAPublicKey.verifySHA: arg(1) "\
                                  "not instance of ByteArray");
        return false;
    }

    try {
    if (ba)
        return thisKey()->verifySHA((QByteArray*)ba, (QByteArray*)bsig);
    else
        return thisKey()->verifySHA((QByteArray*)bs, (QByteArray*)bsig);
    } catch(const CryptoPP::Exception& e) {
        context()->throwError(e.what());
    }
    return false;
}

bool
RSAPublicKeyPrototype::verifySHA256(QScriptValue msg, QScriptValue sig) const
{
    ByteArray   *ba = qscriptvalue_cast<ByteArray*>(msg.data());
    ByteString  *bs = 0;
    ByteArray   *bsig = qscriptvalue_cast<ByteArray*>(sig.data());;

    if (!ba)
        bs = qscriptvalue_cast<ByteString*>(msg.data());

    if (ba==0 && bs == 0) {
        context()->throwError("RSAPublicKey.verifySHA256: arg(0) "\
                                  "not instance of ByteArray or ByteString");
        return false;
    }

    if (!bsig) {
        context()->throwError("RSAPublicKey.verifySHA256: arg(1) "\
                                  "not instance of ByteArray");
        return false;
    }

    try {
        if (ba)
            return thisKey()->verifySHA256((QByteArray*)ba, (QByteArray*)bsig);
        else
            return thisKey()->verifySHA256((QByteArray*)bs, (QByteArray*)bsig);
    } catch(const CryptoPP::Exception& e) {
        context()->throwError(e.what());
    }
    return false;
}

RSAPublicKey *
RSAPublicKeyPrototype::thisKey() const
{
    RSAPublicKey *p = qscriptvalue_cast<RSAPublicKey*>(thisObject());
    if (!p) {
        qFatal("Programmatic error ProcessPrototype::thisIODevice");
    }

    return p;
}

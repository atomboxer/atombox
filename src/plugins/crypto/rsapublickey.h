#ifndef RSAPUBLICKEY_H
#define RSAPUBLICKEY_H

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptContext>
#include <QtScript/QScriptValue>

#include <QtCore/QObject>
#include <QtCore/QString>

#include <core/binary.h>

#include <cryptopp/rsa.h>

class RSAPublicKey : public QObject
{
    Q_OBJECT
public:
    RSAPublicKey(CryptoPP::RSA::PublicKey key, QObject *parent=NULL);
    ~RSAPublicKey();

public:

//public key
    Q_INVOKABLE ByteArray getPublicExponent() const;  /*e*/
    Q_INVOKABLE ByteArray getModulus() const;         /*n*/

    Q_INVOKABLE ByteArray toPEM() const;
    Q_INVOKABLE ByteArray toDER() const;
    Q_INVOKABLE bool      isValid() const;
    Q_INVOKABLE bool      isPublicKey() const { return true; }
    Q_INVOKABLE bool      isPrivateKey() const { return false; }
    ByteArray encryptOAEP(QByteArray *ba) const;
    ByteArray encryptPKCS(QByteArray *ba) const;
    bool verifySHA(QByteArray *ba, QByteArray *sig) const;
    bool verifySHA256(QByteArray *ba, QByteArray *sig) const;

private:
    CryptoPP::RSA::PublicKey key_;
};

QScriptValue __crypto_rsa_createPublicKeyFromPrivateKey(QScriptContext *, QScriptEngine *);
QScriptValue __crypto_rsa_createPublicKeyFromComponents(QScriptContext *, QScriptEngine *);
QScriptValue __crypto_rsa_createPublicKeyFromPEM(QScriptContext *, QScriptEngine *);
QScriptValue __crypto_rsa_createPublicKeyFromBER(QScriptContext *, QScriptEngine *);

#endif

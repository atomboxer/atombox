#ifndef RSAPPUBLICKEYPROTOTYPE_H
#define RSAPUBLICKEYPROTOTYPE_H

#include <QtCore/QObject>

#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

#include "binary.h"
#include "rsapublickey.h"

#ifdef QT_PLUGIN
#define EXPORT Q_DECL_EXPORT
#else
#define EXPORT
#endif

class EXPORT RSAPublicKeyPrototype : public QObject, public QScriptable
{
    Q_OBJECT
public:
    RSAPublicKeyPrototype(QObject *parent = 0);
    ~RSAPublicKeyPrototype();

public slots:
    ByteArray encryptOAEP(QScriptValue msg) const;
    ByteArray encryptPKCS(QScriptValue msg) const;

    bool verifySHA(QScriptValue msg, QScriptValue sig) const;
    bool verifySHA256(QScriptValue msg, QScriptValue sig) const;


private:
    virtual RSAPublicKey*   thisKey() const;
};

#endif //STREAMPROTOTYPE_H

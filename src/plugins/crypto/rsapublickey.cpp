#include "rsapublickey.h"
#include "rsaprivatekey.h"
#include <core/bytearrayclass.h>
#include <core/bytestringclass.h>


#include <cryptopp/hex.h>


#include <cryptopp/rsa.h>
using CryptoPP::RSA;

#include <cryptopp/sha.h>
using CryptoPP::SHA;
using CryptoPP::SHA256;

#include <cryptopp/base64.h>
using CryptoPP::Base64Encoder;
using CryptoPP::Base64Decoder;

#include <cryptopp/cryptlib.h>
using CryptoPP::PrivateKey;
using CryptoPP::PublicKey;
using CryptoPP::BufferedTransformation;

#include <cryptopp/osrng.h>
using CryptoPP::AutoSeededRandomPool;

#include <cryptopp/pem.h>

#include <string>
#include <iostream>
#include <sstream>

Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)
Q_DECLARE_METATYPE(RSAPrivateKey*)


//#define RSA_DEBUG 1

typedef CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA256>::Signer RSASSA_PKCS1v15_SHA256_Signer;
typedef CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA256>::Verifier RSASSA_PKCS1v15_SHA256_Verifier;

static std::string IntegerToString(const CryptoPP::Integer& n)
{
    // Send the CryptoPP::Integer to the output stream string
    std::ostringstream os;
    os <<std::hex<<n;
    // Convert the stream to std::string
    return os.str();
}

static ByteArray IntegerToBinary(const CryptoPP::Integer& n)
{
    // Send the CryptoPP::Integer to the output stream string
    std::ostringstream os;
    os << std::hex<<n;
    // Convert the stream to std::string
    return ByteArray::fromHex(os.str().data());
}

RSAPublicKey::RSAPublicKey(RSA::PublicKey key, QObject *parent) :
              QObject(parent)
{
    key_ = key;

#if 0
    qDebug()<<"RSAPublicKey::RSAPublicKey";
    const CryptoPP::Integer n = key_.GetModulus();
    const CryptoPP::Integer e = key_.GetPublicExponent();

    qDebug() << "RSA Parameters:" << endl;
    qDebug() << " n: " << IntegerToString(n).data();
    qDebug() << " e: " << IntegerToString(e).data();
#endif

}

RSAPublicKey::~RSAPublicKey()
{
}

//public key
ByteArray
RSAPublicKey::getPublicExponent() const  /*e*/
{
    return IntegerToBinary(key_.GetPublicExponent());
}

ByteArray
RSAPublicKey::getModulus() const          /*n*/
{
    return IntegerToBinary(key_.GetModulus());
}

ByteArray
RSAPublicKey::toPEM() const
{
    std::string rsaPublicMaterial;
    CryptoPP::StringSink sink(rsaPublicMaterial);
    PEM_Save(sink.Ref(), key_);
    return QByteArray(rsaPublicMaterial.data(), rsaPublicMaterial.size());
}

ByteArray
RSAPublicKey::toDER() const
{
    std::string rsaPublicMaterial;
    CryptoPP::StringSink sink(rsaPublicMaterial);
    CryptoPP::ByteQueue queue;
    key_.DEREncodePublicKey(queue);
    queue.CopyTo(sink);
    sink.MessageEnd();
    return QByteArray(rsaPublicMaterial.data(), rsaPublicMaterial.size());
}

bool
RSAPublicKey::isValid() const
{
    AutoSeededRandomPool rnd;
    return key_.Validate(rnd,3);
}

ByteArray
RSAPublicKey::encryptOAEP(QByteArray *ba) const
{
#ifdef RSA_DEBUG
    qDebug()<<"RSAPublicKey::encryptOAEP"<<ba->toHex();
#endif
    AutoSeededRandomPool rng;
    CryptoPP::RSAES_OAEP_SHA_Encryptor e(key_);
    std::string data((char*)ba->data(), ba->length());
    std::string cipher;

    CryptoPP::StringSource ss1( data, true,
        new CryptoPP::PK_EncryptorFilter( rng, e,
            new CryptoPP::StringSink( cipher )
        ) // PK_EncryptorFilter
    ); // StringSource
    return ByteArray(cipher.data(), cipher.size());
}

ByteArray
RSAPublicKey::encryptPKCS(QByteArray *ba) const
{
#ifdef RSA_DEBUG
    qDebug()<<"RSAPublicKey::encryptPKCS"<<ba->toHex();
#endif
    AutoSeededRandomPool rng;
    CryptoPP::RSAES_PKCS1v15_Encryptor e(key_);
    std::string data((char*)ba->data(), ba->length());
    std::string cipher;

    CryptoPP::StringSource ss1( data, true,
        new CryptoPP::PK_EncryptorFilter( rng, e,
            new CryptoPP::StringSink( cipher )
        ) // PK_EncryptorFilter
    ); // StringSource
    return ByteArray(cipher.data(), cipher.size());
}

bool
RSAPublicKey::verifySHA(QByteArray *ba, QByteArray *sig) const
{
#ifdef RSA_DEBUG
    qDebug()<<"RSAPublicKey::verifySHA data"<<ba->toHex()
        <<" signature:"<<sig->toHex();
#endif
    CryptoPP::RSASSA_PKCS1v15_SHA_Verifier verifier(key_);
    CryptoPP::SecByteBlock signature((byte*)sig->data(), sig->length());

    AutoSeededRandomPool rng;
    std::string data((char*)ba->data(), ba->length());

    return verifier.VerifyMessage((const byte*) data.c_str(),
            data.length(), signature, signature.size());
}

bool
RSAPublicKey::verifySHA256(QByteArray *ba, QByteArray *sig) const
{
#ifdef RSA_DEBUG
    qDebug()<<"RSAPublicKey::verifySHA data"<<ba->toHex()
        <<" signature:"<<sig->toHex();
#endif
    RSASSA_PKCS1v15_SHA256_Verifier verifier(key_);
    CryptoPP::SecByteBlock signature((byte*)sig->data(), sig->length());

    AutoSeededRandomPool rng;
    std::string data((char*)ba->data(), ba->length());

    return verifier.VerifyMessage((const byte*) data.c_str(),
            data.length(), signature, signature.size());
}

////////////////////////////////////////////////////////////////////////////////


QScriptValue
__crypto_rsa_createPublicKeyFromPrivateKey(QScriptContext *ctx, QScriptEngine *eng)
{

    if (ctx->argumentCount() != 1) {
        ctx->throwError("createPublicKeyFromPrivateKey invalid argument");
        return QScriptValue();
    }

    RSAPrivateKey  *sk = qscriptvalue_cast<RSAPrivateKey*>(ctx->argument(0));

    if (!sk) {
        ctx->throwError("createPublicKeyFromPrivateKey invalid argument not instanceof PrivateKey");
        return QScriptValue();
    }

    CryptoPP::InvertibleRSAFunction privateKey(sk->getRSAKey());
    CryptoPP::RSAFunction publicKey(privateKey);
    return eng->newQObject(new RSAPublicKey(publicKey), QScriptEngine::ScriptOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| ,QScriptEngine::ExcludeSuperClassMethods(*/
                           /*|  QScriptEngine::ExcludeSuperClassProperties*/);
}

QScriptValue
__crypto_rsa_createPublicKeyFromComponents(QScriptContext *ctx, QScriptEngine *eng) /*n e*/
{
    CryptoPP::AutoSeededRandomPool rnd;
	RSA::PublicKey rsaPublic;

    if (ctx->argumentCount() != 2) {
        ctx->throwError("createPublicKeyFromComponents invalid argument count (n, e)");
        return QScriptValue();
    }

    ByteArray  *bn = qscriptvalue_cast<ByteArray*>(ctx->argument(0).data());
    ByteArray  *be = qscriptvalue_cast<ByteArray*>(ctx->argument(4).data());

    if (!(bn && be)) {
        ctx->throwError("createPublicKeyFromComponents invalid arguments, mustbe instanceof  ByteArray");
        return QScriptValue();
    }

    const CryptoPP::Integer n ((std::string(bn->toHex())+"h").c_str());
    const CryptoPP::Integer e ((std::string(be->toHex())+"h").c_str());

    CryptoPP::RSAFunction params;
    params.SetModulus(n);
    params.SetPublicExponent(e);

    return eng->newQObject(new RSAPublicKey(params), QScriptEngine::ScriptOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| ,QScriptEngine::ExcludeSuperClassMethods(*/
                           /*|  QScriptEngine::ExcludeSuperClassProperties*/);
}


QScriptValue __crypto_rsa_createPublicKeyFromBER(QScriptContext *ctx, QScriptEngine *eng)
{
    if (ctx->argumentCount() != 1) {
        ctx->throwError("__crypto_rsa_createPublicKeyFromBER invalid argument count");
        return QScriptValue();
    }

    ByteArray  *ba = qscriptvalue_cast<ByteArray*>(ctx->argument(0).data());
    if (!ba) {
        ctx->throwError("__crypto_rsa_createPublicKeyFromBER invalid argument, mustbe instanceof ByteArray");
        return QScriptValue();
    }

    RSA::PublicKey rsaPublic;
    // std::string pubkey(ba->constData(), ba->size());
    // CryptoPP::HexDecoder decoder;
    // decoder.Put( (byte*)pubkey.c_str(), pubkey.size() );
    // decoder.MessageEnd();
    // rsaPublic.Load(decoder);

    CryptoPP::ByteQueue queue;
    std::string skey(ba->toHex().data());
    CryptoPP::StringSource stringSource(skey,true, new CryptoPP::HexDecoder(new CryptoPP::Redirector(queue)));

    try {
    rsaPublic.BERDecodePublicKey(queue, false /*paramsPresent*/, queue.MaxRetrievable());

    } catch (CryptoPP::Exception& ex) {
        ctx->throwError(QString("__crypto_rsa_createPublicKeyFromBER error:")+ex.what());
        return QScriptValue();
    }


    return eng->newQObject(new RSAPublicKey(rsaPublic), QScriptEngine::ScriptOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| ,QScriptEngine::ExcludeSuperClassMethods(*/
                           /*|  QScriptEngine::ExcludeSuperClassProperties*/);

}

QScriptValue __crypto_rsa_createPublicKeyFromPEM(QScriptContext *ctx, QScriptEngine *eng)
{
    if (ctx->argumentCount() != 1) {
        ctx->throwError("__crypto_rsa_createPublicKeyFromPEM invalid argument count");
        return QScriptValue();
    }

    ByteArray  *ba = qscriptvalue_cast<ByteArray*>(ctx->argument(0).data());
    if (!ba) {
        ctx->throwError("__crypto_rsa_createPublicKeyFromDER invalid argument, mustbe instanceof ByteArray");
        return QScriptValue();
    }
    CryptoPP::InvertibleRSAFunction params;
    std::string rsaPublicMaterial(ba->constData());
    CryptoPP::StringSource stringSource(rsaPublicMaterial,true);
    CryptoPP::PEM_Load(stringSource.Ref(), params);
    RSA::PublicKey rsaPublic(params);


    return eng->newQObject(new RSAPublicKey(rsaPublic), QScriptEngine::ScriptOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| ,QScriptEngine::ExcludeSuperClassMethods(*/
                           /*|  QScriptEngine::ExcludeSuperClassProperties*/);
}


include(../plugins.pri)

isEmpty(USE_SCRIPT_CLASSIC) {
    TARGET         = abscriptcrypto
} else {
    TARGET         = abscriptcrypto_compat
}

DESTDIR        = $$ATOMBOXER_SOURCE_TREE/plugins/script
!isEmpty(USE_SCRIPT_CLASSIC) {
    tandem {
        QT            -= core
    }
} else {
    QT            += core script
}


DEPENDPATH += .
INCLUDEPATH += . $$ATOMBOXER_SOURCE_TREE/src/3rdparty/ $$ATOMBOXER_SOURCE_TREE/src/plugins/core

HEADERS  = \
           cipher.h \
           crypto.h \
           cryptoplugin.h \
           decipher.h \
           hash.h \
           rsaprivatekey.h \
           rsaprivatekeyprototype.h \
           rsapublickey.h \
           rsapublickeyprototype.h


SOURCES  = \
           cipher.cpp \
           crypto.cpp \
           cryptoplugin.cpp \
           decipher.cpp \
           hash.cpp \
           rsaprivatekey.cpp \
           rsaprivatekeyprototype.cpp \
           rsapublickey.cpp \
           rsapublickeyprototype.cpp



RESOURCES += jscryptofiles.qrc

static {
    RESOURCES += crypto.qrc
} else {
    CONFIG   += qt plugin

    aix-xlc-64 {
        LIBS         +=  -L/usr/lib/ -lssl  \
                         -lcrypto
    }

    solaris-cc-64-stlport {
        LIBS         += /usr/lib/64/libssl.so \
                        /usr/lib/64/libcrypto.so
    }

    linux-g++ {
        LIBS         += /usr/lib64/libssl.so \
                        /usr/lib64/libcrypto.so
    }


    win32 {
        LIBS         += $$OPENSSL_DIR/libssl.a \
                        $$OPENSSL_DIR/libcrypto.a
    }

    win32:!tandem {
        LIBS += -lcrypt32 -lgdi32
    }

    isEmpty(USE_SCRIPT_CLASSIC) {
         LIBS += -labscriptcore
         QT   +=  script
    } else {
         LIBS += -labscriptcore_compat
    }
}

!tandem {
PRE_TARGETDEPS  = \
             $$ATOMBOXER_SOURCE_TREE/lib/libcryptopp.a
    LIBS       += -L$$ATOMBOXER_SOURCE_TREE/lib/ -lcryptopp
}

OTHER_FILES += \
    js/crypto.js

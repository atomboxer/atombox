/**
 * @module crypto/rsa
 * @desc RSA public/private key cryptography
 * @since 0.6
 * @summary RSA public/private key cryptography
 * @example

var console = require('console');
var rsa = require('crypto/rsa');

var key = rsa.generatePrivateKey(2048);
console.log(key.isValid());

var n = key.getModulus();
var p = key.getPrime1();
var q = key.getPrime2();
var d = key.getPrivateExponent();
var e = key.getPublicExponent();

var nkey = rsa.createPrivateKeyFromComponents(n,p,q,d,e)

var pem =
"-----BEGIN RSA PRIVATE KEY-----\n"+
"MIICXgIBAAKBgQDDr2r0KWv303GM6gv3ktH2ZdXoG3o6CviI+C8sN/C/+6SLumia\n"+
"B97+jJTUoDhe5aQGFZtfvX61W3gLNCq0fou0RsyUvQ2S6BQr9AsxuMBh8JZ3nHf4\n"+
"swqEKqlIOUg+xsW+X8KbEEceyPFuP74qdLR0IiScBVU7FZmymsePMIMmjQIDAQAB\n"+
"AoGBAJViHnXSATgpVC/Mo+IVVO+Sgh80irQwl9aIEArjkYgdpFl+rkeX5qXPz/YZ\n"+
"ca8QA40ZhYkq8cWk1BlJc6hiSma4lkNQgiFeWCxanRHaYidJMS1D6O9WHxt3H4lH\n"+
"qHbYv2FYgIykaRMw4h/hichXv9R/5OPwu/d6//P4ojMgiYAJAkEA7lmbzVNZEDCQ\n"+
"5RTbVUi8mwVe7UDxRqPIO3taih4xmAzlze8QQfX84w7fRPS9PCLLSdcKhT4TKl/R\n"+
"Mn3UQFXEPwJBANItA6W4ETZkEVj8mSL2m9nObPRnseIo+ZJXywlYqgU7moB7ANP/\n"+
"h5eRmtJqfNxXLpe4YUfDhhNdvsXi0du3cjMCQQDQ9AHgmUclKE5+ZjB6rc+a37R+\n"+
"rkZXNVJewx92ok28DOKSDRjlmq9epzUZEK2tQJuTWlBAy1Cl/2Y4q5Md65KlAkA9\n"+
"IAJTi/jVjLzO2Ifg9z/UVIizfATr9Wa//PB7w2GPLAp/G0beYQ6aMiqIz5eIrYJL\n"+
"dcA1EtRHhZgaDSFjeTKpAkEApr4SEh0QZYbdMFFx9XZ0Lnk2REJLd4IjM4E++LUP\n"+
"0OcJKbtf5n6oJiWz0HVEaO9aBv1ddy1Yti6XjPhXmeeWVg==\n"+
"-----END RSA PRIVATE KEY-----\n"

var k1 = rsa.createPrivateKeyFromPEM(pem.toByteArray());
var k2 = rsa.createPrivateKeyFromBER(k1.toDER());

console.assert(pem == k2.toPEM().decodeToString());

var oaep_enc = k1.encryptOAEP("hello world!".toByteString());
var oaep_dec = k1.decryptOAEP(oaep_enc);
console.assert("hello world!" == oaep_dec.decodeToString());

var pkcs_enc = k1.encryptPKCS("hello world!".toByteString());
var pkcs_dec = k1.decryptPKCS(pkcs_enc);
console.assert("hello world!" == pkcs_dec.decodeToString());

var sha_sig = k1.signSHA256("hello world".toByteArray());
console.assert(k1.verifySHA256("hello world".toByteArray(), sha_sig));
 */



/**
 * @class
 * @name module:crypto/rsa.RSAPrivateKey
 * @classdesc This class incapsulates one RSA Private Key
 * @summary RSA Private Key
 * @see module:crypto/rsa.generatePrivateKey
 * @see module:crypto/rsa.createPrivateKeyFromComponents
 * @see module:crypto/rsa.createPrivateKeyFromPEM
 * @see module:crypto/rsa.createPrivateKeyFromBER
 */


/**
* @method module:crypto/rsa.RSAPrivateKey.prototype.getPublicExponent
* @return {Binary}
*/

/**
* @method module:crypto/rsa.RSAPrivateKey.prototype.getModulus
* @return {Binary}
*/

/**
* @method module:crypto/rsa.RSAPrivateKey.prototype.getPrivateExponent
* @return {Binary}
*/

/**
* @method module:crypto/rsa.RSAPrivateKey.prototype.getPrime1
* @return {Binary}
*/

/**
* @method module:crypto/rsa.RSAPrivateKey.prototype.getPrime2
* @return {Binary}
*/

/**
* @method module:crypto/rsa.RSAPrivateKey.prototype.toPEM
* @return {Binary}
*/

/**
* @method module:crypto/rsa.RSAPrivateKey.prototype.toDER
* @return {Binary}
*/

/**
* @method module:crypto/rsa.RSAPrivateKey.prototype.isValid
* @return {Boolean}
*/

/**
* @method module:crypto/rsa.RSAPrivateKey.prototype.isPublicKey
* @return {Boolean}
*/

/**
* @method module:crypto/rsa.RSAPrivateKey.prototype.isPrivateKey
* @return {Boolean}
*/


/**
 * Encrypt with OAEP_SHA padding
* @method module:crypto/rsa.RSAPrivateKey.prototype.encryptOAEP
* @arg {Binary} data
* @return {Binary}
*/


/**
 * Decrypt with OAEP_SHA padding
 *  @method module:crypto/rsa.RSAPrivateKey.prototype.decryptOAEP
 * @arg {Binary} data
 * @return {Binary}
 */


/**
 * Encrypt with PKCS v1.5 padding
 * @method module:crypto/rsa.RSAPrivateKey.prototype.encryptPKCS
 * @arg {Binary} data
 * @return {Binary}
 */


/**
 * Decrypt with PKCS v1.5 padding
 * @method module:crypto/rsa.RSAPrivateKey.prototype.decryptPKCS
 * @arg {Binary} data
 * @return {Binary}
 */

/**
 * Generates a signature with SHA1, PKCS v1.5 padding
 * @method module:crypto/rsa.RSAPrivateKey.prototype.signSHA
 * @arg {Binary} data
 * @return {Binary}
 */

/**
 * Verifies a signature with SHA1, PKCS v1.5 padding
 * @method module:crypto/rsa.RSAPrivateKey.prototype.verifySHA
 * @arg {Binary} data
 * @arg {Binary} signature
 * @return {Boolean}
 */

/**
 * Generates a signature with SHA256, PKCS v1.5 padding
 * @method module:crypto/rsa.RSAPrivateKey.prototype.signSHA256
 * @arg {Binary} data
 * @return {Binary}
 */

/**
 * Verifies a signature with SHA256, PKCS v1.5 padding
 * @method module:crypto/rsa.RSAPrivateKey.prototype.verifySHA256
 * @arg {Binary} data
 * @arg {Binary} signature
 * @return {Boolean}
 */



/**
 * @class
 * @name module:crypto/rsa.RSAPublicKey
 * @classdesc This class incapsulates one RSA Public Key
 * @summary RSA Public Key
 * @see module:crypto/rsa.createPublicKeyFromPrivateKey
 * @see module:crypto/rsa.createPublicKeyFromComponents
 * @see module:crypto/rsa.createPublicKeyFromPEM
 * @see module:crypto/rsa.createPublicKeyFromBER
 */


/**
* @method module:crypto/rsa.RSAPublicKey.prototype.getPublicExponent
* @return {Binary}
*/

/**
* @method module:crypto/rsa.RSAPublicKey.prototype.getModulus
* @return {Binary}
*/

/**
* @method module:crypto/rsa.RSAPublicKey.prototype.toPEM
* @return {Binary}
*/

/**
* @method module:crypto/rsa.RSAPublicKey.prototype.toDER
* @return {Binary}
*/

/**
* @method module:crypto/rsa.RSAPublicKey.prototype.toBER
* @return {Binary}
*/

/**
* @method module:crypto/rsa.RSAPublicKey.prototype.isValid
* @return {Boolean}
*/

/**
* @method module:crypto/rsa.RSAPublicKey.prototype.isPublicKey
* @return {Boolean}
*/

/**
* @method module:crypto/rsa.RSAPublicKey.prototype.isPrivateKey
* @return {Boolean}
*/


/**
 * Encrypt with OAEP_SHA padding
* @method module:crypto/rsa.RSAPublicKey.prototype.encryptOAEP
* @arg {Binary} data
* @return {Binary}
*/


/**
 * Encrypt with PKCS v1.5 padding
 * @method module:crypto/rsa.RSAPublicKey.prototype.encryptPKCS
 * @arg {Binary} data
 * @return {Binary}
 */

/**
 * Verifies a signature with SHA1, PKCS v1.5 padding
 * @method module:crypto/rsa.RSAPublicKey.prototype.verifySHA
 * @arg {Binary} data
 * @arg {Binary} signature
 * @return {Boolean}
 */


/**
 * Verifies a signature with SHA256, PKCS v1.5 padding
 * @method module:crypto/rsa.RSAPublicKey.prototype.verifySHA256
 * @arg {Binary} data
 * @arg {Binary} signature
 * @return {Boolean}
 */

/**
 *  Generate a Private/Public key pair. Note that RSAPrivateKey will contain the RSA public key part.
 *  @method module:crypto/rsa.generatePrivateKey
 *  @arg modulus Key Length
 *  @return {module:crypto/rsa.RSAPrivateKey}
 *  @throws {Error}
 *  @see module:crypto/rsa.createPublicKeyFromPrivateKey
 @example
var rsa = require('crypto/rsa');
var key = rsa.generatePrivateKey(2048);
 */
exports.generatePrivateKey                  = generatePrivateKey = __internal__.__crypto_rsa_generatePrivateKey;


/**
 *  Creates a RSAPrivateKey object from components
 *  @method module:crypto/rsa.createPrivateKeyFromComponents
 *  @arg n {ByteArray} modulus
 *  @arg p {ByteArray} prime1
 *  @arg q {ByteArray} prime2
 *  @arg d {ByteArray} private exponent
 *  @arg e {ByteArray} public  exponent
 *  @return {module:crypto/rsa.RSAPrivateKey}
 *  @see module:crypto/rsa
 @example
var rsa = require('crypto/rsa');
var key = rsa.generatePrivateKey(2048);
console.log(key.isValid());

var n = key.getModulus();
var p = key.getPrime1();
var q = key.getPrime2();
var d = key.getPrivateExponent();
var e = key.getPublicExponent();

var nkey = rsa.createPrivateKeyFromComponents(n,p,q,d,e)
 */
exports.createPrivateKeyFromComponents      = __crypto_rsa_createPrivateKeyFromComponents = __internal__.__crypto_rsa_createPrivateKeyFromComponents;

/**
 *  Creates a RSAPrivateKey object from a ByteArray containg the PEM format
 *  @method module:crypto/rsa.createPrivateKeyFromPEM
 *  @arg ba {Binary} PEM representation
 *  @return {module:crypto/rsa.RSAPrivateKey}
 *  @see module:crypto/rsa
 @example
var pem =
"-----BEGIN RSA PRIVATE KEY-----\n"+
"MIICXgIBAAKBgQDDr2r0KWv303GM6gv3ktH2ZdXoG3o6CviI+C8sN/C/+6SLumia\n"+
"B97+jJTUoDhe5aQGFZtfvX61W3gLNCq0fou0RsyUvQ2S6BQr9AsxuMBh8JZ3nHf4\n"+
"swqEKqlIOUg+xsW+X8KbEEceyPFuP74qdLR0IiScBVU7FZmymsePMIMmjQIDAQAB\n"+
"AoGBAJViHnXSATgpVC/Mo+IVVO+Sgh80irQwl9aIEArjkYgdpFl+rkeX5qXPz/YZ\n"+
"ca8QA40ZhYkq8cWk1BlJc6hiSma4lkNQgiFeWCxanRHaYidJMS1D6O9WHxt3H4lH\n"+
"qHbYv2FYgIykaRMw4h/hichXv9R/5OPwu/d6//P4ojMgiYAJAkEA7lmbzVNZEDCQ\n"+
"5RTbVUi8mwVe7UDxRqPIO3taih4xmAzlze8QQfX84w7fRPS9PCLLSdcKhT4TKl/R\n"+
"Mn3UQFXEPwJBANItA6W4ETZkEVj8mSL2m9nObPRnseIo+ZJXywlYqgU7moB7ANP/\n"+
"h5eRmtJqfNxXLpe4YUfDhhNdvsXi0du3cjMCQQDQ9AHgmUclKE5+ZjB6rc+a37R+\n"+
"rkZXNVJewx92ok28DOKSDRjlmq9epzUZEK2tQJuTWlBAy1Cl/2Y4q5Md65KlAkA9\n"+
"IAJTi/jVjLzO2Ifg9z/UVIizfATr9Wa//PB7w2GPLAp/G0beYQ6aMiqIz5eIrYJL\n"+
"dcA1EtRHhZgaDSFjeTKpAkEApr4SEh0QZYbdMFFx9XZ0Lnk2REJLd4IjM4E++LUP\n"+
"0OcJKbtf5n6oJiWz0HVEaO9aBv1ddy1Yti6XjPhXmeeWVg==\n"+
"-----END RSA PRIVATE KEY-----\n"

var k1 = rsa.createPrivateKeyFromPEM(pem.toByteArray());
 */
exports.createPrivateKeyFromPEM             = __crypto_rsa_createPrivateKeyFromPEM = __internal__.__crypto_rsa_createPrivateKeyFromPEM;
/**
 *  Creates a RSAPrivateKey object from a ByteArray containg the BER/DER representation
 *  @method module:crypto/rsa.createPrivateKeyFromBER
 *  @arg ba {Binary} BER/DER representation
 *  @return {module:crypto/rsa.RSAPrivateKey}
 *  @see module:crypto.rsa
 @example
var pem =
"-----BEGIN RSA PRIVATE KEY-----\n"+
"MIICXgIBAAKBgQDDr2r0KWv303GM6gv3ktH2ZdXoG3o6CviI+C8sN/C/+6SLumia\n"+
"B97+jJTUoDhe5aQGFZtfvX61W3gLNCq0fou0RsyUvQ2S6BQr9AsxuMBh8JZ3nHf4\n"+
"swqEKqlIOUg+xsW+X8KbEEceyPFuP74qdLR0IiScBVU7FZmymsePMIMmjQIDAQAB\n"+
"AoGBAJViHnXSATgpVC/Mo+IVVO+Sgh80irQwl9aIEArjkYgdpFl+rkeX5qXPz/YZ\n"+
"ca8QA40ZhYkq8cWk1BlJc6hiSma4lkNQgiFeWCxanRHaYidJMS1D6O9WHxt3H4lH\n"+
"qHbYv2FYgIykaRMw4h/hichXv9R/5OPwu/d6//P4ojMgiYAJAkEA7lmbzVNZEDCQ\n"+
"5RTbVUi8mwVe7UDxRqPIO3taih4xmAzlze8QQfX84w7fRPS9PCLLSdcKhT4TKl/R\n"+
"Mn3UQFXEPwJBANItA6W4ETZkEVj8mSL2m9nObPRnseIo+ZJXywlYqgU7moB7ANP/\n"+
"h5eRmtJqfNxXLpe4YUfDhhNdvsXi0du3cjMCQQDQ9AHgmUclKE5+ZjB6rc+a37R+\n"+
"rkZXNVJewx92ok28DOKSDRjlmq9epzUZEK2tQJuTWlBAy1Cl/2Y4q5Md65KlAkA9\n"+
"IAJTi/jVjLzO2Ifg9z/UVIizfATr9Wa//PB7w2GPLAp/G0beYQ6aMiqIz5eIrYJL\n"+
"dcA1EtRHhZgaDSFjeTKpAkEApr4SEh0QZYbdMFFx9XZ0Lnk2REJLd4IjM4E++LUP\n"+
"0OcJKbtf5n6oJiWz0HVEaO9aBv1ddy1Yti6XjPhXmeeWVg==\n"+
"-----END RSA PRIVATE KEY-----\n"

var k1 = rsa.createPrivateKeyFromPEM(pem.toByteArray());
 */
exports.createPrivateKeyFromBER             = __crypto_rsa_createPrivateKeyFromBER = __internal__.__crypto_rsa_createPrivateKeyFromBER;


/**
 *  Creates a RSAPublicKey object from a RSAPrivateKey
 *  @method module:crypto/rsa.createPublicKeyFromPrivateKey
 *  @arg ba {module:crypto/rsa.RSAPrivateKey} RSAPrivateKey object
 *  @return {module:crypto/rsa.RSAPublicKey}
 *  @see module:crypto.rsa
 */
exports.createPublicKeyFromPrivateKey       = __crypto_rsa_createPublicKeyFromPrivateKey = __internal__.__crypto_rsa_createPublicKeyFromPrivateKey;

/**
 *  Creates a RSAPublicKey object from individual components
 *  @method module:crypto/rsa.createPublicKeyFromComponents
 *  @arg ba {module:crypto/rsa.RSAPrivateKey} RSAPrivateKey object
 *  @arg n {ByteArray} modulus
 *  @arg e {ByteArray} public  exponent
 *  @return {module:crypto/rsa.RSAPublicKey}
 *  @see module:crypto/rsa
 */
exports.createPublicKeyFromComponents       = __crypto_rsa_createPublicKeyFromComponents = __internal__.__crypto_rsa_createPublicKeyFromComponents;


/**
 *  Creates a RSAPublicKey object from a ByteArray containg the PEM
 *  @method module:crypto/rsa.createPublicKeyFromPEM
 *  @arg ba {Binary} PEM representation
 *  @return {module:crypto/rsa.RSAPublicKey}
 *  @see module:crypto/rsa
 */
exports.createPublicKeyFromPEM              = __crypto_rsa_createPublicKeyFromPEM = __internal__.__crypto_rsa_createPublicKeyFromPEM;

/**
 *  Creates a RSAPublicKey object from a ByteArray containg the DER/BER
 *  @method module:crypto/rsa.createPublicKeyFromBER
 *  @arg ba {Binary} DER/BER representation
 *  @return {module:crypto/rsa.RSAPublicKey}
 *  @see module:crypto/rsa
 */
exports.createPublicKeyFromBER              = __crypto_rsa_createPublicKeyFromBER  = __internal__.__crypto_rsa_createPublicKeyFromBER;

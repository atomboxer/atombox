__postInit__ = function() {

    if (!Function.prototype.bind) {
        Object.defineProperty(Function.prototype, "bind", {
            value: function(oThis) {
                if (typeof this !== "function") {
                    throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
                }

                var aArgs = Array.prototype.slice.call(arguments, 1),
                fToBind = this,
                fNOP = function() {},
                fBound = function() {
                    return fToBind.apply(this instanceof fNOP ? this : oThis,
                                         aArgs.concat(Array.prototype.slice.call(arguments)));
                };
                fNOP.prototype = this.prototype;
                fBound.prototype = new fNOP();

                return fBound;
            }
        });
    }

    Object.defineProperty(String.prototype, "lpad", {
        value: function(ch, count) {
            var ch = ch || "0";
            var cnt = count || 2;
            var s = "";
            while (s.length < (cnt - this.length)) {
                s += ch;
            }
            s = s.substring(0, cnt - this.length);
            return s + this;
        },
        writable: true
    });

    Object.defineProperty(String.prototype, "rpad", {
        value: function(ch, count) {
            var ch = ch || "0";
            var cnt = count || 2;

            var s = "";
            while (s.length < (cnt - this.length)) {
                s += ch;
            }
            s = s.substring(0, cnt - this.length);
            return this + s;
        }
    });

    Object.defineProperty(String.prototype, "trim", {
        value: function() {
            return this.replace(/^\s+|\s+$/g, '');
        },
        writable: true
    });



    var dateFormat = function() {
        var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function(val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };

        // Regexes and supporting functions are cached through closure
        return function(date, mask, utc) {
            var dF = dateFormat;

            // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
            if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
                mask = date;
                date = undefined;
            }

            // Passing date through Date applies Date.parse, if necessary
            date = date ? new Date(date) : new Date;
            if (isNaN(date)) throw SyntaxError("invalid date");

            mask = String(dF.masks[mask] || mask || dF.masks["default"]);

            // Allow setting the utc argument via the mask
            if (mask.slice(0, 4) == "UTC:") {
                mask = mask.slice(4);
                utc = true;
            }

            var _ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d: d,
                dd: pad(d),
                ddd: dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m: m + 1,
                mm: pad(m + 1),
                mmm: dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy: String(y).slice(2),
                yyyy: y,
                h: H % 12 || 12,
                hh: pad(H % 12 || 12),
                H: H,
                HH: pad(H),
                M: M,
                MM: pad(M),
                s: s,
                ss: pad(s),
                l: pad(L, 3),
                L: pad(L > 99 ? Math.round(L / 10) : L),
                t: H < 12 ? "a" : "p",
                tt: H < 12 ? "am" : "pm",
                T: H < 12 ? "A" : "P",
                TT: H < 12 ? "AM" : "PM",
                Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };

            return mask.replace(token, function($0) {
                return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
            });
        };
    }();

    // Some common format strings
    dateFormat.masks = {
        "default": "ddd mmm dd yyyy HH:MM:ss",
        shortDate: "m/d/yy",
        mediumDate: "mmm d, yyyy",
        longDate: "mmmm d, yyyy",
        fullDate: "dddd, mmmm d, yyyy",
        shortTime: "h:MM TT",
        mediumTime: "h:MM:ss TT",
        longTime: "h:MM:ss TT Z",
        isoDate: "yyyy-mm-dd",
        isoTime: "HH:MM:ss",
        isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
        isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
    };

    // Internationalization strings
    dateFormat.i18n = {
        dayNames: [
            "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
            "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
        ],
        monthNames: [
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
        ]
    };

    // For convenience...
    Object.defineProperty(Date.prototype, "format", {
        value: function(mask, utc) {
            return dateFormat(this, mask, utc);
        }
    });


    // Date.prototype._dayNames = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    // Date.prototype._dayNamesShort = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
    // Date.prototype._monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    // Date.prototype._monthNamesShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    // Object.defineProperty(Date.prototype,"format", {
    // 	value: function(str) {
    // 	    var suffixes = {
    // 		1:"st",
    // 		2:"nd",
    // 		3:"rd",
    // 		21:"st",
    // 		22:"nd",
    // 		23:"rd",
    // 		31:"st"
    // 	    };
    // 	    var result = "";
    // 	    for (var i=0;i<str.length;i++) {
    // 		var ch = str.charAt(i);
    // 		switch (ch) {
    // 		case "d": result += this.getDate().toString().lpad(" "); break;
    // 		case "j": result += this.getDate(); break;
    // 		case "w": result += this.getDay(); break;
    // 		case "N": result += this.getDay() || 7; break;
    // 		case "S": 
    // 		    var d = this.getDate();
    // 		    result += suffixes[d] || "th";
    // 		    break;
    // 		case "D": result += this._dayNamesShort[(this.getDay() || 7)-1]; break;
    // 		case "l": result += this._dayNames[(this.getDay() || 7)-1]; break;
    // 		case "z":
    // 		    var t = this.getTime();
    // 		    var d = new Date(t);
    // 		    d.setDate(1);
    // 		    d.setMonth(0);
    // 		    var diff = t - d.getTime();
    // 		    result += diff / (1000 * 60 * 60 * 24);
    // 		    break;

    // 		case "W":
    // 		    var d = new Date(this.getFullYear(), this.getMonth(), this.getDate());
    // 		    var day = d.getDay() || 7;
    // 		    d.setDate(d.getDate() + (4-day));
    // 		    var year = d.getFullYear();
    // 		    var day = Math.floor((d.getTime() - new Date(year, 0, 1, -6)) / (1000 * 60 * 60 * 24));
    // 		    result += (1 + Math.floor(day / 7)).toString().lpad(" ");
    // 		    break;

    // 		case "m": result += (this.getMonth()+1).toString().lpad(" "); break;
    // 		case "n": result += (this.getMonth()+1); break;
    // 		case "M": result += this._monthNamesShort[this.getMonth()]; break;
    // 		case "F": result += this._monthNames[this.getMonth()]; break;
    // 		case "t":
    // 		    var t = this.getTime();
    // 		    var m = this.getMonth();
    // 		    var d = new Date(t);
    // 		    var day = 0;
    // 		    do {
    // 			day = d.getDate();
    // 			t += 1000 * 60 * 60 * 24;
    // 			d = new Date(t);
    // 		    } while (m == d.getMonth());
    // 		    result += day;
    // 		    break;

    // 		case "L":
    // 		    var d = new Date(this.getTime());
    // 		    d.setDate(1);
    // 		    d.setMonth(1);
    // 		    d.setDate(29);
    // 		    result += (d.getMonth() == 1 ? "1" : "0");
    // 		    break;
    // 		case "Y": result += this.getFullYear().toString().lpad(" "); break;
    // 		case "y": result += this.getFullYear().toString().lpad(" ").substring(2); break;

    // 		case "a": result += (this.getHours() < 12 ? "am" : "pm"); break;
    // 		case "A": result += (this.getHours() < 12 ? "AM" : "PM"); break;
    // 		case "G": result += this.getHours(); break;
    // 		case "H": result += this.getHours().toString().lpad(" "); break;
    // 		case "g": result += this.getHours() % 12; break;
    // 		case "h": result += (this.getHours() % 12).toString().lpad(" "); break;
    // 		case "i": result += this.getMinutes().toString().lpad(" "); break;
    // 		case "s": result += this.getSeconds().toString().lpad(" "); break;

    // 		case "Z": result += -60*this.getTimezoneOffset(); break;

    // 		case "O": 
    // 		case "P": 
    // 		    var base = this.getTimezoneOffset()/-60;
    // 		    var o = Math.abs(base).toString().lpad(" ");
    // 		    if (ch == "P") { o += ":"; }
    // 		    o += "00";
    // 		    result += (base >= 0 ? "+" : "-")+o;
    // 		    break;

    // 		case "U": result += this.getTime()/1000; break; 
    // 		case "u": result += "0"; break; 
    // 		case "c": result += arguments.callee.call(this, "Y-m-d")+"T"+arguments.callee.call(this, "H:i:sP"); break; 
    // 		case "r": result += arguments.callee.call(this, "D, j M Y H:i:s O"); break; 

    // 		default: result += ch; break;
    // 		}
    // 	    }
    // 	    return result;
    // 	},writable:true
    // });


    Object.defineProperty(Date, "parseTandem", {
        value: function(n) {
            var MSECS_PER_DAY = 86400000;
            var msecs = Number(n * 10);
            var ddays = msecs / MSECS_PER_DAY;
            msecs %= MSECS_PER_DAY;
            if (msecs < 0) {
                --ddays;
                msecs += MSECS_PER_DAY;
            }
            var epoch = new Date(1974, 11, 31);
            epoch.setTime(epoch.getTime() + ddays * MSECS_PER_DAY);
            return epoch;
        },
        writable: true
    });

    Object.defineProperty(Date, "parseJulian", {
        value: function(n) {
            var unixtime = ((n - 210866803200000000) / 1000000);
            return new Date(Math.round(unixtime) * 1000 + (12) * 3600 * 1000 /*add 12 hours*/ );
        },
        writable: true
    });



    JSON.toAtomBox = function(o, ab) {
        for (var i in o) {
            if (ab == undefined ||
                ab[i] == undefined) {
                continue;
            }

            if (o[i] !== undefined && typeof(o[i]) == "object") {
//                if (!ab[i].isRedefine())
                    JSON.toAtomBox(o[i], ab[i]);
            }
            else {
                //handle atom
                if (Object.prototype.hasOwnProperty.call(ab[i], "value")) {
//                    if (!ab[i].isRedefine())
                        ab[i].value = o[i];
                }
            }
        }
    }


    //     if (typeof JSON.parse !== 'function') {
    //         JSON.parse = function (text, reviver) {
    //             //     var j;

    //             //     function walk(holder, key) {
    //             //         var k, v, value = holder[key];
    //             //         if (value && typeof value === 'object') {
    //             //             for (k in value) {
    //             //                 if (Object.prototype.hasOwnProperty.call(value, k)) {
    //             //                     v = walk(value, k);
    //             //                     if (v !== undefined) {
    //             //                         value[k] = v;
    //             //                     } else {
    //             //                         delete value[k];
    //             //                     }
    //             //                 }
    //             //             }
    //             //         }
    //             //         return reviver.call(holder, key, value);
    //             //     }
    //             //     text = String(text);
    //             //     cx.lastIndex = 0;
    //             //     if (cx.test(text)) {
    //             //         text = text.replace(cx, function (a) {
    //             //             return '\\u' +
    //             //                 ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
    //             //         });
    //             //     }

    //             //     if (/^[\],:{}\s]*$/
    //             //         .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
    //             //               .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
    //             //               .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
    //             //j = eval('(' + text + ')');
    //             //         return typeof reviver === 'function'
    //             //             ? walk({'': j}, '')
    //             //         : j;
    //             //     }
    //             //     throw new SyntaxError('JSON.parse');
    //             // };
    // 	        return eval(text);
    //         }
    //     }
    // }());
}

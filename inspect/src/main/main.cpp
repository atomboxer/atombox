#include <QtGui>
#include <QtNetwork>
#include <QtNetwork/QNetworkProxyFactory>

#include "scriptremotetargetdebugger.h"
#include "connectdialog.h"

void qScriptDebugRegisterMetaTypes();

class Runner : public QObject
{
    Q_OBJECT
public:
    Runner(QObject *parent = 0);
    void go(const QHostAddress &addr, quint16 port, bool listen);

private slots:
    void slotGo();
    void onAttached();
    void onDetached();
    void onError(ScriptRemoteTargetDebugger::Error error);

private:
    ScriptRemoteTargetDebugger *m_debugger;
    ConnectDialog              *dialog_;
};

Runner::Runner(QObject *parent) : QObject(parent)
{
    dialog_ = new ConnectDialog;
    QObject::connect(dialog_, SIGNAL(signalGo()), this, SLOT(slotGo()));
    dialog_->show();
}

void
Runner::go(const QHostAddress &addr, quint16 port, bool listen) 
{
    m_debugger = new ScriptRemoteTargetDebugger(this);
    QObject::connect(m_debugger, SIGNAL(attached()), this, SLOT(onAttached()));
    QObject::connect(m_debugger, SIGNAL(detached()), this, SLOT(onDetached()));
    QObject::connect(m_debugger, SIGNAL(error(ScriptRemoteTargetDebugger::Error)),
                     this, SLOT(onError(ScriptRemoteTargetDebugger::Error)));
    if (listen) {
        if (m_debugger->listen(addr, port))
            qDebug("listening for debuggee connection at %s:%d", qPrintable(addr.toString()), port);
        else {
            qWarning("Failed to listen!");
            QCoreApplication::quit();
        }
    } else {
        qDebug("attaching to %s:%d", qPrintable(addr.toString()), port);
        m_debugger->attachTo(addr, port);
    }


}

void
Runner::slotGo()
{
    go(QHostAddress(dialog_->ip()), dialog_->port().toInt(), false); 
}

void Runner::onAttached()
{
    dialog_->hide();
}

void Runner::onDetached()
{
    QMessageBox::information(dialog_, 
                             "Warning", 
                             "The remote application has disconnected.",
                             QMessageBox::Ok);
 
    QApplication::quit();
}

void Runner::onError(ScriptRemoteTargetDebugger::Error /*error*/)
{
    QMessageBox::critical(dialog_, 
                          "Error", 
                          "Cannot connect to remote application.",
                          QMessageBox::Ok);
    QApplication::quit();
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QNetworkProxyFactory::setUseSystemConfiguration(true);
    
    QFont font("Verdana");
    font.setStyleHint(QFont::TypeWriter);
    font.setPointSize(8);
    QApplication::setFont(font);

    qScriptDebugRegisterMetaTypes();

    Runner runner;
    return app.exec();
}

#include "main.moc"

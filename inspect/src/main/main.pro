TEMPLATE = app

isEmpty(ATOMBOX_INSPECT_PRI_INCLUDED) {
    include(../../atombox_inspect.pri)
}

QT += gui network core script 
#CONFIG += console

HEADERS     += connectdialog.h \

SOURCES     += \
               connectdialog.cpp \
               main.cpp \

PRE_TARGETDEPS  = \
             $$ATOMBOX_INSPECT_SOURCE_TREE/lib/libQtScriptTools1.a \
             $$ATOMBOX_INSPECT_SOURCE_TREE/lib/libremotedebugger.a \

LIBS += -L$$ATOMBOX_INSPECT_SOURCE_TREE/lib/ \
        $$ATOMBOX_INSPECT_SOURCE_TREE/lib/libremotedebugger.a \
        $$ATOMBOX_INSPECT_SOURCE_TREE/lib/libQtScriptTools1.a \
        #$$ATOMBOX_INSPECT_SOURCE_TREE/lib/libQtScriptClassic1.a 

TARGET      = abinspect
DESTDIR     = $$ATOMBOX_INSPECT_SOURCE_TREE/


RC_FILE = main.rc

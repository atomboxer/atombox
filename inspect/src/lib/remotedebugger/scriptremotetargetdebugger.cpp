#include "scriptremotetargetdebugger.h"
#include <QtNetwork/qtcpserver.h>
#include <QtNetwork/qtcpsocket.h>
#include <QtGui>


#include <qscriptdebugger_p.h>
#include <qscriptdebuggerfrontend_p.h>

#include <qscriptdebuggercommand_p.h>
#include <qscriptdebuggerevent_p.h>
#include <qscriptdebuggerresponse_p.h>
#include <qscriptdebuggerstandardwidgetfactory_p.h>


#define DEBUG_DEBUGGER
#define QT_NO_MENUBAR

class ScriptRemoteTargetDebuggerFrontend
    : public QObject, public QScriptDebuggerFrontend
{
    Q_OBJECT

public:
    enum State {
        UnattachedState,
        ConnectingState,
        HandshakingState,
        AttachedState,
        DetachingState
    };

    ScriptRemoteTargetDebuggerFrontend();
    ~ScriptRemoteTargetDebuggerFrontend();

    void attachTo(const QHostAddress &address, quint16 port);
    void detach();
    bool listen(const QHostAddress &address, quint16 port);

Q_SIGNALS:
    void attached();
    void detached();
    void error(ScriptRemoteTargetDebugger::Error error);

protected:
    void processCommand(int id, const QScriptDebuggerCommand &command);

private Q_SLOTS:
    void onSocketStateChanged(QAbstractSocket::SocketState);
    void onSocketError(QAbstractSocket::SocketError);
    void onReadyRead();
    void onNewConnection();

private:
    void initiateHandshake();

private:
    State m_state;
    QTcpServer *m_server;
    QTcpSocket *m_socket;
    int m_blockSize;

    Q_DISABLE_COPY(ScriptRemoteTargetDebuggerFrontend)
};

ScriptRemoteTargetDebuggerFrontend::ScriptRemoteTargetDebuggerFrontend()
    : m_state(UnattachedState), m_server(0), m_socket(0), m_blockSize(0)
{
}

ScriptRemoteTargetDebuggerFrontend::~ScriptRemoteTargetDebuggerFrontend()
{
}

void ScriptRemoteTargetDebuggerFrontend::attachTo(const QHostAddress &address, quint16 port)
{
    Q_ASSERT(m_state == UnattachedState);
    if (!m_socket) {
        m_socket = new QTcpSocket(this);
        QObject::connect(m_socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)),
                         this, SLOT(onSocketStateChanged(QAbstractSocket::SocketState)));
        QObject::connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)),
                         this, SLOT(onSocketError(QAbstractSocket::SocketError)));
        QObject::connect(m_socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    }
    m_socket->connectToHost(address, port);
}

void ScriptRemoteTargetDebuggerFrontend::detach()
{
    Q_ASSERT_X(false, Q_FUNC_INFO, "implement me");
}

bool ScriptRemoteTargetDebuggerFrontend::listen(const QHostAddress &address, quint16 port)
{
    if (m_socket)
        return false;
    if (!m_server) {
        m_server = new QTcpServer(this);
        QObject::connect(m_server, SIGNAL(newConnection()),
                         this, SLOT(onNewConnection()));
    }
    return m_server->listen(address, port);
}

void ScriptRemoteTargetDebuggerFrontend::onSocketStateChanged(QAbstractSocket::SocketState state)
{
    switch (state) {
    case QAbstractSocket::UnconnectedState:
        m_state = UnattachedState;
        break;
    case QAbstractSocket::HostLookupState:
    case QAbstractSocket::ConnectingState:
        m_state = ConnectingState;
        break;
    case QAbstractSocket::ConnectedState:
        initiateHandshake();
        break;
    case QAbstractSocket::BoundState:
        break;
    case QAbstractSocket::ClosingState:
        emit detached();
        break;
    case QAbstractSocket::ListeningState:
        break;
    }
}

void ScriptRemoteTargetDebuggerFrontend::onSocketError(QAbstractSocket::SocketError err)
{
    if (err != QAbstractSocket::RemoteHostClosedError) {
        qDebug("%s", qPrintable(m_socket->errorString()));
        emit error(ScriptRemoteTargetDebugger::SocketError);
    }
}

void ScriptRemoteTargetDebuggerFrontend::onReadyRead()
{
    switch (m_state) {
    case UnattachedState:
    case ConnectingState:
    case DetachingState:
        Q_ASSERT(0);
        break;

    case HandshakingState: {
        QByteArray handshakeData("Hello");
        if (m_socket->bytesAvailable() >= handshakeData.size()) {
            QByteArray ba = m_socket->read(handshakeData.size());
            if (ba == handshakeData) {
#ifdef DEBUG_DEBUGGER
                qDebug("handshake ok!");
#endif
                m_state = AttachedState;
                emit attached();
                if (m_socket->bytesAvailable() > 0)
                    QMetaObject::invokeMethod(this, "onReadyRead", Qt::QueuedConnection);
            } else {
//                d->error = HandshakeError;
//                d->errorString = QString::fromLatin1("Incorrect handshake data received");
                m_state = DetachingState;
                emit error(ScriptRemoteTargetDebugger::HandshakeError);
                m_socket->close();
            }
        }
    }   break;

    case AttachedState: {
#ifdef DEBUG_DEBUGGER
        qDebug() << "got something! bytes available:" << m_socket->bytesAvailable();
#endif
        QDataStream in(m_socket);
        in.setVersion(QDataStream::Qt_4_5);
        if (m_blockSize == 0) {
            if (m_socket->bytesAvailable() < (int)sizeof(quint32))
                return;
            in >> m_blockSize;
#ifdef DEBUG_DEBUGGER
            qDebug() << "blockSize:" << m_blockSize;
#endif
        }
        if (m_socket->bytesAvailable() < m_blockSize) {
#ifdef DEBUG_DEBUGGER
            qDebug("waiting for %lld more bytes...", m_blockSize - m_socket->bytesAvailable());
#endif
            return;
        }

        int wasAvailable = m_socket->bytesAvailable();
        quint8 type;
        in >> type;
        if (type == 0) {
            // event
#ifdef DEBUG_DEBUGGER
            qDebug("deserializing event");
#endif
            QScriptDebuggerEvent event(QScriptDebuggerEvent::None);
            in >> event;
#ifdef DEBUG_DEBUGGER
            qDebug("notifying event of type %d", event.type());
#endif
            bool handled = notifyEvent(event);
            if (handled) {
                scheduleCommand(QScriptDebuggerCommand::resumeCommand(),
                                /*responseHandler=*/0);
            }
        } else {
            // command response
#ifdef DEBUG_DEBUGGER
            qDebug("deserializing command response");
#endif
            qint32 id;
            in >> id;
            QScriptDebuggerResponse response;
            in >> response;
#ifdef DEBUG_DEBUGGER
            qDebug("notifying command %d finished", id);
#endif
            notifyCommandFinished((int)id, response);
        }
        Q_ASSERT(m_socket->bytesAvailable() == wasAvailable - m_blockSize);
        m_blockSize = 0;
#ifdef DEBUG_DEBUGGER
        qDebug("bytes available is now %lld", m_socket->bytesAvailable());
#endif
        if (m_socket->bytesAvailable() != 0)
            QMetaObject::invokeMethod(this, "onReadyRead", Qt::QueuedConnection);
    }   break;
    }
}

void ScriptRemoteTargetDebuggerFrontend::onNewConnection()
{
    //qDebug("received connection");
    m_socket = m_server->nextPendingConnection();
    m_server->close();
    QObject::connect(m_socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)),
                     this, SLOT(onSocketStateChanged(QAbstractSocket::SocketState)));
    QObject::connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)),
                     this, SLOT(onSocketError(QAbstractSocket::SocketError)));
    QObject::connect(m_socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    initiateHandshake();
}

/*!
  \reimp
*/
void ScriptRemoteTargetDebuggerFrontend::processCommand(int id, const QScriptDebuggerCommand &command)
{
    Q_ASSERT(m_state == AttachedState);
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_5);
    out << (quint32)0; // reserve 4 bytes for block size
    out << (qint32)id;
    out << command;
    out.device()->seek(0);
    out << (quint32)(block.size() - sizeof(quint32));
#ifdef DEBUG_DEBUGGER
    qDebug("writing command (id=%d, %d bytes)", id, block.size());
#endif
    m_socket->write(block);
}

void ScriptRemoteTargetDebuggerFrontend::initiateHandshake()
{
    m_state = HandshakingState;
    QByteArray handshakeData("Hello");
#ifdef DEBUG_DEBUGGER
    qDebug("writing handshake data");
#endif
    m_socket->write(handshakeData);
}

ScriptRemoteTargetDebugger::ScriptRemoteTargetDebugger(QObject *parent)
    : QObject(parent), m_frontend(0), m_debugger(0), m_autoShow(true),
      m_standardWindow(0)
{
}

ScriptRemoteTargetDebugger::~ScriptRemoteTargetDebugger()
{
    if (m_standardWindow) {
        QSettings settings(QSettings::UserScope, QLatin1String("Atombox"));
        QByteArray geometry = m_standardWindow->saveGeometry();
        settings.setValue(QLatin1String("/debugging/mainWindowGeometry"), geometry);
        QByteArray state = m_standardWindow->saveState();
        settings.setValue(QLatin1String("/debugging/mainWindowState"), state);
    }

    delete m_frontend;
    delete m_debugger;
}

void ScriptRemoteTargetDebugger::attachTo(const QHostAddress &address, quint16 port)
{
    createFrontend();
    m_frontend->attachTo(address, port);
}

void ScriptRemoteTargetDebugger::detach()
{
    QSettings settings(QSettings::UserScope, QLatin1String("Atombox"));
    QByteArray geometry = m_standardWindow->saveGeometry();
    settings.setValue(QLatin1String("/debugging/mainWindowGeometry"), geometry);
    QByteArray state = m_standardWindow->saveState();
    settings.setValue(QLatin1String("/debugging/mainWindowState"), state);

    if (m_frontend)
        m_frontend->detach();
}

bool ScriptRemoteTargetDebugger::listen(const QHostAddress &address, quint16 port)
{
    createFrontend();
    return m_frontend->listen(address, port);
}

void ScriptRemoteTargetDebugger::createDebugger()
{
    if (!m_debugger) {
        m_debugger = new QScriptDebugger(this);
        m_debugger->setWidgetFactory(new QScriptDebuggerStandardWidgetFactory(this));
        QObject::connect(m_debugger, SIGNAL(started()),
                         this, SIGNAL(evaluationResumed()));
        QObject::connect(m_debugger, SIGNAL(stopped()),
                         this, SIGNAL(evaluationSuspended()));
        if (m_autoShow) {
            QObject::connect(this, SIGNAL(evaluationSuspended()),
                             this, SLOT(showStandardWindow()));
        }
    }
}

void ScriptRemoteTargetDebugger::createFrontend()
{
    if (!m_frontend) {
        m_frontend = new ScriptRemoteTargetDebuggerFrontend();
        QObject::connect(m_frontend, SIGNAL(attached()),
                         this, SIGNAL(attached()), Qt::QueuedConnection);
        QObject::connect(m_frontend, SIGNAL(detached()),
                         this, SIGNAL(detached()), Qt::QueuedConnection);
        QObject::connect(m_frontend, SIGNAL(error(ScriptRemoteTargetDebugger::Error)),
                         this, SIGNAL(error(ScriptRemoteTargetDebugger::Error)));
        createDebugger();
        m_debugger->setFrontend(m_frontend);
    }
}

QMainWindow *ScriptRemoteTargetDebugger::standardWindow() const
{
    if (m_standardWindow)
        return m_standardWindow;
    if (!QApplication::instance())
        return 0;
    ScriptRemoteTargetDebugger *that = const_cast<ScriptRemoteTargetDebugger*>(this);

    QMainWindow *win = new QMainWindow();
    QDockWidget *scriptsDock = new QDockWidget(win);
    scriptsDock->setWindowTitle(QObject::tr("Loaded Scripts"));
    scriptsDock->setWidget(widget(ScriptsWidget));
    scriptsDock->setObjectName(QLatin1String("scriptdebugger_scriptsDock")); 
    win->addDockWidget(Qt::LeftDockWidgetArea, scriptsDock);

    QDockWidget *breakpointsDock = new QDockWidget(win);
    breakpointsDock->setWindowTitle(QObject::tr("Breakpoints"));
    breakpointsDock->setWidget(widget(BreakpointsWidget));
    breakpointsDock->setObjectName(QLatin1String("scriptdebugger_breakpointsDock")); 
    win->addDockWidget(Qt::LeftDockWidgetArea, breakpointsDock);

    QDockWidget *stackDock = new QDockWidget(win);
    stackDock->setWindowTitle(QObject::tr("Stack"));
    stackDock->setWidget(widget(StackWidget));
    stackDock->setObjectName(QLatin1String("scriptdebugger_stackDock")); 
    win->addDockWidget(Qt::RightDockWidgetArea, stackDock);

    QDockWidget *localsDock = new QDockWidget(win);
    localsDock->setWindowTitle(QObject::tr("Locals"));
    localsDock->setWidget(widget(LocalsWidget));
    localsDock->setObjectName(QLatin1String("scriptdebugger_localsDock")); 
    win->addDockWidget(Qt::RightDockWidgetArea, localsDock);

    QDockWidget *consoleDock = new QDockWidget(win);
    consoleDock->setWindowTitle(QObject::tr("Console"));
    consoleDock->setWidget(widget(ConsoleWidget));
    consoleDock->setObjectName(QLatin1String("scriptdebugger_consoleDock")); 
    win->addDockWidget(Qt::BottomDockWidgetArea, consoleDock);

    QDockWidget *debugOutputDock = new QDockWidget(win);
    debugOutputDock->setWindowTitle(QObject::tr("Debug Output"));
    debugOutputDock->setWidget(widget(DebugOutputWidget));
    debugOutputDock->setObjectName(QLatin1String("scriptdebugger_debugDock")); 
    win->addDockWidget(Qt::BottomDockWidgetArea, debugOutputDock);

    QDockWidget *errorLogDock = new QDockWidget(win);
    errorLogDock->setWindowTitle(QObject::tr("Error Log"));
    errorLogDock->setWidget(widget(ErrorLogWidget));
    errorLogDock->setObjectName(QLatin1String("scriptdebugger_errorDock")); 
    win->addDockWidget(Qt::BottomDockWidgetArea, errorLogDock);

    win->tabifyDockWidget(errorLogDock, debugOutputDock);
    win->tabifyDockWidget(debugOutputDock, consoleDock);
    win->setObjectName(QLatin1String("scriptdebugger_win")); 
    win->addToolBar(Qt::TopToolBarArea, that->createStandardToolBar());

#ifndef QT_NO_MENUBAR
    win->menuBar()->addMenu(that->createStandardMenu(win));

    QMenu *editMenu = win->menuBar()->addMenu(QObject::tr("Search"));
    editMenu->addAction(action(FindInScriptAction));
    editMenu->addAction(action(FindNextInScriptAction));
    editMenu->addAction(action(FindPreviousInScriptAction));
    editMenu->addSeparator();
    editMenu->addAction(action(GoToLineAction));

    QMenu *viewMenu = win->menuBar()->addMenu(QObject::tr("View"));
    viewMenu->addAction(scriptsDock->toggleViewAction());
    viewMenu->addAction(breakpointsDock->toggleViewAction());
    viewMenu->addAction(stackDock->toggleViewAction());
    viewMenu->addAction(localsDock->toggleViewAction());
    viewMenu->addAction(consoleDock->toggleViewAction());
    viewMenu->addAction(debugOutputDock->toggleViewAction());
    viewMenu->addAction(errorLogDock->toggleViewAction());
#endif

    QWidget *central = new QWidget();
    QVBoxLayout *vbox = new QVBoxLayout(central);
    vbox->addWidget(widget(CodeWidget));
    vbox->addWidget(widget(CodeFinderWidget));
    widget(CodeFinderWidget)->hide();
    win->setCentralWidget(central);

    QSettings settings(QSettings::UserScope, QLatin1String("AtomBox"));
    QVariant geometry = settings.value(QLatin1String("/debugging/mainWindowGeometry"));
    if (geometry.isValid())
        win->restoreGeometry(geometry.toByteArray());
    QVariant state = settings.value(QLatin1String("/debugging/mainWindowState"));
    if (state.isValid())
        win->restoreState(state.toByteArray());

    const_cast<ScriptRemoteTargetDebugger*>(this)->m_standardWindow = win;

    win->setWindowTitle(QObject::tr("AtomBox Script Inspector"));
    return win;
}

void ScriptRemoteTargetDebugger::showStandardWindow()
{
    (void)standardWindow(); // ensure it's created
    m_standardWindow->setWindowTitle(QObject::tr("AtomBox Script Inspector"));
    m_standardWindow->show();
}

QWidget *ScriptRemoteTargetDebugger::widget(DebuggerWidget widget) const
{
    const_cast<ScriptRemoteTargetDebugger*>(this)->createDebugger();
    return m_debugger->widget(static_cast<QScriptDebugger::DebuggerWidget>(widget));
}

QAction *ScriptRemoteTargetDebugger::action(DebuggerAction action) const
{
    ScriptRemoteTargetDebugger *that = const_cast<ScriptRemoteTargetDebugger*>(this);
    that->createDebugger();
    return m_debugger->action(static_cast<QScriptDebugger::DebuggerAction>(action), that);
}

QToolBar *ScriptRemoteTargetDebugger::createStandardToolBar(QWidget *parent)
{
    createDebugger();
    return m_debugger->createStandardToolBar(parent, this);
}

QMenu *ScriptRemoteTargetDebugger::createStandardMenu(QWidget *parent)
{
    createDebugger();
    return m_debugger->createStandardMenu(parent, this);
}

bool ScriptRemoteTargetDebugger::autoShowStandardWindow() const
{
    return m_autoShow;
}

void ScriptRemoteTargetDebugger::setAutoShowStandardWindow(bool autoShow)
{
    if (autoShow == m_autoShow)
        return;
    if (autoShow) {
        QObject::connect(this, SIGNAL(evaluationSuspended()),
                         this, SLOT(showStandardWindow()));
    } else {
        QObject::disconnect(this, SIGNAL(evaluationSuspended()),
                            this, SLOT(showStandardWindow()));
    }
    m_autoShow = autoShow;
}

#include "scriptremotetargetdebugger.moc"
